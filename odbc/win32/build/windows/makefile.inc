
#target
SUBTARGET=odbc32

#DYNAMICCRT: yes/no
DYNAMICCRT=yes

#add include path
compilerflags += /I./include \
                 /I../../sqlenforcer/include \
                 /I"$(BOOSTDIR)" \
                 /I"$(BUILDROOT)/commonlib/include"

#library path
linkerflags += /LIBPATH:"$(BUILDROOT)/commonlib/win_$(TARGETENVARCH)_$(BUILDTYPE)"

#library
linkerflags +=  commonlib.lib Shell32.lib
           
SRC=$(wildcard  ./src/*.cpp)


DEF=./src/odbc.def
