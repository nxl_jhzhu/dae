#ifndef DAE_ENFORCER_MGR_H
#define  DAE_ENFORCER_MGR_H

#include <stdio.h>
#include <memory>
#include <map>
#include "DAETableMetadata.h"


struct SqlException;


class DAEEnforceMgr{
public:

    static DAEEnforceMgr* Instance(){
        static DAEEnforceMgr* pEnforcer = new DAEEnforceMgr();
        return pEnforcer;
    }
public:
    bool InitalizeEnforcer(SqlException* pExc);
    bool NewEnforcerCtx(const std::string & dbstring, void* srvp);
    bool SetUserAttributes(void* hd);
    bool EvaluationSql(std::string & sql, bool & deny, const std::string & sqlold, void* svcp);
    bool IsDeny(void* stmtp); // for insert/update/delete
    void BindServerHandle(void* svcp, void* srvp);
    void UpdateDenyMap(void* stmtp, bool deny);
    void ClearHandle(void* hd, int type);

    void SetEnvHandle(const void* handle){if(!_envp) _envp = handle;}
    bool BindMetadataFunction(void* srcp);

private:

    std::map<uint64_t , uint64_t>  _map_srv2nxl;   //srvhp  <->  enforcer_ctx   // for OCIStmtPrepare2
    std::map<uint64_t, uint64_t>  _map_src2srv; //service context 2 server
    std::map<uint64_t, bool> _map_stmt2deny;
    std::map<uint64_t, DAEQueryMetadata*> _map_metadata_func;
    const void* _envp = nullptr;// to alloc handle

};




#endif