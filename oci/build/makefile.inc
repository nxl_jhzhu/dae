
#target
SUBTARGET=oci

#DYNAMICCRT: yes/no
DYNAMICCRT=yes

#add include path
compilerflags += /I./include \
                 /I./include/oci \
                 /I../../sqlenforcer/include \
                 /I"$(BUILDROOT)/commonlib/include"

#library path
linkerflags += /LIBPATH:"$(BUILDROOT)/commonlib/win_$(TARGETENVARCH)_$(BUILDTYPE)"

#library
linkerflags +=  commonlib.lib Shell32.lib
           
SRC=$(wildcard  ./src/*.cpp ./src/windows/*.cpp)


DEF=./src/oci.def
