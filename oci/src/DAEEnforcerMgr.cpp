#include "SqlException.h"
#include "DAEEnforcerWrapper.h"
#include "commfun.h"
#include "DAELog.h"
#include <atomic>
std::atomic_flag lock_stream = ATOMIC_FLAG_INIT;

#include "EMDBConfig.h"
#include "DAEEnforcerMgr.h"
#include "DAETableMetadata.h"
#include "DAEUserAttrsOpr.h"

std::atomic_int64_t inti_ret = 0;

// Workaround to fix bug 66860
SqlException storedExc;

#include "oci.h"


bool DAEEnforceMgr::InitalizeEnforcer(SqlException* pExc)
{
    bool bInit = lock_stream.test_and_set();
    if (!bInit)
    {
        std::vector<std::string> vec;
        bool bRet = true;
        do{
            //load config file
            EMDBConfig::GetInstance();
//            const std::string strCfgFile = CommonFun::GetConfigFilePath();
//            if(NULL == theConfig){
//                theConfig = new CConfig();
//
//                bRet = theConfig->ReadConfig(strCfgFile.c_str(), vec);
//
//                if (!bRet) {
//                    delete theConfig;
//                    theConfig = NULL;
//                    pExc->SetInfo(ERR_CONFIG, detail_err_config);
//                    break;
//                }
//            }
            //load enforcer
            std::string dllpath = CommonFun::GetEnforcerModule();
            printf("GetEnforcerModule:%s\n",dllpath.c_str());
            bRet = Enforcer::Instance()->LoadEnforcer(dllpath);
            if (bRet) {
                EMDBReturn binit = (EMDBReturn)Enforcer::Instance()->EMDBInit("jdbc");
                if(binit == EMDB_SUCCESS){
                    bRet = true;
                    printf("EMDBInit succeed\n");
                } else {
                    bRet = false;
                    printf("EMDBInit failed\n");
                }

                //Log::_logfunc = Enforcer::Instance()->SQLEnforcerWriteLogA;
            } else {
                pExc->SetInfo(ERR_LIBRARY, detail_err_lib_sqlenfoercer);
            }
        } while(0);

        if(bRet){
            inti_ret = 1;
            for (auto errMsg : vec)
            {
                Log::WLog(log_error,errMsg.c_str());
            }
        } else {
            inti_ret = 0;
            storedExc = *pExc;
        }
    }
    else
    {
        *pExc = storedExc;
    }
    return inti_ret==1;
}

bool DAEEnforceMgr::NewEnforcerCtx(const std::string & dbstring, void* srvp){
    size_t irf = dbstring.rfind('/');
    std::string db_name = "";
    if (irf != std::string::npos) {
        db_name = dbstring.substr(irf + 1);
        size_t ilen = db_name.length();
        if(ilen>2 && db_name[0] == '"' && db_name[ilen-1] == '"' ){
          db_name = db_name.substr(1, ilen-2);
        }
    }
    //
    size_t ilf = dbstring.rfind(':');
    std::string server_name = "";
    if (ilf != std::string::npos) {
        server_name = dbstring.substr(0,ilf);
        size_t ilen = server_name.length();
        if(ilen>2 && server_name[0] == '"' && server_name[ilen-1] == '"' ){
            server_name = server_name.substr(1, ilen-2);
        }
    }

    int ret = -1;
    //USES_CONVERSION;
    void* hd = nullptr;
    if (Enforcer::Instance()->IsLoaded())
    {
        ret = Enforcer::Instance()->EMDBNewUserCtx(
                server_name.c_str(),
                db_name.c_str(),
                "",
                "Oracle",
                &hd);

        SetUserAttributes(hd);
    }
    //theLog.WriteLog(1, "RUN EMDBNewUserCtx %s", ret == 0 ?"succeed":"failed");

    if (ret == 0) {
        _map_srv2nxl[(uint64_t)srvp] = (uint64_t)hd;
        //theLog.WriteLog(1, L"--------_map_srv2nxl[%d]=%d",srvhp,hd);
    }
    return true;
}


bool DAEEnforceMgr::SetUserAttributes(void* hd){

//    auto it1 = _map_src2srv.find((uint64_t)srcp);
//    if (it1 == _map_src2srv.end()) {
//        return false;
//    }
//    uint64_t srv = it1->second;
//    auto it2 = _map_srv2nxl.find(srv);
//    if (it2 == _map_srv2nxl.end()) {
//        return false;
//    }
//    void * hd = (void*)(it2->second);
//    if (!hd) {
//        return false;
//    }
    int ret = -1;
    std::map<std::string,std::string> _users;
    DAEUserAttrsOpr::QueryUserAttrs(_users);
    for(auto & it:_users){
        ret = Enforcer::Instance()->EMDBSetUserCtxProperty(hd, it.first.c_str(), it.second.c_str());
        if(ret != 0){
            Log::WLog(log_error,"set user attr failed!");
        }
    }
    return true;

}

bool DAEEnforceMgr::BindMetadataFunction(void* srcp){
    auto it1 = _map_src2srv.find((uint64_t)srcp);
    if (it1 == _map_src2srv.end()) {
        return false;
    }
    uint64_t srv = it1->second;
    auto it2 = _map_srv2nxl.find(srv);
    if (it2 == _map_srv2nxl.end()) {
        return false;
    }
    void * hd = (void*)(it2->second);
    if (!hd) {
        return false;
    }
    auto it3 = _map_metadata_func.find((uint64_t)srcp);
    if(it3 == _map_metadata_func.end()){
        DAEQueryMetadata * data = new DAEQueryMetadata(srcp, _envp);
        QueryMetadataFunc func  = std::bind(&DAEQueryMetadata::QueryMetadata,data,std::placeholders::_1,std::placeholders::_2);
        Enforcer::Instance()->EMDBQueryMetadata_cb(hd,func);
        _map_metadata_func[(uint64_t)srcp] = data;
    }
    return true;
}

bool DAEEnforceMgr::EvaluationSql(std::string & sql_new, bool & deny, const std::string & sql_old, void* svcp){

    if (sql_old.find("all_cons_columns") != std::string::npos) {
        return false;
    }
    //theLog.WriteLog(1, "ENTER OCIStmtPrepare2_ori sql:%s", c_str_sql.c_str());

    auto it1 = _map_src2srv.find((uint64_t)svcp);
    if (it1 == _map_src2srv.end()) {
        return false;
    }
    uint64_t srv = it1->second;
    auto it2 = _map_srv2nxl.find(srv);
    if (it2 == _map_srv2nxl.end()) {
        return false;
    }
    void * hd = (void*)(it2->second);
    if (!hd) {
        return false;
    }


//    std::string table = "CUSTOMER1";
//    data.QueryMetadata(table,meta);
//    std::string test = table + " Metadata:";
//    for(auto it:meta){
//        test+=it.column;
//        test+=" ";
//        test+=std::to_string(it.type);
//        test+="  ";
//    }
//    Log::WLog(log_info,test.c_str());

    EMDBResultHandle result;
    int ret = Enforcer::Instance()->EMDBNewResult(&result);
    if (ret != 0){
        return false;
    }

    ret = Enforcer::Instance()->EMDBEvalSql(sql_old.c_str(), hd, result);
    if (ret != 0) {
        return false;
    }

    EMDBResultCode code;
    ret = Enforcer::Instance()->EMDBResultGetCode(result, &code);
    if (ret != 0) {
        return false;
    }

    const char* output = NULL;
    ret = Enforcer::Instance()->EMDBResultGetDetail(result, &output);
    if (ret != 0) {
        return false;
    }

    if (code == EMDB_USE_NEW_TEXT) {
        sql_new = output;
        Log::WLog(log_info, "-----enforcer succeed sql:%s", sql_new.c_str());
    }
    else if(code == EMDB_DENY_EXECUTE){
        deny = true;
    }
    //theLog.WriteLog(1, L"************NewSQL is [%s]", wstrNewSQL.c_str());

    ret = Enforcer::Instance()->EMDBFreeResult(result);
    if (ret != 0) {
        return false;
    }

    return true;
}

bool DAEEnforceMgr::IsDeny(void* stmtp)  {// for insert/update/delete
    if(stmtp)
        return false;
    auto it = _map_stmt2deny.find((uint64_t)stmtp);
    if(it != _map_stmt2deny.end()){
        return it->second;
    }
    return false;
}


void DAEEnforceMgr::BindServerHandle(void* svcp,void* srvp){
    _map_src2srv[(uint64_t)svcp]=(uint64_t)srvp;
}

void DAEEnforceMgr::UpdateDenyMap(void* stmtp, bool deny){
    _map_stmt2deny[(uint64_t)stmtp]=deny;
}

void DAEEnforceMgr::ClearHandle(void* hd, int type){
    if(OCI_HTYPE_SVCCTX == type){
        auto it = _map_src2srv.find((uint64_t)hd);
        if (it != _map_src2srv.end()) {
            _map_src2srv.erase(it);
        }
        auto it2 =  _map_metadata_func.find((uint64_t)hd);
        if (it2 != _map_metadata_func.end()) {
            delete(it2->second);
            _map_metadata_func.erase(it2);
        }
    } else if(OCI_HTYPE_SERVER == type) {
        auto it = _map_srv2nxl.find((uint64_t)hd);
        if(it != _map_srv2nxl.end()) {
            void * hd = (void*)(it->second);
            if (hd) {
                int ret = Enforcer::Instance()->EMDBClearUserCtxProperty(hd);
                assert(ret==EMDB_SUCCESS);
                ret = Enforcer::Instance()->EMDBFreeUserCtx(hd);
                assert(ret==EMDB_SUCCESS);
            }
            _map_srv2nxl.erase(it);
        }
    } else if(OCI_HTYPE_STMT == type){
        auto it = _map_stmt2deny.find((uint64_t)hd);
        if(it != _map_stmt2deny.end()){
            _map_stmt2deny.erase(it);
        }
    }
}