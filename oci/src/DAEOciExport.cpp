
#include "DAEOciWrapper.h"
#include "DAEEnforcerWrapper.h"
#include "DAELog.h"
#include "SqlException.h"
#include "DAEEnforcerMgr.h"
#include <map>
#include <vector>
#include <assert.h>
#include "oci.h"


//static std::vector<uint64_t> _vec_nxl; // for OCIStmtPrepare

#ifdef _WIN32

#define  OCIHandleAlloc_DAE OCIHandleAlloc
#define  OCIHandleFree_DAE OCIHandleFree
#define  OCIStmtPrepare2_DAE OCIStmtPrepare2
#define  OCIStmtExecute_DAE OCIStmtExecute
#define  OCIServerAttach_DAE OCIServerAttach
#define  OCIAttrSet_DAE OCIAttrSet

#else

#define  OCIHandleAlloc_DAE OCIHandleAlloc_new
#define  OCIHandleFree_DAE OCIHandleFree_new
#define  OCIStmtPrepare2_DAE OCIStmtPrepare2_new
#define  OCIStmtExecute_DAE OCIStmtExecute_new
#define  OCIServerAttach_DAE OCIServerAttach_new
#define  OCIAttrSet_DAE OCIAttrSet_new

//OCIBindByName_new
//OCIBindByName2_new
//OCIBindByPos_new
//OCIBindByPos2_new
//OCIDefineByPos_new
//OCIStmtFetch_new
//OCIStmtFetch2_new
//OCIAttrGet_new

#endif

/*

sword   OCIEnvCreate (OCIEnv **envp, ub4 mode, void  *ctxp,
                 void  *(*malocfp)(void  *ctxp, size_t size),
                 void  *(*ralocfp)(void  *ctxp, void  *memptr, size_t newsize),
                 void   (*mfreefp)(void  *ctxp, void  *memptr),
                 size_t xtramem_sz, void  **usrmempp)
{
    Log::WLog(log_info, "--OCIEnvCreate");
    {
        SqlException exc;
        bool bret = InitalizeEnforcer(&exc);
        assert(bret);
    }
    if(DAEOCIMgrApi::Instance().OCIEnvCreate){
        return DAEOCIMgrApi::Instance().OCIEnvCreate(envp, mode, ctxp,
        malocfp,
        ralocfp,
        mfreefp,
        xtramem_sz, usrmempp);
    } 

    return -1;

}


sword   OCIEnvInit (OCIEnv **envp, ub4 mode, 
                    size_t xtramem_sz, void  **usrmempp)
{
    Log::WLog(log_info, "--OCIEnvInit");
    {
        SqlException exc;
        bool bret = InitalizeEnforcer(&exc);
        assert(bret);
    }
        if(DAEOCIMgrApi::Instance().OCIEnvInit){
            return DAEOCIMgrApi::Instance().OCIEnvInit(envp,  mode, 
            xtramem_sz, usrmempp);
        } 

        return -1;

}




sword   OCIInitialize   (ub4 mode, void  *ctxp, 
                 void  *(*malocfp)(void  *ctxp, size_t size),
                 void  *(*ralocfp)(void  *ctxp, void  *memptr, size_t newsize),
                 void   (*mfreefp)(void  *ctxp, void  *memptr) )
{
    Log::WLog(log_info, "--OCIInitialize");
        if(DAEOCIMgrApi::Instance().OCIInitialize){
            return DAEOCIMgrApi::Instance().OCIInitialize( mode, ctxp,
                malocfp,
                ralocfp,
                mfreefp);
        } 

        return -1;

}

//---------power bi called this
sword   OCIEnvNlsCreate (OCIEnv **envp, ub4 mode, void  *ctxp,
                 void  *(*malocfp)(void  *ctxp, size_t size),
                 void  *(*ralocfp)(void  *ctxp, void  *memptr, size_t newsize),
                 void   (*mfreefp)(void  *ctxp, void  *memptr),
                 size_t xtramem_sz, void  **usrmempp,
                 ub2 charset, ub2 ncharset)
{
    Log::WLog(log_info, "--OCIEnvNlsCreate");
    {
        SqlException exc;
        bool bret = InitalizeEnforcer(&exc);
        assert(bret);

    }


    if(DAEOCIMgrApi::Instance().OCIEnvNlsCreate){
        return DAEOCIMgrApi::Instance().OCIEnvNlsCreate( envp,mode, ctxp,
            malocfp,
            ralocfp,
            mfreefp,
            xtramem_sz,usrmempp,
            charset,ncharset);
    } 

    return -1;

}

sword   OCIFEnvCreate (OCIEnv **envp, ub4 mode, void  *ctxp,
                 void  *(*malocfp)(void  *ctxp, size_t size),
                 void  *(*ralocfp)(void  *ctxp, void  *memptr, size_t newsize),
                 void   (*mfreefp)(void  *ctxp, void  *memptr),
                 size_t xtramem_sz, void  **usrmempp, void  *fupg)
{
    Log::WLog(log_info, "--OCIFEnvCreate");
        if(DAEOCIMgrApi::Instance().OCIFEnvCreate){
            return DAEOCIMgrApi::Instance().OCIFEnvCreate( envp, mode, ctxp,
            malocfp,
            ralocfp,
            mfreefp,
            xtramem_sz, usrmempp, fupg);
        } 

        return -1;

}

*/

sword OCIHandleAlloc_DAE(const void  *parenth, void  **hndlpp, const ub4 type, const size_t xtramem_sz, void  **usrmempp) {
     Log::WLog(log_info, "--OCIHandleAlloc");
	if (DAEOCIMgrApi::OCIHandleAlloc) {
		//theLog.WriteLog(1, L"ENTER OCIHandleAlloc_ori type=%d", type);
		if (OCI_HTYPE_STMT == type) {
			//theLog.WriteLog(1, L"ENTER OCIHandleAlloc_ori");
		} 
		else if (OCI_HTYPE_SVCCTX == type)
		{
            SqlException exc;
            bool bret = DAEEnforceMgr::Instance()->InitalizeEnforcer(&exc);
            Log::WLog(log_info, exc.cdetail.c_str());
            assert(bret);
            DAEEnforceMgr::Instance()->SetEnvHandle(parenth);
		}
		return DAEOCIMgrApi::OCIHandleAlloc(parenth, hndlpp, type, xtramem_sz, usrmempp);
	}
	Log::WLog(1, "OCIHandleAlloc_ori is NULL");
	return 0;
}

sword   OCIHandleFree_DAE(void  *hndlp, const ub4 type) {
    Log::WLog(log_info, "--OCIHandleFree");
	if (DAEOCIMgrApi::OCIHandleFree) {
        DAEEnforceMgr::Instance()->ClearHandle(hndlp,type);
		return DAEOCIMgrApi::OCIHandleFree(hndlp, type);
	}
	return 0;
}

/*
sword   OCIStmtPrepare(OCIStmt *stmtp, OCIError *errhp, const OraText *stmt, 	ub4 stmt_len, ub4 language, ub4 mode) {
    Log::WLog(log_info, "--OCISmtPrepare");
	sword  ret_value = 0;
	bool bdeny = false;
	if (DAEOCIMgrApi::Instance().OCIStmtPrepare) {
		//theLog.WriteLog(1, L"ENTER OCIStmtPrepare_ori");
		//todo
		do {
			const text *stmt_ori = stmt;
			std::string c_str_sql((const char*)stmt_ori, stmt_len);
			//std::wstring wc_str_sql = MyUtils::s2ws(c_str_sql);

			if (c_str_sql.find("all_cons_columns") != std::string::npos) {
				break;
			}
			//theLog.WriteLog(1, "ENTER OCIStmtPrepare sql:%s", c_str_sql.c_str());
            if(_vec_nxl.size()<= 0){
                Log::WLog(log_info, "_vec_nxl size = 0");
                break;
            }

			void * hd = (void*)_vec_nxl.back();
			if (!hd) break;

			EMDBResultHandle result;
			int ret = Enforcer::Instance()->EMDBNewResult(&result);
			if (ret != 0) break;

			ret = Enforcer::Instance()->EMDBEvalSql(c_str_sql.c_str(), hd, result);
			if (ret != 0) break;

			EMDBResultCode code;
			ret = Enforcer::Instance()->EMDBResultGetCode(result, &code);
			if (ret != 0) break;

			const char* output = NULL;
			ret = Enforcer::Instance()->EMDBResultGetDetail(result, &output);
			if (ret != 0) break;

			if (code == EMDB_USE_NEW_TEXT) {
				c_str_sql = output;
				//theLog.WriteLog(1, "-----enforcer succeed sql:%s", c_str_sql.c_str());
			}
			else if (code == EMDB_DENY_EXECUTE) {
				bdeny = true;
				break;
			}
			ret = Enforcer::Instance()->EMDBFreeResult(result);

			ret_value = DAEOCIMgrApi::Instance().OCIStmtPrepare(stmtp, errhp, (const text*)(c_str_sql.c_str()), (ub4)(c_str_sql.length()), language, mode);
			_map_stmt2deny[(uint64_t)(stmtp)] = bdeny;
			return ret_value;

		} while (0);

		ret_value = DAEOCIMgrApi::Instance().OCIStmtPrepare(stmtp, errhp, stmt, stmt_len, language, mode);
		_map_stmt2deny[(uint64_t)(stmtp)] = bdeny;
		return ret_value;
	}

	return 0;
}
*/

sword OCIStmtPrepare2_DAE(OCISvcCtx *svchp, OCIStmt **stmtp, OCIError *errhp, 	const OraText *stmt, ub4 stmt_len, const OraText *key, 	ub4 key_len, ub4 language, ub4 mode) {
	Log::WLog(log_info, "--OCIStmtPrepare2");
    sword  ret_value = 0;
	if (DAEOCIMgrApi::OCIStmtPrepare2) {
		//theLog.WriteLog(1, L"ENTER OCIStmtPrepare2_ori");

        const text *stmt_ori = stmt;
        bool deny = false;
        std::string sql_old((const char*)stmt_ori, stmt_len);
        std::string sql_new = sql_old;

        Log::WLog(log_info, "sql_old:%s",stmt_ori);



		if(DAEEnforceMgr::Instance()->EvaluationSql(sql_new,deny, sql_old, svchp)){
            Log::WLog(log_info, "dae_sql_new:%s",sql_new.c_str());
            ret_value = DAEOCIMgrApi::OCIStmtPrepare2(svchp, stmtp, errhp, (const text*)(sql_new.c_str()), (ub4)(sql_new.length()), key, key_len, language, mode);
		} else {
            ret_value = DAEOCIMgrApi::OCIStmtPrepare2(svchp, stmtp, errhp, stmt, stmt_len, key, key_len, language, mode);
		}
        DAEEnforceMgr::Instance()->UpdateDenyMap((void*)(*stmtp), deny);

		return ret_value;
	}
	return 0;
}

sword   OCIStmtExecute_DAE(OCISvcCtx *svchp, OCIStmt *stmtp, OCIError *errhp, ub4 iters, ub4 rowoff, const OCISnapshot *snap_in,	OCISnapshot *snap_out, ub4 mode) {
	Log::WLog(log_info, "--OCIStmtExecute");
    if (DAEOCIMgrApi::OCIStmtExecute) {
		//theLog.WriteLog(1, L"ENTER OCIStmtExecute_ori");
		if (DAEEnforceMgr::Instance()->IsDeny(stmtp)) {
			return 0; // succeed deny
		}
		else {
			return DAEOCIMgrApi::OCIStmtExecute(svchp, stmtp, errhp, iters, rowoff, snap_in, snap_out, mode);
		}
	}
	return 0;
}


sword   OCIServerAttach_DAE(OCIServer *srvhp, OCIError *errhp,	const OraText *dblink, sb4 dblink_len, ub4 mode) {
	Log::WLog(log_info, "--OCIServerAttach");
    if (DAEOCIMgrApi::OCIServerAttach) {

		const text *db = dblink;
		std::string c_str_db((const char*)db, dblink_len);
		//
        DAEEnforceMgr::Instance()->NewEnforcerCtx(c_str_db, srvhp);
        DAEEnforceMgr::Instance()->BindMetadataFunction(srvhp);
		//
		return DAEOCIMgrApi::OCIServerAttach(srvhp, errhp, dblink, dblink_len, mode);
	}
	return 0;
}



sword   OCIAttrSet_DAE(void  *trgthndlp, ub4 trghndltyp, void  *attributep,
	ub4 size, ub4 attrtype, OCIError *errhp) {
    Log::WLog(log_info, "--OCIAttrSet");
	if (DAEOCIMgrApi::OCIAttrSet) {
		if (OCI_HTYPE_SVCCTX == trghndltyp && OCI_ATTR_SERVER == attrtype) {
            DAEEnforceMgr::Instance()->BindServerHandle((void*)trgthndlp, (void*)attributep);
            DAEEnforceMgr::Instance()->BindMetadataFunction((void*)trgthndlp);
		}
		return DAEOCIMgrApi::OCIAttrSet(trgthndlp, trghndltyp, attributep, size, attrtype, errhp);
	}
	return 0;
}




