//
// Created by jeff on 2021/6/22.
//

#include "DAEUserAttrsOpr.h"
#include <fstream>
#include "DAELog.h"

void DAEUserAttrsOpr::QueryUserAttrs(std::map<std::string,std::string> & userattrs){
#ifdef HARD_CODE
    std::ifstream t("/usr/nextlabs/emdb/config/userattr.txt");
    std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    int i = 0;
    int size = str.length();
    std::string line;
    while(i < size){
        if(str[i] == '\n'){
            if(line.length()>2){
                size_t  ifind = line.find('=');
                if(ifind != std::string::npos){
                    std::string key = line.substr(0,ifind);
                    std::string val = line.substr(ifind+1);
                    userattrs[key]=val;
                }

            }
            line.clear();
        } else {
            line+=str[i];
        }
        i++;
    }
#else
    //TODO: get user attr
#endif
}
