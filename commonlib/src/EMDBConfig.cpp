#include "EMDBConfig.h"

#include <string>

#include "commfun.h"


static constexpr auto CFG_INSTALL_PATH                      = "GLOBAL.install_path";

static constexpr auto CFG_POLICY_CCHOST                     = "POLICY.cchost";
static constexpr auto CFG_POLICY_CCPORT                     = "POLICY.ccport";
static constexpr auto CFG_POLICY_JPCHOST                    = "POLICY.jpchost";
static constexpr auto CFG_POLICY_JPCPORT                    = "POLICY.jpcport";
static constexpr auto CFG_POLICY_JPCUSER                    = "POLICY.jpcuser";
static constexpr auto CFG_POLICY_JPCPWD                     = "POLICY.jpcpwd";
static constexpr auto CFG_POLICY_MODELNAME                  = "POLICY.modelname";

static constexpr auto CFG_LOG_LEVEL                         = "LOG.level";
static constexpr auto CFG_LOG_AUDITLOG_ALL                  = "LOG.auditlogall";
static constexpr auto CFG_LOG_SWITCH_REPORT                 = "LOG.switch_report";

static constexpr auto CFG_USERMODE_MODE                     = "USERMODE.mode";
static constexpr auto CFG_USERMODE_JDBC_CONN_STRING         = "USERMODE.jdbc_conn_string";
static constexpr auto CFG_USERMODE_ODBC_CONN_STRING         = "USERMODE.odbc_conn_string";
static constexpr auto CFG_USERMODE_USER_INFO                = "USERMODE.user_info";
static constexpr auto CFG_USERMODE_CONN_TYPE                = "USERMODE.conn_type";
static constexpr auto CFG_USERMODE_SYNC_TIME                = "USERMODE.sync_time";
static constexpr auto CFG_USERMODE_MULTI_VALUE_SEPERATOR    = "USERMODE.multi_value_separator";

static constexpr auto CFG_EMDBERROR_EXC_BREAK               = "EMDB_ERROR.exception_handler";
static constexpr auto CFG_EMDBERROR_STAR_BREAK              = "EMDB_ERROR.star_handler";



ConnectionType EMDBConfig::conn_type_ = ConnectionType::kNone;



void EMDBConfig::Initialize(const ConnectionType& conn_type)
{
    conn_type_ = conn_type;
    EMDBConfig::GetInstance();
}

EMDBConfig EMDBConfig::GetInstance()
{
    static EMDBConfig config;
    return config;
}

std::string EMDBConfig::GetConfigFilePath()
{
#ifdef _WIN32
    return "C:\\ProgramData\\Nextlabs\\EMDatabase\\Config\\config.ini";
#else
    return "/usr/nextlabs/emdb/config/config.ini";
#endif
}

std::string EMDBConfig::GenerateErrorMessage(const std::string &key, bool have_default_value)
{
    if (have_default_value)
    {
        return "define a wrong value for " + key + " parameter, will use the default value";
    }
    else
    {
        return "define a wrong value for " + key + " parameter";
    }
}



std::string EMDBConfig::get_global_install_path() const
{
    return global_install_path_;
}

std::string EMDBConfig::get_policy_cchost() const
{
    return policy_cchost_;
}

std::string EMDBConfig::get_policy_ccport() const {
    return policy_ccport_;
}

std::string EMDBConfig::get_policy_jpchost() const {
    return policy_jpchost_;
}

std::string EMDBConfig::get_policy_jpcport() const {
    return policy_jpcport_;
}

std::string EMDBConfig::get_policy_jpcuser() const
{
    return policy_jpcuser_;
}

std::string EMDBConfig::get_policy_jpcpwd() const
{
    return policy_jpcpwd_;
}

std::string EMDBConfig::get_policy_modelname() const
{
    return policy_modelname_;
}

LogLevel EMDBConfig::get_log_level() const
{
    return log_level_;
}

bool EMDBConfig::get_log_auditlogall() const
{
    return log_auditlogall_;
}

unsigned int EMDBConfig::get_log_switch_report() const
{
    return log_switch_report_;
}

Usermode EMDBConfig::get_usermode_mode() const
{
    return usermode_mode_;
}

std::string EMDBConfig::get_usermode_jdbc_conn_string() const
{
    return usermode_jdbc_conn_string_;
}

std::string EMDBConfig::get_usermode_odbc_conn_string() const
{
    return usermode_odbc_conn_string_;
}

std::string EMDBConfig::get_usermode_user_info() const
{
    return usermode_user_info_;
}

unsigned EMDBConfig::get_usermode_sync_time() const
{
    return usermode_sync_time_;
}

std::string EMDBConfig::get_usermode_multi_value_separator() const
{
    return usermode_multi_value_separator_;
}

HandlerType EMDBConfig::get_emdb_error_exception_handler() const
{
    return emdb_error_exception_handler_;
}

HandlerType EMDBConfig::get_emdb_error_star_handler() const
{
    return emdb_error_star_handler_;
}

std::vector<std::string> EMDBConfig::get_initial_errors() const
{
    return initial_error;
}



void EMDBConfig::set_global_install_path(const std::string& install_path)
{
    if (!install_path.empty())
    {
#ifdef _WIN32
        #ifdef _WIN64
        global_install_path_ = install_path + "\\Common\\bin64";
    #else
        global_install_path_ = install_path + "\\Common\\bin32";
    #endif
#else
        global_install_path_ = install_path + "/Common/bin64";
#endif
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_policy_cchost(const std::string &cchost)
{
    if (CommonFun::IsValidHttpUrl(cchost))
    {
        policy_cchost_ = cchost;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_policy_ccport(const std::string &ccport)
{
    if (CommonFun::IsValidPort(ccport))
    {
        policy_ccport_ = ccport;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_policy_jpchost(const std::string &jpchost)
{
    if (CommonFun::IsValidHttpUrl(jpchost))
    {
        policy_jpchost_ = jpchost;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_policy_jpcport(const std::string &jpcport)
{
    if (CommonFun::IsValidPort(jpcport))
    {
        policy_jpcport_ = jpcport;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_policy_jpcuser(const std::string &jpcuser)
{
    if (!jpcuser.empty())
    {
        policy_jpcuser_ = jpcuser;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_policy_jpcpwd(const std::string &jpcpwd)
{
    if (!jpcpwd.empty())
    {
        policy_jpcpwd_ = jpcpwd;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_policy_modelname(const std::string &modelname)
{
    if (!modelname.empty())
    {
        policy_modelname_ = modelname;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_log_level(const LogLevel level)
{
    if (level >= LogLevel::kTrace && level <= LogLevel::kFatal)
    {
        log_level_ = level;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_log_auditlogall(const bool auditlogall)
{
    log_auditlogall_ = auditlogall;
}

void EMDBConfig::set_log_switch_report(const unsigned int switch_report)
{
    if (switch_report == 0 || switch_report == 1 || switch_report == LOG_CIPHER)
    {
        log_switch_report_ = switch_report;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_usermode_mode(const Usermode mode)
{
    if (mode >= Usermode::kApplication && mode <= Usermode::kActiveDirectory)
    {
        usermode_mode_ = mode;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_usermode_jdbc_conn_string(const std::string &conn_string)
{
    if (!conn_string.empty())
    {
        usermode_jdbc_conn_string_ = conn_string;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_usermode_odbc_conn_string(const std::string &conn_string)
{
    if (!conn_string.empty())
    {
        usermode_odbc_conn_string_ = conn_string;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_usermode_user_info(const std::string &user_info)
{
    if (!user_info.empty())
    {
        usermode_user_info_ = user_info;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_usermode_sync_time(const unsigned int days)
{
    if (days > 0 && days < 366)
    {
        usermode_sync_time_ = days * 86400;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_usermode_multi_value_separator(const std::string& multi_value_separator)
{
    if (!multi_value_separator.empty())
    {
        usermode_multi_value_separator_ = multi_value_separator;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_emdb_error_exception_handler(const std::string& exception_handler)
{
    if (CommonFun::CaseInsensitiveEquals(exception_handler, "deny"))
    {
        emdb_error_exception_handler_ = HandlerType::kDeny;
    }
    else if (CommonFun::CaseInsensitiveEquals(exception_handler, "allow"))
    {
        emdb_error_exception_handler_ = HandlerType::kAllow;
    }
    else
    {
        throw std::out_of_range("");
    }
}

void EMDBConfig::set_emdb_error_star_handler(const std::string& star_handler)
{
    if (CommonFun::CaseInsensitiveEquals(star_handler, "deny"))
    {
        emdb_error_star_handler_ = HandlerType::kDeny;
    }
    else if (CommonFun::CaseInsensitiveEquals(star_handler, "allow"))
    {
        emdb_error_star_handler_ = HandlerType::kAllow;
    }
    else
    {
        throw std::out_of_range("");
    }
}

EMDBConfig::EMDBConfig()
{
    auto config_file_path = GetConfigFilePath();
    boost::property_tree::iptree tree;
    boost::property_tree::ini_parser::read_ini(config_file_path, tree);

    set_global_install_path(tree);

    set_policy_cchost(tree);
    set_policy_ccport(tree);
    set_policy_jpchost(tree);
    set_policy_jpcport(tree);
    set_policy_jpcuser(tree);
    set_policy_jpcpwd(tree);
    set_policy_modelname(tree);

    set_log_level(tree);
    set_log_auditlogall(tree);
    set_log_switch_report(tree);

    set_usermode_mode(tree);

    if (Usermode::kDatabase == usermode_mode_)
    {
        if (ConnectionType::kJdbc == conn_type_)
        {
            set_usermode_jdbc_conn_string(tree);
        }
        else if (ConnectionType::kOdbc == conn_type_)
        {
            set_usermode_odbc_conn_string(tree);
        }
        set_usermode_sync_time(tree);
    }

    if (Usermode::kDatabase == usermode_mode_ || Usermode::kActiveDirectory == usermode_mode_)
    {
        set_usermode_user_info(tree);
    }

    set_usermode_multi_value_separator(tree);

    set_emdb_error_exception_handler(tree);
    set_emdb_error_star_handler(tree);
}


void EMDBConfig::set_global_install_path(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto install_path = config_property_tree.get<std::string>(CFG_INSTALL_PATH);
        set_global_install_path(install_path);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_INSTALL_PATH, false));
    }
}

void EMDBConfig::set_policy_cchost(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto cchost = config_property_tree.get<std::string>(CFG_POLICY_CCHOST);
        set_policy_cchost(cchost);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_POLICY_CCHOST, false));
    }
}

void EMDBConfig::set_policy_ccport(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto ccport = config_property_tree.get<std::string>(CFG_POLICY_CCPORT);
        set_policy_ccport(ccport);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_POLICY_CCPORT, false));
    }
}

void EMDBConfig::set_policy_jpchost(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto jpchost = config_property_tree.get<std::string>(CFG_POLICY_JPCHOST);
        set_policy_jpchost(jpchost);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_POLICY_JPCHOST, false));
    }
}

void EMDBConfig::set_policy_jpcport(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto jpcport = config_property_tree.get<std::string>(CFG_POLICY_JPCPORT);
        set_policy_jpcport(jpcport);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_POLICY_JPCPORT, false));
    }
}

void EMDBConfig::set_policy_jpcuser(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto jpcuser = config_property_tree.get<std::string>(CFG_POLICY_JPCUSER);
        set_policy_jpcuser(jpcuser);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_POLICY_JPCUSER, false));
    }
}

void EMDBConfig::set_policy_jpcpwd(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto jpcpwd = config_property_tree.get<std::string>(CFG_POLICY_JPCPWD);
        set_policy_jpcpwd(jpcpwd);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_POLICY_JPCPWD, false));
    }
}

void EMDBConfig::set_policy_modelname(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto modelname = config_property_tree.get<std::string>(CFG_POLICY_MODELNAME);
        set_policy_modelname(modelname);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_POLICY_MODELNAME, false));
    }
}

void EMDBConfig::set_log_level(const boost::property_tree::iptree& config_property_tree)
{
    try
    {
        auto level = static_cast<LogLevel>(config_property_tree.get<unsigned>(CFG_LOG_LEVEL));
        set_log_level(level);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_LOG_LEVEL, true));
    }
}

void EMDBConfig::set_log_auditlogall(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto auditlogall = config_property_tree.get<bool>(CFG_LOG_AUDITLOG_ALL);
        set_log_auditlogall(auditlogall);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_LOG_AUDITLOG_ALL, true));
    }
}

void EMDBConfig::set_log_switch_report(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto switch_report = config_property_tree.get<unsigned int>(CFG_LOG_SWITCH_REPORT);
        set_log_switch_report(switch_report);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_LOG_SWITCH_REPORT, true));
    }
}

void EMDBConfig::set_usermode_mode(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto mode = static_cast<Usermode>(config_property_tree.get<unsigned>(CFG_USERMODE_MODE));
        set_usermode_mode(mode);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_USERMODE_MODE, true));
    }
}

void EMDBConfig::set_usermode_jdbc_conn_string(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto conn_string = config_property_tree.get<std::string>(CFG_USERMODE_JDBC_CONN_STRING);
        set_usermode_jdbc_conn_string(conn_string);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_USERMODE_JDBC_CONN_STRING, false));
    }
}

void EMDBConfig::set_usermode_odbc_conn_string(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto conn_string = config_property_tree.get<std::string>(CFG_USERMODE_ODBC_CONN_STRING);
        set_usermode_odbc_conn_string(conn_string);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_USERMODE_ODBC_CONN_STRING, false));
    }
}

void EMDBConfig::set_usermode_user_info(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto user_info = config_property_tree.get<std::string>(CFG_USERMODE_USER_INFO);
        set_usermode_user_info(user_info);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_USERMODE_USER_INFO, false));
    }
}

void EMDBConfig::set_usermode_sync_time(const boost::property_tree::iptree& config_property_tree)
{
    try
    {
        auto days = config_property_tree.get<unsigned int>(CFG_USERMODE_SYNC_TIME);
        set_usermode_sync_time(days);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_USERMODE_SYNC_TIME, true));
    }
}

void EMDBConfig::set_usermode_multi_value_separator(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto multi_value_separator = config_property_tree.get<std::string>(CFG_USERMODE_MULTI_VALUE_SEPERATOR);
        set_usermode_multi_value_separator(multi_value_separator);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_USERMODE_MULTI_VALUE_SEPERATOR, true));
    }
}

void EMDBConfig::set_emdb_error_exception_handler(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto exception_handler = config_property_tree.get<std::string>(CFG_EMDBERROR_EXC_BREAK);
        set_emdb_error_exception_handler(exception_handler);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_EMDBERROR_EXC_BREAK, true));
    }
}

void EMDBConfig::set_emdb_error_star_handler(const boost::property_tree::iptree &config_property_tree)
{
    try
    {
        auto star_handler = config_property_tree.get<std::string>(CFG_EMDBERROR_STAR_BREAK);
        set_emdb_error_star_handler(star_handler);
    }
    catch (...)
    {
        initial_error.push_back(GenerateErrorMessage(CFG_EMDBERROR_STAR_BREAK, true));
    }
}

