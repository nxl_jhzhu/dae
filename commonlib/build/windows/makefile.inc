

#target
STATICTARGET=commonlib

#DYNAMICCRT: yes/no
DYNAMICCRT=yes

#define
compilerflags += 

#add include path
compilerflags += /I./include \
                 /I../sqlenforcer/include \
                 /I"$(BOOSTDIR)" \
                 /I"$(OPENSSLDIR)" \
                 /I"$(MSENFORCECOMMON)/prod/QueryCloudAZSDK_C++/QueryCloudAZSDKCpp/include" \

#library path
linkerflags += 

#library
linkerflags += 
           
SRC= ./src/commfun.cpp \
     ./src/EMDBConfig.cpp \
     ./src/EnforcerDataMgr.cpp \
     ./src/enforcerwrapper.cpp \
     ./src/iodbc_macros.cpp \
     ./src/nanodbc.cpp \
     ./src/ODBCMgrApi.cpp