#ifndef ODBC_MGR_API_H
#define ODBC_MGR_API_H

#include "odbc.h"
class ODBCMgrApi
{
protected:
	ODBCMgrApi() ;
	ODBCMgrApi(const ODBCMgrApi&) {}

public:
	static ODBCMgrApi& Instance()
	{
		static ODBCMgrApi odbcApi;
		return odbcApi;
	}

	bool InitODBCMgrApi();

public:
	SQLAllocHandleFun  SQLAllocHandle = nullptr;
	SQLSetEnvAttrFun   SQLSetEnvAttr = nullptr;
	SQLDriverConnectFun SQLDriverConnect = nullptr;
	SQLDriverConnectAFun SQLDriverConnectA = nullptr;
	SQLDriverConnectWFun SQLDriverConnectW = nullptr;
	SQLExecDirectFun SQLExecDirect;
	SQLExecDirectAFun SQLExecDirectA;
	SQLExecDirectWFun    SQLExecDirectW;
	SQLNumResultColsFun  SQLNumResultCols;
	SQLCloseCursorFun   SQLCloseCursor;
	SQLFreeStmtFun   SQLFreeStmt;
	SQLBindColFun  SQLBindCol;
	SQLFetchFun  SQLFetch;
	SQLGetDataFun SQLGetData;
	SQLGetDiagRecFun SQLGetDiagRec;
	SQLGetDiagRecAFun SQLGetDiagRecA;
	SQLGetDiagRecWFun SQLGetDiagRecW;
	SQLConnectFun SQLConnect;
	SQLConnectAFun SQLConnectA;
	SQLConnectWFun SQLConnectW;
	SQLBrowseConnectFun SQLBrowseConnect;
	SQLBrowseConnectAFun SQLBrowseConnectA;
	SQLBrowseConnectWFun SQLBrowseConnectW;
    SQLFreeHandleFun SQLFreeHandle;
	SQLDisconnectFun SQLDisconnect;
	SQLGetInfoFun SQLGetInfo;
	//add by jeff 19-04-18
	
	SQLCompleteAsyncFun SQLCompleteAsync;
	SQLCancelHandleFun SQLCancelHandle;
	SQLGetInfoAFun SQLGetInfoA;
	SQLGetInfoWFun SQLGetInfoW;
	//
	SQLAllocConnectFun SQLAllocConnect;
	SQLAllocEnvFun SQLAllocEnv=nullptr;
	SQLAllocHandleStdFun SQLAllocHandleStd;
	SQLAllocStmtFun  SQLAllocStmt;
	//SQLBindColFun SQLBindCol;
	SQLBindParamFun SQLBindParam;
	SQLBindParameterFun SQLBindParameter;
	SQLBulkOperationsFun SQLBulkOperations;
	//Sqlcancel Sqlcancel;
	//SQLCloseCursor SQLCloseCursor;

	SQLColAttributeFun SQLColAttribute;
	SQLColAttributeAFun SQLColAttributeA;
	SQLColAttributeWFun SQLColAttributeW;
	SQLColAttributesFun SQLColAttributes;
	SQLColAttributesAFun SQLColAttributesA;
	SQLColAttributesWFun SQLColAttributesW;
	SQLColumnPrivilegesFun SQLColumnPrivileges;
	SQLColumnPrivilegesAFun SQLColumnPrivilegesA;
	SQLColumnPrivilegesWFun SQLColumnPrivilegesW;
	SQLColumnsFun SQLColumns;

	SQLColumnsAFun SQLColumnsA;
	SQLColumnsWFun SQLColumnsW;
	SQLDescribeColFun  SQLDescribeCol;
	SQLDescribeColAFun SQLDescribeColA;
	SQLDescribeColWFun SQLDescribeColW;
	SQLDescribeParamFun SQLDescribeParam;
	SQLEndTranFun SQLEndTran;
	SQLErrorFun SQLError;
	SQLErrorAFun SQLErrorA;
	SQLErrorWFun SQLErrorW;

	SQLExecuteFun SQLExecute;
	SQLExtendedFetchFun SQLExtendedFetch;
	//SQLFetchFun SQLFetch;
	SQLFetchScrollFun SQLFetchScroll;
	SQLForeignKeysFun SQLForeignKeys;
	SQLForeignKeysAFun SQLForeignKeysA;
	SQLForeignKeysWFun SQLForeignKeysW;
	SQLFreeConnectFun SQLFreeConnect;
	//SQLFreeStmtFun SQLFreeStmt;
	SQLGetConnectAttrFun SQLGetConnectAttr;

	SQLGetConnectAttrAFun SQLGetConnectAttrA;
	SQLGetConnectAttrWFun SQLGetConnectAttrW;
	SQLGetConnectOptionFun SQLGetConnectOption;
	SQLGetConnectOptionAFun SQLGetConnectOptionA;
	SQLGetConnectOptionWFun SQLGetConnectOptionW;
	SQLGetCursorNameFun SQLGetCursorName ;
	SQLGetCursorNameAFun SQLGetCursorNameA;
	SQLGetCursorNameWFun SQLGetCursorNameW;
	//SQLGetDataFun SQLGetData;
	SQLGetDiagFieldFun SQLGetDiagField;

	SQLGetDiagFieldAFun SQLGetDiagFieldA;
	SQLGetDiagFieldWFun SQLGetDiagFieldW;
	SQLGetEnvAttrFun SQLGetEnvAttr=nullptr;
	SQLGetFunctionsFun SQLGetFunctions;
	SQLGetStmtAttrFun SQLGetStmtAttr;
	SQLGetStmtAttrAFun SQLGetStmtAttrA;
	SQLGetStmtAttrWFun SQLGetStmtAttrW ;
	SQLGetStmtOptionFun SQLGetStmtOption;
	SQLGetTypeInfoFun SQLGetTypeInfo;
	SQLGetTypeInfoAFun SQLGetTypeInfoA;

	SQLGetTypeInfoWFun SQLGetTypeInfoW;
	SQLMoreResultsFun SQLMoreResults;
	SQLNativeSqlFun SQLNativeSql;
	SQLNativeSqlAFun SQLNativeSqlA;
	SQLNativeSqlWFun SQLNativeSqlW;
	SQLNumParamsFun SQLNumParams;
	//SQLNumResultColsFun SQLNumResultCols;
	SQLParamDataFun SQLParamData;
	SQLParamOptionsFun SQLParamOptions;
	SQLPrepareFun SQLPrepare;

	SQLPrepareAFun SQLPrepareA;
	SQLPrepareWFun SQLPrepareW;
	SQLPrimaryKeysFun SQLPrimaryKeys;
	SQLPrimaryKeysAFun SQLPrimaryKeysA;
	SQLPrimaryKeysWFun SQLPrimaryKeysW;
	SQLProcedureColumnsFun SQLProcedureColumns;
	SQLProcedureColumnsAFun SQLProcedureColumnsA;
	SQLProcedureColumnsWFun SQLProcedureColumnsW;
	SQLProceduresFun SQLProcedures;
	SQLProceduresAFun SQLProceduresA;

	SQLProceduresWFun SQLProceduresW;
	SQLPutDataFun SQLPutData;
	SQLRowCountFun SQLRowCount;
	SQLSetConnectAttrFun SQLSetConnectAttr;
	SQLSetConnectAttrAFun SQLSetConnectAttrA;
	SQLSetConnectAttrWFun SQLSetConnectAttrW;
	SQLSetConnectOptionFun SQLSetConnectOption;
	SQLSetConnectOptionAFun SQLSetConnectOptionA;
	SQLSetConnectOptionWFun SQLSetConnectOptionW;
	SQLSetCursorNameFun SQLSetCursorName;

	SQLSetCursorNameAFun SQLSetCursorNameA;
	SQLSetCursorNameWFun SQLSetCursorNameW;
	SQLSetParamFun SQLSetParam;
	SQLSetPosFun SQLSetPos;
	SQLSetScrollOptionsFun SQLSetScrollOptions;
	SQLSetStmtAttrFun SQLSetStmtAttr;
	SQLSetStmtAttrAFun SQLSetStmtAttrA;
	SQLSetStmtAttrWFun SQLSetStmtAttrW;
	SQLSetStmtOptionFun SQLSetStmtOption;
	SQLSpecialColumnsFun SQLSpecialColumns;

	SQLSpecialColumnsAFun SQLSpecialColumnsA;
	SQLSpecialColumnsWFun SQLSpecialColumnsW;
	SQLStatisticsFun SQLStatistics;
	SQLStatisticsAFun SQLStatisticsA;
	SQLStatisticsWFun SQLStatisticsW;
	SQLTablePrivilegesFun SQLTablePrivileges;
	SQLTablePrivilegesAFun SQLTablePrivilegesA;
	SQLTablePrivilegesWFun SQLTablePrivilegesW;
	SQLTablesFun SQLTables;
	SQLTablesAFun SQLTablesA;

	SQLTablesWFun SQLTablesW;
	SQLTransactFun SQLTransact;

	//19-04-24
	SQLCancelFun SQLCancel;
	SQLCopyDescFun SQLCopyDesc;
	SQLDataSourcesFun SQLDataSources;
	SQLDataSourcesAFun SQLDataSourcesA;
	SQLDataSourcesWFun SQLDataSourcesW;
	SQLDriversFun SQLDrivers;
	SQLDriversAFun SQLDriversA;
	SQLDriversWFun SQLDriversW;
	SQLFreeEnvFun SQLFreeEnv=nullptr;
	SQLGetDescFieldFun SQLGetDescField;

	SQLGetDescFieldAFun SQLGetDescFieldA;
	SQLGetDescFieldWFun SQLGetDescFieldW;
	SQLGetDescRecFun SQLGetDescRec;
	SQLGetDescRecAFun SQLGetDescRecA;
	SQLGetDescRecWFun SQLGetDescRecW;
	SQLSetDescFieldFun SQLSetDescField;
	SQLSetDescFieldAFun SQLSetDescFieldA;
	SQLSetDescFieldWFun SQLSetDescFieldW;
	SQLSetDescRecFun SQLSetDescRec;
	//SQLSetEnvAttrFun SQLSetEnvAttr;
	bool m_bInit = false;
};

#endif

