#ifndef SQLENFORCER_COMMONLIB_INCLUDE_EMDBCONFIG_H
#define SQLENFORCER_COMMONLIB_INCLUDE_EMDBCONFIG_H

// Used as cipher to check whether log sql
#define LOG_CIPHER 4312 // TODO: Remove it

#include <string>
#include <vector>
#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>


enum class LogLevel
{
    kTrace = 0,
    kDebug,
    kInfo,
    kWarning,
    kError,
    kFatal
};

enum class Usermode
{
    kApplication = 0,
    kDatabase,
    kActiveDirectory
};

enum class HandlerType
{
    kDeny = 0,
    kAllow
};

enum class ConnectionType
{
    kNone = 0,
    kJdbc,
    kOdbc
};


class EMDBConfig
{
private:
    static ConnectionType conn_type_;

public:

    static void Initialize(const ConnectionType& conn_type);
    static EMDBConfig GetInstance();
    static std::string GetConfigFilePath();
    static std::string GenerateErrorMessage(const std::string& key, bool have_default_value);



    std::string get_global_install_path() const;
    std::string get_policy_cchost() const;
    std::string get_policy_ccport() const;
    std::string get_policy_jpchost() const;
    std::string get_policy_jpcport() const;
    std::string get_policy_jpcuser() const;
    std::string get_policy_jpcpwd() const;
    std::string get_policy_modelname() const;

    LogLevel get_log_level() const;
    bool get_log_auditlogall() const;
    unsigned int get_log_switch_report() const;

    Usermode get_usermode_mode() const;
    std::string get_usermode_jdbc_conn_string() const;
    std::string get_usermode_odbc_conn_string() const;
    std::string get_usermode_user_info() const;
    unsigned get_usermode_sync_time() const;
    std::string get_usermode_multi_value_separator() const;

    HandlerType get_emdb_error_exception_handler() const;
    HandlerType get_emdb_error_star_handler() const;

    std::vector<std::string> get_initial_errors() const;



    void set_global_install_path(const std::string& install_path);
    void set_policy_cchost(const std::string& cchost);
    void set_policy_ccport(const std::string& ccport);
    void set_policy_jpchost(const std::string& jpchost);
    void set_policy_jpcport(const std::string& jpcport);
    void set_policy_jpcuser(const std::string& jpcuser);
    void set_policy_jpcpwd(const std::string& jpcpwd);
    void set_policy_modelname(const std::string& modelname);

    void set_log_level(const LogLevel level);
    void set_log_auditlogall(const bool auditlogall);
    void set_log_switch_report(const unsigned int switch_report);

    void set_usermode_mode(const Usermode mode);
    void set_usermode_jdbc_conn_string(const std::string& conn_string);
    void set_usermode_odbc_conn_string(const std::string& conn_string);
    void set_usermode_user_info(const std::string& user_info);
    void set_usermode_sync_time(const unsigned int sync_time);
    void set_usermode_multi_value_separator(const std::string& multi_value_separator);

    void set_emdb_error_exception_handler(const std::string& exception_handler);
    void set_emdb_error_star_handler(const std::string& star_handler);


private:

    EMDBConfig();



    void set_global_install_path(const boost::property_tree::iptree& config_property_tree);
    void set_policy_cchost(const boost::property_tree::iptree& config_property_tree);
    void set_policy_ccport(const boost::property_tree::iptree& config_property_tree);
    void set_policy_jpchost(const boost::property_tree::iptree& config_property_tree);
    void set_policy_jpcport(const boost::property_tree::iptree& config_property_tree);
    void set_policy_jpcuser(const boost::property_tree::iptree& config_property_tree);
    void set_policy_jpcpwd(const boost::property_tree::iptree& config_property_tree);
    void set_policy_modelname(const boost::property_tree::iptree& config_property_tree);

    void set_log_level(const boost::property_tree::iptree& config_property_tree);
    void set_log_auditlogall(const boost::property_tree::iptree& config_property_tree);
    void set_log_switch_report(const boost::property_tree::iptree& config_property_tree);

    void set_usermode_mode(const boost::property_tree::iptree& config_property_tree);
    void set_usermode_jdbc_conn_string(const boost::property_tree::iptree& config_property_tree);
    void set_usermode_odbc_conn_string(const boost::property_tree::iptree& config_property_tree);
    void set_usermode_user_info(const boost::property_tree::iptree& config_property_tree);
    void set_usermode_sync_time(const boost::property_tree::iptree& config_property_tree);
    void set_usermode_multi_value_separator(const boost::property_tree::iptree& config_property_tree);

    void set_emdb_error_exception_handler(const boost::property_tree::iptree& config_property_tree);
    void set_emdb_error_star_handler(const boost::property_tree::iptree& config_property_tree);



    std::string global_install_path_;

    std::string policy_cchost_;
    std::string policy_ccport_;
    std::string policy_jpchost_;
    std::string policy_jpcport_;
    std::string policy_jpcuser_;
    std::string policy_jpcpwd_;
    std::string policy_modelname_;

//    std::map<std::string, std::string> odbc_map_; // TODO: Delete or use?
//    std::map<std::string, std::string> jdbc_map_; // TODO: Delete or use?

    LogLevel log_level_ = LogLevel::kInfo;
    bool log_auditlogall_ = false;
    unsigned int log_switch_report_ = 0; // TODO: Chang into bool
    Usermode usermode_mode_ = Usermode::kDatabase;

    std::string usermode_jdbc_conn_string_;
    std::string usermode_odbc_conn_string_;
    std::string usermode_user_info_;
    unsigned usermode_sync_time_ = 86400;    // Unit seconds; 86400s == 1 day
    std::string usermode_multi_value_separator_ = ",";

    HandlerType emdb_error_exception_handler_ = HandlerType::kDeny;
    HandlerType emdb_error_star_handler_ = HandlerType::kDeny;

    std::vector<std::string> initial_error;

};

#endif //SQLENFORCER_COMMONLIB_INCLUDE_EMDBCONFIG_H
