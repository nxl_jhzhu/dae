#ifndef IPOLICY_H_20190314
#define IPOLICY_H_20190314

#include <string>
#include "iuserattr.h"
#include "IPolicyResource.h"
#include "commfun.h"
#include "EMDBType.h"
#include "../../sqlparser/libsql2003/include/EMMaskDef.h"
#include "../../sqlparser/libsql2003/include/keydef.h"

//policy action define
#define ACTION_NONE   ""
#define ACTION_SELECT "SELECT"
#define ACTION_UPDATE "UPDATE"
#define ACTION_DELETE "DELETE"
#define ACTION_INSERT "INSERT"
#define ACTION_DAE_CONFIG "DAE_CONFIG"
#define ACTION_CALL_SP "CALL_SP"


#define CONDITION_EQUAL       "="
#define CONDITION_NOT_EQUAL   "!="
#define CONDITION_SMALL       "<"
#define CONDITION_SMALL_EQUAL "<="
#define CONDITION_LARGE       ">"
#define CONDITION_LARGE_EQUAL ">="

#include <vector>
struct ConditionInfo {
    ConditionInfo(const uint64_t table_id,
                  const std::string & real,
                  const std::string & name,
                  const std::string & act,
                  EMDB_DB_TYPE type,
                  const IPolicyResource *pPolicyRes,
                  IUserAttr *pU,
                  const std::vector<std::string> & cols,
                  NodeType e):
            table_id(table_id),
            real_tbname(real),
            tableName(name),
            _action(act),
            dbType(type),
            pPolicyResource(pPolicyRes),
            pUserAttr(pU),
            _using_cols(cols),
             _join_tp(e){
    }

    uint64_t table_id;
    std::string real_tbname;
    std::string tableName;
    std::string _action;
    EMDB_DB_TYPE dbType;
    const IPolicyResource *pPolicyResource;
    IUserAttr *pUserAttr;
    std::vector<std::string> _using_cols;
    NodeType _join_tp;
};

#endif 