

#target
SUBTARGET=userattribute

#DYNAMICCRT: yes/no
DYNAMICCRT=yes

#define
compilerflags += /DENFORCER_EXPORT

#add include path
compilerflags += /I./include \
                 /I../policymgr/include \
                /I"$(BOOSTDIR)" \
                /I"$(BUILDROOT)/commonlib/include" \
                /I"$(MSENFORCECOMMON)/prod/QueryCloudAZSDK_C++/QueryCloudAZSDKCpp/include" \


#library path
linkerflags += /LIBPATH:"$(BUILDROOT)/commonlib/win_$(TARGETENVARCH)_$(BUILDTYPE)"

#library
linkerflags +=  commonlib.lib Shell32.lib Activeds.lib ADSIid.lib
           
SRC= src/userattr_export.cpp \
     src/userattr_mgr.cpp \
     src/strptime.cpp \
     src/operator_between.cpp \
     src/userattr.cpp \
     src/log.cpp \
     src/ImplQueryUserAttributeAD_WIN.cpp \
     src/WinAD.cpp