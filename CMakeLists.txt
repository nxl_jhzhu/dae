# We need CMake 3.8 at least, because we require
# CMAKE_CXX_STANDARD to be set to C++17.
cmake_minimum_required(VERSION 3.10)
project(DAE)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_VERBOSE_MAKEFILE on)
set(OUT_PUT_SO "/home/jeff/emdb_so/Common/bin64/")

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${OUT_PUT_SO})
#add_definitions("-Wall -g")

add_definitions(-DNANODBC_THROW_NO_SOURCE_LOCATION)


add_subdirectory(commonlib)
add_subdirectory(userattribute)
add_subdirectory(./sqlparser/libsql2003)
add_subdirectory(sqlenforcer)
add_subdirectory(odbc/linux)
add_subdirectory(query_cloud_az_sdk/QueryCloudAZSDKCpp)
add_subdirectory(jdbc)
add_subdirectory(oci)
add_subdirectory(oci/thirdpart/linux/funchook)
add_subdirectory(test)