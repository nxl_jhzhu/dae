//
// Created by jeff on 2020/10/26.
//

#ifndef EMDB_TEST_20201026_H
#define EMDB_TEST_20201026_H

#include "./third_party/microtest.h"
#include "commfun.h"
#include "nanodbc.h"
#include "userattr.h"
#include "userattr_export.h"
#include "SqlException.h"
#include "sqlenforcer_export.h"
#include <sqlext.h>
#include "EMMaskDef.h"
#include "parser.h"
#include "emdb_sdk.h"
#include "QueryCAZPolicyOpr.h"
#include <set>




TEST(ORACLE_TEST){

    EMDBReturn RT =  EMDBInit("jdbc");

     RT =  EMDBInit("jdbc");
    if(RT != EMDB_SUCCESS ){
        return;
    }

    EMDBUserCtxHandle hd;
    RT = EMDBNewUserCtx("10.23.56.70:39015", "HXE", "GUEST","Oracle", &hd);
    if(RT != EMDB_SUCCESS ){
        return;
    }


  //  RT = EMDBSetUserCtxProperty(hd, "name", "Joy Wu");


    RT = EMDBSetUserCtxProperty(hd, "user_id", "3");
    RT = EMDBSetUserCtxProperty(hd, "user_name", "guest");
    RT = EMDBSetUserCtxProperty(hd, "level", "2");
    RT = EMDBSetUserCtxProperty(hd, "birthday", "1995-05-08");
    RT = EMDBSetUserCtxProperty(hd, "age", "24");
    RT = EMDBSetUserCtxProperty(hd, "title", "Software Engineer");//"employee"
    RT = EMDBSetUserCtxProperty(hd, "checked", "1");
    RT = EMDBSetUserCtxProperty(hd, "countrycode", "18");
    RT = EMDBSetUserCtxProperty(hd, "salary", "1200");
    RT = EMDBSetUserCtxProperty(hd, "roles", "Admin,CEO,Manager");
//    RT = EMDBSetUserCtxProperty(hd, "level", "3");
//    RT = EMDBSetUserCtxProperty(hd, "checked", "true");
//    RT = EMDBSetUserCtxProperty(hd, "age", "24");
//    RT = EMDBSetUserCtxProperty(hd, "city", "");
//    RT = EMDBSetUserCtxProperty(hd, "countrycode", "18");
//    RT = EMDBSetUserCtxProperty(hd, "salary", "1200.0");

    std::vector<std::string> vec;
    //vec.push_back("CREATE TABLe ORCLCDB.\"HR\".\"hahah_S\"");
   // vec.push_back("/* SSSS */CREATE TABLe \"HR\".\"hahah_S\"");
//    vec.push_back("SELECT time_c,\n"
//                  "       date_c,\n"
//                  "       timestamp_c\n"
//                  "FROM   GUEST.DATATYPE_MASK2");
    vec.push_back("select A.* from GUEST.\"CUSTOMER2\"  a;");
//    vec.push_back("update guest.CUSTOMER1 \n"
//                  "set c_mktsegment=null;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_privilege_level = return0() \n"
//                  "where c_mktsegment LIKE '%e%';");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set GUEST.CUSTOMER3.c_mktsegment=(\n"
//                  "select GUEST.CUSTOMER1.c_mktsegment \n"
//                  "from GUEST.CUSTOMER1  \n"
//                  "where GUEST.CUSTOMER1.customer_id IN (1,8)\n"
//                  ")\n"
//                  "where salary >= 300;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set GUEST.CUSTOMER3.c_mktsegment=(\n"
//                  "select c_mktsegment from (\n"
//                  "SELECT c_mktsegment,customer_id \n"
//                  "from GUEST.CUSTOMER1\n"
//                  "INTERSECT\n"
//                  "select c_mktsegment,customer_id \n"
//                  "from GUEST.CUSTOMER2\n"
//                  ") c \n"
//                  ")\n"
//                  "where c_mktsegment = 'retail';");
//    vec.push_back("update GUEST.CUSTOMER3\n"
//                  "set c_privilege_level = (\n"
//                  "select sum(c_privilege_level) \n"
//                  "from GUEST.CUSTOMER1\n"
//                  ")\n"
//                  "where c_mktsegment = 'retail';");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'retail_test' \n"
//                  "where customer_id >= all (\n"
//                  "select customer_id \n"
//                  "from GUEST.CUSTOMER2 \n"
//                  "where c_privilege_level > 1\n"
//                  ");");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2705' \n"
//                  "where c_mktsegment like 're%'");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2711 4 10' \n"
//                  "where c_mktsegment not like '%rr%' escape 'r'");
//    vec.push_back("update GUEST.CUSTOMER3\n"
//                  "set c_mktsegment = 'test 2717' \n"
//                  "where exists(\n"
//                  "select * from GUEST.CUSTOMER2\n"
//                  "where c_mktsegment != 'Education'\n"
//                  ")");
//    vec.push_back("update GUEST.CUSTOMER3\n"
//                  "set c_mktsegment = 'test 2724' \n"
//                  "where CUSTOMER3.customer_id in (\n"
//                  "select customer_id \n"
//                  "from GUEST.CUSTOMER2\n"
//                  ");");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2729 all' \n"
//                  "where (\n"
//                  "select o_orderdate \n"
//                  "from GUEST.ORDERS \n"
//                  "where order_id in (1,2)\n"
//                  "  ) < CURRENT_TIMESTAMP;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set C_CUSTKEY = 'TEST3 5 8 10 11' \n"
//                  "WHERE C_MKTSEGMENT!='FM'||'CG';");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set GUEST.CUSTOMER3.c_mktsegment=(\n"
//                  "select GUEST.CUSTOMER1.c_mktsegment \n"
//                  "from GUEST.CUSTOMER1  \n"
//                  "where GUEST.CUSTOMER1.customer_id IN (1,8)\n"
//                  ")\n"
//                  "where salary >= 300;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set GUEST.CUSTOMER3.c_mktsegment=(\n"
//                  "select c_mktsegment from (\n"
//                  "SELECT c_mktsegment,customer_id \n"
//                  "from GUEST.CUSTOMER1\n"
//                  "INTERSECT\n"
//                  "select c_mktsegment,customer_id \n"
//                  "from GUEST.CUSTOMER2\n"
//                  ") c \n"
//                  ")\n"
//                  "where c_mktsegment = 'retail';");
//    vec.push_back("update GUEST.CUSTOMER3\n"
//                  "set c_privilege_level=(\n"
//                  "select b.c_privilege_level\n"
//                  "from GUEST.CUSTOMER1 a \n"
//                  "join GUEST.CUSTOMER2 b\n"
//                  "on a.customer_id=b.customer_id\n"
//                  "where b.customer_id = 14\n"
//                  "group by b.c_privilege_level\n"
//                  ") + 15\n"
//                  "where c_mktsegment = 'retail';");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment='test 2847-1 11' \n"
//                  "where c_mktsegment like concat(\n"
//                  "(\n"
//                  "select c_mktsegment \n"
//                  "from GUEST.CUSTOMER1 \n"
//                  "where c_privilege_level in (3, -1)\n"
//                  "),\n"
//                  "'%'\n"
//                  ");");
//    vec.push_back("update GUEST.CUSTOMER1\n"
//                  "set c_mktsegment='test 2848' \n"
//                  "where c_mktsegment like concat(\n"
//                  "(\n"
//                  "select c_mktsegment \n"
//                  "from GUEST.CUSTOMER1 \n"
//                  "where CUSTOMER_ID in (6,8)\n"
//                  "),\n"
//                  "'TTe%'\n"
//                  ")\n"
//                  "ESCAPE 'T';");
//    vec.push_back("update GUEST.CUSTOMER3\n"
//                  "set c_mktsegment = 'test 2859' \n"
//                  "where exists(\n"
//                  "select * from GUEST.CUSTOMER2\n"
//                  "where c_mktsegment != 'Education'\n"
//                  ")");
//    vec.push_back("update GUEST.CUSTOMER3\n"
//                  "set c_mktsegment = 'test 2866' \n"
//                  "where CUSTOMER3.customer_id in (\n"
//                  "select customer_id \n"
//                  "from GUEST.CUSTOMER2\n"
//                  ");");
//    vec.push_back("update GUEST.CUSTOMER1 \n"
//                  "set c_mktsegment = 'test 2868' \n"
//                  "where guest.return0() in (\n"
//                  "select C_PRIVILEGE_LEVEL\n"
//                  "from GUEST.CUSTOMER3\n"
//                  ") \n"
//                  "OR ABS(c_privilege_level) = 1;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2863 all' \n"
//                  "where (\n"
//                  "select count(*) \n"
//                  "from GUEST.CUSTOMER1\n"
//                  ") = 13 \n"
//                  "And not exists(\n"
//                  "select * from GUEST.CUSTOMER1\n"
//                  "where c_privilege_level = -1\n"
//                  ")");
//    vec.push_back("update GUEST.CUSTOMER3\n"
//                  "set c_mktsegment = 'test 2882 4 10' \n"
//                  "where c_privilege_level >= \n"
//                  "(\n"
//                  "select c_privilege_level \n"
//                  "from GUEST.CUSTOMER1 \n"
//                  "where customer_id in (8,9)\n"
//                  ")+1;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2880 3'\n"
//                  "where c_privilege_level <= 1*-(\n"
//                  "select c_privilege_level \n"
//                  "from GUEST.CUSTOMER1\n"
//                  "where customer_id in (8,9)\n"
//                  ");");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2877 5 8 11' \n"
//                  "where CASE c_mktsegment \n"
//                  "  when TO_NCHAR('Catering') \n"
//                  "  then customer_id \n"
//                  "  ELSE (\n"
//                  "Case when 'building' not in(\n"
//                  "select c_mktsegment \n"
//                  "from GUEST.CUSTOMER1 \n"
//                  "where c_privilege_level < 1\n"
//                  ") \n"
//                  "then c_privilege_level \n"
//                  "END \n"
//                  ") \n"
//                  "  END = 1;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2876 all' \n"
//                  "where CASE when 'building' in (\n"
//                  "select c_mktsegment \n"
//                  "from GUEST.CUSTOMER1 \n"
//                  "where c_privilege_level < 0\n"
//                  ") then c_privilege_level \n"
//                  "Else customer_id \n"
//                  "END >= 0;");
//    vec.push_back("update GUEST.CUSTOMER1\n"
//                  "set c_mktsegment = 'test 2875 4 16' \n"
//                  "where CASE c_mktsegment \n"
//                  "  when (\n"
//                  "select c_mktsegment \n"
//                  "from GUEST.CUSTOMER3\n"
//                  "where customer_id in (1,4)\n"
//                  ") \n"
//                  "  then c_privilege_level \n"
//                  "  end <= 1;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2874 4 5 8 10 11' \n"
//                  "where Case when 'building' not in(\n"
//                  "select c_mktsegment \n"
//                  "from GUEST.CUSTOMER1 \n"
//                  "where c_privilege_level < 1\n"
//                  ") then c_privilege_level \n"
//                  "END > 0;");
//    vec.push_back("update GUEST.CUSTOMER3 \n"
//                  "set c_mktsegment = 'test 2873 all' \n"
//                  "where (select sum(c_privilege_level) \n"
//                  "from GUEST.CUSTOMER1 \n"
//                  "where c_mktsegment = 'FMCG'\n"
//                  "  ) > 0;");
//    vec.push_back("update top 5 GUEST.CUSTOMER3\n"
//                  "set GUEST.CUSTOMER3.c_privilege_level =20");
//    vec.push_back("");
   // vec.push_back("/* SSSS */DROP table Autotest.customer");
   // vec.push_back("DROP TABLe IF");
    //vec.push_back(" (SELECT customer_id, c_privilege_level, c_mktsegment FROM guest.customer1  ORDER BY 1  )  FOR UPDATE OF c_privilege_level, c_mktsegment   ");
//    vec.push_back("select \n"
//                  "    a.[account_id] as [account_id],\n"
//                  "    a.[account_name] as [account_name],\n"
//                  "    a.[salary] as [salary],\n"
//                  "    a.[bank_account] as [bank_account],\n"
//                  "    a.[bank_name] as [bank_name]\n"
//                  "from [dbo].[salary] as a");



//    vec.push_back("SELECT C_CUSTKEY FROM \"Case Sensitive\"");
//    vec.push_back("SELECT * FROM \"Case Sensitive\" ");
//    vec.push_back("SELECT aa.* FROM \"Case Sensitive\" aa");
//    vec.push_back("SELECT customer_id,c1.c_mktsegment ,c_privilege_level, c2.c_mktsegment FROM\n"
//    "GUEST.CUSTOMER_MASK c1 INNER JOIN GUEST.CUSTOMER1 c2 USING (customer_id,\n"
//    "c_privilege_level)");
//    vec.push_back("SELECT customer_id,c1.c_mktsegment ,c_privilege_level, c2.c_mktsegment FROM\n"
//                  "GUEST.CUSTOMER_MASK c1 INNER JOIN GUEST.CUSTOMER1 c2 USING (customer_id)");
//    vec.push_back("select c_mktsegment from ONLY(CUSTOMER_MASK)");
//    vec.push_back("SELECT GUEST.CUSTOMER_MASK.C_MKTSEGMENT FROM GUEST.CUSTOMER_MASK");
//    vec.push_back("SELECT C_CUSTKEY FROM \"Case Sensitive\"");
//    vec.push_back("select C_CUSTKEY from ONLY(\"Case Sensitive\") a");
    //vec.push_back("insert into system.customer_insert(customer_id) values(55)");
   // vec.push_back("insert into system.customer_insert(customer_id) values(55)");
   // vec.push_back("SELECT C_MKTSEGMENT, COUNT(C_PRIVILEGE_LEVEL) FROM GUEST.CUSTOMER_MASK GROUP BY C_MKTSEGMENT HAVING VAR_POP(C_PRIVILEGE_LEVEL) > 1 order by C_MKTSEGMENT,COUNT(C_PRIVILEGE_LEVEL)");
//    vec.push_back("SELECT C_MKTSEGMENT,\n"
//                  "       C_PRIVILEGE_LEVEL\n"
//                  "FROM   (SELECT a.customer_id,\n"
//                  "               a.C_MKTSEGMENT,\n"
//                  "               a.C_PRIVILEGE_LEVEL\n"
//                  "        FROM   GUEST.CUSTOMER1 a\n"
//                  "               RIGHT JOIN GUEST.ORDERS b ON a.customer_id = b.order_id  ) c\n"
//                  "       NATURAL INNER JOIN GUEST.CUSTOMER_MASK");
//    vec.push_back("SELECT customer_id,\n"
//                  "       c1.c_mktsegment,\n"
//                  "       c_privilege_level,\n"
//                  "       c2.c_mktsegment\n"
//                  "FROM   GUEST.CUSTOMER_MASK c1\n"
//                  "       INNER JOIN GUEST.CUSTOMER1 c2\n"
//                  "       USING(customer_id,\n"
//                  "             c_privilege_level)");
//    vec.push_back("select * from GUEST.CUSTOMER1");
//    vec.push_back("SELECT C_MKTSEGMENT,\n"
//                  "       C_PRIVILEGE_LEVEL\n"
//                  "FROM   (SELECT customer_id,\n"
//                  "               C_MKTSEGMENT,\n"
//                  "               C_PRIVILEGE_LEVEL\n"
//                  "        FROM   GUEST.CUSTOMER_mask a\n"
//                  "                   NATURAL RIGHT JOIN GUEST.ORDERS b ) c\n"
//                  "           NATURAL INNER JOIN GUEST.CUSTOMER_mask");
//    vec.push_back("with a(a_custkey,total) as (select customer_id,count(customer_id) from GUEST.CUSTOMER_mask\n"
//                      "                            group by customer_id order by customer_id,count(customer_id)) select * from a");
//    vec.push_back("update guest.customer_mask set c_mktsegment = 'testing' where c_privilege_level > 1");
    //vec.push_back("");
    for(auto sql:vec){

        //std::string sql = vec[0];
        // std::string sql = "SELECT customer_id, c_privilege_level FROM `jdbc-enforcer.EMDB_1_2.customer`";
        printf("ori:%s\n", sql.c_str());

        EMDBResultHandle result;
        RT = EMDBNewResult(&result);
        if(RT != EMDB_SUCCESS ){
            continue;
        }


        RT = EMDBEvalSql(sql.c_str(), hd,  result);
        if(RT != EMDB_SUCCESS ){
            continue;
        }


        EMDBResultCode code;
        RT = EMDBResultGetCode( result, &code);
        if(RT != EMDB_SUCCESS ){
            continue;
        }

        const char * output = NULL;
        RT = EMDBResultGetDetail( result, &output);
        if(RT != EMDB_SUCCESS ){
            continue;
        }

        std::string newsql;
        if(code == EMDB_USE_NEW_TEXT){
            newsql = output;
        }
        printf("new:%s\n", newsql.c_str());

        RT = EMDBFreeResult(result);
        if(RT != EMDB_SUCCESS ){
            continue;
        }
        //sleep(20);
    }
    RT = EMDBClearUserCtxProperty(hd);
    if(RT != EMDB_SUCCESS ){
        return;
    }
    RT = EMDBFreeUserCtx(hd);
    if(RT != EMDB_SUCCESS ){
        return;
    }
    if(EMDB_SUCCESS == RT){

    }

}















#endif //EMDB_TEST_20201026_H
