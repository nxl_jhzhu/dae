#include "microtest.h"
/* commonlib */
#include "commfun.h"
#include "EnforcerDataMgr.h"
#include "nanodbc.h"
/* policymgr */
#include <unistd.h>
/* sqlenforcer */
#include "sqlenforcer_export.h"
//#include "test_jeff.h"
//#include "test_mask.h"
//#include "test_20200727.h"
#include "test_20201026.h"
//#include "test_20201111.h"
//#include "test_20201112.h"
//#include "read_timeout.h"
//#include "query_pc_test.h"
#include "SqlException.h"
#include "parser.h"
#include <string>
#include <fstream>


#include "sqlenforcer_sp.h"

#include <iostream>
#include <functional>
typedef int (*func)(int a);

typedef std::function<int (int)> fp;

class a{
public:
    int add(int num){
        return src+num;
    }
    int src = 100;
};
class b{
public:
    static int test(){
        a t;
        fp f  = std::bind(&a::add,&t,std::placeholders::_1);
        int val = f(100);
        printf("%d\n",val);
        return 1;
    }
};

TEST(FUNCTION){
    b::test();
}
//TEST(sp){
//void parser_sp(){
//    std::string sql = "sa.dsp_userinfo_by_salary_range 10, 10000";
//    EMDB_SP_INFO info;
//    EMDBSP_parser_sp_cmd(sql, info);
//    //std::string ori_cmd = EMDBSP_output_new_sp(info);
//    std::map<unsigned int,std::string> sqls;
//    EMDBSP_load_new_sp( info);
//    EMDBSP_carete_indirect_sql(sqls,info);
//    auto sqlit = sqls.begin();
//    for(; sqlit!=sqls.end(); ++sqlit){
//        sqlit->second += " where field > 0";
//    }
//    EMDBSP_resolve_new_sp(sqls, info);
//    std::string new_cmd = EMDBSP_output_new_sp(info);
//
//    return;
//}

#ifdef JEFF


#define SQL_SERVER_CNN_STR NANODBC_TEXT("Driver=ODBC Driver 17 for SQL Server;Server=tcp:bard-dbc-executer.qapf1.qalab01.nextlabs.com,1433;Uid=sa;PWD=123blue!;Database=NXL_USER")
#define ORACLE_CNN_STR NANODBC_TEXT("DRIVER={Oracle 19 ODBC driver};uid=system;PWD=123blue123blue;dbq=EMDB-Oracle.qapf1.qalab01.nextlabs.com:1521/orcl")
#define MYSQL_CNN_STR NANODBC_TEXT("Driver={MySQL ODBC 8.0 Unicode Driver};Server=EMDB-MySQL.qapf1.qalab01.nextlabs.com;Port=3306;Database=sakila;User=root; Password=123blue!;Option=67108864;")

#include "parser.h"
#include "EMMaskOpr.h"
TEST(MASK) {
    std::string src = "select BUS0T_A, BUS0T_C AS CC FROM BUS0T";
    std::string out;
    bool bret = parser::compress_sql(src, out);
    if(bret) {
        printf("ori:%s\n", src.c_str());
        printf("new:%s\n", out.c_str());
    }

}

TEST(SQLENFORCER) {
    SqlException e;
    std::string conf_str = CommonFun::GetConfigFilePath();
    const char *  cfg = conf_str.c_str();
    bool rc = SQLEnforcerInit("odbc", cfg, &e);
    ASSERT_TRUE(rc);
}

//TEST(CConfig) {
//    class CConfig cfg; cfg.ReadConfig(CommonFun::GetConfigFilePath().c_str());
//    cfg.PrintConfigData();
//}

TEST(CommonFun) {
    void *h = CommonFun::LoadShareLibrary("libodbc_nxl.so");
    ASSERT_NOTNULL(h);
    void *f = CommonFun::GetProcAddress(h, "SQLGetDiagRec");
    ASSERT_NOTNULL(f);
    auto folder = CommonFun::GetProgramDataFolder();
    ASSERT_STREQ(folder, "/etc/Nextlabs/EMDatabase");
    auto url = CommonFun::UrlEncode("http://www.jsons.cn/urlencode/");
    ASSERT_STREQ(url, "http%3A%2F%2Fwww.jsons.cn%2Furlencode%2F");
    ASSERT_TRUE(CommonFun::StrCaseCmp("Hello", "hello") == 0);
    ASSERT_FALSE(CommonFun::StrCaseCmp("Hello", "he1llo") == 0);
    char buf[16] = { 0 }; int result = -1;
    bool b = CommonFun::CopyIntToA(12, buf, 16, result);
    ASSERT_TRUE(b && result == 2);
    wchar_t wbuf[16] = { 0 };
    b = CommonFun::CopyIntToW(123, wbuf, 16, result);
    ASSERT_TRUE(b && result == 3);

    std::string dst = "";
    ASSERT_TRUE(CommonFun::ToUTF8(L"hello world", dst) == "hello world" && dst == "hello world");
    std::wstring wdst = L"";
    ASSERT_TRUE(CommonFun::FromUTF8("hello world", wdst) == L"hello world" && wdst == L"hello world");
}

TEST(EnforcerDataMgr) {
    void *env1 = (void*)1;
    void *cnn1 = (void*)501;
    void *stmt1 = (void*)701;

    auto& EDM_INS = EnforcerDataMgr::Instance();
    EDM_INS.alloc_env_handle(env1);
    EDM_INS.alloc_connect_handle(env1, cnn1);
    EDM_INS.bind_stmt_handle(cnn1, stmt1);
    ASSERT_TRUE(EDM_INS.to_connect_handle_proxy(cnn1) != nullptr);
    ASSERT_TRUE(EDM_INS.to_stmt_handle_proxy(stmt1) != nullptr);
    ASSERT_TRUE(EDM_INS.to_connect_handle_proxy((void*)502) == nullptr);

    EDM_INS.push_error(SQL_HANDLE_STMT, stmt1, 10, "test error");
    EDM_INS.push_error(SQL_HANDLE_STMT, stmt1, 20, "another test error");
    EDM_INS.clear_error(SQL_HANDLE_STMT, stmt1);
    ASSERT_TRUE(EDM_INS.to_stmt_handle_proxy(stmt1)->errs_ == nullptr);

    EDM_INS.push_error(SQL_HANDLE_STMT, stmt1, 10, "test error");
    EDM_INS.push_error(SQL_HANDLE_STMT, stmt1, 20, "another test error");
    int i = -1; std::string err; EDM_INS.get_error(SQL_HANDLE_STMT, stmt1, 2, i, err);
    ASSERT_TRUE(i == 20 && err == "another test error");
    while (EDM_INS.get_error_last(SQL_HANDLE_STMT, stmt1, i, err));
    ASSERT_TRUE(EDM_INS.to_stmt_handle_proxy(stmt1)->errs_ == nullptr);

    EDM_INS.freehandle(SQL_HANDLE_STMT, stmt1);
    EDM_INS.freehandle(SQL_HANDLE_DBC, cnn1);
    EDM_INS.freehandle(SQL_HANDLE_ENV, env1);
}

/*
TEST(NANODBC) {
    unsigned int count = 0;
#ifdef SQL_SERVER_CNN_STR
    try {
        nanodbc::connection cnn(SQL_SERVER_CNN_STR);
        nanodbc::statement stmt(cnn, NANODBC_TEXT("SELECT 1 TEST;"));
        nanodbc::result result = nanodbc::execute(stmt);
        short columns = result.columns();
        nanodbc::string const null_value = NANODBC_TEXT("NULL");
        std::string c;
        for (short col = 0; col < columns; ++col) {
            nanodbc::string colname = result.column_name(col);
            std::string c;
            ASSERT_STREQ(CommonFun::ToUTF8(colname, c), "TEST");
        }
        while (result.next()) {
            for (short col = 0; col < columns; ++col) {
                auto value = result.get<int>(col);
                ASSERT_EQ(value, 1);
            }
        }
    }
    catch (nanodbc::database_error const& e) {
        mt::printFailed(e.what());
        ++count;
    }
#endif
#ifdef ORACLE_CNN_STR
    try {
        nanodbc::connection cnn(ORACLE_CNN_STR);
        nanodbc::statement stmt(cnn, NANODBC_TEXT("SELECT 1 test FROM DUAL"));
        nanodbc::result result = nanodbc::execute(stmt);
        short columns = result.columns();
        nanodbc::string const null_value = NANODBC_TEXT("NULL");
        std::string c;
        for (short col = 0; col < columns; ++col) {
            nanodbc::string colname = result.column_name(col);
            std::string c;
            ASSERT_STREQ(CommonFun::ToUTF8(colname, c), "TEST");
        }
        while (result.next()) {
            for (short col = 0; col < columns; ++col) {
                auto value = result.get<int>(col);
                ASSERT_EQ(value, 1);
            }
        }
    }
    catch (nanodbc::database_error const& e) {
        mt::printFailed(e.what());
        ++count;
    }
#endif
#ifdef MYSQL_CNN_STR
    try {
        nanodbc::connection cnn(MYSQL_CNN_STR);
        nanodbc::statement stmt(cnn, NANODBC_TEXT("SELECT 1 TEST;"));
        nanodbc::result result = nanodbc::execute(stmt);
        short columns = result.columns();
        nanodbc::string const null_value = NANODBC_TEXT("NULL");
        std::string c;
        for (short col = 0; col < columns; ++col) {
            nanodbc::string colname = result.column_name(col);
            std::string c;
            ASSERT_STREQ(CommonFun::ToUTF8(colname, c), "TEST");
        }
        while (result.next()) {
            for (short col = 0; col < columns; ++col) {
                auto value = result.get<int>(col);
                ASSERT_EQ(value, 1);
            }
        }
    }
    catch (nanodbc::database_error const& e) {
        mt::printFailed(e.what());
        ++count;
    }
#endif
    ASSERT_EQ(count, 0);
}
*/
#endif
int Log(int nLogLevel, const char* szMsg) {
    printf("%s\n", szMsg);

    return 0;
}

//#include "userattr_export.h"
//TEST(USER_ATTR){
//    std::string url = "Driver=Simba ODBC Driver for Google BigQuery 64-bit;OAuthMechanism = 1;RefreshToken=1//0gjCRgy--nifiCgYIARAAGBASNwF-L9Ir52m0QREgjWcSX9XMCBr7jluzFsyXDGch76LpQd0YAJZcIqrY9xQxdDPn4Is6qm3W94g;Catalog=jdbc-enforcer;";
//    std::string tb = "EMDB_Test.users_attr.user_name";
//    std::string odbc = "odbc";
//    std::string op = ",";
//    bool RET = UserAttrInit(nullptr, 1, url.c_str(), tb.c_str(), odbc.c_str(), op.c_str(), 6000);
//    SqlException exc;
//    IUserAttr* p =  GetUserAttr("joy", &exc);
//
//}

//#include <stdio.h>
//#include <stdlib.h>
//#include <vcclr.h>
//using namespace system;
//TEST(wchar){
//
//        String str = "Hello";
//
//        // Pin memory so GC can't move it while native function is called
//        pin_ptr<const wchar_t> wch = PtrToStringChars(str);
//        printf_s("%S\n", wch);
//
//        // Conversion to char* :
//        // Can just convert wchar_t* to char* using one of the
//        // conversion functions such as:
//        // WideCharToMultiByte()
//        // wcstombs_s()
//        // ... etc
//        size_t convertedChars = 0;
//        size_t  sizeInBytes = ((str->Length + 1) * 2);
//        errno_t err = 0;
//        char    *ch = (char *)malloc(sizeInBytes);
//
//        err = wcstombs_s(&convertedChars,
//                         ch, sizeInBytes,
//                         wch, sizeInBytes);
//        if (err != 0)
//            printf_s("wcstombs_s  failed!\n");
//
//        printf_s("%s\n", ch);
//
//}

TEST(beautify_sql){
   // const std::string src = "SELECT * FROM EMDB_1_2.customer_mask WHERE ((customer_mask.salary =-100.01  or  customer_mask.salary =1.1e2 ) AND (customer_mask.test_a ='test1\'2'  or  customer_mask.test_a ='test1\'1' ) )  ";
    const std::string src = "SELECT t.str_time.x.s_timestamp FROM Autotest.multitype_struct AS\n"
                            "t,t.str_date.y.s_date AS DATE ORDER BY int_c ";
    std::string  out;
    parser::beautify_sqlbigquery( src, out);
    printf("%s\n",out.c_str());
}

/*
void read_sql_from_file(std::vector<std::string> & vec_sql){
    std::ifstream t("/home/jeff/tempsource/sql_oracle_1228.txt");
    std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    int i = 0;
    int size = str.length();
    std::string sql;
    while(i < size){
        if(str[i] == '\n'){
            if(sql.length()>10){
                vec_sql.push_back(sql);
            }
            sql.clear();
        } else {
            sql+=str[i];
        }
        i++;
    }
}
*/
//void beautiful_oracle(){
TEST(beautiful_oracle){
    std::vector<std::string> vec_sql;
    //read_sql_from_file(vec_sql);

    vec_sql.push_back("update GUEST.CUSTOMER3 set GUEST.CUSTOMER3.c_mktsegment = 'test' where salary <1800 or c_privilege_level is NAN ");
    vec_sql.push_back("update GUEST.CUSTOMER3 set GUEST.CUSTOMER3.c_mktsegment = 'test' where salary is INFINITE or c_privilege_level is NULL");
    vec_sql.push_back("with a(customer_id,c_privilege_level,c_mktsegment) as ( select customer_id,c_privilege_level,c_mktsegment from guest.customer_mask ) search DEPTH FIRST BY customer_id DESC NULLS FIRST SET yyy select * from a");
    vec_sql.push_back("update GUEST.CUSTOMER3 set GUEST.CUSTOMER3.c_mktsegment = 'test' where REGEXP_LIKE(c_custkey, '[^j7d]a');");
    vec_sql.push_back("with a(customer_id,c_privilege_level,c_mktsegment) as ( select customer_id,c_privilege_level,c_mktsegment from guest.customer_mask ) CYCLE c_privilege_level set is_cycle TO 'Y' DEFAULT 'N' select * from a");
    vec_sql.push_back("select * from guest.customer1 order by customer_id desc OFFSET 8 ROWS ");
    vec_sql.push_back("select * from guest.customer1 order by customer_id asc NULLS LAST FETCH NEXT\n"
                      "10 ROWS WITH TIES");
    vec_sql.push_back("SELECT NQ'ASSSA', q'''', Nq''a'b'a'' from guest.customer1");//, q'''',
    vec_sql.push_back("SELECT NQ'ASSSA', q'''', Nq'{a''''a}', Q'(aaaaa)',q'[saa'aa'as]' from guest.customer1");
    vec_sql.push_back("select q'!name LIKE!', Q'{SELECT * FROM dual WHERE a = 'Smith';}', nq'? ?1234 ?', c_privilege_level, c_mktsegment from guest.customer1 where c_mktsegment <> q'\"name like '['\"' and c_mktsegment != q'<'So,' she said, 'It's finished.'>'");
    vec_sql.push_back("select q'!name LIKE '%DBMS_%%'!', Q'{SELECT * FROM dual WHERE a = 'Smith';}', nq'? ?1234 ?',  c_privilege_level, c_mktsegment from guest.customer1 where c_mktsegment <> q'\"name like '['\"' and c_mktsegment != q'<'So,' she said, 'It's finished.'>'");
    vec_sql.push_back("SELECT c_mktsegment as int,c_custkey FROM sa.mask_condition_case_4769 \n"
                      " order by customer_id");
    vec_sql.push_back("select a.c_custkey, ROW_NUMBER() over (order by c_mktsegment) rank1 from   GUEST.MASK_CONDITION_CASE_4776 a");
    vec_sql.push_back("select count(*) over (partition by o_custkey order by order_id rows between\n"
                      "CURRENT ROW and 1 following ),a.* from GUEST.ORDERS a");
    vec_sql.push_back("select SUM( ALL c_privilege_level) SUM from GUEST.CUSTOMER1");
    vec_sql.push_back("SELECT c_mktsegment,c_custkey,\n"
                      "       c_privilege_level,STDDEV(c_privilege_level)\n"
                      "                                over(\n"
                      "                                    partition by c_mktsegment,c_custkey\n"
                      "                                    order by c_privilege_level\n"
                      "                                    )\n"
                      "FROM GUEST.MASK_CONDITION_CASE_4778\n"
                      "group by c_mktsegment,c_privilege_level,c_custkey\n"
                      "having STDDEV(c_privilege_level) is null");
    vec_sql.push_back("SELECT * FROM (GUEST.CUSTOMER1 a right join GUEST.ORDERS b on a.c_custkey=\n"
                      "b.o_custkey) inner join GUEST.CUSTOMER2 c USING\n"
                      "(c_privilege_level,c_mktsegment)");
    vec_sql.push_back("with a(a_custkey,total) as (select o_custkey,count(o_custkey) from GUEST.ORDERS\n"
                      "group by o_custkey order by o_custkey,count(o_custkey)) select * from a");


    vec_sql.push_back("SELECT q'!name LIKE '%DBMS_%%'!' q, \n"
                      "\t\tQ'{SELECT * FROM dual WHERE a = 'Smith';}' qq, \n"
                      "\t\tnq'$ Ÿ1234 $' nq, \n"
                      "\t\tc_mktsegment, \n"
                      "\t\tsum(c_privilege_level) sum \n"
                      "\t\t\tFROM GUEST.CUSTOMER_MASK group by grouping sets ((c_mktsegment, c_privilege_level), ()) \n"
                      "\t\t\t\thaving c_mktsegment <> q'\"name like '['\"' \n"
                      "\t\t\t\tand c_mktsegment != q'<'So,' she said, 'It's finished.'>'");
    vec_sql.push_back("SELECT q'!name LIKE '%DBMS_%%'!' q, Q'{SELECT * FROM dual WHERE a = 'Smith';}' qq, nq'$ Ÿ1234 $' nq, c_mktsegment, sum(c_privilege_level) sum FROM GUEST.CUSTOMER_MASK group by grouping sets ((c_mktsegment, c_privilege_level), ()) having c_mktsegment <> q'\"name like '['\"' and c_mktsegment != q'<'So,' she said, 'It's finished.'>'");

   // vec_sql.push_back("SELECT q'!name LIKE '%DBMS_%%'!' q, Q'{SELECT * FROM dual WHERE a = 'Smith';}' qq, nq'$ Ÿ1234 $' nq, c_mktsegment, sum(c_privilege_level) sum FROM GUEST.CUSTOMER_MASK group by grouping sets ((c_mktsegment, c_privilege_level), ()) having c_mktsegment <> q'\"name like '['\"' and c_mktsegment != q'<'So,' she said, 'It's finished.'>'");
//   vec_sql.push_back("SELECT q'!name LIKE '%DBMS_%%'!' q,\n"
//                     " Q'{SELECT * FROM dual WHERE a = 'Smith';}' qq,\n"
//                     " nq'$ Ÿ1234 $' nq,\n"
//                     " CASE WHEN GUEST.CUSTOMER_MASK.C_PRIVILEGE_LEVEL = 1 THEN TO_NCHAR('**test#@/') ELSE c_mktsegment END AS c_mktsegment , \n"
//                     " SUM(c_privilege_level) SUM \n"
//                     " FROM GUEST.CUSTOMER_MASK \n"
//                     " WHERE ((GUEST.CUSTOMER_MASK.c_privilege_level >= 0 OR GUEST.CUSTOMER_MASK.c_privilege_level IS null) AND \n"
//                     " (GUEST.CUSTOMER_MASK.c_privilege_level < 10 OR GUEST.CUSTOMER_MASK.c_privilege_level IS null) ) \n"
//                     " GROUP BY GROUPING SETS((c_mktsegment, c_privilege_level), ()) HAVING c_mktsegment <> q'\"name like '['\"' AND c_mktsegment <> q'<'So,' she said, 'It's finished.'>'   ");
//   vec_sql.push_back("SELECT \n"
//                     " CASE WHEN GUEST.CUSTOMER_MASK.C_PRIVILEGE_LEVEL = 1 THEN TO_NCHAR('**test#@/') ELSE c_mktsegment END AS c_mktsegment , \n"
//                     " SUM(c_privilege_level) SUM \n"
//                     " FROM GUEST.CUSTOMER_MASK ");
//   vec_sql.push_back("SELECT * FROM (\n"
//                     "SELECT\n"
//                     "\"c1\",\"c2\",\"c4\",\"c5\"\n"
//                     "FROM \n"
//                     "a"
//                     " \"t0\"\n"
//                     "GROUP BY \"c1\",\"c2\",\"c4\",\"c5\",\"o2\"\n"
//                     "ORDER BY \"c5\"\n"
//                     "ASC\n"
//                     ",\"c2\"\n"
//                     "ASC\n"
//                     ",\n"
//                     "COALESCE(\"o2\", TO_TIMESTAMP('1899-12-30 00:00:00', 'yyyy-mm-dd HH24:mi:ss'))\n"
//                     "ASC\n"
//                     ", (CASE WHEN \"o2\" IS NULL THEN 0 ELSE 1 END)\n"
//                     "ASC\n"
//                     ",\"c1\"\n"
//                     "ASC\n"
//                     " ) WHERE ROWNUM <= 501");
    vec_sql.push_back("select c_privilege_level from GUEST.CUSTOMER1 where c_privilege_level NOT IN ( (select c_privilege_level from GUEST.CUSTOMER1 where customer_id = 1),(select c_privilege_level from GUEST.CUSTOMER1 where customer_id = 1) )");
    vec_sql.push_back("select c_privilege_level, c_mktsegment\n"
                      "from GUEST.CUSTOMER1\n"
                      "where c_privilege_level >= -1\n"
                      "group by c_privilege_level, c_mktsegment\n"
                      "having not ((select c_privilege_level from GUEST.CUSTOMER1 where customer_id = 1) = 1 )\n"
                      "and c_mktsegment is not null\n"
                      "order by c_privilege_level, c_mktsegment");
   for(size_t i = 0 ;i<vec_sql.size();i++){
        std::string out;
        if(vec_sql[i].length() < 10){
            continue;
        }
        parser::beautify_sqloracle(vec_sql[i],out);
        if(!out.empty()){
            printf("%s \n",out.c_str());
        } else {
            printf("error-index : %ld \n",i);
        }
    }
}

TEST(beautiful_sqlserver){
    std::vector<std::string> vec_sql;
//    vec_sql.push_back("SELECT c_mktsegment as int,c_custkey FROM sa.mask_condition_case_4769 as\n"
//                      "sample order by customer_id");
//    vec_sql.push_back("select SUM(DISTINCT c_privilege_level) SUM from sa.CUSTOMER");
//    vec_sql.push_back("select  'ss' char,'ss' nchar,'ss' CHARACTER,'ss' numeric,'ss' decimal,'ss' real,'ss' bigint,'ss' integer,   'ss' FLOAT, 'ss' date from jeff.test");
//
    vec_sql.push_back("select \n"
                      "    [$Table].[account_id] as [account_id],\n"
                      "    [$Table].[account_name] as [account_name],\n"
                      "    [$Table].[salary] as [salary],\n"
                      "    [$Table].[bank_account] as [bank_account],\n"
                      "    [$Table].[bank_name] as [bank_name]\n"
                      "from [dbo].[salary] as [$Table]");
    for(size_t i = 0 ;i<vec_sql.size();i++) {
        std::string out;
        if(vec_sql[i].length() < 10){
            continue;
        }
        parser::beautify_sql2003(vec_sql[i],out);
        if(!out.empty()){
            printf("%s \n",out.c_str());
        } else {
            printf("error-index : %ld \n",i);
        }
    }
}

TEST(beautiful_hana){
//void beautiful_hana(){
    std::vector<std::string> vec_sql;
//    vec_sql.push_back("select * from guest.customer3 where cast(salary as int)<2000");
//    vec_sql.push_back("SELECT c_mktsegment, sum(c_privilege_level) sum FROM GUEST.CUSTOMER3 group by\n"
//                      "grouping sets(c_mktsegment, ())");
//    vec_sql.push_back("select arr_date from guest.array_mask1 where '2000-01-01' member of\n"
//                      "hxe.guest.array_mask1.arr_date or INT_C member of array(2,3);");
//    vec_sql.push_back("select salary, c_privilege_level, c_mktsegment, sum(c_privilege_level)\n"
//                      "sum_level  from guest.customer3 group by cube LIMIT 6 ((salary,\n"
//                      "c_privilege_level), c_mktsegment) ORDER BY salary");
//
//    vec_sql.push_back("select c_privilege_level, MAX(salary) max_salary from guest.customer3 group by\n"
//                      "cube TEXT_FILTER '1' ((c_privilege_level), (salary)) ORDER BY salary");
//
//    vec_sql.push_back("select A.c_mktsegment, avg(c_privilege_level) avg_level from guest.customer1 a\n"
//                      "group by rollup TEXT_FILTER 'Education \"FMCG\"' ((A.c_mktsegment),\n"
//                      "(A.c_mktsegment)) ORDER BY A.c_mktsegment");
//
//    vec_sql.push_back("update GUEST.CUSTOMER1 set c_privilege_level=5 where C_MKTSEGMENT like_regexpr 'building|FMCG'");
//    vec_sql.push_back("SELECT TRIM (LEADING 'a' FROM 'aaa123456789aa') \"trim leading\" FROM DUMMY;");
//    vec_sql.push_back("SELECT * FROM mySchema.SEARCH_TEXT WHERE CONTAINS (( Content, Descrip ), 'vintage');");
//    vec_sql.push_back("SELECT * FROM MySchema.SEARCH_TEXT WHERE CONTAINS (*, 'take', LINGUISTIC);");
//    vec_sql.push_back("SELECT SCORE() AS SCORE,*  \n"
//                      "   FROM mySchema.SEARCH_TEXT  \n"
//                      "   WHERE CONTAINS(CONTENT,'cap',FUZZY(0.9))  \n"
//                      "   ORDER BY SCORE DESC");
//    vec_sql.push_back("DELETE FROM GUEST.PARTITIONED PARTITION(2) \n"
//                      "WHERE  customer_id > 5 ");
//    vec_sql.push_back("delete from GUEST.CUSTOMER1  where C_MKTSEGMENT like_regexpr 'building|FMCG' FLAG 'i'");
//    vec_sql.push_back("call aaaaaaaaaaaaa");
//    vec_sql.push_back("call aaaaaaaaaaaaa()");
//    vec_sql.push_back("call aaaaaaaaaaaaa(100)");
//    vec_sql.push_back("SELECT c_mktsegment, sum(c_privilege_level) sum \n"
//                      "FROM GUEST.customer_mask\n"
//                      "group by grouping sets(c_mktsegment, ())");
//    vec_sql.push_back("select a.*, trim(c_mktsegment) from guest.customer1 a");
//    vec_sql.push_back("select c_mktsegment, c_privilege_level, trim(LEADING 'E' from a.c_mktsegment) trim_mkt from guest.customer1 a where trim(a.c_mktsegment) = a.c_mktsegment order by customer_id asc");
//    vec_sql.push_back("SELECT c_mktsegment, c_privilege_level, NTILE(3) OVER (PARTITION BY c_mktsegment ORDER BY c_privilege_level) AS NTILE, FIRST_VALUE(c_privilege_level) OVER (PARTITION BY c_mktsegment ORDER BY c_privilege_level) AS first, LAST_VALUE(c_privilege_level) OVER (PARTITION BY c_mktsegment ORDER BY c_privilege_level) AS last, NTH_VALUE(c_privilege_level, 1) OVER (PARTITION BY c_mktsegment ORDER BY c_privilege_level) AS nth FROM guest.customer1\n"
//                      "order by c_mktsegment, c_privilege_level");
//    vec_sql.push_back("SELECT c_privilege_level, c_mktsegment, VAR( all \"C_PRIVILEGE_LEVEL\") OVER (PARTITION BY c_mktsegment ORDER BY c_privilege_level ASC nulls first) as CUSTOMER_ID_VAR FROM guest.\"CUSTOMER1\";");
//    vec_sql.push_back("select c_privilege_level, c_mktsegment ,first_value(c_mktsegment order by customer_id ASC NULLS FIRST) first_mkt from guest.customer_mask group by c_privilege_level, c_mktsegment");
//
//    vec_sql.push_back("select c_privilege_level, first_value(c_mktsegment order by customer_id ASC  NULLS FIRST) first_mkt from guest.customer1 group by c_privilege_level");


//    vec_sql.push_back("SELECT customer_id, c_privilege_level, c_custkey, LOCATE_REGEXPR(START\n"
//                      "    '[^j7d]a' FLAG 'i' in c_custkey from 1 OCCURRENCE 1 GROUP 3 ) \"locate_regexpr\"\n"
//                      "    FROM guest.customer1;");
//    vec_sql.push_back("SELECT customer_id, c_privilege_level, c_custkey, LOCATE_REGEXPR(AFTER\n"
//                      "    '[^j7d]a' FLAG 'm' in c_custkey from 1 OCCURRENCE 1 GROUP 3 ) \"locate_regexpr\"\n"
//                      "    FROM guest.customer1;");
//    vec_sql.push_back("Select b.customer_id, c_mktsegment, c_privilege_level, c_custkey,\n"
//                      "            OCCURRENCES_REGEXPR( 'a' FLAG 's' IN b.c_custkey FROM 1)\n"
//                      "    OCCURRENCES_REGEXPR_CUSTOMER, OCCURRENCES_REGEXPR('([[:digit:]])' IN 'a1b2')\n"
//                      "    \"occurrences_regexpr\" from guest.\"CUSTOMER1\" as b;");
//    vec_sql.push_back("select b.customer_id, c_mktsegment, c_privilege_level, c_custkey,\n"
//                      "            REPLACE_REGEXPR( 'a' FLAG 'x' IN b.c_custkey WITH 'A' FROM 1 OCCURRENCE 1)\n"
//                      "    REPLACE_REGEXPR_CUSTOMER, REPLACE_REGEXPR('([[:digit:]])' IN 'a1b2' WITH 'A'\n"
//                      "    FROM 1 OCCURRENCE ALL) \"replace_regexpr\" from guest.\"CUSTOMER1\" as b;");
//    vec_sql.push_back("select b.customer_id, c_mktsegment, c_privilege_level, c_custkey,\n"
//                      "            SUBSTRING_REGEXPR( 'a' IN b.c_custkey) ING_key, SUBSTR_REGEXPR( 'a' IN\n"
//                      "    b.c_custkey) from guest.\"CUSTOMER_MASK\" as b");
//    vec_sql.push_back("(select customer_id, c_privilege_level, c_mktsegment from\n"
//                      "guest.CUSTOMER_MASK ORDER BY customer_id)ORDER BY customer_id LIMIT 14 offset 2 for update");
    vec_sql.push_back("select * from a for update");


    for(size_t i = 0 ;i<vec_sql.size();i++) {
        std::string out;
        if(vec_sql[i].length() < 10){
            continue;
        }
        parser::beautify_sqlhana(vec_sql[i],out);
        if(!out.empty()){
            printf("%s \n",out.c_str());
        } else {
            printf("error-index : %ld \n",i);
        }
    }
}



void read_sql_from_file(std::vector<std::string> & vec_sql){
    std::ifstream t("/home/jeff/tempsource/db2_sql.sql");
    std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    int i = 0;
    int size = str.length();
    std::string sql;
    while(i < size){
        if(str[i] == '\n'){
            if(sql.length()>10){
                vec_sql.push_back(sql);
            }
            sql.clear();
        } else {
            sql+=str[i];
        }
        i++;
    }
}



TEST(DB2){

    std::vector<std::string>  vec_sql;
    read_sql_from_file(vec_sql);
//    vec_sql.push_back( "select customer_id,c_mktsegment,\n"
//                       "   CASE when c_privilege_level=1\n"
//                       "   then customer_id\n"
//                       "   else c_privilege_level\n"
//                       "   end\n"
//                       "   as case\n"
//                       "from Autotest.guest.CUSTOMER1\n"
//                       "order by customer_id");
    vec_sql.push_back(" Select customer_id,c_mktsegment, c_privilege_level from guest.customer1\n"
"LIMIT 12 offset 1\n");

    vec_sql.push_back(" Select customer_id,c_mktsegment, c_privilege_level from guest.customer1 where datc_c > date('2020-01-01' ) and time_C !=timestamp('2021-01-01 12:20:20')");

    vec_sql.push_back("(Select customer_id,c_mktsegment, c_privilege_level from guest.customer1\n"
"LIMIT 12 offset 1)");
    vec_sql.push_back("Select customer_id,c_mktsegment, c_privilege_level from guest.customer1 where (GUEST.DATATYPE2.int_c > 1 AND GUEST.DATATYPE2.decimal_c < 4 AND\n"
                      "GUEST.DATATYPE2.time_c <= TIME('12:12:12'))");
    vec_sql.push_back("select *\n"
                      "from guest.customer1 a\n"
                      "cross join guest.ORDERS CHAR\n"
                      "where a.c_custkey=CHAR.o_custkey\n"
                      "and CHAR.o_orderpriority = 1\n"
                      "ORDER BY a.CUSTOMER_ID,CHAR.ORDER_ID");
    for(size_t i = 0 ; i < vec_sql.size(); ++i){
       // std::string src = "SELECT customer_id, c_privilege_level FROM jdbc-enforcer";
       // printf("ori:%s\n", src.c_str());
        std::string out;
        bool B = parser::compress_sqldb2( vec_sql[i],  out);
        if(B){
            //printf("not support:%d\n", i);
            printf("new:%s\n", out.c_str());
        } else {
            printf("new:%ld\n", i);
        }

    }

}



/*
#include "policymgr_export.h"
TEST(POLICYMGR) {
    std::string policysyncinterval="60";
//    jpchost=https://92cc.nextlabs.solutions
//    jpcport=443
//    jpcuser=apiclient2
//    jpcpwd=12345Blue!

    std::string cchost="https://92cc.nextlabs.solutions";
    std::string ccport="443";
    std::string ccuser="Administrator";
    std::string ccpwd="123next!";
    std::string policytag="EMDB";

    bool b = PolicyInit(cchost.c_str(),
                        ccport.c_str(),
                        ccuser.c_str(),
                        ccpwd.c_str(),
                        policytag.c_str(),
                        atoi(policysyncinterval.c_str()),
                        &Log);


//    std::string str = CommonFun::GetConfigFilePath();
//    class CConfig cfg; cfg.ReadConfig(str.c_str());
//    auto theConfig = &cfg;
//    b = PolicyInit((*theConfig)[CFG_POLICY_CCHOST].c_str(),
//                        (*theConfig)[CFG_POLICY_CCPORT].c_str(),
//                        (*theConfig)[CFG_POLICY_CCUSER].c_str(),
//                        (*theConfig)[CFG_POLICY_CCPWD].c_str(),
//                        (*theConfig)[CFG_POLICY_TAG].c_str(),
//                        atoi((*theConfig)[CFG_POLICY_SYNCINTERVAL].c_str()),
//                        &Log);


    ASSERT_TRUE(b);
    sleep(3);
}


*/

#include <EMMaskOpr.h>
TEST(aaa){
    EMDB_DB_TYPE dbtype = EMDB_DB_DB2;
    EMDataType type = EMDATA_DATE;
    std::string symbols ="1002-3-21";
    bool bret = check_date_time_format( dbtype,  type,symbols);
    ASSERT_TRUE(bret);
}
TEST(aaa1){
    EMDB_DB_TYPE dbtype = EMDB_DB_DB2;
    EMDataType type = EMDATA_TIMESTAMP;
    std::string symbols ="1002-3-21 10:22:23.222";
    bool bret = check_date_time_format( dbtype,  type,symbols);
    ASSERT_TRUE(bret);
}
TEST(aaa2){
    EMDB_DB_TYPE dbtype = EMDB_DB_DB2;
    EMDataType type = EMDATA_TIME;
    std::string symbols ="12:32:22a";
    bool bret = check_date_time_format( dbtype,  type,symbols);
    ASSERT_FALSE(bret);
}
TEST(aaa3){
    EMDB_DB_TYPE dbtype = EMDB_DB_DB2;
    EMDataType type = EMDATA_DATE;
    std::string symbols ="2111a-11-11";
    bool bret = check_date_time_format( dbtype,  type,symbols);
    ASSERT_FALSE(bret);
}


TEST_MAIN()