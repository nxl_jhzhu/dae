#ifndef CPOLICY_RESOURCE_H
#define CPOLICY_RESOURCE_H

#include "IPolicyResource.h"
#include <map>
#include <string>





class CPolicyResource : public IPolicyResource
{
    public:
    CPolicyResource();
    ~CPolicyResource();
    public:
    void AddedAttribute(const char* szName, const char* szValue, CEAttributeType attrType=XACML_string);
    bool ValidAttribute(const char* szName, const char* szValue, const char* szOperator, CEAttributeType attrType=XACML_string);
    void CopyDictionary(EMResDictionary & dic);
    void CopyDictionary2(EMResDictionary2 & dic);

protected:
    const ResourceAttrValue* FindAttribute(const char* szName);

    private:
    EMResDictionary m_dicAttrValue;

};

#endif