#ifndef ENFORCERMGR_H
#define ENFORCERMGR_H

#include <string>
#include <map>
#include <shared_mutex>
#include "Interface.h" //interface of sqlparse module
#include "sqlenforcer_export.h"
#include "SqlException.h"

class UserContextInfo;
//struct SqlException;
struct ConditionInfo;

struct SQLParseCtx
{
    UserContextInfo *pUserCtxInfo=NULL;
    bool bError=false;
    std::string strErrorDetail;
 //   unsigned int blocked_stmt_cnt=0;
//    std::string strBlockDetail;
//    INode * _node=NULL;
//    std::string _old_sql;

    SqlException _exc;

    bool _sql_enforcer = false;
    unsigned int _filter_deny_times = 0;
    unsigned int _deny_times = 0;
    unsigned int _mask_times = 0;
    unsigned int _filter_times = 0;
};


class CEnforcerMgr
{
  public:
  static CEnforcerMgr* Instance()
  {
      static CEnforcerMgr* pInstance = new CEnforcerMgr();
      return pInstance;
  }

  protected:
  CEnforcerMgr();
  CEnforcerMgr(const CEnforcerMgr&){}

  public:
  bool Init(const char* wszModuleName,const char* szCfgFile, SqlException * exc);
  USER_CONTEXT NewContext(const char* szUserName, const char* szPwd, SqlException * pExc);
  void FreeContext(USER_CONTEXT);
  void SetUserContextInfo(USER_CONTEXT ctx, USER_CONTEXT_INFO_TYPE infoType, const char *szInfo);
  std::wstring EvaluationSQL(USER_CONTEXT context, const wchar_t* sqltext, SqlException * pExc);
  std::string  EvaluationSQL(USER_CONTEXT context, const char* sqltext, SqlException * pExc);
  void SetMetadataFunc(USER_CONTEXT ctx, QueryMetadataFunc func);

  public:
  bool Init_SDK(const char* wszModuleName, SqlException * pExc);
  USER_CONTEXT NewContext_SDK( SqlException * pExc);
  void AddUserAttrValue_SDK(USER_CONTEXT ctx, const char* key,const char* value, SqlException * pExc);
  void ClearUserContext_SDK(USER_CONTEXT ctx);
  bool FreeUserContext_SDK(USER_CONTEXT ctx);

  
  private:
  //call back for sqlparser
  void StmtVisit(IPlan* plan, IStmt* tbi);
  void ColumnVisit(IPlan* plan, IColumnsRefItem* cli);
  void StartNewStmt(IPlan* plan, uint64_t lastStatID);
  void WhereCluase(IPlan* plan, IWhereCluase* wc);
  void ErrorOccur(IPlan* plan);

//  void ColumnVisitOnSelect(IPlan* plan, IColumnsRefItem* cli, IStmt* stmt, SQLParseCtx* pParseCtx,
//                           IPolicyResource* pResourceAttr);
// std::string WhereCluaseOnSelect(IPlan* plan, IWhereCluase* wc, IStmt* stmt, SQLParseCtx* pParseCtx, 
//                           IPolicyResource* pResourceAttr);
// std::string WhereCluaseOnDelete(IPlan* plan, IWhereCluase* wc, IStmt* stmt, SQLParseCtx* pParseCtx, 
//                           IPolicyResource* pResourceAttr);
// std::string WhereCluaseOnUpdate(IPlan* plan, IWhereCluase* wc, IStmt* stmt, SQLParseCtx* pParseCtx, 
//                           IPolicyResource* pResourceAttr);

// std::string DoFilterForTableItem(ITableItem* tbi,const std::string& strPolicyAction, 
//                                   UserContextInfo* pUserCtx, IPolicyResource* pResourceAttr, MaskConditionMap *  map_mask = NULL);

  //
  UserContextInfo* GetUserContextInfo(USER_CONTEXT ctx);
  std::string GetPolicyActionByStmtType(StmtType stmtType);
public:
  void GetLocalHostInfo(std::string & host_name, std::string & host_ip);

  private:
  std::string m_strModuleName;
  SqlException _exc;
  //use for query pc
    std::string _hostname;
    std::string _host_ip;
};


#endif 