#ifndef EMDBEXPORT_H
#define EMDBEXPORT_H
#include "stdint.h"

#include <stdio.h>
#include <string>

#ifndef WIN32
#define EMDB_SDK_API __attribute__((visibility("default")))
#else
#ifdef EMDB_SDK_EXPORT
#define EMDB_SDK_API  __declspec(dllexport)
#else 
#define EMDB_SDK_API  __declspec(dllimport)
#endif //EMDB_SDK_EXPORT
#endif //WIN32

typedef void* EMDBResultHandle;
typedef void* EMDBUserCtxHandle;

enum EMDBReturn{ EMDB_SUCCESS, EMDB_ERROR /* todo */, EMDB_INVALID_HANDLE };

enum EMDBResultCode{ EMDB_BLOCK_THIS_TEXT, EMDB_USE_NEW_TEXT, EMDB_DENY_EXECUTE };

extern "C"{

    EMDB_SDK_API EMDBReturn EMDBInit(const char *module_name);

    EMDB_SDK_API EMDBReturn EMDBNewUserCtx(const char *server_name, const char *database_name, const char *schema_name, const char *dbtype, EMDBUserCtxHandle *output_user_ctx);

    EMDB_SDK_API EMDBReturn EMDBFreeUserCtx(EMDBUserCtxHandle user_ctx);

    EMDB_SDK_API EMDBReturn EMDBSetUserCtxProperty(EMDBUserCtxHandle user_ctx, const char *key, const char *value);

    EMDB_SDK_API EMDBReturn EMDBClearUserCtxProperty(EMDBUserCtxHandle user_ctx);

    EMDB_SDK_API EMDBReturn EMDBEvalSql(const char *sql, EMDBUserCtxHandle user_ctx, EMDBResultHandle result);

    EMDB_SDK_API EMDBReturn EMDBNewResult(EMDBResultHandle *output_result);

    EMDB_SDK_API EMDBReturn EMDBFreeResult(EMDBResultHandle result);

    EMDB_SDK_API EMDBReturn EMDBResultGetCode(EMDBResultHandle result, EMDBResultCode *output_code);

    EMDB_SDK_API EMDBReturn EMDBResultGetDetail(EMDBResultHandle result, const char **output_detail);





}





#include "EMMaskDef.h"

extern "C" {

    EMDB_SDK_API EMDBReturn EMDBQueryMetadata_cb(EMDBUserCtxHandle user_ctx, QueryMetadataFunc func);
}






#endif // !EMDBEXPORT_H



