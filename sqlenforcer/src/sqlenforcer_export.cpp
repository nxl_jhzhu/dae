#include "sqlenforcer_export.h"
#include "enforcermgr.h"
#include "log.h"



ENFORCER_API bool SQLEnforcerInit(const char* szCallerModuleName,const char* szConfigFile, SqlException * pExc)
{
    bool bInit = CEnforcerMgr::Instance()->Init(szCallerModuleName, szConfigFile, pExc);
    return bInit;
}

ENFORCER_API USER_CONTEXT SQLEnforcerNewContext(const char* szUserName, const char* szPwd, SqlException * pExc)
{
    USER_CONTEXT context = CEnforcerMgr::Instance()->NewContext(szUserName, szPwd, pExc);
    return context;
}


ENFORCER_API void SQLEnforcerFreeContext(USER_CONTEXT context)
{
    CEnforcerMgr::Instance()->FreeContext(context);
}

void SetUserContextInfo(USER_CONTEXT ctx, USER_CONTEXT_INFO_TYPE infoType, const char *szInfo)
{
    CEnforcerMgr::Instance()->SetUserContextInfo(ctx, infoType, szInfo);

}

ENFORCER_API std::wstring SQLEnforcerEvaluationSQLW(USER_CONTEXT context, const wchar_t* sqltext , SqlException * pExc)
{
     return CEnforcerMgr::Instance()->EvaluationSQL(context, sqltext, pExc);
}

ENFORCER_API std::string SQLEnforcerEvaluationSQLA(USER_CONTEXT context, const char* sqltext, SqlException * pExc)
{
    return CEnforcerMgr::Instance()->EvaluationSQL(context, sqltext, pExc);
}

ENFORCER_API int SQLEnforcerWriteLogA(int level, const char* msg)
{
   return theLog->WriteLog(level, "%s", msg);
}

