#include "enforcermgr.h"
#include "sqlparserwrapper.h"
#include "policymgrwrapper.h"
#include "userattrwrapper.h"
#include "log.h"
#include "EMDBConfig.h"
#include "commfun.h"
#include "PolicyResource.h"
#include "usercontextinfo.h"
#include "ODBCMgrApi.h"
#include "assert.h"
#include "QueryCAZPolicyOpr.h"
#include "QueryCloudAZSDKCppWrapper.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_TABLE 10


#include "SqlException.h"
#include "sql_collect.h"
#include <chrono>

#include "sqlenforcer_create.h"
using namespace DAE;

using namespace std::chrono;
#define MAGIC_NUMBER UINT64_MAX

ISqlCollect *theSqlCollector = nullptr;

void ComposePolicyResource(IPolicyResource *pResourceAttr, /*const*/ UserContextInfo *pUserCtx,/*const*/  ITableItem* tbi);
void ComposePolicyResource(IPolicyResource *pResourceAttr, /*const*/ UserContextInfo *pUserCtx,/*const*/  IStoreProcedure* pProcedure);
void query_pc_from_stmt(std::vector<QueryPcResult*> & vecresult, const std::string & action, std::vector<ITableItem*> & tbis, UserContextInfo* user_ctx, IPolicyResource* resources, SqlException * pExc);
void get_from_tables(std::vector<ITableItem*> & tbis, IStmt* stmt);
void get_action_tables(std::vector<ITableItem*> & tbis, IStmt* stmt);

CEnforcerMgr::CEnforcerMgr()
{
}

bool CEnforcerMgr::Init(const char *szModuleName, const char *szCfgFile, SqlException * pExc)
{
    m_strModuleName = szModuleName;

    //log init
    CLog::Instance()->InitLog(m_strModuleName);
    theLog->WriteLog(log_info, "SQLEnforcer::Init");

    //config file
    theLog->WriteLog(log_info, "Read config from:%s", szCfgFile);

    try
    {
        if (CommonFun::CaseInsensitiveEquals(szModuleName, "jdbc"))
        {
            EMDBConfig::Initialize(ConnectionType::kJdbc);
        }
        else if (CommonFun::CaseInsensitiveEquals(szModuleName, "odbc"))
        {
            EMDBConfig::Initialize(ConnectionType::kOdbc);
        }

        auto emdb_config = EMDBConfig::GetInstance();
        auto initial_errors = emdb_config.get_initial_errors();

        for (const auto& error : initial_errors)
        {
            theLog->WriteLog(log_error, error.c_str());
        }
    }
    catch (const std::exception& e)
    {
        pExc->SetInfo(ERR_CONFIG, e.what());
        return false;
    }

    // Set default log level to warning
    theLog->UpdateLogLevel("3");

    auto emdb_config = EMDBConfig::GetInstance();

    try
    {
        auto log_level = std::to_string(static_cast<int>(emdb_config.get_log_level())); // TODO: Change into enum
        //update log level
        theLog->UpdateLogLevel(log_level);
    }
    catch (const std::exception& e)
    {
        theLog->WriteLog(log_error, "Failed to set log level. Use the default log level.");
    }

//    //init ODBCAPI /*todo for jdbc*/
    if(m_strModuleName.compare("jdbc") != 0) {
        if(!ODBCMgrApi::Instance().m_bInit) {
            pExc->SetInfo(ERR_LIBRARY, detail_err_lib_odbc);
            theLog->WriteLog(log_fatal, "Load ODBCAPI failed.");
            return false;
        }
    }

    //load sqlparser
    if (!CSqlparserWrapper::Instance()->LoadParser())
    {
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_sql2003);
        theLog->WriteLog(log_fatal, "Load sql parser failed.");
        return false;
    }
    theLog->WriteLog(log_info, "LoadParser succeed.");

    //load userattribute module
    if (!UserAttrWrapper::Instance()->LoadUserAttributeModule())
    {
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_userattribute);
        theLog->WriteLog(log_fatal, "Load user attribute module failed.");
        return false;
    }
    theLog->WriteLog(log_info, "LoadUserAttribute Module succeed.");

    const int user_mode = static_cast<int>(emdb_config.get_usermode_mode());
    auto odbc_conn_string = emdb_config.get_usermode_odbc_conn_string();
    auto user_info = emdb_config.get_usermode_user_info();
    auto val_separator = emdb_config.get_usermode_multi_value_separator();
    auto sync_time = emdb_config.get_usermode_sync_time();

    UserAttrWrapper::Instance()->UserAttrInit(SQLEnforcerWriteLogA,
                                              user_mode,
                                              odbc_conn_string.c_str(),
                                              user_info.c_str(),
                                              m_strModuleName.c_str(),
                                              val_separator.c_str(),
                                              (int)sync_time);
    theLog->WriteLog(log_info, "UserAttrInit succeed.");

    //load policy manager
    /*
    if (!PolicyMgrWrapper::Instance()->LoadPolicyMgr())
    {
        theLog->WriteLog(log_fatal, "Load policy manager failed.");
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_policymgr);
        return false;
    }
    theLog->WriteLog(log_info, "LoadPolicyMgr succeed.");
    bool bRet =  PolicyMgrWrapper::Instance()->PolicyInit((*theConfig)[CFG_POLICY_CCHOST].c_str(),
                                             (*theConfig)[CFG_POLICY_CCPORT].c_str(),
                                             (*theConfig)[CFG_POLICY_CCUSER].c_str(),
                                             (*theConfig)[CFG_POLICY_CCPWD].c_str(),
                                             (*theConfig)[CFG_POLICY_TAG].c_str(),
                                             atoi((*theConfig)[CFG_POLICY_SYNCINTERVAL].c_str()),
                                             SQLEnforcerWriteLogA);
    if(!bRet){
        pExc->SetInfo(ERR_POLICY, detail_err_ploicyinit);
    }
     */
    if (!QueryCloudAZSDKCppWrapper::Instance()->LoadQueryCloudAZSDKCpp())
    {
        theLog->WriteLog(log_fatal, "Load query policy failed.");
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_policymgr);
        return false;
    }
    if (!QueryCloudAZSDKCppWrapper::Instance()->QueryCloudAZInit(
            emdb_config.get_policy_jpchost().c_str(),
            emdb_config.get_policy_jpcport().c_str(),
            emdb_config.get_policy_cchost().c_str(),
            emdb_config.get_policy_ccport().c_str(),
            emdb_config.get_policy_jpcuser().c_str(),
            emdb_config.get_policy_jpcpwd().c_str(),
            1,
            [](int, const char*) { return 0; }))
    {
        pExc->SetInfo(ERR_POLICY, "QueryCloudAZInit failed.");
        _exc.SetInfo(ERR_POLICY, "QueryCloudAZInit failed.");
        theLog->WriteLog(log_error, "policy arguments is incorrect in config.ini.");
        //return false;
    } else {
        theLog->WriteLog(log_info, "PolicyInit succeed.");
    }


    theSqlCollector =  theSqlCollector ? theSqlCollector : make_sql_collector(pExc->code!=ERR_POLICY);
    if(theSqlCollector)
        theLog->WriteLog(log_info, "make_sql_collector succeed.");
    if(get_host(&_hostname, &_host_ip)){
        theLog->WriteLog(log_info, "get hostname and ip succeed.(%s:%s)",_hostname.c_str(),_host_ip.c_str());
    }

    return true;
}

bool CEnforcerMgr::Init_SDK(const char* szModuleName, SqlException * pExc){

    std::string sPath = CommonFun::GetConfigFilePath();

    m_strModuleName = szModuleName;

    //log init
    CLog::Instance()->InitLog("emdb_api");
    theLog->WriteLog(log_info, "SQLEnforcer::Init");

    //config file
    theLog->WriteLog(log_info, "Read config from:%s", sPath.c_str());

    try
    {
        auto emdb_config = EMDBConfig::GetInstance();
        auto initial_errors = emdb_config.get_initial_errors();

        for (const auto& error : initial_errors)
        {
            theLog->WriteLog(log_error, error.c_str());
        }
    }
    catch (const std::exception& e)
    {
        pExc->SetInfo(ERR_CONFIG, e.what());
        return false;
    }

    // Set default log level to warning
    theLog->UpdateLogLevel("3");

    auto emdb_config = EMDBConfig::GetInstance();

    try
    {
        auto log_level = std::to_string(static_cast<int>(emdb_config.get_log_level())); // TODO: Change into enum
        //update log level
        theLog->UpdateLogLevel(log_level);
    }
    catch (const std::exception& e)
    {
        theLog->WriteLog(log_error, "Failed to set log level. Use the default log level.");
    }

    //    //init ODBCAPI /*todo for jdbc*/
    if(m_strModuleName.compare("jdbc") != 0) {
        if(!ODBCMgrApi::Instance().m_bInit) {
            pExc->SetInfo(ERR_LIBRARY, detail_err_lib_odbc);
            theLog->WriteLog(log_fatal, "Load ODBCAPI failed.");
            return false;
        }
    }

    //load sqlparser
    if (!CSqlparserWrapper::Instance()->LoadParser())
    {
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_sql2003);
        theLog->WriteLog(log_fatal, "Load sql parser failed.");
        return false;
    }

    //load userattribute module
    if (!UserAttrWrapper::Instance()->LoadUserAttributeModule())
    {
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_userattribute);
        theLog->WriteLog(log_fatal, "Load user attribute module failed.");
        return false;
    }
    UserAttrWrapper::Instance()->UserAttrInit_SDK(SQLEnforcerWriteLogA);//log format

    /*
    //load policy manager
    if (!PolicyMgrWrapper::Instance()->LoadPolicyMgr())
    {
        theLog->WriteLog(log_fatal, "Load policy manager failed.");
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_policymgr);
        return false;
    }
    bool bRet =  PolicyMgrWrapper::Instance()->PolicyInit((*theConfig)[CFG_POLICY_CCHOST].c_str(),
                                             (*theConfig)[CFG_POLICY_CCPORT].c_str(),
                                             (*theConfig)[CFG_POLICY_CCUSER].c_str(),
                                             (*theConfig)[CFG_POLICY_CCPWD].c_str(),
                                             (*theConfig)[CFG_POLICY_TAG].c_str(),
                                             atoi((*theConfig)[CFG_POLICY_SYNCINTERVAL].c_str()),
                                             SQLEnforcerWriteLogA);
    if(!bRet){
        pExc->SetInfo(ERR_POLICY, detail_err_ploicyinit);
    }
     */
    if (!QueryCloudAZSDKCppWrapper::Instance()->LoadQueryCloudAZSDKCpp())
    {
        theLog->WriteLog(log_fatal, "Load query policy failed.");
        pExc->SetInfo(ERR_LIBRARY, detail_err_lib_policymgr);
        return false;
    }
    if (!QueryCloudAZSDKCppWrapper::Instance()->QueryCloudAZInit(
            emdb_config.get_policy_jpchost().c_str(),
            emdb_config.get_policy_jpcport().c_str(),
            emdb_config.get_policy_cchost().c_str(),
            emdb_config.get_policy_ccport().c_str(),
            emdb_config.get_policy_jpcuser().c_str(),
            emdb_config.get_policy_jpcpwd().c_str(),
            8,
            [](int, const char*) { return 0; }))
    {
        pExc->SetInfo(ERR_POLICY, "QueryCloudAZInit failed.");
        _exc.SetInfo(ERR_POLICY, "QueryCloudAZInit failed.");
        theLog->WriteLog(log_error, "policy arguments is incorrect in config.ini.");
        //return false;
    } else {
        theLog->WriteLog(log_info, "PolicyInit succeed.");
    }
    
    theSqlCollector =  theSqlCollector ? theSqlCollector : make_sql_collector(pExc->code!=ERR_POLICY);
    if(theSqlCollector)
        theLog->WriteLog(log_info, "make_sql_collector succeed.");
    if(get_host(&_hostname, &_host_ip)){
        theLog->WriteLog(log_info, "get hostname and ip succeed.");
    }

    return true;
}


USER_CONTEXT CEnforcerMgr::NewContext(const char *szUserName, const char *szPwd, SqlException * pExc)
{
    UserContextInfo *userCtxInfo = new UserContextInfo(szUserName, szPwd);
    userCtxInfo->InitUserAttr(pExc);
//    if(pExc->code == ERR_USERINFO){
//        _exc = *pExc;
//    }

    USER_CONTEXT userCtx = (USER_CONTEXT)userCtxInfo;

    return userCtx;
}

void CEnforcerMgr::FreeContext(USER_CONTEXT ctx) {
    UserContextInfo *userCtxInfo = GetUserContextInfo(ctx);
    delete (userCtxInfo);
}

USER_CONTEXT CEnforcerMgr::NewContext_SDK( SqlException * pExc){
    if(UserAttrWrapper::Instance()->GetUserAttr_SDK == NULL) {
        return 0;
    }
    std::string sName = "null";
    std::string sPWD = "null";
    std::string sep = EMDBConfig::GetInstance().get_usermode_multi_value_separator();
    std::string usr_mode = std::to_string(static_cast<unsigned>(EMDBConfig::GetInstance().get_usermode_mode())); // TODO: Use enum Usermode
    std::string user_info = EMDBConfig::GetInstance().get_usermode_user_info();
    UserContextInfo *userCtxInfo = new UserContextInfo(sName.c_str(), sPWD.c_str());
    userCtxInfo->m_pUserAttr = UserAttrWrapper::Instance()->GetUserAttr_SDK(sep.c_str(), usr_mode.c_str(), user_info.c_str());

    USER_CONTEXT userCtx = (USER_CONTEXT)userCtxInfo;
    return userCtx;
	
}

void CEnforcerMgr::AddUserAttrValue_SDK(USER_CONTEXT ctx, const char* key,const char* value, SqlException * pExc){
    if(!ctx){
        return;
    }
    UserContextInfo *pInfo  = (UserContextInfo*)ctx;
    if(pInfo->m_pUserAttr == nullptr && UserAttrWrapper::Instance()->GetUserAttr_SDK){
        std::string sep = EMDBConfig::GetInstance().get_usermode_multi_value_separator();
        std::string usr_mode = std::to_string(static_cast<unsigned>(EMDBConfig::GetInstance().get_usermode_mode())); // TODO: Use enum Usermode
        std::string user_info = EMDBConfig::GetInstance().get_usermode_user_info();

        pInfo->m_pUserAttr = UserAttrWrapper::Instance()->GetUserAttr_SDK(sep.c_str(),usr_mode.c_str(), user_info.c_str());
    }
    if(UserAttrWrapper::Instance()->AddUserAttrValue_SDK) {
        UserAttrWrapper::Instance()->AddUserAttrValue_SDK(pInfo->m_pUserAttr, key, value);
    }
}

void CEnforcerMgr::ClearUserContext_SDK(USER_CONTEXT ctx){
    if(!ctx){
        return;
    }
    UserContextInfo *pInfo  = (UserContextInfo*)ctx;
   
    if(pInfo->m_pUserAttr){
        delete pInfo->m_pUserAttr;
        pInfo->m_pUserAttr = nullptr;
    }
}

bool CEnforcerMgr::FreeUserContext_SDK(USER_CONTEXT ctx){
    if(ctx){
        UserContextInfo *pInfo  = (UserContextInfo*)ctx;
        if(pInfo->m_pUserAttr){
            delete pInfo->m_pUserAttr;
            pInfo->m_pUserAttr = nullptr;
        }
        delete pInfo;
        ctx = 0;
        return true;
    }
    
    return false;
}

void CEnforcerMgr::SetUserContextInfo(USER_CONTEXT ctx, USER_CONTEXT_INFO_TYPE infoType, const char *szInfo)
{
    UserContextInfo *userCtx = GetUserContextInfo(ctx);
    if (NULL != userCtx)
    {
        switch (infoType)
        {
        case CONTEXT_INFO_SERVER:
            userCtx->SetDBServer(szInfo);
            break;

        case CONTEXT_INFO_DATABASE:
            userCtx->SetCurrentDB(szInfo);
            break;

        case CONTEXT_INFO_SCHEMA:
            userCtx->SetSchema(szInfo);
            break;
        case CONTEXT_INFO_DB_TYPE: {
                //EMDB_DB_TYPE dbType = EMDB_DB_UNKNOW;
                std::string strDriverName(szInfo);
                EMDB_DB_TYPE dbType = get_dbtype_by_drivername(strDriverName);
                userCtx->SetEMDBType(dbType);
            } break;
        }
    }
}

UserContextInfo *CEnforcerMgr::GetUserContextInfo(USER_CONTEXT ctx)
{
    UserContextInfo *p_user = (UserContextInfo*)ctx;
    return (p_user && p_user->m_magicNumber == HANDLE_USERCONTEXTINFO) ?  p_user : nullptr;
}

std::wstring CEnforcerMgr::EvaluationSQL(USER_CONTEXT context, const wchar_t *sqltext, SqlException * pExc)
{
    std::string u8_cvt_str;
    CommonFun::ToUTF8(sqltext, u8_cvt_str);
    if(sqltext && u8_cvt_str.length() == 0){
        printf("error wsql: %ls\n", sqltext);
    }
    std::string strNewSQL = EvaluationSQL(context, u8_cvt_str.c_str(), pExc);

    std::wstring u16_cvt_str ;
    //convert result
    CommonFun::FromUTF8(strNewSQL, u16_cvt_str);
    return u16_cvt_str;
}

std::string CEnforcerMgr::EvaluationSQL(USER_CONTEXT context, const char *sqltext, SqlException * pExc)
{
   // out_is_blocked = false;
    CSqlparserWrapper *pSqlparser = CSqlparserWrapper::Instance();

    EnforcerResultCode code = EnforcerResultCode::EF_USE_NEW_TEXT;
    std::string old_sql(sqltext);
    //theLog->WriteLog(log_info, "ori_sql:%s", sqltext);

    //get usercontext info
    UserContextInfo *pUserCtxInfo = GetUserContextInfo(context);
    if (!pUserCtxInfo)
    {
        EnforcerResultData data;
        data.bParseSucceed = false;
        data.retCode = code;
        data.oriSql = old_sql;
        data.newSql = old_sql;
        data.username = "";
        theLog->WriteLog(log_error, "CEnforcerMgr::EvaluationSQL failed because we can't get user context info[%s] by context[%i]."
                                    "And current sql is \"%s\" ", pUserCtxInfo, context, sqltext);
        pExc->SetInfo(ERR_PARSE, detail_err_parse2);
        
        theSqlCollector->collect_enforcer_result(data);
        return sqltext;
    }

//    {
//        IUserAttr* puser = pUserCtxInfo->UserAttribute();
//        if(puser){
//            theLog->WriteLogFunc(log_info, std::bind(&IUserAttr::Print, puser));
//        }
//    }

     if (!pSqlparser->IsWell())
    {
        theLog->WriteLog(log_error, "CEnforcerMgr::EvaluationSQL failed because sql parser module didn't well");
        pExc->SetInfo(ERR_PARSE, detail_err_parse1);
         
        EnforcerResultData data;
        data.bParseSucceed = false;
        data.oriSql = old_sql;
        data.newSql = old_sql;
        data.retCode = code;
        data.username = pUserCtxInfo->GetUsername();
        theSqlCollector->collect_enforcer_result(data);

        return sqltext;
    }

    bool b;
    {
        //std::string out_sql = "";
        b = pSqlparser->BeautifySql(sqltext, old_sql, pUserCtxInfo->GetEMDBType());
        if (b) {
            //theLog->WriteLog(log_info, "ori_sql:\n%s\n", out_sql.c_str());
        #ifdef _DEBUG
            printf("%s\n", old_sql.c_str());
        #endif
        }  
        else {
            //theLog->WriteLog(log_info, "ori_sql:\n%s\n", sqltext);
            old_sql = sqltext;
        #ifdef _DEBUG
            printf("%s\n", sqltext);
        #endif
        }
        
    }

    std::string new_sql = old_sql;//use to cllection log
    std::string strNewSql = sqltext;
    IParseResult *parserResult = pSqlparser->ParseSql(strNewSql, pUserCtxInfo->GetEMDBType());
    CrtDrpTableInfo tbinfo;
    if (parserResult->IsAccept())
    {
        //parse context
        SQLParseCtx parseCtx;
        parseCtx.pUserCtxInfo = pUserCtxInfo;
        parseCtx.bError = false;

        //get parse result
        INode *tree = parserResult->GetParseTree();

        //tree->Print();

        if (tree)
        {
            IPlan *plan = pSqlparser->CreatePlan(std::bind(&CEnforcerMgr::StmtVisit, this, std::placeholders::_1, std::placeholders::_2),
                                                 std::bind(&CEnforcerMgr::ColumnVisit, this, std::placeholders::_1, std::placeholders::_2),
                                                 std::bind(&CEnforcerMgr::StartNewStmt, this, std::placeholders::_1, std::placeholders::_2),
                                                 std::bind(&CEnforcerMgr::WhereCluase, this, std::placeholders::_1, std::placeholders::_2),
                                                 std::bind(&CEnforcerMgr::ErrorOccur, this, std::placeholders::_1),
                                                 &parseCtx,
                                                 parserResult);

            plan->SetDefaultDatabase(pUserCtxInfo->GetCurrentDB().c_str());
            plan->SetDefaultSchema(pUserCtxInfo->GetSchema().c_str());

            pSqlparser->VisitPlan(plan);
            strNewSql = tree->Serialize();

            //tree->Print();
   //         theLog->WriteLog(log_info, "new_sql:%s", strNewSql.c_str());
            
            /* record user's protected sql script */
            {
                
                b = pSqlparser->BeautifySql(strNewSql, new_sql, pUserCtxInfo->GetEMDBType());
               // theLog->WriteLog(log_info, "beautiful_new_sql:%s", new_sql.c_str());
                if (b) {
                    //theLog->WriteLog(log_info, "new sql:\n%s", out_sql.c_str() );
                #ifdef _DEBUG
                    printf("new sql:\n%s\n", new_sql.c_str());
                #endif
                }  
                else {
                    //theLog->WriteLog(log_info, "new sql:\n%s", strNewSql.c_str() );
                #ifdef _DEBUG
                    printf("new sql:\n%s\n", strNewSql.c_str());
                #endif
                }
            }

            pSqlparser->DestroyPlan(plan);
            plan = NULL;
            *pExc = parseCtx._exc;
            if(parseCtx._deny_times > 0){
                code = EnforcerResultCode::EF_BLOCK_THIS_TEXT;
                pExc->SetInfo(ERR_DENY, detail_err_evaluation);
            } else if (parseCtx._sql_enforcer ){
                if(parseCtx._filter_deny_times <= 0){
                    pExc->SetInfo(ERR_NULL, "");
                } else {
                    if(pExc->code != ERR_DENY_SELECT) {
                        pExc->SetInfo(ERR_DENY_SELECT, ""); //ERR_DENY_SELECT Priority than E_ALLOW
                    }
                }
            }
        }
        
    }
#ifdef WINDOWS_APP_DEMO
    else if(is_create_or_drop_stmt(old_sql, tbinfo)){// todo test for windows excel
        bool bdeny = deny_create_or_drop(tbinfo, pUserCtxInfo, pExc);
        if(bdeny){
            code = EnforcerResultCode::EF_BLOCK_THIS_TEXT;
            pExc->SetInfo(ERR_DENY, detail_err_evaluation);
        }else{
            pExc->SetInfo(ERR_NULL, "");
        }
    }
#endif
    else
    {
        char err[1024];
        sprintf(err, "sql script syntax error at(%d,%d): %s",(int)parserResult->GetErrorLine(), (int)parserResult->GetErrorColumn(), parserResult->GetErrorDetail().c_str());
        theLog->WriteLog(log_info, "sql script syntax error at(%d,%d): %s\n", 
            (int)parserResult->GetErrorLine(), (int)parserResult->GetErrorColumn(), parserResult->GetErrorDetail().c_str());

    #ifdef _DEBUG
            printf("sql script syntax error at(%d,%d): %s\n", 
            (int)parserResult->GetErrorLine(), (int)parserResult->GetErrorColumn(), parserResult->GetErrorDetail().c_str());
    #endif
        pExc->SetInfo(ERR_PARSE, err);
    }

    pSqlparser->DestroyParseResult(parserResult);
    parserResult = NULL;
    //theSqlCollector->collect_sql(old_sql.c_str(),new_sql.c_str(), b);
    {
        EnforcerResultData data;
        data.bParseSucceed = b;
        data.oriSql = old_sql;
        data.newSql = new_sql;
        data.retCode = code;
        data.username = pUserCtxInfo->GetUsername();
        theSqlCollector->collect_enforcer_result(data);
        theSqlCollector->collect_sql(old_sql, new_sql);
    }
    return strNewSql;
}

bool check_policy_and_user_deny(SqlException & exc, SQLParseCtx* pParseCtx){
    if(exc.IsDeny(false)){
        return true;
    }
    bool except_deny = true;
    auto exception_handler = EMDBConfig::GetInstance().get_emdb_error_exception_handler();

    if(exception_handler == HandlerType::kAllow){
        except_deny = false;
    }

    if(except_deny){
        if(exc.code != ERR_POLICY) {
            UserContextInfo* pUserCtx = pParseCtx->pUserCtxInfo;
            IUserAttr * puser = pUserCtx->UserAttribute();
            if(puser){
                if(puser->GetAttributeCount() == 0){
                    theLog->WriteLog(log_info, "deny all SQL, becase user attr is empty, and EMDB_ERROR.exception_handler is deny.");
                    return true;
                }
            }
        } else {
            theLog->WriteLog(log_info, "deny all SQL, becase Policy init failed, and EMDB_ERROR.exception_handler is deny.");
            return true;
        }

    }

    return false;
}

void CEnforcerMgr::SetMetadataFunc(USER_CONTEXT ctx, QueryMetadataFunc func){
    UserContextInfo* pinfo = GetUserContextInfo(ctx);
    if(pinfo && func){
        pinfo->_meta_func = func;
    }
}

void CEnforcerMgr::StmtVisit(IPlan *plan, IStmt *stmt)
{
    assert(stmt);
    //get parse context
    SQLParseCtx *parser_ctx = (SQLParseCtx *)(plan->GetContext());
    //std::string sql_old = parser_ctx->_node->Serialize();
    //UserContextInfo *pUserCtx = pParseCtx->pUserCtxInfo;

    //collect resource(table) attribute
    CPolicyResource cResource;
    IPolicyResource *resource = dynamic_cast<IPolicyResource *>(&cResource);

    StmtType stmt_type = stmt->GetStmtType();
    std::string action = GetPolicyActionByStmtType(stmt_type);
    std::vector<QueryPcResult*> results;

    SqlException exc = _exc;//policy init failed.
    UserContextInfo* puser_ctx = parser_ctx->pUserCtxInfo;
    bool deny = check_policy_and_user_deny(exc, parser_ctx);

    if(!deny){
        std::vector<ITableItem*>  tbis;
        get_action_tables(tbis, stmt);

        query_pc_from_stmt(results, action, tbis, puser_ctx, resource, &exc);
        tbis.clear();
        if( stmt_type == StmtType::E_STMT_TYPE_UPDATE && stmt->GetFromItemCount() != 0) {
            get_from_tables(tbis, stmt);
            std::string action = GetPolicyActionByStmtType(E_STMT_TYPE_SELECT);
            query_pc_from_stmt(results, action, tbis, puser_ctx, resource, &exc);
        }
    }
    if(!deny){
        deny = check_policy_and_user_deny(exc, parser_ctx); // err_policy
    }
    std::string strCond ;
    if(!deny) {
        for (auto itrs:results){
            if(!itrs) continue;

            auto table_id = itrs->GetTableId();
            // Reset join predicate when the table, which should be enforced, is in joined tables.
            if (auto join_table = stmt->GetJoinedTable(table_id))
            {
                std::string cond = dynamic_cast<EnforcerPcResult*>(itrs)->_filter_cond;

                if (itrs->IsDeny()) {
                  cond = "1 = 0";
                }

                if (join_table->ResetJoinPredicate(table_id, cond)) {
                  continue;
                }
            }

            if(itrs->IsDeny()){
                exc.SetInfo(ERR_DENY_SELECT,detail_err_evaluation);
                deny = true;
                break;
            } else {
                std::string cond = dynamic_cast<EnforcerPcResult*>(itrs)->_filter_cond;
                if(cond.empty()){
                    continue;
                }
                if(strCond.empty()){
                    strCond += "(";
                    strCond += cond;
                    strCond += ")";
                } else {
                    strCond += " AND (";
                    strCond += cond;
                    strCond += ") ";
                }
            }
        }
    }

    switch (stmt_type)
    {
        case E_STMT_TYPE_SELECT:{
            if(!deny){
                for (auto itrs:results){
                    if(!itrs) continue;
                    EnforcerPcResult * pitrs = dynamic_cast<EnforcerPcResult*>(itrs);
                    if(dynamic_cast<ISelectStmt*>(stmt)->GetSelectExpr() && pitrs->_mask_map.size()>0){
                        //set metadata to ITableItem
                        std::vector<ITableItem*>  tbis;
                        get_action_tables(tbis, stmt);
                        for(auto & ittb:tbis){
//                            if(pitrs->_mask_map.find(ittb->GetTableName()) != pitrs->_mask_map.end() ){ // need expland all column
//                            }
                            if(E_BASIC_TABLE ==  ittb->GetTableItemType() || E_BASIC_TABLE_WITH_ALIAS == ittb->GetTableItemType()){
                                const MetadataVec* pmetadata =  puser_ctx->GetTableMetadata(ittb->GetTableName());
                                if(pmetadata)
                                    ittb->SetMetadata(*pmetadata);
                            }
                        }
                        //mask opr
                        dynamic_cast<ISelectStmt*>(stmt)->GetSelectExpr()->MaskOpr(&pitrs->_mask_map,exc);
                        if(exc.code == ERR_MASK_OPR  && EMDBConfig::GetInstance().get_emdb_error_star_handler() == HandlerType::kDeny){
                            deny = true;
                            break;
                        }
                        parser_ctx->_mask_times++;
                    }
                }
            }
            if(deny){
                strCond = " 1 = 0 ";
                parser_ctx->_filter_deny_times++;
            }
            if(!strCond.empty() && dynamic_cast<ISelectStmt*>(stmt)->GetWhereClause()){
                dynamic_cast<ISelectStmt*>(stmt)->GetWhereClause()->AddCondition(strCond, exc);
                parser_ctx->_filter_times++;
                parser_ctx->_sql_enforcer = true;
            }
        }break;
        case E_STMT_TYPE_UPDATE:{
            if(deny){
                parser_ctx->_deny_times++;
                break;
            }
            if(!strCond.empty() && dynamic_cast<IUpdateStmt*>(stmt)->GetWhereClause()){
                dynamic_cast<IUpdateStmt*>(stmt)->GetWhereClause()->AddCondition(strCond, exc);
                parser_ctx->_filter_times++;
                parser_ctx->_sql_enforcer = true;
            }
        }break;
        case E_STMT_TYPE_DELETE:{
            if(deny){
                parser_ctx->_deny_times++;
                break;
            }
            if(!strCond.empty() && dynamic_cast<IDeleteStmt*>(stmt)->GetWhereClause()){
                dynamic_cast<IDeleteStmt*>(stmt)->GetWhereClause()->AddCondition(strCond, exc);
                parser_ctx->_filter_times++;
                parser_ctx->_sql_enforcer = true;
            }
        }   break;

        case E_STMT_TYPE_INSERT:{
            if(deny){
                parser_ctx->_deny_times++;
                break;
            }
        }break;
        default:
            break;
    }

    //audit log to jpc
    if(parser_ctx->_deny_times <= 0 &&
        parser_ctx->_filter_times > 0 &&
        EMDBConfig::GetInstance().get_log_switch_report() &&
        results.size() > 0){

        for(auto it:results){
            EMAuditlogInfo info;
            info._pc_info = dynamic_cast<EnforcerPcResult*>(it)->_auditlog;
            theSqlCollector->collect_enforcer_result(info);
        }
    }

    free_pc_result(results);
    if(exc.code != ERR_NULL){
        parser_ctx->_exc = exc;
    }

    // deny update while select table masked
    if(stmt_type == E_STMT_TYPE_UPDATE) {
        do{
            std::vector<ITableItem*>  tbis; // this is stmt member ref
            get_action_tables(tbis, stmt);
            assert(tbis.size() == 1) ;  //  update table size need == 1
            std::string table = tbis[0]->GetTableName();
            results.clear();
            std::string action_ = GetPolicyActionByStmtType(E_STMT_TYPE_SELECT);
            query_pc_from_stmt(results, action_, tbis, puser_ctx, resource, &exc);
            IUpdateStmt * stmt_update = dynamic_cast<IUpdateStmt*>(stmt);
            const std::vector<IUpdateItem*>& upits = stmt_update->get_update_items();
            MaskItemMap * mask_map = NULL;
            for (auto itrs:results){
                auto it = dynamic_cast<EnforcerPcResult*>(itrs)->_mask_map.find(table);
                if(it!=  dynamic_cast<EnforcerPcResult*>(itrs)->_mask_map.end()){
                    mask_map = it->second;//
                }
            }
            if(mask_map == NULL || mask_map->size() == 0){
                break;
            }

            UserContextInfo* pUserCtx = parser_ctx->pUserCtxInfo;
            ITableItem * tbi = stmt_update->GetUpdateTable();
            std::string qualify_ = tbi->GetTableAliasName();
            if (qualify_.empty())
            {
                qualify_ = tbi->GetTableObject();
            }
            qualify_ = get_field_qualify( pUserCtx->GetEMDBType(), qualify_);

            for(auto it : upits){
                auto itf = mask_map->find(it->GetFieldName());
                if(itf != mask_map->end()) {
                    it->DenyUpdate(itf->second->_condition, qualify_, itf->second->_datatype, pUserCtx->GetEMDBType());
                    parser_ctx->_sql_enforcer = true;
                    parser_ctx->_filter_times++;
                }
            }
        } while(false);
        free_pc_result(results);
        results.clear();
    }
    
}

std::string get_datatype_name(const EMDataType & datatype)
{
  std::string ret;

  switch (datatype)
  {
    case EMDATA_STRING:
    {
      ret = "STRING";
    } break;
    case EMDATA_NUMBER:
    {
      ret = "NUMBER";
    } break;
    case EMDATA_DATE:
    {
      ret = "DATE";
    } break;
    case EMDATA_TIME:
    {
      ret = "TIME";
    } break;
    case EMDATA_TIMESTAMP:
    {
      ret = "TIMESTAMP";
    } break;
    case EMDATA_STRING2:
    {
      ret = "STRING2";
    } break;
    case EMDATA_TIMESTAMP_TZ_ORA:
    {
      ret = "TIMESTAMP_TZ_ORA";
    } break;
    case EMDATA_YMINTERVAL:
    {
      ret = "YMINTERVAL";
    } break;
    case EMDATA_DSINTERVAL:
    {
      ret = "DSINTERVAL";
    } break;
    case EMDATA_NONSUPPORT:
    {
      ret = "NONSUPPORT";
    } break;
    default:
    {
      // do nothing
    } break;
  }

  return ret;
}

std::string print_mask(const MaskConditionMap & map_){
    std::string ret = "MASK: ";
    for(auto ittb:map_ ){
        ret += "{ \n";
        for(auto it:*ittb.second){
            ret += ittb.first;
            ret += '.';
            ret += it.first;
            ret += ":[ ";
            ret += "\n    COND =";
            ret += it.second->_condition;
            ret += "\n    TYPE =";
            ret += get_datatype_name(it.second->_datatype);
            ret += "\n    FORMAT =";
            if(it.second->_format==0) ret+= "FullMask";
            else if(it.second->_format==1) ret+= "RandNumber";
            else if(it.second->_format==2) ret+= "PartialMask";
            else if(it.second->_format==3) ret+= "KeyMask";
            ret += "\n    SYMBOL =";
            ret += it.second->_symbols;
            ret += "  ],\n";
        }
        ret += "},\n";
    }
    return ret;
}

void get_from_tables(std::vector<ITableItem*> & tbis, IStmt* stmt){
    size_t cnt = stmt->GetTableItemCount();
    //std::vector<ITableItem*> tbis;
    for (size_t i = 0; i < cnt; ++i)
    {
        ITableItem *tbi = stmt->GetTableItem(i);
        if(!tbi)
            continue;
        if (tbi->GetTableItemType() != E_BASIC_TABLE && tbi->GetTableItemType() != E_BASIC_TABLE_WITH_ALIAS)
            continue;
        tbis.push_back(tbi);
    }
}

void get_action_tables(std::vector<ITableItem*> & tbis, IStmt* stmt){
    StmtType stmt_type = stmt->GetStmtType();
    switch(stmt_type){
        case E_STMT_TYPE_UPDATE:
        {
            ITableItem* tbi = dynamic_cast<IUpdateStmt*>(stmt)->GetUpdateTable();
            if(!tbi)
                return;
            if (tbi->GetTableItemType() != E_BASIC_TABLE && tbi->GetTableItemType() != E_BASIC_TABLE_WITH_ALIAS)
                return;

            tbis.push_back(tbi);
        } break;
        case E_STMT_TYPE_DELETE:
        {
            ITableItem* tbi = dynamic_cast<IDeleteStmt*>(stmt)->GetDeleteTable();
            if(!tbi)
                return;
            if (tbi->GetTableItemType() != E_BASIC_TABLE && tbi->GetTableItemType() != E_BASIC_TABLE_WITH_ALIAS)
                return;
            tbis.push_back(tbi);
        } break;
        case E_STMT_TYPE_INSERT:
        {
            ITableItem* tbi = dynamic_cast<IInsertStmt*>(stmt)->GetInsertTable();
            if(!tbi)
                return;
            if (tbi->GetTableItemType() != E_BASIC_TABLE && tbi->GetTableItemType() != E_BASIC_TABLE_WITH_ALIAS)
                return;
            tbis.push_back(tbi);
        } break;
        default:{
            get_from_tables( tbis,stmt);
        } break;
    }
}

void query_pc_from_stmt(std::vector<QueryPcResult*> & vecresult, const std::string & action, std::vector<ITableItem*> & tbis, UserContextInfo* user_ctx, IPolicyResource* resources, SqlException * pExc){

    size_t cnt_real = tbis.size();
    if(cnt_real <= 0 || cnt_real > 10) return; /*todo  quantity limit*/
    //IPolicyRequest * reqs[MAX_TABLE] = {NULL};
    const IPolicyRequest ** reqs = (const IPolicyRequest**)calloc(cnt_real,sizeof(IPolicyRequest*));
    std::vector<ConditionInfo> conds;
    std::vector<RequestInfo> log_reqs;

    std::string  user = user_ctx->GetUsername();
    for (size_t i = 0; i < cnt_real; ++i)
    {
        ITableItem *tbi = tbis[i];
        RequestInfo reqinfo;
        reqinfo._action = action;

        //set schema,table name
        ComposePolicyResource(resources, user_ctx, tbi);
        //resources->CopyDictionary(reqinfo._rescs);
        resources->CopyDictionary2(reqinfo._rescs2);

        IUserAttr * attr = user_ctx->UserAttribute();
        //attr->CopyDictionary(reqinfo._users);
        attr->CopyDictionary2(reqinfo._users2);
        if(user.empty() || user.compare("null") == 0){
            auto it = reqinfo._users2.find("id");
            if(it != reqinfo._users2.end()){
                user = it->second.get()->strValue;
            } else {
                user = "user";
            }
        }
        reqs[i] = create_request(reqinfo, user);
        log_reqs.push_back(reqinfo);

        std::string qualify_ = tbi->GetTableAliasName();
        if (qualify_.empty())
        {
            qualify_ = tbi->GetTableObject();
        }
        conds.push_back(
                ConditionInfo(
                    tbi->get_table_id(),
                    tbi->GetTableName(),
                    qualify_,
                    action,
                    user_ctx->GetEMDBType(),
                    resources,
                    user_ctx->UserAttribute(),
                    tbi->GetUsingColumns(),
                    tbi->GetJoinType()
                )
        );
    }


    if(cnt_real == 1) {
        IPolicyResult* result = NULL;

        QueryStatus  status = single_query_pc(reqs[0], &result);
        if(TEST_LOG){
            int ob = 0;
            if(result){
                ob = result->ObligationCount();
            }
            theLog->WriteLog(log_info, "query result(status=%d,obligation=%d)",status,ob);
        }
/*
        auto st = system_clock::now();
        auto ed = system_clock::now();
        auto duration = duration_cast<microseconds>(ed-st);
        double  timed = double (duration.count())*microseconds::period::num / microseconds::period::den;
        theLog->WriteLog(log_error, "query_single_time:%lf",timed);
*/
        free_pc_request(const_cast<IPolicyRequest*>(reqs[0]));
        free(reqs);

        if (QS_S_OK != status ) {
            pExc->SetInfo(ERR_POLICY, "single_query_pc failed.");
            theLog->WriteLog(log_error, "single_query_pc failed");
            return;
        }
        SqlException e;
        QueryPcResult * tmp = parser_result_array(result, conds[0], e);
        EnforcerPcResult * pc = dynamic_cast<EnforcerPcResult*>(tmp);
        if(e.code != ERR_NULL ){
            theLog->WriteLog(log_warning, "single_query_pc parser_result_array failed: %s", e.cdetail.c_str());
            //log
            free_pc_result(result);
            return;
        }
        if( pc ==NULL){
            pExc->SetInfo(ERR_POLICY_NOMATCH, "no policy matched.");
            free_pc_result(result);
            return;
        }

        if(CommonFun::StrCaseCmp( action.c_str(), ACTION_SELECT) == 0){
            std::string mask = print_mask(pc->_mask_map);
            //printf("%s\n",mask.c_str());
            theLog->WriteLog(log_info, "QUERY PC MASK:%s", mask.c_str());
            theLog->WriteLog(log_info, "QUERY PC FILTER:%s", pc->_filter_cond.c_str());

        }else if(CommonFun::StrCaseCmp( action.c_str(), ACTION_UPDATE) == 0 ||
                 CommonFun::StrCaseCmp( action.c_str(), ACTION_DELETE) == 0) {
            theLog->WriteLog(log_info, "QUERY PC FILTER:%s", pc->_filter_cond.c_str());
        }
        free_pc_result(result);

        pc->_auditlog = log_reqs[0];
        vecresult.push_back(pc);

    } else {
        IPolicyResult ** result = (IPolicyResult**)calloc(cnt_real,sizeof(IPolicyResult*));

        QueryStatus status= multi_query_pc(&reqs[0], (int)cnt_real, result);
/*
        auto st = system_clock::now();
        auto ed = system_clock::now();
        auto duration = duration_cast<microseconds>(ed-st);
        double  timed = double (duration.count())*microseconds::period::num / microseconds::period::den;
        theLog->WriteLog(log_error, "query_multi_time:%lf",timed);
*/
        for(size_t i = 0 ;i < cnt_real ;++i) {
            free_pc_request(const_cast<IPolicyRequest *>(reqs[i]));
        }
        free(reqs);

        if (QS_S_OK != status ) {
            pExc->SetInfo(ERR_POLICY, "multi_query_pc failed.");
            theLog->WriteLog(log_error, "multi_query_pc failed");
            free(result);
            return;
        }
        for (size_t i = 0; i < cnt_real; ++i)
        {
            SqlException e;
            QueryPcResult * tmp = parser_result_array(result[i], conds[i], e);
            EnforcerPcResult * pc = dynamic_cast<EnforcerPcResult*>(tmp);
            if(e.code != ERR_NULL){
                //log
                theLog->WriteLog(log_warning, "multi_query_pc parser_result_array failed: %s", e.cdetail.c_str());
                free_pc_result(result[i]);
                continue;
            }
            if(pc==NULL){
                free_pc_result(result[i]);
                continue;
            }
            if(CommonFun::StrCaseCmp( action.c_str(), ACTION_SELECT) == 0){
                std::string mask = print_mask(pc->_mask_map);
                theLog->WriteLog(log_info, "QUERY PC MASK:%s", mask.c_str());
                theLog->WriteLog(log_info, "QUERY PC FILTER:%s", pc->_filter_cond.c_str());

            }else if(CommonFun::StrCaseCmp( action.c_str(), ACTION_UPDATE) == 0 ||
                     CommonFun::StrCaseCmp( action.c_str(), ACTION_DELETE) == 0) {
                theLog->WriteLog(log_info, "QUERY PC FILTER:%s", pc->_filter_cond.c_str());
            }
            free_pc_result(result[i]);

            pc->_auditlog = log_reqs[i];
            vecresult.push_back(pc);
        }
        if(vecresult.size() == 0){
            pExc->SetInfo(ERR_POLICY_NOMATCH, "no policy matched.");
        }
        free(result);
    }
}

void CEnforcerMgr::ColumnVisit(IPlan *plan, IColumnsRefItem *cli)
{
    return;

}

void CEnforcerMgr::StartNewStmt(IPlan *plan,  uint64_t lastStatID)
{
    return;

}











void CEnforcerMgr::WhereCluase(IPlan *plan, IWhereCluase *wc)
{
    return;

}

void CEnforcerMgr::ErrorOccur(IPlan *plan)
{
    //get parse context
    SQLParseCtx *pParseCtx = (SQLParseCtx *)(plan->GetContext());
    pParseCtx->bError = true;
    pParseCtx->strErrorDetail = plan->GetErrorDetail();
    pParseCtx->_exc.SetInfo(ERR_PARSE,plan->GetErrorDetail());
    #ifdef _DEBUG
    printf("ErrorOccur:%s\n", plan->GetErrorDetail().c_str());
    #endif
}

std::string CEnforcerMgr::GetPolicyActionByStmtType(StmtType stmtType)
{
    switch (stmtType)
    {
    case E_STMT_TYPE_SELECT:
        return ACTION_SELECT;
    case E_STMT_TYPE_UPDATE:
        return ACTION_UPDATE;
    case E_STMT_TYPE_DELETE:
        return ACTION_DELETE;
    case E_STMT_TYPE_INSERT:
        return ACTION_INSERT;
    default:
        return ACTION_NONE;
    }
}



void ComposePolicyResource(IPolicyResource *pResourceAttr,  UserContextInfo *pUserCtx,  ITableItem* tbi){
    assert(pUserCtx && tbi && pResourceAttr);
    pResourceAttr->AddedAttribute(ATTR_NAME_SERVER, pUserCtx->GetDBServer().c_str());
    pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, pUserCtx->GetCurrentDB().c_str());
    EMDB_DB_TYPE type = pUserCtx->GetEMDBType();
    switch(type) {
        case EMDB_DB_DB2:
        case EMDB_DB_HANA:
        case EMDB_DB_BIGQUERY:
        case EMDB_DB_SQLSERVER: {
            pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, tbi->GetDatabaseName().c_str());
            pResourceAttr->AddedAttribute(ATTR_NAME_SCHEMA, tbi->GetSchemaName().c_str());
        } break; 
        case EMDB_DB_MYSQL: { 
            pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, tbi->GetSchemaName().c_str());
            pResourceAttr->AddedAttribute(ATTR_NAME_SCHEMA, tbi->GetSchemaName().c_str());
        } break; 
        case EMDB_DB_ORACLE: {
            pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, pUserCtx->GetCurrentDB().c_str());
            pResourceAttr->AddedAttribute(ATTR_NAME_SCHEMA, tbi->GetSchemaName().c_str());
        } break;
        default:{
            assert(false); /* unreachable */
        }
    }
    
    pResourceAttr->AddedAttribute(ATTR_NAME_TABLE, tbi->GetTableName().c_str());           
}

void ComposePolicyResource(IPolicyResource *pResourceAttr, /*const*/ UserContextInfo *pUserCtx,/*const*/  IStoreProcedure* pProcedure) {
    assert(pUserCtx && pProcedure && pResourceAttr);
    pResourceAttr->AddedAttribute(ATTR_NAME_SERVER, pUserCtx->GetDBServer().c_str());
    pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, pUserCtx->GetCurrentDB().c_str());
    EMDB_DB_TYPE type = pUserCtx->GetEMDBType();
    switch(type) {
        case EMDB_DB_DB2:
        case EMDB_DB_HANA:
        case EMDB_DB_BIGQUERY:
        case EMDB_DB_SQLSERVER: {
            pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, pProcedure->GetDatabaseName().c_str());
            pResourceAttr->AddedAttribute(ATTR_NAME_SCHEMA, pProcedure->GetSchemaName().c_str());
        } break; 
        case EMDB_DB_MYSQL: { 
            pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, pProcedure->GetSchemaName().c_str());
            pResourceAttr->AddedAttribute(ATTR_NAME_SCHEMA, pProcedure->GetSchemaName().c_str());
        } break; 
        case EMDB_DB_ORACLE: {
            pResourceAttr->AddedAttribute(ATTR_NAME_DATABASE, pUserCtx->GetCurrentDB().c_str());
            pResourceAttr->AddedAttribute(ATTR_NAME_SCHEMA, pProcedure->GetSchemaName().c_str());
        } break;
        default: {
            assert(false); /* unreachable */
        } break;
    }
    
    pResourceAttr->AddedAttribute(ATTR_NAME_PROCEDURE, pProcedure->GetStoreProcedureName().c_str()); 
}

void CEnforcerMgr::GetLocalHostInfo(std::string & host_name, std::string & host_ip){
    if(_hostname.empty()){
        host_name = "localhost";
    } else {
        host_name = _hostname;
    }
    if(_host_ip.empty()){
        host_ip = _host_ip;
    } else {
        host_ip = "255.255.255.255";
    }
}