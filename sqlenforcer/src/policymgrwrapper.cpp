// #include "policymgrwrapper.h"
// #include "commfun.h"
// #include "config.h"
// #include "log.h"

// #ifdef WIN32
// #include <windows.h>
// #else
// #include <dlfcn.h>
// #endif

// bool load_lib(const std::string& module_name) {
//     std::string module_path = CommonFun::GetSSLModule(module_name);
//     void *module = CommonFun::LoadShareLibrary(module_path.c_str());
//     if (module==NULL) {
//         theLog->WriteLog(log_fatal, LOAD_MODULE_FAILED, module_path.c_str(), CommonFun::ShareLibraryError().c_str());
//         return false;
//     }
//     return true;
// }



// bool PolicyMgrWrapper::LoadPolicyMgr()
// {
//     //get openssl
//     if (!load_lib("libeay32"))
//         return false;
//     if (!load_lib("ssleay32"))
//         return false;

//     //get sqlparser component path
//     std::string strInstallPath = (*theConfig)[CFG_INSTALL_PATH];
//     #ifdef WIN32
//     std::string strLibPath =  strInstallPath + "\\policymgr.dll";
//     #else
//      std::string strLibPath =  strInstallPath + "/libpolicymgr.so";
//     #endif


//     //load library
//     void* hModule = CommonFun::LoadShareLibrary(strLibPath.c_str());
//     if (hModule==NULL) {
//         #ifdef WIN32
//         theLog->WriteLog(log_fatal, "load policymgr.dll failed, dwlasterror=0x%x", GetLastError() );
//         #else 
//         theLog->WriteLog(log_fatal, "load libpolicymgr.so failed, dlerror=%s", dlerror()!=NULL ? dlerror() : "NULL" );
//         #endif 
//         return false;
//     }

//     //get function address
//     PolicyInit = (PolicyInitFun)CommonFun::GetProcAddress(hModule, "PolicyInit");
//     PolicyEvaluation = (PolicyEvaluationFun)CommonFun::GetProcAddress(hModule, "PolicyEvaluation");
//     FreeEvalResult = (FreeEvalResultFun)CommonFun::GetProcAddress(hModule, "FreeEvalResult");

//     m_bWell = (PolicyInit!=NULL) &&
//               (PolicyEvaluation!=NULL) &&
//               (FreeEvalResult!=NULL);

//     return m_bWell;
// }