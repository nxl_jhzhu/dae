#include "usercontextinfo.h"
#include "userattrwrapper.h"
#include "log.h"


UserContextInfo::UserContextInfo(const char* szUserName, const char* szPwd):
CEMDBHandle(HANDLE_USERCONTEXTINFO),
m_strUserName(szUserName),
m_strPwd(szPwd),
m_pUserAttr(nullptr)
{
   m_emdbtype  = EMDB_DB_TYPE::EMDB_DB_UNKNOW;
   //EncryptPwd();   
}

bool UserContextInfo::InitUserAttr(SqlException * pExc)
{
    if(UserAttrWrapper::Instance()->GetUserAttr){
        m_pUserAttr = UserAttrWrapper::Instance()->GetUserAttr(m_strUserName.c_str(), pExc);
    }

    return (nullptr==m_pUserAttr)?false:true;
}


void UserContextInfo::SetDBServer(const char* szServer)
{
   m_strDBServer = szServer;
   theLog->WriteLog(log_info, "set current db server for user:%s, server:%s", 
   m_strUserName.c_str(), m_strDBServer.c_str() );
}

void UserContextInfo::SetCurrentDB(const char* szDB)
{
   m_strCurrentDB = szDB;
   theLog->WriteLog(log_info, "set current db for user:%s, db:%s", 
   m_strUserName.c_str(), m_strCurrentDB.c_str() );
}

void UserContextInfo::SetSchema(const char* szSchema)
{
    m_strSchema = szSchema;
   theLog->WriteLog(log_info, "set current schema for user:%s, schema:%s", 
   m_strUserName.c_str(), m_strSchema.c_str() );
}

const MetadataVec *  UserContextInfo::GetTableMetadata(const std::string & tb) {
    auto it = _map_tb2matadata.find(tb);
    if(it != _map_tb2matadata.end()){
        return  it->second;//todo: expired
    } else {
        if(_meta_func){
            MetadataVec * ptr = new MetadataVec;
            _meta_func(tb, *ptr);
            {//---log
                std::string log = "table:"+tb+" metadata:";
                for(auto & it:*ptr){
                    log+=it._col; //std::to_string(it._type);
                    log+="  ";
                }
                theLog->WriteLog(log_info, log.c_str());
            }
            if( ptr->size() > 0 ){
                _map_tb2matadata[tb]=ptr;
                return ptr;
            } else {
                delete(ptr);
                return NULL;
            }
        } else {
            return NULL;
        }
    }
}
