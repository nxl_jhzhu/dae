/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Skeleton implementation for Bison GLR parsers in C

   Copyright (C) 2002-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C GLR parser skeleton written by Paul Hilfinger.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "glr.c"

/* Pure parsers.  */
#define YYPURE 0



/* Substitute the type names.  */
#define YYSTYPE HANA_STYPE
#define YYLTYPE HANA_LTYPE
/* Substitute the variable and function names.  */
#define yyparse hana_parse
#define yylex   hana_lex
#define yyerror hana_error
#define yydebug hana_debug

#define yylval  hana_lval
#define yychar  hana_char
#define yynerrs hana_nerrs
#define yylloc  hana_lloc

/* First part of user declarations.  */
#line 1 "sqlparser_hana.yacc" /* glr.c:240  */

/**
 * This Grammar is designed for hana.
 * https://github.com/Raphael2017/SQL/blob/master/sql-2003-2.bnf
 * sqlparser.y
 * defines sqlparser_hana_bison.h
 * outputs sqlparser_hana_bison.cpp
 *
 * Bison Grammar File Spec: http://dinosaur.compilertools.net/bison/bison_6.html
 *
 */
/*********************************
 ** Section 1: C Declarations
 *********************************/

#include "sqlparser_hana_bison.h"
#include "sqlparser_hana_flex.h"
#include "serialize_format.h"

#include <stdio.h>
#include <string.h>
//#include <strings.h>

/*
 * We provide parse error includes error message, first line, first column of error lex for debug
 */
int yyerror( ParseResult* result, yyscan_t scanner, const char *msg) {
    result->accept = false;
    //result->errFirstLine = llocp->first_line;
    //result->errFirstColumn = llocp->first_column;
    result->errDetail = msg;
	return 0;
}

#define YYSTYPE         HANA_STYPE
#define YYLTYPE         HANA_LTYPE


#line 104 "sqlparser_hana_bison.cpp" /* glr.c:240  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "sqlparser_hana_bison.h"

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* Default (constant) value used for initialization for null
   right-hand sides.  Unlike the standard yacc.c template, here we set
   the default value of $$ to a zeroed-out value.  Since the default
   value is undefined, this behavior is technically correct.  */
static YYSTYPE yyval_default;
static YYLTYPE yyloc_default
# if defined HANA_LTYPE_IS_TRIVIAL && HANA_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;

/* Copy the second part of user declarations.  */

#line 137 "sqlparser_hana_bison.cpp" /* glr.c:263  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YYFREE
# define YYFREE free
#endif
#ifndef YYMALLOC
# define YYMALLOC malloc
#endif
#ifndef YYREALLOC
# define YYREALLOC realloc
#endif

#define YYSIZEMAX ((size_t) -1)

#ifdef __cplusplus
   typedef bool yybool;
#else
   typedef unsigned char yybool;
#endif
#define yytrue 1
#define yyfalse 0

#ifndef YYSETJMP
# include <setjmp.h>
# define YYJMP_BUF jmp_buf
# define YYSETJMP(Env) setjmp (Env)
/* Pacify clang.  */
# define YYLONGJMP(Env, Val) (longjmp (Env, Val), YYASSERT (0))
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#ifndef YYASSERT
# define YYASSERT(Condition) ((void) ((Condition) || (abort (), 0)))
#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  205
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   17460

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  295
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  229
/* YYNRULES -- Number of rules.  */
#define YYNRULES  746
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1334
/* YYMAXRHS -- Maximum number of symbols on right-hand side of rule.  */
#define YYMAXRHS 9
/* YYMAXLEFT -- Maximum number of symbols to the left of a handle
   accessed by $0, $-1, etc., in any rule.  */
#define YYMAXLEFT 0

/* YYTRANSLATE(X) -- Bison symbol number corresponding to X.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   533

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned short int yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   291,     2,     2,     2,   294,     2,     2,
      12,    13,   284,   289,   283,   290,    14,   293,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   287,   282,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   285,     2,   286,   288,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   292,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281
};

#if HANA_DEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   275,   275,   286,   287,   295,   296,   297,   298,   302,
     307,   312,   320,   321,   329,   334,   342,   343,   347,   351,
     355,   356,   357,   358,   359,   363,   364,   368,   369,   373,
     374,   378,   385,   392,   401,   410,   419,   428,   440,   449,
     462,   479,   496,   499,   530,   531,   539,   548,   559,   573,
     588,   589,   616,   643,   673,   674,   704,   705,   723,   730,
     733,   736,   738,   741,   770,   771,   780,   787,   788,   791,
     792,   800,   801,   809,   810,   811,   825,   826,   833,   845,
     846,   849,   850,   857,   858,   859,   862,   877,   878,   881,
     882,   883,   884,   887,   896,   899,   900,   903,   904,   905,
     908,   909,   910,   911,   948,   951,   955,   962,   963,   973,
     978,   986,   987,   996,  1004,  1005,  1009,  1017,  1018,  1025,
    1032,  1033,  1040,  1041,  1045,  1052,  1053,  1061,  1075,  1084,
    1085,  1094,  1095,  1096,  1100,  1101,  1112,  1117,  1122,  1127,
    1132,  1137,  1145,  1150,  1158,  1163,  1170,  1171,  1178,  1183,
    1190,  1191,  1192,  1193,  1194,  1195,  1196,  1199,  1200,  1204,
    1205,  1209,  1210,  1211,  1212,  1218,  1226,  1231,  1235,  1240,
    1244,  1250,  1256,  1262,  1273,  1279,  1285,  1292,  1298,  1305,
    1311,  1318,  1324,  1331,  1337,  1344,  1350,  1357,  1363,  1370,
    1376,  1387,  1392,  1397,  1402,  1407,  1412,  1417,  1422,  1427,
    1435,  1440,  1445,  1450,  1461,  1462,  1467,  1472,  1478,  1498,
    1505,  1506,  1513,  1520,  1527,  1534,  1535,  1543,  1569,  1582,
    1595,  1609,  1621,  1622,  1626,  1627,  1628,  1629,  1630,  1631,
    1632,  1633,  1634,  1635,  1639,  1647,  1648,  1656,  1665,  1666,
    1674,  1675,  1683,  1684,  1692,  1693,  1698,  1706,  1707,  1724,
    1725,  1726,  1727,  1728,  1730,  1731,  1732,  1733,  1736,  1741,
    1748,  1753,  1760,  1767,  1776,  1777,  1784,  1787,  1788,  1794,
    1795,  1801,  1802,  1810,  1811,  1817,  1823,  1830,  1836,  1844,
    1845,  1853,  1858,  1863,  1873,  1878,  1886,  1891,  1896,  1901,
    1909,  1914,  1922,  1927,  1935,  1944,  1945,  1950,  1958,  1959,
    1967,  1968,  1976,  1977,  1985,  1986,  1991,  1999,  2000,  2001,
    2002,  2003,  2004,  2007,  2014,  2015,  2023,  2024,  2029,  2030,
    2033,  2033,  2037,  2038,  2039,  2040,  2041,  2042,  2043,  2044,
    2045,  2046,  2050,  2051,  2056,  2063,  2064,  2068,  2069,  2070,
    2074,  2075,  2076,  2077,  2078,  2079,  2080,  2081,  2086,  2094,
    2095,  2099,  2100,  2108,  2113,  2121,  2122,  2131,  2132,  2133,
    2134,  2135,  2139,  2140,  2141,  2146,  2154,  2162,  2169,  2175,
    2185,  2186,  2187,  2188,  2189,  2190,  2191,  2192,  2193,  2194,
    2195,  2196,  2197,  2201,  2208,  2215,  2223,  2230,  2237,  2244,
    2251,  2252,  2262,  2272,  2279,  2292,  2303,  2312,  2313,  2314,
    2318,  2326,  2327,  2331,  2338,  2339,  2346,  2355,  2361,  2371,
    2372,  2373,  2374,  2375,  2376,  2377,  2378,  2379,  2380,  2381,
    2385,  2386,  2389,  2393,  2402,  2407,  2411,  2419,  2420,  2422,
    2431,  2432,  2437,  2438,  2447,  2448,  2452,  2453,  2457,  2458,
    2459,  2463,  2467,  2472,  2473,  2474,  2478,  2482,  2483,  2484,
    2485,  2486,  2490,  2496,  2506,  2507,  2515,  2516,  2519,  2524,
    2535,  2541,  2548,  2555,  2565,  2572,  2581,  2588,  2595,  2602,
    2609,  2618,  2625,  2634,  2641,  2649,  2650,  2657,  2658,  2665,
    2666,  2673,  2682,  2683,  2690,  2735,  2736,  2737,  2738,  2739,
    2740,  2741,  2742,  2743,  2744,  2745,  2746,  2747,  2748,  2749,
    2750,  2751,  2752,  2753,  2754,  2755,  2756,  2757,  2758,  2759,
    2760,  2761,  2762,  2763,  2764,  2765,  2768,  2769,  2770,  2857,
    2861,  2865,  2869,  2873,  2877,  3253,  3254,  3255,  3259,  3260,
    3264,  3265,  3269,  3274,  3283,  3284,  3285,  3286,  3287,  3288,
    3289,  3290,  3291,  3292,  3293,  3294,  3295,  3296,  3297,  3298,
    3299,  3300,  3301,  3302,  3303,  3304,  3305,  3306,  3307,  3308,
    3309,  3310,  3314,  3315,  3319,  3320,  3321,  3322,  3323,  3324,
    3325,  3326,  3327,  3328,  3329,  3330,  3331,  3332,  3333,  3334,
    3335,  3336,  3337,  3338,  3339,  3340,  3341,  3342,  3343,  3344,
    3345,  3346,  3347,  3348,  3349,  3350,  3351,  3352,  3353,  3354,
    3355,  3356,  3357,  3358,  3359,  3360,  3361,  3362,  3363,  3364,
    3365,  3366,  3367,  3368,  3369,  3370,  3371,  3372,  3373,  3374,
    3375,  3376,  3377,  3378,  3379,  3380,  3381,  3382,  3383,  3384,
    3387,  3388,  3389,  3390,  3391,  3392,  3393,  3394,  3395,  3396,
    3397,  3398,  3399,  3400,  3401,  3402,  3403,  3405,  3406,  3407,
    3408,  3409,  3410,  3411,  3414,  3415,  3416,  3417,  3418,  3419,
    3420,  3421,  3422,  3423,  3424,  3425,  3430,  3431,  3436,  3461,
    3462,  3466,  3469,  3474,  3475,  3476,  3480,  3481,  3489,  3490,
    3493,  3494,  3495,  3503,  3504,  3509,  3517,  3518,  3526,  3527,
    3534,  3535,  3538,  3539,  3547,  3548,  3559,  3570,  3586,  3587,
    3596,  3603,  3608,  3614,  3621,  3626,  3632,  3639,  3640,  3641,
    3644,  3651,  3659,  3667,  3674,  3682,  3683,  3690,  3696,  3702,
    3711,  3712,  3714,  3715,  3718,  3731,  3737,  3743,  3749,  3755,
    3763,  3798,  3799,  3800,  3803,  3804,  3810,  3811,  3817,  3818,
    3824,  3825,  3830,  3833,  3834,  3841,  3842
};
#endif

#if HANA_DEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NAME", "STRING", "INTNUM", "BOOL",
  "APPROXNUM", "NULLX", "UNKNOWN", "QUESTIONMARK", "UMINUS", "'('", "')'",
  "'.'", "ALL", "AND", "ANY", "ARRAY", "AS", "ASC", "AVG", "BETWEEN",
  "BIGINT", "BINARY", "BLOB", "BOOLEAN", "BY", "CALL", "CASE", "CAST",
  "CHAR", "CHARACTER", "CHARACTERS", "CLOB", "CNNOP", "COALESCE",
  "CODE_UNITS", "COLLATE", "COMP_EQ", "COMP_GE", "COMP_GT", "COMP_LE",
  "COMP_LT", "COMP_NE", "CONVERT", "CORRESPONDING", "COUNT", "CROSS",
  "CUME_DIST", "CURRENT", "CURRENT_TIMESTAMP", "CURRENT_USER", "DATE",
  "DAY", "DEC", "DECIMAL", "DEFAULT", "DELETE", "DENSE_RANK", "DESC",
  "DISTINCT", "DOUBLE", "ELSE", "END", "END_P", "ESCAPE", "ERROR",
  "EXCEPT", "EXCLUDE", "EXISTS", "FLOAT", "FOLLOWING", "FOR", "FROM",
  "FULL", "G", "GROUP", "GROUPING", "HAVING", "HOUR", "IN", "INNER",
  "INSERT", "INT", "INTEGER", "INTERSECT", "INTERVAL", "MANY", "ONE",
  "EXACT", "INTO", "IS", "JOIN", "K", "LARGE", "LEFT", "LIKE", "M", "MAX",
  "MIN", "MINUTE", "MOD", "MONTH", "MULTISET", "NATIONAL", "NATURAL",
  "NCHAR", "NCLOB", "NO", "NOT", "NULLIF", "NUMERIC", "OBJECT", "OCTETS",
  "OF", "ON", "ONLY", "OR", "ORDER", "OTHERS", "OUTER", "OVER",
  "PARTITION", "PERCENT_RANK", "PRECEDING", "PRECISION", "RANGE", "RANK",
  "READ", "REAL", "RECURSIVE", "REF", "RIGHT", "ROW", "ROWS", "ROW_NUMBER",
  "SCOPE", "SECOND", "SELECT", "SESSION_USER", "SET", "SETS", "SMALLINT",
  "SOME", "STDDEV_POP", "STDDEV_SAMP", "SUM", "SYSTEM_USER", "THEN",
  "TIES", "TIME", "TIMESTAMP", "TO", "UNBOUNDED", "UNION", "UPDATE",
  "USING", "VALUES", "VARCHAR", "NVARCHAR", "VARYING", "VAR_POP",
  "VAR_SAMP", "WHEN", "WHERE", "WITH", "WITHOUT", "YEAR", "ZONE", "LIMIT",
  "OFFSET", "TOP", "WAIT", "NOWAIT", "HINT", "RANGE_RESTRICTION",
  "PARAMETERS", "UPSERT", "REPLACE", "PRIMARY", "KEY", "CONTAINS",
  "P_POINT", "SECONDDATE", "TINYINT", "SMALLDECIMAL", "TEXT", "BINTEXT",
  "ALPHANUM", "VARBINARY", "SHORTTEXT", "ORDINALITY", "UNNEST", "HISTORY",
  "OVERRIDING", "USER", "SYSTEM", "VALUE", "NULLS", "FIRST", "LAST",
  "CORR", "CORR_SPEARMAN", "MEDIAN", "STDDEV", "VAR", "STRING_AGG",
  "MEMBER", "SYSTEM_TIME", "APPLICATION_TIME", "TABLESAMPLE", "BERNOULLI",
  "RETURN", "LATERAL", "BINNING", "LAG", "LEAD", "RANDOM_PARTITION",
  "WEIGHTED_AVG", "EXTRACT", "FIRST_VALUE", "LAST_VALUE", "NTH_VALUE",
  "NTILE", "SERIES_FILTER", "LOCATE_REGEXPR", "OCCURRENCES_REGEXPR",
  "REPLACE_REGEXPR", "SUBSTRING_REGEXPR", "SUBSTR_REGEXPR",
  "PERCENTILE_CONT", "PERCENTILE_DISC", "TRIM", "WITHIN",
  "CUBIC_SPLINE_APPROX", "LINEAR_APPROX", "ROLLUP", "CUBE", "BEST",
  "SUBTOTAL", "BALANCE", "TOTAL", "MULTIPLE", "RESULTSETS", "PREFIX",
  "STRUCTURED", "RESULT", "OVERVIEW", "TEXT_FILTER", "FILL", "UP",
  "MATCHES", "SORT", "MINUS", "ROWCOUNT", "LOCKED", "IGNORE", "SHARE",
  "LOCK", "UTCTIMESTAMP", "COMMIT", "ID", "XML", "JSON", "RETURNS",
  "SERIES", "TABLE", "LIKE_REGEXPR", "FLAG", "LEADING", "TRAILING", "BOTH",
  "WEIGHT", "FULLTEXT", "LANGUAGE", "LINGUISTIC", "FUZZY", "PORTION",
  "START", "AFTER", "OCCURRENCE", "';'", "','", "'*'", "'['", "']'", "':'",
  "'^'", "'+'", "'-'", "'!'", "'|'", "'/'", "'%'", "$accept", "sql_stmt",
  "stmt_list", "stmt", "call_stmt", "sql_argument_list", "sql_argument",
  "value_expression", "sp_name", "dql_stmt", "dml_stmt",
  "opt_partition_rest", "opt_column_ref_list_with_parens",
  "overriding_value", "insert_stmt", "from_constructor", "delete_stmt",
  "update_stmt", "delete_table", "update_table", "update_elem_list",
  "update_elem", "select_stmt", "query_expression",
  "query_expression_body", "query_term", "query_primary",
  "select_with_parens", "subquery", "sap_table_subquery", "table_subquery",
  "row_subquery", "simple_table", "opt_where", "from_clause",
  "opt_from_clause", "opt_groupby", "grouping_element_list",
  "grouping_element", "row_order_by_list", "row_order_by",
  "column_ref_perens", "group_set_name", "grouping_option",
  "grouping_best", "grouping_subtotal", "grouping_prefix_tb",
  "grouping_prefix", "grouping_structured_res", "grouping_text_filter",
  "opt_asc_desc", "opt_having", "with_clause", "with_list",
  "common_table_expr", "opt_derived_column_list",
  "simple_ident_list_with_parens", "simple_ident_list",
  "column_ref_list_with_parens", "column_ref_list", "opt_distinct",
  "select_expr_list", "projection", "from_list", "table_reference",
  "table_primary", "table_primary_non_join", "lateral_table",
  "associated_table", "associated_ref_list", "associated_ref",
  "opt_many2one_part", "opt_search_condition", "opt_tablesample",
  "opt_table_qualify", "collection_derived_table",
  "collection_value_expr_list", "collection_value_expr", "as_derived_part",
  "column_ref", "relation_factor", "func_relation_factor", "joined_table",
  "qualified_join", "case_join", "case_join_when_list", "case_join_when",
  "ret_join_on", "select_expr_list_parens", "opt_case_join_else",
  "cross_join", "join_type", "join_outer", "join_cardinality",
  "hana_construct_table", "construct_column_list", "construct_column",
  "search_condition", "boolean_term", "boolean_factor", "boolean_test",
  "boolean_primary", "predicate", "like_regexpr_redicate",
  "member_of_predicate", "bool_function", "contains_param3", "search_type",
  "search_param_list", "search_param", "expr_const_list",
  "comparison_predicate", "between_predicate", "like_predicate",
  "in_predicate", "null_predicate", "exists_predicate", "row_expr",
  "factor0", "factor1", "factor2", "factor3", "factor4",
  "row_expr_list_parens", "row_expr_list", "row_expr_star", "in_expr",
  "truth_value", "comp_op", "cnn_op", "comp_all_some_any_op",
  "plus_minus_op", "star_div_percent_mod_op", "expr_const", "case_expr",
  "case_arg", "when_clause_list", "when_clause", "case_default",
  "func_expr", "opt_nulls", "aggregate_expression", "aggregate_name",
  "sap_specific_function", "trim_char", "within_group",
  "opt_order_by_clause", "order_by_clause", "order_by_expression_list",
  "order_by_expression", "ranking_windowed_function",
  "ranking_function_name", "opt_over_clause", "over_clause",
  "window_specification", "window_name", "window_specification_details",
  "opt_existing_window_name", "opt_window_partition_clause",
  "opt_window_frame_clause", "window_frame_units", "window_frame_extent",
  "window_frame_start", "window_frame_preceding", "window_frame_between",
  "window_frame_bound", "window_frame_following",
  "opt_window_frame_exclusion", "scalar_function",
  "table_function_param_list", "table_function_param", "expr_point_val",
  "function_call_keyword", "for_xml", "for_json", "opt_returns_clause",
  "opt_option_string_list_p", "option_string_list", "option_string",
  "data_type", "array_type", "predefined_type", "lob_data_type",
  "primary_datetime_field", "all_some_any", "opt_as_label", "as_label",
  "label", "collate_clause", "name_r", "name_f", "reserved", "top_clause",
  "opt_for_update", "for_share_lock", "for_update", "opt_of_ident_list",
  "opt_ignore_lock", "wait_nowait", "limit_num", "offset_num",
  "limit_total", "with_hint_param", "with_hint_param_list", "hint_clause",
  "opt_with_range_restrict", "with_primary_key", "upsert_stmt",
  "replace_stmt", "for_system_time", "sys_as_of_spec", "sys_from_to_spec",
  "sys_between_spec", "for_application_time", "partition_restriction",
  "intnum_list", "tablesample_num", "opt_partition_restriction",
  "opt_for_application_time_clause", "for_application_time_clause",
  "regexpr_str_function", "regex_parameter", "opt_st_or_af", "opt_flag",
  "opt_with_string", "opt_from_position", "opt_occurrence_num",
  "opt_group_num", "param_num", YY_NULLPTR
};
#endif

#define YYPACT_NINF -1048
#define YYTABLE_NINF -564

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const short int yypact[] =
{
     413,   101,  8757,    41,    50,     9,     9, 15855,  8757,  8757,
     188,   153,   -57, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048,    96,   174, -1048, -1048,    83, -1048, -1048,   266, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, 16120,   126, -1048, -1048, -1048,
   -1048,   279, -1048,  8757,   225,  8757,   207,   230,  8757, 16120,
      38, -1048,   319,   132,   132, -1048, -1048,   413,   230,   333,
     230,   230,   201, -1048,   230,    96, -1048, -1048,  3335,   332,
   -1048, 13379,   219, -1048,   270,  8757,   270, -1048, -1048, -1048,
   -1048,  5639,   262,    38, 16120, 16120,   394, -1048, 12823, -1048,
     261, -1048,   261, -1048,    83,  7367,    83,    83,   265,   364,
      83,   201,   449, -1048, -1048, -1048, -1048, -1048, -1048,  3047,
     303,  7367,   461,   467,   473,   478,   481,   492, -1048,   441,
     503,   505,  4487,   507,   517, -1048, -1048,   532,   530,   535,
     538,   550,   556,   558,   560,   575,   577,   587,   594,   605,
     606,   609,   610,   612,   613,  7367,  7367,    -3, -1048,   526,
   -1048, -1048, -1048, -1048,   615,   510,   578, -1048, -1048,   543,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
     923,   -86,    13,   348, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048,   625, -1048, -1048,   626, -1048, -1048,   627,   631, -1048,
     628, -1048,   629,   634,   636, 13657,   638,  4487,   303,   641,
   -1048, 13922,   219, 12545, -1048,  4775, -1048,   -41, -1048,  2768,
    7367, -1048,    21, -1048,   503,  7367,    73,   288,   174,    88,
     373, -1048,   174,   174,   488,   488,   214, -1048, -1048,   303,
   -1048, -1048, -1048,   364,  3047,   648,   111,   923,    22, -1048,
     163,   498,  7367,   658,  7367,  7367,  5063,   662, -1048,   101,
   -1048, -1048,  7367, -1048,   923,  7367,  5927, -1048,  6215,  7367,
    7367,   234,  7367,  7367,  7367,  7367,  7367,   187,   664,   664,
     664,   664,   570,   570,  7655, -1048, -1048,   303,  4487,  1272,
    6503,  4487,  4487,   156,  7367, -1048,  8757, -1048, -1048,   630,
   -1048,   244, -1048,   659,   131,  7367,    74,   557,  7367,   635,
     385,  5351,  7367,   659, -1048, -1048, -1048,  7367, -1048, -1048,
   -1048,  7367,  7367,   230,  6791,  7367,  9035, 11989,   669,   676,
   16120,   668, 14200,   510, -1048,   678, 16120,   611, -1048, -1048,
   -1048,   303, 12823,   285,   261,   101, -1048,   101,   142,  4775,
     163,  8214,  5639,   219, -1048,    -6, -1048,    44, -1048, 16120,
   -1048,   406,   511, -1048,   524,   512, -1048, -1048, -1048,   496,
    7367,   689,   454,   454,   584,   438,   688,   688, -1048,   303,
     690, -1048, -1048, -1048,  7367,  4487,    82, -1048,    95,   691,
      30,   108,  7367,   692,    18,   693,   134,   135, -1048,    51,
   -1048,   163,   418,   -33,   163,    59, -1048, -1048,    78,   633,
     128,   128,   -33,    63,    64, -1048, -1048,   664,   439,   696,
     697,   698,   699,   532,   701,   705,   715,   716,   717,    17,
     719, -1048, -1048, -1048, -1048, -1048, -1048, -1048,   712, -1048,
     713, -1048, -1048, -1048, -1048, -1048, -1048, -1048,   714,   720,
   -1048, -1048, -1048, -1048, -1048,   721,   722,   724, -1048, -1048,
     709, -1048, -1048,    81,   578, -1048, -1048, -1048,   314, -1048,
      69, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,   723,
     140,  7367,   659,  7367,   614,  7367,   154, -1048, -1048, -1048,
     725, -1048,   163, -1048,   -86, -1048, -1048,    13,   348, -1048,
    7367,   616,    90,    19, 14478, -1048,   726, -1048,   417, -1048,
     729, -1048,    98,   733,   700, -1048, 16120, 16120,   736, -1048,
      99, -1048,   474, -1048, -1048, -1048,   105, -1048,   739,   559,
     561,   303, -1048, -1048,   261, -1048,   101,   735,  7936,   743,
     744, 13922, -1048,   445,   455, -1048, -1048, 13922, 13922, 13922,
      37, -1048, -1048, -1048, -1048, 13922,   746, -1048,   681,  7367,
   -1048,   219,  7367, -1048,   583,   589, -1048,   586,   287, -1048,
   -1048, -1048,   508, -1048, -1048, 16120,   327, -1048,   764,   509,
     509, -1048,   365,   163,    72,  1069,  7367, -1048,   706,  1272,
   -1048, -1048,  8757,   106,   616, -1048, -1048,  7367,  7367, -1048,
     767,   762,   616,  7367, 12267,  7367,   763,   769,   771,   616,
     616,   779,   790,   718, -1048, -1048, -1048, -1048,   562,   562,
   -1048,  7367,   728,   792,   793,   798,   799,   800,   804,   805,
   -1048, -1048, -1048,  7367, -1048,  7367,    71, -1048,   147,  7367,
     163,   811,    25, 14756, -1048,   616, -1048, 16120,   802,  9313,
   16385,   303, 11989,   303,   813, -1048, -1048, 16120, -1048,   814,
     711, -1048, 16120,  9591, -1048, -1048, -1048,   303, -1048, 16120,
    7936,   455,   816, 16650, 13101,   319,  8214,   738,   727,   433,
    8214,   727,   727,   138, -1048, -1048, -1048,   345,  4487, 16120,
   13922, -1048, -1048, -1048, -1048, -1048, -1048, -1048, 15034,   806,
     759, -1048,   303,   163, -1048,   827, -1048, -1048, -1048, -1048,
     563,   835, -1048,   585,   808,   107, -1048,  1272, -1048, -1048,
    7367,  7367,   163, -1048,   828,   830, -1048, -1048, -1048,    27,
      35,   109, -1048, -1048, -1048, -1048, -1048,    36,   616,   616,
     616, -1048, -1048, -1048, -1048,  7367,   768,   616,   616,    46,
    7367,   112,   840,   841,   843,   844,   845,   846,   163,   163,
    7367,  7367,   163, -1048,   616, 16915, -1048, -1048, -1048, -1048,
     847, 10134, 16120, -1048,   848, -1048,   849, -1048, -1048, -1048,
     851, -1048, -1048,   645, -1048,   852, -1048,   853, -1048,   855,
     115, -1048, -1048,   856,   857, -1048,   455,  8492, -1048,   433,
     730,   731,   782, -1048,   518,   433,   433,   775,  8214, -1048,
     421,   854,   -43,   858, -1048,   590,   665,   863,   230,  4487,
   -1048, -1048,   875, -1048,   640, -1048,   878, -1048,   764, -1048,
     163,   163, -1048, -1048, -1048, -1048, -1048,   -21, -1048, -1048,
   -1048, -1048,   164,   873, -1048, -1048, -1048,    67, -1048,   881,
   -1048, -1048, -1048, -1048, -1048, -1048,   163,   163, -1048,   620,
   -1048,   876,   774, 10399, -1048,   877,   884, 10664, 16385,   303,
     818,  9869, 16120,  7367,   130, 16650, -1048, -1048, -1048, -1048,
     436,   444,   737,  4487, -1048, -1048,   742,   533,   784,   898,
     904,   797,   632, 16120,  4487,    66, -1048, -1048, 15312,  3623,
     510,   900, -1048, -1048, -1048,   902,   903,   905,   906,   910,
     912,   913,   323,   642, -1048,   924,   859,   812, -1048,   914,
    8757, -1048,   889,   812, -1048, -1048, 10929, 11194, -1048,   916,
   -1048,   918, -1048,   930,   922,   116, -1048,  1272,   118,   936,
     748, -1048, -1048, -1048, -1048,   860, -1048, -1048,   861,   450,
     510,  4487,   151, -1048,  4487,   934,   925,   794,   944, 16120,
   -1048,   795,   510,   946,   941,   942,   943,  3911,   817, -1048,
   -1048,   672, -1048,   732,   163, -1048,   952,   570,   114,   956,
     964,  7367, -1048,   642,   323, -1048,   425,   694,   957, -1048,
   -1048,  7367,    52, -1048, -1048, -1048, -1048, 11459, 17180,   819,
   11724, -1048, 16120, -1048, -1048,   961,   950, -1048, -1048, -1048,
   -1048,   885,   110,   765, -1048,   915,   510, -1048,   972,   973,
   -1048,   858,   120,   695,   967,   977,   978, 16385, -1048, -1048,
    3623,   979,   974,   201,   975,   121, -1048,   976,   980,   981,
     982,   122, -1048, -1048, -1048, -1048,   381,   908, -1048,   406,
   -1048, -1048, -1048,   506, -1048, -1048, -1048, -1048,   983, -1048,
   16120,   987, -1048,   765,   985, -1048, -1048, -1048, -1048,   838,
     839,   476, -1048, -1048,   986,   994, -1048, -1048, 15590,   832,
   -1048, -1048,   570, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
     425, -1048,   597,   866,   886,   887,   945, -1048, -1048, -1048,
   -1048,   123,  1004, -1048,  5639,   947,   495,   521,   926,   929,
   -1048, -1048,  7079,   124, -1048, -1048, -1048, -1048,   391,   776,
   -1048, -1048,   102,   139, -1048,  1001, -1048, -1048, -1048, -1048,
     224, -1048, -1048, 16120,   127,  8214,   931,   933,   937,   939,
   -1048, -1048,  4199, -1048,   812,   143, -1048, 15590, -1048, -1048,
   -1048,  1014,   206, -1048, -1048,   597,   895, -1048,   927, -1048,
     129, -1048,   582, -1048, -1048, -1048, -1048,  1010, -1048,   788,
     801,   787, -1048,   803, -1048, -1048, -1048, -1048,  4487, -1048,
     791, -1048,   880,  1039, -1048,   510,   796,   807, -1048,   810,
   -1048,   891,   879, -1048
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const unsigned short int yydefact[] =
{
       5,     0,     0,     0,     0,   666,   666,     0,     0,     0,
       0,     0,     3,     8,     6,     7,    22,    21,    20,    19,
      47,   401,    50,    54,    56,     0,    23,    24,     0,   534,
     561,   568,   600,   376,   569,   570,   544,   571,   601,   572,
     602,   573,   555,   412,   521,   604,   410,   595,   606,   599,
     574,   567,   522,   575,   608,   609,   627,   565,   576,   566,
     374,   372,   523,   607,   520,   577,   629,   603,   578,   579,
     591,   580,   598,   411,   581,   582,   409,   592,   583,   584,
     585,   413,   586,   524,   597,   379,   381,   375,   596,   594,
     593,   605,   587,   588,   380,   382,   589,   519,   590,   621,
     620,   610,   611,   545,   612,   613,   614,   615,   616,   617,
     618,   619,   656,   622,   623,   625,   624,   626,   661,   662,
     370,   371,   373,   377,   378,   543,   628,   546,   414,   415,
     416,   417,   547,   548,   549,   550,   551,   538,   539,   540,
     541,   542,   552,   553,   554,   418,   419,   630,   631,   632,
     633,   634,   635,   636,   637,   638,   639,   640,   641,   642,
     643,   644,   645,   646,   647,   648,   649,   650,   651,   653,
     652,   654,   655,   657,   658,   659,   660,   556,   557,   558,
     559,   560,   663,   664,   665,     0,   694,    18,   536,   537,
     564,   191,   535,     0,     0,     0,     0,   122,     0,   583,
     109,   111,   114,   114,   114,     1,     2,     5,   122,     0,
     122,   122,   683,   402,   122,   401,    57,   199,     0,     0,
      11,     0,    64,    42,   720,     0,    25,   667,   668,   123,
     124,     0,     0,   110,     0,     0,     0,   115,     0,   703,
       0,   706,     0,     4,     0,     0,     0,     0,     0,   669,
       0,   683,   534,   340,   343,   341,   344,   342,   345,     0,
     694,   349,   570,   544,   601,   602,   555,   467,   469,     0,
       0,     0,     0,   603,     0,   471,   472,   593,   545,   543,
     546,   547,   548,   549,   550,   551,     0,   538,   539,   540,
     541,   542,   552,   553,   554,     0,     0,     0,    12,    14,
      59,    62,   309,   307,     0,    16,   238,   240,   242,   244,
     247,   257,   256,   255,   249,   250,   252,   251,   253,   254,
      17,   295,   298,   300,   302,   304,   310,   308,   311,   312,
     360,   536,   361,   359,   537,   358,   357,   564,   174,   200,
     535,   390,     0,     0,     0,     0,   192,     0,   694,     0,
     721,   528,    64,    27,    26,     0,   128,     0,   125,   528,
       0,   112,     0,   117,     0,     0,    64,    64,    52,   104,
     403,   404,    51,    53,   686,   686,     0,   674,   675,   694,
     673,   670,    55,   669,     0,     0,     0,   314,     0,    10,
     350,     0,     0,     0,     0,     0,     0,     0,   346,     0,
      61,   294,     0,   243,     0,     0,     0,   347,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   731,     0,     0,
       0,     0,     0,     0,     0,   305,   306,   694,     0,     0,
       0,     0,     0,     0,     0,   332,     0,   326,   324,   325,
     322,   323,   327,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   296,   335,   336,     0,   337,   338,
     339,     0,     0,   122,     0,     0,     0,     0,     0,     0,
       0,   194,     0,    65,    39,     0,     0,   722,   529,   531,
     532,   694,     0,   623,     0,     0,    32,    28,    27,     0,
     314,     0,     0,    64,   127,    67,    44,     0,   116,     0,
     113,    38,     0,   701,   698,     0,   704,   105,   106,   362,
       0,     0,   688,   688,   676,     0,   477,   477,    48,   694,
       0,    58,   248,   313,     0,     0,   355,   351,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   461,     0,
     317,   316,     0,   401,   457,     0,   454,   456,   174,     0,
     401,   401,   401,     0,     0,   732,   733,     0,   734,     0,
       0,     0,     0,     0,     0,     0,   658,   659,   660,     0,
       0,     9,    13,   493,   516,   514,   517,   485,   509,   496,
     512,   492,   491,   518,   495,   490,   486,   488,   499,   501,
     487,   489,   494,   497,   498,   503,   505,   507,    15,   483,
     482,   515,   452,     0,   239,   241,   320,   321,     0,   245,
       0,   533,   328,   329,   330,   318,   319,   290,   292,     0,
     286,     0,     0,     0,     0,     0,   259,   331,   333,   525,
     561,   526,   283,   334,   297,   281,   282,   299,   301,   303,
       0,     0,     0,     0,     0,   176,   175,   201,   690,   691,
       0,   692,     0,     0,     0,   198,     0,     0,   193,   715,
       0,   530,     0,    43,   723,    40,     0,   120,   174,     0,
       0,   694,    35,    34,     0,    31,    28,     0,     0,     0,
       0,   528,    60,    66,   129,   131,   134,   528,   528,   528,
     161,   132,   204,   205,   133,   528,   191,   126,    69,     0,
      68,    64,     0,   118,     0,     0,   702,     0,     0,   406,
     405,   687,     0,   684,   685,     0,   680,   671,     0,   475,
     475,    49,    58,   315,     0,     0,     0,   352,     0,     0,
     369,   466,     0,     0,   420,   367,   468,     0,     0,   462,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   726,   727,   728,   729,     0,     0,
     396,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     484,   453,   246,     0,   293,     0,     0,   291,   288,     0,
     261,     0,     0,     0,   407,     0,   460,     0,     0,     0,
       0,   694,     0,   694,     0,   196,   197,     0,   714,     0,
       0,   119,     0,     0,    30,    29,    37,   694,    33,     0,
       0,     0,   132,     0,     0,   114,     0,     0,   222,   224,
       0,   222,   222,     0,   141,   139,   140,     0,     0,     0,
     528,   162,   707,   708,   709,   163,   164,   138,     0,     0,
     107,    45,   694,    46,   700,     0,   705,   363,   364,   689,
     677,     0,   681,   678,     0,     0,   479,     0,   473,   474,
       0,     0,   356,   348,     0,     0,   366,   365,   421,     0,
       0,     0,   393,   383,   455,   459,   458,     0,   420,   420,
     420,   389,   384,   725,   735,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   284,   287,
       0,     0,   260,   258,   420,   427,   423,   422,   425,   408,
       0,     0,     0,   178,   177,   202,     0,   695,   693,   696,
       0,   195,   716,     0,   121,   175,    36,     0,   135,   568,
       0,   167,   169,     0,     0,   137,   130,     0,   223,   224,
       0,     0,     0,   221,     0,   224,   224,     0,     0,   208,
       0,     0,     0,   145,   146,   149,   159,   192,   122,     0,
      63,    41,     0,   682,     0,   672,     0,   478,     0,   476,
     353,   354,   464,   465,   463,   470,   263,     0,   385,   386,
     387,   388,   736,     0,   391,   392,   395,     0,   510,     0,
     513,   500,   502,   504,   506,   508,   285,   289,   368,   654,
     428,     0,   430,     0,   182,   181,     0,     0,     0,   694,
       0,     0,     0,     0,     0,     0,   143,   142,   217,   218,
       0,     0,     0,     0,   219,   220,     0,     0,     0,     0,
       0,     0,     0,     0,   157,     0,   136,   160,     0,     0,
     108,     0,   679,   481,   480,   267,     0,     0,     0,   271,
     269,     0,   264,   266,   273,     0,   738,     0,   394,     0,
       0,   424,     0,   401,   190,   189,     0,     0,   180,   179,
     203,     0,   697,     0,   177,     0,   235,     0,     0,     0,
       0,   165,   166,   225,   226,     0,   228,   230,     0,     0,
     207,     0,   215,   210,     0,     0,     0,     0,     0,     0,
     147,   150,   158,     0,     0,     0,   193,     0,     0,    84,
      85,    70,    71,    87,    74,   699,     0,     0,     0,     0,
       0,     0,   262,   265,     0,   737,     0,   740,     0,   511,
     429,     0,   432,   186,   185,   188,   187,     0,     0,     0,
       0,   234,     0,   237,   168,   173,     0,   227,   232,   229,
     231,     0,     0,     0,   211,     0,   206,   710,     0,     0,
     713,   144,     0,     0,     0,     0,     0,     0,    73,    83,
       0,     0,     0,   683,     0,     0,   279,     0,     0,     0,
       0,     0,   274,   745,   746,   739,     0,   743,   400,   431,
     435,   434,   426,     0,   184,   183,   562,   563,     0,   236,
       0,     0,   233,     0,     0,   216,   209,   712,   711,     0,
       0,     0,   148,   719,     0,     0,    72,    88,     0,    89,
     268,   275,     0,   277,   276,   278,   272,   270,   742,   741,
       0,   730,     0,     0,     0,     0,   447,   436,   440,   437,
     724,     0,   172,   212,     0,     0,     0,     0,     0,     0,
     718,   717,     0,     0,    76,    79,    80,    81,     0,   100,
     280,   744,     0,     0,   443,     0,   445,   439,   438,   441,
       0,   433,   171,     0,     0,     0,     0,     0,     0,     0,
     152,   151,     0,    82,   401,   310,    75,     0,    90,    91,
      92,     0,    97,   444,   446,     0,     0,   449,     0,   450,
       0,   214,     0,   154,   153,   156,   155,     0,    77,   101,
       0,     0,    86,    95,   442,   448,   451,   170,     0,    78,
       0,    94,    98,     0,    93,   213,   102,     0,    96,     0,
      99,     0,     0,   103
};

  /* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
   -1048, -1048,   850, -1048, -1048, -1048,   622, -1048, -1048, -1048,
   -1048, -1048,   564, -1048, -1048,  -227, -1048, -1048,   829,   862,
   -1048,   354,  -185,    10,  1031,   216,   809, -1048,  -267, -1048,
    -243, -1048, -1048,  -316,   571, -1048, -1048, -1048,  -102, -1048,
    -218,  -182, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048,   872,   867,  -195, -1048, -1047,   591,   359,
    -169,  -166,   592, -1048,  -454,   144, -1048, -1048, -1048,   -24,
      49, -1048, -1048, -1048, -1048, -1048, -1048,    68, -1048,  -457,
      12, -1048,  -590, -1048, -1048, -1048,    -7,  -117, -1048, -1048,
   -1048, -1048,  -209,  -524, -1048, -1048,   -55,  -257,   657,  -183,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,    40,   -35,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048,  1738,   643,   639,
     644,  -144, -1048,  -427,  -348, -1048,   468,   485, -1048, -1048,
   -1048, -1048, -1048,  -400, -1048, -1048, -1048,   574, -1048,  -439,
   -1048, -1048,   842, -1048, -1048,   355,  -211,    58, -1048,   608,
   -1048,   935,  -421,  -244, -1048,   211, -1048, -1048, -1048, -1048,
   -1048, -1048,   -74, -1048, -1048,  -174, -1048, -1048,  -462,  -269,
     379, -1048, -1048, -1048, -1048,   403,   619, -1048,   157,   397,
   -1048,  -833, -1048,    91, -1048,  -312,   786,   651, -1048,    -2,
    -422,  1536,  1126,   754, -1048, -1048, -1048, -1048, -1048,  -250,
     766,   646,   350, -1048,  -247, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048,  -214, -1048, -1048, -1048, -1048, -1048,
   -1048,  -262, -1048, -1048, -1048, -1048, -1048, -1048, -1036
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const short int yydefgoto[] =
{
      -1,    10,    11,    12,    13,   297,   298,   299,   186,    14,
      15,   353,   484,   485,    16,   366,    17,    18,   222,   223,
     495,   496,    19,    20,    21,    22,    23,   300,   301,   681,
     682,   302,    24,   348,   493,   701,   840,  1111,  1112,  1253,
    1254,  1255,  1113,  1172,  1173,  1259,  1312,  1324,  1313,  1292,
     509,   960,    25,   200,   201,   236,   237,   362,   487,   666,
     231,   357,   358,   683,   811,   685,   686,   687,   688,   953,
     954,  1163,  1101,  1036,   830,   689,   930,   931,  1081,   303,
     690,   304,   691,   692,   949,  1092,  1093,  1205,  1245,  1155,
     693,   823,   939,   943,   694,  1075,  1076,   305,   306,   307,
     308,   309,   310,   311,   312,   313,  1051,  1052,  1053,  1054,
    1175,   314,   315,   316,   317,   318,   319,   490,   321,   322,
     323,   324,   325,   326,   388,   542,   617,   609,   451,   452,
     453,   457,   461,   327,   328,   391,   526,   527,   728,   329,
     709,   330,   331,   332,   570,   887,   212,   213,   370,   371,
     333,   334,   867,   868,   906,   907,  1001,  1002,  1063,  1192,
    1193,  1236,  1264,  1238,  1239,  1265,  1266,  1271,   335,   545,
     546,   547,   336,   377,   378,   858,   719,   855,   856,   598,
     599,   600,   601,   337,   633,   477,   478,   479,   454,   338,
     339,   340,   197,   379,   380,   381,   716,   965,   853,   249,
     512,   713,   651,   652,   220,   706,   504,    26,    27,   831,
     832,   833,   834,   835,   350,   660,  1037,   351,   663,   664,
     341,   559,   557,   753,  1056,  1127,  1187,  1231,  1185
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const short int yytable[] =
{
     191,   383,   386,   400,   251,   202,   191,   191,   240,   242,
     427,    28,   354,   389,   187,   367,   616,   501,   239,   241,
     203,   204,   564,   565,   969,   667,   636,   401,   649,   695,
     760,   735,   786,   491,   498,   523,   481,   684,   904,   244,
     974,   246,   247,   731,   647,   250,   530,   494,   975,   978,
     503,   506,   435,   435,   435,   436,   436,   436,   539,   986,
     435,   543,   435,   436,   739,   436,   552,   553,   491,  1045,
     435,   435,   742,   436,   436,   431,   749,   750,  1103,   435,
    1058,   435,   436,   702,   436,   773,   209,   900,   812,   403,
     473,   761,   466,   190,   771,     1,   621,   400,   190,   190,
     190,   474,   435,   785,   435,   436,   435,   436,   507,   436,
     827,   791,   798,     1,   729,   193,   642,  1177,   801,   866,
     967,   500,   976,   435,   522,   988,   436,   386,  1014,  1141,
     435,  1144,   518,   436,  1221,  1227,  1272,  1286,   218,   618,
    1301,   195,  1317,   435,   238,   726,   436,   554,   508,  1079,
    1229,   425,   426,  1241,   482,   622,   -81,   560,   561,   562,
     349,   603,   606,   435,   208,   607,   436,   947,   486,   435,
     435,   623,   436,   436,  1293,   435,   400,   698,   436,  1190,
     571,   196,   435,   217,   733,   436,   635,  1191,   205,   435,
     431,   191,   436,   191,  1261,   616,   191,   202,   435,   435,
     615,   436,   436,   455,   456,   224,   775,   226,  1209,  1210,
     224,  1294,   227,   901,  1153,   209,   695,   228,   206,   346,
     812,   860,     5,   191,   400,   207,  1300,  1268,   431,   431,
    1178,   948,   202,   363,   665,   194,   363,   224,   347,   502,
       5,   619,   492,  1032,  1143,   229,   525,   209,    28,   605,
     524,   210,  1046,  1047,  1048,  1049,  1050,   671,   667,  1203,
     214,   744,   -81,  1104,  1269,   732,   608,     7,   724,   385,
     374,     5,   721,  1211,  1296,   375,   190,   699,  1105,   216,
     428,     5,   624,   613,   190,   614,   190,   875,    44,   190,
     190,   230,   219,   221,   640,   751,  1080,   458,     7,   225,
     672,  1297,   673,   675,   499,   524,   459,   460,     7,   450,
     450,   450,   190,   524,    52,  1091,   190,   450,   639,   450,
     606,   234,   828,   607,   829,   190,   190,   450,   450,   190,
    1055,   235,   741,  1298,   524,    62,   450,    64,   450,   746,
     747,   748,   743,   471,   876,   924,   524,   743,   695,   480,
     211,   480,   934,   649,   695,   400,   932,   480,   695,   450,
     245,   450,   936,   450,   743,   385,   944,   915,   647,   815,
     514,   248,    83,   524,  1299,   824,   825,   826,   -57,   615,
     450,   792,   799,   837,   347,   842,  1183,   450,   802,   524,
     968,  1184,   977,   349,   520,   989,  1228,   784,  1015,  1142,
     450,   524,    97,   360,  1222,   524,   499,  1287,   548,   385,
     492,   400,   499,   364,   548,  1019,   647,   737,   738,   365,
     450,  1024,  1025,   781,   806,     1,   450,   450,   548,  -562,
    1183,  -534,   450,   -57,   191,  1184,   190,   376,   -57,   450,
    1028,     2,   190,  1029,   190,   398,   450,   807,   611,  1310,
     190,   -57,  1311,   347,   505,   450,   450,   979,   980,   981,
     368,  -562,   372,   373,   646,   650,   555,   556,   655,   219,
     658,     3,   515,   392,   480,   695,   836,   516,   517,   393,
     668,   669,   670,   998,   -57,   394,   695,   847,   848,   696,
     395,   808,    28,   396,  1027,  1030,     4,   703,   873,   520,
     851,   852,   549,   817,   397,   881,   882,   342,   343,   344,
     253,   254,   255,   256,   257,   399,   258,   402,   956,   405,
     -57,   940,   941,   942,  1083,  1084,  1085,   190,  1232,   406,
     818,   -57,  1086,  1087,  1088,   -57,   407,   819,  1149,  1150,
    1151,   909,   408,   400,   917,   429,   919,   409,   820,   400,
     410,   821,     5,   400,   950,   951,  1233,   190,   932,   269,
     926,   190,   411,   190,  1248,  1249,   817,   190,   412,     6,
     413,   952,   414,   190,   253,   254,   255,   256,   257,     7,
     258,   817,   190,  1276,  1277,  1070,   915,   415,   822,   416,
     190,     8,     9,   818,   432,   961,  1046,  1047,  1048,   417,
     819,   253,   254,   255,   256,   257,   418,   258,   818,  1278,
    1279,   820,   945,   946,   821,   819,   915,   419,   420,   -57,
     935,   421,   422,   269,   423,   424,   820,   430,   431,   821,
     817,  1288,  1289,  1290,  1023,   433,   462,   463,   464,   465,
    -563,   467,   788,   984,   985,   466,   468,  1233,   469,  1094,
     269,   822,   472,   475,   795,   796,   510,   818,   563,   511,
    1234,   521,   525,   529,   819,  1078,   822,   535,   558,   612,
     400,   355,   625,   653,   627,   820,   696,   628,   821,   480,
     654,   400,   656,   659,   662,   480,   480,   480,   385,   524,
     705,   704,   707,   480,   711,   708,   712,   717,  1318,   715,
     718,   740,  1040,   722,   730,   734,   736,   745,   752,   754,
     755,   756,   757,   668,   758,   822,  1070,  1176,   759,  -397,
    -398,  -399,   563,   762,   763,   764,   765,   770,   816,   779,
     191,   774,   766,   767,   768,   190,   769,  -527,   783,   794,
     789,   548,   668,   790,   865,  1070,   793,   190,   190,   563,
     797,  1262,   800,   803,   809,   813,   814,   804,   839,   805,
     838,  1256,  1072,   849,   844,   845,  1090,   846,   854,   190,
     863,   871,   190,  1181,   857,   872,   878,  1102,   190,   190,
     190,   908,   879,  1189,   880,   910,   190,   914,   916,  1039,
     650,  1257,   883,  1235,   884,   921,   886,   891,   892,   885,
     668,   925,   890,   893,   894,   895,   190,   927,   696,   896,
     897,   668,   650,   695,   696,   903,   911,   920,   696,   922,
     520,  1302,  1260,   190,   933,  1285,   923,   955,   480,   928,
    1256,   937,  1263,   958,  1152,   190,   957,  1156,   959,   962,
     963,   972,   964,   973,   188,   983,   802,   966,   938,   188,
     188,   188,  1132,   990,   991,  1010,   992,   993,   994,   995,
    1257,  1003,  1007,  1008,  1009,  1012,  1011,  1013,  1026,  1016,
    1017,  1022,  1033,  1031,   190,  1034,  1035,  1038,   190,  1041,
     190,   190,  1043,  1020,  1021,  1057,  1059,  1060,   190,  1061,
    1089,  1066,  1073,   190,   190,  1263,  1042,  1062,  1067,  1095,
     190,   190,  1096,   908,   190,   190,  1091,   190,  1097,  1005,
    1006,   190,  1098,  1115,  1116,  1117,  1131,  1118,  1119,  1099,
     190,   190,  1120,  1219,  1121,  1124,  1122,  1129,  1125,   190,
    1137,   209,  1138,  1126,  1139,   696,  1140,   189,  1157,  1145,
    1146,  1158,   189,   189,   189,   434,   696,  1159,  1160,  1147,
    1148,  1164,  1162,  1165,  1166,  1170,  1174,  1167,   435,  1169,
    1179,   436,   437,   438,   439,   440,   441,   442,  1180,  1201,
    1188,  1171,  1198,  1200,  1202,  1186,  1207,  1208,  1204,  1206,
    1213,  1212,  1214,  1215,  1217,  1230,  1218,  1240,  1220,  1223,
    1242,  1246,  1247,  1224,  1225,  1226,   190,  1244,  1258,  1250,
    1267,  1065,   190,   190,   443,  1069,  1071,  1251,   400,  1074,
    1077,  1268,  1269,   668,  1270,   444,  1273,  1295,  1309,  1280,
     445,  1275,  1281,  1319,  1303,  1291,  1304,   188,   190,  1315,
    1305,   955,  1306,   446,  1322,   188,  1106,   188,  1320,   190,
     188,   188,  1326,  1328,  1332,  1321,  1327,  1316,  1323,  1329,
     572,  1333,   674,   841,   352,  1330,   215,   243,   191,   382,
     232,  1325,  1331,   188,  1134,  1136,   700,   188,  1216,  1308,
    1284,   233,  1130,  1307,   850,  1161,   188,   188,  1274,   676,
     188,  1018,  1100,  1082,   697,  1154,  1243,  1199,   604,  1182,
     777,   434,  1123,   772,   190,   634,   637,   955,   190,   190,
     727,   361,   190,   190,   435,   638,   190,   436,   437,   438,
     439,   440,   441,   442,   888,  1128,  1000,   385,   710,  1237,
     189,  1314,   874,   859,   190,  1044,   864,   661,   189,   190,
     189,   447,   198,   189,   189,  1195,   720,   519,  1069,   488,
    1077,   513,   918,     0,     0,     0,     0,     0,     0,     0,
     443,   190,     0,     0,     0,     0,   189,   190,   190,   714,
     189,   444,     0,     0,     0,   921,   445,     0,     0,   189,
     189,     0,     0,   189,     0,     0,     0,     0,     0,   446,
       0,     0,     0,     0,     0,     0,     0,   188,     0,     0,
     190,   448,     0,   188,     0,   188,     0,     0,   363,     0,
       0,   188,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   449,   450,   668,     0,   861,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   190,   190,
       0,   190,     0,   190,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   190,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   363,     0,   696,     0,     0,     0,   447,   188,     0,
     189,     0,     0,     0,     0,   668,   189,     0,   189,     0,
       0,   190,   385,     0,   189,   573,     0,   574,   575,     0,
       0,     0,     0,     0,     0,     0,   576,     0,   188,   190,
       0,     0,   188,     0,   188,     0,     0,     0,   188,     0,
       0,     0,     0,     0,   188,   577,     0,     0,   578,     0,
       0,     0,     0,   188,   579,     0,     0,   448,     0,     0,
       0,   188,     0,   580,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   581,   582,     0,     0,
     449,   450,     0,     0,   190,     0,   190,     0,     0,     0,
       0,   189,     0,     0,     0,     0,     0,     0,   190,     0,
     583,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   189,   584,     0,     0,   189,     0,   189,     0,     0,
       0,   189,     0,     0,     0,   585,     0,   189,     0,     0,
       0,     0,     0,   586,   587,     0,   189,     0,     0,     0,
       0,   588,   589,     0,   189,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   590,   591,   592,   593,
     594,   595,   596,   597,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   188,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   188,   188,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     188,     0,     0,   188,     0,     0,     0,     0,     0,   188,
     188,   188,     0,     0,     0,     0,     0,   188,   192,     0,
       0,     0,     0,   192,   192,   192,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   188,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   188,     0,     0,     0,     0,   189,
       0,     0,     0,     0,     0,     0,   188,     0,     0,     0,
       0,   189,   189,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   189,     0,     0,   189,     0,     0,     0,
       0,     0,   189,   189,   189,   188,     0,     0,     0,   188,
     189,   188,   188,     0,     0,     0,     0,     0,     0,   188,
       0,     0,     0,     0,   188,   188,     0,     0,     0,     0,
     189,   188,   188,     0,     0,   188,   188,     0,   188,     0,
       0,     0,   188,     0,     0,     0,     0,   189,     0,     0,
       0,   188,   188,     0,     0,     0,     0,     0,     0,   189,
     188,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   189,     0,
       0,   192,   189,     0,   189,   189,     0,     0,     0,   192,
       0,   192,   189,     0,   192,   192,     0,   189,   189,     0,
       0,     0,     0,     0,   189,   189,     0,   188,   189,   189,
       0,   189,     0,   188,   188,   189,     0,   192,     0,     0,
       0,   192,     0,     0,   189,   189,     0,     0,     0,     0,
     192,   192,     0,   189,   192,     0,     0,     0,     0,   188,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     188,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     189,     0,     0,     0,     0,   188,   189,   189,     0,   188,
     188,     0,     0,   188,   188,     0,     0,   188,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   189,     0,     0,   188,     0,     0,     0,     0,
     188,   192,     0,   189,     0,     0,     0,   192,     0,   192,
       0,     0,     0,     0,     0,   192,     0,     0,     0,     0,
       0,     0,   188,     0,     0,     0,     0,     0,   188,   188,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   189,     0,
       0,   188,   189,   189,     0,     0,   189,   189,     0,     0,
     189,     0,     0,     0,     0,     0,   320,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   189,   359,
       0,     0,   192,   189,     0,     0,     0,     0,     0,   188,
       0,     0,   188,   369,   188,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   189,     0,   387,     0,   390,
       0,   189,   189,     0,     0,     0,   192,     0,   192,   188,
     404,     0,   192,     0,     0,     0,     0,     0,   192,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   189,   192,     0,     0,     0,     0,
       0,     0,   188,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     188,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   189,     0,     0,   189,     0,   189,     0,     0,
       0,     0,     0,     0,     0,   404,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   497,     0,
       0,     0,   189,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   188,     0,   188,     0,     0,
       0,     0,   387,     0,     0,     0,     0,     0,     0,   188,
     528,     0,     0,   531,   534,   189,     0,     0,     0,     0,
     536,     0,     0,   537,     0,     0,   541,     0,   544,     0,
     550,   551,     0,   189,   544,     0,     0,     0,     0,     0,
       0,     0,   569,     0,     0,     0,   320,     0,   544,   404,
     404,     0,   610,     0,     0,     0,     0,     0,     0,     0,
     192,     0,     0,   620,     0,     0,   626,     0,     0,   632,
       0,     0,   192,   192,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   643,     0,     0,     0,     0,   189,     0,
     189,     0,     0,     0,     0,     0,     0,   192,     0,     0,
       0,     0,   189,   192,   192,   192,     0,     0,     0,     0,
     359,   192,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   369,     0,
       0,   192,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   723,   725,     0,     0,     0,     0,   192,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     192,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   192,
       0,     0,     0,   192,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   192,     0,     0,     0,     0,   192,   192,
       0,     0,     0,     0,     0,   192,     0,     0,     0,   192,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   776,
       0,   778,     0,   780,     0,   192,   192,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   782,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   497,     0,     0,
     843,   192,     0,     0,     0,     0,     0,   192,   192,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   862,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   869,   870,     0,     0,     0,
       0,   544,     0,   877,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   889,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   898,     0,   899,     0,     0,     0,   902,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   192,
       0,     0,     0,     0,     0,     0,     0,   192,   192,     0,
       0,   192,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   404,     0,     0,   192,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   192,     0,   970,   971,
       0,     0,   192,   192,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   982,     0,     0,     0,     0,   987,     0,
       0,     0,     0,     0,     0,   192,     0,     0,   996,   997,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   192,  1197,     0,   192,     0,   192,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   404,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   192,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   192,     0,     0,     0,     0,     0,
       0,   404,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    29,   404,     0,     0,     0,     0,  1114,     0,     0,
       0,     0,     0,     0,     0,    30,    31,   476,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,    35,    36,
       0,    37,     0,   435,    38,    39,   436,     0,     0,   192,
       0,     0,     0,    40,    41,    42,     0,    43,     0,     0,
       0,     0,    44,   192,     0,    45,     0,    46,    47,   404,
      48,     0,   404,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,    67,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,     0,    80,     0,    81,    82,    83,     0,  1114,     0,
      84,     0,     0,    85,    86,    87,     0,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   125,     0,     0,     0,     0,
     126,     0,   359,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,   137,   138,   139,   140,   141,   142,
     143,   144,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,     0,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
     252,   253,   254,   255,   256,   257,   404,   258,     0,   384,
     450,     0,     0,     0,    30,    31,     0,    32,    33,     0,
       0,    34,     0,     0,     0,     0,   261,   262,   263,     0,
      37,     0,     0,   264,    39,     0,     0,     0,     0,     0,
       0,     0,   265,    41,   266,     0,    43,     0,   267,   268,
     269,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,   270,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,   271,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,   272,   273,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
     274,    80,     0,    81,    82,    83,     5,   275,     0,    84,
       0,     0,    85,    86,    87,   276,     0,    88,    89,   277,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     7,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   278,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   279,     0,     0,     0,     0,   126,
       0,     0,   280,   128,   129,   130,   131,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   293,
     294,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,     0,     0,
       0,     0,     0,     0,     0,     0,   295,   296,   252,   253,
     254,   255,   256,   257,     0,   258,     0,   259,   260,     0,
       0,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,   261,   262,   263,     0,    37,     0,
       0,   264,    39,     0,     0,     0,     0,     0,     0,     0,
     265,    41,   266,     0,    43,     0,   267,   268,   269,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,   270,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,   271,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,   272,   273,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,   274,    80,
       0,    81,    82,    83,     0,   275,     0,    84,     0,     0,
      85,    86,    87,   276,     0,    88,    89,   277,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   278,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   279,     0,     0,     0,     0,   126,     0,     0,
     280,   128,   129,   130,   131,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   293,   294,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,     0,     0,     0,     0,
       0,     0,     0,     0,   295,   296,   252,   253,   254,   255,
     256,   257,     0,   258,     0,  1107,     0,     0,     0,     0,
      30,    31,     0,    32,    33,     0,     0,    34,     0,     0,
       0,     0,   261,   262,   263,     0,    37,     0,     0,   264,
      39,     0,     0,     0,     0,     0,     0,     0,   265,    41,
     266,     0,    43,     0,   267,   268,   269,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,  1108,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,   271,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,   273,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,   274,    80,     0,    81,
      82,    83,     0,   275,     0,    84,     0,     0,    85,    86,
      87,   276,     0,    88,    89,   277,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     279,     0,     0,     0,     0,   126,     0,     0,   280,   128,
     129,   130,   131,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   293,   294,     0,   145,   146,
    1109,  1110,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,     0,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,     0,     0,     0,     0,     0,     0,
       0,     0,   295,   296,   252,   253,   254,   255,   256,   257,
       0,   258,     0,   489,  1168,     0,     0,     0,    30,    31,
       0,    32,    33,     0,     0,    34,     0,     0,     0,     0,
     261,   262,   263,     0,    37,     0,     0,   264,    39,     0,
       0,     0,     0,     0,     0,     0,   265,    41,   266,     0,
      43,     0,   267,   268,   269,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,   271,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,   273,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,   274,    80,     0,    81,    82,    83,
       5,   275,     0,    84,     0,     0,    85,    86,    87,   276,
       0,    88,    89,   277,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     7,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,     0,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   279,     0,
       0,     0,     0,   126,     0,     0,   280,   128,   129,   130,
     131,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   293,   294,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,     0,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,     0,     0,     0,     0,     0,     0,     0,     0,
     295,   296,   252,   253,   254,   255,   256,   257,     0,   258,
       0,   489,  1283,     0,     0,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,   261,   262,
     263,     0,    37,     0,     0,   264,    39,     0,     0,     0,
       0,     0,     0,     0,   265,    41,   266,     0,    43,     0,
     267,   268,   269,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,   271,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
     273,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,   274,    80,     0,    81,    82,    83,     5,   275,
       0,    84,     0,     0,    85,    86,    87,   276,     0,    88,
      89,   277,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     7,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   279,     0,     0,     0,
       0,   126,     0,     0,   280,   128,   129,   130,   131,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,     0,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,     0,     0,     0,     0,     0,     0,     0,   295,   296,
     252,   253,   254,   255,   256,   257,     0,   258,     0,   259,
       0,     0,     0,     0,    30,    31,     0,    32,    33,     0,
       0,    34,     0,     0,     0,     0,   261,   262,   263,     0,
      37,     0,     0,   264,    39,     0,     0,     0,     0,     0,
       0,     0,   265,    41,   266,     0,    43,     0,   267,   268,
     269,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,   270,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,   271,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,   272,   273,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
     274,    80,     0,    81,    82,    83,     0,   275,     0,    84,
       0,     0,    85,    86,    87,   276,     0,    88,    89,   277,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   278,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   279,     0,     0,     0,     0,   126,
       0,     0,   280,   128,   129,   130,   131,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   293,
     294,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,     0,     0,
       0,     0,     0,     0,     0,     0,   295,   296,   252,   253,
     254,   255,   256,   257,     0,   258,     0,   489,     0,     0,
       0,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,   261,   262,   263,     0,    37,     0,
       0,   264,    39,     0,     0,     0,     0,     0,     0,     0,
     265,    41,   266,     0,    43,     0,   267,   268,   269,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,   271,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,   273,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,   274,    80,
       0,    81,    82,    83,     5,   275,     0,    84,     0,     0,
      85,    86,    87,   276,     0,    88,    89,   277,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     7,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   279,     0,     0,     0,     0,   126,     0,     0,
     280,   128,   129,   130,   131,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   293,   294,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,     0,     0,     0,     0,
       0,     0,     0,     0,   295,   296,   252,   253,   254,   255,
     256,   257,     0,   258,     0,   355,     0,     0,     0,     0,
      30,    31,     0,    32,    33,     0,     0,    34,     0,     0,
       0,     0,   261,   262,   263,     0,    37,     0,     0,   264,
      39,     0,     0,     0,     0,     0,     0,     0,   265,    41,
     266,     0,    43,     0,   267,   268,   269,    44,     0,     0,
      45,     0,    46,    47,   532,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,   271,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,   273,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,   274,    80,     0,    81,
      82,    83,     0,   275,     0,    84,     0,     0,    85,    86,
      87,   276,     0,    88,    89,   277,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     279,     0,     0,     0,     0,   126,     0,     0,   280,   128,
     129,   130,   131,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   293,   294,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,     0,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,     0,     0,     0,   533,     0,     0,
       0,     0,   295,   296,   252,   253,   254,   255,   256,   257,
       0,   258,     0,   355,     0,     0,   629,     0,   630,    31,
       0,    32,    33,     0,     0,    34,     0,     0,     0,     0,
     261,   262,   263,     0,    37,     0,     0,   264,    39,     0,
       0,     0,     0,     0,     0,     0,   265,    41,   266,     0,
      43,     0,   267,   268,   269,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,   271,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,   273,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,   274,    80,     0,    81,    82,    83,
       0,   275,     0,    84,     0,   631,    85,    86,    87,   276,
       0,    88,    89,   277,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     0,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,     0,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   279,     0,
       0,     0,     0,   126,     0,     0,   280,   128,   129,   130,
     131,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   293,   294,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,     0,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,     0,     0,     0,     0,     0,     0,     0,     0,
     295,   296,   252,   253,   254,   255,   256,   257,     0,   258,
       0,   355,     0,     0,     0,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,   261,   262,
     263,     0,    37,     0,     0,   264,    39,     0,     0,     0,
       0,     0,     0,     0,   265,    41,   266,     0,    43,     0,
     267,   268,   269,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,   271,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
     273,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,   274,    80,     0,    81,    82,    83,     0,   275,
       0,    84,     0,     0,    85,    86,    87,   276,     0,    88,
      89,   277,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   279,     0,     0,     0,
       0,   126,     0,     0,   280,   128,   129,   130,   131,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,     0,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,     0,     0,   356,     0,     0,     0,     0,   295,   296,
     252,   253,   254,   255,   256,   257,     0,   258,     0,   355,
     538,     0,     0,     0,    30,    31,     0,    32,    33,     0,
       0,    34,     0,     0,     0,     0,   261,   262,   263,     0,
      37,     0,     0,   264,    39,     0,     0,     0,     0,     0,
       0,     0,   265,    41,   266,     0,    43,     0,   267,   268,
     269,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,     0,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,   271,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,     0,   273,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
     274,    80,     0,    81,    82,    83,     0,   275,     0,    84,
       0,     0,    85,    86,    87,   276,     0,    88,    89,   277,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   103,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   279,     0,     0,     0,     0,   126,
       0,     0,   280,   128,   129,   130,   131,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   293,
     294,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,     0,     0,
       0,     0,     0,     0,     0,     0,   295,   296,   252,   253,
     254,   255,   256,   257,     0,   258,     0,   355,     0,     0,
       0,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,   261,   262,   263,     0,    37,     0,
       0,   264,    39,     0,     0,     0,     0,     0,     0,     0,
     265,    41,   266,     0,    43,     0,   267,   268,   269,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,   271,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,   273,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,   274,    80,
       0,    81,    82,    83,     0,   275,     0,    84,     0,     0,
      85,    86,    87,   276,     0,    88,    89,   277,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   279,     0,     0,     0,     0,   126,     0,     0,
     280,   128,   129,   130,   131,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   293,   294,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,     0,     0,     0,   540,
       0,     0,     0,     0,   295,   296,   252,   253,   254,   255,
     256,   257,     0,   258,     0,   355,   602,     0,     0,     0,
      30,    31,     0,    32,    33,     0,     0,    34,     0,     0,
       0,     0,   261,   262,   263,     0,    37,     0,     0,   264,
      39,     0,     0,     0,     0,     0,     0,     0,   265,    41,
     266,     0,    43,     0,   267,   268,   269,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,   271,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,   273,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,   274,    80,     0,    81,
      82,    83,     0,   275,     0,    84,     0,     0,    85,    86,
      87,   276,     0,    88,    89,   277,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     279,     0,     0,     0,     0,   126,     0,     0,   280,   128,
     129,   130,   131,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   293,   294,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,     0,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,     0,     0,     0,     0,     0,     0,
       0,     0,   295,   296,   252,   253,   254,   255,   256,   257,
       0,   258,     0,   355,   641,     0,     0,     0,    30,    31,
       0,    32,    33,     0,     0,    34,     0,     0,     0,     0,
     261,   262,   263,     0,    37,     0,     0,   264,    39,     0,
       0,     0,     0,     0,     0,     0,   265,    41,   266,     0,
      43,     0,   267,   268,   269,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,   271,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,   273,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,   274,    80,     0,    81,    82,    83,
       0,   275,     0,    84,     0,     0,    85,    86,    87,   276,
       0,    88,    89,   277,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     0,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,     0,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   279,     0,
       0,     0,     0,   126,     0,     0,   280,   128,   129,   130,
     131,   281,   282,   283,   284,   285,   286,   287,   288,   289,
     290,   291,   292,   293,   294,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,     0,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,     0,     0,     0,     0,     0,     0,     0,     0,
     295,   296,   252,   253,   254,   255,   256,   257,     0,   258,
       0,  1282,  1283,     0,     0,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,   261,   262,
     263,     0,    37,     0,     0,   264,    39,     0,     0,     0,
       0,     0,     0,     0,   265,    41,   266,     0,    43,     0,
     267,   268,   269,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,   271,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
     273,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,   274,    80,     0,    81,    82,    83,     0,   275,
       0,    84,     0,     0,    85,    86,    87,   276,     0,    88,
      89,   277,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   279,     0,     0,     0,
       0,   126,     0,     0,   280,   128,   129,   130,   131,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,     0,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,     0,     0,     0,     0,     0,     0,     0,   295,   296,
     252,   253,   254,   255,   256,   257,     0,   258,     0,   355,
       0,     0,     0,     0,    30,    31,     0,    32,    33,     0,
       0,    34,     0,     0,     0,     0,   261,   262,   263,     0,
      37,     0,     0,   264,    39,     0,     0,     0,     0,     0,
       0,     0,   265,    41,   266,     0,    43,     0,   267,   268,
     269,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,     0,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,   271,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,     0,   273,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
     274,    80,     0,    81,    82,    83,     0,   275,     0,    84,
       0,     0,    85,    86,    87,   276,     0,    88,    89,   277,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   103,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   279,     0,     0,     0,     0,   126,
       0,     0,   280,   128,   129,   130,   131,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   293,
     294,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,     0,     0,
       0,     0,     0,     0,     0,     0,   295,   296,   252,   253,
     254,   255,   256,   257,     0,   258,     0,   355,     0,     0,
       0,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,   261,   262,   263,     0,    37,     0,
       0,   264,    39,     0,     0,     0,     0,     0,     0,     0,
     265,    41,   266,     0,    43,     0,   267,   268,   269,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,   271,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,   273,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,   274,    80,
       0,    81,    82,    83,     0,   275,     0,    84,     0,     0,
      85,    86,    87,   276,     0,    88,    89,   277,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   279,     0,     0,     0,     0,   126,     0,     0,
     280,   128,   129,   130,   131,   281,   282,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   293,   294,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   566,   567,   568,   177,   178,
     179,   180,   181,   182,   183,   184,     0,     0,     0,   252,
       0,     0,     0,     0,   295,   296,   677,     0,   810,     0,
       0,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,     0,    35,    36,     0,    37,
       0,     0,    38,    39,     0,     0,     0,     0,     0,     0,
       0,    40,    41,    42,     0,    43,     0,     0,     0,     0,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,     0,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,     0,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,     0,    67,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,     0,
      80,     0,    81,    82,    83,     5,     0,     0,    84,     0,
       0,    85,    86,    87,     0,     0,    88,    89,    90,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     7,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   103,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   679,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   125,     0,     0,     0,     0,   126,     0,
     680,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,     0,   137,   138,   139,   140,   141,   142,   143,   144,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   252,     0,     0,
       0,     0,     0,   185,   677,     0,   678,     0,     0,     0,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   679,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,   680,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   252,     0,     0,     0,     0,
       0,   185,     0,     0,   678,     0,     0,     0,     0,    30,
      31,     0,    32,    33,     0,     0,    34,     0,     0,     0,
       0,     0,    35,    36,     0,    37,     0,     0,    38,    39,
       0,     0,     0,     0,     0,     0,     0,    40,    41,    42,
       0,    43,     0,     0,     0,     0,    44,     0,     0,    45,
       0,    46,    47,     0,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,     0,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,    67,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,     0,    80,     0,    81,    82,
      83,     0,     0,     0,    84,     0,     0,    85,    86,    87,
       0,     0,    88,    89,    90,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     0,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   679,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   125,
       0,     0,     0,     0,   126,     0,   680,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,   137,   138,
     139,   140,   141,   142,   143,   144,     0,   145,   146,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
      29,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,     0,    30,    31,     0,    32,    33,   185,
       0,    34,     0,     0,     0,     0,     0,    35,    36,     0,
      37,     0,     0,    38,    39,     0,     0,     0,     0,     0,
       0,     0,    40,    41,    42,     0,    43,     0,     0,     0,
       0,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,     0,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,     0,    67,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
       0,    80,     0,    81,    82,    83,     0,     0,     0,    84,
       0,     0,    85,    86,    87,     0,     0,    88,    89,    90,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   103,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   125,     0,     0,     0,     0,   126,
       0,     0,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,     0,   137,   138,   139,   140,   141,   142,   143,
     144,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   252,     0,
       0,     0,     0,     0,   185,     0,     0,     0,     0,   644,
       0,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,     0,    35,    36,     0,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     0,     0,
      40,    41,    42,     0,    43,     0,     0,     0,     0,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,    67,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,     0,    80,
       0,    81,    82,    83,     0,     0,     0,    84,     0,     0,
      85,    86,    87,     0,     0,    88,    89,    90,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,     0,     0,     0,     0,   126,     0,     0,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,   137,   138,   139,   140,   141,   142,   143,   144,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,   252,     0,     0,   645,
       0,     0,     0,     0,     0,     0,     0,   912,     0,     0,
      30,    31,     0,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     0,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,     0,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,     0,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,    29,     0,     0,   913,     0,     0,
       0,     0,     0,     0,     0,   644,     0,     0,    30,    31,
       0,    32,    33,     0,     0,    34,     0,     0,     0,     0,
       0,    35,    36,     0,    37,     0,     0,    38,    39,     0,
       0,     0,     0,     0,     0,     0,    40,    41,    42,     0,
      43,     0,     0,     0,     0,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,     0,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,    67,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,     0,    80,     0,    81,    82,    83,
       0,     0,     0,    84,     0,     0,    85,    86,    87,     0,
       0,    88,    89,    90,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     0,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,     0,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   125,     0,
       0,     0,     0,   126,     0,     0,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,     0,   137,   138,   139,
     140,   141,   142,   143,   144,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,     0,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,    29,     0,     0,   645,     0,     0,     0,     0,
       0,     0,     0,   912,     0,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,    29,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,    30,    31,   913,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,    29,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,    30,    31,  1004,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,   252,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,    30,    31,  1064,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,    29,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,    30,    31,  1068,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,    29,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,    30,    31,  1133,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,    29,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,    30,    31,  1135,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,    29,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,    30,    31,  1194,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,   648,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,    30,    31,  1068,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,   262,
     263,     0,    37,     0,     0,   264,    39,     0,     0,     0,
       0,     0,     0,     0,   265,    41,   266,     0,    43,     0,
     267,   268,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,   271,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
     273,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,   274,    80,     0,    81,    82,    83,     0,   275,
       0,    84,     0,     0,    85,    86,    87,   276,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   279,     0,     0,     0,
       0,   126,     0,     0,   280,   128,   129,   130,   131,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,     0,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
      29,   253,   254,   255,   256,   257,     0,   258,     0,     0,
       0,     0,     0,     0,    30,    31,     0,    32,    33,     0,
       0,    34,     0,     0,     0,     0,     0,    35,    36,     0,
      37,     0,     0,    38,    39,     0,     0,     0,     0,     0,
       0,     0,    40,    41,    42,     0,    43,     0,     0,     0,
     269,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,     0,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,     0,    67,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
       0,    80,     0,    81,    82,    83,     0,     0,     0,    84,
       0,     0,    85,    86,    87,     0,     0,    88,    89,   277,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   103,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   125,     0,     0,     0,     0,   126,
       0,     0,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,     0,   137,   138,   139,   140,   141,   142,   143,
     144,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,    29,     0,
       0,     0,     0,     0,     0,     0,     0,   482,     0,     0,
       0,     0,    30,    31,   476,    32,    33,     0,     0,    34,
       0,     0,     0,     0,     0,    35,    36,     0,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     0,     0,
      40,    41,    42,     0,    43,     0,     0,     0,     0,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,    67,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,     0,    80,
       0,    81,    82,    83,     5,     0,     0,    84,     0,     0,
      85,    86,    87,     0,     0,    88,    89,    90,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     7,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     483,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,     0,     0,     0,     0,   126,     0,     0,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,   137,   138,   139,   140,   141,   142,   143,   144,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,    29,     0,     0,     0,
       0,     0,     0,     0,     0,     1,     0,     0,     0,     0,
      30,    31,     0,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     5,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     7,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,     0,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,     0,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,   252,     0,     0,     0,     0,     0,
       0,     0,     0,     1,     0,     0,     0,     0,    30,    31,
       0,    32,    33,     0,     0,    34,     0,     0,     0,     0,
       0,    35,    36,     0,    37,     0,     0,    38,    39,     0,
       0,     0,     0,     0,     0,     0,    40,    41,    42,     0,
      43,     0,     0,     0,     0,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,     0,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,    67,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,     0,    80,     0,    81,    82,    83,
       5,     0,     0,    84,     0,     0,    85,    86,    87,     0,
       0,    88,    89,    90,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     7,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,     0,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   125,     0,
       0,     0,     0,   126,     0,     0,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,     0,   137,   138,   139,
     140,   141,   142,   143,   144,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,     0,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,    29,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   345,     0,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,     0,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
      29,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   470,     0,     0,    30,    31,     0,    32,    33,     0,
       0,    34,     0,     0,     0,     0,     0,    35,    36,     0,
      37,     0,     0,    38,    39,     0,     0,     0,     0,     0,
       0,     0,    40,    41,    42,     0,    43,     0,     0,     0,
       0,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,     0,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,     0,    67,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
       0,    80,     0,    81,    82,    83,     0,     0,     0,    84,
       0,     0,    85,    86,    87,     0,     0,    88,    89,    90,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   103,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   125,     0,     0,     0,     0,   126,
       0,     0,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,     0,   137,   138,   139,   140,   141,   142,   143,
     144,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,    29,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,     0,    30,
      31,   476,    32,    33,     0,     0,    34,     0,     0,     0,
       0,     0,    35,    36,     0,    37,     0,     0,    38,    39,
       0,     0,     0,     0,     0,     0,     0,    40,    41,    42,
       0,    43,     0,     0,     0,     0,    44,     0,     0,    45,
       0,    46,    47,     0,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,     0,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,    67,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,     0,    80,     0,    81,    82,
      83,     0,     0,     0,    84,     0,     0,    85,    86,    87,
       0,     0,    88,    89,    90,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     0,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,     0,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   125,
       0,     0,     0,     0,   126,     0,     0,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,   137,   138,
     139,   140,   141,   142,   143,   144,     0,   145,   146,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
       0,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,    29,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   657,     0,     0,    30,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,     0,
      35,    36,     0,    37,     0,     0,    38,    39,     0,     0,
       0,     0,     0,     0,     0,    40,    41,    42,     0,    43,
       0,     0,     0,     0,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,     0,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,    67,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,     0,    80,     0,    81,    82,    83,     0,
       0,     0,    84,     0,     0,    85,    86,    87,     0,     0,
      88,    89,    90,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,     0,     0,
       0,     0,   126,     0,     0,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,   137,   138,   139,   140,
     141,   142,   143,   144,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,     0,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,    29,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   787,     0,     0,    30,    31,     0,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,    35,    36,
       0,    37,     0,     0,    38,    39,     0,     0,     0,     0,
       0,     0,     0,    40,    41,    42,     0,    43,     0,     0,
       0,     0,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,    67,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,     0,    80,     0,    81,    82,    83,     0,     0,     0,
      84,     0,     0,    85,    86,    87,     0,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   125,     0,     0,     0,     0,
     126,     0,     0,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,   137,   138,   139,   140,   141,   142,
     143,   144,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,     0,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,    29,
       0,     0,     0,     0,     0,     0,     0,     0,   905,     0,
       0,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,     0,    35,    36,     0,    37,
       0,     0,    38,    39,     0,     0,     0,     0,     0,     0,
       0,    40,    41,    42,     0,    43,     0,     0,     0,     0,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,     0,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,     0,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,     0,    67,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,     0,
      80,     0,    81,    82,    83,     0,     0,     0,    84,     0,
       0,    85,    86,    87,     0,     0,    88,    89,    90,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     0,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   103,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   125,     0,     0,     0,     0,   126,     0,
       0,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,     0,   137,   138,   139,   140,   141,   142,   143,   144,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   252,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   345,     0,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   252,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   657,     0,     0,    30,
      31,     0,    32,    33,     0,     0,    34,     0,     0,     0,
       0,     0,    35,    36,     0,    37,     0,     0,    38,    39,
       0,     0,     0,     0,     0,     0,     0,    40,    41,    42,
       0,    43,     0,     0,     0,     0,    44,     0,     0,    45,
       0,    46,    47,     0,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,     0,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,    67,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,     0,    80,     0,    81,    82,
      83,     0,     0,     0,    84,     0,     0,    85,    86,    87,
       0,     0,    88,    89,    90,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     0,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,     0,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   125,
       0,     0,     0,     0,   126,     0,     0,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,   137,   138,
     139,   140,   141,   142,   143,   144,     0,   145,   146,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
       0,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,    29,     0,     0,     0,     0,     0,     0,
       0,     0,  1252,     0,     0,     0,     0,    30,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,     0,
      35,    36,     0,    37,     0,     0,    38,    39,     0,     0,
       0,     0,     0,     0,     0,    40,    41,    42,     0,    43,
       0,     0,     0,     0,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,     0,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,    67,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,     0,    80,     0,    81,    82,    83,     0,
       0,     0,    84,     0,     0,    85,    86,    87,     0,     0,
      88,    89,    90,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,     0,     0,
       0,     0,   126,     0,     0,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,   137,   138,   139,   140,
     141,   142,   143,   144,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,    29,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,     0,    35,    36,     0,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     0,     0,
      40,    41,    42,     0,    43,     0,     0,     0,     0,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,    67,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,   199,    79,     0,    80,
       0,    81,    82,    83,     0,     0,     0,    84,     0,     0,
      85,    86,    87,     0,     0,    88,    89,    90,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,     0,     0,     0,     0,   126,     0,     0,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,   137,   138,   139,   140,   141,   142,   143,   144,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,    29,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,     0,    30,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,     0,
      35,    36,     0,    37,     0,     0,    38,    39,     0,     0,
       0,     0,     0,     0,     0,    40,    41,    42,     0,    43,
       0,     0,     0,     0,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,     0,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,    67,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,     0,    80,     0,    81,    82,    83,     0,
       0,     0,    84,     0,     0,    85,    86,    87,     0,     0,
      88,    89,    90,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,     0,     0,
       0,     0,   126,     0,     0,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,   137,   138,   139,   140,
     141,   142,   143,   144,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,   252,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,     0,    35,    36,     0,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     0,     0,
      40,    41,    42,     0,    43,     0,     0,     0,     0,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,    67,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,     0,    80,
       0,    81,    82,    83,     0,     0,     0,    84,     0,     0,
      85,    86,    87,     0,     0,    88,    89,    90,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,     0,     0,     0,     0,   126,     0,     0,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,   137,   138,   139,   140,   141,   142,   143,   144,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,    29,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,     0,    30,   929,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,     0,
      35,    36,     0,    37,     0,     0,    38,    39,     0,     0,
       0,     0,     0,     0,     0,    40,    41,    42,     0,    43,
       0,     0,     0,     0,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,     0,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,    67,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,     0,    80,     0,    81,    82,    83,     0,
       0,     0,    84,     0,     0,    85,    86,    87,     0,     0,
      88,    89,    90,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,     0,     0,
       0,     0,   126,     0,     0,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,   137,   138,   139,   140,
     141,   142,   143,   144,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,    29,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,     0,    35,    36,     0,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     0,     0,
      40,    41,    42,     0,    43,     0,     0,     0,     0,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,    67,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,     0,    80,
       0,    81,    82,    83,     0,     0,     0,    84,     0,     0,
      85,    86,    87,     0,     0,    88,    89,    90,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,     0,     0,     0,     0,   126,     0,     0,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,   137,   138,   139,   140,   141,   142,   143,   144,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   999,   172,  1196,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,     0,     0,    31,     0,
      32,     0,     0,     0,    34,     0,     0,     0,     0,     0,
      35,     0,     0,    37,     0,     0,    38,    39,     0,     0,
       0,     0,     0,     0,     0,    40,    41,     0,     0,     0,
       0,     0,     0,     0,    44,     0,     0,    45,     0,     0,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,     0,     0,    59,     0,
       0,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,    67,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,     0,    74,    75,     0,     0,    77,
       0,    78,    79,     0,    80,     0,     0,    82,    83,     0,
       0,     0,    84,     0,     0,     0,     0,     0,     0,     0,
      88,    89,    90,    91,    92,     0,     0,     0,     0,    93,
       0,     0,     0,     0,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,     0,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   126,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,     0,   173,
     174,   175,   176,     0,     0,     0,     0,     0,   182,   183,
     184
};

static const short int yycheck[] =
{
       2,   251,   259,   270,   215,     7,     8,     9,   203,   204,
      13,     1,   226,   260,     2,   242,   443,   365,   203,   204,
       8,     9,   422,   423,   857,   482,   453,   270,   467,   491,
      13,    13,    13,    74,    13,    13,   352,   491,    13,   208,
      13,   210,   211,    13,   466,   214,   394,   359,    13,    13,
     366,   367,    35,    35,    35,    38,    38,    38,   406,    13,
      35,   409,    35,    38,    13,    38,   414,   415,    74,    90,
      35,    35,    13,    38,    38,   118,    13,    13,    12,    35,
      13,    35,    38,    39,    38,    16,   119,    16,   678,   272,
     347,    74,    14,     2,    13,    12,    22,   364,     7,     8,
       9,   348,    35,    13,    35,    38,    35,    38,    20,    38,
      73,    13,    13,    12,    19,    74,   464,     3,    13,    13,
      13,   364,    13,    35,    13,    13,    38,   384,    13,    13,
      35,    13,   379,    38,    13,    13,    13,    13,    12,     8,
      13,    91,    13,    35,    12,    63,    38,   416,    60,    19,
    1186,   295,   296,  1200,    12,    81,    13,   419,   420,   421,
     123,   430,     6,    35,    68,     9,    38,    29,   353,    35,
      35,    97,    38,    38,    72,    35,   443,   493,    38,   127,
     427,   172,    35,   185,   532,    38,   453,   135,     0,    35,
     118,   193,    38,   195,  1230,   622,   198,   199,    35,    35,
     443,    38,    38,   289,   290,   193,    66,   195,    88,    89,
     198,    72,     5,    66,    63,   119,   678,    10,    65,   221,
     810,   149,   139,   225,   491,   282,  1273,   125,   118,   118,
     116,    93,   234,   235,   481,   194,   238,   225,   165,   166,
     139,   110,   283,   286,  1077,    15,   164,   119,   238,   432,
     283,   155,   273,   274,   275,   276,   277,   484,   715,   149,
      86,   183,   119,   197,   125,   157,   110,   166,   525,   259,
       5,   139,   519,   153,    50,    10,   185,   283,   212,    13,
     283,   139,   208,    39,   193,    41,   195,   744,    54,   198,
     199,    61,   166,    14,   463,   557,   166,   284,   166,    74,
     485,    77,   487,   488,   283,   283,   293,   294,   166,   292,
     292,   292,   221,   283,    80,   164,   225,   292,   462,   292,
       6,   283,   285,     9,   287,   234,   235,   292,   292,   238,
     166,    12,   543,   109,   283,   101,   292,   103,   292,   550,
     551,   552,   283,   345,   744,   802,   283,   283,   810,   351,
     254,   353,   814,   792,   816,   622,   813,   359,   820,   292,
      27,   292,   816,   292,   283,   355,   820,   789,   790,   681,
     156,   170,   138,   283,   150,   687,   688,   689,    13,   622,
     292,   283,   283,   695,   165,   701,     5,   292,   283,   283,
     283,    10,   283,   123,   384,   283,    15,   641,   283,   283,
     292,   283,   168,   141,   283,   283,   283,   283,   410,   399,
     283,   678,   283,    19,   416,   939,   838,   283,   283,   158,
     292,   945,   946,   269,   671,    12,   292,   292,   430,    12,
       5,    14,   292,    68,   436,    10,   345,    73,    73,   292,
      19,    28,   351,    22,   353,     4,   292,   674,   436,   243,
     359,    86,   246,   165,   166,   292,   292,   878,   879,   880,
     244,    12,   246,   247,   466,   467,   279,   280,   470,   166,
     472,    58,   258,    12,   476,   937,   690,   263,   264,    12,
     482,   196,   197,   904,   119,    12,   948,   200,   201,   491,
      12,   676,   482,    12,   948,    74,    83,   499,   742,   489,
     173,   174,   411,    48,    12,   749,   750,   175,   176,   177,
       4,     5,     6,     7,     8,    12,    10,    12,   830,    12,
     155,    88,    89,    90,    88,    89,    90,   436,    22,    12,
      75,   166,    88,    89,    90,   170,     4,    82,    88,    89,
      90,   785,    12,   810,   791,    19,   793,    12,    93,   816,
      12,    96,   139,   820,   209,   210,    50,   466,  1015,    53,
     807,   470,    12,   472,    88,    89,    48,   476,    12,   156,
      12,   828,    12,   482,     4,     5,     6,     7,     8,   166,
      10,    48,   491,    88,    89,  1007,  1008,    12,   133,    12,
     499,   178,   179,    75,    16,   842,   273,   274,   275,    12,
      82,     4,     5,     6,     7,     8,    12,    10,    75,    88,
      89,    93,   821,   822,    96,    82,  1038,    12,    12,   254,
     815,    12,    12,    53,    12,    12,    93,    12,   118,    96,
      48,   240,   241,   242,   116,    92,   288,    12,    12,    12,
      12,    12,   644,   887,   888,    14,    12,    50,    12,   116,
      53,   133,    14,    12,   656,   657,   283,    75,   152,   171,
     154,    13,   164,     5,    82,  1013,   133,     5,     4,    39,
     937,    12,   115,     4,    39,    93,   678,   292,    96,   681,
       4,   948,    14,     5,    73,   687,   688,   689,   678,   283,
     166,   180,   180,   695,     5,   199,   242,   259,   116,   115,
      12,   283,   959,    13,    13,    13,    13,    74,   269,    13,
      13,    13,    13,   715,    13,   133,  1138,  1117,    13,     4,
       4,     4,   152,     4,    12,    12,    12,    18,   283,   115,
     732,     8,    12,    12,    12,   644,    12,    12,   122,    39,
      14,   743,   744,    14,   732,  1167,    13,   656,   657,   152,
      14,   154,   278,    14,    19,    12,    12,   198,    77,   198,
      14,  1218,  1009,   255,   181,   176,  1023,   181,     4,   678,
      64,     4,   681,  1121,   265,    13,    13,  1034,   687,   688,
     689,   783,    13,  1131,    13,   787,   695,   789,   790,   958,
     792,  1218,    13,  1193,     4,   797,   234,     5,     5,    81,
     802,   803,    74,     5,     5,     5,   715,   809,   810,     5,
       5,   813,   814,  1275,   816,     4,    14,     4,   820,     5,
     810,  1275,  1222,   732,   814,  1252,   115,   829,   830,    13,
    1287,    93,  1232,    27,  1091,   744,   838,  1094,    79,    12,
       5,    13,   257,    13,     2,    77,   283,    39,   121,     7,
       8,     9,  1063,    13,    13,   210,    13,    13,    13,    13,
    1287,    14,    14,    14,    13,    12,    14,    12,    93,    13,
      13,    89,    14,    19,   783,   285,   211,    14,   787,     4,
     789,   790,     4,   153,   153,    12,     5,   267,   797,    13,
     153,    14,    74,   802,   803,  1295,   256,   123,    14,   115,
     809,   810,     4,   905,   813,   814,   164,   816,     4,   911,
     912,   820,   115,    13,    12,    12,    27,    12,    12,   287,
     829,   830,    12,  1173,    12,   283,    13,    13,     4,   838,
      14,   119,    14,    74,     4,   937,    14,     2,     4,     3,
     192,    16,     7,     8,     9,    22,   948,   153,     4,    89,
      89,     5,   157,    12,    12,   283,     4,    14,    35,   142,
       4,    38,    39,    40,    41,    42,    43,    44,     4,    19,
      13,   239,   153,    12,    89,   281,     4,     4,   213,    64,
      13,   286,     5,     5,     5,    77,    12,     4,    13,    13,
       3,   153,   153,    13,    13,    13,   905,    12,   166,    13,
     134,  1003,   911,   912,    81,  1007,  1008,    13,  1275,  1011,
    1012,   125,   125,  1015,    69,    92,    12,    16,     4,    93,
      97,    74,    93,    13,    93,   249,    93,   185,   937,   134,
      93,  1033,    93,   110,   247,   193,  1038,   195,   250,   948,
     198,   199,   251,     4,   153,   244,   166,   120,   245,   253,
     428,   172,   488,   699,   225,   248,    25,   207,  1060,   250,
     198,  1318,   252,   221,  1066,  1067,   495,   225,  1170,  1287,
    1252,   199,  1060,  1284,   715,  1099,   234,   235,  1244,   488,
     238,   937,  1033,  1015,   492,  1092,  1203,  1142,   431,  1124,
     622,    22,  1052,   608,  1003,   452,   457,  1099,  1007,  1008,
     526,   234,  1011,  1012,    35,   461,  1015,    38,    39,    40,
      41,    42,    43,    44,   759,  1057,   905,  1107,   510,  1193,
     185,  1295,   743,   720,  1033,   968,   729,   476,   193,  1038,
     195,   208,     6,   198,   199,  1137,   517,   383,  1140,   353,
    1142,   375,   792,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      81,  1060,    -1,    -1,    -1,    -1,   221,  1066,  1067,   513,
     225,    92,    -1,    -1,    -1,  1167,    97,    -1,    -1,   234,
     235,    -1,    -1,   238,    -1,    -1,    -1,    -1,    -1,   110,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   345,    -1,    -1,
    1099,   268,    -1,   351,    -1,   353,    -1,    -1,  1200,    -1,
      -1,   359,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   291,   292,  1218,    -1,   149,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1137,  1138,
      -1,  1140,    -1,  1142,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1167,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1273,    -1,  1275,    -1,    -1,    -1,   208,   436,    -1,
     345,    -1,    -1,    -1,    -1,  1287,   351,    -1,   353,    -1,
      -1,  1200,  1282,    -1,   359,    23,    -1,    25,    26,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    34,    -1,   466,  1218,
      -1,    -1,   470,    -1,   472,    -1,    -1,    -1,   476,    -1,
      -1,    -1,    -1,    -1,   482,    53,    -1,    -1,    56,    -1,
      -1,    -1,    -1,   491,    62,    -1,    -1,   268,    -1,    -1,
      -1,   499,    -1,    71,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    84,    85,    -1,    -1,
     291,   292,    -1,    -1,  1273,    -1,  1275,    -1,    -1,    -1,
      -1,   436,    -1,    -1,    -1,    -1,    -1,    -1,  1287,    -1,
     108,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   466,   130,    -1,    -1,   470,    -1,   472,    -1,    -1,
      -1,   476,    -1,    -1,    -1,   143,    -1,   482,    -1,    -1,
      -1,    -1,    -1,   151,   152,    -1,   491,    -1,    -1,    -1,
      -1,   159,   160,    -1,   499,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   644,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   656,   657,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     678,    -1,    -1,   681,    -1,    -1,    -1,    -1,    -1,   687,
     688,   689,    -1,    -1,    -1,    -1,    -1,   695,     2,    -1,
      -1,    -1,    -1,     7,     8,     9,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   715,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   732,    -1,    -1,    -1,    -1,   644,
      -1,    -1,    -1,    -1,    -1,    -1,   744,    -1,    -1,    -1,
      -1,   656,   657,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   678,    -1,    -1,   681,    -1,    -1,    -1,
      -1,    -1,   687,   688,   689,   783,    -1,    -1,    -1,   787,
     695,   789,   790,    -1,    -1,    -1,    -1,    -1,    -1,   797,
      -1,    -1,    -1,    -1,   802,   803,    -1,    -1,    -1,    -1,
     715,   809,   810,    -1,    -1,   813,   814,    -1,   816,    -1,
      -1,    -1,   820,    -1,    -1,    -1,    -1,   732,    -1,    -1,
      -1,   829,   830,    -1,    -1,    -1,    -1,    -1,    -1,   744,
     838,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   783,    -1,
      -1,   185,   787,    -1,   789,   790,    -1,    -1,    -1,   193,
      -1,   195,   797,    -1,   198,   199,    -1,   802,   803,    -1,
      -1,    -1,    -1,    -1,   809,   810,    -1,   905,   813,   814,
      -1,   816,    -1,   911,   912,   820,    -1,   221,    -1,    -1,
      -1,   225,    -1,    -1,   829,   830,    -1,    -1,    -1,    -1,
     234,   235,    -1,   838,   238,    -1,    -1,    -1,    -1,   937,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     948,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     905,    -1,    -1,    -1,    -1,  1003,   911,   912,    -1,  1007,
    1008,    -1,    -1,  1011,  1012,    -1,    -1,  1015,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   937,    -1,    -1,  1033,    -1,    -1,    -1,    -1,
    1038,   345,    -1,   948,    -1,    -1,    -1,   351,    -1,   353,
      -1,    -1,    -1,    -1,    -1,   359,    -1,    -1,    -1,    -1,
      -1,    -1,  1060,    -1,    -1,    -1,    -1,    -1,  1066,  1067,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1003,    -1,
      -1,  1099,  1007,  1008,    -1,    -1,  1011,  1012,    -1,    -1,
    1015,    -1,    -1,    -1,    -1,    -1,   218,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1033,   231,
      -1,    -1,   436,  1038,    -1,    -1,    -1,    -1,    -1,  1137,
      -1,    -1,  1140,   245,  1142,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1060,    -1,   259,    -1,   261,
      -1,  1066,  1067,    -1,    -1,    -1,   470,    -1,   472,  1167,
     272,    -1,   476,    -1,    -1,    -1,    -1,    -1,   482,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1099,   499,    -1,    -1,    -1,    -1,
      -1,    -1,  1200,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1218,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1137,    -1,    -1,  1140,    -1,  1142,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   347,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   360,    -1,
      -1,    -1,  1167,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1273,    -1,  1275,    -1,    -1,
      -1,    -1,   384,    -1,    -1,    -1,    -1,    -1,    -1,  1287,
     392,    -1,    -1,   395,   396,  1200,    -1,    -1,    -1,    -1,
     402,    -1,    -1,   405,    -1,    -1,   408,    -1,   410,    -1,
     412,   413,    -1,  1218,   416,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   424,    -1,    -1,    -1,   428,    -1,   430,   431,
     432,    -1,   434,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     644,    -1,    -1,   445,    -1,    -1,   448,    -1,    -1,   451,
      -1,    -1,   656,   657,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   465,    -1,    -1,    -1,    -1,  1273,    -1,
    1275,    -1,    -1,    -1,    -1,    -1,    -1,   681,    -1,    -1,
      -1,    -1,  1287,   687,   688,   689,    -1,    -1,    -1,    -1,
     492,   695,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   510,    -1,
      -1,   715,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   524,   525,    -1,    -1,    -1,    -1,   732,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     744,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   783,
      -1,    -1,    -1,   787,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   797,    -1,    -1,    -1,    -1,   802,   803,
      -1,    -1,    -1,    -1,    -1,   809,    -1,    -1,    -1,   813,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   621,
      -1,   623,    -1,   625,    -1,   829,   830,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   640,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   699,    -1,    -1,
     702,   905,    -1,    -1,    -1,    -1,    -1,   911,   912,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   726,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   737,   738,    -1,    -1,    -1,
      -1,   743,    -1,   745,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   761,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   773,    -1,   775,    -1,    -1,    -1,   779,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1003,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1011,  1012,    -1,
      -1,  1015,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   828,    -1,    -1,  1033,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1060,    -1,   860,   861,
      -1,    -1,  1066,  1067,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   885,    -1,    -1,    -1,    -1,   890,    -1,
      -1,    -1,    -1,    -1,    -1,  1099,    -1,    -1,   900,   901,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1137,  1138,    -1,  1140,    -1,  1142,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   959,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1200,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1218,    -1,    -1,    -1,    -1,    -1,
      -1,  1023,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     3,  1034,    -1,    -1,    -1,    -1,  1039,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    17,    18,    19,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    35,    36,    37,    38,    -1,    -1,  1273,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,
      -1,    -1,    54,  1287,    -1,    57,    -1,    59,    60,  1091,
      62,    -1,  1094,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,    -1,   134,    -1,   136,   137,   138,    -1,  1170,    -1,
     142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,  1244,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,    -1,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
       3,     4,     5,     6,     7,     8,  1318,    10,    -1,    12,
     292,    -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,    52,
      53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    96,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,   110,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
     133,   134,    -1,   136,   137,   138,   139,   140,    -1,   142,
      -1,    -1,   145,   146,   147,   148,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,   166,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,   225,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,
       5,     6,     7,     8,    -1,    10,    -1,    12,    13,    -1,
      -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    51,    52,    53,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    96,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,   110,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,   133,   134,
      -1,   136,   137,   138,    -1,   140,    -1,   142,    -1,    -1,
     145,   146,   147,   148,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   289,   290,     3,     4,     5,     6,
       7,     8,    -1,    10,    -1,    12,    -1,    -1,    -1,    -1,
      17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    29,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    51,    52,    53,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    78,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    96,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,   133,   134,    -1,   136,
     137,   138,    -1,   140,    -1,   142,    -1,    -1,   145,   146,
     147,   148,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
     237,   238,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,    -1,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   289,   290,     3,     4,     5,     6,     7,     8,
      -1,    10,    -1,    12,    13,    -1,    -1,    -1,    17,    18,
      -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      29,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    51,    52,    53,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    96,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,   133,   134,    -1,   136,   137,   138,
     139,   140,    -1,   142,    -1,    -1,   145,   146,   147,   148,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,   166,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,    -1,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     289,   290,     3,     4,     5,     6,     7,     8,    -1,    10,
      -1,    12,    13,    -1,    -1,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    29,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      51,    52,    53,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,   133,   134,    -1,   136,   137,   138,   139,   140,
      -1,   142,    -1,    -1,   145,   146,   147,   148,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,   166,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,   225,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,    -1,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   289,   290,
       3,     4,     5,     6,     7,     8,    -1,    10,    -1,    12,
      -1,    -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,    52,
      53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    96,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,   110,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
     133,   134,    -1,   136,   137,   138,    -1,   140,    -1,   142,
      -1,    -1,   145,   146,   147,   148,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,   225,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,
       5,     6,     7,     8,    -1,    10,    -1,    12,    -1,    -1,
      -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    51,    52,    53,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    96,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,   133,   134,
      -1,   136,   137,   138,   139,   140,    -1,   142,    -1,    -1,
     145,   146,   147,   148,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,   166,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   289,   290,     3,     4,     5,     6,
       7,     8,    -1,    10,    -1,    12,    -1,    -1,    -1,    -1,
      17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    29,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    51,    52,    53,    54,    -1,    -1,
      57,    -1,    59,    60,    61,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    96,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,   133,   134,    -1,   136,
     137,   138,    -1,   140,    -1,   142,    -1,    -1,   145,   146,
     147,   148,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,    -1,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,    -1,    -1,    -1,   284,    -1,    -1,
      -1,    -1,   289,   290,     3,     4,     5,     6,     7,     8,
      -1,    10,    -1,    12,    -1,    -1,    15,    -1,    17,    18,
      -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      29,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    51,    52,    53,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    96,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,   133,   134,    -1,   136,   137,   138,
      -1,   140,    -1,   142,    -1,   144,   145,   146,   147,   148,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,    -1,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     289,   290,     3,     4,     5,     6,     7,     8,    -1,    10,
      -1,    12,    -1,    -1,    -1,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    29,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      51,    52,    53,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,   133,   134,    -1,   136,   137,   138,    -1,   140,
      -1,   142,    -1,    -1,   145,   146,   147,   148,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,   225,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,    -1,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    -1,    -1,   284,    -1,    -1,    -1,    -1,   289,   290,
       3,     4,     5,     6,     7,     8,    -1,    10,    -1,    12,
      13,    -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,    52,
      53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    96,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
     133,   134,    -1,   136,   137,   138,    -1,   140,    -1,   142,
      -1,    -1,   145,   146,   147,   148,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,   225,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,
       5,     6,     7,     8,    -1,    10,    -1,    12,    -1,    -1,
      -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    51,    52,    53,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    96,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,   133,   134,
      -1,   136,   137,   138,    -1,   140,    -1,   142,    -1,    -1,
     145,   146,   147,   148,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,    -1,    -1,    -1,   284,
      -1,    -1,    -1,    -1,   289,   290,     3,     4,     5,     6,
       7,     8,    -1,    10,    -1,    12,    13,    -1,    -1,    -1,
      17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    29,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    51,    52,    53,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    96,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,   133,   134,    -1,   136,
     137,   138,    -1,   140,    -1,   142,    -1,    -1,   145,   146,
     147,   148,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,    -1,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   289,   290,     3,     4,     5,     6,     7,     8,
      -1,    10,    -1,    12,    13,    -1,    -1,    -1,    17,    18,
      -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      29,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    51,    52,    53,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    96,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,   133,   134,    -1,   136,   137,   138,
      -1,   140,    -1,   142,    -1,    -1,   145,   146,   147,   148,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,    -1,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     289,   290,     3,     4,     5,     6,     7,     8,    -1,    10,
      -1,    12,    13,    -1,    -1,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    29,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      51,    52,    53,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,   133,   134,    -1,   136,   137,   138,    -1,   140,
      -1,   142,    -1,    -1,   145,   146,   147,   148,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,   225,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,    -1,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   289,   290,
       3,     4,     5,     6,     7,     8,    -1,    10,    -1,    12,
      -1,    -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,    52,
      53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    96,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
     133,   134,    -1,   136,   137,   138,    -1,   140,    -1,   142,
      -1,    -1,   145,   146,   147,   148,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,   225,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,
       5,     6,     7,     8,    -1,    10,    -1,    12,    -1,    -1,
      -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    51,    52,    53,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    96,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,   133,   134,
      -1,   136,   137,   138,    -1,   140,    -1,   142,    -1,    -1,
     145,   146,   147,   148,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,    -1,    -1,    -1,     3,
      -1,    -1,    -1,    -1,   289,   290,    10,    -1,    12,    -1,
      -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    -1,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,    -1,
     134,    -1,   136,   137,   138,   139,    -1,    -1,   142,    -1,
      -1,   145,   146,   147,    -1,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,   166,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
     214,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,     3,    -1,    -1,
      -1,    -1,    -1,   287,    10,    -1,    12,    -1,    -1,    -1,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,   214,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,     3,    -1,    -1,    -1,    -1,
      -1,   287,    -1,    -1,    12,    -1,    -1,    -1,    -1,    17,
      18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,
      -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,    -1,   134,    -1,   136,   137,
     138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,
      -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,    -1,
      -1,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
       3,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,    -1,    17,    18,    -1,    20,    21,   287,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,
      -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    -1,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
      -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,
      -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,    -1,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,     3,    -1,
      -1,    -1,    -1,    -1,   287,    -1,    -1,    -1,    -1,    14,
      -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,    -1,   134,
      -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,
     145,   146,   147,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
      -1,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,     3,    -1,    -1,   284,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    14,    -1,    -1,
      17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,    -1,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,     3,    -1,    -1,   284,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    14,    -1,    -1,    17,    18,
      -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,    -1,   134,    -1,   136,   137,   138,
      -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,    -1,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,    -1,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,     3,    -1,    -1,   284,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    -1,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,     3,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    17,    18,   284,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,     3,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    17,    18,   284,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,     3,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    17,    18,   284,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,     3,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    17,    18,   284,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,     3,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    17,    18,   284,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,     3,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    17,    18,   284,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,     3,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    17,    18,   284,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,     3,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    17,    18,   284,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      51,    52,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,   133,   134,    -1,   136,   137,   138,    -1,   140,
      -1,   142,    -1,    -1,   145,   146,   147,   148,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,   225,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,    -1,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
       3,     4,     5,     6,     7,     8,    -1,    10,    -1,    -1,
      -1,    -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,
      53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    -1,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
      -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,
      -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,    -1,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,     3,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    12,    -1,    -1,
      -1,    -1,    17,    18,    19,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,    -1,   134,
      -1,   136,   137,   138,   139,    -1,    -1,   142,    -1,    -1,
     145,   146,   147,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,   166,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
      -1,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,     3,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    12,    -1,    -1,    -1,    -1,
      17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,   139,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,   166,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,    -1,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,     3,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    12,    -1,    -1,    -1,    -1,    17,    18,
      -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,    -1,   134,    -1,   136,   137,   138,
     139,    -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,   166,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,    -1,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,    -1,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,     3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    -1,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,    -1,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
       3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    14,    -1,    -1,    17,    18,    -1,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,
      -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    -1,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
      -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,
      -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,    -1,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,     3,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,    -1,    17,
      18,    19,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,
      -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,    -1,   134,    -1,   136,   137,
     138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,
      -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,    -1,
      -1,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
      -1,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,     3,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    14,    -1,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,    -1,   134,    -1,   136,   137,   138,    -1,
      -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,    -1,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,     3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    14,    -1,    -1,    17,    18,    -1,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,
      -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,
     142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,    -1,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,     3,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    12,    -1,
      -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    -1,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,    -1,
     134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,
      -1,   145,   146,   147,    -1,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,     3,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    14,    -1,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,     3,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    14,    -1,    -1,    17,
      18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,
      -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,    -1,   134,    -1,   136,   137,
     138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,
      -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,    -1,
      -1,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
      -1,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,     3,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    12,    -1,    -1,    -1,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,    -1,   134,    -1,   136,   137,   138,    -1,
      -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,     3,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,    -1,   134,
      -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,
     145,   146,   147,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
      -1,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,     3,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,    -1,   134,    -1,   136,   137,   138,    -1,
      -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,     3,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,    -1,   134,
      -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,
     145,   146,   147,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
      -1,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,     3,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,    -1,   134,    -1,   136,   137,   138,    -1,
      -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,     3,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,    -1,   134,
      -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,
     145,   146,   147,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
      -1,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,     3,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,    -1,    -1,    18,    -1,
      20,    -1,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,
      30,    -1,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    -1,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,    -1,
      -1,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,    -1,   125,   126,    -1,    -1,   129,
      -1,   131,   132,    -1,   134,    -1,    -1,   137,   138,    -1,
      -1,    -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,    -1,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,    -1,   269,
     270,   271,   272,    -1,    -1,    -1,    -1,    -1,   278,   279,
     280
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const unsigned short int yystos[] =
{
       0,    12,    28,    58,    83,   139,   156,   166,   178,   179,
     296,   297,   298,   299,   304,   305,   309,   311,   312,   317,
     318,   319,   320,   321,   327,   347,   502,   503,   318,     3,
      17,    18,    20,    21,    24,    30,    31,    33,    36,    37,
      45,    46,    47,    49,    54,    57,    59,    60,    62,    69,
      72,    76,    80,    87,    88,    89,    90,    94,    95,    98,
      99,   100,   101,   102,   103,   104,   107,   111,   113,   114,
     115,   117,   120,   124,   125,   126,   128,   129,   131,   132,
     134,   136,   137,   138,   142,   145,   146,   147,   150,   151,
     152,   153,   154,   159,   162,   163,   167,   168,   169,   177,
     179,   180,   181,   182,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   194,   195,   196,   197,   198,   200,   201,
     202,   203,   204,   205,   206,   207,   212,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   226,   227,   228,
     229,   230,   231,   232,   233,   235,   236,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   255,   256,   258,   259,   260,   261,   262,   263,
     264,   266,   267,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   287,   303,   375,   437,   446,
     478,   484,   486,    74,   194,    91,   172,   487,   487,   131,
     348,   349,   484,   375,   375,     0,    65,   282,    68,   119,
     155,   254,   441,   442,    86,   319,    13,   484,    12,   166,
     499,    14,   313,   314,   375,    74,   375,     5,    10,    15,
      61,   355,   314,   348,   283,    12,   350,   351,    12,   317,
     350,   317,   350,   297,   355,    27,   355,   355,   170,   494,
     355,   441,     3,     4,     5,     6,     7,     8,    10,    12,
      13,    29,    30,    31,    36,    45,    47,    51,    52,    53,
      70,    96,   110,   111,   133,   140,   148,   152,   182,   207,
     215,   220,   221,   222,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,   233,   289,   290,   300,   301,   302,
     322,   323,   326,   374,   376,   392,   393,   394,   395,   396,
     397,   398,   399,   400,   406,   407,   408,   409,   410,   411,
     412,   413,   414,   415,   416,   417,   418,   428,   429,   434,
     436,   437,   438,   445,   446,   463,   467,   478,   484,   485,
     486,   515,   175,   176,   177,    14,   484,   165,   328,   123,
     509,   512,   313,   306,   509,    12,   284,   356,   357,   412,
     141,   349,   352,   484,    19,   158,   310,   310,   320,   412,
     443,   444,   320,   320,     5,    10,    73,   468,   469,   488,
     489,   490,   321,   494,    12,   318,   392,   412,   419,   499,
     412,   430,    12,    12,    12,    12,    12,    12,     4,    12,
     323,   325,    12,   394,   412,    12,    12,     4,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,   416,   416,    13,   283,    19,
      12,   118,    16,    92,    22,    35,    38,    39,    40,    41,
      42,    43,    44,    81,    92,    97,   110,   208,   268,   291,
     292,   423,   424,   425,   483,   289,   290,   426,   284,   293,
     294,   427,   288,    12,    12,    12,    14,    12,    12,    12,
      14,   484,    14,   392,   499,    12,    19,   480,   481,   482,
     484,   328,    12,   195,   307,   308,   317,   353,   481,    12,
     412,    74,   283,   329,   480,   315,   316,   412,    13,   283,
     325,   419,   166,   328,   501,   166,   328,    20,    60,   345,
     283,   171,   495,   495,   156,   258,   263,   264,   499,   488,
     318,    13,    13,    13,   283,   164,   431,   432,   412,     5,
     419,   412,    61,   284,   412,     5,   412,   412,    13,   419,
     284,   412,   420,   419,   412,   464,   465,   466,   484,   478,
     412,   412,   419,   419,   464,   279,   280,   517,     4,   516,
     516,   516,   516,   152,   428,   428,   270,   271,   272,   412,
     439,   499,   301,    23,    25,    26,    34,    53,    56,    62,
      71,    84,    85,   108,   130,   143,   151,   152,   159,   160,
     184,   185,   186,   187,   188,   189,   190,   191,   474,   475,
     476,   477,    13,   464,   393,   394,     6,     9,   110,   422,
     412,   375,    39,    39,    41,   325,   418,   421,     8,   110,
     412,    22,    81,    97,   208,   115,   412,    39,   292,    15,
      17,   144,   412,   479,   413,   323,   418,   414,   415,   416,
     355,    13,   419,   412,    14,   284,   484,   485,     3,   434,
     484,   497,   498,     4,     4,   484,    14,    14,   484,     5,
     510,   482,    73,   513,   514,   499,   354,   374,   484,   196,
     197,   310,   317,   317,   307,   317,   353,    10,    12,   193,
     214,   324,   325,   358,   359,   360,   361,   362,   363,   370,
     375,   377,   378,   385,   389,   463,   484,   357,   328,   283,
     329,   330,    39,   484,   180,   166,   500,   180,   199,   435,
     444,     5,   242,   496,   496,   115,   491,   259,    12,   471,
     471,   499,    13,   412,   392,   412,    63,   432,   433,    19,
      13,    13,   157,   419,    13,    13,    13,   283,   283,    13,
     283,   441,    13,   283,   183,    74,   441,   441,   441,    13,
      13,   516,   269,   518,    13,    13,    13,    13,    13,    13,
      13,    74,     4,    12,    12,    12,    12,    12,    12,    12,
      18,    13,   422,    16,     8,    66,   412,   421,   412,   115,
     412,   269,   412,   122,   448,    13,    13,    14,   484,    14,
      14,    13,   283,    13,    39,   484,   484,    14,    13,   283,
     278,    13,   283,    14,   198,   198,   499,   310,   317,    19,
      12,   359,   377,    12,    12,   480,   283,    48,    75,    82,
      93,    96,   133,   386,   480,   480,   480,    73,   285,   287,
     369,   504,   505,   506,   507,   508,   509,   480,    14,    77,
     331,   316,   328,   412,   181,   176,   181,   200,   201,   255,
     354,   173,   174,   493,     4,   472,   473,   265,   470,   470,
     149,   149,   412,    64,   474,   375,    13,   447,   448,   412,
     412,     4,    13,   448,   465,   374,   428,   412,    13,    13,
      13,   448,   448,    13,     4,    81,   234,   440,   440,   412,
      74,     5,     5,     5,     5,     5,     5,     5,   412,   412,
      16,    66,   412,     4,    13,    12,   449,   450,   484,   448,
     484,    14,    14,   284,   484,   485,   484,   499,   497,   499,
       4,   484,     5,   115,   374,   484,   499,   484,    13,    18,
     371,   372,   374,   318,   463,   350,   359,    93,   121,   387,
      88,    89,    90,   388,   359,   387,   387,    29,    93,   379,
     209,   210,   392,   364,   365,   484,   480,   484,    27,    79,
     346,   499,    12,     5,   257,   492,    39,    13,   283,   476,
     412,   412,    13,    13,    13,    13,    13,   283,    13,   447,
     447,   447,   412,    77,   448,   448,    13,   412,    13,   283,
      13,    13,    13,    13,    13,    13,   412,   412,   447,   266,
     450,   451,   452,    14,   284,   484,   484,    14,    14,    13,
     210,    14,    12,    12,    13,   283,    13,    13,   360,   388,
     153,   153,    89,   116,   388,   388,    93,   359,    19,    22,
      74,    19,   286,    14,   285,   211,   368,   511,    14,   355,
     392,     4,   256,     4,   473,    90,   273,   274,   275,   276,
     277,   401,   402,   403,   404,   166,   519,    12,    13,     5,
     267,    13,   123,   453,   284,   484,    14,    14,   284,   484,
     485,   484,   499,    74,   484,   390,   391,   484,   419,    19,
     166,   373,   372,    88,    89,    90,    88,    89,    90,   153,
     392,   164,   380,   381,   116,   115,     4,     4,   115,   287,
     365,   367,   392,    12,   197,   212,   484,    12,    78,   237,
     238,   332,   333,   337,   412,    13,    12,    12,    12,    12,
      12,    12,    13,   403,   283,     4,    74,   520,   442,    13,
     375,    27,   441,   284,   484,   284,   484,    14,    14,     4,
      14,    13,   283,   476,    13,     3,   192,    89,    89,    88,
      89,    90,   392,    63,   381,   384,   392,     4,    16,   153,
       4,   364,   157,   366,     5,    12,    12,    14,    13,   142,
     283,   239,   338,   339,     4,   405,   428,     3,   116,     4,
       4,   419,   404,     5,    10,   523,   281,   521,    13,   419,
     127,   135,   454,   455,   284,   484,     3,   486,   153,   391,
      12,    19,    89,   149,   213,   382,    64,     4,     4,    88,
      89,   153,   286,    13,     5,     5,   333,     5,    12,   494,
      13,    13,   283,    13,    13,    13,    13,    13,    15,   523,
      77,   522,    22,    50,   154,   428,   456,   457,   458,   459,
       4,   352,     3,   382,    12,   383,   153,   153,    88,    89,
      13,    13,    12,   334,   335,   336,   374,   418,   166,   340,
     428,   523,   154,   428,   457,   460,   461,   134,   125,   125,
      69,   462,    13,    12,   356,    74,    88,    89,    88,    89,
      93,    93,    12,    13,   336,   418,    13,   283,   240,   241,
     242,   249,   344,    72,    72,    16,    50,    77,   109,   150,
     352,    13,   359,    93,    93,    93,    93,   441,   335,     4,
     243,   246,   341,   343,   460,   134,   120,    13,   116,    13,
     250,   244,   247,   245,   342,   392,   251,   166,     4,   253,
     248,   252,   153,   172
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned short int yyr1[] =
{
       0,   295,   296,   297,   297,   298,   298,   298,   298,   299,
     299,   299,   300,   300,   301,   301,   302,   302,   303,   304,
     305,   305,   305,   305,   305,   306,   306,   307,   307,   308,
     308,   309,   309,   309,   309,   309,   309,   309,   310,   311,
     311,   312,   313,   314,   315,   315,   316,   317,   318,   318,
     319,   319,   319,   319,   320,   320,   321,   321,   322,   323,
     324,   325,   326,   327,   328,   328,   329,   330,   330,   331,
     331,   332,   332,   333,   333,   333,   334,   334,   335,   335,
     335,   336,   336,   337,   337,   337,   338,   339,   339,   340,
     340,   340,   340,   341,   341,   342,   342,   343,   343,   343,
     344,   344,   344,   344,   345,   345,   345,   346,   346,   347,
     347,   348,   348,   349,   350,   350,   351,   352,   352,   353,
     354,   354,   355,   355,   355,   356,   356,   357,   357,   358,
     358,   359,   359,   359,   360,   360,   361,   361,   361,   361,
     361,   361,   362,   362,   363,   363,   364,   364,   365,   365,
     366,   366,   366,   366,   366,   366,   366,   367,   367,   368,
     368,   369,   369,   369,   369,   370,   371,   371,   372,   372,
     373,   373,   373,   373,   374,   374,   374,   374,   374,   374,
     374,   374,   374,   374,   374,   374,   374,   374,   374,   374,
     374,   375,   375,   375,   375,   375,   375,   375,   375,   375,
     376,   376,   376,   376,   377,   377,   378,   378,   378,   379,
     380,   380,   381,   382,   383,   384,   384,   385,   386,   386,
     386,   386,   387,   387,   388,   388,   388,   388,   388,   388,
     388,   388,   388,   388,   389,   390,   390,   391,   392,   392,
     393,   393,   394,   394,   395,   395,   395,   396,   396,   397,
     397,   397,   397,   397,   397,   397,   397,   397,   398,   398,
     399,   399,   400,   400,   401,   401,   401,   402,   402,   402,
     402,   402,   402,   403,   403,   404,   404,   404,   404,   405,
     405,   406,   406,   406,   407,   407,   408,   408,   408,   408,
     409,   409,   410,   410,   411,   412,   412,   412,   413,   413,
     414,   414,   415,   415,   416,   416,   416,   417,   417,   417,
     417,   417,   417,   418,   419,   419,   420,   420,   421,   421,
     422,   422,   423,   423,   423,   423,   423,   423,   423,   423,
     423,   423,   424,   424,   425,   426,   426,   427,   427,   427,
     428,   428,   428,   428,   428,   428,   428,   428,   429,   430,
     430,   431,   431,   432,   432,   433,   433,   434,   434,   434,
     434,   434,   435,   435,   435,   436,   436,   436,   436,   436,
     437,   437,   437,   437,   437,   437,   437,   437,   437,   437,
     437,   437,   437,   438,   438,   438,   438,   438,   438,   438,
     438,   438,   438,   438,   438,   438,   438,   439,   439,   439,
     440,   441,   441,   442,   443,   443,   444,   445,   445,   446,
     446,   446,   446,   446,   446,   446,   446,   446,   446,   446,
     447,   447,   448,   448,   449,   450,   451,   452,   452,   452,
     453,   453,   454,   454,   455,   455,   456,   456,   457,   457,
     457,   458,   459,   460,   460,   460,   461,   462,   462,   462,
     462,   462,   463,   463,   464,   464,   465,   465,   466,   466,
     467,   467,   467,   467,   467,   467,   467,   467,   467,   467,
     467,   467,   467,   468,   469,   470,   470,   471,   471,   472,
     472,   473,   474,   474,   475,   476,   476,   476,   476,   476,
     476,   476,   476,   476,   476,   476,   476,   476,   476,   476,
     476,   476,   476,   476,   476,   476,   476,   476,   476,   476,
     476,   476,   476,   476,   476,   476,   477,   477,   477,   478,
     478,   478,   478,   478,   478,   479,   479,   479,   480,   480,
     481,   481,   482,   483,   484,   484,   484,   484,   484,   484,
     484,   484,   484,   484,   484,   484,   484,   484,   484,   484,
     484,   484,   484,   484,   484,   484,   484,   484,   484,   484,
     484,   484,   485,   485,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   487,   487,   487,   488,
     488,   489,   490,   490,   490,   490,   491,   491,   492,   492,
     493,   493,   493,   494,   494,   494,   495,   495,   496,   496,
     497,   497,   498,   498,   499,   499,   499,   499,   500,   500,
     501,   502,   502,   502,   503,   503,   503,   504,   504,   504,
     505,   506,   507,   508,   509,   510,   510,   511,   511,   511,
     512,   512,   513,   513,   514,   515,   515,   515,   515,   515,
     516,   517,   517,   517,   518,   518,   519,   519,   520,   520,
     521,   521,   521,   522,   522,   523,   523
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     2,     1,     3,     0,     1,     1,     1,     6,
       5,     3,     1,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     0,     1,     3,
       3,     6,     5,     7,     6,     6,     8,     7,     2,     5,
       6,     8,     1,     4,     1,     3,     3,     1,     5,     6,
       1,     4,     4,     4,     1,     4,     1,     3,     3,     1,
       1,     1,     1,     8,     0,     2,     2,     0,     1,     0,
       4,     1,     3,     2,     1,     5,     1,     3,     4,     1,
       1,     1,     2,     2,     1,     1,     5,     0,     2,     0,
       2,     2,     2,     2,     2,     0,     2,     0,     2,     4,
       0,     2,     4,     8,     0,     1,     1,     0,     2,     2,
       3,     1,     3,     4,     0,     1,     3,     1,     3,     3,
       1,     3,     0,     1,     1,     1,     3,     2,     1,     1,
       3,     1,     1,     1,     1,     3,     4,     3,     2,     2,
       2,     2,     4,     4,     6,     3,     1,     3,     5,     1,
       0,     4,     4,     5,     5,     5,     5,     0,     1,     0,
       1,     0,     1,     1,     1,     5,     3,     1,     4,     1,
       7,     5,     4,     2,     1,     3,     3,     5,     5,     7,
       7,     6,     6,     9,     9,     8,     8,     8,     8,     7,
       7,     1,     3,     5,     4,     7,     6,     6,     5,     2,
       1,     3,     5,     7,     1,     1,     6,     5,     3,     5,
       1,     2,     4,     6,     3,     0,     2,     4,     3,     3,
       3,     2,     0,     1,     0,     3,     3,     4,     3,     4,
       3,     4,     4,     5,     6,     1,     3,     2,     1,     3,
       1,     3,     1,     2,     1,     3,     4,     1,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     5,     3,
       5,     4,     8,     6,     1,     2,     1,     1,     4,     1,
       4,     1,     4,     1,     3,     4,     4,     4,     4,     1,
       3,     3,     3,     3,     5,     6,     3,     5,     4,     6,
       3,     4,     3,     4,     2,     1,     2,     3,     1,     3,
       1,     3,     1,     3,     1,     2,     2,     1,     1,     1,
       1,     1,     1,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     2,
       2,     2,     1,     2,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     2,     2,     5,     0,
       1,     1,     2,     4,     4,     0,     2,     1,     1,     1,
       1,     1,     0,     2,     2,     5,     5,     4,     6,     4,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     5,     5,     6,     6,     6,     6,     5,
       1,     6,     6,     5,     7,     6,     4,     1,     1,     1,
       5,     0,     1,     3,     1,     3,     3,     4,     5,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     1,     2,     2,     3,     1,     4,     0,     1,     3,
       0,     3,     0,     3,     1,     1,     1,     1,     2,     2,
       1,     2,     4,     1,     2,     1,     2,     0,     3,     2,
       2,     3,     3,     4,     1,     3,     1,     1,     3,     3,
       4,     3,     4,     6,     6,     6,     4,     1,     4,     1,
       6,     1,     1,     4,     4,     0,     2,     0,     3,     1,
       3,     3,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       4,     1,     4,     1,     4,     1,     4,     1,     4,     1,
       4,     6,     1,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     1,
       2,     1,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     0,     2,     2,     0,
       1,     3,     5,     1,     1,     1,     0,     2,     0,     2,
       0,     1,     2,     0,     4,     4,     0,     2,     0,     2,
       1,     1,     1,     3,     0,     6,     6,     8,     0,     5,
       3,     5,     6,     3,     5,     7,     3,     1,     1,     1,
       5,     6,     6,     5,     4,     1,     3,     5,     5,     4,
       0,     1,     0,     1,     8,     5,     4,     4,     4,     4,
       8,     0,     1,     1,     0,     2,     0,     2,     0,     2,
       0,     2,     2,     0,     2,     1,     1
};


/* YYDPREC[RULE-NUM] -- Dynamic precedence of rule #RULE-NUM (0 if none).  */
static const unsigned char yydprec[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0
};

/* YYMERGER[RULE-NUM] -- Index of merging function for rule #RULE-NUM.  */
static const unsigned char yymerger[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0
};

/* YYIMMEDIATE[RULE-NUM] -- True iff rule #RULE-NUM is not to be deferred, as
   in the case of predicates.  */
static const yybool yyimmediate[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0
};

/* YYCONFLP[YYPACT[STATE-NUM]] -- Pointer into YYCONFL of start of
   list of conflicting reductions corresponding to action entry for
   state STATE-NUM in yytable.  0 means no conflicts.  The list in
   yyconfl is terminated by a rule number of 0.  */
static const unsigned char yyconflp[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       1,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     5,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     3,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0
};

/* YYCONFL[I] -- lists of conflicting rule numbers, each terminated by
   0, pointed into by YYCONFLP.  */
static const short int yyconfl[] =
{
       0,   161,     0,    58,     0,   310,     0
};

/* Error token number */
#define YYTERROR 1


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

# define YYRHSLOC(Rhs, K) ((Rhs)[K].yystate.yyloc)


YYSTYPE yylval;
YYLTYPE yylloc;

int yynerrs;
int yychar;

static const int YYEOF = 0;
static const int YYEMPTY = -2;

typedef enum { yyok, yyaccept, yyabort, yyerr } YYRESULTTAG;

#define YYCHK(YYE)                              \
  do {                                          \
    YYRESULTTAG yychk_flag = YYE;               \
    if (yychk_flag != yyok)                     \
      return yychk_flag;                        \
  } while (0)

#if HANA_DEBUG

# ifndef YYFPRINTF
#  define YYFPRINTF fprintf
# endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined HANA_LTYPE_IS_TRIVIAL && HANA_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YYDPRINTF(Args)                        \
  do {                                          \
    if (yydebug)                                \
      YYFPRINTF Args;                           \
  } while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yyvaluep)
    return;
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, result, scanner);
  YYFPRINTF (yyoutput, ")");
}

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                  \
  do {                                                                  \
    if (yydebug)                                                        \
      {                                                                 \
        YYFPRINTF (stderr, "%s ", Title);                               \
        yy_symbol_print (stderr, Type, Value, Location, result, scanner);        \
        YYFPRINTF (stderr, "\n");                                       \
      }                                                                 \
  } while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;

struct yyGLRStack;
static void yypstack (struct yyGLRStack* yystackp, size_t yyk)
  YY_ATTRIBUTE_UNUSED;
static void yypdumpstack (struct yyGLRStack* yystackp)
  YY_ATTRIBUTE_UNUSED;

#else /* !HANA_DEBUG */

# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)

#endif /* !HANA_DEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYMAXDEPTH * sizeof (GLRStackItem)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

/* Minimum number of free items on the stack allowed after an
   allocation.  This is to allow allocation and initialization
   to be completed by functions that call yyexpandGLRStack before the
   stack is expanded, thus insuring that all necessary pointers get
   properly redirected to new data.  */
#define YYHEADROOM 2

#ifndef YYSTACKEXPANDABLE
#  define YYSTACKEXPANDABLE 1
#endif

#if YYSTACKEXPANDABLE
# define YY_RESERVE_GLRSTACK(Yystack)                   \
  do {                                                  \
    if (Yystack->yyspaceLeft < YYHEADROOM)              \
      yyexpandGLRStack (Yystack);                       \
  } while (0)
#else
# define YY_RESERVE_GLRSTACK(Yystack)                   \
  do {                                                  \
    if (Yystack->yyspaceLeft < YYHEADROOM)              \
      yyMemoryExhausted (Yystack);                      \
  } while (0)
#endif


#if YYERROR_VERBOSE

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static size_t
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return strlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* !YYERROR_VERBOSE */

/** State numbers, as in LALR(1) machine */
typedef int yyStateNum;

/** Rule numbers, as in LALR(1) machine */
typedef int yyRuleNum;

/** Grammar symbol */
typedef int yySymbol;

/** Item references, as in LALR(1) machine */
typedef short int yyItemNum;

typedef struct yyGLRState yyGLRState;
typedef struct yyGLRStateSet yyGLRStateSet;
typedef struct yySemanticOption yySemanticOption;
typedef union yyGLRStackItem yyGLRStackItem;
typedef struct yyGLRStack yyGLRStack;

struct yyGLRState {
  /** Type tag: always true.  */
  yybool yyisState;
  /** Type tag for yysemantics.  If true, yysval applies, otherwise
   *  yyfirstVal applies.  */
  yybool yyresolved;
  /** Number of corresponding LALR(1) machine state.  */
  yyStateNum yylrState;
  /** Preceding state in this stack */
  yyGLRState* yypred;
  /** Source position of the last token produced by my symbol */
  size_t yyposn;
  union {
    /** First in a chain of alternative reductions producing the
     *  non-terminal corresponding to this state, threaded through
     *  yynext.  */
    yySemanticOption* yyfirstVal;
    /** Semantic value for this state.  */
    YYSTYPE yysval;
  } yysemantics;
  /** Source location for this state.  */
  YYLTYPE yyloc;
};

struct yyGLRStateSet {
  yyGLRState** yystates;
  /** During nondeterministic operation, yylookaheadNeeds tracks which
   *  stacks have actually needed the current lookahead.  During deterministic
   *  operation, yylookaheadNeeds[0] is not maintained since it would merely
   *  duplicate yychar != YYEMPTY.  */
  yybool* yylookaheadNeeds;
  size_t yysize, yycapacity;
};

struct yySemanticOption {
  /** Type tag: always false.  */
  yybool yyisState;
  /** Rule number for this reduction */
  yyRuleNum yyrule;
  /** The last RHS state in the list of states to be reduced.  */
  yyGLRState* yystate;
  /** The lookahead for this reduction.  */
  int yyrawchar;
  YYSTYPE yyval;
  YYLTYPE yyloc;
  /** Next sibling in chain of options.  To facilitate merging,
   *  options are chained in decreasing order by address.  */
  yySemanticOption* yynext;
};

/** Type of the items in the GLR stack.  The yyisState field
 *  indicates which item of the union is valid.  */
union yyGLRStackItem {
  yyGLRState yystate;
  yySemanticOption yyoption;
};

struct yyGLRStack {
  int yyerrState;
  /* To compute the location of the error token.  */
  yyGLRStackItem yyerror_range[3];

  YYJMP_BUF yyexception_buffer;
  yyGLRStackItem* yyitems;
  yyGLRStackItem* yynextFree;
  size_t yyspaceLeft;
  yyGLRState* yysplitPoint;
  yyGLRState* yylastDeleted;
  yyGLRStateSet yytops;
};

#if YYSTACKEXPANDABLE
static void yyexpandGLRStack (yyGLRStack* yystackp);
#endif

static _Noreturn void
yyFail (yyGLRStack* yystackp, ParseResult* result, yyscan_t scanner, const char* yymsg)
{
  if (yymsg != YY_NULLPTR)
    yyerror (result, scanner, yymsg);
  YYLONGJMP (yystackp->yyexception_buffer, 1);
}

static _Noreturn void
yyMemoryExhausted (yyGLRStack* yystackp)
{
  YYLONGJMP (yystackp->yyexception_buffer, 2);
}

#if HANA_DEBUG || YYERROR_VERBOSE
/** A printable representation of TOKEN.  */
static inline const char*
yytokenName (yySymbol yytoken)
{
  if (yytoken == YYEMPTY)
    return "";

  return yytname[yytoken];
}
#endif

/** Fill in YYVSP[YYLOW1 .. YYLOW0-1] from the chain of states starting
 *  at YYVSP[YYLOW0].yystate.yypred.  Leaves YYVSP[YYLOW1].yystate.yypred
 *  containing the pointer to the next state in the chain.  */
static void yyfillin (yyGLRStackItem *, int, int) YY_ATTRIBUTE_UNUSED;
static void
yyfillin (yyGLRStackItem *yyvsp, int yylow0, int yylow1)
{
  int i;
  yyGLRState *s = yyvsp[yylow0].yystate.yypred;
  for (i = yylow0-1; i >= yylow1; i -= 1)
    {
#if HANA_DEBUG
      yyvsp[i].yystate.yylrState = s->yylrState;
#endif
      yyvsp[i].yystate.yyresolved = s->yyresolved;
      if (s->yyresolved)
        yyvsp[i].yystate.yysemantics.yysval = s->yysemantics.yysval;
      else
        /* The effect of using yysval or yyloc (in an immediate rule) is
         * undefined.  */
        yyvsp[i].yystate.yysemantics.yyfirstVal = YY_NULLPTR;
      yyvsp[i].yystate.yyloc = s->yyloc;
      s = yyvsp[i].yystate.yypred = s->yypred;
    }
}

/* Do nothing if YYNORMAL or if *YYLOW <= YYLOW1.  Otherwise, fill in
 * YYVSP[YYLOW1 .. *YYLOW-1] as in yyfillin and set *YYLOW = YYLOW1.
 * For convenience, always return YYLOW1.  */
static inline int yyfill (yyGLRStackItem *, int *, int, yybool)
     YY_ATTRIBUTE_UNUSED;
static inline int
yyfill (yyGLRStackItem *yyvsp, int *yylow, int yylow1, yybool yynormal)
{
  if (!yynormal && yylow1 < *yylow)
    {
      yyfillin (yyvsp, *yylow, yylow1);
      *yylow = yylow1;
    }
  return yylow1;
}

/** Perform user action for rule number YYN, with RHS length YYRHSLEN,
 *  and top stack item YYVSP.  YYLVALP points to place to put semantic
 *  value ($$), and yylocp points to place for location information
 *  (@$).  Returns yyok for normal return, yyaccept for YYACCEPT,
 *  yyerr for YYERROR, yyabort for YYABORT.  */
static YYRESULTTAG
yyuserAction (yyRuleNum yyn, size_t yyrhslen, yyGLRStackItem* yyvsp,
              yyGLRStack* yystackp,
              YYSTYPE* yyvalp, YYLTYPE *yylocp, ParseResult* result, yyscan_t scanner)
{
  yybool yynormal YY_ATTRIBUTE_UNUSED = (yystackp->yysplitPoint == YY_NULLPTR);
  int yylow;
  YYUSE (yyvalp);
  YYUSE (yylocp);
  YYUSE (result);
  YYUSE (scanner);
  YYUSE (yyrhslen);
# undef yyerrok
# define yyerrok (yystackp->yyerrState = 0)
# undef YYACCEPT
# define YYACCEPT return yyaccept
# undef YYABORT
# define YYABORT return yyabort
# undef YYERROR
# define YYERROR return yyerrok, yyerr
# undef YYRECOVERING
# define YYRECOVERING() (yystackp->yyerrState != 0)
# undef yyclearin
# define yyclearin (yychar = YYEMPTY)
# undef YYFILL
# define YYFILL(N) yyfill (yyvsp, &yylow, N, yynormal)
# undef YYBACKUP
# define YYBACKUP(Token, Value)                                              \
  return yyerror (result, scanner, YY_("syntax error: cannot back up")),     \
         yyerrok, yyerr

  yylow = 1;
  if (yyrhslen == 0)
    *yyvalp = yyval_default;
  else
    *yyvalp = yyvsp[YYFILL (1-yyrhslen)].yystate.yysemantics.yysval;
  YYLLOC_DEFAULT ((*yylocp), (yyvsp - yyrhslen), yyrhslen);
  yystackp->yyerror_range[1].yystate.yyloc = *yylocp;

  switch (yyn)
    {
        case 2:
#line 276 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    g_QuestMarkId = 0;
    ((*yyvalp).node) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node);
    result->result_tree_ = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node);
    result->accept = true;
    YYACCEPT;
}
#line 7176 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 4:
#line 288 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SEMICOLON_LIST_SERIALIZE_FORMAT;
}
#line 7185 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 5:
#line 295 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7191 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 9:
#line 303 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CALL, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CALL_HANA_SERIALIZE_FORMAT;
}
#line 7200 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 10:
#line 308 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CALL, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CALL_HANA_SERIALIZE_FORMAT;
}
#line 7209 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 11:
#line 313 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CALL, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CALL_HANA_SERIALIZE_FORMAT2;
}
#line 7218 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 13:
#line 322 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7227 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 14:
#line 330 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 7236 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 15:
#line 335 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 7245 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 25:
#line 363 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7251 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 27:
#line 368 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) =nullptr;}
#line 7257 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 29:
#line 373 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node)= Node::makeTerminalNode(E_STRING,"OVERRIDING SYSTEM VALUE");}
#line 7263 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 30:
#line 374 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node)= Node::makeTerminalNode(E_STRING,"OVERRIDING USER VALUE");}
#line 7269 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 31:
#line 379 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7280 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 32:
#line 386 "sqlparser_hana.yacc" /* glr.c:816  */
    {
     Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
     as->serialize_format = &AS_SERIALIZE_FORMAT;
     ((*yyvalp).node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
     ((*yyvalp).node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7291 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 33:
#line 393 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
    ((*yyvalp).node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7304 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 34:
#line 402 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
    ((*yyvalp).node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7317 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 35:
#line 411 "sqlparser_hana.yacc" /* glr.c:816  */
    {
     Node * source = Node::makeNonTerminalNode(E_INSERT_SELECT, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
     source->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
     Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
     as->serialize_format = &AS_SERIALIZE_FORMAT;
     ((*yyvalp).node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
     ((*yyvalp).node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7330 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 36:
#line 420 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
    ((*yyvalp).node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7343 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 37:
#line 429 "sqlparser_hana.yacc" /* glr.c:816  */
    {
     Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
     source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
     Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));
     as->serialize_format = &AS_SERIALIZE_FORMAT;
     ((*yyvalp).node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
     ((*yyvalp).node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7356 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 38:
#line 441 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_VALUES_CTOR, E_VALUES_CTOR_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TABLE_VALUE_CTOR_SERIALIZE_FORMAT;
}
#line 7365 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 39:
#line 450 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_DELETE, E_DELETE_PROPERTY_CNT,
    		nullptr,	/* E_DELETE_OPT_WITH 0 */
    		nullptr,	/* E_DELETE_OPT_TOP 1 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node),		/* E_DELETE_DELETE_RELATION 2 */
    		nullptr,	/* E_DELETE_DELETE_RELATION_OPT_TABLE_HINT 3 */
    		nullptr,        /* E_DELETE_OPT_OUTPUT 4 */
    		nullptr,	/* E_DELETE_FROM_LIST 5 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),		/* E_DELETE_OPT_WHERE 6 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node) 	/* E_DELETE_OPT_QUERY_HINT 7 */);
    ((*yyvalp).node)->serialize_format = &DELETE_SERIALIZE_FORMAT;
}
#line 7382 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 40:
#line 463 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_DELETE, E_DELETE_PROPERTY_CNT,
        nullptr,	/* E_DELETE_OPT_WITH 0 */
        nullptr,	/* E_DELETE_OPT_TOP 1 */
        (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node),		/* E_DELETE_DELETE_RELATION 2 */
        nullptr,	/* E_DELETE_DELETE_RELATION_OPT_TABLE_HINT 3 */
        nullptr,        /* E_DELETE_OPT_OUTPUT 4 */
        nullptr,	/* E_DELETE_FROM_LIST 5 */
        (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),		/* E_DELETE_OPT_WHERE 6 */
        (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node) 	/* E_DELETE_OPT_QUERY_HINT 7 */);
    ((*yyvalp).node)->serialize_format = &DELETE_S_FORMAT;
}
#line 7399 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 41:
#line 480 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_UPDATE, E_UPDATE_PROPERTY_CNT,
    		nullptr,	/* E_UPDATE_OPT_WITH 0 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node),	/* E_UPDATE_OPT_TOP 1 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node),		    /* E_UPDATE_UPDATE_RELATION 2 */
    		nullptr,	/* E_UPDATE_UPDATE_RELATION_OPT_TABLE_HINT 3 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node),		    /* E_UPDATE_UPDATE_ELEM_LIST 4 */
    		nullptr,	/* E_UPDATE_OPT_OUTPUT 5 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node),	        /* E_UPDATE_FROM_LIST 6 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),		    /* E_UPDATE_OPT_WHERE 7 */
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)		    /* E_UPDATE_OPT_QUERY_HINT 8 */);
    ((*yyvalp).node)->serialize_format = &UPDATE_SERIALIZE_FORMAT;
}
#line 7417 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 43:
#line 500 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node) == nullptr && (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node) == nullptr && (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node) == nullptr) {
        ((*yyvalp).node) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node);
    } else {
        ((*yyvalp).node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
        ((*yyvalp).node)->serialize_format = &AS_SERIALIZE_FORMAT;
    }

}
#line 7431 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 45:
#line 532 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_UPDATE_ELEM_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7440 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 46:
#line 540 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_EQ, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_EQ_SERIALIZE_FORMAT;
}
#line 7449 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 47:
#line 549 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node);
    // 0 , 1  2 already
    // $$->setChild(E_DIRECT_SELECT_FOR_UPDATE, $2);
    // $$->setChild(E_DIRECT_HINT_CLAUSE, $3);
}
#line 7460 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 48:
#line 560 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT_HANA;

    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node) || (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)){
        std::string val = "";
        if((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)){val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));}
        if((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)){val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));}
        Node * str  = Node::makeTerminalNode(E_STRING, val);
        ((*yyvalp).node)->setChild(2, str);
    }

}
#line 7478 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 49:
#line 574 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr,(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT_HANA;

    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node) || (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)){
        std::string val = "";
        if((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)){val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)); val += " ";}
        if((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)){val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)); }
        Node * str  = Node::makeTerminalNode(E_STRING, val);
        ((*yyvalp).node)->setChild(2, str);
    }
}
#line 7495 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 51:
#line 590 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_UNION, "UNION");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    ((*yyvalp).node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7526 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 52:
#line 617 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_EXCEPT, "EXCEPT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    ((*yyvalp).node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7557 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 53:
#line 644 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_EXCEPT, "MINUS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    ((*yyvalp).node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7588 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 55:
#line 675 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_INTERSECT, "INTERSECT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    ((*yyvalp).node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7619 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 57:
#line 706 "sqlparser_hana.yacc" /* glr.c:816  */
    {
   // if ($2->getChild(E_DIRECT_SELECT_WITH)) {
    //    yyerror(&@1, result, scanner, "error use common table expression");
   //     YYABORT;
   // }
    //$$ = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, $2->getChild(E_DIRECT_SELECT_SELECT_CLAUSE));
    //$$->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
    //$2->setChild(E_DIRECT_SELECT_SELECT_CLAUSE, nullptr);
    //delete($2);

    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 7637 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 58:
#line 724 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 7646 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 63:
#line 742 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                    (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node),             /* E_SELECT_DISTINCT 0 */
                    (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node),             /* E_SELECT_SELECT_EXPR_LIST 1 */
                    (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node),             /* E_SELECT_FROM_LIST 2 */
                    (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node),             /* E_SELECT_OPT_WHERE 3 */
                    (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node),             /* E_SELECT_GROUP_BY 4 */
                    (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node),             /* E_SELECT_HAVING 5 */
                    nullptr,        /* E_SELECT_SET_OPERATION 6 */
                    nullptr,        /* E_SELECT_ALL_SPECIFIED 7 */
                    nullptr,        /* E_SELECT_FORMER_SELECT_STMT 8 */
                    nullptr,        /* E_SELECT_LATER_SELECT_STMT 9 */
                    nullptr,             /* E_SELECT_ORDER_BY 10 */
                    nullptr,            /* E_SELECT_LIMIT 11 */
                    nullptr,        /* E_SELECT_FOR_UPDATE 12 */
                    nullptr,        /* E_SELECT_HINTS 13 */
                    nullptr,        /* E_SELECT_WHEN 14 */
                    (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node),             /* E_SELECT_OPT_TOP 15 */
                    nullptr,        /* E_SELECT_OPT_WITH 16 */
                    nullptr,        /* E_SELECT_OPT_OPTION 17 */
	                nullptr,         /* E_SELECT_OPT_INTO 18 */
	                nullptr
                    );
    ((*yyvalp).node)->serialize_format = &SELECT_SERIALIZE_FORMAT;
}
#line 7676 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 64:
#line 770 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7682 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 65:
#line 772 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WHERE_CLAUSE, E_WHERE_CLAUSE_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WHERE_SERIALIZE_FORMAT;
}
#line 7691 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 66:
#line 781 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FROM_CLAUSE, E_FROM_CLAUSE_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FROM_SERIALIZE_FORMAT;
}
#line 7700 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 67:
#line 787 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7706 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 69:
#line 791 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7712 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 70:
#line 793 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_GROUP_BY, E_GROUP_BY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &GROUP_BY_SERIALIZE_FORMAT;
}
#line 7721 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 72:
#line 802 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7730 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 73:
#line 809 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"()"); }
#line 7736 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 75:
#line 812 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string name = (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node));
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)){
        name += (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));
    }
    Node * name_n = Node::makeTerminalNode(E_IDENTIFIER, name);
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name_n, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 7752 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 77:
#line 827 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7761 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 78:
#line 834 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string ss = (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)){
        ss += (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    }
    Node * node_s  = Node::makeTerminalNode(E_STRING, ss );
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, node_s);
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 7777 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 82:
#line 851 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) =  Node::makeTerminalNode(E_STRING, "()" );
}
#line 7785 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 83:
#line 857 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "GROUPING SETS ");}
#line 7791 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 84:
#line 858 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ROLLUP "); }
#line 7797 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 85:
#line 859 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)  = Node::makeTerminalNode(E_IDENTIFIER, "CUBE "); }
#line 7803 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 86:
#line 863 "sqlparser_hana.yacc" /* glr.c:816  */
    {
      std::string val;
      if((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)){ val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)); }
      if((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)){ val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)); }
      if((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)){ val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)); }
      if((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)){ val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
      if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){ val += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)); }
      if(!val.empty()){
        ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, val);
      } else { ((*yyvalp).node) = nullptr; }
}
#line 7819 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 87:
#line 877 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7825 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 88:
#line 878 "sqlparser_hana.yacc" /* glr.c:816  */
    {    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "BEST "+ (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text()); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)); }
#line 7831 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 89:
#line 881 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7837 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 90:
#line 882 "sqlparser_hana.yacc" /* glr.c:816  */
    {     ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "WITH SUBTOTAL " ); }
#line 7843 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 91:
#line 883 "sqlparser_hana.yacc" /* glr.c:816  */
    {  ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "WITH BALANCE " ); }
#line 7849 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 92:
#line 884 "sqlparser_hana.yacc" /* glr.c:816  */
    {     ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "WITH TOTAL " ); }
#line 7855 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 93:
#line 888 "sqlparser_hana.yacc" /* glr.c:816  */
    {
     std::string pref_s ;
     if((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)){ pref_s += (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
     if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){ pref_s += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text(); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)); }
     if(!pref_s.empty()){
         ((*yyvalp).node) = Node::makeTerminalNode( E_STRING, pref_s );
     } else { ((*yyvalp).node) = nullptr;  }
}
#line 7868 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 94:
#line 896 "sqlparser_hana.yacc" /* glr.c:816  */
    {    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, " MULTIPLE RESULTSETS " ); }
#line 7874 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 95:
#line 899 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7880 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 96:
#line 900 "sqlparser_hana.yacc" /* glr.c:816  */
    {     ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "PREFIX "+ (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() ); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)); }
#line 7886 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 97:
#line 903 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7892 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 98:
#line 904 "sqlparser_hana.yacc" /* glr.c:816  */
    {    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "STRUCTURED RESULT " ); }
#line 7898 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 99:
#line 905 "sqlparser_hana.yacc" /* glr.c:816  */
    {    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "STRUCTURED RESULT WITH OVERVIEW " ); }
#line 7904 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 100:
#line 908 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7910 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 101:
#line 909 "sqlparser_hana.yacc" /* glr.c:816  */
    {  ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TEXT_FILTER " + (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() + " " ); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)); }
#line 7916 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 102:
#line 910 "sqlparser_hana.yacc" /* glr.c:816  */
    {  ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TEXT_FILTER " + (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text() +  " FILL UP " );  delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)); }
#line 7922 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 103:
#line 911 "sqlparser_hana.yacc" /* glr.c:816  */
    {  ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TEXT_FILTER " + (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node)->text() +  " FILL UP SORT MATCHES TO TOP " ); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node)); }
#line 7928 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 104:
#line 948 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_SORT_ASC, "");
}
#line 7936 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 105:
#line 952 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_SORT_ASC, "ASC");
}
#line 7944 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 106:
#line 956 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_SORT_DESC, "DESC");
}
#line 7952 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 107:
#line 962 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 7958 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 108:
#line 964 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_HAVING, E_HAVING_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &HAVING_SERIALIZE_FORMAT;
}
#line 7967 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 109:
#line 974 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WITH_CLAUSE_SERIALIZE_FORMAT;
}
#line 7976 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 110:
#line 979 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WITH_RECURSIVE_CLAUSE_SERIALIZE_FORMAT;
}
#line 7985 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 112:
#line 988 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WITH_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7994 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 113:
#line 997 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_COMMON_TABLE_EXPR, E_COMMON_TABLE_EXPR_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMON_TABLE_EXPR_SERIALIZE_FORMAT;
}
#line 8003 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 114:
#line 1004 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 8009 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 116:
#line 1010 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8018 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 118:
#line 1019 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8027 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 119:
#line 1026 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8036 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 121:
#line 1034 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8045 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 122:
#line 1040 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 8051 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 123:
#line 1042 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_ALL, "ALL");
}
#line 8059 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 124:
#line 1046 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_DISTINCT, "DISTINCT");
}
#line 8067 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 126:
#line 1054 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SELECT_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8076 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 127:
#line 1062 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    if (!(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)) {
    	((*yyvalp).node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    	((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
    else {
	Node* alias_node = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
        alias_node->serialize_format = &AS_SERIALIZE_FORMAT;

        ((*yyvalp).node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, alias_node);
        ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
}
#line 8094 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 128:
#line 1076 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* star_node = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, star_node);
    ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 8104 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 130:
#line 1086 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FROM_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8113 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 135:
#line 1102 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_JOINED_TABLE_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8122 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 136:
#line 1113 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8131 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 137:
#line 1118 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8140 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 138:
#line 1123 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8149 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 139:
#line 1128 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8158 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 140:
#line 1133 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8167 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 141:
#line 1138 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8176 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 142:
#line 1146 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_LATERAL_QUERY, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &LATERAL_QUERY_SERIALIZE_FORMAT;
}
#line 8185 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 143:
#line 1151 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_LATERAL_QUERY, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &LATERAL_QUERY_SERIALIZE_FORMAT;
}
#line 8194 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 144:
#line 1159 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ASSOCIATED_TABLE, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &ASSOCIATED_TAB_SERIALIZE_FORMAT;
}
#line 8203 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 145:
#line 1164 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ASSOCIATED_TABLE,3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &ASSOCIATED_TAB_SERIALIZE_FORMAT;
}
#line 8212 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 147:
#line 1172 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ASSOCIATED_REF_LIST, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &ASSOCIATED_LIST_SERIALIZE_FORMAT;
}
#line 8221 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 148:
#line 1179 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ASSOCIATED_REF, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &ASSOCIATED_REF_SERIALIZE_FORMAT;
}
#line 8230 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 149:
#line 1184 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ASSOCIATED_REF, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 8239 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 150:
#line 1190 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)=nullptr; }
#line 8245 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 151:
#line 1191 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "USING TO ONE JOIN"); }
#line 8251 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 152:
#line 1192 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "USING TO MANY JOIN"); }
#line 8257 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 153:
#line 1193 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "USING MANY TO ONE JOIN"); }
#line 8263 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 154:
#line 1194 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "USING MANY TO MANY JOIN"); }
#line 8269 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 155:
#line 1195 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "USING ONE TO ONE JOIN"); }
#line 8275 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 156:
#line 1196 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "USING ONE TO MANY JOIN"); }
#line 8281 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 157:
#line 1199 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)=nullptr; }
#line 8287 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 159:
#line 1204 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = NULL; }
#line 8293 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 161:
#line 1209 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = NULL; }
#line 8299 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 165:
#line 1219 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_UNNEST_TABLE, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &UNNEST_TABLE_FORMAT;
}
#line 8308 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 166:
#line 1227 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_COLLECT_VAL_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8317 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 168:
#line 1236 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_COLLECT_VAL, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COLLECT_VAL_FORMAT;
}
#line 8326 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 170:
#line 1245 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "WITH ORDINALITY AS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8336 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 171:
#line 1251 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "AS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8346 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 172:
#line 1257 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "WITH ORDINALITY AS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node),nullptr);
    ((*yyvalp).node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8356 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 173:
#line 1263 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "AS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8366 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 174:
#line 1274 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8376 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 175:
#line 1280 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8386 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 176:
#line 1286 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8397 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 177:
#line 1293 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8407 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 178:
#line 1299 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8418 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 179:
#line 1306 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8428 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 180:
#line 1312 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8439 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 181:
#line 1319 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8449 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 182:
#line 1325 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8460 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 183:
#line 1332 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8470 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 184:
#line 1338 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-8)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8481 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 185:
#line 1345 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8491 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 186:
#line 1351 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8502 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 187:
#line 1358 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8512 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 188:
#line 1364 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8523 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 189:
#line 1371 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8533 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 190:
#line 1377 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8544 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 191:
#line 1388 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 8553 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 192:
#line 1393 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 8562 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 193:
#line 1398 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 8571 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 194:
#line 1403 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 8580 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 195:
#line 1408 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8589 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 196:
#line 1413 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8598 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 197:
#line 1418 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8607 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 198:
#line 1423 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8616 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 199:
#line 1428 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_VAR, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TABLE_VAR_SERIALIZE_FORMAT;
}
#line 8625 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 200:
#line 1436 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 8634 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 201:
#line 1441 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 8643 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 202:
#line 1446 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 8652 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 203:
#line 1451 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8661 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 206:
#line 1468 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 8670 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 207:
#line 1473 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_INNER, "");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 8680 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 208:
#line 1479 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE_JOIN_TABLE, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 8689 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 209:
#line 1499 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE_JOIN, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CASE_JOIN_SERIALIZE_FORMAT;
}
#line 8698 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 211:
#line 1507 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE_JOIN_WHEN_LIST, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SPACE_LIST_SERIALIZE_FORMAT;
}
#line 8707 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 212:
#line 1514 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE_JOIN_WHEN, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CASE_JOIN_WHEN_SERIALIZE_FORMAT;
}
#line 8716 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 213:
#line 1521 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE_JOIN_RET, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CASE_JOIN_RET_SERIALIZE_FORMAT;
}
#line 8725 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 214:
#line 1528 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8734 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 215:
#line 1534 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 8740 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 216:
#line 1536 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE_JOIN_ELSE, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CASE_JOIN_ELSE_SERIALIZE_FORMAT;
}
#line 8749 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 217:
#line 1544 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_CROSS, "CROSS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &JOINED_TB_2_SERIALIZE_FORMAT;
}
#line 8759 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 218:
#line 1570 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "FULL ";
    if ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.ival))
    {
        val += "OUTER ";
    }
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        val+=(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node) = Node::makeTerminalNode(E_JOIN_FULL, val);
}
#line 8776 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 219:
#line 1583 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "LEFT ";
    if ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.ival))
    {
        val += "OUTER ";
    }
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        val+=(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node) = Node::makeTerminalNode(E_JOIN_LEFT, val);
}
#line 8793 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 220:
#line 1596 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "RIGHT ";
    if ((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.ival))
    {
        val+="OUTER ";
    }
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        val += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node) = Node::makeTerminalNode(E_JOIN_RIGHT, val);

}
#line 8811 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 221:
#line 1610 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "INNER ";
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        val += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node) = Node::makeTerminalNode(E_JOIN_INNER, val);
}
#line 8824 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 222:
#line 1621 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).ival) = 0; }
#line 8830 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 223:
#line 1622 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).ival) = 1; /*this is a flag*/}
#line 8836 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 224:
#line 1626 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 8842 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 225:
#line 1627 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "MANY TO MANY"); }
#line 8848 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 226:
#line 1628 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "MANY TO ONE"); }
#line 8854 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 227:
#line 1629 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "MANY TO EXACT ONE"); }
#line 8860 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 228:
#line 1630 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "ONE TO MANY"); }
#line 8866 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 229:
#line 1631 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "EXACT ONE TO MANY"); }
#line 8872 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 230:
#line 1632 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "ONE TO ONE"); }
#line 8878 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 231:
#line 1633 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "EXACT ONE TO ONE"); }
#line 8884 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 232:
#line 1634 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "ONE TO EXACT ONE"); }
#line 8890 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 233:
#line 1635 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "EXACT ONE TO EXACT ONE"); }
#line 8896 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 234:
#line 1640 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CONSTRUCT_FROM_TABLE, E_CONSTRUCT_FROM_TABLE_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CONSTRUCT_FROM_TABLE_SERIALIZE_FORMAT;
}
#line 8905 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 236:
#line 1649 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CONSTRUCT_COL_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8914 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 237:
#line 1657 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CONSTRUCT_COL, E_CONSTRUCT_COL_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &CONSTRUCT_COL_SERIALIZE_FORMAT;
}
#line 8923 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 239:
#line 1667 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_OR, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_OR);
}
#line 8932 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 241:
#line 1676 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_AND, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_AND);
}
#line 8941 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 243:
#line 1685 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NOT, E_OP_UNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_NOT);
}
#line 8950 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 245:
#line 1694 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 8959 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 246:
#line 1699 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 8968 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 248:
#line 1708 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8977 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 258:
#line 1737 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_LIKE_REGEXPR, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_LIKE_REGEXPR);
}
#line 8986 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 259:
#line 1742 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_LIKE_REGEXPR, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_LIKE_REGEXPR);
}
#line 8995 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 260:
#line 1749 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NOT_MEMBER, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_NOT_MEMBER);
}
#line 9004 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 261:
#line 1754 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_MEMBER, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_MEMBER);
}
#line 9013 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 262:
#line 1761 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * node_str = Node::makeTerminalNode(E_STRING, "CONTAINS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		node_str, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9024 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 263:
#line 1768 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * node_str = Node::makeTerminalNode(E_STRING, "CONTAINS");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        node_str, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9035 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 265:
#line 1778 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text() + " " + (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, val);
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 9046 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 267:
#line 1787 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "EXACT"); }
#line 9052 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 268:
#line 1789 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "EXACT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9062 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 269:
#line 1794 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "FUZZY"); }
#line 9068 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 270:
#line 1796 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FUZZY");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9078 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 271:
#line 1801 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "LINGUISTIC"); }
#line 9084 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 272:
#line 1803 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LINGUISTIC");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9094 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 274:
#line 1812 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9103 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 275:
#line 1818 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "WEIGHT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9113 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 276:
#line 1824 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FULLTEXT");
    Node * on_ = Node::makeTerminalNode(E_IDENTIFIER, "ON");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, on_, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9124 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 277:
#line 1831 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FULLTEXT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9134 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 278:
#line 1837 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LANGUAGE");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9144 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 280:
#line 1846 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9153 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 281:
#line 1854 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype), E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype));
}
#line 9162 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 282:
#line 1859 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype), E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype));
}
#line 9171 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 283:
#line 1864 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype), E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype));
}
#line 9180 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 284:
#line 1874 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_BTW, E_OP_TERNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_BTW);
}
#line 9189 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 285:
#line 1879 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NOT_BTW, E_OP_TERNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_NOT_BTW);
}
#line 9198 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 286:
#line 1887 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 9207 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 287:
#line 1892 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_TERNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 9216 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 288:
#line 1897 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 9225 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 289:
#line 1902 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_TERNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 9234 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 290:
#line 1910 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_IN, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_IN);
}
#line 9243 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 291:
#line 1915 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NOT_IN, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_NOT_IN);
}
#line 9252 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 292:
#line 1923 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 9261 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 293:
#line 1928 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 9270 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 294:
#line 1936 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_EXISTS, E_OP_UNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_EXISTS);
}
#line 9279 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 296:
#line 1946 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_COLLATE, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 9288 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 297:
#line 1951 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_CNN, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype));
}
#line 9297 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 299:
#line 1960 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_ADD, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype));
}
#line 9306 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 301:
#line 1969 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_MUL, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype));
}
#line 9315 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 303:
#line 1978 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_POW, E_OP_BINARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_POW);
}
#line 9324 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 305:
#line 1987 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_POS, E_OP_UNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_POS);
}
#line 9333 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 306:
#line 1992 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OP_NEG, E_OP_UNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_OP_NEG);
}
#line 9342 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 313:
#line 2008 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 9351 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 315:
#line 2016 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9360 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 317:
#line 2024 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, " * "); }
#line 9366 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 322:
#line 2037 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_LE; }
#line 9372 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 323:
#line 2038 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_LT; }
#line 9378 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 324:
#line 2039 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_GE; }
#line 9384 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 325:
#line 2040 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_GT; }
#line 9390 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 326:
#line 2041 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_EQ; }
#line 9396 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 327:
#line 2042 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_NE; }
#line 9402 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 328:
#line 2043 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_GE; }
#line 9408 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 329:
#line 2044 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_LE; }
#line 9414 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 330:
#line 2045 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_NE; }
#line 9420 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 331:
#line 2046 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_NE; }
#line 9426 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 332:
#line 2050 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_CNN; }
#line 9432 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 333:
#line 2051 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_CNN; }
#line 9438 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 334:
#line 2057 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).nodetype) = Node::comp_all_some_any_op_form((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.nodetype), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.ival));
}
#line 9446 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 335:
#line 2063 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_ADD; }
#line 9452 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 336:
#line 2064 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_MINUS; }
#line 9458 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 337:
#line 2068 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_MUL; }
#line 9464 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 338:
#line 2069 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_DIV; }
#line 9470 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 339:
#line 2070 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).nodetype) = E_OP_REM; }
#line 9476 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 346:
#line 2080 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "DATE" + (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text()); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)); }
#line 9482 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 347:
#line 2081 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING,  "TIMESTAMP" + (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text()); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));  }
#line 9488 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 348:
#line 2087 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE, E_CASE_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = Node::op_serialize_format(E_CASE);
}
#line 9497 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 349:
#line 2094 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 9503 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 352:
#line 2101 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WHEN_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SPACE_LIST_SERIALIZE_FORMAT;
}
#line 9512 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 353:
#line 2109 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 9521 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 354:
#line 2114 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 9530 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 355:
#line 2121 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 9536 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 356:
#line 2123 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_CASE_DEFAULT, E_CASE_DEFAULT_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &ELSE_SERIALIZE_FORMAT;
}
#line 9545 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 362:
#line 2139 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 9551 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 363:
#line 2140 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "NULLS FIRST"); }
#line 9557 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 364:
#line 2141 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "NULLS LAST"); }
#line 9563 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 365:
#line 2147 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "COUNT");
    Node* star = Node::makeTerminalNode(E_STAR, "*");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		name, star, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9575 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 366:
#line 2155 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "COUNT");
    Node * dist = Node::makeTerminalNode(E_IDENTIFIER, "DISTINCT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		name, dist, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr,  nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9587 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 367:
#line 2163 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "COUNT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9598 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 368:
#line 2170 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr,  (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9608 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 369:
#line 2176 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* name = Node::makeTerminalNode(E_STRING, "CHAR");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9619 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 370:
#line 2185 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CORR"); }
#line 9625 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 371:
#line 2186 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CORR_SPEARMAN"); }
#line 9631 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 372:
#line 2187 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MIN"); }
#line 9637 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 373:
#line 2188 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MEDIAN"); }
#line 9643 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 374:
#line 2189 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MAX"); }
#line 9649 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 375:
#line 2190 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SUM"); }
#line 9655 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 376:
#line 2191 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "AVG"); }
#line 9661 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 377:
#line 2192 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV"); }
#line 9667 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 378:
#line 2193 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR"); }
#line 9673 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 379:
#line 2194 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_POP"); }
#line 9679 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 380:
#line 2195 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_POP"); }
#line 9685 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 381:
#line 2196 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_SAMP"); }
#line 9691 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 382:
#line 2197 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_SAMP"); }
#line 9697 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 383:
#line 2202 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "BINNING");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 9708 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 384:
#line 2209 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "SERIES_FILTER");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 9719 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 385:
#line 2216 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "EXTRACT");
    Node * from = Node::makeTerminalNode(E_STRING, "FROM");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, from, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9731 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 386:
#line 2224 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FIRST_VALUE");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9742 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 387:
#line 2231 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LAST_VALUE");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9753 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 388:
#line 2238 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "NTH_VALUE");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9764 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 389:
#line 2245 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "NTILE");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 9775 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 391:
#line 2253 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_CONT");
    Node * over = Node::makeTerminalNode(E_STRING, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text() + (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text());
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, over, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node) ,nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9789 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 392:
#line 2263 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_DISC");
    Node * over = Node::makeTerminalNode(E_STRING, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text() + (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text());
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, over, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node) ,nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9803 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 393:
#line 2273 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "STRING_AGG");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9814 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 394:
#line 2280 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "TRIM");
    std::string param1;
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)){ param1 += (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)->text(); param1 += " "; delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)); }
    param1 += (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));
    param1 += " FROM ";
     Node * param = Node::makeTerminalNode(E_IDENTIFIER, param1);
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, param, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9831 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 395:
#line 2293 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "TRIM");
    std::string param1 = (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));
    param1 += " FROM ";
    Node * param = Node::makeTerminalNode(E_IDENTIFIER, param1);
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
             name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, param, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9846 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 396:
#line 2304 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "TRIM");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9857 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 397:
#line 2312 "sqlparser_hana.yacc" /* glr.c:816  */
    {  ((*yyvalp).node)  = Node::makeTerminalNode(E_STRING, "LEADING"); }
#line 9863 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 398:
#line 2313 "sqlparser_hana.yacc" /* glr.c:816  */
    {  ((*yyvalp).node)  = Node::makeTerminalNode(E_STRING, "TRAILING"); }
#line 9869 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 399:
#line 2314 "sqlparser_hana.yacc" /* glr.c:816  */
    {  ((*yyvalp).node)  = Node::makeTerminalNode(E_STRING, "BOTH"); }
#line 9875 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 400:
#line 2319 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WITHIN_GROUP, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WITHIN_GROUP_SERIALIZE_FORMAT;
}
#line 9884 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 401:
#line 2326 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node)=nullptr; }
#line 9890 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 403:
#line 2332 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ORDER_BY_CLAUSE, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &AGG_ORDER_SERIALIZE_FORMAT;
}
#line 9899 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 405:
#line 2340 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9908 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 406:
#line 2347 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_ORDER_BY_EXPR, 4,
        		(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 9918 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 407:
#line 2356 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 9928 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 408:
#line 2362 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 9938 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 409:
#line 2371 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "RANK"); }
#line 9944 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 410:
#line 2372 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "DENSE_RANK"); }
#line 9950 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 411:
#line 2373 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENT_RANK"); }
#line 9956 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 412:
#line 2374 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CUME_DIST"); }
#line 9962 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 413:
#line 2375 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW_NUMBER"); }
#line 9968 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 414:
#line 2376 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LAG"); }
#line 9974 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 415:
#line 2377 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LEAD"); }
#line 9980 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 416:
#line 2378 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "RANDOM_PARTITION"); }
#line 9986 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 417:
#line 2379 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "WEIGHTED_AVG"); }
#line 9992 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 418:
#line 2380 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CUBIC_SPLINE_APPROX"); }
#line 9998 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 419:
#line 2381 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LINEAR_APPROX"); }
#line 10004 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 420:
#line 2385 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr;}
#line 10010 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 422:
#line 2390 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OVER "+ (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text()); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 10018 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 423:
#line 2394 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OVER_CLAUSE, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OVER_CLAUSE_SERIALIZE_FORMAT;
}
#line 10027 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 424:
#line 2403 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node); }
#line 10033 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 426:
#line 2412 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WINDOW_SPECIFIC, E_WINDOW_SPECIFIC_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WINDOW_SPECIFIC_CLAUSE_SERIALIZE_FORMAT;
}
#line 10042 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 427:
#line 2419 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 10048 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 429:
#line 2423 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string s3 = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node) ? (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() : "";
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "SERIES TABLE "+s3);
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 10058 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 430:
#line 2431 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 10064 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 431:
#line 2433 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node); }
#line 10070 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 432:
#line 2437 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 10076 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 433:
#line 2439 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string s3 = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node) ? (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() : "";
    ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text()+" "+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+" "+s3);
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 10086 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 434:
#line 2447 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"ROWS"); }
#line 10092 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 435:
#line 2448 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"RANGE"); }
#line 10098 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 438:
#line 2457 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED PRECEDING"); }
#line 10104 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 439:
#line 2458 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"CURRENT ROW"); }
#line 10110 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 441:
#line 2463 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+" PRECEDING"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10116 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 442:
#line 2468 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BETWEEN "+(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text()+" AND "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text()); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)); }
#line 10122 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 444:
#line 2473 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED FOLLOWING"); }
#line 10128 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 446:
#line 2478 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+" FOLLOWING"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10134 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 447:
#line 2482 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 10140 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 448:
#line 2483 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE CURRENT ROW"); }
#line 10146 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 449:
#line 2484 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE GROUP"); }
#line 10152 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 450:
#line 2485 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE TIES"); }
#line 10158 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 451:
#line 2486 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE NO OTHERS"); }
#line 10164 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 452:
#line 2491 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10174 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 453:
#line 2497 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10184 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 455:
#line 2508 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 10193 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 458:
#line 2520 "sqlparser_hana.yacc" /* glr.c:816  */
    {
        ((*yyvalp).node) = Node::makeNonTerminalNode(E_POINT_VALUE, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
        ((*yyvalp).node)->serialize_format = &POINT_VALUE_FORMAT;
}
#line 10202 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 459:
#line 2525 "sqlparser_hana.yacc" /* glr.c:816  */
    {
        ((*yyvalp).node) = Node::makeNonTerminalNode(E_POINT_VALUE, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
        ((*yyvalp).node)->serialize_format = &POINT_VALUE_FORMAT;
}
#line 10211 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 460:
#line 2536 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10221 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 461:
#line 2542 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "RIGHT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10232 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 462:
#line 2549 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "RIGHT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        fun_name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10243 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 463:
#line 2556 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* expr_list = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    expr_list->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;

    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "LEFT");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, expr_list, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10257 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 464:
#line 2566 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CAST");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_AS_SERIALIZE_FORMAT;
}
#line 10268 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 465:
#line 2573 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* transcoding_name = Node::makeTerminalNode(E_STRING, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text());
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT");
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        		fun_name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), nullptr, nullptr, transcoding_name);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_USING_SERIALIZE_FORMAT;
}
#line 10281 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 466:
#line 2582 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10292 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 467:
#line 2589 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10303 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 468:
#line 2596 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10314 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 469:
#line 2603 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_USER");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10325 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 470:
#line 2610 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* expr_list = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    expr_list->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, expr_list, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10338 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 471:
#line 2619 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SESSION_USER");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10349 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 472:
#line 2626 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SYSTEM_USER");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    ((*yyvalp).node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10360 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 473:
#line 2635 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* xml = Node::makeTerminalNode(E_STRING, "XML");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FOR_CLAUSE_HANA, 4, xml, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FOR_CLAUSE_FORMAT;
}
#line 10370 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 474:
#line 2642 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node* json = Node::makeTerminalNode(E_STRING, "JSON");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FOR_CLAUSE_HANA, 4, json, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FOR_CLAUSE_FORMAT;
}
#line 10380 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 475:
#line 2649 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = NULL;}
#line 10386 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 476:
#line 2651 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_RETURNS_CLAUSE, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &RETURNS_CLAUSE_FORMAT;
}
#line 10395 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 477:
#line 2657 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = NULL;}
#line 10401 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 478:
#line 2659 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 10410 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 480:
#line 2667 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 10419 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 481:
#line 2674 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OPTION_STRING, 2, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OPTION_STRING_FORMAT;
}
#line 10428 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 484:
#line 2691 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node);
    (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->set_text( (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text() + " ARRAY ");
}
#line 10437 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 485:
#line 2735 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "DATE");}
#line 10443 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 486:
#line 2736 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TIME");}
#line 10449 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 487:
#line 2737 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "SECONDDATE");}
#line 10455 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 488:
#line 2738 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP");}
#line 10461 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 489:
#line 2739 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TINYINT");}
#line 10467 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 490:
#line 2740 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "SMALLINT");}
#line 10473 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 491:
#line 2741 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "INTEGER");}
#line 10479 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 492:
#line 2742 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "INT");  }
#line 10485 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 493:
#line 2743 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "BIGINT");}
#line 10491 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 494:
#line 2744 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "SMALLDECIMAL");}
#line 10497 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 495:
#line 2745 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "REAL");}
#line 10503 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 496:
#line 2746 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "DOUBLE");}
#line 10509 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 497:
#line 2747 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TEXT");}
#line 10515 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 498:
#line 2748 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "BINTEXT");}
#line 10521 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 499:
#line 2749 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "VARCHAR");}
#line 10527 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 500:
#line 2750 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "VARCHAR("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10533 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 501:
#line 2751 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "NVARCHAR");}
#line 10539 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 502:
#line 2752 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "NVARCHAR("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10545 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 503:
#line 2753 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "ALPHANUM");}
#line 10551 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 504:
#line 2754 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "ALPHANUM("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10557 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 505:
#line 2755 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "VARBINARY");}
#line 10563 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 506:
#line 2756 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "VARBINARY("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10569 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 507:
#line 2757 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "SHORTTEXT");}
#line 10575 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 508:
#line 2758 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "SHORTTEXT("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10581 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 509:
#line 2759 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "DECIMAL");}
#line 10587 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 510:
#line 2760 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10593 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 511:
#line 2761 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text()+","+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10599 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 512:
#line 2762 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "FLOAT");}
#line 10605 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 513:
#line 2763 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "FLOAT("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")"); delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)); }
#line 10611 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 514:
#line 2764 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "BOOLEAN");}
#line 10617 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 516:
#line 2768 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "BLOB");}
#line 10623 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 517:
#line 2769 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "CLOB");}
#line 10629 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 518:
#line 2770 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "NCLOB");}
#line 10635 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 519:
#line 2858 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "YEAR");
}
#line 10643 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 520:
#line 2862 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "MONTH");
}
#line 10651 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 521:
#line 2866 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "DAY");
}
#line 10659 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 522:
#line 2870 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "HOUR");
}
#line 10667 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 523:
#line 2874 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "MINUTE");
}
#line 10675 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 524:
#line 2878 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "SECOND");
}
#line 10683 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 525:
#line 3253 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).ival) = 0; }
#line 10689 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 526:
#line 3254 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).ival) = 1; }
#line 10695 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 527:
#line 3255 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).ival) = 2; }
#line 10701 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 528:
#line 3259 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 10707 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 530:
#line 3264 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node); }
#line 10713 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 533:
#line 3275 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "COLLATE "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text());
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 10722 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 538:
#line 3287 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LOCATE_REGEXPR"); }
#line 10728 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 539:
#line 3288 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OCCURRENCES_REGEXPR"); }
#line 10734 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 540:
#line 3289 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "REPLACE_REGEXPR"); }
#line 10740 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 541:
#line 3290 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTRING_REGEXPR"); }
#line 10746 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 542:
#line 3291 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTR_REGEXPR"); }
#line 10752 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 543:
#line 3292 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "STRING_AGG"); }
#line 10758 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 544:
#line 3293 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CHAR"); }
#line 10764 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 545:
#line 3294 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CONTAINS"); }
#line 10770 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 546:
#line 3295 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BINNING"); }
#line 10776 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 547:
#line 3296 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "EXTRACT"); }
#line 10782 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 548:
#line 3297 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "FIRST_VALUE"); }
#line 10788 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 549:
#line 3298 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LAST_VALUE"); }
#line 10794 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 550:
#line 3299 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "NTH_VALUE"); }
#line 10800 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 551:
#line 3300 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "NTILE"); }
#line 10806 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 552:
#line 3301 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_CONT"); }
#line 10812 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 553:
#line 3302 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_DISC"); }
#line 10818 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 554:
#line 3303 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TRIM"); }
#line 10824 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 555:
#line 3304 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "COUNT"); }
#line 10830 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 556:
#line 3305 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "WEIGHT");  }
#line 10836 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 557:
#line 3306 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "FULLTEXT");  }
#line 10842 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 558:
#line 3307 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LANGUAGE");  }
#line 10848 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 559:
#line 3308 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LINGUISTIC");  }
#line 10854 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 560:
#line 3309 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "FUZZY");  }
#line 10860 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 561:
#line 3310 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ANY");  }
#line 10866 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 565:
#line 3320 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "K"); }
#line 10872 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 566:
#line 3321 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "M"); }
#line 10878 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 567:
#line 3322 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "G"); }
#line 10884 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 568:
#line 3323 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ARRAY"); }
#line 10890 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 569:
#line 3324 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BINARY"); }
#line 10896 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 570:
#line 3325 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CAST"); }
#line 10902 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 571:
#line 3326 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CHARACTERS"); }
#line 10908 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 572:
#line 3327 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CODE_UNITS"); }
#line 10914 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 573:
#line 3328 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CORRESPONDING"); }
#line 10920 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 574:
#line 3329 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "FOLLOWING"); }
#line 10926 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 575:
#line 3330 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "INTERVAL"); }
#line 10932 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 576:
#line 3331 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LARGE"); }
#line 10938 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 577:
#line 3332 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MULTISET"); }
#line 10944 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 578:
#line 3333 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OBJECT"); }
#line 10950 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 579:
#line 3334 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OCTETS"); }
#line 10956 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 580:
#line 3335 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ONLY"); }
#line 10962 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 581:
#line 3336 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECEDING"); }
#line 10968 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 582:
#line 3337 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECISION"); }
#line 10974 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 583:
#line 3338 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "RECURSIVE"); }
#line 10980 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 584:
#line 3339 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "REF"); }
#line 10986 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 585:
#line 3340 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW"); }
#line 10992 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 586:
#line 3341 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SCOPE"); }
#line 10998 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 587:
#line 3342 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "UNBOUNDED"); }
#line 11004 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 588:
#line 3343 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "VARCHAR"); }
#line 11010 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 589:
#line 3344 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "WITHOUT"); }
#line 11016 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 590:
#line 3345 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ZONE"); }
#line 11022 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 591:
#line 3346 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OF"); }
#line 11028 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 592:
#line 3347 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "READ"); }
#line 11034 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 593:
#line 3348 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TIMESTAMP"); }
#line 11040 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 594:
#line 3349 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TIME");  }
#line 11046 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 595:
#line 3350 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "DESC"); }
#line 11052 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 596:
#line 3351 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TIES"); }
#line 11058 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 597:
#line 3352 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SETS"); }
#line 11064 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 598:
#line 3353 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OTHERS"); }
#line 11070 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 599:
#line 3354 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "EXCLUDE"); }
#line 11076 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 600:
#line 3355 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ASC"); }
#line 11082 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 601:
#line 3356 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE"); }
#line 11088 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 602:
#line 3357 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT"); }
#line 11094 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 603:
#line 3358 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF"); }
#line 11100 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 604:
#line 3359 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "DEFAULT"); }
#line 11106 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 605:
#line 3360 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TO"); }
#line 11112 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 606:
#line 3361 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "DOUBLE"); }
#line 11118 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 607:
#line 3362 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MOD"); }
#line 11124 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 608:
#line 3363 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MANY"); }
#line 11130 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 609:
#line 3364 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ONE"); }
#line 11136 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 610:
#line 3365 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PRIMARY"); }
#line 11142 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 611:
#line 3366 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "KEY"); }
#line 11148 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 612:
#line 3367 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SECONDDATE"); }
#line 11154 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 613:
#line 3368 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TINYINT"); }
#line 11160 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 614:
#line 3369 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SMALLDECIMAL"); }
#line 11166 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 615:
#line 3370 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TEXT"); }
#line 11172 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 616:
#line 3371 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BINTEXT"); }
#line 11178 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 617:
#line 3372 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ALPHANUM"); }
#line 11184 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 618:
#line 3373 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "VARBINARY"); }
#line 11190 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 619:
#line 3374 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SHORTTEXT"); }
#line 11196 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 620:
#line 3375 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "REPLACE"); }
#line 11202 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 621:
#line 3376 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PARAMETERS"); }
#line 11208 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 622:
#line 3377 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "HISTORY"); }
#line 11214 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 623:
#line 3378 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OVERRIDING"); }
#line 11220 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 624:
#line 3379 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SYSTEM"); }
#line 11226 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 625:
#line 3380 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "USER"); }
#line 11232 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 626:
#line 3381 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "VALUE"); }
#line 11238 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 627:
#line 3382 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "EXACT"); }
#line 11244 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 628:
#line 3383 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BERNOULLI"); }
#line 11250 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 629:
#line 3384 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "NCHAR"); }
#line 11256 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 630:
#line 3387 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BEST"); }
#line 11262 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 631:
#line 3388 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SUBTOTAL"); }
#line 11268 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 632:
#line 3389 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BALANCE"); }
#line 11274 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 633:
#line 3390 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TOTAL"); }
#line 11280 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 634:
#line 3391 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MULTIPLE"); }
#line 11286 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 635:
#line 3392 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "RESULTSETS"); }
#line 11292 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 636:
#line 3393 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PREFIX"); }
#line 11298 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 637:
#line 3394 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "STRUCTURED"); }
#line 11304 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 638:
#line 3395 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "RESULT"); }
#line 11310 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 639:
#line 3396 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "OVERVIEW"); }
#line 11316 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 640:
#line 3397 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TEXT_FILTER"); }
#line 11322 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 641:
#line 3398 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "FILL"); }
#line 11328 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 642:
#line 3399 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "UP"); }
#line 11334 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 643:
#line 3400 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "MATCHES"); }
#line 11340 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 644:
#line 3401 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SORT"); }
#line 11346 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 645:
#line 3402 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ROWCOUNT"); }
#line 11352 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 646:
#line 3403 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LOCKED"); }
#line 11358 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 647:
#line 3405 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SHARE"); }
#line 11364 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 648:
#line 3406 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LOCK"); }
#line 11370 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 649:
#line 3407 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "UTCTIMESTAMP"); }
#line 11376 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 650:
#line 3408 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "COMMIT"); }
#line 11382 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 651:
#line 3409 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ID"); }
#line 11388 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 652:
#line 3410 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "JSON"); }
#line 11394 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 653:
#line 3411 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "XML"); }
#line 11400 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 654:
#line 3414 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "SERIES"); }
#line 11406 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 655:
#line 3415 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TABLE");  }
#line 11412 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 656:
#line 3416 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "ORDINALITY");  }
#line 11418 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 657:
#line 3417 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "FLAG");  }
#line 11424 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 658:
#line 3418 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LEADING");  }
#line 11430 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 659:
#line 3419 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "TRAILING");  }
#line 11436 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 660:
#line 3420 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "BOTH");  }
#line 11442 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 661:
#line 3421 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "FIRST");  }
#line 11448 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 662:
#line 3422 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "LAST");  }
#line 11454 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 663:
#line 3423 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "PORTION");  }
#line 11460 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 664:
#line 3424 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "START");  }
#line 11466 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 665:
#line 3425 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_IDENTIFIER, "AFTER");  }
#line 11472 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 666:
#line 3430 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11478 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 667:
#line 3432 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TOP_CLAUSE, E_OP_UNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TOP_CLAUSE_FORMAT;
}
#line 11487 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 668:
#line 3437 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_TOP_CLAUSE, E_OP_UNARY_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &TOP_CLAUSE_FORMAT;
}
#line 11496 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 669:
#line 3461 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr;}
#line 11502 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 671:
#line 3466 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, " FOR SHARE LOCK "); }
#line 11508 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 672:
#line 3470 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FOR_UPDATE, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FOR_UPDATE_FORMAT_HANA;
}
#line 11517 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 676:
#line 3480 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = nullptr;}
#line 11523 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 677:
#line 3482 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * p_of = Node::makeTerminalNode(E_STRING, "OF");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OF_COLUMS, 2, p_of, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 11533 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 678:
#line 3489 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = nullptr;}
#line 11539 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 679:
#line 3490 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "IGNORE LOCKED");}
#line 11545 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 680:
#line 3493 "sqlparser_hana.yacc" /* glr.c:816  */
    {((*yyvalp).node) = nullptr;}
#line 11551 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 681:
#line 3494 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = Node::makeTerminalNode(E_NOWAIT, "NOWAIT");}
#line 11557 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 682:
#line 3496 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_WAIT_INT, 1,  (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &WAIT_TIME_FORMAT;
}
#line 11566 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 683:
#line 3503 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11572 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 684:
#line 3505 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_LIMIT_NUM, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &LIMIT_NUM_FORMAT_HANA;
}
#line 11581 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 685:
#line 3510 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_LIMIT_NUM, 3, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &LIMIT_NUM_FORMAT_HANA;
}
#line 11590 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 686:
#line 3517 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11596 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 687:
#line 3519 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_OFFSET_NUM, 1, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &OFFSET_NUM_FORMAT;
}
#line 11605 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 688:
#line 3526 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11611 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 689:
#line 3528 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "TOTAL ROWCOUNT");
}
#line 11619 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 693:
#line 3540 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 11628 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 694:
#line 3547 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11634 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 695:
#line 3549 "sqlparser_hana.yacc" /* glr.c:816  */
    {
     std::string ss = (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text();
     delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    std::string sval = "WITH HINT(" + ss + ")";
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        sval += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11649 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 696:
#line 3560 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string ss = (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    std::string sval = "WITH RANGE_RESTRICTION(" + ss + ")";
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        sval += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11664 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 697:
#line 3571 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string ss1 = (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node));
    std::string ss2 = (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    std::string sval = "WITH PARAMETERS(" + ss1 +"=" + ss2 + ")";
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        sval += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11681 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 698:
#line 3586 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11687 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 699:
#line 3588 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string sval = "WITH RANGE_RESTRICTION(" + (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text() + ")";
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11697 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 700:
#line 3597 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "WITH PRIMARY KEY");
}
#line 11705 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 701:
#line 3604 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_UPSERT, 6, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), NULL,NULL);
    ((*yyvalp).node)->serialize_format = &UPSERT_SERIALIZE_FORMAT;
}
#line 11714 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 702:
#line 3609 "sqlparser_hana.yacc" /* glr.c:816  */
    {

    ((*yyvalp).node) = Node::makeNonTerminalNode(E_UPSERT, 6, (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &UPSERT_SERIALIZE_FORMAT;
}
#line 11724 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 703:
#line 3615 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_UPSERT, 6, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), NULL, NULL,NULL);
    ((*yyvalp).node)->serialize_format = &UPSERT_SERIALIZE_FORMAT;
}
#line 11733 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 704:
#line 3622 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_REPLACE, 5, (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), NULL);
    ((*yyvalp).node)->serialize_format = &REPLACE_SERIALIZE_FORMAT;
}
#line 11742 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 705:
#line 3627 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * pwith = Node::makeTerminalNode(E_STRING, " WITH PRIMARY KEY");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_REPLACE, 5, (((yyGLRStackItem const *)yyvsp)[YYFILL (-5)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node), NULL, pwith);
    ((*yyvalp).node)->serialize_format = &REPLACE_SERIALIZE_FORMAT;
}
#line 11752 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 706:
#line 3633 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_REPLACE, 5, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node), NULL, NULL);
    ((*yyvalp).node)->serialize_format = &REPLACE_SERIALIZE_FORMAT;
}
#line 11761 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 710:
#line 3645 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "FOR SYSTEM_TIME AS OF "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text());
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 11770 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 711:
#line 3652 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "FOR SYSTEM_TIME FROM "+(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text()+" TO "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text());
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 11780 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 712:
#line 3660 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "FOR SYSTEM_TIME BETWEEN "+(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text()+" AND "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text());
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 11790 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 713:
#line 3668 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING, "FOR APPLICATION_TIME AS OF "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text());
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 11799 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 714:
#line 3675 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "PARTITION("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")";
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING,val );
}
#line 11809 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 716:
#line 3684 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 11818 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 717:
#line 3691 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "TABLESAMPLE BERNOULLI("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")";
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING,val );
}
#line 11828 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 718:
#line 3697 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "TABLESAMPLE SYSTEM("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")";
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING,val );
}
#line 11838 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 719:
#line 3703 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = "TABLESAMPLE("+(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text()+")";
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node) = Node::makeTerminalNode(E_STRING,val );
}
#line 11848 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 720:
#line 3711 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11854 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 722:
#line 3714 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr; }
#line 11860 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 724:
#line 3719 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string ss = "FOR PORTION OF APPLICATION_TIME FROM ";
    ss += (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    ss += " TO ";
    ss += (((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, ss);
}
#line 11874 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 725:
#line 3732 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LOCATE_REGEXPR");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node), (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 11884 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 726:
#line 3738 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "OCCURRENCES_REGEXPR");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 11894 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 727:
#line 3744 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "REPLACE_REGEXPR");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 11904 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 728:
#line 3750 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTRING_REGEXPR");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 11914 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 729:
#line 3756 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTR_REGEXPR");
    ((*yyvalp).node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node), nullptr);
    ((*yyvalp).node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 11924 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 730:
#line 3764 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    std::string val = (((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-7)].yystate.yysemantics.yysval.node));
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node)){
        val += " ";
        val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-6)].yystate.yysemantics.yysval.node));
    }
    val +=" IN ";
    val += (((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node)->text();
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-4)].yystate.yysemantics.yysval.node));
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)){
        val+= " ";
        val+=(((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-3)].yystate.yysemantics.yysval.node));
    }
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)){
        val+= " ";
        val+=(((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-2)].yystate.yysemantics.yysval.node));
    }
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)){
        val+= " ";
        val+=(((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (-1)].yystate.yysemantics.yysval.node));
    }
    if((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)){
        val+= " ";
        val+=(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text();
        delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
    }
    ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, val );
}
#line 11962 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 731:
#line 3798 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr;}
#line 11968 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 732:
#line 3799 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "START" ); }
#line 11974 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 733:
#line 3800 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "AFTER" ); }
#line 11980 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 734:
#line 3803 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node) = nullptr;}
#line 11986 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 735:
#line 3805 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "FLAG "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() );
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 11995 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 736:
#line 3810 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)=nullptr;}
#line 12001 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 737:
#line 3812 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "WITH "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() );
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 12010 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 738:
#line 3817 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)=nullptr;}
#line 12016 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 739:
#line 3819 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "FROM "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() );
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 12025 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 740:
#line 3824 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)=nullptr;}
#line 12031 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 741:
#line 3826 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "OCCURRENCE "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() );
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 12040 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 742:
#line 3830 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "OCCURRENCE ALL"); }
#line 12046 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 743:
#line 3833 "sqlparser_hana.yacc" /* glr.c:816  */
    { ((*yyvalp).node)=nullptr;}
#line 12052 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;

  case 744:
#line 3835 "sqlparser_hana.yacc" /* glr.c:816  */
    {
    ((*yyvalp).node)= Node::makeTerminalNode(E_STRING, "GROUP "+(((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node)->text() );
    delete((((yyGLRStackItem const *)yyvsp)[YYFILL (0)].yystate.yysemantics.yysval.node));
}
#line 12061 "sqlparser_hana_bison.cpp" /* glr.c:816  */
    break;


#line 12065 "sqlparser_hana_bison.cpp" /* glr.c:816  */
      default: break;
    }

  return yyok;
# undef yyerrok
# undef YYABORT
# undef YYACCEPT
# undef YYERROR
# undef YYBACKUP
# undef yyclearin
# undef YYRECOVERING
}


static void
yyuserMerge (int yyn, YYSTYPE* yy0, YYSTYPE* yy1)
{
  YYUSE (yy0);
  YYUSE (yy1);

  switch (yyn)
    {

      default: break;
    }
}

                              /* Bison grammar-table manipulation.  */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yytype)
    {
          case 3: /* NAME  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12116 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 4: /* STRING  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12122 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 5: /* INTNUM  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12128 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 6: /* BOOL  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12134 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 7: /* APPROXNUM  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12140 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 8: /* NULLX  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12146 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 9: /* UNKNOWN  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12152 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 10: /* QUESTIONMARK  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12158 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 296: /* sql_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12164 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 297: /* stmt_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12170 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 298: /* stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12176 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 299: /* call_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12182 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 300: /* sql_argument_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12188 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 301: /* sql_argument  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12194 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 302: /* value_expression  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12200 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 303: /* sp_name  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12206 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 304: /* dql_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12212 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 305: /* dml_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12218 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 306: /* opt_partition_rest  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12224 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 307: /* opt_column_ref_list_with_parens  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12230 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 308: /* overriding_value  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12236 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 309: /* insert_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12242 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 310: /* from_constructor  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12248 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 311: /* delete_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12254 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 312: /* update_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12260 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 313: /* delete_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12266 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 314: /* update_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12272 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 315: /* update_elem_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12278 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 316: /* update_elem  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12284 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 317: /* select_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12290 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 318: /* query_expression  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12296 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 319: /* query_expression_body  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12302 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 320: /* query_term  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12308 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 321: /* query_primary  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12314 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 322: /* select_with_parens  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12320 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 323: /* subquery  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12326 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 324: /* sap_table_subquery  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12332 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 325: /* table_subquery  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12338 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 326: /* row_subquery  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12344 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 327: /* simple_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12350 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 328: /* opt_where  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12356 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 329: /* from_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12362 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 330: /* opt_from_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12368 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 331: /* opt_groupby  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12374 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 332: /* grouping_element_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12380 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 333: /* grouping_element  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12386 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 334: /* row_order_by_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12392 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 335: /* row_order_by  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12398 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 336: /* column_ref_perens  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12404 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 337: /* group_set_name  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12410 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 338: /* grouping_option  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12416 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 339: /* grouping_best  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12422 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 340: /* grouping_subtotal  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12428 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 341: /* grouping_prefix_tb  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12434 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 342: /* grouping_prefix  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12440 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 343: /* grouping_structured_res  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12446 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 344: /* grouping_text_filter  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12452 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 345: /* opt_asc_desc  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12458 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 346: /* opt_having  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12464 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 347: /* with_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12470 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 348: /* with_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12476 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 349: /* common_table_expr  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12482 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 350: /* opt_derived_column_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12488 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 351: /* simple_ident_list_with_parens  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12494 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 352: /* simple_ident_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12500 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 353: /* column_ref_list_with_parens  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12506 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 354: /* column_ref_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12512 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 355: /* opt_distinct  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12518 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 356: /* select_expr_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12524 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 357: /* projection  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12530 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 358: /* from_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12536 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 359: /* table_reference  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12542 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 360: /* table_primary  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12548 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 361: /* table_primary_non_join  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12554 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 362: /* lateral_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12560 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 363: /* associated_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12566 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 364: /* associated_ref_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12572 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 365: /* associated_ref  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12578 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 366: /* opt_many2one_part  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12584 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 367: /* opt_search_condition  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12590 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 368: /* opt_tablesample  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12596 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 369: /* opt_table_qualify  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12602 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 370: /* collection_derived_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12608 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 371: /* collection_value_expr_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12614 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 372: /* collection_value_expr  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12620 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 373: /* as_derived_part  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12626 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 374: /* column_ref  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12632 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 375: /* relation_factor  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12638 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 376: /* func_relation_factor  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12644 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 377: /* joined_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12650 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 378: /* qualified_join  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12656 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 379: /* case_join  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12662 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 380: /* case_join_when_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12668 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 381: /* case_join_when  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12674 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 382: /* ret_join_on  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12680 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 383: /* select_expr_list_parens  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12686 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 384: /* opt_case_join_else  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12692 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 385: /* cross_join  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12698 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 386: /* join_type  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12704 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 388: /* join_cardinality  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12710 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 389: /* hana_construct_table  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12716 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 390: /* construct_column_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12722 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 391: /* construct_column  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12728 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 392: /* search_condition  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12734 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 393: /* boolean_term  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12740 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 394: /* boolean_factor  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12746 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 395: /* boolean_test  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12752 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 396: /* boolean_primary  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12758 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 397: /* predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12764 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 398: /* like_regexpr_redicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12770 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 399: /* member_of_predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12776 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 400: /* bool_function  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12782 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 401: /* contains_param3  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12788 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 402: /* search_type  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12794 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 403: /* search_param_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12800 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 404: /* search_param  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12806 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 405: /* expr_const_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12812 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 406: /* comparison_predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12818 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 407: /* between_predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12824 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 408: /* like_predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12830 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 409: /* in_predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12836 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 410: /* null_predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12842 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 411: /* exists_predicate  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12848 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 412: /* row_expr  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12854 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 413: /* factor0  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12860 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 414: /* factor1  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12866 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 415: /* factor2  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12872 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 416: /* factor3  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12878 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 417: /* factor4  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12884 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 418: /* row_expr_list_parens  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12890 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 419: /* row_expr_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12896 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 420: /* row_expr_star  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12902 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 421: /* in_expr  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12908 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 422: /* truth_value  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12914 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 428: /* expr_const  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12920 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 429: /* case_expr  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12926 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 430: /* case_arg  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12932 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 431: /* when_clause_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12938 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 432: /* when_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12944 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 433: /* case_default  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12950 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 434: /* func_expr  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12956 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 435: /* opt_nulls  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12962 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 436: /* aggregate_expression  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12968 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 437: /* aggregate_name  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12974 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 438: /* sap_specific_function  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12980 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 439: /* trim_char  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12986 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 440: /* within_group  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12992 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 441: /* opt_order_by_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 12998 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 442: /* order_by_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13004 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 443: /* order_by_expression_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13010 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 444: /* order_by_expression  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13016 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 445: /* ranking_windowed_function  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13022 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 446: /* ranking_function_name  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13028 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 447: /* opt_over_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13034 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 448: /* over_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13040 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 449: /* window_specification  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13046 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 450: /* window_name  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13052 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 451: /* window_specification_details  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13058 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 452: /* opt_existing_window_name  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13064 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 453: /* opt_window_partition_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13070 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 454: /* opt_window_frame_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13076 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 455: /* window_frame_units  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13082 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 456: /* window_frame_extent  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13088 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 457: /* window_frame_start  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13094 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 458: /* window_frame_preceding  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13100 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 459: /* window_frame_between  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13106 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 460: /* window_frame_bound  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13112 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 461: /* window_frame_following  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13118 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 462: /* opt_window_frame_exclusion  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13124 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 463: /* scalar_function  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13130 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 464: /* table_function_param_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13136 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 465: /* table_function_param  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13142 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 466: /* expr_point_val  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13148 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 467: /* function_call_keyword  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13154 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 468: /* for_xml  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13160 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 469: /* for_json  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13166 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 470: /* opt_returns_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13172 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 471: /* opt_option_string_list_p  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13178 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 472: /* option_string_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13184 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 473: /* option_string  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13190 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 474: /* data_type  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13196 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 475: /* array_type  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13202 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 476: /* predefined_type  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13208 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 477: /* lob_data_type  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13214 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 478: /* primary_datetime_field  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13220 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 480: /* opt_as_label  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13226 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 481: /* as_label  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13232 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 482: /* label  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13238 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 483: /* collate_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13244 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 484: /* name_r  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13250 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 485: /* name_f  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13256 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 486: /* reserved  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13262 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 487: /* top_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13268 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 488: /* opt_for_update  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13274 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 489: /* for_share_lock  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13280 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 490: /* for_update  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13286 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 491: /* opt_of_ident_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13292 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 492: /* opt_ignore_lock  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13298 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 493: /* wait_nowait  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13304 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 494: /* limit_num  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13310 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 495: /* offset_num  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13316 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 496: /* limit_total  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13322 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 497: /* with_hint_param  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13328 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 498: /* with_hint_param_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13334 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 499: /* hint_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13340 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 500: /* opt_with_range_restrict  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13346 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 501: /* with_primary_key  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13352 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 502: /* upsert_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13358 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 503: /* replace_stmt  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13364 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 504: /* for_system_time  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13370 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 505: /* sys_as_of_spec  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13376 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 506: /* sys_from_to_spec  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13382 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 507: /* sys_between_spec  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13388 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 508: /* for_application_time  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13394 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 509: /* partition_restriction  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13400 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 510: /* intnum_list  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13406 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 511: /* tablesample_num  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13412 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 512: /* opt_partition_restriction  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13418 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 513: /* opt_for_application_time_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13424 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 514: /* for_application_time_clause  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13430 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 515: /* regexpr_str_function  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13436 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 516: /* regex_parameter  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13442 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 517: /* opt_st_or_af  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13448 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 518: /* opt_flag  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13454 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 519: /* opt_with_string  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13460 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 520: /* opt_from_position  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13466 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 521: /* opt_occurrence_num  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13472 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 522: /* opt_group_num  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13478 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;

    case 523: /* param_num  */
#line 100 "sqlparser_hana.yacc" /* glr.c:846  */
      { delete(((*yyvaluep).node)); }
#line 13484 "sqlparser_hana_bison.cpp" /* glr.c:846  */
        break;


      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}

/** Number of symbols composing the right hand side of rule #RULE.  */
static inline int
yyrhsLength (yyRuleNum yyrule)
{
  return yyr2[yyrule];
}

static void
yydestroyGLRState (char const *yymsg, yyGLRState *yys, ParseResult* result, yyscan_t scanner)
{
  if (yys->yyresolved)
    yydestruct (yymsg, yystos[yys->yylrState],
                &yys->yysemantics.yysval, &yys->yyloc, result, scanner);
  else
    {
#if HANA_DEBUG
      if (yydebug)
        {
          if (yys->yysemantics.yyfirstVal)
            YYFPRINTF (stderr, "%s unresolved", yymsg);
          else
            YYFPRINTF (stderr, "%s incomplete", yymsg);
          YY_SYMBOL_PRINT ("", yystos[yys->yylrState], YY_NULLPTR, &yys->yyloc);
        }
#endif

      if (yys->yysemantics.yyfirstVal)
        {
          yySemanticOption *yyoption = yys->yysemantics.yyfirstVal;
          yyGLRState *yyrh;
          int yyn;
          for (yyrh = yyoption->yystate, yyn = yyrhsLength (yyoption->yyrule);
               yyn > 0;
               yyrh = yyrh->yypred, yyn -= 1)
            yydestroyGLRState (yymsg, yyrh, result, scanner);
        }
    }
}

/** Left-hand-side symbol for rule #YYRULE.  */
static inline yySymbol
yylhsNonterm (yyRuleNum yyrule)
{
  return yyr1[yyrule];
}

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-1048)))

/** True iff LR state YYSTATE has only a default reduction (regardless
 *  of token).  */
static inline yybool
yyisDefaultedState (yyStateNum yystate)
{
  return yypact_value_is_default (yypact[yystate]);
}

/** The default reduction for YYSTATE, assuming it has one.  */
static inline yyRuleNum
yydefaultAction (yyStateNum yystate)
{
  return yydefact[yystate];
}

#define yytable_value_is_error(Yytable_value) \
  0

/** Set *YYACTION to the action to take in YYSTATE on seeing YYTOKEN.
 *  Result R means
 *    R < 0:  Reduce on rule -R.
 *    R = 0:  Error.
 *    R > 0:  Shift to state R.
 *  Set *YYCONFLICTS to a pointer into yyconfl to a 0-terminated list
 *  of conflicting reductions.
 */
static inline void
yygetLRActions (yyStateNum yystate, int yytoken,
                int* yyaction, const short int** yyconflicts)
{
  int yyindex = yypact[yystate] + yytoken;
  if (yypact_value_is_default (yypact[yystate])
      || yyindex < 0 || YYLAST < yyindex || yycheck[yyindex] != yytoken)
    {
      *yyaction = -yydefact[yystate];
      *yyconflicts = yyconfl;
    }
  else if (! yytable_value_is_error (yytable[yyindex]))
    {
      *yyaction = yytable[yyindex];
      *yyconflicts = yyconfl + yyconflp[yyindex];
    }
  else
    {
      *yyaction = 0;
      *yyconflicts = yyconfl + yyconflp[yyindex];
    }
}

/** Compute post-reduction state.
 * \param yystate   the current state
 * \param yysym     the nonterminal to push on the stack
 */
static inline yyStateNum
yyLRgotoState (yyStateNum yystate, yySymbol yysym)
{
  int yyr = yypgoto[yysym - YYNTOKENS] + yystate;
  if (0 <= yyr && yyr <= YYLAST && yycheck[yyr] == yystate)
    return yytable[yyr];
  else
    return yydefgoto[yysym - YYNTOKENS];
}

static inline yybool
yyisShiftAction (int yyaction)
{
  return 0 < yyaction;
}

static inline yybool
yyisErrorAction (int yyaction)
{
  return yyaction == 0;
}

                                /* GLRStates */

/** Return a fresh GLRStackItem in YYSTACKP.  The item is an LR state
 *  if YYISSTATE, and otherwise a semantic option.  Callers should call
 *  YY_RESERVE_GLRSTACK afterwards to make sure there is sufficient
 *  headroom.  */

static inline yyGLRStackItem*
yynewGLRStackItem (yyGLRStack* yystackp, yybool yyisState)
{
  yyGLRStackItem* yynewItem = yystackp->yynextFree;
  yystackp->yyspaceLeft -= 1;
  yystackp->yynextFree += 1;
  yynewItem->yystate.yyisState = yyisState;
  return yynewItem;
}

/** Add a new semantic action that will execute the action for rule
 *  YYRULE on the semantic values in YYRHS to the list of
 *  alternative actions for YYSTATE.  Assumes that YYRHS comes from
 *  stack #YYK of *YYSTACKP. */
static void
yyaddDeferredAction (yyGLRStack* yystackp, size_t yyk, yyGLRState* yystate,
                     yyGLRState* yyrhs, yyRuleNum yyrule)
{
  yySemanticOption* yynewOption =
    &yynewGLRStackItem (yystackp, yyfalse)->yyoption;
  YYASSERT (!yynewOption->yyisState);
  yynewOption->yystate = yyrhs;
  yynewOption->yyrule = yyrule;
  if (yystackp->yytops.yylookaheadNeeds[yyk])
    {
      yynewOption->yyrawchar = yychar;
      yynewOption->yyval = yylval;
      yynewOption->yyloc = yylloc;
    }
  else
    yynewOption->yyrawchar = YYEMPTY;
  yynewOption->yynext = yystate->yysemantics.yyfirstVal;
  yystate->yysemantics.yyfirstVal = yynewOption;

  YY_RESERVE_GLRSTACK (yystackp);
}

                                /* GLRStacks */

/** Initialize YYSET to a singleton set containing an empty stack.  */
static yybool
yyinitStateSet (yyGLRStateSet* yyset)
{
  yyset->yysize = 1;
  yyset->yycapacity = 16;
  yyset->yystates = (yyGLRState**) YYMALLOC (16 * sizeof yyset->yystates[0]);
  if (! yyset->yystates)
    return yyfalse;
  yyset->yystates[0] = YY_NULLPTR;
  yyset->yylookaheadNeeds =
    (yybool*) YYMALLOC (16 * sizeof yyset->yylookaheadNeeds[0]);
  if (! yyset->yylookaheadNeeds)
    {
      YYFREE (yyset->yystates);
      return yyfalse;
    }
  return yytrue;
}

static void yyfreeStateSet (yyGLRStateSet* yyset)
{
  YYFREE (yyset->yystates);
  YYFREE (yyset->yylookaheadNeeds);
}

/** Initialize *YYSTACKP to a single empty stack, with total maximum
 *  capacity for all stacks of YYSIZE.  */
static yybool
yyinitGLRStack (yyGLRStack* yystackp, size_t yysize)
{
  yystackp->yyerrState = 0;
  yynerrs = 0;
  yystackp->yyspaceLeft = yysize;
  yystackp->yyitems =
    (yyGLRStackItem*) YYMALLOC (yysize * sizeof yystackp->yynextFree[0]);
  if (!yystackp->yyitems)
    return yyfalse;
  yystackp->yynextFree = yystackp->yyitems;
  yystackp->yysplitPoint = YY_NULLPTR;
  yystackp->yylastDeleted = YY_NULLPTR;
  return yyinitStateSet (&yystackp->yytops);
}


#if YYSTACKEXPANDABLE
# define YYRELOC(YYFROMITEMS,YYTOITEMS,YYX,YYTYPE) \
  &((YYTOITEMS) - ((YYFROMITEMS) - (yyGLRStackItem*) (YYX)))->YYTYPE

/** If *YYSTACKP is expandable, extend it.  WARNING: Pointers into the
    stack from outside should be considered invalid after this call.
    We always expand when there are 1 or fewer items left AFTER an
    allocation, so that we can avoid having external pointers exist
    across an allocation.  */
static void
yyexpandGLRStack (yyGLRStack* yystackp)
{
  yyGLRStackItem* yynewItems;
  yyGLRStackItem* yyp0, *yyp1;
  size_t yynewSize;
  size_t yyn;
  size_t yysize = yystackp->yynextFree - yystackp->yyitems;
  if (YYMAXDEPTH - YYHEADROOM < yysize)
    yyMemoryExhausted (yystackp);
  yynewSize = 2*yysize;
  if (YYMAXDEPTH < yynewSize)
    yynewSize = YYMAXDEPTH;
  yynewItems = (yyGLRStackItem*) YYMALLOC (yynewSize * sizeof yynewItems[0]);
  if (! yynewItems)
    yyMemoryExhausted (yystackp);
  for (yyp0 = yystackp->yyitems, yyp1 = yynewItems, yyn = yysize;
       0 < yyn;
       yyn -= 1, yyp0 += 1, yyp1 += 1)
    {
      *yyp1 = *yyp0;
      if (*(yybool *) yyp0)
        {
          yyGLRState* yys0 = &yyp0->yystate;
          yyGLRState* yys1 = &yyp1->yystate;
          if (yys0->yypred != YY_NULLPTR)
            yys1->yypred =
              YYRELOC (yyp0, yyp1, yys0->yypred, yystate);
          if (! yys0->yyresolved && yys0->yysemantics.yyfirstVal != YY_NULLPTR)
            yys1->yysemantics.yyfirstVal =
              YYRELOC (yyp0, yyp1, yys0->yysemantics.yyfirstVal, yyoption);
        }
      else
        {
          yySemanticOption* yyv0 = &yyp0->yyoption;
          yySemanticOption* yyv1 = &yyp1->yyoption;
          if (yyv0->yystate != YY_NULLPTR)
            yyv1->yystate = YYRELOC (yyp0, yyp1, yyv0->yystate, yystate);
          if (yyv0->yynext != YY_NULLPTR)
            yyv1->yynext = YYRELOC (yyp0, yyp1, yyv0->yynext, yyoption);
        }
    }
  if (yystackp->yysplitPoint != YY_NULLPTR)
    yystackp->yysplitPoint = YYRELOC (yystackp->yyitems, yynewItems,
                                      yystackp->yysplitPoint, yystate);

  for (yyn = 0; yyn < yystackp->yytops.yysize; yyn += 1)
    if (yystackp->yytops.yystates[yyn] != YY_NULLPTR)
      yystackp->yytops.yystates[yyn] =
        YYRELOC (yystackp->yyitems, yynewItems,
                 yystackp->yytops.yystates[yyn], yystate);
  YYFREE (yystackp->yyitems);
  yystackp->yyitems = yynewItems;
  yystackp->yynextFree = yynewItems + yysize;
  yystackp->yyspaceLeft = yynewSize - yysize;
}
#endif

static void
yyfreeGLRStack (yyGLRStack* yystackp)
{
  YYFREE (yystackp->yyitems);
  yyfreeStateSet (&yystackp->yytops);
}

/** Assuming that YYS is a GLRState somewhere on *YYSTACKP, update the
 *  splitpoint of *YYSTACKP, if needed, so that it is at least as deep as
 *  YYS.  */
static inline void
yyupdateSplit (yyGLRStack* yystackp, yyGLRState* yys)
{
  if (yystackp->yysplitPoint != YY_NULLPTR && yystackp->yysplitPoint > yys)
    yystackp->yysplitPoint = yys;
}

/** Invalidate stack #YYK in *YYSTACKP.  */
static inline void
yymarkStackDeleted (yyGLRStack* yystackp, size_t yyk)
{
  if (yystackp->yytops.yystates[yyk] != YY_NULLPTR)
    yystackp->yylastDeleted = yystackp->yytops.yystates[yyk];
  yystackp->yytops.yystates[yyk] = YY_NULLPTR;
}

/** Undelete the last stack in *YYSTACKP that was marked as deleted.  Can
    only be done once after a deletion, and only when all other stacks have
    been deleted.  */
static void
yyundeleteLastStack (yyGLRStack* yystackp)
{
  if (yystackp->yylastDeleted == YY_NULLPTR || yystackp->yytops.yysize != 0)
    return;
  yystackp->yytops.yystates[0] = yystackp->yylastDeleted;
  yystackp->yytops.yysize = 1;
  YYDPRINTF ((stderr, "Restoring last deleted stack as stack #0.\n"));
  yystackp->yylastDeleted = YY_NULLPTR;
}

static inline void
yyremoveDeletes (yyGLRStack* yystackp)
{
  size_t yyi, yyj;
  yyi = yyj = 0;
  while (yyj < yystackp->yytops.yysize)
    {
      if (yystackp->yytops.yystates[yyi] == YY_NULLPTR)
        {
          if (yyi == yyj)
            {
              YYDPRINTF ((stderr, "Removing dead stacks.\n"));
            }
          yystackp->yytops.yysize -= 1;
        }
      else
        {
          yystackp->yytops.yystates[yyj] = yystackp->yytops.yystates[yyi];
          /* In the current implementation, it's unnecessary to copy
             yystackp->yytops.yylookaheadNeeds[yyi] since, after
             yyremoveDeletes returns, the parser immediately either enters
             deterministic operation or shifts a token.  However, it doesn't
             hurt, and the code might evolve to need it.  */
          yystackp->yytops.yylookaheadNeeds[yyj] =
            yystackp->yytops.yylookaheadNeeds[yyi];
          if (yyj != yyi)
            {
              YYDPRINTF ((stderr, "Rename stack %lu -> %lu.\n",
                          (unsigned long int) yyi, (unsigned long int) yyj));
            }
          yyj += 1;
        }
      yyi += 1;
    }
}

/** Shift to a new state on stack #YYK of *YYSTACKP, corresponding to LR
 * state YYLRSTATE, at input position YYPOSN, with (resolved) semantic
 * value *YYVALP and source location *YYLOCP.  */
static inline void
yyglrShift (yyGLRStack* yystackp, size_t yyk, yyStateNum yylrState,
            size_t yyposn,
            YYSTYPE* yyvalp, YYLTYPE* yylocp)
{
  yyGLRState* yynewState = &yynewGLRStackItem (yystackp, yytrue)->yystate;

  yynewState->yylrState = yylrState;
  yynewState->yyposn = yyposn;
  yynewState->yyresolved = yytrue;
  yynewState->yypred = yystackp->yytops.yystates[yyk];
  yynewState->yysemantics.yysval = *yyvalp;
  yynewState->yyloc = *yylocp;
  yystackp->yytops.yystates[yyk] = yynewState;

  YY_RESERVE_GLRSTACK (yystackp);
}

/** Shift stack #YYK of *YYSTACKP, to a new state corresponding to LR
 *  state YYLRSTATE, at input position YYPOSN, with the (unresolved)
 *  semantic value of YYRHS under the action for YYRULE.  */
static inline void
yyglrShiftDefer (yyGLRStack* yystackp, size_t yyk, yyStateNum yylrState,
                 size_t yyposn, yyGLRState* yyrhs, yyRuleNum yyrule)
{
  yyGLRState* yynewState = &yynewGLRStackItem (yystackp, yytrue)->yystate;
  YYASSERT (yynewState->yyisState);

  yynewState->yylrState = yylrState;
  yynewState->yyposn = yyposn;
  yynewState->yyresolved = yyfalse;
  yynewState->yypred = yystackp->yytops.yystates[yyk];
  yynewState->yysemantics.yyfirstVal = YY_NULLPTR;
  yystackp->yytops.yystates[yyk] = yynewState;

  /* Invokes YY_RESERVE_GLRSTACK.  */
  yyaddDeferredAction (yystackp, yyk, yynewState, yyrhs, yyrule);
}

#if !HANA_DEBUG
# define YY_REDUCE_PRINT(Args)
#else
# define YY_REDUCE_PRINT(Args)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print Args;               \
} while (0)

/*----------------------------------------------------------------------.
| Report that stack #YYK of *YYSTACKP is going to be reduced by YYRULE. |
`----------------------------------------------------------------------*/

static inline void
yy_reduce_print (int yynormal, yyGLRStackItem* yyvsp, size_t yyk,
                 yyRuleNum yyrule, ParseResult* result, yyscan_t scanner)
{
  int yynrhs = yyrhsLength (yyrule);
  int yylow = 1;
  int yyi;
  YYFPRINTF (stderr, "Reducing stack %lu by rule %d (line %lu):\n",
             (unsigned long int) yyk, yyrule - 1,
             (unsigned long int) yyrline[yyrule]);
  if (! yynormal)
    yyfillin (yyvsp, 1, -yynrhs);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyvsp[yyi - yynrhs + 1].yystate.yylrState],
                       &yyvsp[yyi - yynrhs + 1].yystate.yysemantics.yysval
                       , &(((yyGLRStackItem const *)yyvsp)[YYFILL ((yyi + 1) - (yynrhs))].yystate.yyloc)                       , result, scanner);
      if (!yyvsp[yyi - yynrhs + 1].yystate.yyresolved)
        YYFPRINTF (stderr, " (unresolved)");
      YYFPRINTF (stderr, "\n");
    }
}
#endif

/** Pop the symbols consumed by reduction #YYRULE from the top of stack
 *  #YYK of *YYSTACKP, and perform the appropriate semantic action on their
 *  semantic values.  Assumes that all ambiguities in semantic values
 *  have been previously resolved.  Set *YYVALP to the resulting value,
 *  and *YYLOCP to the computed location (if any).  Return value is as
 *  for userAction.  */
static inline YYRESULTTAG
yydoAction (yyGLRStack* yystackp, size_t yyk, yyRuleNum yyrule,
            YYSTYPE* yyvalp, YYLTYPE *yylocp, ParseResult* result, yyscan_t scanner)
{
  int yynrhs = yyrhsLength (yyrule);

  if (yystackp->yysplitPoint == YY_NULLPTR)
    {
      /* Standard special case: single stack.  */
      yyGLRStackItem* yyrhs = (yyGLRStackItem*) yystackp->yytops.yystates[yyk];
      YYASSERT (yyk == 0);
      yystackp->yynextFree -= yynrhs;
      yystackp->yyspaceLeft += yynrhs;
      yystackp->yytops.yystates[0] = & yystackp->yynextFree[-1].yystate;
      YY_REDUCE_PRINT ((1, yyrhs, yyk, yyrule, result, scanner));
      return yyuserAction (yyrule, yynrhs, yyrhs, yystackp,
                           yyvalp, yylocp, result, scanner);
    }
  else
    {
      int yyi;
      yyGLRState* yys;
      yyGLRStackItem yyrhsVals[YYMAXRHS + YYMAXLEFT + 1];
      yys = yyrhsVals[YYMAXRHS + YYMAXLEFT].yystate.yypred
        = yystackp->yytops.yystates[yyk];
      if (yynrhs == 0)
        /* Set default location.  */
        yyrhsVals[YYMAXRHS + YYMAXLEFT - 1].yystate.yyloc = yys->yyloc;
      for (yyi = 0; yyi < yynrhs; yyi += 1)
        {
          yys = yys->yypred;
          YYASSERT (yys);
        }
      yyupdateSplit (yystackp, yys);
      yystackp->yytops.yystates[yyk] = yys;
      YY_REDUCE_PRINT ((0, yyrhsVals + YYMAXRHS + YYMAXLEFT - 1, yyk, yyrule, result, scanner));
      return yyuserAction (yyrule, yynrhs, yyrhsVals + YYMAXRHS + YYMAXLEFT - 1,
                           yystackp, yyvalp, yylocp, result, scanner);
    }
}

/** Pop items off stack #YYK of *YYSTACKP according to grammar rule YYRULE,
 *  and push back on the resulting nonterminal symbol.  Perform the
 *  semantic action associated with YYRULE and store its value with the
 *  newly pushed state, if YYFORCEEVAL or if *YYSTACKP is currently
 *  unambiguous.  Otherwise, store the deferred semantic action with
 *  the new state.  If the new state would have an identical input
 *  position, LR state, and predecessor to an existing state on the stack,
 *  it is identified with that existing state, eliminating stack #YYK from
 *  *YYSTACKP.  In this case, the semantic value is
 *  added to the options for the existing state's semantic value.
 */
static inline YYRESULTTAG
yyglrReduce (yyGLRStack* yystackp, size_t yyk, yyRuleNum yyrule,
             yybool yyforceEval, ParseResult* result, yyscan_t scanner)
{
  size_t yyposn = yystackp->yytops.yystates[yyk]->yyposn;

  if (yyforceEval || yystackp->yysplitPoint == YY_NULLPTR)
    {
      YYSTYPE yysval;
      YYLTYPE yyloc;

      YYRESULTTAG yyflag = yydoAction (yystackp, yyk, yyrule, &yysval, &yyloc, result, scanner);
      if (yyflag == yyerr && yystackp->yysplitPoint != YY_NULLPTR)
        {
          YYDPRINTF ((stderr, "Parse on stack %lu rejected by rule #%d.\n",
                     (unsigned long int) yyk, yyrule - 1));
        }
      if (yyflag != yyok)
        return yyflag;
      YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyrule], &yysval, &yyloc);
      yyglrShift (yystackp, yyk,
                  yyLRgotoState (yystackp->yytops.yystates[yyk]->yylrState,
                                 yylhsNonterm (yyrule)),
                  yyposn, &yysval, &yyloc);
    }
  else
    {
      size_t yyi;
      int yyn;
      yyGLRState* yys, *yys0 = yystackp->yytops.yystates[yyk];
      yyStateNum yynewLRState;

      for (yys = yystackp->yytops.yystates[yyk], yyn = yyrhsLength (yyrule);
           0 < yyn; yyn -= 1)
        {
          yys = yys->yypred;
          YYASSERT (yys);
        }
      yyupdateSplit (yystackp, yys);
      yynewLRState = yyLRgotoState (yys->yylrState, yylhsNonterm (yyrule));
      YYDPRINTF ((stderr,
                  "Reduced stack %lu by rule #%d; action deferred.  "
                  "Now in state %d.\n",
                  (unsigned long int) yyk, yyrule - 1, yynewLRState));
      for (yyi = 0; yyi < yystackp->yytops.yysize; yyi += 1)
        if (yyi != yyk && yystackp->yytops.yystates[yyi] != YY_NULLPTR)
          {
            yyGLRState *yysplit = yystackp->yysplitPoint;
            yyGLRState *yyp = yystackp->yytops.yystates[yyi];
            while (yyp != yys && yyp != yysplit && yyp->yyposn >= yyposn)
              {
                if (yyp->yylrState == yynewLRState && yyp->yypred == yys)
                  {
                    yyaddDeferredAction (yystackp, yyk, yyp, yys0, yyrule);
                    yymarkStackDeleted (yystackp, yyk);
                    YYDPRINTF ((stderr, "Merging stack %lu into stack %lu.\n",
                                (unsigned long int) yyk,
                                (unsigned long int) yyi));
                    return yyok;
                  }
                yyp = yyp->yypred;
              }
          }
      yystackp->yytops.yystates[yyk] = yys;
      yyglrShiftDefer (yystackp, yyk, yynewLRState, yyposn, yys0, yyrule);
    }
  return yyok;
}

static size_t
yysplitStack (yyGLRStack* yystackp, size_t yyk)
{
  if (yystackp->yysplitPoint == YY_NULLPTR)
    {
      YYASSERT (yyk == 0);
      yystackp->yysplitPoint = yystackp->yytops.yystates[yyk];
    }
  if (yystackp->yytops.yysize >= yystackp->yytops.yycapacity)
    {
      yyGLRState** yynewStates;
      yybool* yynewLookaheadNeeds;

      yynewStates = YY_NULLPTR;

      if (yystackp->yytops.yycapacity
          > (YYSIZEMAX / (2 * sizeof yynewStates[0])))
        yyMemoryExhausted (yystackp);
      yystackp->yytops.yycapacity *= 2;

      yynewStates =
        (yyGLRState**) YYREALLOC (yystackp->yytops.yystates,
                                  (yystackp->yytops.yycapacity
                                   * sizeof yynewStates[0]));
      if (yynewStates == YY_NULLPTR)
        yyMemoryExhausted (yystackp);
      yystackp->yytops.yystates = yynewStates;

      yynewLookaheadNeeds =
        (yybool*) YYREALLOC (yystackp->yytops.yylookaheadNeeds,
                             (yystackp->yytops.yycapacity
                              * sizeof yynewLookaheadNeeds[0]));
      if (yynewLookaheadNeeds == YY_NULLPTR)
        yyMemoryExhausted (yystackp);
      yystackp->yytops.yylookaheadNeeds = yynewLookaheadNeeds;
    }
  yystackp->yytops.yystates[yystackp->yytops.yysize]
    = yystackp->yytops.yystates[yyk];
  yystackp->yytops.yylookaheadNeeds[yystackp->yytops.yysize]
    = yystackp->yytops.yylookaheadNeeds[yyk];
  yystackp->yytops.yysize += 1;
  return yystackp->yytops.yysize-1;
}

/** True iff YYY0 and YYY1 represent identical options at the top level.
 *  That is, they represent the same rule applied to RHS symbols
 *  that produce the same terminal symbols.  */
static yybool
yyidenticalOptions (yySemanticOption* yyy0, yySemanticOption* yyy1)
{
  if (yyy0->yyrule == yyy1->yyrule)
    {
      yyGLRState *yys0, *yys1;
      int yyn;
      for (yys0 = yyy0->yystate, yys1 = yyy1->yystate,
           yyn = yyrhsLength (yyy0->yyrule);
           yyn > 0;
           yys0 = yys0->yypred, yys1 = yys1->yypred, yyn -= 1)
        if (yys0->yyposn != yys1->yyposn)
          return yyfalse;
      return yytrue;
    }
  else
    return yyfalse;
}

/** Assuming identicalOptions (YYY0,YYY1), destructively merge the
 *  alternative semantic values for the RHS-symbols of YYY1 and YYY0.  */
static void
yymergeOptionSets (yySemanticOption* yyy0, yySemanticOption* yyy1)
{
  yyGLRState *yys0, *yys1;
  int yyn;
  for (yys0 = yyy0->yystate, yys1 = yyy1->yystate,
       yyn = yyrhsLength (yyy0->yyrule);
       yyn > 0;
       yys0 = yys0->yypred, yys1 = yys1->yypred, yyn -= 1)
    {
      if (yys0 == yys1)
        break;
      else if (yys0->yyresolved)
        {
          yys1->yyresolved = yytrue;
          yys1->yysemantics.yysval = yys0->yysemantics.yysval;
        }
      else if (yys1->yyresolved)
        {
          yys0->yyresolved = yytrue;
          yys0->yysemantics.yysval = yys1->yysemantics.yysval;
        }
      else
        {
          yySemanticOption** yyz0p = &yys0->yysemantics.yyfirstVal;
          yySemanticOption* yyz1 = yys1->yysemantics.yyfirstVal;
          while (yytrue)
            {
              if (yyz1 == *yyz0p || yyz1 == YY_NULLPTR)
                break;
              else if (*yyz0p == YY_NULLPTR)
                {
                  *yyz0p = yyz1;
                  break;
                }
              else if (*yyz0p < yyz1)
                {
                  yySemanticOption* yyz = *yyz0p;
                  *yyz0p = yyz1;
                  yyz1 = yyz1->yynext;
                  (*yyz0p)->yynext = yyz;
                }
              yyz0p = &(*yyz0p)->yynext;
            }
          yys1->yysemantics.yyfirstVal = yys0->yysemantics.yyfirstVal;
        }
    }
}

/** Y0 and Y1 represent two possible actions to take in a given
 *  parsing state; return 0 if no combination is possible,
 *  1 if user-mergeable, 2 if Y0 is preferred, 3 if Y1 is preferred.  */
static int
yypreference (yySemanticOption* y0, yySemanticOption* y1)
{
  yyRuleNum r0 = y0->yyrule, r1 = y1->yyrule;
  int p0 = yydprec[r0], p1 = yydprec[r1];

  if (p0 == p1)
    {
      if (yymerger[r0] == 0 || yymerger[r0] != yymerger[r1])
        return 0;
      else
        return 1;
    }
  if (p0 == 0 || p1 == 0)
    return 0;
  if (p0 < p1)
    return 3;
  if (p1 < p0)
    return 2;
  return 0;
}

static YYRESULTTAG yyresolveValue (yyGLRState* yys,
                                   yyGLRStack* yystackp, ParseResult* result, yyscan_t scanner);


/** Resolve the previous YYN states starting at and including state YYS
 *  on *YYSTACKP. If result != yyok, some states may have been left
 *  unresolved possibly with empty semantic option chains.  Regardless
 *  of whether result = yyok, each state has been left with consistent
 *  data so that yydestroyGLRState can be invoked if necessary.  */
static YYRESULTTAG
yyresolveStates (yyGLRState* yys, int yyn,
                 yyGLRStack* yystackp, ParseResult* result, yyscan_t scanner)
{
  if (0 < yyn)
    {
      YYASSERT (yys->yypred);
      YYCHK (yyresolveStates (yys->yypred, yyn-1, yystackp, result, scanner));
      if (! yys->yyresolved)
        YYCHK (yyresolveValue (yys, yystackp, result, scanner));
    }
  return yyok;
}

/** Resolve the states for the RHS of YYOPT on *YYSTACKP, perform its
 *  user action, and return the semantic value and location in *YYVALP
 *  and *YYLOCP.  Regardless of whether result = yyok, all RHS states
 *  have been destroyed (assuming the user action destroys all RHS
 *  semantic values if invoked).  */
static YYRESULTTAG
yyresolveAction (yySemanticOption* yyopt, yyGLRStack* yystackp,
                 YYSTYPE* yyvalp, YYLTYPE *yylocp, ParseResult* result, yyscan_t scanner)
{
  yyGLRStackItem yyrhsVals[YYMAXRHS + YYMAXLEFT + 1];
  int yynrhs = yyrhsLength (yyopt->yyrule);
  YYRESULTTAG yyflag =
    yyresolveStates (yyopt->yystate, yynrhs, yystackp, result, scanner);
  if (yyflag != yyok)
    {
      yyGLRState *yys;
      for (yys = yyopt->yystate; yynrhs > 0; yys = yys->yypred, yynrhs -= 1)
        yydestroyGLRState ("Cleanup: popping", yys, result, scanner);
      return yyflag;
    }

  yyrhsVals[YYMAXRHS + YYMAXLEFT].yystate.yypred = yyopt->yystate;
  if (yynrhs == 0)
    /* Set default location.  */
    yyrhsVals[YYMAXRHS + YYMAXLEFT - 1].yystate.yyloc = yyopt->yystate->yyloc;
  {
    int yychar_current = yychar;
    YYSTYPE yylval_current = yylval;
    YYLTYPE yylloc_current = yylloc;
    yychar = yyopt->yyrawchar;
    yylval = yyopt->yyval;
    yylloc = yyopt->yyloc;
    yyflag = yyuserAction (yyopt->yyrule, yynrhs,
                           yyrhsVals + YYMAXRHS + YYMAXLEFT - 1,
                           yystackp, yyvalp, yylocp, result, scanner);
    yychar = yychar_current;
    yylval = yylval_current;
    yylloc = yylloc_current;
  }
  return yyflag;
}

#if HANA_DEBUG
static void
yyreportTree (yySemanticOption* yyx, int yyindent)
{
  int yynrhs = yyrhsLength (yyx->yyrule);
  int yyi;
  yyGLRState* yys;
  yyGLRState* yystates[1 + YYMAXRHS];
  yyGLRState yyleftmost_state;

  for (yyi = yynrhs, yys = yyx->yystate; 0 < yyi; yyi -= 1, yys = yys->yypred)
    yystates[yyi] = yys;
  if (yys == YY_NULLPTR)
    {
      yyleftmost_state.yyposn = 0;
      yystates[0] = &yyleftmost_state;
    }
  else
    yystates[0] = yys;

  if (yyx->yystate->yyposn < yys->yyposn + 1)
    YYFPRINTF (stderr, "%*s%s -> <Rule %d, empty>\n",
               yyindent, "", yytokenName (yylhsNonterm (yyx->yyrule)),
               yyx->yyrule - 1);
  else
    YYFPRINTF (stderr, "%*s%s -> <Rule %d, tokens %lu .. %lu>\n",
               yyindent, "", yytokenName (yylhsNonterm (yyx->yyrule)),
               yyx->yyrule - 1, (unsigned long int) (yys->yyposn + 1),
               (unsigned long int) yyx->yystate->yyposn);
  for (yyi = 1; yyi <= yynrhs; yyi += 1)
    {
      if (yystates[yyi]->yyresolved)
        {
          if (yystates[yyi-1]->yyposn+1 > yystates[yyi]->yyposn)
            YYFPRINTF (stderr, "%*s%s <empty>\n", yyindent+2, "",
                       yytokenName (yystos[yystates[yyi]->yylrState]));
          else
            YYFPRINTF (stderr, "%*s%s <tokens %lu .. %lu>\n", yyindent+2, "",
                       yytokenName (yystos[yystates[yyi]->yylrState]),
                       (unsigned long int) (yystates[yyi-1]->yyposn + 1),
                       (unsigned long int) yystates[yyi]->yyposn);
        }
      else
        yyreportTree (yystates[yyi]->yysemantics.yyfirstVal, yyindent+2);
    }
}
#endif

static YYRESULTTAG
yyreportAmbiguity (yySemanticOption* yyx0,
                   yySemanticOption* yyx1, ParseResult* result, yyscan_t scanner)
{
  YYUSE (yyx0);
  YYUSE (yyx1);

#if HANA_DEBUG
  YYFPRINTF (stderr, "Ambiguity detected.\n");
  YYFPRINTF (stderr, "Option 1,\n");
  yyreportTree (yyx0, 2);
  YYFPRINTF (stderr, "\nOption 2,\n");
  yyreportTree (yyx1, 2);
  YYFPRINTF (stderr, "\n");
#endif

  yyerror (result, scanner, YY_("syntax is ambiguous"));
  return yyabort;
}

/** Resolve the locations for each of the YYN1 states in *YYSTACKP,
 *  ending at YYS1.  Has no effect on previously resolved states.
 *  The first semantic option of a state is always chosen.  */
static void
yyresolveLocations (yyGLRState* yys1, int yyn1,
                    yyGLRStack *yystackp, ParseResult* result, yyscan_t scanner)
{
  if (0 < yyn1)
    {
      yyresolveLocations (yys1->yypred, yyn1 - 1, yystackp, result, scanner);
      if (!yys1->yyresolved)
        {
          yyGLRStackItem yyrhsloc[1 + YYMAXRHS];
          int yynrhs;
          yySemanticOption *yyoption = yys1->yysemantics.yyfirstVal;
          YYASSERT (yyoption != YY_NULLPTR);
          yynrhs = yyrhsLength (yyoption->yyrule);
          if (yynrhs > 0)
            {
              yyGLRState *yys;
              int yyn;
              yyresolveLocations (yyoption->yystate, yynrhs,
                                  yystackp, result, scanner);
              for (yys = yyoption->yystate, yyn = yynrhs;
                   yyn > 0;
                   yys = yys->yypred, yyn -= 1)
                yyrhsloc[yyn].yystate.yyloc = yys->yyloc;
            }
          else
            {
              /* Both yyresolveAction and yyresolveLocations traverse the GSS
                 in reverse rightmost order.  It is only necessary to invoke
                 yyresolveLocations on a subforest for which yyresolveAction
                 would have been invoked next had an ambiguity not been
                 detected.  Thus the location of the previous state (but not
                 necessarily the previous state itself) is guaranteed to be
                 resolved already.  */
              yyGLRState *yyprevious = yyoption->yystate;
              yyrhsloc[0].yystate.yyloc = yyprevious->yyloc;
            }
          {
            int yychar_current = yychar;
            YYSTYPE yylval_current = yylval;
            YYLTYPE yylloc_current = yylloc;
            yychar = yyoption->yyrawchar;
            yylval = yyoption->yyval;
            yylloc = yyoption->yyloc;
            YYLLOC_DEFAULT ((yys1->yyloc), yyrhsloc, yynrhs);
            yychar = yychar_current;
            yylval = yylval_current;
            yylloc = yylloc_current;
          }
        }
    }
}

/** Resolve the ambiguity represented in state YYS in *YYSTACKP,
 *  perform the indicated actions, and set the semantic value of YYS.
 *  If result != yyok, the chain of semantic options in YYS has been
 *  cleared instead or it has been left unmodified except that
 *  redundant options may have been removed.  Regardless of whether
 *  result = yyok, YYS has been left with consistent data so that
 *  yydestroyGLRState can be invoked if necessary.  */
static YYRESULTTAG
yyresolveValue (yyGLRState* yys, yyGLRStack* yystackp, ParseResult* result, yyscan_t scanner)
{
  yySemanticOption* yyoptionList = yys->yysemantics.yyfirstVal;
  yySemanticOption* yybest = yyoptionList;
  yySemanticOption** yypp;
  yybool yymerge = yyfalse;
  YYSTYPE yysval;
  YYRESULTTAG yyflag;
  YYLTYPE *yylocp = &yys->yyloc;

  for (yypp = &yyoptionList->yynext; *yypp != YY_NULLPTR; )
    {
      yySemanticOption* yyp = *yypp;

      if (yyidenticalOptions (yybest, yyp))
        {
          yymergeOptionSets (yybest, yyp);
          *yypp = yyp->yynext;
        }
      else
        {
          switch (yypreference (yybest, yyp))
            {
            case 0:
              yyresolveLocations (yys, 1, yystackp, result, scanner);
              return yyreportAmbiguity (yybest, yyp, result, scanner);
              break;
            case 1:
              yymerge = yytrue;
              break;
            case 2:
              break;
            case 3:
              yybest = yyp;
              yymerge = yyfalse;
              break;
            default:
              /* This cannot happen so it is not worth a YYASSERT (yyfalse),
                 but some compilers complain if the default case is
                 omitted.  */
              break;
            }
          yypp = &yyp->yynext;
        }
    }

  if (yymerge)
    {
      yySemanticOption* yyp;
      int yyprec = yydprec[yybest->yyrule];
      yyflag = yyresolveAction (yybest, yystackp, &yysval, yylocp, result, scanner);
      if (yyflag == yyok)
        for (yyp = yybest->yynext; yyp != YY_NULLPTR; yyp = yyp->yynext)
          {
            if (yyprec == yydprec[yyp->yyrule])
              {
                YYSTYPE yysval_other;
                YYLTYPE yydummy;
                yyflag = yyresolveAction (yyp, yystackp, &yysval_other, &yydummy, result, scanner);
                if (yyflag != yyok)
                  {
                    yydestruct ("Cleanup: discarding incompletely merged value for",
                                yystos[yys->yylrState],
                                &yysval, yylocp, result, scanner);
                    break;
                  }
                yyuserMerge (yymerger[yyp->yyrule], &yysval, &yysval_other);
              }
          }
    }
  else
    yyflag = yyresolveAction (yybest, yystackp, &yysval, yylocp, result, scanner);

  if (yyflag == yyok)
    {
      yys->yyresolved = yytrue;
      yys->yysemantics.yysval = yysval;
    }
  else
    yys->yysemantics.yyfirstVal = YY_NULLPTR;
  return yyflag;
}

static YYRESULTTAG
yyresolveStack (yyGLRStack* yystackp, ParseResult* result, yyscan_t scanner)
{
  if (yystackp->yysplitPoint != YY_NULLPTR)
    {
      yyGLRState* yys;
      int yyn;

      for (yyn = 0, yys = yystackp->yytops.yystates[0];
           yys != yystackp->yysplitPoint;
           yys = yys->yypred, yyn += 1)
        continue;
      YYCHK (yyresolveStates (yystackp->yytops.yystates[0], yyn, yystackp
                             , result, scanner));
    }
  return yyok;
}

static void
yycompressStack (yyGLRStack* yystackp)
{
  yyGLRState* yyp, *yyq, *yyr;

  if (yystackp->yytops.yysize != 1 || yystackp->yysplitPoint == YY_NULLPTR)
    return;

  for (yyp = yystackp->yytops.yystates[0], yyq = yyp->yypred, yyr = YY_NULLPTR;
       yyp != yystackp->yysplitPoint;
       yyr = yyp, yyp = yyq, yyq = yyp->yypred)
    yyp->yypred = yyr;

  yystackp->yyspaceLeft += yystackp->yynextFree - yystackp->yyitems;
  yystackp->yynextFree = ((yyGLRStackItem*) yystackp->yysplitPoint) + 1;
  yystackp->yyspaceLeft -= yystackp->yynextFree - yystackp->yyitems;
  yystackp->yysplitPoint = YY_NULLPTR;
  yystackp->yylastDeleted = YY_NULLPTR;

  while (yyr != YY_NULLPTR)
    {
      yystackp->yynextFree->yystate = *yyr;
      yyr = yyr->yypred;
      yystackp->yynextFree->yystate.yypred = &yystackp->yynextFree[-1].yystate;
      yystackp->yytops.yystates[0] = &yystackp->yynextFree->yystate;
      yystackp->yynextFree += 1;
      yystackp->yyspaceLeft -= 1;
    }
}

static YYRESULTTAG
yyprocessOneStack (yyGLRStack* yystackp, size_t yyk,
                   size_t yyposn, ParseResult* result, yyscan_t scanner)
{
  while (yystackp->yytops.yystates[yyk] != YY_NULLPTR)
    {
      yyStateNum yystate = yystackp->yytops.yystates[yyk]->yylrState;
      YYDPRINTF ((stderr, "Stack %lu Entering state %d\n",
                  (unsigned long int) yyk, yystate));

      YYASSERT (yystate != YYFINAL);

      if (yyisDefaultedState (yystate))
        {
          YYRESULTTAG yyflag;
          yyRuleNum yyrule = yydefaultAction (yystate);
          if (yyrule == 0)
            {
              YYDPRINTF ((stderr, "Stack %lu dies.\n",
                          (unsigned long int) yyk));
              yymarkStackDeleted (yystackp, yyk);
              return yyok;
            }
          yyflag = yyglrReduce (yystackp, yyk, yyrule, yyimmediate[yyrule], result, scanner);
          if (yyflag == yyerr)
            {
              YYDPRINTF ((stderr,
                          "Stack %lu dies "
                          "(predicate failure or explicit user error).\n",
                          (unsigned long int) yyk));
              yymarkStackDeleted (yystackp, yyk);
              return yyok;
            }
          if (yyflag != yyok)
            return yyflag;
        }
      else
        {
          yySymbol yytoken;
          int yyaction;
          const short int* yyconflicts;

          yystackp->yytops.yylookaheadNeeds[yyk] = yytrue;
          if (yychar == YYEMPTY)
            {
              YYDPRINTF ((stderr, "Reading a token: "));
              yychar = yylex (scanner);
            }

          if (yychar <= YYEOF)
            {
              yychar = yytoken = YYEOF;
              YYDPRINTF ((stderr, "Now at end of input.\n"));
            }
          else
            {
              yytoken = YYTRANSLATE (yychar);
              YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
            }

          yygetLRActions (yystate, yytoken, &yyaction, &yyconflicts);

          while (*yyconflicts != 0)
            {
              YYRESULTTAG yyflag;
              size_t yynewStack = yysplitStack (yystackp, yyk);
              YYDPRINTF ((stderr, "Splitting off stack %lu from %lu.\n",
                          (unsigned long int) yynewStack,
                          (unsigned long int) yyk));
              yyflag = yyglrReduce (yystackp, yynewStack,
                                    *yyconflicts,
                                    yyimmediate[*yyconflicts], result, scanner);
              if (yyflag == yyok)
                YYCHK (yyprocessOneStack (yystackp, yynewStack,
                                          yyposn, result, scanner));
              else if (yyflag == yyerr)
                {
                  YYDPRINTF ((stderr, "Stack %lu dies.\n",
                              (unsigned long int) yynewStack));
                  yymarkStackDeleted (yystackp, yynewStack);
                }
              else
                return yyflag;
              yyconflicts += 1;
            }

          if (yyisShiftAction (yyaction))
            break;
          else if (yyisErrorAction (yyaction))
            {
              YYDPRINTF ((stderr, "Stack %lu dies.\n",
                          (unsigned long int) yyk));
              yymarkStackDeleted (yystackp, yyk);
              break;
            }
          else
            {
              YYRESULTTAG yyflag = yyglrReduce (yystackp, yyk, -yyaction,
                                                yyimmediate[-yyaction], result, scanner);
              if (yyflag == yyerr)
                {
                  YYDPRINTF ((stderr,
                              "Stack %lu dies "
                              "(predicate failure or explicit user error).\n",
                              (unsigned long int) yyk));
                  yymarkStackDeleted (yystackp, yyk);
                  break;
                }
              else if (yyflag != yyok)
                return yyflag;
            }
        }
    }
  return yyok;
}

static void
yyreportSyntaxError (yyGLRStack* yystackp, ParseResult* result, yyscan_t scanner)
{
  if (yystackp->yyerrState != 0)
    return;
#if ! YYERROR_VERBOSE
  yyerror (result, scanner, YY_("syntax error"));
#else
  {
  yySymbol yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);
  size_t yysize0 = yytnamerr (YY_NULLPTR, yytokenName (yytoken));
  size_t yysize = yysize0;
  yybool yysize_overflow = yyfalse;
  char* yymsg = YY_NULLPTR;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected").  */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[yystackp->yytops.yystates[0]->yylrState];
      yyarg[yycount++] = yytokenName (yytoken);
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for this
             state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;
          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytokenName (yyx);
                {
                  size_t yysz = yysize + yytnamerr (YY_NULLPTR, yytokenName (yyx));
                  yysize_overflow |= yysz < yysize;
                  yysize = yysz;
                }
              }
        }
    }

  switch (yycount)
    {
#define YYCASE_(N, S)                   \
      case N:                           \
        yyformat = S;                   \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  {
    size_t yysz = yysize + strlen (yyformat);
    yysize_overflow |= yysz < yysize;
    yysize = yysz;
  }

  if (!yysize_overflow)
    yymsg = (char *) YYMALLOC (yysize);

  if (yymsg)
    {
      char *yyp = yymsg;
      int yyi = 0;
      while ((*yyp = *yyformat))
        {
          if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
            {
              yyp += yytnamerr (yyp, yyarg[yyi++]);
              yyformat += 2;
            }
          else
            {
              yyp++;
              yyformat++;
            }
        }
      yyerror (result, scanner, yymsg);
      YYFREE (yymsg);
    }
  else
    {
      yyerror (result, scanner, YY_("syntax error"));
      yyMemoryExhausted (yystackp);
    }
  }
#endif /* YYERROR_VERBOSE */
  yynerrs += 1;
}

/* Recover from a syntax error on *YYSTACKP, assuming that *YYSTACKP->YYTOKENP,
   yylval, and yylloc are the syntactic category, semantic value, and location
   of the lookahead.  */
static void
yyrecoverSyntaxError (yyGLRStack* yystackp, ParseResult* result, yyscan_t scanner)
{
  size_t yyk;
  int yyj;

  if (yystackp->yyerrState == 3)
    /* We just shifted the error token and (perhaps) took some
       reductions.  Skip tokens until we can proceed.  */
    while (yytrue)
      {
        yySymbol yytoken;
        if (yychar == YYEOF)
          yyFail (yystackp, result, scanner, YY_NULLPTR);
        if (yychar != YYEMPTY)
          {
            /* We throw away the lookahead, but the error range
               of the shifted error token must take it into account.  */
            yyGLRState *yys = yystackp->yytops.yystates[0];
            yyGLRStackItem yyerror_range[3];
            yyerror_range[1].yystate.yyloc = yys->yyloc;
            yyerror_range[2].yystate.yyloc = yylloc;
            YYLLOC_DEFAULT ((yys->yyloc), yyerror_range, 2);
            yytoken = YYTRANSLATE (yychar);
            yydestruct ("Error: discarding",
                        yytoken, &yylval, &yylloc, result, scanner);
          }
        YYDPRINTF ((stderr, "Reading a token: "));
        yychar = yylex (scanner);
        if (yychar <= YYEOF)
          {
            yychar = yytoken = YYEOF;
            YYDPRINTF ((stderr, "Now at end of input.\n"));
          }
        else
          {
            yytoken = YYTRANSLATE (yychar);
            YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
          }
        yyj = yypact[yystackp->yytops.yystates[0]->yylrState];
        if (yypact_value_is_default (yyj))
          return;
        yyj += yytoken;
        if (yyj < 0 || YYLAST < yyj || yycheck[yyj] != yytoken)
          {
            if (yydefact[yystackp->yytops.yystates[0]->yylrState] != 0)
              return;
          }
        else if (! yytable_value_is_error (yytable[yyj]))
          return;
      }

  /* Reduce to one stack.  */
  for (yyk = 0; yyk < yystackp->yytops.yysize; yyk += 1)
    if (yystackp->yytops.yystates[yyk] != YY_NULLPTR)
      break;
  if (yyk >= yystackp->yytops.yysize)
    yyFail (yystackp, result, scanner, YY_NULLPTR);
  for (yyk += 1; yyk < yystackp->yytops.yysize; yyk += 1)
    yymarkStackDeleted (yystackp, yyk);
  yyremoveDeletes (yystackp);
  yycompressStack (yystackp);

  /* Now pop stack until we find a state that shifts the error token.  */
  yystackp->yyerrState = 3;
  while (yystackp->yytops.yystates[0] != YY_NULLPTR)
    {
      yyGLRState *yys = yystackp->yytops.yystates[0];
      yyj = yypact[yys->yylrState];
      if (! yypact_value_is_default (yyj))
        {
          yyj += YYTERROR;
          if (0 <= yyj && yyj <= YYLAST && yycheck[yyj] == YYTERROR
              && yyisShiftAction (yytable[yyj]))
            {
              /* Shift the error token.  */
              /* First adjust its location.*/
              YYLTYPE yyerrloc;
              yystackp->yyerror_range[2].yystate.yyloc = yylloc;
              YYLLOC_DEFAULT (yyerrloc, (yystackp->yyerror_range), 2);
              YY_SYMBOL_PRINT ("Shifting", yystos[yytable[yyj]],
                               &yylval, &yyerrloc);
              yyglrShift (yystackp, 0, yytable[yyj],
                          yys->yyposn, &yylval, &yyerrloc);
              yys = yystackp->yytops.yystates[0];
              break;
            }
        }
      yystackp->yyerror_range[1].yystate.yyloc = yys->yyloc;
      if (yys->yypred != YY_NULLPTR)
        yydestroyGLRState ("Error: popping", yys, result, scanner);
      yystackp->yytops.yystates[0] = yys->yypred;
      yystackp->yynextFree -= 1;
      yystackp->yyspaceLeft += 1;
    }
  if (yystackp->yytops.yystates[0] == YY_NULLPTR)
    yyFail (yystackp, result, scanner, YY_NULLPTR);
}

#define YYCHK1(YYE)                                                          \
  do {                                                                       \
    switch (YYE) {                                                           \
    case yyok:                                                               \
      break;                                                                 \
    case yyabort:                                                            \
      goto yyabortlab;                                                       \
    case yyaccept:                                                           \
      goto yyacceptlab;                                                      \
    case yyerr:                                                              \
      goto yyuser_error;                                                     \
    default:                                                                 \
      goto yybuglab;                                                         \
    }                                                                        \
  } while (0)

/*----------.
| yyparse.  |
`----------*/

int
yyparse (ParseResult* result, yyscan_t scanner)
{
  int yyresult;
  yyGLRStack yystack;
  yyGLRStack* const yystackp = &yystack;
  size_t yyposn;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY;
  yylval = yyval_default;
  yylloc = yyloc_default;

  /* User initialization code.  */
  #line 67 "sqlparser_hana.yacc" /* glr.c:2270  */
{
	// Initialize
	yylloc.first_column = 0;
	yylloc.last_column = 0;
	yylloc.first_line = 0;
	yylloc.last_line = 0;
}

#line 14927 "sqlparser_hana_bison.cpp" /* glr.c:2270  */

  if (! yyinitGLRStack (yystackp, YYINITDEPTH))
    goto yyexhaustedlab;
  switch (YYSETJMP (yystack.yyexception_buffer))
    {
    case 0: break;
    case 1: goto yyabortlab;
    case 2: goto yyexhaustedlab;
    default: goto yybuglab;
    }
  yyglrShift (&yystack, 0, 0, 0, &yylval, &yylloc);
  yyposn = 0;

  while (yytrue)
    {
      /* For efficiency, we have two loops, the first of which is
         specialized to deterministic operation (single stack, no
         potential ambiguity).  */
      /* Standard mode */
      while (yytrue)
        {
          yyRuleNum yyrule;
          int yyaction;
          const short int* yyconflicts;

          yyStateNum yystate = yystack.yytops.yystates[0]->yylrState;
          YYDPRINTF ((stderr, "Entering state %d\n", yystate));
          if (yystate == YYFINAL)
            goto yyacceptlab;
          if (yyisDefaultedState (yystate))
            {
              yyrule = yydefaultAction (yystate);
              if (yyrule == 0)
                {
               yystack.yyerror_range[1].yystate.yyloc = yylloc;
                  yyreportSyntaxError (&yystack, result, scanner);
                  goto yyuser_error;
                }
              YYCHK1 (yyglrReduce (&yystack, 0, yyrule, yytrue, result, scanner));
            }
          else
            {
              yySymbol yytoken;
              if (yychar == YYEMPTY)
                {
                  YYDPRINTF ((stderr, "Reading a token: "));
                  yychar = yylex (scanner);
                }

              if (yychar <= YYEOF)
                {
                  yychar = yytoken = YYEOF;
                  YYDPRINTF ((stderr, "Now at end of input.\n"));
                }
              else
                {
                  yytoken = YYTRANSLATE (yychar);
                  YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
                }

              yygetLRActions (yystate, yytoken, &yyaction, &yyconflicts);
              if (*yyconflicts != 0)
                break;
              if (yyisShiftAction (yyaction))
                {
                  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
                  yychar = YYEMPTY;
                  yyposn += 1;
                  yyglrShift (&yystack, 0, yyaction, yyposn, &yylval, &yylloc);
                  if (0 < yystack.yyerrState)
                    yystack.yyerrState -= 1;
                }
              else if (yyisErrorAction (yyaction))
                {
               yystack.yyerror_range[1].yystate.yyloc = yylloc;
                  yyreportSyntaxError (&yystack, result, scanner);
                  goto yyuser_error;
                }
              else
                YYCHK1 (yyglrReduce (&yystack, 0, -yyaction, yytrue, result, scanner));
            }
        }

      while (yytrue)
        {
          yySymbol yytoken_to_shift;
          size_t yys;

          for (yys = 0; yys < yystack.yytops.yysize; yys += 1)
            yystackp->yytops.yylookaheadNeeds[yys] = yychar != YYEMPTY;

          /* yyprocessOneStack returns one of three things:

              - An error flag.  If the caller is yyprocessOneStack, it
                immediately returns as well.  When the caller is finally
                yyparse, it jumps to an error label via YYCHK1.

              - yyok, but yyprocessOneStack has invoked yymarkStackDeleted
                (&yystack, yys), which sets the top state of yys to NULL.  Thus,
                yyparse's following invocation of yyremoveDeletes will remove
                the stack.

              - yyok, when ready to shift a token.

             Except in the first case, yyparse will invoke yyremoveDeletes and
             then shift the next token onto all remaining stacks.  This
             synchronization of the shift (that is, after all preceding
             reductions on all stacks) helps prevent double destructor calls
             on yylval in the event of memory exhaustion.  */

          for (yys = 0; yys < yystack.yytops.yysize; yys += 1)
            YYCHK1 (yyprocessOneStack (&yystack, yys, yyposn, result, scanner));
          yyremoveDeletes (&yystack);
          if (yystack.yytops.yysize == 0)
            {
              yyundeleteLastStack (&yystack);
              if (yystack.yytops.yysize == 0)
                yyFail (&yystack, result, scanner, YY_("syntax error"));
              YYCHK1 (yyresolveStack (&yystack, result, scanner));
              YYDPRINTF ((stderr, "Returning to deterministic operation.\n"));
           yystack.yyerror_range[1].yystate.yyloc = yylloc;
              yyreportSyntaxError (&yystack, result, scanner);
              goto yyuser_error;
            }

          /* If any yyglrShift call fails, it will fail after shifting.  Thus,
             a copy of yylval will already be on stack 0 in the event of a
             failure in the following loop.  Thus, yychar is set to YYEMPTY
             before the loop to make sure the user destructor for yylval isn't
             called twice.  */
          yytoken_to_shift = YYTRANSLATE (yychar);
          yychar = YYEMPTY;
          yyposn += 1;
          for (yys = 0; yys < yystack.yytops.yysize; yys += 1)
            {
              int yyaction;
              const short int* yyconflicts;
              yyStateNum yystate = yystack.yytops.yystates[yys]->yylrState;
              yygetLRActions (yystate, yytoken_to_shift, &yyaction,
                              &yyconflicts);
              /* Note that yyconflicts were handled by yyprocessOneStack.  */
              YYDPRINTF ((stderr, "On stack %lu, ", (unsigned long int) yys));
              YY_SYMBOL_PRINT ("shifting", yytoken_to_shift, &yylval, &yylloc);
              yyglrShift (&yystack, yys, yyaction, yyposn,
                          &yylval, &yylloc);
              YYDPRINTF ((stderr, "Stack %lu now in state #%d\n",
                          (unsigned long int) yys,
                          yystack.yytops.yystates[yys]->yylrState));
            }

          if (yystack.yytops.yysize == 1)
            {
              YYCHK1 (yyresolveStack (&yystack, result, scanner));
              YYDPRINTF ((stderr, "Returning to deterministic operation.\n"));
              yycompressStack (&yystack);
              break;
            }
        }
      continue;
    yyuser_error:
      yyrecoverSyntaxError (&yystack, result, scanner);
      yyposn = yystack.yytops.yystates[0]->yyposn;
    }

 yyacceptlab:
  yyresult = 0;
  goto yyreturn;

 yybuglab:
  YYASSERT (yyfalse);
  goto yyabortlab;

 yyabortlab:
  yyresult = 1;
  goto yyreturn;

 yyexhaustedlab:
  yyerror (result, scanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturn;

 yyreturn:
  if (yychar != YYEMPTY)
    yydestruct ("Cleanup: discarding lookahead",
                YYTRANSLATE (yychar), &yylval, &yylloc, result, scanner);

  /* If the stack is well-formed, pop the stack until it is empty,
     destroying its entries as we go.  But free the stack regardless
     of whether it is well-formed.  */
  if (yystack.yyitems)
    {
      yyGLRState** yystates = yystack.yytops.yystates;
      if (yystates)
        {
          size_t yysize = yystack.yytops.yysize;
          size_t yyk;
          for (yyk = 0; yyk < yysize; yyk += 1)
            if (yystates[yyk])
              {
                while (yystates[yyk])
                  {
                    yyGLRState *yys = yystates[yyk];
                 yystack.yyerror_range[1].yystate.yyloc = yys->yyloc;
                  if (yys->yypred != YY_NULLPTR)
                      yydestroyGLRState ("Cleanup: popping", yys, result, scanner);
                    yystates[yyk] = yys->yypred;
                    yystack.yynextFree -= 1;
                    yystack.yyspaceLeft += 1;
                  }
                break;
              }
        }
      yyfreeGLRStack (&yystack);
    }

  return yyresult;
}

/* DEBUGGING ONLY */
#if HANA_DEBUG
static void
yy_yypstack (yyGLRState* yys)
{
  if (yys->yypred)
    {
      yy_yypstack (yys->yypred);
      YYFPRINTF (stderr, " -> ");
    }
  YYFPRINTF (stderr, "%d@%lu", yys->yylrState,
             (unsigned long int) yys->yyposn);
}

static void
yypstates (yyGLRState* yyst)
{
  if (yyst == YY_NULLPTR)
    YYFPRINTF (stderr, "<null>");
  else
    yy_yypstack (yyst);
  YYFPRINTF (stderr, "\n");
}

static void
yypstack (yyGLRStack* yystackp, size_t yyk)
{
  yypstates (yystackp->yytops.yystates[yyk]);
}

#define YYINDEX(YYX)                                                         \
    ((YYX) == YY_NULLPTR ? -1 : (yyGLRStackItem*) (YYX) - yystackp->yyitems)


static void
yypdumpstack (yyGLRStack* yystackp)
{
  yyGLRStackItem* yyp;
  size_t yyi;
  for (yyp = yystackp->yyitems; yyp < yystackp->yynextFree; yyp += 1)
    {
      YYFPRINTF (stderr, "%3lu. ",
                 (unsigned long int) (yyp - yystackp->yyitems));
      if (*(yybool *) yyp)
        {
          YYASSERT (yyp->yystate.yyisState);
          YYASSERT (yyp->yyoption.yyisState);
          YYFPRINTF (stderr, "Res: %d, LR State: %d, posn: %lu, pred: %ld",
                     yyp->yystate.yyresolved, yyp->yystate.yylrState,
                     (unsigned long int) yyp->yystate.yyposn,
                     (long int) YYINDEX (yyp->yystate.yypred));
          if (! yyp->yystate.yyresolved)
            YYFPRINTF (stderr, ", firstVal: %ld",
                       (long int) YYINDEX (yyp->yystate
                                             .yysemantics.yyfirstVal));
        }
      else
        {
          YYASSERT (!yyp->yystate.yyisState);
          YYASSERT (!yyp->yyoption.yyisState);
          YYFPRINTF (stderr, "Option. rule: %d, state: %ld, next: %ld",
                     yyp->yyoption.yyrule - 1,
                     (long int) YYINDEX (yyp->yyoption.yystate),
                     (long int) YYINDEX (yyp->yyoption.yynext));
        }
      YYFPRINTF (stderr, "\n");
    }
  YYFPRINTF (stderr, "Tops:");
  for (yyi = 0; yyi < yystackp->yytops.yysize; yyi += 1)
    YYFPRINTF (stderr, "%lu: %ld; ", (unsigned long int) yyi,
               (long int) YYINDEX (yystackp->yytops.yystates[yyi]));
  YYFPRINTF (stderr, "\n");
}
#endif

#undef yylval
#undef yychar
#undef yynerrs
#undef yylloc

/* Substitute the variable and function names.  */
#define yyparse hana_parse
#define yylex   hana_lex
#define yyerror hana_error
#define yylval  hana_lval
#define yychar  hana_char
#define yydebug hana_debug
#define yynerrs hana_nerrs
#define yylloc  hana_lloc

#line 3844 "sqlparser_hana.yacc" /* glr.c:2584  */

/*********************************
 ** Section 4: Additional C code
 *********************************/

/* empty */
