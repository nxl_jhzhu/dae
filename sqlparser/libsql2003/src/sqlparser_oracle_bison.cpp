/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         ORACLE_STYPE
#define YYLTYPE         ORACLE_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         oracle_parse
#define yylex           oracle_lex
#define yyerror         oracle_error
#define yydebug         oracle_debug
#define yynerrs         oracle_nerrs


/* Copy the first part of user declarations.  */
#line 1 "oracle.y" /* yacc.c:339  */

/**
 * This Grammar is designed for oracle.
 * https://github.com/Raphael2017/SQL/blob/master/sql-2003-2.bnf
 * sqlparser.y
 * defines sqlparser_oracle_bison.h
 * outputs sqlparser_oracle_bison.cpp
 *
 * Bison Grammar File Spec: http://dinosaur.compilertools.net/bison/bison_6.html
 *
 */
/*********************************
 ** Section 1: C Declarations
 *********************************/

#include "sqlparser_oracle_bison.h"
#include "sqlparser_oracle_flex.h"
#include "serialize_format.h"

#include <stdio.h>
#include <string.h>
//#include <strings.h>

/*
 * We provide parse error includes error message, first line, first column of error lex for debug
 */
int yyerror(YYLTYPE* llocp, ParseResult* result, yyscan_t scanner, const char *msg) {
    result->accept = false;
    result->errFirstLine = llocp->first_line;
    result->errFirstColumn = llocp->first_column;
    result->errDetail = msg;
	return 0;
}


#line 110 "sqlparser_oracle_bison.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sqlparser_oracle_bison.h".  */
#ifndef YY_ORACLE_SQLPARSER_ORACLE_BISON_H_INCLUDED
# define YY_ORACLE_SQLPARSER_ORACLE_BISON_H_INCLUDED
/* Debug traces.  */
#ifndef ORACLE_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define ORACLE_DEBUG 1
#  else
#   define ORACLE_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define ORACLE_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined ORACLE_DEBUG */
#if ORACLE_DEBUG
extern int oracle_debug;
#endif
/* "%code requires" blocks.  */
#line 42 "oracle.y" /* yacc.c:355  */

// %code requires block

#include "node.h"

#line 154 "sqlparser_oracle_bison.cpp" /* yacc.c:355  */

/* Token type.  */
#ifndef ORACLE_TOKENTYPE
# define ORACLE_TOKENTYPE
  enum oracle_tokentype
  {
    ORACLE_NAME = 258,
    ORACLE_STRING = 259,
    ORACLE_INTNUM = 260,
    ORACLE_BOOL = 261,
    ORACLE_APPROXNUM = 262,
    ORACLE_NULLX = 263,
    ORACLE_UNKNOWN = 264,
    ORACLE_QUESTIONMARK = 265,
    ORACLE_PARAM = 266,
    ORACLE_UMINUS = 267,
    ORACLE_ALL = 268,
    ORACLE_AND = 269,
    ORACLE_ANY = 270,
    ORACLE_ARRAY = 271,
    ORACLE_AS = 272,
    ORACLE_ASC = 273,
    ORACLE_AVG = 274,
    ORACLE_BETWEEN = 275,
    ORACLE_BIGINT = 276,
    ORACLE_BINARY = 277,
    ORACLE_BLOB = 278,
    ORACLE_BOOLEAN = 279,
    ORACLE_BY = 280,
    ORACLE_CALL = 281,
    ORACLE_CASE = 282,
    ORACLE_CAST = 283,
    ORACLE_CHAR = 284,
    ORACLE_CHARACTER = 285,
    ORACLE_CHARACTERS = 286,
    ORACLE_CLOB = 287,
    ORACLE_CNNOP = 288,
    ORACLE_COALESCE = 289,
    ORACLE_CODE_UNITS = 290,
    ORACLE_COLLATE = 291,
    ORACLE_COMP_EQ = 292,
    ORACLE_COMP_GE = 293,
    ORACLE_COMP_GT = 294,
    ORACLE_COMP_LE = 295,
    ORACLE_COMP_LT = 296,
    ORACLE_COMP_NE = 297,
    ORACLE_CONVERT = 298,
    ORACLE_CORRESPONDING = 299,
    ORACLE_COUNT = 300,
    ORACLE_CROSS = 301,
    ORACLE_CUME_DIST = 302,
    ORACLE_CURRENT = 303,
    ORACLE_CURRENT_TIMESTAMP = 304,
    ORACLE_CURRENT_USER = 305,
    ORACLE_DATE = 306,
    ORACLE_DAY = 307,
    ORACLE_DEC = 308,
    ORACLE_DECIMAL = 309,
    ORACLE_DEFAULT = 310,
    ORACLE_DELETE = 311,
    ORACLE_DENSE_RANK = 312,
    ORACLE_DESC = 313,
    ORACLE_DISTINCT = 314,
    ORACLE_DOUBLE = 315,
    ORACLE_ELSE = 316,
    ORACLE_END = 317,
    ORACLE_END_P = 318,
    ORACLE_ESCAPE = 319,
    ORACLE_ERROR = 320,
    ORACLE_EXCEPT = 321,
    ORACLE_EXCLUDE = 322,
    ORACLE_EXISTS = 323,
    ORACLE_FLOAT = 324,
    ORACLE_FOLLOWING = 325,
    ORACLE_FOR = 326,
    ORACLE_FROM = 327,
    ORACLE_FULL = 328,
    ORACLE_G = 329,
    ORACLE_GROUP = 330,
    ORACLE_GROUPING = 331,
    ORACLE_HAVING = 332,
    ORACLE_HOUR = 333,
    ORACLE_IN = 334,
    ORACLE_INNER = 335,
    ORACLE_INSERT = 336,
    ORACLE_INT = 337,
    ORACLE_INTEGER = 338,
    ORACLE_INTERSECT = 339,
    ORACLE_INTERVAL = 340,
    ORACLE_INTO = 341,
    ORACLE_IS = 342,
    ORACLE_JOIN = 343,
    ORACLE_K = 344,
    ORACLE_LARGE = 345,
    ORACLE_LEFT = 346,
    ORACLE_LIKE = 347,
    ORACLE_M = 348,
    ORACLE_MAX = 349,
    ORACLE_MIN = 350,
    ORACLE_MINUTE = 351,
    ORACLE_MOD = 352,
    ORACLE_MONTH = 353,
    ORACLE_MULTISET = 354,
    ORACLE_NATIONAL = 355,
    ORACLE_NATURAL = 356,
    ORACLE_NCHAR = 357,
    ORACLE_NCLOB = 358,
    ORACLE_NO = 359,
    ORACLE_NOT = 360,
    ORACLE_NULLIF = 361,
    ORACLE_NUMERIC = 362,
    ORACLE_OBJECT = 363,
    ORACLE_OCTETS = 364,
    ORACLE_OF = 365,
    ORACLE_ON = 366,
    ORACLE_ONLY = 367,
    ORACLE_OR = 368,
    ORACLE_ORDER = 369,
    ORACLE_OTHERS = 370,
    ORACLE_OUTER = 371,
    ORACLE_OVER = 372,
    ORACLE_PARTITION = 373,
    ORACLE_PERCENT_RANK = 374,
    ORACLE_PRECEDING = 375,
    ORACLE_PRECISION = 376,
    ORACLE_RANGE = 377,
    ORACLE_RANK = 378,
    ORACLE_READ = 379,
    ORACLE_REAL = 380,
    ORACLE_RECURSIVE = 381,
    ORACLE_REF = 382,
    ORACLE_RIGHT = 383,
    ORACLE_ROW = 384,
    ORACLE_ROWS = 385,
    ORACLE_ROW_NUMBER = 386,
    ORACLE_SCOPE = 387,
    ORACLE_SECOND = 388,
    ORACLE_SELECT = 389,
    ORACLE_SESSION_USER = 390,
    ORACLE_SET = 391,
    ORACLE_SETS = 392,
    ORACLE_SMALLINT = 393,
    ORACLE_SOME = 394,
    ORACLE_STDDEV = 395,
    ORACLE_STDDEV_POP = 396,
    ORACLE_STDDEV_SAMP = 397,
    ORACLE_SUM = 398,
    ORACLE_SYSTEM_USER = 399,
    ORACLE_THEN = 400,
    ORACLE_TIES = 401,
    ORACLE_TIME = 402,
    ORACLE_TIMESTAMP = 403,
    ORACLE_TO = 404,
    ORACLE_UNBOUNDED = 405,
    ORACLE_UNION = 406,
    ORACLE_UPDATE = 407,
    ORACLE_USING = 408,
    ORACLE_VALUES = 409,
    ORACLE_VARCHAR = 410,
    ORACLE_VARYING = 411,
    ORACLE_VAR_POP = 412,
    ORACLE_VAR_SAMP = 413,
    ORACLE_WHEN = 414,
    ORACLE_WHERE = 415,
    ORACLE_WITH = 416,
    ORACLE_WITHOUT = 417,
    ORACLE_YEAR = 418,
    ORACLE_ZONE = 419,
    ORACLE_RETURNING = 420,
    ORACLE_SKIP = 421,
    ORACLE_LOCKED = 422,
    ORACLE_NOWAIT = 423,
    ORACLE_WAIT = 424,
    ORACLE_NULLS = 425,
    ORACLE_LAST = 426,
    ORACLE_SIBLINGS = 427,
    ORACLE_OFFSET = 428,
    ORACLE_FIRST = 429,
    ORACLE_NEXT = 430,
    ORACLE_FETCH = 431,
    ORACLE_PERCENT = 432,
    ORACLE_MINUS = 433,
    ORACLE_UNIQUE = 434,
    ORACLE_NAN = 435,
    ORACLE_INFINITE = 436,
    ORACLE_REGEXP_LIKE = 437,
    ORACLE_APPLY = 438,
    ORACLE_CYCLE = 439,
    ORACLE_SEARCH = 440,
    ORACLE_DEPTH = 441,
    ORACLE_BREADTH = 442,
    ORACLE_START = 443,
    ORACLE_CONNECT = 444,
    ORACLE_NOCYCLE = 445,
    ORACLE_CONTAINERS = 446,
    ORACLE_SHARDS = 447,
    ORACLE_SAMPLE = 448,
    ORACLE_BLOCK = 449,
    ORACLE_SEED = 450,
    ORACLE_PRIOR = 451
  };
#endif

/* Value type.  */
#if ! defined ORACLE_STYPE && ! defined ORACLE_STYPE_IS_DECLARED

union ORACLE_STYPE
{
#line 85 "oracle.y" /* yacc.c:355  */

    struct Node* node;
    int    ival;
    NodeType nodetype;

#line 369 "sqlparser_oracle_bison.cpp" /* yacc.c:355  */
};

typedef union ORACLE_STYPE ORACLE_STYPE;
# define ORACLE_STYPE_IS_TRIVIAL 1
# define ORACLE_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined ORACLE_LTYPE && ! defined ORACLE_LTYPE_IS_DECLARED
typedef struct ORACLE_LTYPE ORACLE_LTYPE;
struct ORACLE_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define ORACLE_LTYPE_IS_DECLARED 1
# define ORACLE_LTYPE_IS_TRIVIAL 1
#endif



int oracle_parse (ParseResult* result, yyscan_t scanner);

#endif /* !YY_ORACLE_SQLPARSER_ORACLE_BISON_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 399 "sqlparser_oracle_bison.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined ORACLE_LTYPE_IS_TRIVIAL && ORACLE_LTYPE_IS_TRIVIAL \
             && defined ORACLE_STYPE_IS_TRIVIAL && ORACLE_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  124
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   7006

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  212
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  187
/* YYNRULES -- Number of rules.  */
#define YYNRULES  568
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  1000

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   451

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   208,     2,     2,     2,   211,     2,     2,
      13,    14,   204,   206,   203,   207,    15,   210,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,   200,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   205,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   201,   209,   202,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199
};

#if ORACLE_DEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   243,   243,   254,   255,   263,   264,   265,   266,   270,
     275,   280,   285,   293,   294,   302,   307,   315,   316,   320,
     324,   328,   329,   330,   334,   342,   347,   352,   357,   362,
     371,   380,   397,   413,   414,   421,   422,   429,   430,   431,
     436,   437,   445,   454,   462,   472,   484,   485,   512,   539,
     569,   570,   600,   601,   614,   621,   623,   625,   627,   635,
     661,   689,   690,   693,   694,   703,   711,   712,   720,   721,
     729,   730,   731,   741,   742,   746,   751,   759,   760,   768,
     777,   780,   784,   791,   792,   793,   797,   798,   808,   813,
     821,   822,   831,   839,   840,   844,   852,   853,   861,   862,
     866,   873,   874,   882,   896,   900,   901,   909,   910,   914,
     915,   927,   932,   937,   942,   947,   954,   961,   968,   975,
     985,   986,   991,   995,  1001,  1007,  1016,  1022,  1028,  1034,
    1040,  1112,  1117,  1122,  1127,  1132,  1137,  1142,  1147,  1156,
    1157,  1158,  1159,  1163,  1169,  1177,  1182,  1188,  1193,  1202,
    1211,  1218,  1227,  1238,  1249,  1260,  1267,  1268,  1273,  1274,
    1282,  1283,  1291,  1292,  1300,  1301,  1306,  1314,  1315,  1333,
    1334,  1335,  1336,  1337,  1338,  1339,  1340,  1344,  1353,  1361,
    1369,  1374,  1382,  1387,  1392,  1397,  1405,  1410,  1418,  1423,
    1431,  1432,  1433,  1436,  1445,  1446,  1451,  1459,  1460,  1468,
    1469,  1477,  1478,  1486,  1487,  1492,  1500,  1501,  1502,  1503,
    1508,  1509,  1513,  1514,  1522,  1523,  1530,  1530,  1535,  1536,
    1537,  1538,  1539,  1540,  1541,  1542,  1543,  1544,  1548,  1549,
    1554,  1561,  1562,  1566,  1567,  1568,  1569,  1573,  1574,  1575,
    1576,  1577,  1578,  1579,  1580,  1581,  1582,  1587,  1595,  1596,
    1600,  1601,  1609,  1614,  1622,  1623,  1632,  1633,  1634,  1635,
    1640,  1646,  1652,  1658,  1664,  1676,  1691,  1692,  1693,  1694,
    1695,  1696,  1697,  1698,  1699,  1700,  1701,  1705,  1714,  1715,
    1716,  1717,  1718,  1722,  1726,  1735,  1740,  1744,  1752,  1753,
    1757,  1758,  1763,  1764,  1773,  1774,  1778,  1779,  1783,  1784,
    1785,  1789,  1793,  1798,  1799,  1800,  1804,  1808,  1809,  1810,
    1811,  1812,  1816,  1822,  1834,  1840,  1847,  1854,  1861,  1870,
    1877,  1884,  1891,  1898,  1907,  1914,  1927,  1928,  1929,  1930,
    1934,  1942,  1947,  1952,  1957,  1966,  1974,  1980,  1985,  1993,
    1998,  2003,  2008,  2009,  2014,  2015,  2016,  2017,  2018,  2019,
    2023,  2031,  2036,  2040,  2046,  2050,  2051,  2056,  2063,  2068,
    2069,  2074,  2079,  2086,  2090,  2094,  2098,  2102,  2109,  2116,
    2120,  2125,  2130,  2135,  2139,  2143,  2148,  2153,  2158,  2162,
    2169,  2170,  2174,  2179,  2184,  2189,  2194,  2199,  2204,  2208,
    2212,  2216,  2220,  2224,  2228,  2235,  2240,  2244,  2248,  2255,
    2260,  2265,  2270,  2275,  2280,  2285,  2289,  2294,  2298,  2303,
    2307,  2311,  2318,  2323,  2327,  2332,  2339,  2344,  2348,  2353,
    2357,  2362,  2367,  2372,  2377,  2382,  2386,  2391,  2395,  2400,
    2404,  2411,  2416,  2421,  2426,  2434,  2438,  2442,  2449,  2453,
    2457,  2464,  2468,  2475,  2476,  2477,  2481,  2482,  2486,  2487,
    2491,  2496,  2506,  2507,  2511,  2512,  2516,  2517,  2518,  2519,
    2520,  2521,  2522,  2526,  2528,  2529,  2530,  2531,  2532,  2533,
    2534,  2535,  2536,  2537,  2538,  2543,  2544,  2545,  2546,  2547,
    2548,  2549,  2550,  2551,  2552,  2553,  2554,  2555,  2556,  2557,
    2558,  2559,  2560,  2561,  2562,  2563,  2564,  2565,  2567,  2568,
    2569,  2570,  2571,  2572,  2573,  2574,  2575,  2576,  2577,  2578,
    2579,  2583,  2584,  2585,  2586,  2589,  2590,  2591,  2592,  2593,
    2598,  2599,  2606,  2613,  2614,  2622,  2623,  2630,  2631,  2634,
    2635,  2636,  2643,  2650,  2651,  2654,  2655,  2658,  2659,  2660,
    2663,  2664,  2672,  2673,  2676,  2677,  2680,  2681,  2700,  2721,
    2722,  2725,  2732,  2741,  2742,  2746,  2753,  2754,  2755,  2756,
    2759,  2772,  2781,  2786,  2791,  2799,  2805,  2814,  2815
};
#endif

#if ORACLE_DEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NAME", "STRING", "INTNUM", "BOOL",
  "APPROXNUM", "NULLX", "UNKNOWN", "QUESTIONMARK", "PARAM", "UMINUS",
  "'('", "')'", "'.'", "ALL", "AND", "ANY", "ARRAY", "AS", "ASC", "AVG",
  "BETWEEN", "BIGINT", "BINARY", "BLOB", "BOOLEAN", "BY", "CALL", "CASE",
  "CAST", "CHAR", "CHARACTER", "CHARACTERS", "CLOB", "CNNOP", "COALESCE",
  "CODE_UNITS", "COLLATE", "COMP_EQ", "COMP_GE", "COMP_GT", "COMP_LE",
  "COMP_LT", "COMP_NE", "CONVERT", "CORRESPONDING", "COUNT", "CROSS",
  "CUME_DIST", "CURRENT", "CURRENT_TIMESTAMP", "CURRENT_USER", "DATE",
  "DAY", "DEC", "DECIMAL", "DEFAULT", "DELETE", "DENSE_RANK", "DESC",
  "DISTINCT", "DOUBLE", "ELSE", "END", "END_P", "ESCAPE", "ERROR",
  "EXCEPT", "EXCLUDE", "EXISTS", "FLOAT", "FOLLOWING", "FOR", "FROM",
  "FULL", "G", "GROUP", "GROUPING", "HAVING", "HOUR", "IN", "INNER",
  "INSERT", "INT", "INTEGER", "INTERSECT", "INTERVAL", "INTO", "IS",
  "JOIN", "K", "LARGE", "LEFT", "LIKE", "M", "MAX", "MIN", "MINUTE", "MOD",
  "MONTH", "MULTISET", "NATIONAL", "NATURAL", "NCHAR", "NCLOB", "NO",
  "NOT", "NULLIF", "NUMERIC", "OBJECT", "OCTETS", "OF", "ON", "ONLY", "OR",
  "ORDER", "OTHERS", "OUTER", "OVER", "PARTITION", "PERCENT_RANK",
  "PRECEDING", "PRECISION", "RANGE", "RANK", "READ", "REAL", "RECURSIVE",
  "REF", "RIGHT", "ROW", "ROWS", "ROW_NUMBER", "SCOPE", "SECOND", "SELECT",
  "SESSION_USER", "SET", "SETS", "SMALLINT", "SOME", "STDDEV",
  "STDDEV_POP", "STDDEV_SAMP", "SUM", "SYSTEM_USER", "THEN", "TIES",
  "TIME", "TIMESTAMP", "TO", "UNBOUNDED", "UNION", "UPDATE", "USING",
  "VALUES", "VARCHAR", "VARYING", "VAR_POP", "VAR_SAMP", "WHEN", "WHERE",
  "WITH", "WITHOUT", "YEAR", "ZONE", "RETURNING", "SKIP", "LOCKED",
  "NOWAIT", "WAIT", "NULLS", "LAST", "SIBLINGS", "OFFSET", "FIRST", "NEXT",
  "FETCH", "PERCENT", "MINUS", "UNIQUE", "NAN", "INFINITE", "REGEXP_LIKE",
  "APPLY", "CYCLE", "SEARCH", "DEPTH", "BREADTH", "START", "CONNECT",
  "NOCYCLE", "CONTAINERS", "SHARDS", "SAMPLE", "BLOCK", "SEED", "PRIOR",
  "';'", "'{'", "'}'", "','", "'*'", "'^'", "'+'", "'-'", "'!'", "'|'",
  "'/'", "'%'", "$accept", "sql_stmt", "stmt_list", "stmt", "call_stmt",
  "sql_argument_list", "sql_argument", "value_expression", "sp_name",
  "dql_stmt", "dml_stmt", "insert_stmt", "insert_columns_and_source",
  "from_constructor", "delete_stmt", "update_stmt", "opt_returning_into",
  "variable_list", "variable", "update_elem_list", "update_elem",
  "select_stmt", "query_expression", "query_expression_body", "query_term",
  "query_primary", "select_with_parens", "subquery", "table_subquery",
  "row_subquery", "cte_subquery", "simple_table",
  "opt_hierarchical_query_clause", "opt_where", "opt_from_clause",
  "opt_groupby", "grouping_element_list", "grouping_element",
  "opt_order_by", "order_by", "sort_list", "sort_key", "opt_asc_desc",
  "opt_nulls_c", "opt_having", "with_clause", "with_list",
  "common_table_expr", "opt_derived_column_list",
  "simple_ident_list_with_parens", "simple_ident_list", "opt_distinct",
  "select_expr_list", "projection", "from_list", "table_reference",
  "table_primary", "table_primary_non_join", "partition_by",
  "table_function", "opt_simple_ident_list_with_parens", "asterisk_expr",
  "column_ref", "relation_factor", "joined_table", "cross_apply",
  "qualified_join", "cross_join", "natural_join", "join_type",
  "join_outer", "search_condition", "boolean_term", "boolean_factor",
  "boolean_test", "boolean_primary", "predicate", "bool_function",
  "comparison_predicate", "quantified_comparison_predicate",
  "between_predicate", "like_predicate", "in_predicate", "null_predicate",
  "null_value", "exists_predicate", "row_expr", "factor0", "factor1",
  "factor2", "factor3", "factor4", "row_expr_list", "in_expr",
  "truth_value", "comp_op", "cnn_op", "comp_all_some_any_op",
  "plus_minus_op", "star_div_percent_mod_op", "expr_const", "case_expr",
  "case_arg", "when_clause_list", "when_clause", "case_default",
  "func_expr", "aggregate_windowed_function", "aggregate_function_name",
  "ranking_windowed_function", "ranking_function_name", "over_clause",
  "window_specification", "window_name", "window_specification_details",
  "opt_existing_window_name", "opt_window_partition_clause",
  "opt_window_frame_clause", "window_frame_units", "window_frame_extent",
  "window_frame_start", "window_frame_preceding", "window_frame_between",
  "window_frame_bound", "window_frame_following",
  "opt_window_frame_exclusion", "scalar_function", "function_call_keyword",
  "data_type", "user_defined_type_name", "relation_factor_type",
  "reference_type", "collection_type", "predefined_type", "interval_type",
  "interval_qualifier", "start_field", "end_field",
  "single_datetime_field", "non_second_primary_datetime_field",
  "boolean_type", "datetime_type", "numeric_type", "exact_numeric_type",
  "approximate_numeric_type", "character_string_type",
  "binary_large_object_string_type", "national_character_string_type",
  "large_object_length", "char_length_units", "multiplier",
  "distinct_or_all", "all_some_any", "opt_as_label", "as_label", "label",
  "collate_clause", "name_r", "name_type", "reserved_type",
  "reserved_other", "reserved_non_type", "reserved", "opt_for_update",
  "for_update", "opt_of_column_ref_list", "column_ref_list",
  "opt_ignore_lock", "opt_wait_nowait", "row_limiting_clause", "row_rows",
  "opt_variable_v2", "variable_v2", "opt_offset_clause", "first_next",
  "only_or_ties", "opt_fetch_rows_clause", "opt_search_clause",
  "search_clause", "opt_cycle_clause", "cycle_clause", "connect_opt",
  "connect_by", "start_with", "hierarchical_query_clause", "sample_clause",
  "opt_seed_value", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,    40,    41,    46,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303,   304,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,   316,   317,   318,   319,   320,   321,
     322,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,   338,   339,   340,   341,
     342,   343,   344,   345,   346,   347,   348,   349,   350,   351,
     352,   353,   354,   355,   356,   357,   358,   359,   360,   361,
     362,   363,   364,   365,   366,   367,   368,   369,   370,   371,
     372,   373,   374,   375,   376,   377,   378,   379,   380,   381,
     382,   383,   384,   385,   386,   387,   388,   389,   390,   391,
     392,   393,   394,   395,   396,   397,   398,   399,   400,   401,
     402,   403,   404,   405,   406,   407,   408,   409,   410,   411,
     412,   413,   414,   415,   416,   417,   418,   419,   420,   421,
     422,   423,   424,   425,   426,   427,   428,   429,   430,   431,
     432,   433,   434,   435,   436,   437,   438,   439,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   449,   450,   451,
      59,   123,   125,    44,    42,    94,    43,    45,    33,   124,
      47,    37
};
# endif

#define YYPACT_NINF -809

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-809)))

#define YYTABLE_NINF -355

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      49,   132,  6505,    39,     0,    44,  6505,  6659,    65,   127,
      82,   -16,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
      43,   187,  -809,  -809,    56,   197,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,   314,  -809,  -809,  -809,  -809,   302,  -809,
    -809,  -809,  6505,  6505,  -809,  -809,    72,  3574,   228,  6505,
    -809,   179,   391,  6505,  -809,  -809,    49,    72,    35,    72,
      72,   316,  -809,    72,    43,  -809,  2220,  5575,   246,   124,
    3574,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  3396,  3957,
     402,   414,   428,   432,  -809,   459,  -809,   455,   461,   466,
    -809,  -809,   479,  -809,  3957,  3957,  -809,  -809,  -809,   412,
     289,  -809,  -809,   484,  1847,    96,   -33,   298,  -809,  -809,
    -809,  -809,  -809,  -809,   495,  -809,   503,  -809,  -809,   505,
     326,  3957,  -809,  6505,  6505,   493,  -809,   511,  -809,    56,
    3957,   510,    56,    56,   371,   352,  -809,    56,   316,  2015,
    -809,   520,  2630,   533,   538,   359,   536,   448,   558,  -809,
    -809,   488,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,   500,   395,  5729,   568,  2630,  -809,  5083,   430,  3957,
    -809,  -809,  -809,   148,   412,  3396,   578,     6,   580,    89,
     435,  3957,  3957,  3957,   598,  -809,   593,  3957,   595,  -809,
    -809,  -809,  5243,   246,  3574,  3779,  6505,  -809,  6505,   401,
    3957,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  3957,  -809,
    -809,  -809,  -809,  3957,  3957,  2835,   601,  3957,  4289,   246,
     420,    37,  -809,   603,   421,   606,  2425,   187,  -809,   422,
      61,  3957,   187,   187,   515,   381,  -809,   451,  -809,   352,
    2015,    73,   290,   132,  -809,  -809,  -809,   500,  3957,  -809,
    2630,  4923,  2630,  2630,   133,  3957,  -809,  -809,   591,  -809,
     386,  -809,   619,    60,  3957,   301,   596,  3040,   520,  5575,
    6505,   618,  5883,   448,  -809,  -809,  -809,  -809,   246,   624,
    -809,  3957,  -809,  2630,   576,   435,    18,   627,     7,   628,
    -809,    54,  -809,  4769,   630,   633,   634,  6037,  -809,   293,
    -809,  -809,  6037,  6191,  -809,  -809,  -809,  -809,  -809,   297,
    -809,  -809,   635,  -809,  -809,  -809,    96,   -33,   298,  -809,
    -809,  -809,   637,    12,  3957,   528,    13,  -809,   419,   485,
    3957,  3957,  -809,  6505,   132,   477,   464,   653,  3957,  -809,
    -809,   499,  -809,  6505,   323,  -809,  -809,  -809,   373,   334,
    -809,  -809,  -809,   657,  -809,  -809,  -809,   581,   660,  -809,
     186,   201,   668,  -809,   669,   671,   570,   679,  -809,  -809,
     267,   498,   271,   682,   683,  -809,   684,  -809,   103,   118,
     685,  -809,  -809,    86,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,   120,  -809,   662,   687,
    -809,  -809,   558,  -809,  -809,  -809,   368,  -809,     8,  -809,
    -809,  -809,  3396,  -809,  -809,  -809,    75,  -809,  -809,  -809,
      62,  3957,   619,  3957,  -809,  -809,  -809,  -809,    89,  -809,
    -809,   437,  -809,  6505,  6505,   692,   297,   417,  -809,   209,
     377,  3957,   646,  -809,  4923,  -809,  6505,  -809,  3957,  4769,
     508,   698,  6505,  6505,  6505,   391,    50,   599,  -809,  5243,
     599,   353,   535,   599,  5243,   631,  -809,     4,   602,  6037,
     560,   697,   648,   539,   537,  -809,  -809,   528,   528,    14,
    6351,  -809,  -809,  4449,  3957,  -809,  -809,    89,  -809,   717,
     369,   545,  -809,  -809,   531,  -809,   266,  -809,   534,   719,
    -809,  -809,   731,   571,  -809,  -809,  -809,  -809,  -809,   381,
    -809,   632,   734,   737,   636,   732,   739,   638,   733,   745,
     747,   748,  -809,   749,   742,  -809,   604,  -809,   100,   145,
     296,   752,   649,   746,   745,   757,  6505,   759,   615,   616,
     762,   620,   621,   767,   760,  -809,   639,  -809,  -809,  6810,
    -809,  3957,   763,  -809,  3957,    19,  -809,    63,  5883,  -809,
    -809,  6505,   648,  3957,  3957,    89,  -809,   304,   765,    25,
    -809,   766,   768,   771,  -809,  -809,  5243,  5243,  -809,  -809,
     585,  -809,  5243,   690,  5243,  -809,  -809,  5243,   769,   773,
     761,  -809,  -809,  2630,   200,   764,   696,  -809,  -809,  -809,
    -809,   528,  6505,  -809,  -809,  -809,  -809,   443,   699,  -809,
     613,   617,  6505,  -809,  -809,  -809,  -809,  -809,  6505,  6505,
    -809,   623,  -809,   182,  -809,   782,   783,   785,   788,   791,
     790,   789,   802,   423,   794,    26,    27,   795,   806,   324,
     807,   808,   801,   810,   705,   804,   809,   811,   815,   812,
      42,   813,   814,   654,   655,   816,   658,   664,   818,   828,
    6505,   821,    89,  -809,    89,  3957,  3957,   462,  -809,   696,
      89,    89,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,
    2630,   391,  -809,  5243,  -809,   644,   823,   834,   827,   448,
     642,  -809,  2630,    72,  2630,  -809,  -809,  -809,   829,   721,
    4609,  5421,   817,   819,   707,  -809,   833,  -809,   373,   195,
     839,  -809,  -809,   745,   835,  -809,   745,   836,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,    58,  -809,  -809,   846,  -809,
     853,  -809,    45,   847,  -809,  -809,   845,   848,   858,   850,
     852,   861,  -809,   745,   854,  -809,  -809,   862,   736,   416,
    -809,  -809,   436,  -809,  -809,  -809,   855,   662,  6810,    89,
      89,  6505,  -809,   448,  -809,  -809,  2630,   391,   674,   859,
    6505,  -809,   448,  4135,   448,  -809,   849,   758,  -809,   467,
    -809,  -809,  -809,   673,  -809,  6505,  6505,  6505,  6505,   195,
    -809,   725,  -809,   864,   866,  -809,   869,  -809,  -809,   871,
     872,  -809,   882,   883,   740,  -809,   875,  -809,   745,   877,
     879,  -809,   880,  6505,   750,   751,   754,   756,  -809,  -809,
     881,   448,  -809,   885,  -809,   674,   893,  3218,   755,  -809,
     694,    89,  3957,   211,  6505,  5421,   119,   119,   772,   895,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,   894,   898,  -809,
     899,  -809,  -809,  -809,  -809,   753,   770,   774,   775,  6810,
     911,  -809,  -809,  -809,   886,  4135,  -809,  -809,  -809,  -809,
     652,  -809,  -809,   499,   499,   913,  6505,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,  -809,   904,  4135,  -809,   562,   787,
     479,   803,   805,   860,  -809,  -809,  -809,   786,   792,   874,
     914,  -809,   919,    51,   227,  -809,   921,  -809,  -809,  -809,
    -809,   300,  -809,  6505,  6505,   932,  -809,  -809,  -809,   562,
     820,  -809,   825,  -809,  -809,  -809,  -809,  -809,  -809,  -809
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       5,     0,     0,     0,     0,    98,     0,     0,     0,     0,
       0,     3,     8,     6,     7,    23,    22,    21,    20,    43,
      73,    46,    50,    52,     0,     0,   452,   457,   505,   266,
     469,   458,   465,   478,   468,   461,   464,   506,   479,   507,
     480,   274,   281,   365,   470,   279,   500,   472,   504,   481,
     477,   275,   366,   456,   463,   475,   482,   476,   267,   268,
     367,   364,   483,   471,   462,   466,   508,   467,   484,   485,
     498,   486,   503,   280,   487,   488,   278,   499,   474,   489,
     490,   491,   282,   492,   493,   502,   276,   270,   271,   269,
     501,   460,   459,   494,   495,   473,   272,   273,   496,   363,
     497,   509,   510,     0,    19,   515,   516,   517,   131,   519,
     518,   453,     0,     0,    99,   100,    98,     0,     0,   489,
      88,    90,    93,     0,     1,     2,     5,    98,     0,    98,
      98,   520,    74,    98,    73,    53,     0,     0,    63,     0,
       0,   237,   240,   238,   241,   239,   242,   243,     0,   248,
     478,   506,   507,   320,   322,     0,   244,     0,   508,     0,
     324,   325,   459,   122,     0,     0,    55,    57,   208,     0,
     101,   104,   206,     0,   446,   194,   197,   199,   201,   203,
     207,   210,   211,   259,   515,   258,   516,   257,   256,   517,
     126,     0,    89,     0,     0,     0,    94,     0,     4,     0,
       0,     0,     0,     0,     0,   540,   521,     0,   520,     0,
      10,     0,     0,     0,     0,    13,    15,    17,   158,   160,
     162,   164,   167,   176,   169,   174,   170,   172,   171,   173,
     175,    18,   126,     0,   132,     0,    31,     0,     0,     0,
      24,    28,    25,     0,     0,     0,     0,   212,     0,   249,
       0,     0,     0,     0,     0,   245,     0,     0,     0,   246,
     204,   205,     0,    63,     0,     0,     0,   228,     0,     0,
       0,   103,   447,   449,   195,   450,   231,   232,     0,   236,
     233,   234,   235,     0,     0,     0,     0,     0,     0,    63,
      40,     0,    91,     0,    96,     0,     0,    48,    75,    77,
      80,     0,    47,    49,   523,     0,    44,   546,    51,   540,
       0,     0,   212,     0,    56,   193,   163,     0,     0,     9,
       0,     0,     0,     0,     0,     0,   222,   220,   221,   218,
     219,   223,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   134,     0,    64,    29,    30,    27,    26,    63,     0,
      54,     0,   209,     0,   254,   250,     0,     0,     0,     0,
     316,     0,   315,     0,   486,     0,     0,   446,    65,   105,
     107,   109,   446,   446,   108,   142,   139,   140,   141,    61,
     102,   312,     0,   448,   451,   229,   196,   198,   200,   202,
     441,   442,     0,     0,     0,     0,     0,   123,   127,    33,
       0,     0,    95,     0,     0,   549,     0,     0,     0,    81,
      82,    83,    76,     0,   529,   539,   538,   537,     0,     0,
     532,    45,   168,     0,    14,   454,   392,     0,   415,   368,
     410,   411,   409,   369,   388,   394,     0,   397,   391,   390,
       0,     0,   430,   429,   393,   398,   490,   389,     0,     0,
     495,   511,   512,    16,   327,   330,   328,   329,   326,   349,
     513,   347,   348,   346,   380,   381,   342,   345,   344,   331,
     514,   455,   159,   161,   216,   217,     0,   165,     0,   224,
     225,   226,     0,   214,   186,   190,     0,   191,   192,   188,
     182,     0,     0,     0,   227,   443,   445,   444,   178,   230,
     179,   127,   138,     0,     0,   133,    61,    54,   213,     0,
       0,     0,     0,   251,     0,   319,     0,   321,     0,     0,
       0,   108,     0,     0,     0,   120,     0,   156,   155,     0,
     156,     0,     0,   156,     0,     0,   113,   510,   111,   446,
       0,     0,    66,   563,     0,    62,   313,   265,   261,     0,
       0,   277,   314,     0,     0,    32,    41,    42,    97,     0,
       0,   553,   550,    12,     0,    78,     0,    79,   525,   126,
     524,   530,     0,   527,   533,   534,   541,   542,   543,   535,
     177,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   396,     0,   362,   350,     0,   352,   359,   419,
     417,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   337,   338,     0,   341,   343,     0,
     166,     0,     0,   189,     0,     0,   187,   184,     0,   136,
     137,     0,    66,     0,     0,   255,   247,     0,     0,     0,
     110,     0,     0,     0,   121,   112,     0,     0,   157,   152,
       0,   153,     0,     0,     0,   154,   106,     0,     0,     0,
       0,   115,   114,     0,   556,     0,    86,   562,   564,   264,
     260,   263,   288,   284,   283,   286,   124,   128,     0,    58,
       0,     0,     0,    92,   554,    11,    85,    84,     0,     0,
     531,     0,   522,     0,   536,   413,     0,     0,   407,     0,
       0,   405,     0,   434,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   427,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   332,   180,   215,   183,     0,     0,   128,   135,    86,
     252,   253,   317,   318,   323,   117,   118,   119,   149,   143,
       0,     0,   151,     0,   144,     0,     0,     0,     0,   561,
     557,   559,     0,    98,     0,    59,   262,   289,     0,   290,
       0,     0,     0,     0,     0,   526,   127,   528,     0,     0,
       0,   414,   400,     0,     0,   399,     0,     0,   435,   436,
     440,   438,   439,   437,   433,   432,   408,   387,     0,   385,
       0,   395,     0,   357,   351,   355,     0,     0,     0,     0,
     425,     0,   420,     0,     0,   428,   383,     0,     0,   372,
     373,   374,   377,   378,   379,   403,     0,   340,     0,   181,
     185,     0,    60,   146,   148,   150,     0,     0,   567,     0,
       0,   558,   560,     0,    87,   285,     0,    73,   125,   129,
      38,    37,    34,    35,    39,     0,     0,     0,     0,     0,
     544,     0,   547,     0,     0,   402,     0,   401,   431,     0,
       0,   361,     0,     0,   358,   418,     0,   416,     0,     0,
       0,   423,     0,     0,     0,     0,     0,     0,   336,   339,
     333,   145,   147,     0,   566,   567,     0,     0,   275,    67,
      68,    71,     0,   292,     0,     0,    80,    80,     0,   128,
     548,   545,   412,   406,   404,   386,   384,     0,     0,   422,
       0,   421,   426,   382,   335,     0,     0,     0,     0,     0,
       0,   565,   116,    70,     0,     0,   291,   295,   294,   287,
       0,   130,    36,    83,    83,     0,     0,   360,   356,   424,
     370,   371,   375,   376,   334,     0,     0,    69,     0,     0,
       0,     0,     0,   307,   296,   300,   297,     0,     0,     0,
     129,   568,     0,     0,     0,   303,     0,   305,   299,   298,
     301,     0,   293,     0,     0,     0,    72,   304,   306,     0,
       0,   309,     0,   310,   551,   552,   555,   302,   308,   311
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -809,  -809,   824,  -809,  -809,  -173,  -809,  -809,   822,  -809,
    -809,  -809,  -809,   703,  -809,  -809,  -809,    34,  -809,   547,
    -809,  -117,    10,   925,   236,   744,  -809,  -176,  -181,  -809,
    -809,  -809,   450,  -182,   714,   327,  -554,  -809,  -133,  -809,
    -235,  -809,  -301,  -322,   221,  -809,   135,  -809,  -809,  -137,
    -391,  -109,   -61,  -809,   427,  -350,  -300,  -809,  -809,  -809,
    -809,  -809,  -398,    31,  -334,  -809,  -809,  -809,  -809,   431,
     -79,  -201,   641,  -128,  -809,  -809,  -809,  -809,  -809,  -809,
    -809,  -809,  -809,  -809,   478,  -809,  1125,   695,   689,   686,
      11,  -809,  -233,   476,   494,  -809,  -809,  -809,  -809,  -809,
    -808,  -809,  -809,   622,  -809,  -809,  -809,  -809,   670,  -809,
     884,  -473,  -809,   303,  -809,  -809,  -809,  -809,  -809,  -809,
      32,  -809,  -809,   -15,  -809,  -809,  -809,  -809,   465,  -809,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,   -45,
    -809,  -809,  -809,  -809,  -809,  -809,  -809,  -809,  -581,   178,
    -809,  -809,  -809,  -302,  -809,   710,  -452,    -2,  -610,  -809,
    -318,  -809,  -809,   776,  -809,  -809,  -657,  -809,  -809,   676,
    -613,  -809,   399,  -809,  -809,   128,  -809,  -809,  -809,  -809,
    -809,  -809,   439,   446,  -809,  -809,    91
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     9,    10,    11,    12,   214,   215,   216,   103,    13,
      14,    15,   240,   241,    16,    17,   555,   852,   853,   289,
     290,    18,   246,    20,    21,    22,   166,   167,   367,   168,
     405,    23,   542,   236,   263,   666,   899,   900,   131,   132,
     298,   299,   411,   567,   765,    24,   120,   121,   195,   196,
     293,   117,   169,   170,   368,   369,   370,   371,   661,   372,
     645,   171,   172,   173,   374,   375,   376,   377,   378,   535,
     649,   217,   218,   219,   220,   221,   222,   223,   224,   225,
     226,   227,   228,   229,   489,   230,   247,   175,   176,   177,
     178,   179,   248,   484,   477,   337,   270,   338,   278,   283,
     180,   181,   250,   354,   355,   512,   182,   183,   105,   185,
     106,   551,   673,   674,   768,   769,   847,   939,   940,   963,
     975,   965,   966,   976,   977,   982,   187,   188,   453,   454,
     455,   456,   457,   458,   459,   595,   596,   804,   597,   107,
     461,   462,   463,   464,   465,   466,   467,   468,   704,   794,
     795,   394,   499,   271,   272,   273,   274,   232,   469,   109,
     110,   471,   111,   205,   206,   414,   570,   692,   573,   306,
     576,   693,   418,   307,   579,   862,   420,   561,   562,   683,
     684,   762,   543,   544,   545,   539,   894
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     108,   208,   243,   470,   108,   122,   345,   140,   311,   731,
      19,    25,   558,   520,   617,   568,   618,   658,   199,   357,
     202,   203,   242,   719,   207,   621,   548,   552,   671,   521,
     315,   775,   382,   104,   343,   314,   735,   118,   514,   744,
     797,   799,   267,   267,   267,   268,   268,   268,   267,   267,
     267,   268,   268,   268,   267,   267,   816,   268,   268,   871,
     114,   267,     1,   200,   268,   525,   412,   279,   485,     1,
     536,   538,   189,   267,   669,   670,   268,   401,     2,   244,
     779,   379,   409,   485,   316,   423,   314,   422,   114,   113,
     267,   189,   788,   268,   123,   189,   789,   267,   267,   267,
     268,   268,   268,   189,   189,   614,   115,   399,     3,   311,
     108,   108,   127,   710,   112,   190,   607,   122,   508,   189,
     189,   108,   410,   407,   987,   267,   347,   124,   268,   624,
     736,   610,   962,     4,   115,   234,    19,   237,   190,   474,
     409,   646,   475,   138,   139,     1,   189,   424,   125,    19,
     974,   483,   509,   616,   104,   189,   314,   500,   711,   268,
     128,     1,   314,   516,   189,   859,   506,   189,   486,   520,
     793,   280,   275,   565,   979,   260,   261,   281,   282,   650,
     410,   974,   238,   896,   126,   521,     5,   314,   615,   322,
     189,   122,   294,     5,   189,   473,   470,   129,   766,   583,
     189,   659,   864,   380,     6,   866,   189,   189,   189,   351,
     201,   135,   189,     7,   586,   269,   269,   269,   890,   189,
     189,   269,   269,   269,   130,   189,   116,   269,   269,   798,
     800,   341,   880,   189,   269,   294,   647,   662,   189,   189,
     189,   476,   189,   487,   488,   817,   269,    25,   872,   622,
       8,   189,  -354,    19,   192,   349,   189,   518,   487,   488,
     108,     5,   190,   269,   275,   189,   108,   608,   609,     5,
     269,   269,   269,   189,   133,   189,   460,   189,   189,   584,
     189,   239,   611,   612,   601,     5,   398,   559,     7,   189,
     568,   774,   189,   373,   587,   389,     7,   920,   269,   384,
     988,   470,   276,   277,   712,   239,   189,   755,   189,   713,
     860,   483,     7,   325,   574,   575,   314,   137,   742,   954,
     349,   678,    43,   614,   491,   322,   267,   136,   292,   268,
     326,   327,   328,   329,   330,   331,   937,   501,   502,  -131,
     505,   288,   526,   314,   938,   585,   748,   749,    52,   189,
     980,   990,   752,   314,   754,   189,   189,   633,   314,   861,
     588,   108,   778,   189,   602,   275,    60,   191,    61,   527,
     275,   275,   332,   854,   474,   889,   528,   475,   991,    43,
     333,   957,   193,   492,   529,   334,   415,   530,   644,   714,
     204,   416,   417,   760,   373,   598,   493,   531,   335,   761,
     325,   294,   972,   594,   194,    52,   615,   992,  -131,   235,
     339,   569,   532,   267,    19,   251,   268,   326,   327,   328,
     329,   330,   331,    60,   533,    61,   480,   252,   481,   527,
     603,   -53,  -132,    99,   553,   297,   528,   189,   302,   303,
     686,   253,   568,   687,   652,   254,   189,   530,   189,   993,
    -132,   651,   628,   835,   655,   715,  -133,   788,   770,   332,
     803,   789,   759,   255,   906,   907,   189,   333,   256,   460,
     314,   314,   334,   189,   257,  -133,   314,   831,   314,   258,
    -135,   314,   904,   259,   533,   335,   -53,   262,   540,   541,
      99,   -53,   264,   351,   571,   572,   534,   265,   336,   269,
     790,   629,   630,   284,   -53,   574,   575,   854,   285,   189,
     470,   577,   578,   295,   108,   791,   286,   108,   287,   792,
     108,   108,   108,   325,   296,   634,   304,   108,   305,   349,
     599,   600,   108,   313,   -53,   793,   267,   275,   301,   268,
     326,   327,   328,   329,   330,   331,   318,   638,   675,   833,
     373,   677,   319,   641,   642,   643,   321,   526,   680,   681,
     373,   842,   320,   844,   322,   373,   141,   142,   143,   144,
     145,   -53,   146,   147,   460,   323,   189,   314,   324,   189,
     884,   885,   332,   342,   527,   336,   269,   344,   189,   189,
     333,   528,   350,   -53,   352,   334,   -53,   353,   -53,   529,
     886,   887,   530,   359,   108,   943,   944,   360,   335,   362,
     385,   470,   531,   959,   834,   395,   155,   402,   189,   404,
     156,   967,   968,   400,   403,   408,   737,   532,   413,   738,
     419,   479,   482,   503,   526,   891,   494,   721,   507,   533,
     511,   515,   517,   522,   108,   108,   523,   524,   550,   546,
     108,   547,   108,   554,   843,   108,   141,   142,   143,   144,
     145,   527,   146,   147,   805,   560,   563,   564,   528,   936,
     675,   580,   566,   582,   581,   958,   529,   373,   373,   530,
     294,   589,   590,   373,   591,   373,   569,   776,   373,   531,
     189,   189,   593,   526,   592,   604,   605,   606,   613,   750,
     892,   268,   619,   959,   532,   189,   155,   631,   336,   269,
     156,   636,   640,   960,   903,   973,   533,   189,   648,   189,
     527,   654,   657,   660,   663,   664,   665,   528,   108,   541,
     540,   679,   682,   685,   689,   529,   690,   688,   530,   696,
     691,   751,   697,   695,   700,   699,   702,   698,   531,   701,
     703,   108,   705,   706,   707,   708,   709,   716,   836,   718,
     717,   827,   720,   532,   722,   723,   724,   725,   849,   569,
     726,   727,   728,   729,   756,   533,   764,   733,   730,   743,
     745,   753,   746,   460,   373,   747,   757,   184,   771,   758,
     772,   189,   763,   777,   773,   780,   784,   781,   189,   782,
     837,   783,   786,   960,   785,   961,   184,   787,   796,   801,
     184,   802,   806,   807,   808,   809,   810,   811,   184,   184,
     814,   820,   821,   812,   813,   823,   815,   818,   819,   849,
     822,   824,   825,   826,   184,   184,   828,   838,   569,   839,
     840,   841,   846,   845,   863,   855,   857,   856,   858,   865,
     867,   869,   189,   294,   294,   908,   909,   189,   870,   874,
     873,   184,   875,   876,   877,   878,   879,   882,   881,   888,
     184,   883,   893,   895,   911,   128,   905,   902,   912,   184,
     913,   108,   184,   914,   460,   915,   916,   917,   918,   919,
     189,   921,  -353,   922,   923,   934,   929,   935,   930,   956,
     925,   926,   941,   569,   927,   184,   928,   932,   947,   184,
     946,   189,   948,   949,   924,   184,   955,   969,   971,   978,
     950,   184,   184,   184,   945,   983,   979,   184,   980,   904,
     981,   984,   985,   986,   184,   184,   996,   951,   989,   942,
     184,   952,   953,   999,   970,   197,   346,   556,   184,   134,
     198,   308,   998,   184,   184,   184,   632,   184,   348,   739,
     832,   656,   653,   472,   623,   386,   184,   387,   626,   388,
     620,   184,   964,   868,   997,   767,   383,   513,   694,   637,
     184,   994,   995,   668,   309,   421,   931,   910,   184,   667,
     184,   451,   184,   184,     0,   184,     0,     0,     0,     0,
       0,   186,     0,     0,   184,     0,     0,   184,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     186,   184,     0,   184,   186,     0,     0,     0,     0,     0,
       0,     0,   186,   186,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   186,   186,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   184,     0,     0,     0,     0,     0,
     184,   184,     0,     0,     0,   186,     0,     0,   184,     0,
       0,     0,     0,     0,   186,     0,     0,     0,     0,     0,
       0,     0,     0,   186,     0,     0,   186,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   186,
       0,     0,     0,   186,     0,     0,     0,     0,     0,   186,
       0,     0,     0,     0,     0,   186,   186,   186,     0,     0,
       0,   186,     0,     0,     0,     0,     0,     0,   186,   186,
       0,     0,   184,     0,   186,     0,     0,     0,     0,     0,
       0,   184,   186,   184,     0,     0,     0,   186,   186,   186,
       0,   186,     0,     0,     0,     0,     0,     0,     0,     0,
     186,   184,     0,     0,   451,   186,     0,     0,   184,     0,
       0,     0,     0,     0,   186,     0,     0,     0,     0,     0,
       0,     0,   186,     0,   186,   452,   186,   186,     0,   186,
       0,     0,     0,     0,     0,     0,     0,     0,   186,     0,
       0,   186,     0,     0,   184,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   186,     0,   186,     0,     0,
       0,     0,   174,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   231,     0,     0,     0,   174,     0,     0,     0,     0,
       0,     0,     0,     0,   249,     0,     0,     0,   186,     0,
       0,     0,     0,     0,   186,   186,     0,     0,     0,   451,
       0,   184,   186,     0,   184,     0,     0,     0,     0,     0,
       0,     0,     0,   184,   184,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   291,     0,     0,     0,
       0,     0,     0,     0,     0,   300,     0,     0,     0,     0,
       0,     0,     0,   184,   312,     0,     0,   317,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     317,     0,     0,     0,     0,     0,   186,     0,     0,     0,
       0,     0,     0,     0,     0,   186,   356,   186,   358,     0,
       0,     0,   361,     0,     0,     0,     0,     0,     0,   174,
       0,     0,     0,     0,     0,   186,     0,     0,   452,     0,
       0,     0,   186,     0,     0,   184,   184,     0,     0,     0,
     393,     0,   396,     0,     0,     0,     0,     0,     0,     0,
     184,   231,     0,     0,     0,     0,   300,     0,     0,     0,
       0,     0,   184,     0,   184,   312,     0,     0,   186,     0,
       0,     0,     0,     0,     0,   231,     0,   317,   317,     0,
     478,     0,     0,     0,     0,     0,     0,     0,     0,   490,
       0,     0,   498,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   510,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   451,     0,
       0,     0,     0,   452,     0,   186,   184,     0,   186,     0,
       0,     0,     0,   184,     0,     0,     0,   186,   186,   549,
       0,     0,     0,     0,     0,   291,   557,     0,     0,     0,
       0,     0,     0,   300,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   186,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   184,     0,     0,
       0,     0,   184,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   451,
       0,     0,     0,     0,     0,   184,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   625,     0,   627,   186,
     186,     0,     0,     0,     0,     0,   184,     0,     0,     0,
       0,     0,     0,     0,   186,     0,   635,     0,     0,     0,
       0,     0,     0,   639,     0,     0,   186,     0,   186,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   452,     0,     0,     0,     0,     0,     0,     0,
     186,     0,     0,     0,     0,     0,     0,   186,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   732,     0,     0,   734,
       0,     0,     0,     0,     0,     0,     0,     0,   740,   741,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   186,     0,     0,     0,     0,   186,     0,   317,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   452,     0,     0,     0,     0,     0,   186,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     186,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      26,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     829,   830,     0,     0,     0,     0,    27,   266,    28,    29,
       0,    30,    31,    32,     0,   317,     0,     0,    33,     0,
      34,    35,    36,   267,    37,    38,   268,   317,     0,   317,
       0,     0,     0,    39,    40,    41,     0,    42,     0,     0,
       0,     0,    43,    44,     0,     0,     0,    45,    46,     0,
      47,     0,     0,     0,     0,     0,     0,    48,     0,     0,
      49,     0,     0,     0,    50,     0,    51,     0,    52,     0,
       0,     0,    53,     0,     0,    54,     0,     0,     0,    55,
      56,     0,     0,    57,    58,    59,    60,     0,    61,    62,
      63,     0,    64,    65,     0,     0,    66,    67,    68,    69,
      70,   317,    71,     0,     0,    72,     0,     0,   901,    73,
      74,    75,     0,    76,    77,    78,    79,    80,     0,    81,
       0,    82,    83,    84,     0,     0,     0,    85,     0,     0,
      86,    87,    88,    89,     0,     0,    90,    91,    92,     0,
      93,     0,     0,     0,     0,    94,    95,    96,    97,     0,
       0,     0,    98,    99,   100,     0,     0,     0,    26,   141,
     142,   143,   144,   145,   101,   146,   147,     0,   310,     0,
       0,     0,     0,     0,    27,     0,    28,    29,     0,    30,
      31,    32,     0,   102,     0,   149,   150,     0,    34,    35,
      36,     0,   151,    38,     0,     0,   269,     0,     0,     0,
     901,   152,    40,    41,     0,    42,     0,   153,   154,   155,
      43,    44,     0,   156,     0,    45,    46,     0,    47,     0,
       0,   901,     0,     0,     0,    48,   211,     0,    49,     0,
       0,     0,    50,     0,    51,     0,    52,     0,     0,     0,
      53,     0,     0,    54,     0,     0,     0,    55,    56,   157,
       0,    57,    58,    59,    60,     0,    61,    62,    63,     0,
      64,    65,     0,   212,   158,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,   159,    81,     0,    82,
      83,    84,     5,   160,     0,    85,     0,     0,    86,    87,
      88,    89,   161,     0,    90,    91,   162,     0,    93,     0,
       0,     0,     0,    94,    95,    96,    97,     0,     0,     7,
      98,    99,   100,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   101,     0,     0,     0,     0,     0,     0,     0,
     213,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   102,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   164,   165,    26,   141,   142,   143,   144,   145,     0,
     146,   147,     0,   209,   210,     0,     0,     0,     0,    27,
       0,    28,    29,     0,    30,    31,    32,     0,     0,     0,
     149,   150,     0,    34,    35,    36,     0,   151,    38,     0,
       0,     0,     0,     0,     0,     0,   152,    40,    41,     0,
      42,     0,   153,   154,   155,    43,    44,     0,   156,     0,
      45,    46,     0,    47,     0,     0,     0,     0,     0,     0,
      48,   211,     0,    49,     0,     0,     0,    50,     0,    51,
       0,    52,     0,     0,     0,    53,     0,     0,    54,     0,
       0,     0,    55,    56,   157,     0,    57,    58,    59,    60,
       0,    61,    62,    63,     0,    64,    65,     0,   212,   158,
      67,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,    78,    79,
      80,   159,    81,     0,    82,    83,    84,     0,   160,     0,
      85,     0,     0,    86,    87,    88,    89,   161,     0,    90,
      91,   162,     0,    93,     0,     0,     0,     0,    94,    95,
      96,    97,     0,     0,     0,    98,    99,   100,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   101,     0,     0,
       0,     0,     0,     0,     0,   213,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   102,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   164,   165,    26,   141,
     142,   143,   144,   145,     0,   146,   147,     0,   209,   406,
       0,     0,     0,     0,    27,     0,    28,    29,     0,    30,
      31,    32,     0,     0,     0,   149,   150,     0,    34,    35,
      36,     0,   151,    38,     0,     0,     0,     0,     0,     0,
       0,   152,    40,    41,     0,    42,     0,   153,   154,   155,
      43,    44,     0,   156,     0,    45,    46,     0,    47,     0,
       0,     0,     0,     0,     0,    48,   211,     0,    49,     0,
       0,     0,    50,     0,    51,     0,    52,     0,     0,     0,
      53,     0,     0,    54,     0,     0,     0,    55,    56,   157,
       0,    57,    58,    59,    60,     0,    61,    62,    63,     0,
      64,    65,     0,   212,   158,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,   159,    81,     0,    82,
      83,    84,     0,   160,     0,    85,     0,     0,    86,    87,
      88,    89,   161,     0,    90,    91,   162,     0,    93,     0,
       0,     0,     0,    94,    95,    96,    97,     0,     0,     0,
      98,    99,   100,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   101,     0,     0,     0,     0,     0,     0,     0,
     213,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   102,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   164,   165,    26,   141,   142,   143,   144,   145,     0,
     146,   147,     0,   209,     0,     0,     0,     0,     0,    27,
       0,    28,    29,     0,    30,    31,    32,     0,     0,     0,
     149,   150,     0,    34,    35,    36,     0,   151,    38,     0,
       0,     0,     0,     0,     0,     0,   152,    40,    41,     0,
      42,     0,   153,   154,   155,    43,    44,     0,   156,     0,
      45,    46,     0,    47,     0,     0,     0,     0,     0,     0,
      48,   211,     0,    49,     0,     0,     0,    50,     0,    51,
       0,    52,     0,     0,     0,    53,     0,     0,    54,     0,
       0,     0,    55,    56,   157,     0,    57,    58,    59,    60,
       0,    61,    62,    63,     0,    64,    65,     0,   212,   158,
      67,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,    78,    79,
      80,   159,    81,     0,    82,    83,    84,     0,   160,     0,
      85,     0,     0,    86,    87,    88,    89,   161,     0,    90,
      91,   162,     0,    93,     0,     0,     0,     0,    94,    95,
      96,    97,     0,     0,     0,    98,    99,   100,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   101,     0,     0,
       0,     0,     0,     0,     0,   213,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   102,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   164,   165,    26,   141,
     142,   143,   144,   145,     0,   146,   147,     0,   148,     0,
       0,   390,     0,     0,    27,     0,    28,    29,     0,    30,
      31,    32,     0,     0,     0,   149,   150,     0,    34,    35,
      36,     0,   151,    38,     0,     0,     0,     0,     0,     0,
       0,   152,    40,    41,     0,    42,     0,   153,   154,   155,
      43,    44,     0,   156,     0,    45,    46,   391,    47,     0,
       0,     0,     0,     0,     0,    48,     0,     0,    49,     0,
       0,     0,    50,     0,    51,     0,    52,     0,     0,     0,
      53,     0,     0,    54,     0,     0,     0,    55,    56,   157,
       0,    57,    58,    59,    60,     0,    61,    62,    63,     0,
      64,    65,     0,     0,   158,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,   159,    81,     0,    82,
      83,    84,     0,   160,     0,    85,     0,     0,    86,    87,
      88,    89,   161,     0,    90,    91,   162,     0,    93,     0,
       0,     0,     0,    94,    95,    96,    97,     0,     0,     0,
      98,    99,   100,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   101,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   102,     0,     0,     0,     0,     0,     0,     0,   392,
       0,   164,   165,    26,   141,   142,   143,   144,   145,     0,
     146,   147,     0,   148,     0,     0,   495,     0,   496,    27,
       0,    28,    29,     0,    30,    31,    32,     0,     0,     0,
     149,   150,     0,    34,    35,    36,     0,   151,    38,     0,
       0,     0,     0,     0,     0,     0,   152,    40,    41,     0,
      42,     0,   153,   154,   155,    43,    44,     0,   156,     0,
      45,    46,     0,    47,     0,     0,     0,     0,     0,     0,
      48,     0,     0,    49,     0,     0,     0,    50,     0,    51,
       0,    52,     0,     0,     0,    53,     0,     0,    54,     0,
       0,     0,    55,    56,   157,     0,    57,    58,    59,    60,
       0,    61,    62,    63,     0,    64,    65,     0,     0,   158,
      67,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,    78,    79,
      80,   159,    81,     0,    82,    83,    84,     0,   160,     0,
      85,     0,   497,    86,    87,    88,    89,   161,     0,    90,
      91,   162,     0,    93,     0,     0,     0,     0,    94,    95,
      96,    97,     0,     0,     0,    98,    99,   100,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   101,     0,     0,
       0,    26,   141,   142,   143,   144,   145,     0,   146,   147,
       0,   245,   933,     0,     0,     0,   102,    27,     0,    28,
      29,     0,    30,    31,    32,     0,   164,   165,   149,   150,
       0,    34,    35,    36,     0,   151,    38,     0,     0,     0,
       0,     0,     0,     0,   152,    40,    41,     0,    42,     0,
     153,   154,   155,    43,    44,     0,   156,     0,    45,    46,
       0,    47,     0,     0,     0,     0,     0,     0,    48,     0,
       0,    49,     0,     0,     0,    50,     0,    51,     0,    52,
       0,     0,     0,    53,     0,     0,    54,     0,     0,     0,
      55,    56,   157,     0,    57,    58,    59,    60,     0,    61,
      62,    63,     0,    64,    65,     0,     0,   158,    67,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,    78,    79,    80,   159,
      81,     0,    82,    83,    84,     5,   160,     0,    85,     0,
       0,    86,    87,    88,    89,   161,     0,    90,    91,   162,
       0,    93,     0,     0,     0,     0,    94,    95,    96,    97,
       0,     0,     7,    98,    99,   100,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   101,     0,     0,     0,    26,
     141,   142,   143,   144,   145,     0,   146,   147,     0,   245,
       0,     0,     0,     0,   102,    27,     0,    28,    29,     0,
      30,    31,    32,     0,   164,   165,   149,   150,     0,    34,
      35,    36,     0,   151,    38,     0,     0,     0,     0,     0,
       0,     0,   152,    40,    41,     0,    42,     0,   153,   154,
     155,    43,    44,     0,   156,     0,    45,    46,     0,    47,
       0,     0,     0,     0,     0,     0,    48,     0,     0,    49,
       0,     0,     0,    50,     0,    51,     0,    52,     0,     0,
       0,    53,     0,     0,    54,     0,     0,     0,    55,    56,
     157,     0,    57,    58,    59,    60,     0,    61,    62,    63,
       0,    64,    65,     0,     0,   158,    67,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,    78,    79,    80,   159,    81,     0,
      82,    83,    84,     5,   160,     0,    85,     0,     0,    86,
      87,    88,    89,   161,     0,    90,    91,   162,     0,    93,
       0,     0,     0,     0,    94,    95,    96,    97,     0,     0,
       7,    98,    99,   100,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   101,     0,     0,     0,    26,   141,   142,
     143,   144,   145,     0,   146,   147,     0,   148,     0,     0,
       0,     0,   102,    27,     0,    28,    29,     0,    30,    31,
      32,     0,   164,   165,   149,   150,     0,    34,    35,    36,
       0,   151,    38,     0,     0,     0,     0,     0,     0,     0,
     152,    40,    41,     0,    42,     0,   153,   154,   155,    43,
      44,     0,   156,     0,    45,    46,     0,    47,     0,     0,
       0,     0,     0,     0,    48,     0,     0,    49,     0,     0,
       0,    50,     0,    51,     0,    52,     0,     0,     0,    53,
       0,     0,    54,     0,     0,     0,    55,    56,   157,     0,
      57,    58,    59,    60,     0,    61,    62,    63,     0,    64,
      65,     0,     0,   158,    67,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,    78,    79,    80,   159,    81,     0,    82,    83,
      84,     0,   160,     0,    85,     0,     0,    86,    87,    88,
      89,   161,     0,    90,    91,   162,     0,    93,     0,     0,
       0,     0,    94,    95,    96,    97,     0,     0,     0,    98,
      99,   100,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   101,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     102,     0,     0,     0,     0,     0,     0,     0,   163,     0,
     164,   165,    26,   141,   142,   143,   144,   145,     0,   146,
     147,     0,   148,   381,     0,     0,     0,     0,    27,     0,
      28,    29,     0,    30,    31,    32,     0,     0,     0,   149,
     150,     0,    34,    35,    36,     0,   151,    38,     0,     0,
       0,     0,     0,     0,     0,   152,    40,    41,     0,    42,
       0,   153,   154,   155,    43,    44,     0,   156,     0,    45,
      46,     0,    47,     0,     0,     0,     0,     0,     0,    48,
       0,     0,    49,     0,     0,     0,    50,     0,    51,     0,
      52,     0,     0,     0,    53,     0,     0,    54,     0,     0,
       0,    55,    56,   157,     0,    57,    58,    59,    60,     0,
      61,    62,    63,     0,    64,    65,     0,     0,   158,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
     159,    81,     0,    82,    83,    84,     0,   160,     0,    85,
       0,     0,    86,    87,    88,    89,   161,     0,    90,    91,
     162,     0,    93,     0,     0,     0,     0,    94,    95,    96,
      97,     0,     0,     0,    98,    99,   100,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   101,     0,     0,     0,
      26,   141,   142,   143,   144,   145,     0,   146,   147,     0,
     148,     0,     0,     0,     0,   102,    27,     0,    28,    29,
       0,    30,    31,    32,     0,   164,   165,   149,   150,     0,
      34,    35,    36,     0,   151,    38,     0,     0,     0,     0,
       0,     0,     0,   152,    40,    41,     0,    42,     0,   153,
     154,   155,    43,    44,     0,   156,     0,    45,    46,     0,
      47,     0,     0,     0,     0,     0,     0,    48,     0,     0,
      49,     0,     0,     0,    50,     0,    51,     0,    52,     0,
       0,     0,    53,     0,     0,    54,     0,     0,     0,    55,
      56,   157,     0,    57,    58,    59,    60,     0,    61,    62,
      63,     0,    64,    65,     0,     0,   158,    67,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,    78,    79,    80,   159,    81,
       0,    82,    83,    84,     0,   160,     0,    85,     0,     0,
      86,    87,    88,    89,   161,     0,    90,    91,   162,     0,
      93,     0,     0,     0,     0,    94,    95,    96,    97,     0,
       0,     0,    98,    99,   100,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   101,     0,     0,     0,    26,   141,
     142,   143,   144,   145,     0,   146,   147,     0,   897,     0,
       0,     0,     0,   102,    27,     0,    28,    29,     0,    30,
      31,    32,     0,   164,   165,   149,   150,     0,    34,    35,
      36,     0,   151,    38,     0,     0,     0,     0,     0,     0,
       0,   152,    40,    41,     0,    42,     0,   153,   154,   155,
      43,    44,     0,   156,     0,    45,    46,     0,    47,     0,
       0,     0,     0,     0,     0,    48,     0,     0,    49,     0,
       0,     0,    50,     0,   898,     0,    52,     0,     0,     0,
      53,     0,     0,    54,     0,     0,     0,    55,    56,   157,
       0,    57,    58,    59,    60,     0,    61,    62,    63,     0,
      64,    65,     0,     0,   158,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,   159,    81,     0,    82,
      83,    84,     0,   160,     0,    85,     0,     0,    86,    87,
      88,    89,   161,     0,    90,    91,   162,     0,    93,     0,
       0,     0,    26,    94,    95,    96,    97,     0,     0,     0,
      98,    99,   100,     0,   233,     0,     0,     0,    27,     0,
      28,    29,   101,    30,    31,    32,     0,     0,     0,     0,
      33,     0,    34,    35,    36,     0,    37,    38,     0,     0,
       0,   102,     0,     0,     0,    39,    40,    41,     0,    42,
       0,   164,   165,     0,    43,    44,     0,     0,     0,    45,
      46,     0,    47,     0,     0,     0,     0,     0,     0,    48,
       0,     0,    49,     0,     0,     0,    50,     0,    51,     0,
      52,     0,     0,     0,    53,     0,     0,    54,     0,     0,
       0,    55,    56,     0,     0,    57,    58,    59,    60,     0,
      61,    62,    63,     0,    64,    65,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
       0,     0,    86,    87,    88,    89,     0,     0,    90,    91,
      92,     0,    93,     0,     0,     0,     0,    94,    95,    96,
      97,     0,    26,     0,    98,    99,   100,     0,     0,     0,
       0,     0,     0,     0,   504,     0,   101,     0,    27,     0,
      28,    29,     0,    30,    31,    32,     0,     0,     0,     0,
      33,     0,    34,    35,    36,   102,    37,    38,     0,     0,
       0,     0,     0,   397,     0,    39,    40,    41,     0,    42,
       0,     0,     0,     0,    43,    44,     0,     0,     0,    45,
      46,     0,    47,     0,     0,     0,     0,     0,     0,    48,
       0,     0,    49,     0,     0,     0,    50,     0,    51,     0,
      52,     0,     0,     0,    53,     0,     0,    54,     0,     0,
       0,    55,    56,     0,     0,    57,    58,    59,    60,     0,
      61,    62,    63,     0,    64,    65,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
       0,     0,    86,    87,    88,    89,     0,     0,    90,    91,
      92,     0,    93,     0,     0,     0,     0,    94,    95,    96,
      97,     0,    26,     0,    98,    99,   100,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   101,     0,    27,     0,
      28,    29,     0,    30,    31,    32,     0,     0,     0,     0,
      33,     0,    34,    35,    36,   102,    37,    38,     0,     0,
       0,     0,     0,   676,     0,    39,    40,    41,     0,    42,
       0,     0,     0,     0,    43,    44,     0,     0,     0,    45,
      46,     0,    47,     0,     0,     0,     0,     0,     0,    48,
       0,     0,    49,     0,     0,     0,    50,     0,    51,     0,
      52,     0,     0,     0,    53,     0,     0,    54,     0,     0,
       0,    55,    56,     0,     0,    57,    58,    59,    60,     0,
      61,    62,    63,     0,    64,    65,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
       0,     0,    86,    87,    88,    89,     0,     0,    90,    91,
      92,     0,    93,     0,     0,     0,     0,    94,    95,    96,
      97,     0,    26,     0,    98,    99,   100,     0,     0,     0,
       0,     0,   519,     0,     0,     0,   101,     0,    27,     0,
      28,    29,     0,    30,    31,    32,     0,     0,     0,     0,
      33,     0,    34,    35,    36,   102,    37,    38,     0,     0,
       0,     0,     0,   848,     0,    39,    40,    41,     0,    42,
       0,     0,     0,     0,    43,    44,     0,     0,     0,    45,
      46,     0,    47,     0,     0,     0,     0,     0,     0,    48,
       0,     0,    49,     0,     0,     0,    50,     0,    51,     0,
      52,     0,     0,     0,    53,     0,     0,    54,     0,     0,
       0,    55,    56,     0,     0,    57,    58,    59,    60,     0,
      61,    62,    63,     0,    64,    65,     0,     0,    66,    67,
      68,    69,    70,     0,   364,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     5,     0,     0,    85,
       0,     0,    86,    87,    88,    89,     0,     0,    90,    91,
      92,     0,    93,     0,     0,     0,   425,    94,    95,    96,
      97,     0,     0,     7,    98,    99,   100,     0,     0,     0,
       0,     0,     0,     0,    28,    29,   101,   426,   427,   428,
     429,     0,     0,     0,    33,   430,   431,     0,   432,     0,
      37,    38,     0,   365,   366,   102,     0,     0,     0,    39,
      40,    41,     0,    42,     0,     0,     0,   433,    43,   434,
     435,     0,     0,    45,    46,     0,   436,     0,     0,     0,
       0,     0,     0,    48,     0,   437,    49,     0,     0,     0,
      50,     0,    51,     0,    52,     0,     0,     0,   438,   439,
       0,   440,     0,     0,     0,    55,    56,     0,     0,    57,
      58,    59,    60,     0,    61,    62,   441,     0,   442,   443,
       0,     0,    66,   444,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,   445,    79,   446,     0,    81,     0,    82,    83,    84,
       0,     0,     0,    85,   447,     0,    86,    87,    88,    89,
       0,     0,    90,   448,   449,     0,    93,     0,     0,     0,
       0,   450,     0,    96,    97,     0,    26,     0,    98,    99,
     100,     0,     0,     0,     0,     0,     1,     0,     0,     0,
     101,     0,    27,     0,    28,    29,     0,    30,    31,    32,
       0,     0,     0,     0,    33,     0,    34,    35,    36,   102,
      37,    38,     0,     0,     0,     0,     0,     0,     0,    39,
      40,    41,     0,    42,     0,     0,     0,     0,    43,    44,
       0,     0,     0,    45,    46,     0,    47,     0,     0,     0,
       0,     0,     0,    48,     0,     0,    49,     0,     0,     0,
      50,     0,    51,     0,    52,     0,     0,     0,    53,     0,
       0,    54,     0,     0,     0,    55,    56,     0,     0,    57,
      58,    59,    60,     0,    61,    62,    63,     0,    64,    65,
       0,     0,    66,    67,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,    78,    79,    80,     0,    81,     0,    82,    83,    84,
       5,     0,     0,    85,     0,     0,    86,    87,    88,    89,
       0,     0,    90,    91,    92,     0,    93,     0,     0,     0,
       0,    94,    95,    96,    97,     0,    26,     7,    98,    99,
     100,     0,     0,     0,     0,     0,   363,     0,     0,     0,
     101,     0,    27,     0,    28,    29,     0,    30,    31,    32,
       0,     0,     0,     0,    33,     0,    34,    35,    36,   102,
      37,    38,     0,     0,     0,     0,     0,     0,     0,    39,
      40,    41,     0,    42,     0,     0,     0,     0,    43,    44,
       0,     0,     0,    45,    46,     0,    47,     0,     0,     0,
       0,     0,     0,    48,     0,     0,    49,     0,     0,     0,
      50,     0,    51,     0,    52,     0,     0,     0,    53,     0,
       0,    54,     0,     0,     0,    55,    56,     0,     0,    57,
      58,    59,    60,     0,    61,    62,    63,     0,    64,    65,
       0,     0,    66,    67,    68,    69,    70,     0,   364,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,    78,    79,    80,     0,    81,     0,    82,    83,    84,
       0,     0,     0,    85,     0,     0,    86,    87,    88,    89,
       0,     0,    90,    91,    92,     0,    93,     0,     0,     0,
       0,    94,    95,    96,    97,     0,     0,     0,    98,    99,
     100,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     101,     0,     0,     0,    26,     0,     0,     0,     0,     0,
       0,   850,   851,     0,     0,     0,     0,   365,   366,   102,
      27,     0,    28,    29,     0,    30,    31,    32,     0,     0,
       0,     0,    33,     0,    34,    35,    36,     0,    37,    38,
       0,     0,     0,     0,     0,     0,     0,    39,    40,    41,
       0,    42,     0,     0,     0,     0,    43,    44,     0,     0,
       0,    45,    46,     0,    47,     0,     0,     0,     0,     0,
       0,    48,     0,     0,    49,     0,     0,     0,    50,     0,
      51,     0,    52,     0,     0,     0,    53,     0,     0,    54,
       0,     0,     0,    55,    56,     0,     0,    57,    58,    59,
      60,     0,    61,    62,    63,     0,    64,    65,     0,     0,
      66,    67,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,    78,
      79,    80,     0,    81,     0,    82,    83,    84,     0,     0,
       0,    85,     0,     0,    86,    87,    88,    89,     0,     0,
      90,    91,    92,     0,    93,     0,     0,     0,    26,    94,
      95,    96,    97,     0,     0,     0,    98,    99,   100,     0,
     233,     0,     0,     0,    27,     0,    28,    29,   101,    30,
      31,    32,     0,     0,     0,     0,    33,     0,    34,    35,
      36,     0,    37,    38,     0,     0,     0,   102,     0,     0,
       0,    39,    40,    41,     0,    42,     0,     0,     0,     0,
      43,    44,     0,     0,     0,    45,    46,     0,    47,     0,
       0,     0,     0,     0,     0,    48,     0,     0,    49,     0,
       0,     0,    50,     0,    51,     0,    52,     0,     0,     0,
      53,     0,     0,    54,     0,     0,     0,    55,    56,     0,
       0,    57,    58,    59,    60,     0,    61,    62,    63,     0,
      64,    65,     0,     0,    66,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,     0,    81,     0,    82,
      83,    84,     0,     0,     0,    85,     0,     0,    86,    87,
      88,    89,     0,     0,    90,    91,    92,     0,    93,     0,
       0,     0,    26,    94,    95,    96,    97,     0,     0,     0,
      98,    99,   100,     0,   340,     0,     0,     0,    27,     0,
      28,    29,   101,    30,    31,    32,     0,     0,     0,     0,
      33,     0,    34,    35,    36,     0,    37,    38,     0,     0,
       0,   102,     0,     0,     0,    39,    40,    41,     0,    42,
       0,     0,     0,     0,    43,    44,     0,     0,     0,    45,
      46,     0,    47,     0,     0,     0,     0,     0,     0,    48,
       0,     0,    49,     0,     0,     0,    50,     0,    51,     0,
      52,     0,     0,     0,    53,     0,     0,    54,     0,     0,
       0,    55,    56,     0,     0,    57,    58,    59,    60,     0,
      61,    62,    63,     0,    64,    65,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
       0,     0,    86,    87,    88,    89,     0,     0,    90,    91,
      92,     0,    93,     0,     0,     0,    26,    94,    95,    96,
      97,     0,     0,     0,    98,    99,   100,     0,   504,     0,
       0,     0,    27,     0,    28,    29,   101,    30,    31,    32,
       0,     0,     0,     0,    33,     0,    34,    35,    36,     0,
      37,    38,     0,     0,     0,   102,     0,     0,     0,    39,
      40,    41,     0,    42,     0,     0,     0,     0,    43,    44,
       0,     0,     0,    45,    46,     0,    47,     0,     0,     0,
       0,     0,     0,    48,     0,     0,    49,     0,     0,     0,
      50,     0,    51,     0,    52,     0,     0,     0,    53,     0,
       0,    54,     0,     0,     0,    55,    56,     0,     0,    57,
      58,    59,    60,     0,    61,    62,    63,     0,    64,    65,
       0,     0,    66,    67,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,    78,    79,    80,     0,    81,     0,    82,    83,    84,
       0,     0,     0,    85,     0,     0,    86,    87,    88,    89,
       0,     0,    90,    91,    92,     0,    93,     0,     0,     0,
      26,    94,    95,    96,    97,     0,     0,     0,    98,    99,
     100,     0,     0,     0,     0,     0,    27,   266,    28,    29,
     101,    30,    31,    32,     0,     0,     0,     0,    33,     0,
      34,    35,    36,     0,    37,    38,     0,     0,     0,   102,
       0,     0,     0,    39,    40,    41,     0,    42,     0,     0,
       0,     0,    43,    44,     0,     0,     0,    45,    46,     0,
      47,     0,     0,     0,     0,     0,     0,    48,     0,     0,
      49,     0,     0,     0,    50,     0,    51,     0,    52,     0,
       0,     0,    53,     0,     0,    54,     0,     0,     0,    55,
      56,     0,     0,    57,    58,    59,    60,     0,    61,    62,
      63,     0,    64,    65,     0,     0,    66,    67,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,    78,    79,    80,     0,    81,
       0,    82,    83,    84,     0,     0,     0,    85,     0,     0,
      86,    87,    88,    89,     0,     0,    90,    91,    92,     0,
      93,     0,     0,     0,    26,    94,    95,    96,    97,     0,
       0,     0,    98,    99,   100,     0,     0,     0,     0,     0,
      27,   266,    28,    29,   101,    30,    31,    32,     0,     0,
       0,     0,    33,     0,    34,    35,    36,     0,    37,    38,
       0,     0,     0,   102,     0,     0,     0,    39,    40,    41,
       0,    42,     0,     0,     0,     0,    43,    44,     0,     0,
       0,    45,    46,     0,    47,     0,     0,     0,     0,     0,
       0,    48,     0,     0,    49,     0,     0,     0,    50,     0,
      51,     0,    52,     0,     0,     0,    53,     0,     0,    54,
       0,     0,     0,    55,    56,     0,     0,    57,    58,    59,
      60,     0,    61,    62,    63,     0,    64,    65,     0,     0,
      66,    67,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,    78,
      79,    80,     0,    81,     0,    82,    83,    84,     0,     0,
       0,    85,     0,     0,    86,    87,    88,    89,     0,     0,
      90,    91,    92,     0,    93,     0,     0,     0,     0,    94,
      95,    96,    97,     0,    26,     0,    98,    99,   100,     0,
       0,     0,     0,     0,   672,     0,     0,     0,   101,     0,
      27,     0,    28,    29,     0,    30,    31,    32,     0,     0,
       0,     0,    33,     0,    34,    35,    36,   537,    37,    38,
       0,     0,     0,     0,     0,     0,     0,    39,    40,    41,
       0,    42,     0,     0,     0,     0,    43,    44,     0,     0,
       0,    45,    46,     0,    47,     0,     0,     0,     0,     0,
       0,    48,     0,     0,    49,     0,     0,     0,    50,     0,
      51,     0,    52,     0,     0,     0,    53,     0,     0,    54,
       0,     0,     0,    55,    56,     0,     0,    57,    58,    59,
      60,     0,    61,    62,    63,     0,    64,    65,     0,     0,
      66,    67,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,    78,
      79,    80,     0,    81,     0,    82,    83,    84,     0,     0,
       0,    85,     0,     0,    86,    87,    88,    89,     0,     0,
      90,    91,    92,     0,    93,     0,     0,     0,    26,    94,
      95,    96,    97,     0,     0,     0,    98,    99,   100,     0,
       0,     0,     0,     0,    27,     0,    28,    29,   101,    30,
      31,    32,     0,     0,     0,     0,    33,     0,    34,    35,
      36,     0,    37,    38,     0,     0,     0,   102,     0,     0,
       0,    39,    40,    41,     0,    42,     0,     0,     0,     0,
      43,    44,     0,     0,     0,    45,    46,     0,    47,     0,
       0,     0,     0,     0,     0,    48,     0,     0,    49,     0,
       0,     0,    50,     0,    51,     0,    52,     0,     0,     0,
      53,     0,     0,    54,     0,     0,     0,    55,    56,     0,
       0,    57,    58,    59,    60,     0,    61,    62,    63,     0,
      64,    65,     0,     0,    66,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,     0,    81,     0,    82,
      83,    84,     0,     0,     0,    85,     0,     0,    86,    87,
      88,    89,     0,     0,    90,    91,    92,     0,    93,     0,
       0,     0,    26,    94,    95,    96,    97,     0,     0,     0,
      98,    99,   100,     0,     0,     0,     0,     0,    27,     0,
      28,    29,   101,    30,    31,    32,     0,     0,     0,     0,
      33,     0,    34,    35,    36,     0,    37,    38,     0,     0,
       0,   102,     0,     0,     0,    39,    40,    41,     0,    42,
       0,     0,     0,     0,    43,    44,     0,     0,     0,    45,
      46,     0,    47,     0,     0,     0,     0,     0,     0,    48,
       0,     0,    49,     0,     0,     0,    50,     0,    51,     0,
      52,     0,     0,     0,    53,     0,     0,    54,     0,     0,
       0,    55,    56,     0,     0,    57,    58,    59,    60,     0,
      61,    62,    63,     0,    64,    65,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,   119,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
       0,     0,    86,    87,    88,    89,     0,     0,    90,    91,
      92,     0,    93,   425,     0,     0,     0,    94,    95,    96,
      97,     0,     0,     0,    98,    99,   100,     0,     0,     0,
       0,    28,    29,     0,     0,     0,   101,     0,     0,     0,
       0,    33,     0,     0,     0,     0,     0,    37,    38,     0,
       0,     0,     0,     0,     0,   102,    39,    40,    41,     0,
      42,     0,     0,     0,     0,    43,     0,     0,     0,     0,
      45,    46,     0,     0,     0,     0,     0,     0,     0,     0,
      48,     0,     0,    49,     0,     0,     0,    50,     0,    51,
       0,    52,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    55,    56,     0,     0,    57,    58,    59,    60,
       0,    61,    62,     0,     0,     0,     0,     0,     0,    66,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    79,
      80,     0,    81,     0,    82,    83,    84,     0,     0,     0,
      85,     0,     0,    86,    87,    88,    89,     0,     0,    90,
       0,     0,     0,    93,     0,     0,     0,     0,    94,     0,
      96,    97,     0,     0,     0,    98,    99,   100,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   101,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   102
};

static const yytype_int16 yycheck[] =
{
       2,   134,   139,   321,     6,     7,   239,   116,   209,   619,
       0,     1,   403,   363,   466,   413,   468,    13,   127,   252,
     129,   130,   139,   604,   133,    17,    14,    14,    14,   363,
     211,   688,   265,     2,   235,   211,    17,     6,    20,    14,
      14,    14,    36,    36,    36,    39,    39,    39,    36,    36,
      36,    39,    39,    39,    36,    36,    14,    39,    39,    14,
      16,    36,    13,    28,    39,   367,   301,   100,     8,    13,
     372,   373,   117,    36,   547,   548,    39,    40,    29,   140,
     693,   263,    21,     8,   212,   318,   262,    14,    16,    89,
      36,   136,    34,    39,    29,   140,    38,    36,    36,    36,
      39,    39,    39,   148,   149,    19,    62,   289,    59,   310,
     112,   113,    69,    13,    75,   117,    13,   119,   351,   164,
     165,   123,    61,   296,    73,    36,   243,     0,    39,    67,
      67,    13,   940,    84,    62,   137,   126,    13,   140,     6,
      21,    91,     9,   112,   113,    13,   191,   320,    66,   139,
     958,   332,   353,    33,   123,   200,   332,   338,    13,    39,
     117,    13,   338,   156,   209,   778,   348,   212,   108,   519,
     112,   204,   174,   408,   123,   164,   165,   210,   211,   529,
      61,   989,    58,   840,   200,   519,   137,   363,   102,   116,
     235,   193,   194,   137,   239,   323,   514,   154,   671,    13,
     245,   197,   783,   264,   155,   786,   251,   252,   253,   203,
     175,    14,   257,   164,    13,   209,   209,   209,   828,   264,
     265,   209,   209,   209,   181,   270,   182,   209,   209,   203,
     203,   233,   813,   278,   209,   237,   186,   539,   283,   284,
     285,   108,   287,   183,   184,   203,   209,   237,   203,   482,
     201,   296,   152,   243,   119,   245,   301,   203,   183,   184,
     262,   137,   264,   209,   266,   310,   268,   164,   165,   137,
     209,   209,   209,   318,    87,   320,   321,   322,   323,    93,
     325,   157,   164,   165,    13,   137,   288,   404,   164,   334,
     688,   682,   337,   262,    93,   284,   164,   878,   209,   268,
      73,   619,   206,   207,   159,   157,   351,   657,   353,    13,
     115,   492,   164,    23,   132,   133,   492,    15,    14,   929,
     310,   554,    55,    19,    23,   116,    36,    13,   193,    39,
      40,    41,    42,    43,    44,    45,   125,   339,   340,    13,
     342,    15,    49,   519,   133,   159,   646,   647,    81,   394,
     123,    51,   652,   529,   654,   400,   401,   148,   534,   164,
     159,   363,   180,   408,    93,   367,    99,   139,   101,    76,
     372,   373,    82,   771,     6,   827,    83,     9,    78,    55,
      90,   935,   203,    82,    91,    95,     5,    94,   525,    93,
      74,    10,    11,   193,   363,   440,    95,   104,   108,   199,
      23,   403,   956,   136,    13,    81,   102,   107,    13,   163,
      15,   413,   119,    36,   404,    13,    39,    40,    41,    42,
      43,    44,    45,    99,   131,   101,    40,    13,    42,    76,
     159,    14,    13,   166,    15,   199,    83,   482,   202,   203,
     174,    13,   840,   177,    91,    13,   491,    94,   493,   149,
      13,   530,    15,   753,   533,   159,    13,    34,    15,    82,
     136,    38,   663,     4,   855,   856,   511,    90,    13,   514,
     646,   647,    95,   518,    13,    13,   652,    15,   654,    13,
      13,   657,    15,     4,   131,   108,    69,    75,   191,   192,
     166,    74,   203,   203,   171,   172,   203,    13,   208,   209,
      77,   503,   504,   205,    87,   132,   133,   905,    13,   554,
     828,   177,   178,    20,   516,    92,    13,   519,    13,    96,
     522,   523,   524,    23,    13,   148,   155,   529,   176,   519,
      32,    33,   534,    13,   117,   112,    36,   539,    28,    39,
      40,    41,    42,    43,    44,    45,    13,   516,   550,   750,
     519,   553,    14,   522,   523,   524,    20,    49,   189,   190,
     529,   762,   203,   764,   116,   534,     4,     5,     6,     7,
       8,   154,    10,    11,   619,    17,   621,   753,    90,   624,
     164,   165,    82,    15,    76,   208,   209,   157,   633,   634,
      90,    83,    14,   176,    14,    95,   179,   162,   181,    91,
     164,   165,    94,     5,   606,   906,   907,    14,   108,    14,
     209,   929,   104,    51,   751,    14,    54,    14,   663,    13,
      58,   943,   944,   203,   203,   203,   628,   119,   113,   631,
     179,    40,    13,    15,    49,   836,    40,   606,    14,   131,
      64,    14,    14,    13,   646,   647,    13,    13,   120,    14,
     652,    14,   654,   168,   763,   657,     4,     5,     6,     7,
       8,    76,    10,    11,   709,   188,   202,    14,    83,   902,
     672,    14,   173,    13,    93,    23,    91,   646,   647,    94,
     682,    13,    13,   652,    13,   654,   688,   689,   657,   104,
     735,   736,    13,    49,   124,    13,    13,    13,    13,   114,
     837,    39,    15,    51,   119,   750,    54,    15,   208,   209,
      58,    65,    14,   151,   847,   153,   131,   762,   119,   764,
      76,   186,    91,   121,   164,    28,    78,    83,   730,   192,
     191,    14,   187,   202,    15,    91,     5,   203,    94,     5,
     169,   156,     5,   111,     5,    13,    13,   111,   104,   111,
       5,   753,     5,     5,     5,    13,   152,     5,   114,    13,
     111,   730,     5,   119,     5,   150,   150,     5,   770,   771,
     150,   150,     5,    13,     5,   131,    80,    14,   139,    14,
      14,    91,    14,   828,   753,    14,    13,   117,    89,    28,
     177,   836,    28,   170,   177,    13,     5,    14,   843,    14,
     156,    13,    13,   151,    14,   153,   136,     5,    14,    14,
     140,     5,     5,     5,    13,     5,   111,    13,   148,   149,
       5,   167,   167,    14,    13,   167,    14,    14,    14,   831,
      14,   167,    14,     5,   164,   165,    15,    14,   840,     5,
      13,   199,   121,    14,     5,    28,   139,    28,    15,    14,
      14,     5,   897,   855,   856,   857,   858,   902,     5,    14,
      13,   191,    14,     5,    14,    13,     5,     5,    14,    14,
     200,   135,   198,    14,   149,   117,   203,    28,    14,   209,
      14,   883,   212,    14,   929,    14,    14,     5,     5,    14,
     935,    14,   152,    14,    14,   140,    15,   203,    13,    13,
     150,   150,   904,   905,   150,   235,   150,    14,    14,   239,
      15,   956,    14,    14,   883,   245,     5,     4,    14,   132,
     167,   251,   252,   253,   152,   139,   123,   257,   123,    15,
      70,   139,    58,    14,   264,   265,     4,   167,    17,   905,
     270,   167,   167,   118,   946,   123,   243,   400,   278,    24,
     126,   207,   132,   283,   284,   285,   506,   287,   244,   632,
     739,   534,   531,   322,   486,   270,   296,   278,   492,   283,
     476,   301,   940,   795,   989,   672,   266,   355,   579,   514,
     310,   983,   984,   544,   208,   309,   895,   859,   318,   543,
     320,   321,   322,   323,    -1,   325,    -1,    -1,    -1,    -1,
      -1,   117,    -1,    -1,   334,    -1,    -1,   337,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     136,   351,    -1,   353,   140,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   148,   149,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   164,   165,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   394,    -1,    -1,    -1,    -1,    -1,
     400,   401,    -1,    -1,    -1,   191,    -1,    -1,   408,    -1,
      -1,    -1,    -1,    -1,   200,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   209,    -1,    -1,   212,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
      -1,    -1,    -1,   239,    -1,    -1,    -1,    -1,    -1,   245,
      -1,    -1,    -1,    -1,    -1,   251,   252,   253,    -1,    -1,
      -1,   257,    -1,    -1,    -1,    -1,    -1,    -1,   264,   265,
      -1,    -1,   482,    -1,   270,    -1,    -1,    -1,    -1,    -1,
      -1,   491,   278,   493,    -1,    -1,    -1,   283,   284,   285,
      -1,   287,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     296,   511,    -1,    -1,   514,   301,    -1,    -1,   518,    -1,
      -1,    -1,    -1,    -1,   310,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   318,    -1,   320,   321,   322,   323,    -1,   325,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   334,    -1,
      -1,   337,    -1,    -1,   554,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   351,    -1,   353,    -1,    -1,
      -1,    -1,   117,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   136,    -1,    -1,    -1,   140,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   149,    -1,    -1,    -1,   394,    -1,
      -1,    -1,    -1,    -1,   400,   401,    -1,    -1,    -1,   619,
      -1,   621,   408,    -1,   624,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   633,   634,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   191,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   200,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   663,   209,    -1,    -1,   212,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     235,    -1,    -1,    -1,    -1,    -1,   482,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   491,   251,   493,   253,    -1,
      -1,    -1,   257,    -1,    -1,    -1,    -1,    -1,    -1,   264,
      -1,    -1,    -1,    -1,    -1,   511,    -1,    -1,   514,    -1,
      -1,    -1,   518,    -1,    -1,   735,   736,    -1,    -1,    -1,
     285,    -1,   287,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     750,   296,    -1,    -1,    -1,    -1,   301,    -1,    -1,    -1,
      -1,    -1,   762,    -1,   764,   310,    -1,    -1,   554,    -1,
      -1,    -1,    -1,    -1,    -1,   320,    -1,   322,   323,    -1,
     325,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   334,
      -1,    -1,   337,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   353,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   828,    -1,
      -1,    -1,    -1,   619,    -1,   621,   836,    -1,   624,    -1,
      -1,    -1,    -1,   843,    -1,    -1,    -1,   633,   634,   394,
      -1,    -1,    -1,    -1,    -1,   400,   401,    -1,    -1,    -1,
      -1,    -1,    -1,   408,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   663,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   897,    -1,    -1,
      -1,    -1,   902,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   929,
      -1,    -1,    -1,    -1,    -1,   935,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   491,    -1,   493,   735,
     736,    -1,    -1,    -1,    -1,    -1,   956,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   750,    -1,   511,    -1,    -1,    -1,
      -1,    -1,    -1,   518,    -1,    -1,   762,    -1,   764,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   828,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     836,    -1,    -1,    -1,    -1,    -1,    -1,   843,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   621,    -1,    -1,   624,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   633,   634,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   897,    -1,    -1,    -1,    -1,   902,    -1,   663,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   929,    -1,    -1,    -1,    -1,    -1,   935,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     956,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     735,   736,    -1,    -1,    -1,    -1,    19,    20,    21,    22,
      -1,    24,    25,    26,    -1,   750,    -1,    -1,    31,    -1,
      33,    34,    35,    36,    37,    38,    39,   762,    -1,   764,
      -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,    -1,
      -1,    -1,    55,    56,    -1,    -1,    -1,    60,    61,    -1,
      63,    -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,
      73,    -1,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,
      -1,    -1,    85,    -1,    -1,    88,    -1,    -1,    -1,    92,
      93,    -1,    -1,    96,    97,    98,    99,    -1,   101,   102,
     103,    -1,   105,   106,    -1,    -1,   109,   110,   111,   112,
     113,   836,   115,    -1,    -1,   118,    -1,    -1,   843,   122,
     123,   124,    -1,   126,   127,   128,   129,   130,    -1,   132,
      -1,   134,   135,   136,    -1,    -1,    -1,   140,    -1,    -1,
     143,   144,   145,   146,    -1,    -1,   149,   150,   151,    -1,
     153,    -1,    -1,    -1,    -1,   158,   159,   160,   161,    -1,
      -1,    -1,   165,   166,   167,    -1,    -1,    -1,     3,     4,
       5,     6,     7,     8,   177,    10,    11,    -1,    13,    -1,
      -1,    -1,    -1,    -1,    19,    -1,    21,    22,    -1,    24,
      25,    26,    -1,   196,    -1,    30,    31,    -1,    33,    34,
      35,    -1,    37,    38,    -1,    -1,   209,    -1,    -1,    -1,
     935,    46,    47,    48,    -1,    50,    -1,    52,    53,    54,
      55,    56,    -1,    58,    -1,    60,    61,    -1,    63,    -1,
      -1,   956,    -1,    -1,    -1,    70,    71,    -1,    73,    -1,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    -1,    -1,    88,    -1,    -1,    -1,    92,    93,    94,
      -1,    96,    97,    98,    99,    -1,   101,   102,   103,    -1,
     105,   106,    -1,   108,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,   131,   132,    -1,   134,
     135,   136,   137,   138,    -1,   140,    -1,    -1,   143,   144,
     145,   146,   147,    -1,   149,   150,   151,    -1,   153,    -1,
      -1,    -1,    -1,   158,   159,   160,   161,    -1,    -1,   164,
     165,   166,   167,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     185,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   196,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   206,   207,     3,     4,     5,     6,     7,     8,    -1,
      10,    11,    -1,    13,    14,    -1,    -1,    -1,    -1,    19,
      -1,    21,    22,    -1,    24,    25,    26,    -1,    -1,    -1,
      30,    31,    -1,    33,    34,    35,    -1,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,
      50,    -1,    52,    53,    54,    55,    56,    -1,    58,    -1,
      60,    61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,
      70,    71,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,
      -1,    81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,
      -1,    -1,    92,    93,    94,    -1,    96,    97,    98,    99,
      -1,   101,   102,   103,    -1,   105,   106,    -1,   108,   109,
     110,   111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,
      -1,    -1,   122,   123,   124,    -1,   126,   127,   128,   129,
     130,   131,   132,    -1,   134,   135,   136,    -1,   138,    -1,
     140,    -1,    -1,   143,   144,   145,   146,   147,    -1,   149,
     150,   151,    -1,   153,    -1,    -1,    -1,    -1,   158,   159,
     160,   161,    -1,    -1,    -1,   165,   166,   167,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   185,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   196,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   206,   207,     3,     4,
       5,     6,     7,     8,    -1,    10,    11,    -1,    13,    14,
      -1,    -1,    -1,    -1,    19,    -1,    21,    22,    -1,    24,
      25,    26,    -1,    -1,    -1,    30,    31,    -1,    33,    34,
      35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    52,    53,    54,
      55,    56,    -1,    58,    -1,    60,    61,    -1,    63,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    71,    -1,    73,    -1,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    -1,    -1,    88,    -1,    -1,    -1,    92,    93,    94,
      -1,    96,    97,    98,    99,    -1,   101,   102,   103,    -1,
     105,   106,    -1,   108,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,   131,   132,    -1,   134,
     135,   136,    -1,   138,    -1,   140,    -1,    -1,   143,   144,
     145,   146,   147,    -1,   149,   150,   151,    -1,   153,    -1,
      -1,    -1,    -1,   158,   159,   160,   161,    -1,    -1,    -1,
     165,   166,   167,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     185,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   196,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   206,   207,     3,     4,     5,     6,     7,     8,    -1,
      10,    11,    -1,    13,    -1,    -1,    -1,    -1,    -1,    19,
      -1,    21,    22,    -1,    24,    25,    26,    -1,    -1,    -1,
      30,    31,    -1,    33,    34,    35,    -1,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,
      50,    -1,    52,    53,    54,    55,    56,    -1,    58,    -1,
      60,    61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,
      70,    71,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,
      -1,    81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,
      -1,    -1,    92,    93,    94,    -1,    96,    97,    98,    99,
      -1,   101,   102,   103,    -1,   105,   106,    -1,   108,   109,
     110,   111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,
      -1,    -1,   122,   123,   124,    -1,   126,   127,   128,   129,
     130,   131,   132,    -1,   134,   135,   136,    -1,   138,    -1,
     140,    -1,    -1,   143,   144,   145,   146,   147,    -1,   149,
     150,   151,    -1,   153,    -1,    -1,    -1,    -1,   158,   159,
     160,   161,    -1,    -1,    -1,   165,   166,   167,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   185,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   196,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   206,   207,     3,     4,
       5,     6,     7,     8,    -1,    10,    11,    -1,    13,    -1,
      -1,    16,    -1,    -1,    19,    -1,    21,    22,    -1,    24,
      25,    26,    -1,    -1,    -1,    30,    31,    -1,    33,    34,
      35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    52,    53,    54,
      55,    56,    -1,    58,    -1,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,    73,    -1,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    -1,    -1,    88,    -1,    -1,    -1,    92,    93,    94,
      -1,    96,    97,    98,    99,    -1,   101,   102,   103,    -1,
     105,   106,    -1,    -1,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,   131,   132,    -1,   134,
     135,   136,    -1,   138,    -1,   140,    -1,    -1,   143,   144,
     145,   146,   147,    -1,   149,   150,   151,    -1,   153,    -1,
      -1,    -1,    -1,   158,   159,   160,   161,    -1,    -1,    -1,
     165,   166,   167,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   196,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   204,
      -1,   206,   207,     3,     4,     5,     6,     7,     8,    -1,
      10,    11,    -1,    13,    -1,    -1,    16,    -1,    18,    19,
      -1,    21,    22,    -1,    24,    25,    26,    -1,    -1,    -1,
      30,    31,    -1,    33,    34,    35,    -1,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,
      50,    -1,    52,    53,    54,    55,    56,    -1,    58,    -1,
      60,    61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,
      70,    -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,
      -1,    81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,
      -1,    -1,    92,    93,    94,    -1,    96,    97,    98,    99,
      -1,   101,   102,   103,    -1,   105,   106,    -1,    -1,   109,
     110,   111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,
      -1,    -1,   122,   123,   124,    -1,   126,   127,   128,   129,
     130,   131,   132,    -1,   134,   135,   136,    -1,   138,    -1,
     140,    -1,   142,   143,   144,   145,   146,   147,    -1,   149,
     150,   151,    -1,   153,    -1,    -1,    -1,    -1,   158,   159,
     160,   161,    -1,    -1,    -1,   165,   166,   167,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,
      -1,     3,     4,     5,     6,     7,     8,    -1,    10,    11,
      -1,    13,    14,    -1,    -1,    -1,   196,    19,    -1,    21,
      22,    -1,    24,    25,    26,    -1,   206,   207,    30,    31,
      -1,    33,    34,    35,    -1,    37,    38,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,
      52,    53,    54,    55,    56,    -1,    58,    -1,    60,    61,
      -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,
      -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,    81,
      -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,    -1,
      92,    93,    94,    -1,    96,    97,    98,    99,    -1,   101,
     102,   103,    -1,   105,   106,    -1,    -1,   109,   110,   111,
     112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,    -1,
     122,   123,   124,    -1,   126,   127,   128,   129,   130,   131,
     132,    -1,   134,   135,   136,   137,   138,    -1,   140,    -1,
      -1,   143,   144,   145,   146,   147,    -1,   149,   150,   151,
      -1,   153,    -1,    -1,    -1,    -1,   158,   159,   160,   161,
      -1,    -1,   164,   165,   166,   167,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,     3,
       4,     5,     6,     7,     8,    -1,    10,    11,    -1,    13,
      -1,    -1,    -1,    -1,   196,    19,    -1,    21,    22,    -1,
      24,    25,    26,    -1,   206,   207,    30,    31,    -1,    33,
      34,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    46,    47,    48,    -1,    50,    -1,    52,    53,
      54,    55,    56,    -1,    58,    -1,    60,    61,    -1,    63,
      -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,    73,
      -1,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,
      -1,    85,    -1,    -1,    88,    -1,    -1,    -1,    92,    93,
      94,    -1,    96,    97,    98,    99,    -1,   101,   102,   103,
      -1,   105,   106,    -1,    -1,   109,   110,   111,   112,   113,
      -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,
     124,    -1,   126,   127,   128,   129,   130,   131,   132,    -1,
     134,   135,   136,   137,   138,    -1,   140,    -1,    -1,   143,
     144,   145,   146,   147,    -1,   149,   150,   151,    -1,   153,
      -1,    -1,    -1,    -1,   158,   159,   160,   161,    -1,    -1,
     164,   165,   166,   167,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,    -1,    -1,     3,     4,     5,
       6,     7,     8,    -1,    10,    11,    -1,    13,    -1,    -1,
      -1,    -1,   196,    19,    -1,    21,    22,    -1,    24,    25,
      26,    -1,   206,   207,    30,    31,    -1,    33,    34,    35,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      46,    47,    48,    -1,    50,    -1,    52,    53,    54,    55,
      56,    -1,    58,    -1,    60,    61,    -1,    63,    -1,    -1,
      -1,    -1,    -1,    -1,    70,    -1,    -1,    73,    -1,    -1,
      -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,
      -1,    -1,    88,    -1,    -1,    -1,    92,    93,    94,    -1,
      96,    97,    98,    99,    -1,   101,   102,   103,    -1,   105,
     106,    -1,    -1,   109,   110,   111,   112,   113,    -1,   115,
      -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,
     126,   127,   128,   129,   130,   131,   132,    -1,   134,   135,
     136,    -1,   138,    -1,   140,    -1,    -1,   143,   144,   145,
     146,   147,    -1,   149,   150,   151,    -1,   153,    -1,    -1,
      -1,    -1,   158,   159,   160,   161,    -1,    -1,    -1,   165,
     166,   167,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     196,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   204,    -1,
     206,   207,     3,     4,     5,     6,     7,     8,    -1,    10,
      11,    -1,    13,    14,    -1,    -1,    -1,    -1,    19,    -1,
      21,    22,    -1,    24,    25,    26,    -1,    -1,    -1,    30,
      31,    -1,    33,    34,    35,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    52,    53,    54,    55,    56,    -1,    58,    -1,    60,
      61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    92,    93,    94,    -1,    96,    97,    98,    99,    -1,
     101,   102,   103,    -1,   105,   106,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
     131,   132,    -1,   134,   135,   136,    -1,   138,    -1,   140,
      -1,    -1,   143,   144,   145,   146,   147,    -1,   149,   150,
     151,    -1,   153,    -1,    -1,    -1,    -1,   158,   159,   160,
     161,    -1,    -1,    -1,   165,   166,   167,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,
       3,     4,     5,     6,     7,     8,    -1,    10,    11,    -1,
      13,    -1,    -1,    -1,    -1,   196,    19,    -1,    21,    22,
      -1,    24,    25,    26,    -1,   206,   207,    30,    31,    -1,
      33,    34,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,    52,
      53,    54,    55,    56,    -1,    58,    -1,    60,    61,    -1,
      63,    -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,
      73,    -1,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,
      -1,    -1,    85,    -1,    -1,    88,    -1,    -1,    -1,    92,
      93,    94,    -1,    96,    97,    98,    99,    -1,   101,   102,
     103,    -1,   105,   106,    -1,    -1,   109,   110,   111,   112,
     113,    -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,
     123,   124,    -1,   126,   127,   128,   129,   130,   131,   132,
      -1,   134,   135,   136,    -1,   138,    -1,   140,    -1,    -1,
     143,   144,   145,   146,   147,    -1,   149,   150,   151,    -1,
     153,    -1,    -1,    -1,    -1,   158,   159,   160,   161,    -1,
      -1,    -1,   165,   166,   167,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,     3,     4,
       5,     6,     7,     8,    -1,    10,    11,    -1,    13,    -1,
      -1,    -1,    -1,   196,    19,    -1,    21,    22,    -1,    24,
      25,    26,    -1,   206,   207,    30,    31,    -1,    33,    34,
      35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    52,    53,    54,
      55,    56,    -1,    58,    -1,    60,    61,    -1,    63,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,    73,    -1,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    -1,    -1,    88,    -1,    -1,    -1,    92,    93,    94,
      -1,    96,    97,    98,    99,    -1,   101,   102,   103,    -1,
     105,   106,    -1,    -1,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,   131,   132,    -1,   134,
     135,   136,    -1,   138,    -1,   140,    -1,    -1,   143,   144,
     145,   146,   147,    -1,   149,   150,   151,    -1,   153,    -1,
      -1,    -1,     3,   158,   159,   160,   161,    -1,    -1,    -1,
     165,   166,   167,    -1,    15,    -1,    -1,    -1,    19,    -1,
      21,    22,   177,    24,    25,    26,    -1,    -1,    -1,    -1,
      31,    -1,    33,    34,    35,    -1,    37,    38,    -1,    -1,
      -1,   196,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,   206,   207,    -1,    55,    56,    -1,    -1,    -1,    60,
      61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,   103,    -1,   105,   106,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
      -1,    -1,   143,   144,   145,   146,    -1,    -1,   149,   150,
     151,    -1,   153,    -1,    -1,    -1,    -1,   158,   159,   160,
     161,    -1,     3,    -1,   165,   166,   167,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    15,    -1,   177,    -1,    19,    -1,
      21,    22,    -1,    24,    25,    26,    -1,    -1,    -1,    -1,
      31,    -1,    33,    34,    35,   196,    37,    38,    -1,    -1,
      -1,    -1,    -1,   204,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    -1,    55,    56,    -1,    -1,    -1,    60,
      61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,   103,    -1,   105,   106,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
      -1,    -1,   143,   144,   145,   146,    -1,    -1,   149,   150,
     151,    -1,   153,    -1,    -1,    -1,    -1,   158,   159,   160,
     161,    -1,     3,    -1,   165,   166,   167,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    19,    -1,
      21,    22,    -1,    24,    25,    26,    -1,    -1,    -1,    -1,
      31,    -1,    33,    34,    35,   196,    37,    38,    -1,    -1,
      -1,    -1,    -1,   204,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    -1,    55,    56,    -1,    -1,    -1,    60,
      61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,   103,    -1,   105,   106,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
      -1,    -1,   143,   144,   145,   146,    -1,    -1,   149,   150,
     151,    -1,   153,    -1,    -1,    -1,    -1,   158,   159,   160,
     161,    -1,     3,    -1,   165,   166,   167,    -1,    -1,    -1,
      -1,    -1,    13,    -1,    -1,    -1,   177,    -1,    19,    -1,
      21,    22,    -1,    24,    25,    26,    -1,    -1,    -1,    -1,
      31,    -1,    33,    34,    35,   196,    37,    38,    -1,    -1,
      -1,    -1,    -1,   204,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    -1,    55,    56,    -1,    -1,    -1,    60,
      61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,   103,    -1,   105,   106,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,   137,    -1,    -1,   140,
      -1,    -1,   143,   144,   145,   146,    -1,    -1,   149,   150,
     151,    -1,   153,    -1,    -1,    -1,     3,   158,   159,   160,
     161,    -1,    -1,   164,   165,   166,   167,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    21,    22,   177,    24,    25,    26,
      27,    -1,    -1,    -1,    31,    32,    33,    -1,    35,    -1,
      37,    38,    -1,   194,   195,   196,    -1,    -1,    -1,    46,
      47,    48,    -1,    50,    -1,    -1,    -1,    54,    55,    56,
      57,    -1,    -1,    60,    61,    -1,    63,    -1,    -1,    -1,
      -1,    -1,    -1,    70,    -1,    72,    73,    -1,    -1,    -1,
      77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,    86,
      -1,    88,    -1,    -1,    -1,    92,    93,    -1,    -1,    96,
      97,    98,    99,    -1,   101,   102,   103,    -1,   105,   106,
      -1,    -1,   109,   110,   111,   112,   113,    -1,   115,    -1,
      -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,
     127,   128,   129,   130,    -1,   132,    -1,   134,   135,   136,
      -1,    -1,    -1,   140,   141,    -1,   143,   144,   145,   146,
      -1,    -1,   149,   150,   151,    -1,   153,    -1,    -1,    -1,
      -1,   158,    -1,   160,   161,    -1,     3,    -1,   165,   166,
     167,    -1,    -1,    -1,    -1,    -1,    13,    -1,    -1,    -1,
     177,    -1,    19,    -1,    21,    22,    -1,    24,    25,    26,
      -1,    -1,    -1,    -1,    31,    -1,    33,    34,    35,   196,
      37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,
      47,    48,    -1,    50,    -1,    -1,    -1,    -1,    55,    56,
      -1,    -1,    -1,    60,    61,    -1,    63,    -1,    -1,    -1,
      -1,    -1,    -1,    70,    -1,    -1,    73,    -1,    -1,    -1,
      77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,    -1,
      -1,    88,    -1,    -1,    -1,    92,    93,    -1,    -1,    96,
      97,    98,    99,    -1,   101,   102,   103,    -1,   105,   106,
      -1,    -1,   109,   110,   111,   112,   113,    -1,   115,    -1,
      -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,
     127,   128,   129,   130,    -1,   132,    -1,   134,   135,   136,
     137,    -1,    -1,   140,    -1,    -1,   143,   144,   145,   146,
      -1,    -1,   149,   150,   151,    -1,   153,    -1,    -1,    -1,
      -1,   158,   159,   160,   161,    -1,     3,   164,   165,   166,
     167,    -1,    -1,    -1,    -1,    -1,    13,    -1,    -1,    -1,
     177,    -1,    19,    -1,    21,    22,    -1,    24,    25,    26,
      -1,    -1,    -1,    -1,    31,    -1,    33,    34,    35,   196,
      37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,
      47,    48,    -1,    50,    -1,    -1,    -1,    -1,    55,    56,
      -1,    -1,    -1,    60,    61,    -1,    63,    -1,    -1,    -1,
      -1,    -1,    -1,    70,    -1,    -1,    73,    -1,    -1,    -1,
      77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,    -1,
      -1,    88,    -1,    -1,    -1,    92,    93,    -1,    -1,    96,
      97,    98,    99,    -1,   101,   102,   103,    -1,   105,   106,
      -1,    -1,   109,   110,   111,   112,   113,    -1,   115,    -1,
      -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,
     127,   128,   129,   130,    -1,   132,    -1,   134,   135,   136,
      -1,    -1,    -1,   140,    -1,    -1,   143,   144,   145,   146,
      -1,    -1,   149,   150,   151,    -1,   153,    -1,    -1,    -1,
      -1,   158,   159,   160,   161,    -1,    -1,    -1,   165,   166,
     167,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,    -1,    -1,     3,    -1,    -1,    -1,    -1,    -1,
      -1,    10,    11,    -1,    -1,    -1,    -1,   194,   195,   196,
      19,    -1,    21,    22,    -1,    24,    25,    26,    -1,    -1,
      -1,    -1,    31,    -1,    33,    34,    35,    -1,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,
      -1,    50,    -1,    -1,    -1,    -1,    55,    56,    -1,    -1,
      -1,    60,    61,    -1,    63,    -1,    -1,    -1,    -1,    -1,
      -1,    70,    -1,    -1,    73,    -1,    -1,    -1,    77,    -1,
      79,    -1,    81,    -1,    -1,    -1,    85,    -1,    -1,    88,
      -1,    -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,
      99,    -1,   101,   102,   103,    -1,   105,   106,    -1,    -1,
     109,   110,   111,   112,   113,    -1,   115,    -1,    -1,   118,
      -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,   128,
     129,   130,    -1,   132,    -1,   134,   135,   136,    -1,    -1,
      -1,   140,    -1,    -1,   143,   144,   145,   146,    -1,    -1,
     149,   150,   151,    -1,   153,    -1,    -1,    -1,     3,   158,
     159,   160,   161,    -1,    -1,    -1,   165,   166,   167,    -1,
      15,    -1,    -1,    -1,    19,    -1,    21,    22,   177,    24,
      25,    26,    -1,    -1,    -1,    -1,    31,    -1,    33,    34,
      35,    -1,    37,    38,    -1,    -1,    -1,   196,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    -1,    -1,    -1,
      55,    56,    -1,    -1,    -1,    60,    61,    -1,    63,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,    73,    -1,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    -1,    -1,    88,    -1,    -1,    -1,    92,    93,    -1,
      -1,    96,    97,    98,    99,    -1,   101,   102,   103,    -1,
     105,   106,    -1,    -1,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,    -1,   132,    -1,   134,
     135,   136,    -1,    -1,    -1,   140,    -1,    -1,   143,   144,
     145,   146,    -1,    -1,   149,   150,   151,    -1,   153,    -1,
      -1,    -1,     3,   158,   159,   160,   161,    -1,    -1,    -1,
     165,   166,   167,    -1,    15,    -1,    -1,    -1,    19,    -1,
      21,    22,   177,    24,    25,    26,    -1,    -1,    -1,    -1,
      31,    -1,    33,    34,    35,    -1,    37,    38,    -1,    -1,
      -1,   196,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    -1,    55,    56,    -1,    -1,    -1,    60,
      61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,   103,    -1,   105,   106,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
      -1,    -1,   143,   144,   145,   146,    -1,    -1,   149,   150,
     151,    -1,   153,    -1,    -1,    -1,     3,   158,   159,   160,
     161,    -1,    -1,    -1,   165,   166,   167,    -1,    15,    -1,
      -1,    -1,    19,    -1,    21,    22,   177,    24,    25,    26,
      -1,    -1,    -1,    -1,    31,    -1,    33,    34,    35,    -1,
      37,    38,    -1,    -1,    -1,   196,    -1,    -1,    -1,    46,
      47,    48,    -1,    50,    -1,    -1,    -1,    -1,    55,    56,
      -1,    -1,    -1,    60,    61,    -1,    63,    -1,    -1,    -1,
      -1,    -1,    -1,    70,    -1,    -1,    73,    -1,    -1,    -1,
      77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,    -1,
      -1,    88,    -1,    -1,    -1,    92,    93,    -1,    -1,    96,
      97,    98,    99,    -1,   101,   102,   103,    -1,   105,   106,
      -1,    -1,   109,   110,   111,   112,   113,    -1,   115,    -1,
      -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,
     127,   128,   129,   130,    -1,   132,    -1,   134,   135,   136,
      -1,    -1,    -1,   140,    -1,    -1,   143,   144,   145,   146,
      -1,    -1,   149,   150,   151,    -1,   153,    -1,    -1,    -1,
       3,   158,   159,   160,   161,    -1,    -1,    -1,   165,   166,
     167,    -1,    -1,    -1,    -1,    -1,    19,    20,    21,    22,
     177,    24,    25,    26,    -1,    -1,    -1,    -1,    31,    -1,
      33,    34,    35,    -1,    37,    38,    -1,    -1,    -1,   196,
      -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,    -1,
      -1,    -1,    55,    56,    -1,    -1,    -1,    60,    61,    -1,
      63,    -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,
      73,    -1,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,
      -1,    -1,    85,    -1,    -1,    88,    -1,    -1,    -1,    92,
      93,    -1,    -1,    96,    97,    98,    99,    -1,   101,   102,
     103,    -1,   105,   106,    -1,    -1,   109,   110,   111,   112,
     113,    -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,
     123,   124,    -1,   126,   127,   128,   129,   130,    -1,   132,
      -1,   134,   135,   136,    -1,    -1,    -1,   140,    -1,    -1,
     143,   144,   145,   146,    -1,    -1,   149,   150,   151,    -1,
     153,    -1,    -1,    -1,     3,   158,   159,   160,   161,    -1,
      -1,    -1,   165,   166,   167,    -1,    -1,    -1,    -1,    -1,
      19,    20,    21,    22,   177,    24,    25,    26,    -1,    -1,
      -1,    -1,    31,    -1,    33,    34,    35,    -1,    37,    38,
      -1,    -1,    -1,   196,    -1,    -1,    -1,    46,    47,    48,
      -1,    50,    -1,    -1,    -1,    -1,    55,    56,    -1,    -1,
      -1,    60,    61,    -1,    63,    -1,    -1,    -1,    -1,    -1,
      -1,    70,    -1,    -1,    73,    -1,    -1,    -1,    77,    -1,
      79,    -1,    81,    -1,    -1,    -1,    85,    -1,    -1,    88,
      -1,    -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,
      99,    -1,   101,   102,   103,    -1,   105,   106,    -1,    -1,
     109,   110,   111,   112,   113,    -1,   115,    -1,    -1,   118,
      -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,   128,
     129,   130,    -1,   132,    -1,   134,   135,   136,    -1,    -1,
      -1,   140,    -1,    -1,   143,   144,   145,   146,    -1,    -1,
     149,   150,   151,    -1,   153,    -1,    -1,    -1,    -1,   158,
     159,   160,   161,    -1,     3,    -1,   165,   166,   167,    -1,
      -1,    -1,    -1,    -1,    13,    -1,    -1,    -1,   177,    -1,
      19,    -1,    21,    22,    -1,    24,    25,    26,    -1,    -1,
      -1,    -1,    31,    -1,    33,    34,    35,   196,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,
      -1,    50,    -1,    -1,    -1,    -1,    55,    56,    -1,    -1,
      -1,    60,    61,    -1,    63,    -1,    -1,    -1,    -1,    -1,
      -1,    70,    -1,    -1,    73,    -1,    -1,    -1,    77,    -1,
      79,    -1,    81,    -1,    -1,    -1,    85,    -1,    -1,    88,
      -1,    -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,
      99,    -1,   101,   102,   103,    -1,   105,   106,    -1,    -1,
     109,   110,   111,   112,   113,    -1,   115,    -1,    -1,   118,
      -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,   128,
     129,   130,    -1,   132,    -1,   134,   135,   136,    -1,    -1,
      -1,   140,    -1,    -1,   143,   144,   145,   146,    -1,    -1,
     149,   150,   151,    -1,   153,    -1,    -1,    -1,     3,   158,
     159,   160,   161,    -1,    -1,    -1,   165,   166,   167,    -1,
      -1,    -1,    -1,    -1,    19,    -1,    21,    22,   177,    24,
      25,    26,    -1,    -1,    -1,    -1,    31,    -1,    33,    34,
      35,    -1,    37,    38,    -1,    -1,    -1,   196,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    -1,    -1,    -1,
      55,    56,    -1,    -1,    -1,    60,    61,    -1,    63,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    -1,    -1,    73,    -1,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    -1,    -1,    88,    -1,    -1,    -1,    92,    93,    -1,
      -1,    96,    97,    98,    99,    -1,   101,   102,   103,    -1,
     105,   106,    -1,    -1,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,    -1,   132,    -1,   134,
     135,   136,    -1,    -1,    -1,   140,    -1,    -1,   143,   144,
     145,   146,    -1,    -1,   149,   150,   151,    -1,   153,    -1,
      -1,    -1,     3,   158,   159,   160,   161,    -1,    -1,    -1,
     165,   166,   167,    -1,    -1,    -1,    -1,    -1,    19,    -1,
      21,    22,   177,    24,    25,    26,    -1,    -1,    -1,    -1,
      31,    -1,    33,    34,    35,    -1,    37,    38,    -1,    -1,
      -1,   196,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    -1,    55,    56,    -1,    -1,    -1,    60,
      61,    -1,    63,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,   103,    -1,   105,   106,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
      -1,    -1,   143,   144,   145,   146,    -1,    -1,   149,   150,
     151,    -1,   153,     3,    -1,    -1,    -1,   158,   159,   160,
     161,    -1,    -1,    -1,   165,   166,   167,    -1,    -1,    -1,
      -1,    21,    22,    -1,    -1,    -1,   177,    -1,    -1,    -1,
      -1,    31,    -1,    -1,    -1,    -1,    -1,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,   196,    46,    47,    48,    -1,
      50,    -1,    -1,    -1,    -1,    55,    -1,    -1,    -1,    -1,
      60,    61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      70,    -1,    -1,    73,    -1,    -1,    -1,    77,    -1,    79,
      -1,    81,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,    99,
      -1,   101,   102,    -1,    -1,    -1,    -1,    -1,    -1,   109,
      -1,   111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,
      -1,    -1,   122,   123,   124,    -1,   126,   127,    -1,   129,
     130,    -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,
     140,    -1,    -1,   143,   144,   145,   146,    -1,    -1,   149,
      -1,    -1,    -1,   153,    -1,    -1,    -1,    -1,   158,    -1,
     160,   161,    -1,    -1,    -1,   165,   166,   167,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   196
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,    13,    29,    59,    84,   137,   155,   164,   201,   213,
     214,   215,   216,   221,   222,   223,   226,   227,   233,   234,
     235,   236,   237,   243,   257,   234,     3,    19,    21,    22,
      24,    25,    26,    31,    33,    34,    35,    37,    38,    46,
      47,    48,    50,    55,    56,    60,    61,    63,    70,    73,
      77,    79,    81,    85,    88,    92,    93,    96,    97,    98,
      99,   101,   102,   103,   105,   106,   109,   110,   111,   112,
     113,   115,   118,   122,   123,   124,   126,   127,   128,   129,
     130,   132,   134,   135,   136,   140,   143,   144,   145,   146,
     149,   150,   151,   153,   158,   159,   160,   161,   165,   166,
     167,   177,   196,   220,   275,   320,   322,   351,   369,   371,
     372,   374,    75,    89,    16,    62,   182,   263,   275,   129,
     258,   259,   369,    29,     0,    66,   200,    69,   117,   154,
     181,   250,   251,    87,   235,    14,    13,    15,   275,   275,
     263,     4,     5,     6,     7,     8,    10,    11,    13,    30,
      31,    37,    46,    52,    53,    54,    58,    94,   109,   131,
     138,   147,   151,   204,   206,   207,   238,   239,   241,   264,
     265,   273,   274,   275,   298,   299,   300,   301,   302,   303,
     312,   313,   318,   319,   320,   321,   322,   338,   339,   351,
     369,   139,   258,   203,    13,   260,   261,   220,   214,   263,
      28,   175,   263,   263,    74,   375,   376,   263,   250,    13,
      14,    71,   108,   185,   217,   218,   219,   283,   284,   285,
     286,   287,   288,   289,   290,   291,   292,   293,   294,   295,
     297,   298,   369,    15,   369,   163,   245,    13,    58,   157,
     224,   225,   233,   261,   264,    13,   234,   298,   304,   298,
     314,    13,    13,    13,    13,     4,    13,    13,    13,     4,
     302,   302,    75,   246,   203,    13,    20,    36,    39,   209,
     308,   365,   366,   367,   368,   369,   206,   207,   310,   100,
     204,   210,   211,   311,   205,    13,    13,    13,    15,   231,
     232,   298,   258,   262,   369,    20,    13,   236,   252,   253,
     298,    28,   236,   236,   155,   176,   381,   385,   237,   375,
      13,   283,   298,    13,   239,   240,   285,   298,    13,    14,
     203,    20,   116,    17,    90,    23,    40,    41,    42,    43,
      44,    45,    82,    90,    95,   108,   208,   307,   309,    15,
      15,   369,    15,   283,   157,   304,   225,   233,   246,   234,
      14,   203,    14,   162,   315,   316,   298,   304,   298,     5,
      14,   298,    14,    13,   115,   194,   195,   240,   266,   267,
     268,   269,   271,   275,   276,   277,   278,   279,   280,   245,
     264,    14,   304,   367,   275,   209,   299,   300,   301,   302,
      16,    62,   204,   298,   363,    14,   298,   204,   369,   245,
     203,    40,    14,   203,    13,   242,    14,   217,   203,    21,
      61,   254,   252,   113,   377,     5,    10,    11,   384,   179,
     388,   381,    14,   304,   217,     3,    24,    25,    26,    27,
      32,    33,    35,    54,    56,    57,    63,    72,    85,    86,
      88,   103,   105,   106,   110,   128,   130,   141,   150,   151,
     158,   320,   322,   340,   341,   342,   343,   344,   345,   346,
     351,   352,   353,   354,   355,   356,   357,   358,   359,   370,
     372,   373,   284,   285,     6,     9,   108,   306,   298,    40,
      40,    42,    13,   240,   305,     8,   108,   183,   184,   296,
     298,    23,    82,    95,    40,    16,    18,   142,   298,   364,
     240,   369,   369,    15,    15,   369,   245,    14,   304,   283,
     298,    64,   317,   315,    20,    14,   156,    14,   203,    13,
     267,   276,    13,    13,    13,   365,    49,    76,    83,    91,
      94,   104,   119,   131,   203,   281,   365,   196,   365,   397,
     191,   192,   244,   394,   395,   396,    14,    14,    14,   298,
     120,   323,    14,    15,   168,   228,   231,   298,   262,   233,
     188,   389,   390,   202,    14,   252,   173,   255,   274,   369,
     378,   171,   172,   380,   132,   133,   382,   177,   178,   386,
      14,    93,    13,    13,    93,   159,    13,    93,   159,    13,
      13,    13,   124,    13,   136,   347,   348,   350,   351,    32,
      33,    13,    93,   159,    13,    13,    13,    13,   164,   165,
      13,   164,   165,    13,    19,   102,    33,   368,   368,    15,
     306,    17,   304,   296,    67,   298,   305,   298,    15,   369,
     369,    15,   244,   148,   148,   298,    65,   340,   275,   298,
      14,   275,   275,   275,   261,   272,    91,   186,   119,   282,
     267,   282,    91,   281,   186,   282,   266,    91,    13,   197,
     121,   270,   365,   164,    28,    78,   247,   395,   394,   323,
     323,    14,    13,   324,   325,   369,   204,   369,   304,    14,
     189,   190,   187,   391,   392,   202,   174,   177,   203,    15,
       5,   169,   379,   383,   384,   111,     5,     5,   111,    13,
       5,   111,    13,     5,   360,     5,     5,     5,    13,   152,
      13,    13,   159,    13,    93,   159,     5,   111,    13,   360,
       5,   275,     5,   150,   150,     5,   150,   150,     5,    13,
     139,   370,   298,    14,   298,    17,    67,   369,   369,   247,
     298,   298,    14,    14,    14,    14,    14,    14,   268,   268,
     114,   156,   268,    91,   268,   267,     5,    13,    28,   283,
     193,   199,   393,    28,    80,   256,   323,   325,   326,   327,
      15,    89,   177,   177,   262,   378,   369,   170,   180,   382,
      13,    14,    14,    13,     5,    14,    13,     5,    34,    38,
      77,    92,    96,   112,   361,   362,    14,    14,   203,    14,
     203,    14,     5,   136,   349,   351,     5,     5,    13,     5,
     111,    13,    14,    13,     5,    14,    14,   203,    14,    14,
     167,   167,    14,   167,   167,    14,     5,   275,    15,   298,
     298,    15,   256,   283,   261,   268,   114,   156,    14,     5,
      13,   199,   283,   263,   283,    14,   121,   328,   204,   369,
      10,    11,   229,   230,   274,    28,    28,   139,    15,   382,
     115,   164,   387,     5,   360,    14,   360,    14,   361,     5,
       5,    14,   203,    13,    14,    14,     5,    14,    13,     5,
     360,    14,     5,   135,   164,   165,   164,   165,    14,   368,
     370,   283,   261,   198,   398,    14,   378,    13,    79,   248,
     249,   298,    28,   250,    15,   203,   262,   262,   369,   369,
     387,   149,    14,    14,    14,    14,    14,     5,     5,    14,
     360,    14,    14,    14,   275,   150,   150,   150,   150,    15,
      13,   398,    14,    14,   140,   203,   304,   125,   133,   329,
     330,   369,   229,   254,   254,   152,    15,    14,    14,    14,
     167,   167,   167,   167,   370,     5,    13,   248,    23,    51,
     151,   153,   312,   331,   332,   333,   334,   255,   255,     4,
     369,    14,   248,   153,   312,   332,   335,   336,   132,   123,
     123,    70,   337,   139,   139,    58,    14,    73,    73,    17,
      51,    78,   107,   149,   369,   369,     4,   335,   132,   118
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   212,   213,   214,   214,   215,   215,   215,   215,   216,
     216,   216,   216,   217,   217,   218,   218,   219,   219,   220,
     221,   222,   222,   222,   223,   224,   224,   224,   224,   224,
     225,   226,   227,   228,   228,   229,   229,   230,   230,   230,
     231,   231,   232,   233,   234,   234,   235,   235,   235,   235,
     236,   236,   237,   237,   238,   239,   240,   241,   242,   243,
     243,   244,   244,   245,   245,   246,   247,   247,   248,   248,
     249,   249,   249,   250,   250,   251,   251,   252,   252,   253,
     254,   254,   254,   255,   255,   255,   256,   256,   257,   257,
     258,   258,   259,   260,   260,   261,   262,   262,   263,   263,
     263,   264,   264,   265,   265,   266,   266,   267,   267,   268,
     268,   269,   269,   269,   269,   269,   270,   271,   271,   271,
     272,   272,   273,   273,   273,   273,   274,   274,   274,   274,
     274,   275,   275,   275,   275,   275,   275,   275,   275,   276,
     276,   276,   276,   277,   277,   278,   278,   278,   278,   279,
     280,   280,   281,   281,   281,   281,   282,   282,   283,   283,
     284,   284,   285,   285,   286,   286,   286,   287,   287,   288,
     288,   288,   288,   288,   288,   288,   288,   289,   290,   291,
     292,   292,   293,   293,   293,   293,   294,   294,   295,   295,
     296,   296,   296,   297,   298,   298,   298,   299,   299,   300,
     300,   301,   301,   302,   302,   302,   303,   303,   303,   303,
     303,   303,   304,   304,   305,   305,   306,   306,   307,   307,
     307,   307,   307,   307,   307,   307,   307,   307,   308,   308,
     309,   310,   310,   311,   311,   311,   311,   312,   312,   312,
     312,   312,   312,   312,   312,   312,   312,   313,   314,   314,
     315,   315,   316,   316,   317,   317,   318,   318,   318,   318,
     319,   319,   319,   319,   319,   319,   320,   320,   320,   320,
     320,   320,   320,   320,   320,   320,   320,   321,   322,   322,
     322,   322,   322,   323,   323,   324,   325,   326,   327,   327,
     328,   328,   329,   329,   330,   330,   331,   331,   332,   332,
     332,   333,   334,   335,   335,   335,   336,   337,   337,   337,
     337,   337,   338,   338,   339,   339,   339,   339,   339,   339,
     339,   339,   339,   339,   339,   339,   340,   340,   340,   340,
     341,   342,   342,   342,   342,   343,   344,   344,   344,   345,
     345,   345,   345,   345,   345,   345,   345,   345,   345,   345,
     346,   347,   347,   348,   348,   349,   349,   349,   350,   350,
     350,   350,   350,   351,   351,   351,   351,   351,   352,   353,
     353,   353,   353,   353,   353,   353,   353,   353,   353,   353,
     354,   354,   355,   355,   355,   355,   355,   355,   355,   355,
     355,   355,   355,   355,   355,   356,   356,   356,   356,   357,
     357,   357,   357,   357,   357,   357,   357,   357,   357,   357,
     357,   357,   358,   358,   358,   358,   359,   359,   359,   359,
     359,   359,   359,   359,   359,   359,   359,   359,   359,   359,
     359,   360,   360,   360,   360,   361,   361,   361,   362,   362,
     362,   363,   363,   364,   364,   364,   365,   365,   366,   366,
     367,   368,   369,   369,   370,   370,   371,   371,   371,   371,
     371,   371,   371,   371,   371,   371,   371,   371,   371,   371,
     371,   371,   371,   371,   371,   372,   372,   372,   372,   372,
     372,   372,   372,   372,   372,   372,   372,   372,   372,   372,
     372,   372,   372,   372,   372,   372,   372,   372,   372,   372,
     372,   372,   372,   372,   372,   372,   372,   372,   372,   372,
     372,   373,   373,   373,   373,   374,   374,   374,   374,   374,
     375,   375,   376,   377,   377,   378,   378,   379,   379,   380,
     380,   380,   381,   382,   382,   383,   383,   384,   384,   384,
     385,   385,   386,   386,   387,   387,   388,   388,   388,   389,
     389,   390,   390,   391,   391,   392,   393,   393,   393,   393,
     394,   395,   396,   396,   396,   397,   397,   398,   398
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     1,     3,     0,     1,     1,     1,     5,
       4,     7,     6,     1,     3,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     4,     1,     2,     2,     1,     2,
       2,     4,     6,     0,     4,     1,     3,     1,     1,     1,
       1,     3,     3,     1,     4,     5,     1,     4,     4,     4,
       1,     4,     1,     3,     3,     1,     1,     1,     3,     8,
       9,     0,     1,     0,     2,     2,     0,     4,     1,     3,
       2,     1,     5,     0,     1,     3,     4,     1,     3,     3,
       0,     1,     1,     0,     2,     2,     0,     2,     2,     3,
       1,     3,     6,     0,     1,     3,     1,     3,     0,     1,
       1,     1,     3,     2,     1,     1,     3,     1,     1,     1,
       3,     2,     3,     2,     3,     3,     5,     4,     4,     4,
       0,     1,     1,     3,     5,     7,     1,     3,     5,     7,
       9,     1,     3,     5,     4,     7,     6,     6,     5,     1,
       1,     1,     1,     4,     4,     6,     5,     6,     5,     4,
       5,     4,     2,     2,     2,     1,     0,     1,     1,     3,
       1,     3,     1,     2,     1,     3,     4,     1,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     4,     3,     3,
       5,     6,     3,     5,     4,     6,     3,     4,     3,     4,
       1,     1,     1,     2,     1,     2,     3,     1,     3,     1,
       3,     1,     3,     1,     2,     2,     1,     1,     1,     3,
       1,     1,     1,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     2,     2,     2,     1,     2,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     2,     5,     0,     1,
       1,     2,     4,     4,     0,     2,     1,     1,     1,     1,
       5,     4,     6,     5,     5,     4,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     4,     1,     1,
       1,     1,     1,     2,     2,     3,     1,     4,     0,     1,
       0,     3,     0,     3,     1,     1,     1,     1,     2,     2,
       1,     2,     4,     1,     2,     1,     2,     0,     3,     2,
       2,     3,     3,     4,     4,     3,     3,     6,     6,     4,
       1,     4,     1,     6,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     5,     7,     6,     5,     2,     2,     5,
       4,     2,     1,     2,     1,     1,     1,     1,     1,     1,
       2,     3,     1,     4,     1,     1,     4,     1,     4,     1,
       6,     4,     1,     1,     1,     1,     1,     1,     1,     1,
       7,     7,     4,     4,     4,     7,     7,     4,     4,     4,
       1,     1,     6,     4,     6,     4,     6,     4,     1,     1,
       1,     1,     1,     1,     1,     4,     2,     1,     1,     4,
       4,     5,     5,     4,     6,     3,     6,     3,     4,     1,
       1,     1,     6,     3,     4,     1,     5,     2,     5,     2,
       4,     6,     6,     5,     7,     4,     6,     3,     4,     1,
       1,     3,     2,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     0,     1,     2,     1,
       1,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     1,     5,     0,     2,     1,     3,     0,     2,     0,
       1,     2,     2,     1,     1,     0,     1,     1,     1,     1,
       0,     3,     1,     1,     1,     2,     0,     5,     6,     0,
       1,     9,     9,     0,     1,     8,     0,     1,     2,     1,
       4,     3,     2,     1,     2,     6,     5,     0,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (&yylloc, result, scanner, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if ORACLE_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined ORACLE_LTYPE_IS_TRIVIAL && ORACLE_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location, result, scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, result, scanner);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule, ParseResult* result, yyscan_t scanner)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       , result, scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, result, scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !ORACLE_DEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !ORACLE_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yytype)
    {
          case 3: /* NAME  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3309 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 4: /* STRING  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3315 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 5: /* INTNUM  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3321 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 6: /* BOOL  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3327 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 7: /* APPROXNUM  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3333 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 8: /* NULLX  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3339 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 9: /* UNKNOWN  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3345 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 10: /* QUESTIONMARK  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3351 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 11: /* PARAM  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3357 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 213: /* sql_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3363 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 214: /* stmt_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3369 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 215: /* stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3375 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 216: /* call_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3381 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 217: /* sql_argument_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3387 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 218: /* sql_argument  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3393 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 219: /* value_expression  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3399 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 220: /* sp_name  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3405 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 221: /* dql_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3411 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 222: /* dml_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3417 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 223: /* insert_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3423 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 224: /* insert_columns_and_source  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3429 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 225: /* from_constructor  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3435 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 226: /* delete_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3441 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 227: /* update_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3447 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 228: /* opt_returning_into  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3453 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 229: /* variable_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3459 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 230: /* variable  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3465 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 231: /* update_elem_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3471 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 232: /* update_elem  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3477 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 233: /* select_stmt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3483 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 234: /* query_expression  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3489 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 235: /* query_expression_body  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3495 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 236: /* query_term  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3501 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 237: /* query_primary  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3507 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 238: /* select_with_parens  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3513 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 239: /* subquery  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3519 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 240: /* table_subquery  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3525 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 241: /* row_subquery  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3531 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 242: /* cte_subquery  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3537 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 243: /* simple_table  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3543 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 244: /* opt_hierarchical_query_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3549 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 245: /* opt_where  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3555 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 246: /* opt_from_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3561 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 247: /* opt_groupby  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3567 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 248: /* grouping_element_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3573 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 249: /* grouping_element  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3579 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 250: /* opt_order_by  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3585 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 251: /* order_by  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3591 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 252: /* sort_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3597 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 253: /* sort_key  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3603 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 254: /* opt_asc_desc  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3609 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 255: /* opt_nulls_c  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3615 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 256: /* opt_having  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3621 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 257: /* with_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3627 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 258: /* with_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3633 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 259: /* common_table_expr  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3639 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 260: /* opt_derived_column_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3645 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 261: /* simple_ident_list_with_parens  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3651 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 262: /* simple_ident_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3657 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 263: /* opt_distinct  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3663 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 264: /* select_expr_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3669 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 265: /* projection  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3675 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 266: /* from_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3681 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 267: /* table_reference  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3687 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 268: /* table_primary  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3693 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 269: /* table_primary_non_join  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3699 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 270: /* partition_by  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3705 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 271: /* table_function  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3711 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 272: /* opt_simple_ident_list_with_parens  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3717 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 273: /* asterisk_expr  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3723 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 274: /* column_ref  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3729 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 275: /* relation_factor  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3735 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 276: /* joined_table  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3741 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 277: /* cross_apply  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3747 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 278: /* qualified_join  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3753 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 279: /* cross_join  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3759 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 280: /* natural_join  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3765 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 281: /* join_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3771 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 283: /* search_condition  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3777 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 284: /* boolean_term  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3783 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 285: /* boolean_factor  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3789 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 286: /* boolean_test  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3795 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 287: /* boolean_primary  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3801 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 288: /* predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3807 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 289: /* bool_function  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3813 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 290: /* comparison_predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3819 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 291: /* quantified_comparison_predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3825 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 292: /* between_predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3831 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 293: /* like_predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3837 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 294: /* in_predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3843 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 295: /* null_predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3849 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 296: /* null_value  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3855 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 297: /* exists_predicate  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3861 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 298: /* row_expr  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3867 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 299: /* factor0  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3873 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 300: /* factor1  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3879 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 301: /* factor2  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3885 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 302: /* factor3  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3891 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 303: /* factor4  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3897 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 304: /* row_expr_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3903 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 305: /* in_expr  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3909 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 306: /* truth_value  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3915 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 312: /* expr_const  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3921 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 313: /* case_expr  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3927 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 314: /* case_arg  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3933 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 315: /* when_clause_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3939 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 316: /* when_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3945 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 317: /* case_default  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3951 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 318: /* func_expr  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3957 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 319: /* aggregate_windowed_function  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3963 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 320: /* aggregate_function_name  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3969 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 321: /* ranking_windowed_function  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3975 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 322: /* ranking_function_name  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3981 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 323: /* over_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3987 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 324: /* window_specification  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3993 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 325: /* window_name  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3999 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 326: /* window_specification_details  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4005 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 327: /* opt_existing_window_name  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4011 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 328: /* opt_window_partition_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4017 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 329: /* opt_window_frame_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4023 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 330: /* window_frame_units  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4029 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 331: /* window_frame_extent  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4035 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 332: /* window_frame_start  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4041 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 333: /* window_frame_preceding  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4047 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 334: /* window_frame_between  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4053 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 335: /* window_frame_bound  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4059 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 336: /* window_frame_following  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4065 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 337: /* opt_window_frame_exclusion  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4071 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 338: /* scalar_function  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4077 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 339: /* function_call_keyword  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4083 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 340: /* data_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4089 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 341: /* user_defined_type_name  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4095 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 342: /* relation_factor_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4101 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 343: /* reference_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4107 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 344: /* collection_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4113 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 345: /* predefined_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4119 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 346: /* interval_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4125 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 347: /* interval_qualifier  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4131 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 348: /* start_field  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4137 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 349: /* end_field  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4143 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 350: /* single_datetime_field  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4149 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 351: /* non_second_primary_datetime_field  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4155 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 352: /* boolean_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4161 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 353: /* datetime_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4167 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 354: /* numeric_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4173 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 355: /* exact_numeric_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4179 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 356: /* approximate_numeric_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4185 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 357: /* character_string_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4191 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 358: /* binary_large_object_string_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4197 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 359: /* national_character_string_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4203 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 360: /* large_object_length  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4209 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 361: /* char_length_units  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4215 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 362: /* multiplier  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4221 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 363: /* distinct_or_all  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4227 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 365: /* opt_as_label  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4233 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 366: /* as_label  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4239 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 367: /* label  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4245 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 368: /* collate_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4251 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 369: /* name_r  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4257 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 370: /* name_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4263 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 371: /* reserved_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4269 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 372: /* reserved_other  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4275 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 373: /* reserved_non_type  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4281 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 374: /* reserved  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4287 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 375: /* opt_for_update  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4293 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 376: /* for_update  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4299 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 377: /* opt_of_column_ref_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4305 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 378: /* column_ref_list  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4311 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 379: /* opt_ignore_lock  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4317 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 380: /* opt_wait_nowait  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4323 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 381: /* row_limiting_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4329 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 382: /* row_rows  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4335 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 383: /* opt_variable_v2  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4341 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 384: /* variable_v2  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4347 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 385: /* opt_offset_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4353 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 386: /* first_next  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4359 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 387: /* only_or_ties  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4365 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 388: /* opt_fetch_rows_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4371 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 389: /* opt_search_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4377 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 390: /* search_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4383 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 391: /* opt_cycle_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4389 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 392: /* cycle_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4395 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 393: /* connect_opt  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4401 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 394: /* connect_by  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4407 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 395: /* start_with  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4413 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 396: /* hierarchical_query_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4419 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 397: /* sample_clause  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4425 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;

    case 398: /* opt_seed_value  */
#line 95 "oracle.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 4431 "sqlparser_oracle_bison.cpp" /* yacc.c:1257  */
        break;


      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (ParseResult* result, yyscan_t scanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined ORACLE_LTYPE_IS_TRIVIAL && ORACLE_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

/* User initialization code.  */
#line 64 "oracle.y" /* yacc.c:1429  */
{
	// Initialize
	yylloc.first_column = 0;
	yylloc.last_column = 0;
	yylloc.first_line = 0;
	yylloc.last_line = 0;
}

#line 4548 "sqlparser_oracle_bison.cpp" /* yacc.c:1429  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, &yylloc, scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 244 "oracle.y" /* yacc.c:1646  */
    {
    g_QuestMarkId = 0;
    (yyval.node) = (yyvsp[-1].node);
    result->result_tree_ = (yyvsp[-1].node);
    result->accept = true;
    YYACCEPT;
}
#line 4743 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 4:
#line 256 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SEMICOLON_LIST_SERIALIZE_FORMAT;
}
#line 4752 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 5:
#line 263 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4758 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 9:
#line 271 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &CALL_SERIALIZE_FORMAT;
}
#line 4767 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 10:
#line 276 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-2].node), nullptr);
    (yyval.node)->serialize_format = &CALL_SERIALIZE_FORMAT;
}
#line 4776 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 11:
#line 281 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node));
    (yyval.node)->serialize_format = &CALL_SQL_SERVER_SERIALIZE_FORMAT;
}
#line 4785 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 12:
#line 286 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-3].node), nullptr);
    (yyval.node)->serialize_format = &CALL_SQL_SERVER_SERIALIZE_FORMAT;
}
#line 4794 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 14:
#line 295 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4803 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 15:
#line 303 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 4812 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 16:
#line 308 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 4821 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 24:
#line 335 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 4830 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 25:
#line 343 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4839 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 26:
#line 348 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4848 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 27:
#line 353 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4857 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 28:
#line 358 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4866 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 29:
#line 363 "oracle.y" /* yacc.c:1646  */
    {
    Node* src = Node::makeTerminalNode(E_IDENTIFIER, "DEFAULT VALUES");
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, nullptr, src);
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4876 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 30:
#line 372 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_VALUES_CTOR, E_VALUES_CTOR_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &TABLE_VALUE_CTOR_SERIALIZE_FORMAT;
}
#line 4885 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 31:
#line 381 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DELETE, E_DELETE_PROPERTY_CNT,
    		nullptr,	/* E_DELETE_OPT_WITH 0 */
    		nullptr,	/* E_DELETE_OPT_TOP 1 */
    		(yyvsp[-1].node),		/* E_DELETE_DELETE_RELATION 2 */
    		nullptr,	/* E_DELETE_DELETE_RELATION_OPT_TABLE_HINT 3 */
    		nullptr,        /* E_DELETE_OPT_OUTPUT 4 */
    		nullptr,	/* E_DELETE_FROM_LIST 5 */
    		(yyvsp[0].node),		/* E_DELETE_OPT_WHERE 6 */
    		nullptr 	/* E_DELETE_OPT_QUERY_HINT 7 */);
    (yyval.node)->serialize_format = &DELETE_SERIALIZE_FORMAT;
}
#line 4902 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 32:
#line 398 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPDATE, E_UPDATE_PROPERTY_CNT,
    		nullptr,	/* E_UPDATE_OPT_WITH 0 */
    		nullptr,	/* E_UPDATE_OPT_TOP 1 */
    		(yyvsp[-4].node),		/* E_UPDATE_UPDATE_RELATION 2 */
    		nullptr,	/* E_UPDATE_UPDATE_RELATION_OPT_TABLE_HINT 3 */
    		(yyvsp[-2].node),		/* E_UPDATE_UPDATE_ELEM_LIST 4 */
    		nullptr,	/* E_UPDATE_OPT_OUTPUT 5 */
    		nullptr,	/* E_UPDATE_FROM_LIST 6 */
    		(yyvsp[-1].node),		/* E_UPDATE_OPT_WHERE 7 */
    		(yyvsp[0].node)		/* E_UPDATE_OPT_QUERY_HINT 8 */);
    (yyval.node)->serialize_format = &UPDATE_SERIALIZE_FORMAT;
}
#line 4920 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 33:
#line 413 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = NULL;}
#line 4926 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 34:
#line 415 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_RETURNING_INTO, 2, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &RETURNING_INTO_SERIALIZE_FORMAT;
}
#line 4935 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 36:
#line 423 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_VAR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4944 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 41:
#line 438 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPDATE_ELEM_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4953 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 42:
#line 446 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_EQ, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &OP_EQ_SERIALIZE_FORMAT;
}
#line 4962 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 43:
#line 455 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = (yyvsp[0].node);

}
#line 4971 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 44:
#line 463 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, nullptr, (yyvsp[-3].node), nullptr, nullptr,nullptr);
    (yyval.node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT;

        (yyval.node)->setChild(E_DIRECT_SELECT_ORDER, (yyvsp[-2].node));
        (yyval.node)->setChild(E_DIRECT_SELECT_FOR_UPDATE, (yyvsp[-1].node));
        (yyval.node)->setChild(E_DIRECT_HINT_CLAUSE, (yyvsp[0].node));

}
#line 4985 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 45:
#line 473 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-3].node), nullptr, nullptr,nullptr);
    (yyval.node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT;


        (yyval.node)->setChild(E_DIRECT_SELECT_ORDER, (yyvsp[-2].node));
        (yyval.node)->setChild(E_DIRECT_SELECT_FOR_UPDATE, (yyvsp[-1].node));
        (yyval.node)->setChild(E_DIRECT_HINT_CLAUSE, (yyvsp[0].node));
}
#line 4999 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 47:
#line 486 "oracle.y" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_UNION, "UNION");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr             /*user for bigquery*/
                        );
    (yyval.node)->serialize_format = &SELECT_UNION_SERIALIZE_FORMAT;
}
#line 5030 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 48:
#line 513 "oracle.y" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_EXCEPT, "EXCEPT");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,             /* E_SELECT_OPT_INTO 18 */
                        nullptr             /*user for bigquery*/
                        );
    (yyval.node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT;
}
#line 5061 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 49:
#line 540 "oracle.y" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_EXCEPT, "MINUS");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                     nullptr,             /* E_SELECT_DISTINCT 0 */
                     nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                     nullptr,             /* E_SELECT_FROM_LIST 2 */
                     nullptr,             /* E_SELECT_OPT_WHERE 3 */
                     nullptr,             /* E_SELECT_GROUP_BY 4 */
                     nullptr,             /* E_SELECT_HAVING 5 */
                     set_op,              /* E_SELECT_SET_OPERATION 6 */
                     (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                     (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                     (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                     nullptr,             /* E_SELECT_ORDER_BY 10 */
                     nullptr,             /* E_SELECT_LIMIT 11 */
                     nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                     nullptr,             /* E_SELECT_HINTS 13 */
                     nullptr,             /* E_SELECT_WHEN 14 */
                     nullptr,             /* E_SELECT_OPT_TOP 15 */
                     nullptr,             /* E_SELECT_OPT_WITH 16 */
                     nullptr,             /* E_SELECT_OPT_OPTION 17 */
                     nullptr,             /* E_SELECT_OPT_INTO 18 */
                     nullptr             /*user for bigquery*/
                     );
    (yyval.node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT;
}
#line 5092 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 51:
#line 571 "oracle.y" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_INTERSECT, "INTERSECT");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr             /*user for bigquery*/
                        );
    (yyval.node)->serialize_format = &SELECT_INTERSECT_SERIALIZE_FORMAT;
}
#line 5123 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 53:
#line 602 "oracle.y" /* yacc.c:1646  */
    {
    if ((yyvsp[-1].node)->getChild(E_DIRECT_SELECT_WITH)) {
        yyerror(&(yylsp[-2]), result, scanner, "error use common table expression");
        YYABORT;
    }
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5136 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 54:
#line 615 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5145 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 58:
#line 628 "oracle.y" /* yacc.c:1646  */
    {
        (yyval.node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
        (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
    }
#line 5154 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 59:
#line 636 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                    (yyvsp[-6].node),             /* E_SELECT_DISTINCT 0 */
                    (yyvsp[-5].node),             /* E_SELECT_SELECT_EXPR_LIST 1 */
                    (yyvsp[-4].node),             /* E_SELECT_FROM_LIST 2 */
                    (yyvsp[-3].node),             /* E_SELECT_OPT_WHERE 3 */
                    (yyvsp[-1].node),             /* E_SELECT_GROUP_BY 4 */
                    (yyvsp[0].node),             /* E_SELECT_HAVING 5 */
                    nullptr,        /* E_SELECT_SET_OPERATION 6 */
                    nullptr,        /* E_SELECT_ALL_SPECIFIED 7 */
                    nullptr,        /* E_SELECT_FORMER_SELECT_STMT 8 */
                    nullptr,        /* E_SELECT_LATER_SELECT_STMT 9 */
                    nullptr,        /* E_SELECT_ORDER_BY 10 */
                    nullptr,             /* E_SELECT_LIMIT 11 */
                    nullptr,        /* E_SELECT_FOR_UPDATE 12 */
                    nullptr,        /* E_SELECT_HINTS 13 */
                    nullptr,        /* E_SELECT_WHEN 14 */
                    nullptr,        /* E_SELECT_OPT_TOP 15 */
                    nullptr,        /* E_SELECT_OPT_WITH 16 */
                    nullptr,        /* E_SELECT_OPT_OPTION 17 */
	                nullptr,         /* E_SELECT_OPT_INTO 18 */
	                (yyvsp[-2].node)             /*user for oracle */
                    );
    (yyval.node)->serialize_format = &SELECT_SERIALIZE_FORMAT_ORA;
}
#line 5184 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 60:
#line 662 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
          (yyvsp[-6].node),             /* E_SELECT_DISTINCT 0 */
          (yyvsp[-5].node),             /* E_SELECT_SELECT_EXPR_LIST 1 */
          (yyvsp[-4].node),             /* E_SELECT_FROM_LIST 2 */
          (yyvsp[-3].node),             /* E_SELECT_OPT_WHERE 3 */
          (yyvsp[-1].node),             /* E_SELECT_GROUP_BY 4 */
          (yyvsp[0].node),             /* E_SELECT_HAVING 5 */
          nullptr,        /* E_SELECT_SET_OPERATION 6 */
          nullptr,        /* E_SELECT_ALL_SPECIFIED 7 */
          nullptr,        /* E_SELECT_FORMER_SELECT_STMT 8 */
          nullptr,        /* E_SELECT_LATER_SELECT_STMT 9 */
          nullptr,        /* E_SELECT_ORDER_BY 10 */
          nullptr,        /* E_SELECT_LIMIT 11 */
          nullptr,        /* E_SELECT_FOR_UPDATE 12 */
          nullptr,        /* E_SELECT_HINTS 13 */
          nullptr,        /* E_SELECT_WHEN 14 */
          nullptr,        /* E_SELECT_OPT_TOP 15 */
          nullptr,        /* E_SELECT_OPT_WITH 16 */
          nullptr,        /* E_SELECT_OPT_OPTION 17 */
          nullptr,        /* E_SELECT_OPT_INTO 18 */
          (yyvsp[-2].node)         /* user for oracle*/
          );
    (yyval.node)->serialize_format = &SELECT_SERIALIZE_FORMAT2_ORA;
}
#line 5214 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 61:
#line 689 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5220 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 63:
#line 693 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5226 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 64:
#line 695 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHERE_CLAUSE, E_WHERE_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHERE_SERIALIZE_FORMAT;
}
#line 5235 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 65:
#line 704 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FROM_CLAUSE, E_FROM_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &FROM_SERIALIZE_FORMAT;
}
#line 5244 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 66:
#line 711 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5250 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 67:
#line 713 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_GROUP_BY, E_GROUP_BY_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &GROUP_BY_SERIALIZE_FORMAT;
}
#line 5259 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 69:
#line 722 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 5268 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 70:
#line 729 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"()"); }
#line 5274 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 72:
#line 732 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "GROUPING SETS");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5285 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 73:
#line 741 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5291 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 75:
#line 747 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ORDER_BY, E_ORDER_BY_PROPERTY_CNT, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &ORDER_BY_SERIALIZE_FORMAT;
}
#line 5300 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 76:
#line 752 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ORDER_BY, E_ORDER_BY_PROPERTY_CNT, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &ORDER_SIBLINGS_BY_SERIALIZE_FORMAT;
}
#line 5309 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 78:
#line 761 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SORT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 5318 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 79:
#line 769 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SORT_KEY, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 5327 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 80:
#line 777 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_ASC, "");
}
#line 5335 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 81:
#line 781 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_ASC, "ASC");
}
#line 5343 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 82:
#line 785 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_DESC, "DESC");
}
#line 5351 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 83:
#line 791 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5357 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 84:
#line 792 "oracle.y" /* yacc.c:1646  */
    {    (yyval.node) = Node::makeTerminalNode(E_STRING, "NULLS FIRST"); }
#line 5363 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 85:
#line 793 "oracle.y" /* yacc.c:1646  */
    {    (yyval.node) = Node::makeTerminalNode(E_STRING, "NULLS LAST"); }
#line 5369 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 86:
#line 797 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5375 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 87:
#line 799 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_HAVING, E_HAVING_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &HAVING_SERIALIZE_FORMAT;
}
#line 5384 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 88:
#line 809 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WITH_CLAUSE_SERIALIZE_FORMAT;
}
#line 5393 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 89:
#line 814 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WITH_RECURSIVE_CLAUSE_SERIALIZE_FORMAT;
}
#line 5402 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 91:
#line 823 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WITH_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 5411 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 92:
#line 832 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_COMMON_TABLE_EXPR, 5, (yyvsp[-5].node), (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMON_TABLE_EXPR_SERIALIZE_FORMAT_ORA;
}
#line 5420 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 93:
#line 839 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5426 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 95:
#line 845 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5435 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 97:
#line 854 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 5444 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 98:
#line 861 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5450 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 99:
#line 863 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_ALL, "ALL");
}
#line 5458 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 100:
#line 867 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_DISTINCT, "DISTINCT");
}
#line 5466 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 102:
#line 875 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 5475 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 103:
#line 883 "oracle.y" /* yacc.c:1646  */
    {
    if (!(yyvsp[0].node)) {
    	(yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, (yyvsp[-1].node));
    	(yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
    else {
	    Node* alias_node = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
        alias_node->serialize_format = &ORA_AS_SERIALIZE_FORMAT;

        (yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, alias_node);
        (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
}
#line 5493 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 106:
#line 902 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FROM_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 5502 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 110:
#line 916 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5511 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 111:
#line 928 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &ORA_AS_SERIALIZE_FORMAT;
}
#line 5520 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 112:
#line 933 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &ORA_AS_SERIALIZE_FORMAT;
}
#line 5529 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 113:
#line 938 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &ORA_AS_SERIALIZE_FORMAT;
}
#line 5538 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 114:
#line 943 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node), nullptr, nullptr, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &ORA_AS_SERIALIZE_FORMAT;
}
#line 5547 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 115:
#line 948 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[-1].node), nullptr, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &ORA_AS_SERIALIZE_FORMAT;
}
#line 5556 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 116:
#line 955 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_PARTITION_BY, 1, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &PARTITION_BY_SERIALIZE_FORMAT;
}
#line 5565 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 117:
#line 962 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "ONLY");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5576 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 118:
#line 969 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CONTAINERS");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5587 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 119:
#line 976 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SHARDS");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5598 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 120:
#line 985 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5604 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 122:
#line 992 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STAR, "*");
}
#line 5612 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 123:
#line 996 "oracle.y" /* yacc.c:1646  */
    {
    Node* star_node = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_ASTERISK_QUALIFY, 4, nullptr, nullptr, (yyvsp[-2].node), star_node);
    (yyval.node)->serialize_format = &ASTERISK_QUALIFY_SERIALIZE_FORMAT;
}
#line 5622 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 124:
#line 1002 "oracle.y" /* yacc.c:1646  */
    {
    Node* star_node = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_ASTERISK_QUALIFY, 4, nullptr, (yyvsp[-4].node), (yyvsp[-2].node), star_node);
    (yyval.node)->serialize_format = &ASTERISK_QUALIFY_SERIALIZE_FORMAT;
}
#line 5632 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 125:
#line 1008 "oracle.y" /* yacc.c:1646  */
    {
    Node* star_node = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_ASTERISK_QUALIFY, 4, (yyvsp[-6].node), (yyvsp[-4].node), (yyvsp[-2].node), star_node);
    (yyval.node)->serialize_format = &ASTERISK_QUALIFY_SERIALIZE_FORMAT;
}
#line 5642 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 126:
#line 1017 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 5652 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 127:
#line 1023 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 5662 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 128:
#line 1029 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 5672 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 129:
#line 1035 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 5682 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 130:
#line 1041 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), (yyvsp[-8].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 5692 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 131:
#line 1113 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 5701 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 132:
#line 1118 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 5710 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 133:
#line 1123 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 5719 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 134:
#line 1128 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, (yyvsp[-3].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 5728 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 135:
#line 1133 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 5737 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 136:
#line 1138 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, (yyvsp[-5].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 5746 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 137:
#line 1143 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, (yyvsp[-3].node), (yyvsp[-5].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 5755 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 138:
#line 1148 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, (yyvsp[-4].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 5764 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 143:
#line 1164 "oracle.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_CROSS, "CROSS");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-3].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &JOINED_TB_3_SERIALIZE_FORMAT;
}
#line 5774 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 144:
#line 1170 "oracle.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_CROSS, "OUTER");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-3].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &JOINED_TB_3_SERIALIZE_FORMAT;
}
#line 5784 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 145:
#line 1178 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 5793 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 146:
#line 1183 "oracle.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_INNER, "");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 5803 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 147:
#line 1189 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_USING_SERIALIZE_FORMAT;
}
#line 5812 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 148:
#line 1194 "oracle.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_INNER, "");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_USING_SERIALIZE_FORMAT;
}
#line 5822 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 149:
#line 1203 "oracle.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_CROSS, "CROSS");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-3].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &JOINED_TB_2_SERIALIZE_FORMAT;
}
#line 5832 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 150:
#line 1212 "oracle.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_NATURAL, "NATURAL " + (yyvsp[-2].node)->text());
    delete((yyvsp[-2].node));
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-4].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &JOINED_TB_2_SERIALIZE_FORMAT;
}
#line 5843 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 151:
#line 1219 "oracle.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_NATURAL, "NATURAL");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-3].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &JOINED_TB_2_SERIALIZE_FORMAT;
}
#line 5853 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 152:
#line 1228 "oracle.y" /* yacc.c:1646  */
    {
    if ((yyvsp[0].ival))
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_FULL, "FULL OUTER");
    }
    else
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_FULL, "FULL");
    }
}
#line 5868 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 153:
#line 1239 "oracle.y" /* yacc.c:1646  */
    {
    if ((yyvsp[0].ival))
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_LEFT, "LEFT OUTER");
    }
    else
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_LEFT, "LEFT");
    }
}
#line 5883 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 154:
#line 1250 "oracle.y" /* yacc.c:1646  */
    {
    if ((yyvsp[0].ival))
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_RIGHT, "RIGHT OUTER");
    }
    else
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_RIGHT, "RIGHT");
    }
}
#line 5898 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 155:
#line 1261 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_JOIN_INNER, "INNER");
}
#line 5906 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 156:
#line 1267 "oracle.y" /* yacc.c:1646  */
    { (yyval.ival) = 0; }
#line 5912 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 157:
#line 1268 "oracle.y" /* yacc.c:1646  */
    { (yyval.ival) = 1; /*this is a flag*/}
#line 5918 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 159:
#line 1275 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_OR, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_OR);
}
#line 5927 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 161:
#line 1284 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_AND, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_AND);
}
#line 5936 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 163:
#line 1293 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT);
}
#line 5945 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 165:
#line 1302 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 5954 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 166:
#line 1307 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 5963 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 168:
#line 1316 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5972 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 177:
#line 1345 "oracle.y" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "REGEXP_LIKE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5983 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 178:
#line 1354 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode((yyvsp[-1].nodetype), E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 5992 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 179:
#line 1362 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode((yyvsp[-1].nodetype), E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 6001 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 180:
#line 1370 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_BTW, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_BTW);
}
#line 6010 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 181:
#line 1375 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_BTW, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_BTW);
}
#line 6019 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 182:
#line 1383 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node), NULL);
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 6028 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 183:
#line 1388 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 6037 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 184:
#line 1393 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 6046 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 185:
#line 1398 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 6055 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 186:
#line 1406 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IN);
}
#line 6064 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 187:
#line 1411 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_IN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_IN);
}
#line 6073 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 188:
#line 1419 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 6082 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 189:
#line 1424 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 6091 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 191:
#line 1432 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NAN"); }
#line 6097 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 192:
#line 1433 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "INFINITE"); }
#line 6103 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 193:
#line 1437 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_EXISTS, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_EXISTS);
}
#line 6112 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 195:
#line 1447 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_COLLATE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 6121 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 196:
#line 1452 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_CNN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 6130 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 198:
#line 1461 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_ADD, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 6139 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 200:
#line 1470 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_MUL, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 6148 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 202:
#line 1479 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_POW, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_POW);
}
#line 6157 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 204:
#line 1488 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_POS, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_POS);
}
#line 6166 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 205:
#line 1493 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NEG, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NEG);
}
#line 6175 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 209:
#line 1504 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 6184 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 213:
#line 1515 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 6193 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 215:
#line 1524 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 6202 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 218:
#line 1535 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LE; }
#line 6208 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 219:
#line 1536 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LT; }
#line 6214 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 220:
#line 1537 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GE; }
#line 6220 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 221:
#line 1538 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GT; }
#line 6226 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 222:
#line 1539 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_EQ; }
#line 6232 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 223:
#line 1540 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 6238 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 224:
#line 1541 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GE; }
#line 6244 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 225:
#line 1542 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LE; }
#line 6250 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 226:
#line 1543 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 6256 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 227:
#line 1544 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 6262 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 228:
#line 1548 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_CNN; }
#line 6268 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 229:
#line 1549 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_CNN; }
#line 6274 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 230:
#line 1555 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.nodetype) = Node::comp_all_some_any_op_form((yyvsp[-1].nodetype), (yyvsp[0].ival));
}
#line 6282 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 231:
#line 1561 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_ADD; }
#line 6288 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 232:
#line 1562 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MINUS; }
#line 6294 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 233:
#line 1566 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MUL; }
#line 6300 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 234:
#line 1567 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_DIV; }
#line 6306 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 235:
#line 1568 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_REM; }
#line 6312 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 236:
#line 1569 "oracle.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MOD; }
#line 6318 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 244:
#line 1580 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "DEFAULT"); }
#line 6324 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 245:
#line 1581 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "DATE " + (yyvsp[0].node)->text()); delete((yyvsp[0].node)); }
#line 6330 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 246:
#line 1582 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING,  "TIMESTAMP " + (yyvsp[0].node)->text()); delete((yyvsp[0].node));  }
#line 6336 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 247:
#line 1588 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE, E_CASE_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_CASE);
}
#line 6345 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 248:
#line 1595 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 6351 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 251:
#line 1602 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SPACE_LIST_SERIALIZE_FORMAT;
}
#line 6360 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 252:
#line 1610 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 6369 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 253:
#line 1615 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 6378 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 254:
#line 1622 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 6384 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 255:
#line 1624 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_DEFAULT, E_CASE_DEFAULT_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &ELSE_SERIALIZE_FORMAT;
}
#line 6393 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 260:
#line 1641 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6403 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 261:
#line 1647 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6413 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 262:
#line 1653 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-5].node), (yyvsp[-3].node), (yyvsp[-2].node), nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 6423 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 263:
#line 1659 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-4].node),(yyvsp[-2].node), (yyvsp[-1].node), nullptr,  nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 6433 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 264:
#line 1665 "oracle.y" /* yacc.c:1646  */
    {
    if (!Node::CHECK_FUNCTION_CALL_WITH_STAR((yyvsp[-4].node)))
    {
        yyerror(&(yylsp[-4]), result, scanner, "error use *");
        YYABORT;
    }
    Node* star = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-4].node), star, (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6449 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 265:
#line 1677 "oracle.y" /* yacc.c:1646  */
    {
    if (!Node::CHECK_FUNCTION_CALL_WITH_STAR((yyvsp[-3].node)))
    {
        yyerror(&(yylsp[-3]), result, scanner, "error use *");
        YYABORT;
    }
    Node* star = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), star, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6465 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 266:
#line 1691 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "AVG"); }
#line 6471 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 267:
#line 1692 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MAX"); }
#line 6477 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 268:
#line 1693 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MIN"); }
#line 6483 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 269:
#line 1694 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SUM"); }
#line 6489 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 270:
#line 1695 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_POP"); }
#line 6495 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 271:
#line 1696 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_SAMP"); }
#line 6501 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 272:
#line 1697 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_POP"); }
#line 6507 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 273:
#line 1698 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_SAMP"); }
#line 6513 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 274:
#line 1699 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "COUNT"); }
#line 6519 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 275:
#line 1700 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "GROUPING"); }
#line 6525 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 276:
#line 1701 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV"); }
#line 6531 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 277:
#line 1706 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), nullptr, (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6541 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 278:
#line 1714 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RANK"); }
#line 6547 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 279:
#line 1715 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DENSE_RANK"); }
#line 6553 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 280:
#line 1716 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENT_RANK"); }
#line 6559 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 281:
#line 1717 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CUME_DIST"); }
#line 6565 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 282:
#line 1718 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW_NUMBER"); }
#line 6571 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 283:
#line 1723 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OVER "+ (yyvsp[0].node)->text()); delete((yyvsp[0].node));
}
#line 6579 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 284:
#line 1727 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OVER_CLAUSE, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &OVER_CLAUSE_SERIALIZE_FORMAT;
}
#line 6588 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 285:
#line 1736 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[-1].node); }
#line 6594 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 287:
#line 1745 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WINDOW_SPECIFIC, E_WINDOW_SPECIFIC_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WINDOW_SPECIFIC_CLAUSE_SERIALIZE_FORMAT;
}
#line 6603 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 288:
#line 1752 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 6609 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 290:
#line 1757 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 6615 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 291:
#line 1759 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 6621 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 292:
#line 1763 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 6627 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 293:
#line 1765 "oracle.y" /* yacc.c:1646  */
    {
    std::string s3 = (yyvsp[0].node) ? (yyvsp[0].node)->text() : "";
    (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, (yyvsp[-2].node)->text()+" "+(yyvsp[-1].node)->text()+" "+s3);
    delete((yyvsp[-2].node)); delete((yyvsp[-1].node)); delete((yyvsp[0].node));
}
#line 6637 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 294:
#line 1773 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"ROWS"); }
#line 6643 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 295:
#line 1774 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"RANGE"); }
#line 6649 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 298:
#line 1783 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED PRECEDING"); }
#line 6655 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 299:
#line 1784 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"CURRENT ROW"); }
#line 6661 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 301:
#line 1789 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,(yyvsp[-1].node)->text()+" PRECEDING"); delete((yyvsp[-1].node)); }
#line 6667 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 302:
#line 1794 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BETWEEN "+(yyvsp[-2].node)->text()+" AND "+(yyvsp[0].node)->text()); delete((yyvsp[-2].node)); delete((yyvsp[0].node)); }
#line 6673 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 304:
#line 1799 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED FOLLOWING"); }
#line 6679 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 306:
#line 1804 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,(yyvsp[-1].node)->text()+" FOLLOWING"); delete((yyvsp[-1].node)); }
#line 6685 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 307:
#line 1808 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 6691 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 308:
#line 1809 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE CURRENT ROW"); }
#line 6697 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 309:
#line 1810 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE GROUP"); }
#line 6703 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 310:
#line 1811 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE TIES"); }
#line 6709 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 311:
#line 1812 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE NO OTHERS"); }
#line 6715 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 312:
#line 1817 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-2].node), nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6725 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 313:
#line 1823 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6735 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 314:
#line 1835 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6745 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 315:
#line 1841 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "RIGHT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6756 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 316:
#line 1848 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "LEFT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6767 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 317:
#line 1855 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CAST");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-3].node), nullptr, nullptr, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &FUN_CALL_AS_SERIALIZE_FORMAT;
}
#line 6778 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 318:
#line 1862 "oracle.y" /* yacc.c:1646  */
    {
    Node* transcoding_name = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text());
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT");
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        		fun_name, (yyvsp[-3].node), nullptr, nullptr, transcoding_name);
    (yyval.node)->serialize_format = &FUN_CALL_USING_SERIALIZE_FORMAT;
}
#line 6791 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 319:
#line 1871 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6802 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 320:
#line 1878 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 6813 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 321:
#line 1885 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6824 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 322:
#line 1892 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 6835 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 323:
#line 1899 "oracle.y" /* yacc.c:1646  */
    {
    Node* expr_list = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node));
    expr_list->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, expr_list, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 6848 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 324:
#line 1908 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SESSION_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 6859 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 325:
#line 1915 "oracle.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SYSTEM_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 6870 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 330:
#line 1935 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 6879 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 331:
#line 1943 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 6888 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 332:
#line 1948 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 6897 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 333:
#line 1953 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 6906 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 334:
#line 1958 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 6915 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 335:
#line 1967 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "REF("+(yyvsp[-3].node)->text()+")SCOPE "+(yyvsp[0].node)->text());
    delete((yyvsp[-3].node));delete((yyvsp[0].node));
}
#line 6924 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 336:
#line 1975 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-4].node)->text()+" ARRAY("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-4].node));
    delete((yyvsp[-1].node));
}
#line 6934 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 337:
#line 1981 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" ARRAY");
    delete((yyvsp[-1].node));
}
#line 6943 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 338:
#line 1986 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" MULTISET");
    delete((yyvsp[-1].node));
}
#line 6952 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 339:
#line 1994 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-4].node)->text()+" CHARACTER SET "+(yyvsp[-1].node)->text()+(yyvsp[0].node)->text());
    delete((yyvsp[-4].node));delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6961 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 340:
#line 1999 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-3].node)->text()+" CHARACTER SET "+(yyvsp[0].node)->text());
    delete((yyvsp[-3].node));delete((yyvsp[0].node));
}
#line 6970 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 341:
#line 2004 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6979 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 343:
#line 2010 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6988 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 350:
#line 2024 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "INTERVAL "+(yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 6997 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 351:
#line 2032 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-2].node)->text()+" TO "+(yyvsp[0].node)->text());
    delete((yyvsp[-2].node));delete((yyvsp[0].node));
}
#line 7006 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 353:
#line 2041 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-3].node)->text()+"("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));
    delete((yyvsp[-1].node));
}
#line 7016 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 356:
#line 2052 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7025 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 357:
#line 2057 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND");
}
#line 7033 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 358:
#line 2064 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-3].node)->text()+"("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 7042 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 360:
#line 2070 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 7051 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 361:
#line 2075 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7060 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 362:
#line 2080 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND");
}
#line 7068 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 363:
#line 2087 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "YEAR");
}
#line 7076 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 364:
#line 2091 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "MONTH");
}
#line 7084 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 365:
#line 2095 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DAY");
}
#line 7092 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 366:
#line 2099 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "HOUR");
}
#line 7100 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 367:
#line 2103 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "MINUTE");
}
#line 7108 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 368:
#line 2110 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BOOLEAN");
}
#line 7116 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 369:
#line 2117 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DATE");
}
#line 7124 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 370:
#line 2121 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME("+(yyvsp[-4].node)->text()+") WITH TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 7133 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 371:
#line 2126 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME("+(yyvsp[-4].node)->text()+") WITHOUT TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 7142 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 372:
#line 2131 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7151 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 373:
#line 2136 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME WITH TIME ZONE");
}
#line 7159 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 374:
#line 2140 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME WITHOUT TIME ZONE");
}
#line 7167 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 375:
#line 2144 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP("+(yyvsp[-4].node)->text()+") WITH TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 7176 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 376:
#line 2149 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP("+(yyvsp[-4].node)->text()+") WITHOUT TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 7185 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 377:
#line 2154 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7194 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 378:
#line 2159 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP WITH TIME ZONE");
}
#line 7202 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 379:
#line 2163 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP WITHOUT TIME ZONE");
}
#line 7210 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 382:
#line 2175 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NUMERIC("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 7219 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 383:
#line 2180 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NUMERIC("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7228 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 384:
#line 2185 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 7237 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 385:
#line 2190 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7246 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 386:
#line 2195 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DEC("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 7255 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 387:
#line 2200 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DEC("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7264 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 388:
#line 2205 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DEC");
}
#line 7272 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 389:
#line 2209 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SMALLINT");
}
#line 7280 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 390:
#line 2213 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "INTEGER");
}
#line 7288 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 391:
#line 2217 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "INT");
}
#line 7296 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 392:
#line 2221 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BIGINT");
}
#line 7304 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 393:
#line 2225 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NUMERIC");
}
#line 7312 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 394:
#line 2229 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL");
}
#line 7320 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 395:
#line 2236 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FLOAT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7329 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 396:
#line 2241 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DOUBLE PRECISION");
}
#line 7337 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 397:
#line 2245 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FLOAT");
}
#line 7345 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 398:
#line 2249 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "REAL");
}
#line 7353 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 399:
#line 2256 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7362 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 400:
#line 2261 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7371 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 401:
#line 2266 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7380 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 402:
#line 2271 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7389 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 403:
#line 2276 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "VARCHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7398 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 404:
#line 2281 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7407 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 405:
#line 2286 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER LARGE OBJECT");
}
#line 7415 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 406:
#line 2290 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7424 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 407:
#line 2295 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR LARGE OBJECT");
}
#line 7432 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 408:
#line 2299 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CLOB("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7441 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 409:
#line 2304 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CLOB");
}
#line 7449 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 410:
#line 2308 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR");
}
#line 7457 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 411:
#line 2312 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER");
}
#line 7465 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 412:
#line 2319 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BINARY LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7474 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 413:
#line 2324 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BINARY LARGE OBJECT");
}
#line 7482 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 414:
#line 2328 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BLOB("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7491 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 415:
#line 2333 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BLOB");
}
#line 7499 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 416:
#line 2340 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7508 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 417:
#line 2345 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER");
}
#line 7516 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 418:
#line 2349 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7525 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 419:
#line 2354 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHAR");
}
#line 7533 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 420:
#line 2358 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7542 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 421:
#line 2363 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7551 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 422:
#line 2368 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHAR VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7560 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 423:
#line 2373 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7569 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 424:
#line 2378 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7578 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 425:
#line 2383 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER LARGE OBJECT");
}
#line 7586 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 426:
#line 2387 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7595 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 427:
#line 2392 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR LARGE OBJECT");
}
#line 7603 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 428:
#line 2396 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCLOB("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 7612 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 429:
#line 2401 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCLOB");
}
#line 7620 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 430:
#line 2405 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR");
}
#line 7628 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 431:
#line 2412 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-2].node)->text()+" "+(yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text());
    delete((yyvsp[-2].node));delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 7637 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 432:
#line 2417 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 7646 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 433:
#line 2422 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 7655 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 434:
#line 2427 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 7664 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 435:
#line 2435 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTERS");
}
#line 7672 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 436:
#line 2439 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CODE_UNITS");
}
#line 7680 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 437:
#line 2443 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "OCTETS");
}
#line 7688 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 438:
#line 2450 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "K");
}
#line 7696 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 439:
#line 2454 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "M");
}
#line 7704 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 440:
#line 2458 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "G");
}
#line 7712 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 441:
#line 2465 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_ALL, "ALL");
}
#line 7720 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 442:
#line 2469 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_DISTINCT, "DISTINCT");
}
#line 7728 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 443:
#line 2475 "oracle.y" /* yacc.c:1646  */
    { (yyval.ival) = 0; }
#line 7734 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 444:
#line 2476 "oracle.y" /* yacc.c:1646  */
    { (yyval.ival) = 1; }
#line 7740 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 445:
#line 2477 "oracle.y" /* yacc.c:1646  */
    { (yyval.ival) = 2; }
#line 7746 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 446:
#line 2481 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 7752 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 448:
#line 2486 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 7758 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 451:
#line 2497 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "COLLATE "+(yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 7767 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 456:
#line 2516 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "INT"); }
#line 7773 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 457:
#line 2517 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ARRAY"); }
#line 7779 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 458:
#line 2518 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BINARY"); }
#line 7785 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 459:
#line 2519 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIMESTAMP"); }
#line 7791 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 460:
#line 2520 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIME");  }
#line 7797 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 461:
#line 2521 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CHARACTERS"); }
#line 7803 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 462:
#line 2522 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NCHAR"); }
#line 7809 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 463:
#line 2526 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "INTERVAL"); }
#line 7815 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 464:
#line 2528 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CLOB"); }
#line 7821 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 465:
#line 2529 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BLOB"); }
#line 7827 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 466:
#line 2530 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NCLOB"); }
#line 7833 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 467:
#line 2531 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NUMERIC"); }
#line 7839 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 468:
#line 2532 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CHARACTER"); }
#line 7845 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 469:
#line 2533 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BIGINT"); }
#line 7851 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 470:
#line 2534 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DEC"); }
#line 7857 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 471:
#line 2535 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NATIONAL"); }
#line 7863 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 472:
#line 2536 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DOUBLE"); }
#line 7869 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 473:
#line 2537 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VARYING"); }
#line 7875 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 474:
#line 2538 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "REAL"); }
#line 7881 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 475:
#line 2543 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "K"); }
#line 7887 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 476:
#line 2544 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "M"); }
#line 7893 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 477:
#line 2545 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "G"); }
#line 7899 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 478:
#line 2546 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CAST"); }
#line 7905 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 479:
#line 2547 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CODE_UNITS"); }
#line 7911 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 480:
#line 2548 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CORRESPONDING"); }
#line 7917 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 481:
#line 2549 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FOLLOWING"); }
#line 7923 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 482:
#line 2550 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LARGE"); }
#line 7929 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 483:
#line 2551 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MULTISET"); }
#line 7935 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 484:
#line 2552 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OBJECT"); }
#line 7941 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 485:
#line 2553 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OCTETS"); }
#line 7947 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 486:
#line 2554 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ONLY"); }
#line 7953 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 487:
#line 2555 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECEDING"); }
#line 7959 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 488:
#line 2556 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECISION"); }
#line 7965 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 489:
#line 2557 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RECURSIVE"); }
#line 7971 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 490:
#line 2558 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "REF"); }
#line 7977 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 491:
#line 2559 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW"); }
#line 7983 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 492:
#line 2560 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SCOPE"); }
#line 7989 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 493:
#line 2561 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SECOND"); }
#line 7995 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 494:
#line 2562 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "UNBOUNDED"); }
#line 8001 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 495:
#line 2563 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VARCHAR"); }
#line 8007 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 496:
#line 2564 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "WITHOUT"); }
#line 8013 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 497:
#line 2565 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ZONE"); }
#line 8019 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 498:
#line 2567 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OF"); }
#line 8025 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 499:
#line 2568 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "READ"); }
#line 8031 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 500:
#line 2569 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DESC"); }
#line 8037 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 501:
#line 2570 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIES"); }
#line 8043 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 502:
#line 2571 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SETS"); }
#line 8049 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 503:
#line 2572 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OTHERS"); }
#line 8055 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 504:
#line 2573 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "EXCLUDE"); }
#line 8061 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 505:
#line 2574 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ASC"); }
#line 8067 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 506:
#line 2575 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE"); }
#line 8073 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 507:
#line 2576 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT"); }
#line 8079 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 508:
#line 2577 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF"); }
#line 8085 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 509:
#line 2578 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FIRST"); }
#line 8091 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 510:
#line 2579 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SAMPLE"); }
#line 8097 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 520:
#line 2598 "oracle.y" /* yacc.c:1646  */
    { (yyval.node)=NULL; }
#line 8103 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 521:
#line 2600 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = (yyvsp[0].node);
}
#line 8111 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 522:
#line 2607 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FOR_UPDATE, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &FOR_UPDATE_FORMAT_HANA;
}
#line 8120 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 523:
#line 2613 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8126 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 524:
#line 2615 "oracle.y" /* yacc.c:1646  */
    {
    Node * p_of = Node::makeTerminalNode(E_STRING, "OF");
    (yyval.node) = Node::makeNonTerminalNode(E_OF_COLUMS, 2, p_of, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 8136 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 526:
#line 2624 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8145 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 527:
#line 2630 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8151 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 528:
#line 2631 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "SKIP LOCKED");}
#line 8157 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 529:
#line 2634 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8163 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 530:
#line 2635 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_NOWAIT, "NOWAIT");}
#line 8169 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 531:
#line 2637 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WAIT_INT, 1,  (yyvsp[0].node));
    (yyval.node)->serialize_format = &WAIT_TIME_FORMAT;
}
#line 8178 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 532:
#line 2644 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_OFFSET_FETCH, 2,  (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 8187 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 533:
#line 2650 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "ROW");}
#line 8193 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 534:
#line 2651 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "ROWS");}
#line 8199 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 535:
#line 2654 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8205 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 540:
#line 2663 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8211 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 541:
#line 2665 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_OFFSET_ROW, 2,  (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &OFFSET_SERIALIZE_FORMAT;
}
#line 8220 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 542:
#line 2672 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "FIRST");}
#line 8226 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 543:
#line 2673 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "NEXT");}
#line 8232 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 544:
#line 2676 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "ONLY");}
#line 8238 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 545:
#line 2677 "oracle.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "WITH TIES");}
#line 8244 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 546:
#line 2680 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8250 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 547:
#line 2682 "oracle.y" /* yacc.c:1646  */
    {
    Node * name1 = nullptr;
    {
        std::string name = "FETCH "+(yyvsp[-3].node)->text();
        delete((yyvsp[-3].node));
        name1 = Node::makeTerminalNode(E_STRING, name);
    }
    Node * name2 = nullptr;
    {
        std::string name = (yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text();
        delete((yyvsp[-1].node));
        delete((yyvsp[0].node));
        name2 = Node::makeTerminalNode(E_STRING, name);
    }
    (yyval.node) = Node::makeNonTerminalNode(EORA_OFFSET_ROW, 3, name1, (yyvsp[-2].node), name2);
    (yyval.node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;

}
#line 8273 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 548:
#line 2701 "oracle.y" /* yacc.c:1646  */
    {
    Node * name1 = nullptr;
    {
        std::string name = "FETCH "+(yyvsp[-4].node)->text();
        delete((yyvsp[-4].node));
        name1 = Node::makeTerminalNode(E_STRING, name);
    }
    Node * name2 = nullptr;
    {
        std::string name =" PERCENT "+ (yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text();
        delete((yyvsp[-1].node));
        delete((yyvsp[0].node));
        name2 = Node::makeTerminalNode(E_STRING, name);
    }
    (yyval.node) = Node::makeNonTerminalNode(EORA_OFFSET_ROW, 3, name1, (yyvsp[-3].node), name2);
    (yyval.node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 8295 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 549:
#line 2721 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8301 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 551:
#line 2726 "oracle.y" /* yacc.c:1646  */
    {
    std::string name = "SEARCH DEPTH FIRST BY ";
    Node * prename  = Node::makeTerminalNode(E_STRING, name);
    (yyval.node) = Node::makeNonTerminalNode(EROA_SEARCH_CLAUSE, 5, prename, (yyvsp[-4].node), (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SEARCH_CLAUSE_SERIALIZE_FORMAT_ORA;
}
#line 8312 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 552:
#line 2733 "oracle.y" /* yacc.c:1646  */
    {
    std::string name = "SEARCH BREADTH FIRST BY ";
    Node * prename  = Node::makeTerminalNode(E_STRING, name);
    (yyval.node) = Node::makeNonTerminalNode(EROA_SEARCH_CLAUSE, 5, prename, (yyvsp[-4].node), (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SEARCH_CLAUSE_SERIALIZE_FORMAT_ORA;
}
#line 8323 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 553:
#line 2741 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8329 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 555:
#line 2747 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_CYCLE_CLAUSE, 4, (yyvsp[-6].node), (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &CYCLE_CLAUSE_SERIALIZE_FORMAT_ORA;
}
#line 8338 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 556:
#line 2753 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8344 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 557:
#line 2754 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "NOCYCLE"); }
#line 8350 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 558:
#line 2755 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "NOCYCLE PRIOR"); }
#line 8356 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 559:
#line 2756 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "PRIOR"); }
#line 8362 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 560:
#line 2760 "oracle.y" /* yacc.c:1646  */
    {
    std::string name = "CONNECT BY ";
    if((yyvsp[-1].node)){
        name += (yyvsp[-1].node)->text();
        delete((yyvsp[-1].node));
    }
    Node * prename  = Node::makeTerminalNode(E_STRING, name);
    (yyval.node) = Node::makeNonTerminalNode(EORA_CONNECT_BY, 2, prename, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 8377 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 561:
#line 2773 "oracle.y" /* yacc.c:1646  */
    {
    std::string name = "START WITH";
    Node * prename  = Node::makeTerminalNode(E_STRING, name);
    (yyval.node) = Node::makeNonTerminalNode(EORA_START_WITH, 2, prename, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 8388 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 562:
#line 2782 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_HIERARCHICAL_CLAUSE, 2, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 8397 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 563:
#line 2787 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_HIERARCHICAL_CLAUSE, 2, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 8406 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 564:
#line 2792 "oracle.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(EORA_HIERARCHICAL_CLAUSE, 2, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 8415 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 565:
#line 2800 "oracle.y" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_STRING, "SAMPLE BLOCK");
    (yyval.node) = Node::makeNonTerminalNode(EORA_SAMPLE_CLAUSE, 3, name, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SAMPLE_CLAUSE_SERIALIZE_FORMAT;
}
#line 8425 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 566:
#line 2806 "oracle.y" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_STRING, "SAMPLE");
    (yyval.node) = Node::makeNonTerminalNode(EORA_SAMPLE_CLAUSE, 3, name, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SAMPLE_CLAUSE_SERIALIZE_FORMAT;
}
#line 8435 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 567:
#line 2814 "oracle.y" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 8441 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;

  case 568:
#line 2816 "oracle.y" /* yacc.c:1646  */
    {
   (yyval.node) = Node::makeNonTerminalNode(EORA_SEED_VALUE, 1, (yyvsp[-1].node));
   (yyval.node)->serialize_format = &EORA_SEED_VALUE_SERIALIZE_FORMAT;
}
#line 8450 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
    break;


#line 8454 "sqlparser_oracle_bison.cpp" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (&yylloc, result, scanner, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (&yylloc, result, scanner, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, result, scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp, result, scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, result, scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, result, scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp, result, scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 2822 "oracle.y" /* yacc.c:1906  */

/*********************************
 ** Section 4: Additional C code
 *********************************/

/* empty */

