/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         HANA_STYPE
#define YYLTYPE         HANA_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         hana_parse
#define yylex           hana_lex
#define yyerror         hana_error
#define yydebug         hana_debug
#define yynerrs         hana_nerrs


/* Copy the first part of user declarations.  */
#line 1 "sqlparser_hana.yacc" /* yacc.c:339  */

/**
 * This Grammar is designed for hana.
 * https://github.com/Raphael2017/SQL/blob/master/sql-2003-2.bnf
 * sqlparser.y
 * defines sqlparser_hana_bison.h
 * outputs sqlparser_hana_bison.cpp
 *
 * Bison Grammar File Spec: http://dinosaur.compilertools.net/bison/bison_6.html
 *
 */
/*********************************
 ** Section 1: C Declarations
 *********************************/

#include "sqlparser_hana_bison.h"
#include "sqlparser_hana_flex.h"
#include "serialize_format.h"

#include <stdio.h>
#include <string.h>
//#include <strings.h>

/*
 * We provide parse error includes error message, first line, first column of error lex for debug
 */
int yyerror(YYLTYPE* llocp, ParseResult* result, yyscan_t scanner, const char *msg) {
    result->accept = false;
    result->errFirstLine = llocp->first_line;
    result->errFirstColumn = llocp->first_column;
    result->errDetail = msg;
	return 0;
}

#define YYSTYPE         HANA_STYPE
#define YYLTYPE         HANA_LTYPE


#line 113 "sqlparser_hana_bison.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sqlparser_hana_bison.h".  */
#ifndef YY_HANA_SQLPARSER_HANA_BISON_H_INCLUDED
# define YY_HANA_SQLPARSER_HANA_BISON_H_INCLUDED
/* Debug traces.  */
#ifndef HANA_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define HANA_DEBUG 1
#  else
#   define HANA_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define HANA_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined HANA_DEBUG */
#if HANA_DEBUG
extern int hana_debug;
#endif
/* "%code requires" blocks.  */
#line 45 "sqlparser_hana.yacc" /* yacc.c:355  */

// %code requires block

#include "node.h"

#line 157 "sqlparser_hana_bison.cpp" /* yacc.c:355  */

/* Token type.  */
#ifndef HANA_TOKENTYPE
# define HANA_TOKENTYPE
  enum hana_tokentype
  {
    HANA_NAME = 258,
    HANA_STRING = 259,
    HANA_INTNUM = 260,
    HANA_BOOL = 261,
    HANA_APPROXNUM = 262,
    HANA_NULLX = 263,
    HANA_UNKNOWN = 264,
    HANA_QUESTIONMARK = 265,
    HANA_UMINUS = 266,
    HANA_ALL = 267,
    HANA_AND = 268,
    HANA_ANY = 269,
    HANA_ARRAY = 270,
    HANA_AS = 271,
    HANA_ASC = 272,
    HANA_AVG = 273,
    HANA_BETWEEN = 274,
    HANA_BIGINT = 275,
    HANA_BINARY = 276,
    HANA_BLOB = 277,
    HANA_BOOLEAN = 278,
    HANA_BY = 279,
    HANA_CALL = 280,
    HANA_CASE = 281,
    HANA_CAST = 282,
    HANA_CHAR = 283,
    HANA_CHARACTER = 284,
    HANA_CHARACTERS = 285,
    HANA_CLOB = 286,
    HANA_CNNOP = 287,
    HANA_COALESCE = 288,
    HANA_CODE_UNITS = 289,
    HANA_COLLATE = 290,
    HANA_COMP_EQ = 291,
    HANA_COMP_GE = 292,
    HANA_COMP_GT = 293,
    HANA_COMP_LE = 294,
    HANA_COMP_LT = 295,
    HANA_COMP_NE = 296,
    HANA_CONVERT = 297,
    HANA_CORRESPONDING = 298,
    HANA_COUNT = 299,
    HANA_CROSS = 300,
    HANA_CUME_DIST = 301,
    HANA_CURRENT = 302,
    HANA_CURRENT_TIMESTAMP = 303,
    HANA_CURRENT_USER = 304,
    HANA_DATE = 305,
    HANA_DAY = 306,
    HANA_DEC = 307,
    HANA_DECIMAL = 308,
    HANA_DEFAULT = 309,
    HANA_DELETE = 310,
    HANA_DENSE_RANK = 311,
    HANA_DESC = 312,
    HANA_DISTINCT = 313,
    HANA_DOUBLE = 314,
    HANA_ELSE = 315,
    HANA_END = 316,
    HANA_END_P = 317,
    HANA_ESCAPE = 318,
    HANA_ERROR = 319,
    HANA_EXCEPT = 320,
    HANA_EXCLUDE = 321,
    HANA_EXISTS = 322,
    HANA_FLOAT = 323,
    HANA_FOLLOWING = 324,
    HANA_FOR = 325,
    HANA_FROM = 326,
    HANA_FULL = 327,
    HANA_G = 328,
    HANA_GROUP = 329,
    HANA_GROUPING = 330,
    HANA_HAVING = 331,
    HANA_HOUR = 332,
    HANA_IN = 333,
    HANA_INNER = 334,
    HANA_INSERT = 335,
    HANA_INT = 336,
    HANA_INTEGER = 337,
    HANA_INTERSECT = 338,
    HANA_INTERVAL = 339,
    HANA_MANY = 340,
    HANA_ONE = 341,
    HANA_EXACT = 342,
    HANA_INTO = 343,
    HANA_IS = 344,
    HANA_JOIN = 345,
    HANA_K = 346,
    HANA_LARGE = 347,
    HANA_LEFT = 348,
    HANA_LIKE = 349,
    HANA_M = 350,
    HANA_MAX = 351,
    HANA_MIN = 352,
    HANA_MINUTE = 353,
    HANA_MOD = 354,
    HANA_MONTH = 355,
    HANA_MULTISET = 356,
    HANA_NATIONAL = 357,
    HANA_NATURAL = 358,
    HANA_NCHAR = 359,
    HANA_NCLOB = 360,
    HANA_NO = 361,
    HANA_NOT = 362,
    HANA_NULLIF = 363,
    HANA_NUMERIC = 364,
    HANA_OBJECT = 365,
    HANA_OCTETS = 366,
    HANA_OF = 367,
    HANA_ON = 368,
    HANA_ONLY = 369,
    HANA_OR = 370,
    HANA_ORDER = 371,
    HANA_OTHERS = 372,
    HANA_OUTER = 373,
    HANA_OVER = 374,
    HANA_PARTITION = 375,
    HANA_PERCENT_RANK = 376,
    HANA_PRECEDING = 377,
    HANA_PRECISION = 378,
    HANA_RANGE = 379,
    HANA_RANK = 380,
    HANA_READ = 381,
    HANA_REAL = 382,
    HANA_RECURSIVE = 383,
    HANA_REF = 384,
    HANA_RIGHT = 385,
    HANA_ROW = 386,
    HANA_ROWS = 387,
    HANA_ROW_NUMBER = 388,
    HANA_SCOPE = 389,
    HANA_SECOND = 390,
    HANA_SELECT = 391,
    HANA_SESSION_USER = 392,
    HANA_SET = 393,
    HANA_SETS = 394,
    HANA_SMALLINT = 395,
    HANA_SOME = 396,
    HANA_STDDEV_POP = 397,
    HANA_STDDEV_SAMP = 398,
    HANA_SUM = 399,
    HANA_SYSTEM_USER = 400,
    HANA_THEN = 401,
    HANA_TIES = 402,
    HANA_TIME = 403,
    HANA_TIMESTAMP = 404,
    HANA_TO = 405,
    HANA_UNBOUNDED = 406,
    HANA_UNION = 407,
    HANA_UPDATE = 408,
    HANA_USING = 409,
    HANA_VALUES = 410,
    HANA_VARCHAR = 411,
    HANA_NVARCHAR = 412,
    HANA_VARYING = 413,
    HANA_VAR_POP = 414,
    HANA_VAR_SAMP = 415,
    HANA_WHEN = 416,
    HANA_WHERE = 417,
    HANA_WITH = 418,
    HANA_WITHOUT = 419,
    HANA_YEAR = 420,
    HANA_ZONE = 421,
    HANA_LIMIT = 422,
    HANA_OFFSET = 423,
    HANA_TOP = 424,
    HANA_WAIT = 425,
    HANA_NOWAIT = 426,
    HANA_HINT = 427,
    HANA_RANGE_RESTRICTION = 428,
    HANA_PARAMETERS = 429,
    HANA_UPSERT = 430,
    HANA_REPLACE = 431,
    HANA_PRIMARY = 432,
    HANA_KEY = 433,
    HANA_CONTAINS = 434,
    HANA_P_POINT = 435,
    HANA_SECONDDATE = 436,
    HANA_TINYINT = 437,
    HANA_SMALLDECIMAL = 438,
    HANA_TEXT = 439,
    HANA_BINTEXT = 440,
    HANA_ALPHANUM = 441,
    HANA_VARBINARY = 442,
    HANA_SHORTTEXT = 443,
    HANA_ORDINALITY = 444,
    HANA_UNNEST = 445,
    HANA_HISTORY = 446,
    HANA_OVERRIDING = 447,
    HANA_USER = 448,
    HANA_SYSTEM = 449,
    HANA_VALUE = 450,
    HANA_NULLS = 451,
    HANA_FIRST = 452,
    HANA_LAST = 453,
    HANA_CORR = 454,
    HANA_CORR_SPEARMAN = 455,
    HANA_MEDIAN = 456,
    HANA_STDDEV = 457,
    HANA_VAR = 458,
    HANA_STRING_AGG = 459,
    HANA_MEMBER = 460,
    HANA_SYSTEM_TIME = 461,
    HANA_APPLICATION_TIME = 462,
    HANA_TABLESAMPLE = 463,
    HANA_BERNOULLI = 464,
    HANA_RETURN = 465,
    HANA_LATERAL = 466,
    HANA_BINNING = 467,
    HANA_LAG = 468,
    HANA_LEAD = 469,
    HANA_RANDOM_PARTITION = 470,
    HANA_WEIGHTED_AVG = 471,
    HANA_EXTRACT = 472,
    HANA_FIRST_VALUE = 473,
    HANA_LAST_VALUE = 474,
    HANA_NTH_VALUE = 475,
    HANA_NTILE = 476,
    HANA_SERIES_FILTER = 477,
    HANA_LOCATE_REGEXPR = 478,
    HANA_OCCURRENCES_REGEXPR = 479,
    HANA_REPLACE_REGEXPR = 480,
    HANA_SUBSTRING_REGEXPR = 481,
    HANA_SUBSTR_REGEXPR = 482,
    HANA_PERCENTILE_CONT = 483,
    HANA_PERCENTILE_DISC = 484,
    HANA_TRIM = 485,
    HANA_WITHIN = 486,
    HANA_CUBIC_SPLINE_APPROX = 487,
    HANA_LINEAR_APPROX = 488,
    HANA_ROLLUP = 489,
    HANA_CUBE = 490,
    HANA_BEST = 491,
    HANA_SUBTOTAL = 492,
    HANA_BALANCE = 493,
    HANA_TOTAL = 494,
    HANA_MULTIPLE = 495,
    HANA_RESULTSETS = 496,
    HANA_PREFIX = 497,
    HANA_STRUCTURED = 498,
    HANA_RESULT = 499,
    HANA_OVERVIEW = 500,
    HANA_TEXT_FILTER = 501,
    HANA_FILL = 502,
    HANA_UP = 503,
    HANA_MATCHES = 504,
    HANA_SORT = 505,
    HANA_MINUS = 506,
    HANA_ROWCOUNT = 507,
    HANA_LOCKED = 508,
    HANA_IGNORE = 509,
    HANA_SHARE = 510,
    HANA_LOCK = 511,
    HANA_UTCTIMESTAMP = 512,
    HANA_COMMIT = 513,
    HANA_ID = 514,
    HANA_XML = 515,
    HANA_JSON = 516,
    HANA_RETURNS = 517,
    HANA_SERIES = 518,
    HANA_TABLE = 519,
    HANA_LIKE_REGEXPR = 520,
    HANA_FLAG = 521,
    HANA_LEADING = 522,
    HANA_TRAILING = 523,
    HANA_BOTH = 524,
    HANA_WEIGHT = 525,
    HANA_FULLTEXT = 526,
    HANA_LANGUAGE = 527,
    HANA_LINGUISTIC = 528,
    HANA_FUZZY = 529,
    HANA_PORTION = 530,
    HANA_START = 531,
    HANA_AFTER = 532,
    HANA_OCCURRENCE = 533
  };
#endif

/* Value type.  */
#if ! defined HANA_STYPE && ! defined HANA_STYPE_IS_DECLARED

union HANA_STYPE
{
#line 88 "sqlparser_hana.yacc" /* yacc.c:355  */

    struct Node* node;
    int    ival;
    NodeType nodetype;

#line 454 "sqlparser_hana_bison.cpp" /* yacc.c:355  */
};

typedef union HANA_STYPE HANA_STYPE;
# define HANA_STYPE_IS_TRIVIAL 1
# define HANA_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined HANA_LTYPE && ! defined HANA_LTYPE_IS_DECLARED
typedef struct HANA_LTYPE HANA_LTYPE;
struct HANA_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define HANA_LTYPE_IS_DECLARED 1
# define HANA_LTYPE_IS_TRIVIAL 1
#endif



int hana_parse (ParseResult* result, yyscan_t scanner);

#endif /* !YY_HANA_SQLPARSER_HANA_BISON_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 484 "sqlparser_hana_bison.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined HANA_LTYPE_IS_TRIVIAL && HANA_LTYPE_IS_TRIVIAL \
             && defined HANA_STYPE_IS_TRIVIAL && HANA_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  205
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   17689

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  295
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  229
/* YYNRULES -- Number of rules.  */
#define YYNRULES  747
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  1335

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   533

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint16 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   291,     2,     2,     2,   294,     2,     2,
      12,    13,   284,   289,   283,   290,    14,   293,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   287,   282,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   285,     2,   286,   288,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   292,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281
};

#if HANA_DEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   273,   273,   284,   285,   293,   294,   295,   296,   300,
     305,   310,   318,   319,   327,   332,   340,   341,   345,   349,
     353,   354,   355,   356,   357,   361,   362,   366,   367,   371,
     372,   376,   383,   390,   399,   408,   417,   426,   438,   447,
     460,   477,   494,   497,   528,   529,   537,   546,   557,   571,
     586,   587,   614,   641,   671,   672,   702,   703,   721,   728,
     731,   734,   736,   739,   768,   769,   778,   785,   786,   789,
     790,   798,   799,   807,   808,   809,   823,   824,   831,   843,
     844,   847,   848,   855,   856,   857,   860,   875,   876,   879,
     880,   881,   882,   885,   894,   897,   898,   901,   902,   903,
     906,   907,   908,   909,   946,   949,   953,   960,   961,   971,
     976,   984,   985,   994,  1002,  1003,  1007,  1015,  1016,  1023,
    1030,  1031,  1038,  1039,  1043,  1050,  1051,  1059,  1073,  1082,
    1083,  1092,  1093,  1094,  1098,  1099,  1110,  1115,  1120,  1125,
    1130,  1135,  1143,  1148,  1156,  1161,  1168,  1169,  1176,  1181,
    1188,  1189,  1190,  1191,  1192,  1193,  1194,  1197,  1198,  1202,
    1203,  1207,  1208,  1209,  1210,  1211,  1216,  1224,  1229,  1233,
    1238,  1242,  1248,  1254,  1260,  1271,  1277,  1283,  1290,  1296,
    1303,  1309,  1316,  1322,  1329,  1335,  1342,  1348,  1355,  1361,
    1368,  1374,  1385,  1390,  1395,  1400,  1405,  1410,  1415,  1420,
    1425,  1433,  1438,  1443,  1448,  1459,  1460,  1465,  1470,  1476,
    1496,  1503,  1504,  1511,  1518,  1525,  1532,  1533,  1541,  1567,
    1580,  1593,  1607,  1619,  1620,  1624,  1625,  1626,  1627,  1628,
    1629,  1630,  1631,  1632,  1633,  1637,  1645,  1646,  1654,  1663,
    1664,  1672,  1673,  1681,  1682,  1690,  1691,  1696,  1704,  1705,
    1722,  1723,  1724,  1725,  1726,  1728,  1729,  1730,  1731,  1734,
    1739,  1746,  1751,  1758,  1765,  1774,  1775,  1782,  1785,  1786,
    1792,  1793,  1799,  1800,  1808,  1809,  1815,  1821,  1828,  1834,
    1842,  1843,  1851,  1856,  1861,  1871,  1876,  1884,  1889,  1894,
    1899,  1907,  1912,  1920,  1925,  1933,  1942,  1943,  1948,  1956,
    1957,  1965,  1966,  1974,  1975,  1983,  1984,  1989,  1997,  1998,
    1999,  2000,  2001,  2002,  2005,  2012,  2013,  2021,  2022,  2027,
    2028,  2031,  2031,  2035,  2036,  2037,  2038,  2039,  2040,  2041,
    2042,  2043,  2044,  2048,  2049,  2054,  2061,  2062,  2066,  2067,
    2068,  2072,  2073,  2074,  2075,  2076,  2077,  2078,  2079,  2084,
    2092,  2093,  2097,  2098,  2106,  2111,  2119,  2120,  2129,  2130,
    2131,  2132,  2133,  2137,  2138,  2139,  2144,  2152,  2160,  2167,
    2173,  2183,  2184,  2185,  2186,  2187,  2188,  2189,  2190,  2191,
    2192,  2193,  2194,  2195,  2199,  2206,  2213,  2221,  2228,  2235,
    2242,  2249,  2250,  2260,  2270,  2277,  2290,  2301,  2310,  2311,
    2312,  2316,  2324,  2325,  2329,  2336,  2337,  2344,  2353,  2359,
    2369,  2370,  2371,  2372,  2373,  2374,  2375,  2376,  2377,  2378,
    2379,  2383,  2384,  2387,  2391,  2400,  2405,  2409,  2417,  2418,
    2420,  2429,  2430,  2435,  2436,  2445,  2446,  2450,  2451,  2455,
    2456,  2457,  2461,  2465,  2470,  2471,  2472,  2476,  2480,  2481,
    2482,  2483,  2484,  2488,  2494,  2504,  2505,  2513,  2514,  2517,
    2522,  2533,  2539,  2546,  2553,  2563,  2570,  2579,  2586,  2593,
    2600,  2607,  2616,  2623,  2632,  2639,  2647,  2648,  2655,  2656,
    2663,  2664,  2671,  2680,  2681,  2688,  2733,  2734,  2735,  2736,
    2737,  2738,  2739,  2740,  2741,  2742,  2743,  2744,  2745,  2746,
    2747,  2748,  2749,  2750,  2751,  2752,  2753,  2754,  2755,  2756,
    2757,  2758,  2759,  2760,  2761,  2762,  2763,  2766,  2767,  2768,
    2855,  2859,  2863,  2867,  2871,  2875,  3251,  3252,  3253,  3257,
    3258,  3262,  3263,  3267,  3272,  3281,  3282,  3283,  3284,  3285,
    3286,  3287,  3288,  3289,  3290,  3291,  3292,  3293,  3294,  3295,
    3296,  3297,  3298,  3299,  3300,  3301,  3302,  3303,  3304,  3305,
    3306,  3307,  3308,  3312,  3313,  3317,  3318,  3319,  3320,  3321,
    3322,  3323,  3324,  3325,  3326,  3327,  3328,  3329,  3330,  3331,
    3332,  3333,  3334,  3335,  3336,  3337,  3338,  3339,  3340,  3341,
    3342,  3343,  3344,  3345,  3346,  3347,  3348,  3349,  3350,  3351,
    3352,  3353,  3354,  3355,  3356,  3357,  3358,  3359,  3360,  3361,
    3362,  3363,  3364,  3365,  3366,  3367,  3368,  3369,  3370,  3371,
    3372,  3373,  3374,  3375,  3376,  3377,  3378,  3379,  3380,  3381,
    3382,  3385,  3386,  3387,  3388,  3389,  3390,  3391,  3392,  3393,
    3394,  3395,  3396,  3397,  3398,  3399,  3400,  3401,  3403,  3404,
    3405,  3406,  3407,  3408,  3409,  3412,  3413,  3414,  3415,  3416,
    3417,  3418,  3419,  3420,  3421,  3422,  3423,  3428,  3429,  3434,
    3459,  3460,  3464,  3467,  3472,  3473,  3474,  3478,  3479,  3487,
    3488,  3491,  3492,  3493,  3501,  3502,  3507,  3515,  3516,  3524,
    3525,  3532,  3533,  3536,  3537,  3545,  3546,  3557,  3568,  3584,
    3585,  3594,  3601,  3606,  3612,  3619,  3624,  3630,  3637,  3638,
    3639,  3642,  3649,  3657,  3665,  3672,  3680,  3681,  3688,  3694,
    3700,  3709,  3710,  3712,  3713,  3716,  3729,  3735,  3741,  3747,
    3753,  3761,  3796,  3797,  3798,  3801,  3802,  3808,  3809,  3815,
    3816,  3822,  3823,  3828,  3831,  3832,  3839,  3840
};
#endif

#if HANA_DEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NAME", "STRING", "INTNUM", "BOOL",
  "APPROXNUM", "NULLX", "UNKNOWN", "QUESTIONMARK", "UMINUS", "'('", "')'",
  "'.'", "ALL", "AND", "ANY", "ARRAY", "AS", "ASC", "AVG", "BETWEEN",
  "BIGINT", "BINARY", "BLOB", "BOOLEAN", "BY", "CALL", "CASE", "CAST",
  "CHAR", "CHARACTER", "CHARACTERS", "CLOB", "CNNOP", "COALESCE",
  "CODE_UNITS", "COLLATE", "COMP_EQ", "COMP_GE", "COMP_GT", "COMP_LE",
  "COMP_LT", "COMP_NE", "CONVERT", "CORRESPONDING", "COUNT", "CROSS",
  "CUME_DIST", "CURRENT", "CURRENT_TIMESTAMP", "CURRENT_USER", "DATE",
  "DAY", "DEC", "DECIMAL", "DEFAULT", "DELETE", "DENSE_RANK", "DESC",
  "DISTINCT", "DOUBLE", "ELSE", "END", "END_P", "ESCAPE", "ERROR",
  "EXCEPT", "EXCLUDE", "EXISTS", "FLOAT", "FOLLOWING", "FOR", "FROM",
  "FULL", "G", "GROUP", "GROUPING", "HAVING", "HOUR", "IN", "INNER",
  "INSERT", "INT", "INTEGER", "INTERSECT", "INTERVAL", "MANY", "ONE",
  "EXACT", "INTO", "IS", "JOIN", "K", "LARGE", "LEFT", "LIKE", "M", "MAX",
  "MIN", "MINUTE", "MOD", "MONTH", "MULTISET", "NATIONAL", "NATURAL",
  "NCHAR", "NCLOB", "NO", "NOT", "NULLIF", "NUMERIC", "OBJECT", "OCTETS",
  "OF", "ON", "ONLY", "OR", "ORDER", "OTHERS", "OUTER", "OVER",
  "PARTITION", "PERCENT_RANK", "PRECEDING", "PRECISION", "RANGE", "RANK",
  "READ", "REAL", "RECURSIVE", "REF", "RIGHT", "ROW", "ROWS", "ROW_NUMBER",
  "SCOPE", "SECOND", "SELECT", "SESSION_USER", "SET", "SETS", "SMALLINT",
  "SOME", "STDDEV_POP", "STDDEV_SAMP", "SUM", "SYSTEM_USER", "THEN",
  "TIES", "TIME", "TIMESTAMP", "TO", "UNBOUNDED", "UNION", "UPDATE",
  "USING", "VALUES", "VARCHAR", "NVARCHAR", "VARYING", "VAR_POP",
  "VAR_SAMP", "WHEN", "WHERE", "WITH", "WITHOUT", "YEAR", "ZONE", "LIMIT",
  "OFFSET", "TOP", "WAIT", "NOWAIT", "HINT", "RANGE_RESTRICTION",
  "PARAMETERS", "UPSERT", "REPLACE", "PRIMARY", "KEY", "CONTAINS",
  "P_POINT", "SECONDDATE", "TINYINT", "SMALLDECIMAL", "TEXT", "BINTEXT",
  "ALPHANUM", "VARBINARY", "SHORTTEXT", "ORDINALITY", "UNNEST", "HISTORY",
  "OVERRIDING", "USER", "SYSTEM", "VALUE", "NULLS", "FIRST", "LAST",
  "CORR", "CORR_SPEARMAN", "MEDIAN", "STDDEV", "VAR", "STRING_AGG",
  "MEMBER", "SYSTEM_TIME", "APPLICATION_TIME", "TABLESAMPLE", "BERNOULLI",
  "RETURN", "LATERAL", "BINNING", "LAG", "LEAD", "RANDOM_PARTITION",
  "WEIGHTED_AVG", "EXTRACT", "FIRST_VALUE", "LAST_VALUE", "NTH_VALUE",
  "NTILE", "SERIES_FILTER", "LOCATE_REGEXPR", "OCCURRENCES_REGEXPR",
  "REPLACE_REGEXPR", "SUBSTRING_REGEXPR", "SUBSTR_REGEXPR",
  "PERCENTILE_CONT", "PERCENTILE_DISC", "TRIM", "WITHIN",
  "CUBIC_SPLINE_APPROX", "LINEAR_APPROX", "ROLLUP", "CUBE", "BEST",
  "SUBTOTAL", "BALANCE", "TOTAL", "MULTIPLE", "RESULTSETS", "PREFIX",
  "STRUCTURED", "RESULT", "OVERVIEW", "TEXT_FILTER", "FILL", "UP",
  "MATCHES", "SORT", "MINUS", "ROWCOUNT", "LOCKED", "IGNORE", "SHARE",
  "LOCK", "UTCTIMESTAMP", "COMMIT", "ID", "XML", "JSON", "RETURNS",
  "SERIES", "TABLE", "LIKE_REGEXPR", "FLAG", "LEADING", "TRAILING", "BOTH",
  "WEIGHT", "FULLTEXT", "LANGUAGE", "LINGUISTIC", "FUZZY", "PORTION",
  "START", "AFTER", "OCCURRENCE", "';'", "','", "'*'", "'['", "']'", "':'",
  "'^'", "'+'", "'-'", "'!'", "'|'", "'/'", "'%'", "$accept", "sql_stmt",
  "stmt_list", "stmt", "call_stmt", "sql_argument_list", "sql_argument",
  "value_expression", "sp_name", "dql_stmt", "dml_stmt",
  "opt_partition_rest", "opt_column_ref_list_with_parens",
  "overriding_value", "insert_stmt", "from_constructor", "delete_stmt",
  "update_stmt", "delete_table", "update_table", "update_elem_list",
  "update_elem", "select_stmt", "query_expression",
  "query_expression_body", "query_term", "query_primary",
  "select_with_parens", "subquery", "sap_table_subquery", "table_subquery",
  "row_subquery", "simple_table", "opt_where", "from_clause",
  "opt_from_clause", "opt_groupby", "grouping_element_list",
  "grouping_element", "row_order_by_list", "row_order_by",
  "column_ref_perens", "group_set_name", "grouping_option",
  "grouping_best", "grouping_subtotal", "grouping_prefix_tb",
  "grouping_prefix", "grouping_structured_res", "grouping_text_filter",
  "opt_asc_desc", "opt_having", "with_clause", "with_list",
  "common_table_expr", "opt_derived_column_list",
  "simple_ident_list_with_parens", "simple_ident_list",
  "column_ref_list_with_parens", "column_ref_list", "opt_distinct",
  "select_expr_list", "projection", "from_list", "table_reference",
  "table_primary", "table_primary_non_join", "lateral_table",
  "associated_table", "associated_ref_list", "associated_ref",
  "opt_many2one_part", "opt_search_condition", "opt_tablesample",
  "opt_table_qualify", "collection_derived_table",
  "collection_value_expr_list", "collection_value_expr", "as_derived_part",
  "column_ref", "relation_factor", "func_relation_factor", "joined_table",
  "qualified_join", "case_join", "case_join_when_list", "case_join_when",
  "ret_join_on", "select_expr_list_parens", "opt_case_join_else",
  "cross_join", "join_type", "join_outer", "join_cardinality",
  "hana_construct_table", "construct_column_list", "construct_column",
  "search_condition", "boolean_term", "boolean_factor", "boolean_test",
  "boolean_primary", "predicate", "like_regexpr_redicate",
  "member_of_predicate", "bool_function", "contains_param3", "search_type",
  "search_param_list", "search_param", "expr_const_list",
  "comparison_predicate", "between_predicate", "like_predicate",
  "in_predicate", "null_predicate", "exists_predicate", "row_expr",
  "factor0", "factor1", "factor2", "factor3", "factor4",
  "row_expr_list_parens", "row_expr_list", "row_expr_star", "in_expr",
  "truth_value", "comp_op", "cnn_op", "comp_all_some_any_op",
  "plus_minus_op", "star_div_percent_mod_op", "expr_const", "case_expr",
  "case_arg", "when_clause_list", "when_clause", "case_default",
  "func_expr", "opt_nulls", "aggregate_expression", "aggregate_name",
  "sap_specific_function", "trim_char", "within_group",
  "opt_order_by_clause", "order_by_clause", "order_by_expression_list",
  "order_by_expression", "ranking_windowed_function",
  "ranking_function_name", "opt_over_clause", "over_clause",
  "window_specification", "window_name", "window_specification_details",
  "opt_existing_window_name", "opt_window_partition_clause",
  "opt_window_frame_clause", "window_frame_units", "window_frame_extent",
  "window_frame_start", "window_frame_preceding", "window_frame_between",
  "window_frame_bound", "window_frame_following",
  "opt_window_frame_exclusion", "scalar_function",
  "table_function_param_list", "table_function_param", "expr_point_val",
  "function_call_keyword", "for_xml", "for_json", "opt_returns_clause",
  "opt_option_string_list_p", "option_string_list", "option_string",
  "data_type", "array_type", "predefined_type", "lob_data_type",
  "primary_datetime_field", "all_some_any", "opt_as_label", "as_label",
  "label", "collate_clause", "name_r", "name_f", "reserved", "top_clause",
  "opt_for_update", "for_share_lock", "for_update", "opt_of_ident_list",
  "opt_ignore_lock", "wait_nowait", "limit_num", "offset_num",
  "limit_total", "with_hint_param", "with_hint_param_list", "hint_clause",
  "opt_with_range_restrict", "with_primary_key", "upsert_stmt",
  "replace_stmt", "for_system_time", "sys_as_of_spec", "sys_from_to_spec",
  "sys_between_spec", "for_application_time", "partition_restriction",
  "intnum_list", "tablesample_num", "opt_partition_restriction",
  "opt_for_application_time_clause", "for_application_time_clause",
  "regexpr_str_function", "regex_parameter", "opt_st_or_af", "opt_flag",
  "opt_with_string", "opt_from_position", "opt_occurrence_num",
  "opt_group_num", "param_num", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,    40,    41,    46,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303,   304,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,   316,   317,   318,   319,   320,   321,
     322,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,   338,   339,   340,   341,
     342,   343,   344,   345,   346,   347,   348,   349,   350,   351,
     352,   353,   354,   355,   356,   357,   358,   359,   360,   361,
     362,   363,   364,   365,   366,   367,   368,   369,   370,   371,
     372,   373,   374,   375,   376,   377,   378,   379,   380,   381,
     382,   383,   384,   385,   386,   387,   388,   389,   390,   391,
     392,   393,   394,   395,   396,   397,   398,   399,   400,   401,
     402,   403,   404,   405,   406,   407,   408,   409,   410,   411,
     412,   413,   414,   415,   416,   417,   418,   419,   420,   421,
     422,   423,   424,   425,   426,   427,   428,   429,   430,   431,
     432,   433,   434,   435,   436,   437,   438,   439,   440,   441,
     442,   443,   444,   445,   446,   447,   448,   449,   450,   451,
     452,   453,   454,   455,   456,   457,   458,   459,   460,   461,
     462,   463,   464,   465,   466,   467,   468,   469,   470,   471,
     472,   473,   474,   475,   476,   477,   478,   479,   480,   481,
     482,   483,   484,   485,   486,   487,   488,   489,   490,   491,
     492,   493,   494,   495,   496,   497,   498,   499,   500,   501,
     502,   503,   504,   505,   506,   507,   508,   509,   510,   511,
     512,   513,   514,   515,   516,   517,   518,   519,   520,   521,
     522,   523,   524,   525,   526,   527,   528,   529,   530,   531,
     532,   533,    59,    44,    42,    91,    93,    58,    94,    43,
      45,    33,   124,    47,    37
};
# endif

#define YYPACT_NINF -1048

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-1048)))

#define YYTABLE_NINF -565

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     460,   158,  8986,   -25,    65,   -10,   -10, 16084,  8986,  8986,
     167,   110,  -103, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048,    40,   173, -1048, -1048,    51, -1048, -1048,   205, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048, -1048, 16349,    69, -1048, -1048, -1048,
   -1048,   265, -1048,  8986,   254,  8986,   163,   169,  8986, 16349,
     -20, -1048,   271,   229,   229, -1048, -1048,   460,   169,   380,
     169,   169,   197, -1048,   169,    40, -1048, -1048,  3564,   416,
   -1048, 13608,   256, -1048,   300,  8986,   300, -1048, -1048, -1048,
   -1048,  5868,   306,   -20, 16349, 16349,   422, -1048, 13052, -1048,
     293, -1048,   293, -1048,    51,  7596,    51,    51,   340,   381,
      51,   197,   444, -1048, -1048, -1048, -1048, -1048, -1048,  3276,
     292,  7596,   459,   461,   464,   469,   478,   481, -1048,   492,
     489,   517,  4716,   526,   532, -1048, -1048,   500,   539,   544,
     564,   572,   577,   588,   598,   602,   606,   613,   615,   617,
     625,   629,   647,   659,   661,  7596,  7596,    15, -1048,   656,
   -1048, -1048, -1048, -1048,   668,   440,   674, -1048, -1048,   592,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
     249,   222,   238,   404, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048,   687, -1048, -1048,   692, -1048, -1048,   694,   695, -1048,
     696, -1048,   698,   700,   702, 13886,   701,  4716,   292,   705,
   -1048, 14151,   256, 12774, -1048,  5004, -1048,   -23, -1048,  2997,
    7596, -1048,    22, -1048,   489,  7596,   301,   438,   173,    90,
     424, -1048,   173,   173,   548,   548,   140, -1048, -1048,   292,
   -1048, -1048, -1048,   381,  3276,   707,    96,   249,    26, -1048,
     187,   558,  7596,   718,  7596,  7596,  5292,   719, -1048,   158,
   -1048, -1048,  7596, -1048,   249,  7596,  6156, -1048,  6444,  7596,
    7596,   514,  7596,  7596,  7596,  7596,  7596,   236,   721,   721,
     721,   721,   542,   542,  7884, -1048, -1048,   292,  4716,   896,
    6732,  4716,  4716,   155,  7596, -1048,  8986, -1048, -1048,   688,
   -1048,   201, -1048,   714,   157,  7596,   358,   614,  7596,   689,
     441,  5580,  7596,   714, -1048, -1048, -1048,  7596, -1048, -1048,
   -1048,  7596,  7596,   169,  7020,  7596,  9264, 12218,   728,   730,
   16349,   722, 14429,   440, -1048,   732, 16349,   662, -1048, -1048,
   -1048,   292, 13052,   454,   293,   158, -1048,   158,   239,  5004,
     187,  8443,  5868,   256, -1048,    -7, -1048,   114, -1048, 16349,
   -1048,   456,   566, -1048,   581,   569, -1048, -1048, -1048,   549,
    7596,   745,   509,   509,   638,   495,   743,   743, -1048,   292,
     744, -1048, -1048, -1048,  7596,  4716,    24, -1048,    31,   746,
      30,    81,  7596,   747,    18,   748,   139,   143, -1048,    33,
   -1048,   187,   473,   -85,   187,    59, -1048, -1048,    45,   684,
     128,   128,   -85,    75,    91, -1048, -1048,   721,   493,   751,
     758,   759,   760,   500,   764,   767,   779,   780,   785,    19,
     788, -1048, -1048, -1048, -1048, -1048, -1048, -1048,   781, -1048,
     782, -1048, -1048, -1048, -1048, -1048, -1048, -1048,   784,   786,
   -1048, -1048, -1048, -1048, -1048,   793,   794,   798, -1048, -1048,
     797, -1048, -1048,    92,   674, -1048, -1048, -1048,   311, -1048,
      64, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,   808,
     151,  7596,   714,  7596,   706,  7596,   168, -1048, -1048, -1048,
     805, -1048,   187, -1048,   222, -1048, -1048,   238,   404, -1048,
    7596,   703,    94,    23, 14707, -1048,   809, -1048,   385, -1048,
     815, -1048,   100,   817,   783, -1048, 16349, 16349,   819, -1048,
     101, -1048,   556, -1048, -1048, -1048,   102, -1048,   826,   643,
     644,   292, -1048, -1048,   293, -1048,   158,   824,  8165,   832,
     833, 14151, -1048,   563,   565, -1048, -1048, 14151, 14151, 14151,
      84, -1048, -1048, -1048, -1048, 14151,   835, -1048,   770,  7596,
   -1048,   256,  7596, -1048,   671,   675, -1048,   672,   421, -1048,
   -1048, -1048,   599, -1048, -1048, 16349,   458, -1048,   851,   591,
     591, -1048,   435,   187,    67,    95,  7596, -1048,   795,   896,
   -1048, -1048,  8986,   105,   703, -1048, -1048,  7596,  7596, -1048,
     853,   845,   703,  7596, 12496,  7596,   847,   848,   849,   703,
     703,   850,   860,   787, -1048, -1048, -1048, -1048,   631,   631,
   -1048,  7596,   792,   862,   864,   866,   867,   868,   870,   874,
   -1048, -1048, -1048,  7596, -1048,  7596,    68, -1048,   177,  7596,
     187,   876,    27, 14985, -1048,   703, -1048, 16349,   869,  9542,
   16614,   292, 12218,   292,   877, -1048, -1048, 16349, -1048,   880,
     772, -1048, 16349,  9820, -1048, -1048, -1048,   292, -1048, 16349,
    8165,   565,   881, 16879, 13330,   271,  8443,   789,   774,   508,
    8443,   774,   774,   184, -1048, -1048, -1048,   297,  4716, 16349,
   14151, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048, 15263,
     871,   814, -1048,   292,   187, -1048,   890, -1048, -1048, -1048,
   -1048,   622,   901, -1048,   651,   873,   107, -1048,   896, -1048,
   -1048,  7596,  7596,   187, -1048,   900,   902, -1048, -1048, -1048,
      35,    39,   108, -1048, -1048, -1048, -1048, -1048,    47,   703,
     703,   703, -1048, -1048, -1048, -1048,  7596,   843,   703,   703,
      62,  7596,   109,   910,   911,   912,   916,   920,   921,   187,
     187,  7596,  7596,   187, -1048,   703, 17144, -1048, -1048, -1048,
   -1048,   895, 10363, 16349, -1048,   923, -1048,   924, -1048, -1048,
   -1048,   922, -1048, -1048,   729, -1048,   926, -1048,   929, -1048,
     930,   113, -1048, -1048,   931,   932, -1048,   565,  8721, -1048,
     508,   790,   800,   857, -1048,   527,   508,   508,   855,  8443,
   -1048,   207,   935,   -40,   936, -1048,   666,   749,   941,   169,
    4716, -1048, -1048,   952, -1048,   708, -1048,   953, -1048,   851,
   -1048,   187,   187, -1048, -1048, -1048, -1048, -1048,   209, -1048,
   -1048, -1048, -1048,   186,   947, -1048, -1048, -1048,    63, -1048,
     956, -1048, -1048, -1048, -1048, -1048, -1048,   187,   187, -1048,
     699, -1048,   949,   840, 10628, -1048,   951,   954, 10893, 16614,
     292,   897, 10098, 16349,  7596,   136, 16879, -1048, -1048, -1048,
   -1048,   518,   546,   816,  4716, -1048, -1048,   806,   537,   858,
     968,   970,   861,   690, 16349,  4716,    52, -1048, -1048, 15541,
    3852,   440,   962, -1048, -1048, -1048,   966,   967,   971,   972,
     973,   974,   969,   371,   704, -1048,   984,   915,   872, -1048,
     977,  8986, -1048,   965,   872, -1048, -1048, 11158, 11423, -1048,
     980, -1048,   981, -1048,   992,   983,   119, -1048,   896,   127,
     995,   811, -1048, -1048, -1048, -1048,   919, -1048, -1048,   927,
     574,   440,  4716,    88, -1048,  4716,   997,   989,   856,  1008,
   16349, -1048,   863,   440,  1010,  1001,  1005,  1004,  4140,   879,
   -1048, -1048,   736, -1048,   791,   187, -1048,  1018,   542,   121,
    1019,  1020,  7596, -1048,   704,   371, -1048,   420,   750,  1014,
   -1048, -1048,  7596,   118, -1048, -1048, -1048, -1048, 11688, 17409,
     875, 11953, -1048, 16349, -1048, -1048,  1017,  1015, -1048, -1048,
   -1048, -1048,   944,   152,   822, -1048,   976,   440, -1048,  1037,
    1038, -1048,   936,   361,   763,  1032,  1045,  1046, 16614, -1048,
   -1048,  3852,  1048,  1042,   197,  1044,   129, -1048,  1047,  1050,
    1051,  1055,   130, -1048, -1048, -1048, -1048,   414,   993, -1048,
     456, -1048, -1048, -1048,   520, -1048, -1048, -1048, -1048,  1054,
   -1048, 16349,  1058, -1048,   822,  1057, -1048, -1048, -1048, -1048,
     925,   937,   568, -1048, -1048,  1059,  1060, -1048, -1048, 15819,
     908, -1048, -1048,   542, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048,   420, -1048,   529,   942,   950,   963,  1023, -1048, -1048,
   -1048, -1048,   132,  1065, -1048,  5868,  1027,   580,   589,   986,
     996, -1048, -1048,  7308,   133, -1048, -1048, -1048, -1048,   425,
     844, -1048, -1048,    86,   189, -1048,  1078, -1048, -1048, -1048,
   -1048,   350, -1048, -1048, 16349,   134,  8443,  1009,  1011,  1012,
    1013, -1048, -1048,  4428, -1048,   872,   131, -1048, 15819, -1048,
   -1048, -1048,  1093,   190, -1048, -1048,   529,   975, -1048,   987,
   -1048,   135, -1048,   670, -1048, -1048, -1048, -1048,  1095, -1048,
     865,   878,   883, -1048,   882, -1048, -1048, -1048, -1048,  4716,
   -1048,   859, -1048,   945,  1108, -1048,   440,   884,   885, -1048,
     886, -1048,   960,   948, -1048
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       5,     0,     0,     0,     0,   667,   667,     0,     0,     0,
       0,     0,     3,     8,     6,     7,    22,    21,    20,    19,
      47,   402,    50,    54,    56,     0,    23,    24,     0,   535,
     562,   569,   601,   377,   570,   571,   545,   572,   602,   573,
     603,   574,   556,   413,   522,   605,   411,   596,   607,   600,
     575,   568,   523,   576,   609,   610,   628,   566,   577,   567,
     375,   373,   524,   608,   521,   578,   630,   604,   579,   580,
     592,   581,   599,   412,   582,   583,   410,   593,   584,   585,
     586,   414,   587,   525,   598,   380,   382,   376,   597,   595,
     594,   606,   588,   589,   381,   383,   590,   520,   591,   622,
     621,   611,   612,   546,   613,   614,   615,   616,   617,   618,
     619,   620,   657,   623,   624,   626,   625,   627,   662,   663,
     371,   372,   374,   378,   379,   544,   629,   547,   415,   416,
     417,   418,   548,   549,   550,   551,   552,   539,   540,   541,
     542,   543,   553,   554,   555,   419,   420,   631,   632,   633,
     634,   635,   636,   637,   638,   639,   640,   641,   642,   643,
     644,   645,   646,   647,   648,   649,   650,   651,   652,   654,
     653,   655,   656,   658,   659,   660,   661,   557,   558,   559,
     560,   561,   664,   665,   666,     0,   695,    18,   537,   538,
     565,   192,   536,     0,     0,     0,     0,   122,     0,   584,
     109,   111,   114,   114,   114,     1,     2,     5,   122,     0,
     122,   122,   684,   403,   122,   402,    57,   200,     0,     0,
      11,     0,    64,    42,   721,     0,    25,   668,   669,   123,
     124,     0,     0,   110,     0,     0,     0,   115,     0,   704,
       0,   707,     0,     4,     0,     0,     0,     0,     0,   670,
       0,   684,   535,   341,   344,   342,   345,   343,   346,     0,
     695,   350,   571,   545,   602,   603,   556,   468,   470,     0,
       0,     0,     0,   604,     0,   472,   473,   594,   546,   544,
     547,   548,   549,   550,   551,   552,     0,   539,   540,   541,
     542,   543,   553,   554,   555,     0,     0,     0,    12,    14,
      59,    62,   310,   308,     0,    16,   239,   241,   243,   245,
     248,   258,   257,   256,   250,   251,   253,   252,   254,   255,
      17,   296,   299,   301,   303,   305,   311,   309,   312,   313,
     361,   537,   362,   360,   538,   359,   358,   565,   175,   201,
     536,   391,     0,     0,     0,     0,   193,     0,   695,     0,
     722,   529,    64,    27,    26,     0,   128,     0,   125,   529,
       0,   112,     0,   117,     0,     0,    64,    64,    52,   104,
     404,   405,    51,    53,   687,   687,     0,   675,   676,   695,
     674,   671,    55,   670,     0,     0,     0,   315,     0,    10,
     351,     0,     0,     0,     0,     0,     0,     0,   347,     0,
      61,   295,     0,   244,     0,     0,     0,   348,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   732,     0,     0,
       0,     0,     0,     0,     0,   306,   307,   695,     0,     0,
       0,     0,     0,     0,     0,   333,     0,   327,   325,   326,
     323,   324,   328,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   297,   336,   337,     0,   338,   339,
     340,     0,     0,   122,     0,     0,     0,     0,     0,     0,
       0,   195,     0,    65,    39,     0,     0,   723,   530,   532,
     533,   695,     0,   624,     0,     0,    32,    28,    27,     0,
     315,     0,     0,    64,   127,    67,    44,     0,   116,     0,
     113,    38,     0,   702,   699,     0,   705,   105,   106,   363,
       0,     0,   689,   689,   677,     0,   478,   478,    48,   695,
       0,    58,   249,   314,     0,     0,   356,   352,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   462,     0,
     318,   317,     0,   402,   458,     0,   455,   457,   175,     0,
     402,   402,   402,     0,     0,   733,   734,     0,   735,     0,
       0,     0,     0,     0,     0,     0,   659,   660,   661,     0,
       0,     9,    13,   494,   517,   515,   518,   486,   510,   497,
     513,   493,   492,   519,   496,   491,   487,   489,   500,   502,
     488,   490,   495,   498,   499,   504,   506,   508,    15,   484,
     483,   516,   453,     0,   240,   242,   321,   322,     0,   246,
       0,   534,   329,   330,   331,   319,   320,   291,   293,     0,
     287,     0,     0,     0,     0,     0,   260,   332,   334,   526,
     562,   527,   284,   335,   298,   282,   283,   300,   302,   304,
       0,     0,     0,     0,     0,   177,   176,   202,   691,   692,
       0,   693,     0,     0,     0,   199,     0,     0,   194,   716,
       0,   531,     0,    43,   724,    40,     0,   120,   175,     0,
       0,   695,    35,    34,     0,    31,    28,     0,     0,     0,
       0,   529,    60,    66,   129,   131,   134,   529,   529,   529,
     161,   132,   205,   206,   133,   529,   192,   126,    69,     0,
      68,    64,     0,   118,     0,     0,   703,     0,     0,   407,
     406,   688,     0,   685,   686,     0,   681,   672,     0,   476,
     476,    49,    58,   316,     0,     0,     0,   353,     0,     0,
     370,   467,     0,     0,   421,   368,   469,     0,     0,   463,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   727,   728,   729,   730,     0,     0,
     397,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     485,   454,   247,     0,   294,     0,     0,   292,   289,     0,
     262,     0,     0,     0,   408,     0,   461,     0,     0,     0,
       0,   695,     0,   695,     0,   197,   198,     0,   715,     0,
       0,   119,     0,     0,    30,    29,    37,   695,    33,     0,
       0,     0,   132,     0,     0,   114,     0,     0,   223,   225,
       0,   223,   223,     0,   141,   139,   140,     0,     0,     0,
     529,   165,   162,   708,   709,   710,   163,   164,   138,     0,
       0,   107,    45,   695,    46,   701,     0,   706,   364,   365,
     690,   678,     0,   682,   679,     0,     0,   480,     0,   474,
     475,     0,     0,   357,   349,     0,     0,   367,   366,   422,
       0,     0,     0,   394,   384,   456,   460,   459,     0,   421,
     421,   421,   390,   385,   726,   736,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   285,
     288,     0,     0,   261,   259,   421,   428,   424,   423,   426,
     409,     0,     0,     0,   179,   178,   203,     0,   696,   694,
     697,     0,   196,   717,     0,   121,   176,    36,     0,   135,
     569,     0,   168,   170,     0,     0,   137,   130,     0,   224,
     225,     0,     0,     0,   222,     0,   225,   225,     0,     0,
     209,     0,     0,     0,   145,   146,   149,   159,   193,   122,
       0,    63,    41,     0,   683,     0,   673,     0,   479,     0,
     477,   354,   355,   465,   466,   464,   471,   264,     0,   386,
     387,   388,   389,   737,     0,   392,   393,   396,     0,   511,
       0,   514,   501,   503,   505,   507,   509,   286,   290,   369,
     655,   429,     0,   431,     0,   183,   182,     0,     0,     0,
     695,     0,     0,     0,     0,     0,     0,   143,   142,   218,
     219,     0,     0,     0,     0,   220,   221,     0,     0,     0,
       0,     0,     0,     0,     0,   157,     0,   136,   160,     0,
       0,   108,     0,   680,   482,   481,   268,     0,     0,     0,
     272,   270,     0,   265,   267,   274,     0,   739,     0,   395,
       0,     0,   425,     0,   402,   191,   190,     0,     0,   181,
     180,   204,     0,   698,     0,   178,     0,   236,     0,     0,
       0,     0,   166,   167,   226,   227,     0,   229,   231,     0,
       0,   208,     0,   216,   211,     0,     0,     0,     0,     0,
       0,   147,   150,   158,     0,     0,     0,   194,     0,     0,
      84,    85,    70,    71,    87,    74,   700,     0,     0,     0,
       0,     0,     0,   263,   266,     0,   738,     0,   741,     0,
     512,   430,     0,   433,   187,   186,   189,   188,     0,     0,
       0,     0,   235,     0,   238,   169,   174,     0,   228,   233,
     230,   232,     0,     0,     0,   212,     0,   207,   711,     0,
       0,   714,   144,     0,     0,     0,     0,     0,     0,    73,
      83,     0,     0,     0,   684,     0,     0,   280,     0,     0,
       0,     0,     0,   275,   746,   747,   740,     0,   744,   401,
     432,   436,   435,   427,     0,   185,   184,   563,   564,     0,
     237,     0,     0,   234,     0,     0,   217,   210,   713,   712,
       0,     0,     0,   148,   720,     0,     0,    72,    88,     0,
      89,   269,   276,     0,   278,   277,   279,   273,   271,   743,
     742,     0,   731,     0,     0,     0,     0,   448,   437,   441,
     438,   725,     0,   173,   213,     0,     0,     0,     0,     0,
       0,   719,   718,     0,     0,    76,    79,    80,    81,     0,
     100,   281,   745,     0,     0,   444,     0,   446,   440,   439,
     442,     0,   434,   172,     0,     0,     0,     0,     0,     0,
       0,   152,   151,     0,    82,   402,   311,    75,     0,    90,
      91,    92,     0,    97,   445,   447,     0,     0,   450,     0,
     451,     0,   215,     0,   154,   153,   156,   155,     0,    77,
     101,     0,     0,    86,    95,   443,   449,   452,   171,     0,
      78,     0,    94,    98,     0,    93,   214,   102,     0,    96,
       0,    99,     0,     0,   103
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1048, -1048,   907, -1048, -1048, -1048,   691, -1048, -1048, -1048,
   -1048, -1048,   628, -1048, -1048,  -230, -1048, -1048,   892,   933,
   -1048,   426,  -181,    10,  1098,   318,   893, -1048,  -233, -1048,
    -241, -1048, -1048,  -322,   633, -1048, -1048, -1048,   -47, -1048,
    -156,  -119, -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048,   943,   906,  -187, -1048, -1047,   657,   429,
    -190,  -110,   654, -1048,  -472,   211, -1048, -1048, -1048,    50,
     117, -1048, -1048, -1048, -1048, -1048, -1048,   137, -1048,  -449,
       6, -1048,  -610, -1048, -1048, -1048,    54,   -52, -1048, -1048,
   -1048, -1048,  -125,  -427, -1048, -1048,    13,  -257,   726,  -193,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048, -1048,   111,    36,
   -1048, -1048, -1048, -1048, -1048, -1048, -1048,  1966,   710,   711,
     697,  -184, -1048,  -440,  -323, -1048,   538,   559, -1048, -1048,
   -1048, -1048, -1048,  -397, -1048, -1048, -1048,   645, -1048,  -457,
   -1048, -1048,   693, -1048, -1048,   411,  -214,   115, -1048,   664,
   -1048,  1201,  -418,  -240, -1048,   266, -1048, -1048, -1048, -1048,
   -1048, -1048,   -18, -1048, -1048,  -118, -1048, -1048,  -444,  -236,
     434, -1048, -1048, -1048, -1048,   462,   663, -1048,   210,   452,
   -1048,  -803, -1048,    87, -1048,  -355,   830,   709, -1048,    -2,
    -428,  1621,  1180,   807, -1048,   498, -1048, -1048, -1048,  -242,
     818,   676,   399, -1048,  -219, -1048, -1048, -1048, -1048, -1048,
   -1048, -1048, -1048, -1048,  -199, -1048, -1048, -1048, -1048, -1048,
   -1048,  -164, -1048, -1048, -1048, -1048, -1048, -1048,  -913
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    10,    11,    12,    13,   297,   298,   299,   186,    14,
      15,   353,   484,   485,    16,   366,    17,    18,   222,   223,
     495,   496,    19,    20,    21,    22,    23,   300,   301,   681,
     682,   302,    24,   348,   493,   701,   841,  1112,  1113,  1254,
    1255,  1256,  1114,  1173,  1174,  1260,  1313,  1325,  1314,  1293,
     509,   961,    25,   200,   201,   236,   237,   362,   487,   666,
     231,   357,   358,   683,   811,   685,   686,   687,   688,   954,
     955,  1164,  1102,  1037,   830,   689,   931,   932,  1082,   303,
     690,   304,   691,   692,   950,  1093,  1094,  1206,  1246,  1156,
     693,   823,   940,   944,   694,  1076,  1077,   305,   306,   307,
     308,   309,   310,   311,   312,   313,  1052,  1053,  1054,  1055,
    1176,   314,   315,   316,   317,   318,   319,   490,   321,   322,
     323,   324,   325,   326,   388,   542,   617,   609,   451,   452,
     453,   457,   461,   327,   328,   391,   526,   527,   728,   329,
     709,   330,   331,   332,   570,   888,   212,   213,   370,   371,
     333,   334,   868,   869,   907,   908,  1002,  1003,  1064,  1193,
    1194,  1237,  1265,  1239,  1240,  1266,  1267,  1272,   335,   545,
     546,   547,   336,   377,   378,   859,   719,   856,   857,   598,
     599,   600,   601,   337,   633,   477,   478,   479,   454,   338,
     339,   340,   197,   379,   380,   381,   716,   966,   854,   249,
     512,   713,   651,   652,   220,   706,   504,    26,    27,   832,
     833,   834,   835,   836,   350,   660,  1038,   351,   663,   664,
     341,   559,   557,   753,  1057,  1128,  1188,  1232,  1186
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     191,   251,   386,   616,   494,   202,   191,   191,   187,   383,
     649,    28,   367,   636,   203,   204,   240,   242,   244,   684,
     246,   247,   239,   241,   250,   564,   565,   354,   427,   401,
     481,   735,   760,   667,   209,   498,   786,   400,   647,   523,
     905,   389,   501,   731,   503,   506,   739,   695,   975,   193,
     729,   491,   976,   435,   435,   970,   436,   436,   435,   466,
     979,   436,   435,     1,  1104,   436,   435,   491,   812,   436,
     435,   530,   742,   436,   435,   987,  1059,   436,   431,   403,
     773,   218,   435,   539,   901,   436,   543,   726,   749,   190,
     473,   552,   553,   761,   190,   190,   190,   435,   435,   435,
     436,   436,   436,   435,   750,   771,   436,   785,   208,   522,
     507,   425,   426,   791,   798,   801,   435,   434,   867,   436,
     968,   977,   989,   500,  1178,   435,  1015,   386,   436,   474,
     435,   400,  1142,   436,   437,   438,   439,   440,   441,   442,
    1145,   642,  1222,  1228,   -81,  1273,  1287,  1302,  1318,   435,
     508,  1154,   436,   702,  1242,  1080,   195,   827,  1294,   209,
     518,   606,   196,   435,   607,   618,   436,   205,   227,   194,
       1,   698,   486,   228,   435,   206,   443,   436,   435,   207,
     554,   436,   616,   217,   229,   431,   435,   444,   525,   436,
       5,   191,   445,   191,   603,   210,   191,   202,   524,   224,
     812,   226,   615,   435,   224,   446,   436,   349,   571,   733,
     400,  1269,   435,   948,   431,   436,   861,   775,   216,   346,
     635,   435,   435,   191,   436,   436,  1029,  1301,   744,  1030,
     230,   224,   202,   363,   695,   219,   363,  1179,   732,   605,
     613,   238,   614,   902,   862,  1191,  1033,   209,    28,  1105,
     -81,   482,  1092,  1192,   671,   560,   561,   562,   400,   214,
     492,  1295,   665,   234,  1106,   608,   667,   619,   724,   385,
     431,   434,   190,   640,  1230,  1144,   699,   949,   639,   221,
     190,  1031,   190,   235,   435,   190,   190,   436,   437,   438,
     439,   440,   441,   442,   211,   876,   514,     5,   428,  1046,
     721,  1204,  1081,   447,   672,   499,   673,   675,   190,   524,
     450,   450,   190,   524,  1270,   450,   524,   606,  1262,   450,
     607,   190,   190,   450,     7,   190,   815,   450,   225,   741,
     443,   450,   824,   825,   826,   649,   746,   747,   748,   450,
     838,   444,   743,   471,   937,   374,   445,   877,   945,   480,
     375,   480,  1056,   925,   450,   450,   450,   480,   524,   446,
     450,   916,   647,   448,   933,   385,   695,   248,     5,   828,
     935,   829,   695,   450,   743,   743,   695,   524,     5,   843,
     621,   615,   450,   792,   799,   802,   449,   450,   524,   400,
     969,   978,   990,   751,   520,     7,  1016,  -563,   515,  -535,
    1297,   784,  1143,   516,   517,     7,   450,   245,   548,   385,
     524,   647,  1223,   524,   548,   499,  1288,   492,   499,  1184,
     450,   347,   737,   349,  1185,  1184,   738,  1298,   548,  1229,
    1185,   450,   190,  1311,   191,   450,  1312,   781,   190,   622,
     190,   364,   611,   450,   807,   400,   190,   360,   -57,  1210,
    1211,   365,   806,   514,   376,   623,  -563,   447,   219,  1299,
     450,   980,   981,   982,   646,   650,   347,   502,   655,   450,
     658,   392,     1,   393,   480,   957,   394,  1028,   450,   450,
     668,   395,  1047,  1048,  1049,  1050,  1051,   999,     2,   696,
     396,   837,    28,   397,   695,   808,   398,   703,   549,   520,
    1300,   399,   874,   -57,   407,   695,   951,   952,   -57,   882,
     883,   455,   456,  1020,  1212,   555,   556,   448,     3,  1025,
    1026,   -57,   458,   190,   253,   254,   255,   256,   257,   402,
     258,   459,   460,   253,   254,   255,   256,   257,   405,   258,
     449,   450,  1233,     4,   406,   910,   253,   254,   255,   256,
     257,   408,   258,   190,   -57,   515,   409,   190,   431,   190,
     516,   517,   368,   190,   372,   373,   624,   933,    44,   190,
    1234,   953,   918,   269,   920,   817,   410,   400,   190,  1234,
    1071,   916,   269,   400,   411,   817,   190,   400,   927,   412,
     -57,   342,   343,   344,    52,   269,   941,   942,   943,     5,
     413,   -57,   818,   347,   505,   -57,  1084,  1085,  1086,   819,
     414,   916,   818,   817,   415,    62,     6,    64,   416,   819,
     820,   848,   849,   821,   962,   417,     7,   418,   936,   419,
     820,   852,   853,   821,  1087,  1088,  1089,   420,     8,     9,
     818,   421,   788,  1024,  1047,  1048,  1049,   819,   985,   986,
     669,   670,    83,  1095,   795,   796,  1249,  1250,   820,   422,
     822,   821,  1150,  1151,  1152,  1289,  1290,  1291,  1277,  1278,
     822,   423,   563,   424,  1235,   429,   696,  1279,  1280,   480,
     430,   563,    97,  1263,   433,   480,   480,   480,   385,   -57,
     432,  1079,   462,   480,   563,   188,   946,   947,   822,   463,
     188,   188,   188,  1041,   464,   400,   465,   510,  -564,   466,
     467,  1071,   468,   668,   469,   472,   400,   475,   817,   511,
     521,  1177,   525,   529,   535,   558,   355,   612,   627,   625,
     191,   190,   653,   628,   654,   662,   656,   659,   866,   524,
    1071,   548,   668,   190,   190,   818,   704,   705,   708,   707,
     711,   712,   819,   715,   717,   718,   740,   722,   745,   730,
     734,   736,   752,   820,   754,   190,   821,  1091,   190,  1040,
    1257,   755,   756,   757,   190,   190,   190,   758,  1103,  1258,
     759,   909,   190,  -398,  -399,   911,  1319,   915,   917,  -400,
     650,  1073,   762,   763,   764,   922,   765,  1236,   766,  1182,
     668,   926,   190,   822,  1303,   767,   768,   928,   696,  1190,
     769,   668,   650,  1286,   696,   770,   774,  -528,   696,   190,
     520,   779,   794,   789,   934,   783,  1261,   956,   480,   790,
     793,   190,   695,   797,   800,  1153,  1264,   958,  1157,  1257,
     803,   804,   805,   809,   813,   814,   816,   840,  1258,   839,
    1133,   846,   845,   847,   850,   855,   858,   872,   873,   864,
     879,   880,   881,   884,   885,   887,   891,   892,   886,   893,
     190,   894,   895,   896,   190,   897,   190,   190,   188,   898,
     904,   921,   938,   912,   190,   923,   188,   924,   188,   190,
     190,   188,   188,   960,   929,   939,   190,   190,   959,  1264,
     190,   190,   963,   190,   909,   802,   964,   190,   965,  1004,
    1006,  1007,   967,   973,   188,   974,   190,   190,   188,   573,
     984,   574,   575,   991,   992,   993,   190,   188,   188,   994,
     576,   188,  1220,   995,   996,  1010,   696,  1008,  1009,  1011,
    1012,  1013,  1014,  1021,  1017,  1018,  1023,   696,  1027,   577,
    1034,  1035,   578,  1022,  1032,  1039,  1042,  1044,   579,  1058,
    1036,  1060,  1062,  1063,  1043,  1067,  1061,   580,  1068,  1090,
    1092,  1074,  1097,  1096,  1098,  1116,  1099,  1100,  1117,  1118,
     581,   582,  1123,  1119,  1120,  1121,  1122,  1125,  1126,  1127,
    1130,   209,  1132,   190,  1138,  1139,  1140,  1141,  1146,   190,
     190,  1158,  1066,  1147,   583,  1159,  1070,  1072,  1148,  1160,
    1075,  1078,  1161,  1166,   668,  1165,  1149,  1167,  1168,  1171,
    1163,  1170,  1175,  1180,  1181,   190,   584,  1189,  1199,  1201,
    1172,  1187,   956,  1203,  1202,  1205,   190,  1107,   188,   585,
    1207,  1208,  1209,   400,   188,  1214,   188,   586,   587,  1213,
    1215,  1216,   188,  1218,  1219,   588,   589,  1221,  1241,   191,
    1224,  1243,  1326,  1225,  1226,  1135,  1137,  1131,  1227,  1245,
    1231,  1308,  1251,  1252,  1259,  1269,  1268,  1274,  1247,  1281,
     590,   591,   592,   593,   594,   595,   596,   597,  1270,  1282,
    1248,   190,  1271,  1292,  1296,   190,   190,  1310,   956,   190,
     190,  1276,  1304,   190,  1305,  1306,  1307,  1317,  1320,  1316,
    1327,  1328,  1329,  1333,   243,  1321,   674,   352,   385,   572,
    1334,   190,  1322,   215,  1217,   842,   190,  1324,   700,   188,
    1323,   232,  1309,  1331,  1285,  1275,  1196,  1330,  1332,  1070,
     361,  1078,   233,   382,   851,   676,   697,  1155,   190,  1019,
    1162,  1101,  1244,  1083,   190,   190,  1200,   604,   638,   188,
     777,  1183,   634,   188,  1124,   188,   922,   772,   637,   188,
     889,   727,  1001,  1129,   710,   188,  1238,   875,  1315,  1045,
     720,   865,   860,   488,   188,   661,   198,   190,   831,   714,
     519,   919,   188,   513,     0,     0,     0,     0,     0,   363,
       0,     0,     0,   189,     0,     0,     0,     0,   189,   189,
     189,     0,     0,     0,     0,     0,     0,   668,     0,     0,
       0,     0,     0,     0,     0,   190,   190,     0,   190,     0,
     190,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   190,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   363,     0,   696,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   668,     0,   190,     0,
       0,     0,     0,   385,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   190,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   188,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   188,
     188,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   190,     0,   190,     0,     0,     0,     0,     0,     0,
       0,   188,     0,     0,   188,   190,     0,     0,     0,     0,
     188,   188,   188,     0,     0,     0,   189,     0,   188,     0,
       0,     0,     0,     0,   189,     0,   189,     0,     0,   189,
     189,     0,     0,     0,     0,     0,     0,     0,   188,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   189,     0,     0,   188,   189,     0,     0,     0,
       0,     0,     0,     0,     0,   189,   189,   188,     0,   189,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   188,     0,     0,     0,
     188,     0,   188,   188,     0,     0,     0,     0,     0,     0,
     188,     0,     0,     0,     0,   188,   188,     0,     0,     0,
       0,     0,   188,   188,     0,     0,   188,   188,     0,   188,
       0,     0,     0,   188,     0,     0,     0,     0,     0,     0,
       0,     0,   188,   188,     0,     0,     0,     0,     0,     0,
       0,     0,   188,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   189,     0,     0,     0,
       0,     0,   189,     0,   189,     0,     0,     0,     0,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   188,
       0,     0,     0,     0,     0,   188,   188,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   192,     0,     0,     0,     0,   192,   192,
     192,   188,     0,     0,     0,     0,     0,   189,     0,     0,
       0,     0,   188,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   189,     0,     0,
       0,   189,     0,   189,     0,     0,     0,   189,     0,     0,
       0,     0,     0,   189,     0,     0,     0,     0,     0,     0,
       0,     0,   189,     0,     0,     0,     0,   188,     0,     0,
     189,   188,   188,     0,     0,   188,   188,     0,     0,   188,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   188,     0,     0,
       0,     0,   188,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   188,     0,     0,     0,     0,     0,
     188,   188,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   188,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   192,     0,     0,     0,
       0,     0,     0,     0,   192,     0,   192,     0,     0,   192,
     192,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   188,     0,     0,   188,     0,   188,     0,     0,     0,
       0,     0,   192,     0,     0,   189,   192,     0,     0,     0,
       0,     0,     0,     0,     0,   192,   192,   189,   189,   192,
       0,   188,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   189,
       0,     0,   189,     0,     0,     0,     0,     0,   189,   189,
     189,     0,     0,     0,   188,     0,   189,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   188,     0,     0,     0,   189,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   189,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   189,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   192,   188,     0,   188,
       0,     0,   192,     0,   192,     0,     0,     0,     0,     0,
     192,   188,     0,     0,   189,     0,     0,     0,   189,     0,
     189,   189,     0,     0,     0,     0,     0,     0,   189,     0,
       0,     0,     0,   189,   189,     0,     0,     0,     0,     0,
     189,   189,     0,     0,   189,   189,     0,   189,     0,     0,
       0,   189,     0,     0,     0,     0,     0,     0,     0,     0,
     189,   189,     0,     0,     0,     0,     0,     0,     0,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   192,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   192,     0,   192,     0,     0,     0,   192,     0,     0,
       0,     0,     0,   192,     0,     0,     0,   189,     0,     0,
       0,     0,     0,   189,   189,     0,     0,     0,     0,     0,
     192,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   189,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   320,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   359,     0,     0,
       0,     0,     0,     0,     0,   189,     0,     0,     0,   189,
     189,   369,     0,   189,   189,     0,     0,   189,     0,     0,
       0,     0,     0,     0,     0,   387,     0,   390,     0,     0,
       0,     0,     0,     0,     0,   189,     0,     0,   404,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   189,     0,     0,   192,     0,     0,   189,   189,
       0,     0,     0,     0,     0,     0,     0,   192,   192,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   189,   192,     0,     0,     0,     0,     0,   192,   192,
     192,     0,     0,   404,     0,     0,   192,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   497,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   192,     0,     0,   189,
       0,     0,   189,     0,   189,     0,     0,     0,     0,     0,
     387,     0,     0,   192,     0,     0,     0,     0,   528,     0,
       0,   531,   534,     0,     0,   192,     0,     0,   536,   189,
       0,   537,     0,     0,   541,     0,   544,     0,   550,   551,
       0,     0,   544,     0,     0,     0,     0,     0,     0,     0,
     569,     0,     0,     0,   320,     0,   544,   404,   404,     0,
     610,     0,   189,     0,   192,     0,     0,     0,   192,     0,
       0,   620,     0,     0,   626,     0,     0,   632,   192,     0,
     189,     0,     0,   192,   192,     0,     0,     0,     0,     0,
     192,   643,     0,     0,   192,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     192,   192,     0,     0,     0,     0,     0,     0,   359,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   189,   369,   189,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   189,
     723,   725,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   192,     0,     0,
       0,     0,     0,   192,   192,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   776,     0,   778,
       0,   780,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   782,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   192,     0,     0,     0,     0,
       0,     0,     0,   192,   192,     0,     0,   192,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   192,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   497,     0,     0,   844,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   192,     0,     0,     0,     0,     0,   192,   192,
       0,     0,   863,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   870,   871,     0,     0,     0,     0,   544,
       0,   878,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   192,     0,     0,     0,     0,     0,   890,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   899,
       0,   900,     0,     0,     0,   903,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   192,
    1198,     0,   192,     0,   192,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   404,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   192,     0,     0,     0,     0,   971,   972,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     192,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   983,     0,     0,     0,     0,   988,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   997,   998,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   192,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   192,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   404,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     404,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      29,   404,     0,     0,     0,     0,  1115,     0,     0,     0,
       0,     0,     0,     0,    30,    31,   476,    32,    33,     0,
       0,    34,     0,     0,     0,     0,     0,    35,    36,     0,
      37,     0,   435,    38,    39,   436,     0,     0,     0,     0,
       0,     0,    40,    41,    42,     0,    43,     0,     0,     0,
       0,    44,     0,     0,    45,     0,    46,    47,   404,    48,
       0,   404,     0,     0,     0,     0,    49,     0,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,     0,    67,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
       0,    80,     0,    81,    82,    83,     0,  1115,     0,    84,
       0,     0,    85,    86,    87,     0,     0,    88,    89,    90,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   103,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   125,     0,     0,     0,     0,   126,
       0,   359,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,     0,   137,   138,   139,   140,   141,   142,   143,
     144,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,     0,   252,
     253,   254,   255,   256,   257,   404,   258,     0,   384,   450,
       0,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,   261,   262,   263,     0,    37,
       0,     0,   264,    39,     0,     0,     0,     0,     0,     0,
       0,   265,    41,   266,     0,    43,     0,   267,   268,   269,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,   270,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,   271,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,   272,   273,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,   274,
      80,     0,    81,    82,    83,     5,   275,     0,    84,     0,
       0,    85,    86,    87,   276,     0,    88,    89,   277,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     7,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   278,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   279,     0,     0,     0,     0,   126,     0,
       0,   280,   128,   129,   130,   131,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,     0,     0,     0,
       0,     0,     0,     0,     0,   295,   296,   252,   253,   254,
     255,   256,   257,     0,   258,     0,   259,   260,     0,     0,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,   261,   262,   263,     0,    37,     0,     0,
     264,    39,     0,     0,     0,     0,     0,     0,     0,   265,
      41,   266,     0,    43,     0,   267,   268,   269,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,   270,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
     271,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,   272,   273,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,   274,    80,     0,
      81,    82,    83,     0,   275,     0,    84,     0,     0,    85,
      86,    87,   276,     0,    88,    89,   277,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   278,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   279,     0,     0,     0,     0,   126,     0,     0,   280,
     128,   129,   130,   131,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,   293,   294,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,     0,     0,     0,     0,
       0,     0,     0,   295,   296,   252,   253,   254,   255,   256,
     257,     0,   258,     0,  1108,     0,     0,     0,     0,    30,
      31,     0,    32,    33,     0,     0,    34,     0,     0,     0,
       0,   261,   262,   263,     0,    37,     0,     0,   264,    39,
       0,     0,     0,     0,     0,     0,     0,   265,    41,   266,
       0,    43,     0,   267,   268,   269,    44,     0,     0,    45,
       0,    46,    47,     0,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
    1109,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,   271,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,   273,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,   274,    80,     0,    81,    82,
      83,     0,   275,     0,    84,     0,     0,    85,    86,    87,
     276,     0,    88,    89,   277,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     0,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,     0,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   279,
       0,     0,     0,     0,   126,     0,     0,   280,   128,   129,
     130,   131,   281,   282,   283,   284,   285,   286,   287,   288,
     289,   290,   291,   292,   293,   294,     0,   145,   146,  1110,
    1111,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
       0,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,     0,     0,     0,     0,     0,     0,     0,
       0,   295,   296,   252,   253,   254,   255,   256,   257,     0,
     258,     0,   489,  1169,     0,     0,     0,    30,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,   261,
     262,   263,     0,    37,     0,     0,   264,    39,     0,     0,
       0,     0,     0,     0,     0,   265,    41,   266,     0,    43,
       0,   267,   268,   269,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,   271,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,   273,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,   274,    80,     0,    81,    82,    83,     5,
     275,     0,    84,     0,     0,    85,    86,    87,   276,     0,
      88,    89,   277,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     7,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   279,     0,     0,
       0,     0,   126,     0,     0,   280,   128,   129,   130,   131,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,   293,   294,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,     0,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,     0,     0,     0,     0,     0,     0,     0,     0,   295,
     296,   252,   253,   254,   255,   256,   257,     0,   258,     0,
     489,  1284,     0,     0,     0,    30,    31,     0,    32,    33,
       0,     0,    34,     0,     0,     0,     0,   261,   262,   263,
       0,    37,     0,     0,   264,    39,     0,     0,     0,     0,
       0,     0,     0,   265,    41,   266,     0,    43,     0,   267,
     268,   269,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,   271,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,   273,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,   274,    80,     0,    81,    82,    83,     5,   275,     0,
      84,     0,     0,    85,    86,    87,   276,     0,    88,    89,
     277,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     7,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   279,     0,     0,     0,     0,
     126,     0,     0,   280,   128,   129,   130,   131,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     293,   294,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,     0,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
       0,     0,     0,     0,     0,     0,     0,   295,   296,   252,
     253,   254,   255,   256,   257,     0,   258,     0,   259,     0,
       0,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,   261,   262,   263,     0,    37,
       0,     0,   264,    39,     0,     0,     0,     0,     0,     0,
       0,   265,    41,   266,     0,    43,     0,   267,   268,   269,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,   270,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,   271,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,   272,   273,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,   274,
      80,     0,    81,    82,    83,     0,   275,     0,    84,     0,
       0,    85,    86,    87,   276,     0,    88,    89,   277,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     0,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   278,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   279,     0,     0,     0,     0,   126,     0,
       0,   280,   128,   129,   130,   131,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,     0,     0,     0,
       0,     0,     0,     0,     0,   295,   296,   252,   253,   254,
     255,   256,   257,     0,   258,     0,   489,     0,     0,     0,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,   261,   262,   263,     0,    37,     0,     0,
     264,    39,     0,     0,     0,     0,     0,     0,     0,   265,
      41,   266,     0,    43,     0,   267,   268,   269,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
     271,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,   273,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,   274,    80,     0,
      81,    82,    83,     5,   275,     0,    84,     0,     0,    85,
      86,    87,   276,     0,    88,    89,   277,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       7,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   279,     0,     0,     0,     0,   126,     0,     0,   280,
     128,   129,   130,   131,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,   293,   294,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,     0,     0,     0,     0,
       0,     0,     0,   295,   296,   252,   253,   254,   255,   256,
     257,     0,   258,     0,   355,     0,     0,     0,     0,    30,
      31,     0,    32,    33,     0,     0,    34,     0,     0,     0,
       0,   261,   262,   263,     0,    37,     0,     0,   264,    39,
       0,     0,     0,     0,     0,     0,     0,   265,    41,   266,
       0,    43,     0,   267,   268,   269,    44,     0,     0,    45,
       0,    46,    47,   532,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,   271,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,   273,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,   274,    80,     0,    81,    82,
      83,     0,   275,     0,    84,     0,     0,    85,    86,    87,
     276,     0,    88,    89,   277,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     0,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,     0,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   279,
       0,     0,     0,     0,   126,     0,     0,   280,   128,   129,
     130,   131,   281,   282,   283,   284,   285,   286,   287,   288,
     289,   290,   291,   292,   293,   294,     0,   145,   146,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
       0,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,     0,     0,     0,   533,     0,     0,     0,
       0,   295,   296,   252,   253,   254,   255,   256,   257,     0,
     258,     0,   355,     0,     0,   629,     0,   630,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,   261,
     262,   263,     0,    37,     0,     0,   264,    39,     0,     0,
       0,     0,     0,     0,     0,   265,    41,   266,     0,    43,
       0,   267,   268,   269,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,   271,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,   273,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,   274,    80,     0,    81,    82,    83,     0,
     275,     0,    84,     0,   631,    85,    86,    87,   276,     0,
      88,    89,   277,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   279,     0,     0,
       0,     0,   126,     0,     0,   280,   128,   129,   130,   131,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,   293,   294,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,     0,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,     0,     0,     0,     0,     0,     0,     0,     0,   295,
     296,   252,   253,   254,   255,   256,   257,     0,   258,     0,
     355,     0,     0,     0,     0,    30,    31,     0,    32,    33,
       0,     0,    34,     0,     0,     0,     0,   261,   262,   263,
       0,    37,     0,     0,   264,    39,     0,     0,     0,     0,
       0,     0,     0,   265,    41,   266,     0,    43,     0,   267,
     268,   269,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,   271,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,   273,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,   274,    80,     0,    81,    82,    83,     0,   275,     0,
      84,     0,     0,    85,    86,    87,   276,     0,    88,    89,
     277,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   279,     0,     0,     0,     0,
     126,     0,     0,   280,   128,   129,   130,   131,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     293,   294,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,     0,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
       0,     0,   356,     0,     0,     0,     0,   295,   296,   252,
     253,   254,   255,   256,   257,     0,   258,     0,   355,   538,
       0,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,   261,   262,   263,     0,    37,
       0,     0,   264,    39,     0,     0,     0,     0,     0,     0,
       0,   265,    41,   266,     0,    43,     0,   267,   268,   269,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,     0,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,   271,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,     0,   273,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,   274,
      80,     0,    81,    82,    83,     0,   275,     0,    84,     0,
       0,    85,    86,    87,   276,     0,    88,    89,   277,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     0,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   103,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   279,     0,     0,     0,     0,   126,     0,
       0,   280,   128,   129,   130,   131,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,     0,     0,     0,
       0,     0,     0,     0,     0,   295,   296,   252,   253,   254,
     255,   256,   257,     0,   258,     0,   355,     0,     0,     0,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,   261,   262,   263,     0,    37,     0,     0,
     264,    39,     0,     0,     0,     0,     0,     0,     0,   265,
      41,   266,     0,    43,     0,   267,   268,   269,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
     271,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,   273,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,   274,    80,     0,
      81,    82,    83,     0,   275,     0,    84,     0,     0,    85,
      86,    87,   276,     0,    88,    89,   277,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   279,     0,     0,     0,     0,   126,     0,     0,   280,
     128,   129,   130,   131,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,   293,   294,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,     0,     0,   540,     0,
       0,     0,     0,   295,   296,   252,   253,   254,   255,   256,
     257,     0,   258,     0,   355,   602,     0,     0,     0,    30,
      31,     0,    32,    33,     0,     0,    34,     0,     0,     0,
       0,   261,   262,   263,     0,    37,     0,     0,   264,    39,
       0,     0,     0,     0,     0,     0,     0,   265,    41,   266,
       0,    43,     0,   267,   268,   269,    44,     0,     0,    45,
       0,    46,    47,     0,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,   271,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,   273,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,   274,    80,     0,    81,    82,
      83,     0,   275,     0,    84,     0,     0,    85,    86,    87,
     276,     0,    88,    89,   277,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     0,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,     0,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   279,
       0,     0,     0,     0,   126,     0,     0,   280,   128,   129,
     130,   131,   281,   282,   283,   284,   285,   286,   287,   288,
     289,   290,   291,   292,   293,   294,     0,   145,   146,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
       0,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,     0,     0,     0,     0,     0,     0,     0,
       0,   295,   296,   252,   253,   254,   255,   256,   257,     0,
     258,     0,   355,   641,     0,     0,     0,    30,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,   261,
     262,   263,     0,    37,     0,     0,   264,    39,     0,     0,
       0,     0,     0,     0,     0,   265,    41,   266,     0,    43,
       0,   267,   268,   269,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,   271,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,   273,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,   274,    80,     0,    81,    82,    83,     0,
     275,     0,    84,     0,     0,    85,    86,    87,   276,     0,
      88,    89,   277,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   279,     0,     0,
       0,     0,   126,     0,     0,   280,   128,   129,   130,   131,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,   293,   294,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,     0,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,     0,     0,     0,     0,     0,     0,     0,     0,   295,
     296,   252,   253,   254,   255,   256,   257,     0,   258,     0,
    1283,  1284,     0,     0,     0,    30,    31,     0,    32,    33,
       0,     0,    34,     0,     0,     0,     0,   261,   262,   263,
       0,    37,     0,     0,   264,    39,     0,     0,     0,     0,
       0,     0,     0,   265,    41,   266,     0,    43,     0,   267,
     268,   269,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,   271,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,   273,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,   274,    80,     0,    81,    82,    83,     0,   275,     0,
      84,     0,     0,    85,    86,    87,   276,     0,    88,    89,
     277,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   279,     0,     0,     0,     0,
     126,     0,     0,   280,   128,   129,   130,   131,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     293,   294,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,     0,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
       0,     0,     0,     0,     0,     0,     0,   295,   296,   252,
     253,   254,   255,   256,   257,     0,   258,     0,   355,     0,
       0,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,   261,   262,   263,     0,    37,
       0,     0,   264,    39,     0,     0,     0,     0,     0,     0,
       0,   265,    41,   266,     0,    43,     0,   267,   268,   269,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,     0,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,   271,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,     0,   273,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,   274,
      80,     0,    81,    82,    83,     0,   275,     0,    84,     0,
       0,    85,    86,    87,   276,     0,    88,    89,   277,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     0,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   103,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   279,     0,     0,     0,     0,   126,     0,
       0,   280,   128,   129,   130,   131,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,     0,     0,     0,
       0,     0,     0,     0,     0,   295,   296,   252,   253,   254,
     255,   256,   257,     0,   258,     0,   355,     0,     0,     0,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,   261,   262,   263,     0,    37,     0,     0,
     264,    39,     0,     0,     0,     0,     0,     0,     0,   265,
      41,   266,     0,    43,     0,   267,   268,   269,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
     271,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,   273,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,   274,    80,     0,
      81,    82,    83,     0,   275,     0,    84,     0,     0,    85,
      86,    87,   276,     0,    88,    89,   277,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   279,     0,     0,     0,     0,   126,     0,     0,   280,
     128,   129,   130,   131,   281,   282,   283,   284,   285,   286,
     287,   288,   289,   290,   291,   292,   293,   294,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   566,   567,   568,   177,   178,   179,
     180,   181,   182,   183,   184,     0,     0,     0,   252,     0,
       0,     0,     0,   295,   296,   677,     0,   810,     0,     0,
       0,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,     0,    35,    36,     0,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     0,     0,
      40,    41,    42,     0,    43,     0,     0,     0,     0,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,    67,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,     0,    80,
       0,    81,    82,    83,     5,     0,     0,    84,     0,     0,
      85,    86,    87,     0,     0,    88,    89,    90,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     7,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   679,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,     0,     0,     0,     0,   126,     0,   680,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,   137,   138,   139,   140,   141,   142,   143,   144,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,   252,     0,     0,     0,
       0,     0,   185,   677,     0,   678,     0,     0,     0,     0,
      30,    31,     0,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     0,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   679,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,   680,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,     0,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,   252,     0,     0,     0,     0,     0,
     185,     0,     0,   678,     0,     0,     0,     0,    30,    31,
       0,    32,    33,     0,     0,    34,     0,     0,     0,     0,
       0,    35,    36,     0,    37,     0,     0,    38,    39,     0,
       0,     0,     0,     0,     0,     0,    40,    41,    42,     0,
      43,     0,     0,     0,     0,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,     0,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,    67,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,     0,    80,     0,    81,    82,    83,
       0,     0,     0,    84,     0,     0,    85,    86,    87,     0,
       0,    88,    89,    90,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     0,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   679,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   125,     0,
       0,     0,     0,   126,     0,   680,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,     0,   137,   138,   139,
     140,   141,   142,   143,   144,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,    29,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,     0,    30,    31,     0,    32,    33,   185,     0,
      34,     0,     0,     0,     0,     0,    35,    36,     0,    37,
       0,     0,    38,    39,     0,     0,     0,     0,     0,     0,
       0,    40,    41,    42,     0,    43,     0,     0,     0,     0,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,     0,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,     0,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,     0,    67,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,     0,
      80,     0,    81,    82,    83,     0,     0,     0,    84,     0,
       0,    85,    86,    87,     0,     0,    88,    89,    90,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     0,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   103,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   125,     0,     0,     0,     0,   126,     0,
       0,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,     0,   137,   138,   139,   140,   141,   142,   143,   144,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   252,     0,     0,
       0,     0,     0,   185,     0,     0,     0,     0,   644,     0,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   252,     0,     0,   645,     0,
       0,     0,     0,     0,     0,     0,   913,     0,     0,    30,
      31,     0,    32,    33,     0,     0,    34,     0,     0,     0,
       0,     0,    35,    36,     0,    37,     0,     0,    38,    39,
       0,     0,     0,     0,     0,     0,     0,    40,    41,    42,
       0,    43,     0,     0,     0,     0,    44,     0,     0,    45,
       0,    46,    47,     0,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,     0,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,    67,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,     0,    80,     0,    81,    82,
      83,     0,     0,     0,    84,     0,     0,    85,    86,    87,
       0,     0,    88,    89,    90,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     0,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,     0,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   125,
       0,     0,     0,     0,   126,     0,     0,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,   137,   138,
     139,   140,   141,   142,   143,   144,     0,   145,   146,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
       0,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,    29,     0,     0,   914,     0,     0,     0,
       0,     0,     0,     0,   644,     0,     0,    30,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,     0,
      35,    36,     0,    37,     0,     0,    38,    39,     0,     0,
       0,     0,     0,     0,     0,    40,    41,    42,     0,    43,
       0,     0,     0,     0,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,     0,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,    67,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,     0,    80,     0,    81,    82,    83,     0,
       0,     0,    84,     0,     0,    85,    86,    87,     0,     0,
      88,    89,    90,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     0,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,     0,     0,
       0,     0,   126,     0,     0,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,   137,   138,   139,   140,
     141,   142,   143,   144,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,     0,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,    29,     0,     0,   645,     0,     0,     0,     0,     0,
       0,     0,   913,     0,     0,    30,    31,     0,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,    35,    36,
       0,    37,     0,     0,    38,    39,     0,     0,     0,     0,
       0,     0,     0,    40,    41,    42,     0,    43,     0,     0,
       0,     0,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,    67,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,     0,    80,     0,    81,    82,    83,     0,     0,     0,
      84,     0,     0,    85,    86,    87,     0,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   125,     0,     0,     0,     0,
     126,     0,     0,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,   137,   138,   139,   140,   141,   142,
     143,   144,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,    29,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
      30,    31,   914,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     0,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,     0,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,    29,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,     0,    30,    31,  1005,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,    35,    36,
       0,    37,     0,     0,    38,    39,     0,     0,     0,     0,
       0,     0,     0,    40,    41,    42,     0,    43,     0,     0,
       0,     0,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,    67,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,     0,    80,     0,    81,    82,    83,     0,     0,     0,
      84,     0,     0,    85,    86,    87,     0,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   125,     0,     0,     0,     0,
     126,     0,     0,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,   137,   138,   139,   140,   141,   142,
     143,   144,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,   252,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
      30,    31,  1065,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     0,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,     0,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,    29,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,     0,    30,    31,  1069,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,    35,    36,
       0,    37,     0,     0,    38,    39,     0,     0,     0,     0,
       0,     0,     0,    40,    41,    42,     0,    43,     0,     0,
       0,     0,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,    67,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,     0,    80,     0,    81,    82,    83,     0,     0,     0,
      84,     0,     0,    85,    86,    87,     0,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   125,     0,     0,     0,     0,
     126,     0,     0,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,   137,   138,   139,   140,   141,   142,
     143,   144,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,    29,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
      30,    31,  1134,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     0,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,     0,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,    29,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,     0,    30,    31,  1136,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,    35,    36,
       0,    37,     0,     0,    38,    39,     0,     0,     0,     0,
       0,     0,     0,    40,    41,    42,     0,    43,     0,     0,
       0,     0,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,    67,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,     0,    80,     0,    81,    82,    83,     0,     0,     0,
      84,     0,     0,    85,    86,    87,     0,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   125,     0,     0,     0,     0,
     126,     0,     0,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,   137,   138,   139,   140,   141,   142,
     143,   144,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,    29,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,     0,
      30,    31,  1195,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     0,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,     0,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,   648,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,     0,    30,    31,  1069,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,   262,   263,
       0,    37,     0,     0,   264,    39,     0,     0,     0,     0,
       0,     0,     0,   265,    41,   266,     0,    43,     0,   267,
     268,     0,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,   271,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,   273,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,   274,    80,     0,    81,    82,    83,     0,   275,     0,
      84,     0,     0,    85,    86,    87,   276,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   279,     0,     0,     0,     0,
     126,     0,     0,   280,   128,   129,   130,   131,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     293,   294,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,     0,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,    29,
     253,   254,   255,   256,   257,     0,   258,     0,     0,     0,
       0,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,     0,    35,    36,     0,    37,
       0,     0,    38,    39,     0,     0,     0,     0,     0,     0,
       0,    40,    41,    42,     0,    43,     0,     0,     0,   269,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,     0,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,     0,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,     0,    67,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,     0,
      80,     0,    81,    82,    83,     0,     0,     0,    84,     0,
       0,    85,    86,    87,     0,     0,    88,    89,   277,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     0,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   103,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   125,     0,     0,     0,     0,   126,     0,
       0,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,     0,   137,   138,   139,   140,   141,   142,   143,   144,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,     0,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,    29,     0,     0,
       0,     0,     0,     0,     0,     0,   482,     0,     0,     0,
       0,    30,    31,   476,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     5,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       7,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   483,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,     0,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,    29,     0,     0,     0,     0,
       0,     0,     0,     0,     1,     0,     0,     0,     0,    30,
      31,     0,    32,    33,     0,     0,    34,     0,     0,     0,
       0,     0,    35,    36,     0,    37,     0,     0,    38,    39,
       0,     0,     0,     0,     0,     0,     0,    40,    41,    42,
       0,    43,     0,     0,     0,     0,    44,     0,     0,    45,
       0,    46,    47,     0,    48,     0,     0,     0,     0,     0,
       0,    49,     0,     0,    50,     0,     0,     0,    51,     0,
       0,     0,    52,     0,     0,     0,     0,     0,     0,    53,
      54,    55,    56,     0,     0,     0,    57,    58,     0,     0,
      59,    60,    61,    62,    63,    64,    65,     0,     0,    66,
       0,     0,     0,    67,     0,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,     0,    78,    79,     0,    80,     0,    81,    82,
      83,     5,     0,     0,    84,     0,     0,    85,    86,    87,
       0,     0,    88,    89,    90,    91,    92,     0,     0,     0,
       0,    93,     0,     0,    94,    95,     0,     0,     7,    96,
      97,    98,     0,     0,     0,     0,     0,     0,     0,    99,
       0,   100,   101,   102,   103,     0,   104,   105,   106,   107,
     108,   109,   110,   111,   112,     0,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   125,
       0,     0,     0,     0,   126,     0,     0,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,   137,   138,
     139,   140,   141,   142,   143,   144,     0,   145,   146,     0,
       0,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,     0,   162,   163,     0,
     164,   165,   166,   167,   168,   169,   170,     0,   171,   172,
       0,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   252,     0,     0,     0,     0,     0,     0,
       0,     0,     1,     0,     0,     0,     0,    30,    31,     0,
      32,    33,     0,     0,    34,     0,     0,     0,     0,     0,
      35,    36,     0,    37,     0,     0,    38,    39,     0,     0,
       0,     0,     0,     0,     0,    40,    41,    42,     0,    43,
       0,     0,     0,     0,    44,     0,     0,    45,     0,    46,
      47,     0,    48,     0,     0,     0,     0,     0,     0,    49,
       0,     0,    50,     0,     0,     0,    51,     0,     0,     0,
      52,     0,     0,     0,     0,     0,     0,    53,    54,    55,
      56,     0,     0,     0,    57,    58,     0,     0,    59,    60,
      61,    62,    63,    64,    65,     0,     0,    66,     0,     0,
       0,    67,     0,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
       0,    78,    79,     0,    80,     0,    81,    82,    83,     5,
       0,     0,    84,     0,     0,    85,    86,    87,     0,     0,
      88,    89,    90,    91,    92,     0,     0,     0,     0,    93,
       0,     0,    94,    95,     0,     0,     7,    96,    97,    98,
       0,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101,   102,   103,     0,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,     0,     0,
       0,     0,   126,     0,     0,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,   137,   138,   139,   140,
     141,   142,   143,   144,     0,   145,   146,     0,     0,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,     0,   162,   163,     0,   164,   165,
     166,   167,   168,   169,   170,     0,   171,   172,     0,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,    29,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   345,     0,     0,    30,    31,     0,    32,    33,
       0,     0,    34,     0,     0,     0,     0,     0,    35,    36,
       0,    37,     0,     0,    38,    39,     0,     0,     0,     0,
       0,     0,     0,    40,    41,    42,     0,    43,     0,     0,
       0,     0,    44,     0,     0,    45,     0,    46,    47,     0,
      48,     0,     0,     0,     0,     0,     0,    49,     0,     0,
      50,     0,     0,     0,    51,     0,     0,     0,    52,     0,
       0,     0,     0,     0,     0,    53,    54,    55,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
      63,    64,    65,     0,     0,    66,     0,     0,     0,    67,
       0,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,     0,    78,
      79,     0,    80,     0,    81,    82,    83,     0,     0,     0,
      84,     0,     0,    85,    86,    87,     0,     0,    88,    89,
      90,    91,    92,     0,     0,     0,     0,    93,     0,     0,
      94,    95,     0,     0,     0,    96,    97,    98,     0,     0,
       0,     0,     0,     0,     0,    99,     0,   100,   101,   102,
     103,     0,   104,   105,   106,   107,   108,   109,   110,   111,
     112,     0,   113,   114,   115,   116,   117,     0,   118,   119,
     120,   121,   122,   123,   124,   125,     0,     0,     0,     0,
     126,     0,     0,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,   137,   138,   139,   140,   141,   142,
     143,   144,     0,   145,   146,     0,     0,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,     0,   162,   163,     0,   164,   165,   166,   167,
     168,   169,   170,     0,   171,   172,     0,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,    29,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     470,     0,     0,    30,    31,     0,    32,    33,     0,     0,
      34,     0,     0,     0,     0,     0,    35,    36,     0,    37,
       0,     0,    38,    39,     0,     0,     0,     0,     0,     0,
       0,    40,    41,    42,     0,    43,     0,     0,     0,     0,
      44,     0,     0,    45,     0,    46,    47,     0,    48,     0,
       0,     0,     0,     0,     0,    49,     0,     0,    50,     0,
       0,     0,    51,     0,     0,     0,    52,     0,     0,     0,
       0,     0,     0,    53,    54,    55,    56,     0,     0,     0,
      57,    58,     0,     0,    59,    60,    61,    62,    63,    64,
      65,     0,     0,    66,     0,     0,     0,    67,     0,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,     0,    78,    79,     0,
      80,     0,    81,    82,    83,     0,     0,     0,    84,     0,
       0,    85,    86,    87,     0,     0,    88,    89,    90,    91,
      92,     0,     0,     0,     0,    93,     0,     0,    94,    95,
       0,     0,     0,    96,    97,    98,     0,     0,     0,     0,
       0,     0,     0,    99,     0,   100,   101,   102,   103,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,     0,
     113,   114,   115,   116,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   125,     0,     0,     0,     0,   126,     0,
       0,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,     0,   137,   138,   139,   140,   141,   142,   143,   144,
       0,   145,   146,     0,     0,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
       0,   162,   163,     0,   164,   165,   166,   167,   168,   169,
     170,     0,   171,   172,    29,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,     0,    30,    31,
     476,    32,    33,     0,     0,    34,     0,     0,     0,     0,
       0,    35,    36,     0,    37,     0,     0,    38,    39,     0,
       0,     0,     0,     0,     0,     0,    40,    41,    42,     0,
      43,     0,     0,     0,     0,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,     0,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,    67,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,     0,    80,     0,    81,    82,    83,
       0,     0,     0,    84,     0,     0,    85,    86,    87,     0,
       0,    88,    89,    90,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     0,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,     0,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   125,     0,
       0,     0,     0,   126,     0,     0,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,     0,   137,   138,   139,
     140,   141,   142,   143,   144,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,     0,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,    29,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   657,     0,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,     0,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
      29,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   787,     0,     0,    30,    31,     0,    32,    33,     0,
       0,    34,     0,     0,     0,     0,     0,    35,    36,     0,
      37,     0,     0,    38,    39,     0,     0,     0,     0,     0,
       0,     0,    40,    41,    42,     0,    43,     0,     0,     0,
       0,    44,     0,     0,    45,     0,    46,    47,     0,    48,
       0,     0,     0,     0,     0,     0,    49,     0,     0,    50,
       0,     0,     0,    51,     0,     0,     0,    52,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,    63,
      64,    65,     0,     0,    66,     0,     0,     0,    67,     0,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,     0,    78,    79,
       0,    80,     0,    81,    82,    83,     0,     0,     0,    84,
       0,     0,    85,    86,    87,     0,     0,    88,    89,    90,
      91,    92,     0,     0,     0,     0,    93,     0,     0,    94,
      95,     0,     0,     0,    96,    97,    98,     0,     0,     0,
       0,     0,     0,     0,    99,     0,   100,   101,   102,   103,
       0,   104,   105,   106,   107,   108,   109,   110,   111,   112,
       0,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   125,     0,     0,     0,     0,   126,
       0,     0,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,     0,   137,   138,   139,   140,   141,   142,   143,
     144,     0,   145,   146,     0,     0,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,     0,   162,   163,     0,   164,   165,   166,   167,   168,
     169,   170,     0,   171,   172,     0,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,    29,     0,
       0,     0,     0,     0,     0,     0,     0,   906,     0,     0,
       0,     0,    30,    31,     0,    32,    33,     0,     0,    34,
       0,     0,     0,     0,     0,    35,    36,     0,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     0,     0,
      40,    41,    42,     0,    43,     0,     0,     0,     0,    44,
       0,     0,    45,     0,    46,    47,     0,    48,     0,     0,
       0,     0,     0,     0,    49,     0,     0,    50,     0,     0,
       0,    51,     0,     0,     0,    52,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,    63,    64,    65,
       0,     0,    66,     0,     0,     0,    67,     0,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,     0,    78,    79,     0,    80,
       0,    81,    82,    83,     0,     0,     0,    84,     0,     0,
      85,    86,    87,     0,     0,    88,    89,    90,    91,    92,
       0,     0,     0,     0,    93,     0,     0,    94,    95,     0,
       0,     0,    96,    97,    98,     0,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,   102,   103,     0,   104,
     105,   106,   107,   108,   109,   110,   111,   112,     0,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,     0,     0,     0,     0,   126,     0,     0,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,   137,   138,   139,   140,   141,   142,   143,   144,     0,
     145,   146,     0,     0,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,     0,
     162,   163,     0,   164,   165,   166,   167,   168,   169,   170,
       0,   171,   172,     0,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,   252,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   345,     0,     0,
      30,    31,     0,    32,    33,     0,     0,    34,     0,     0,
       0,     0,     0,    35,    36,     0,    37,     0,     0,    38,
      39,     0,     0,     0,     0,     0,     0,     0,    40,    41,
      42,     0,    43,     0,     0,     0,     0,    44,     0,     0,
      45,     0,    46,    47,     0,    48,     0,     0,     0,     0,
       0,     0,    49,     0,     0,    50,     0,     0,     0,    51,
       0,     0,     0,    52,     0,     0,     0,     0,     0,     0,
      53,    54,    55,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,    63,    64,    65,     0,     0,
      66,     0,     0,     0,    67,     0,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,     0,    78,    79,     0,    80,     0,    81,
      82,    83,     0,     0,     0,    84,     0,     0,    85,    86,
      87,     0,     0,    88,    89,    90,    91,    92,     0,     0,
       0,     0,    93,     0,     0,    94,    95,     0,     0,     0,
      96,    97,    98,     0,     0,     0,     0,     0,     0,     0,
      99,     0,   100,   101,   102,   103,     0,   104,   105,   106,
     107,   108,   109,   110,   111,   112,     0,   113,   114,   115,
     116,   117,     0,   118,   119,   120,   121,   122,   123,   124,
     125,     0,     0,     0,     0,   126,     0,     0,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,   137,
     138,   139,   140,   141,   142,   143,   144,     0,   145,   146,
       0,     0,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,     0,   162,   163,
       0,   164,   165,   166,   167,   168,   169,   170,     0,   171,
     172,     0,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,   252,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   657,     0,     0,    30,    31,
       0,    32,    33,     0,     0,    34,     0,     0,     0,     0,
       0,    35,    36,     0,    37,     0,     0,    38,    39,     0,
       0,     0,     0,     0,     0,     0,    40,    41,    42,     0,
      43,     0,     0,     0,     0,    44,     0,     0,    45,     0,
      46,    47,     0,    48,     0,     0,     0,     0,     0,     0,
      49,     0,     0,    50,     0,     0,     0,    51,     0,     0,
       0,    52,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,    57,    58,     0,     0,    59,
      60,    61,    62,    63,    64,    65,     0,     0,    66,     0,
       0,     0,    67,     0,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,     0,    78,    79,     0,    80,     0,    81,    82,    83,
       0,     0,     0,    84,     0,     0,    85,    86,    87,     0,
       0,    88,    89,    90,    91,    92,     0,     0,     0,     0,
      93,     0,     0,    94,    95,     0,     0,     0,    96,    97,
      98,     0,     0,     0,     0,     0,     0,     0,    99,     0,
     100,   101,   102,   103,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,     0,   113,   114,   115,   116,   117,
       0,   118,   119,   120,   121,   122,   123,   124,   125,     0,
       0,     0,     0,   126,     0,     0,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,     0,   137,   138,   139,
     140,   141,   142,   143,   144,     0,   145,   146,     0,     0,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,     0,   162,   163,     0,   164,
     165,   166,   167,   168,   169,   170,     0,   171,   172,     0,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,    29,     0,     0,     0,     0,     0,     0,     0,
       0,  1253,     0,     0,     0,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,    29,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,   199,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,    29,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,    30,    31,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,   252,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
     171,   172,    29,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,    30,   930,     0,    32,
      33,     0,     0,    34,     0,     0,     0,     0,     0,    35,
      36,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,    42,     0,    43,     0,
       0,     0,     0,    44,     0,     0,    45,     0,    46,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,     0,
      78,    79,     0,    80,     0,    81,    82,    83,     0,     0,
       0,    84,     0,     0,    85,    86,    87,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,    94,    95,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,   103,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,   120,   121,   122,   123,   124,   125,     0,     0,     0,
       0,   126,     0,     0,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,   137,   138,   139,   140,   141,
     142,   143,   144,     0,   145,   146,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,    29,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
       0,    30,    31,     0,    32,    33,     0,     0,    34,     0,
       0,     0,     0,     0,    35,    36,     0,    37,     0,     0,
      38,    39,     0,     0,     0,     0,     0,     0,     0,    40,
      41,    42,     0,    43,     0,     0,     0,     0,    44,     0,
       0,    45,     0,    46,    47,     0,    48,     0,     0,     0,
       0,     0,     0,    49,     0,     0,    50,     0,     0,     0,
      51,     0,     0,     0,    52,     0,     0,     0,     0,     0,
       0,    53,    54,    55,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,    63,    64,    65,     0,
       0,    66,     0,     0,     0,    67,     0,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,     0,    78,    79,     0,    80,     0,
      81,    82,    83,     0,     0,     0,    84,     0,     0,    85,
      86,    87,     0,     0,    88,    89,    90,    91,    92,     0,
       0,     0,     0,    93,     0,     0,    94,    95,     0,     0,
       0,    96,    97,    98,     0,     0,     0,     0,     0,     0,
       0,    99,     0,   100,   101,   102,   103,     0,   104,   105,
     106,   107,   108,   109,   110,   111,   112,     0,   113,   114,
     115,   116,   117,     0,   118,   119,   120,   121,   122,   123,
     124,   125,     0,     0,     0,     0,   126,     0,     0,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
     137,   138,   139,   140,   141,   142,   143,   144,     0,   145,
     146,     0,     0,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,     0,   162,
     163,     0,   164,   165,   166,   167,   168,   169,   170,     0,
    1000,   172,  1197,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,     0,     0,    31,     0,    32,
       0,     0,     0,    34,     0,     0,     0,     0,     0,    35,
       0,     0,    37,     0,     0,    38,    39,     0,     0,     0,
       0,     0,     0,     0,    40,    41,     0,     0,     0,     0,
       0,     0,     0,    44,     0,     0,    45,     0,     0,    47,
       0,    48,     0,     0,     0,     0,     0,     0,    49,     0,
       0,    50,     0,     0,     0,    51,     0,     0,     0,    52,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,    57,    58,     0,     0,    59,     0,     0,
      62,    63,    64,    65,     0,     0,    66,     0,     0,     0,
      67,     0,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,     0,    74,    75,     0,     0,    77,     0,
      78,    79,     0,    80,     0,     0,    82,    83,     0,     0,
       0,    84,     0,     0,     0,     0,     0,     0,     0,    88,
      89,    90,    91,    92,     0,     0,     0,     0,    93,     0,
       0,     0,     0,     0,     0,     0,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,    99,     0,   100,   101,
     102,     0,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,     0,   113,   114,   115,   116,   117,     0,   118,
     119,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   126,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,     0,   162,   163,     0,   164,   165,   166,
     167,   168,   169,   170,     0,   171,   172,     0,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   182,   183,   184
};

static const yytype_int16 yycheck[] =
{
       2,   215,   259,   443,   359,     7,     8,     9,     2,   251,
     467,     1,   242,   453,     8,     9,   203,   204,   208,   491,
     210,   211,   203,   204,   214,   422,   423,   226,    13,   270,
     352,    13,    13,   482,   119,    13,    13,   270,   466,    13,
      13,   260,   365,    13,   366,   367,    13,   491,    13,    74,
      19,    74,    13,    35,    35,   858,    38,    38,    35,    14,
      13,    38,    35,    12,    12,    38,    35,    74,   678,    38,
      35,   394,    13,    38,    35,    13,    13,    38,   118,   272,
      16,    12,    35,   406,    16,    38,   409,    63,    13,     2,
     347,   414,   415,    74,     7,     8,     9,    35,    35,    35,
      38,    38,    38,    35,    13,    13,    38,    13,    68,    13,
      20,   295,   296,    13,    13,    13,    35,    22,    13,    38,
      13,    13,    13,   364,     3,    35,    13,   384,    38,   348,
      35,   364,    13,    38,    39,    40,    41,    42,    43,    44,
      13,   464,    13,    13,    13,    13,    13,    13,    13,    35,
      60,    63,    38,    39,  1201,    19,    91,    73,    72,   119,
     379,     6,   172,    35,     9,     8,    38,     0,     5,   194,
      12,   493,   353,    10,    35,    65,    81,    38,    35,   282,
     416,    38,   622,   185,    15,   118,    35,    92,   164,    38,
     139,   193,    97,   195,   430,   155,   198,   199,   283,   193,
     810,   195,   443,    35,   198,   110,    38,   123,   427,   532,
     443,   125,    35,    29,   118,    38,   149,    66,    13,   221,
     453,    35,    35,   225,    38,    38,    19,  1274,   183,    22,
      61,   225,   234,   235,   678,   166,   238,   116,   157,   432,
      39,    12,    41,    66,   149,   127,   286,   119,   238,   197,
     119,    12,   164,   135,   484,   419,   420,   421,   491,    86,
     283,    72,   481,   283,   212,   110,   715,   110,   525,   259,
     118,    22,   185,   463,  1187,  1078,   283,    93,   462,    14,
     193,    74,   195,    12,    35,   198,   199,    38,    39,    40,
      41,    42,    43,    44,   254,   744,   156,   139,   283,    90,
     519,   149,   166,   208,   485,   283,   487,   488,   221,   283,
     292,   292,   225,   283,   125,   292,   283,     6,  1231,   292,
       9,   234,   235,   292,   166,   238,   681,   292,    74,   543,
      81,   292,   687,   688,   689,   792,   550,   551,   552,   292,
     695,    92,   283,   345,   816,     5,    97,   744,   820,   351,
      10,   353,   166,   802,   292,   292,   292,   359,   283,   110,
     292,   789,   790,   268,   813,   355,   810,   170,   139,   285,
     814,   287,   816,   292,   283,   283,   820,   283,   139,   701,
      22,   622,   292,   283,   283,   283,   291,   292,   283,   622,
     283,   283,   283,   557,   384,   166,   283,    12,   258,    14,
      50,   641,   283,   263,   264,   166,   292,    27,   410,   399,
     283,   839,   283,   283,   416,   283,   283,   283,   283,     5,
     292,   165,   283,   123,    10,     5,   283,    77,   430,    15,
      10,   292,   345,   243,   436,   292,   246,   269,   351,    81,
     353,    19,   436,   292,   674,   678,   359,   141,    13,    88,
      89,   158,   671,   156,    73,    97,    12,   208,   166,   109,
     292,   879,   880,   881,   466,   467,   165,   166,   470,   292,
     472,    12,    12,    12,   476,   830,    12,   949,   292,   292,
     482,    12,   273,   274,   275,   276,   277,   905,    28,   491,
      12,   690,   482,    12,   938,   676,     4,   499,   411,   489,
     150,    12,   742,    68,     4,   949,   209,   210,    73,   749,
     750,   289,   290,   940,   153,   279,   280,   268,    58,   946,
     947,    86,   284,   436,     4,     5,     6,     7,     8,    12,
      10,   293,   294,     4,     5,     6,     7,     8,    12,    10,
     291,   292,    22,    83,    12,   785,     4,     5,     6,     7,
       8,    12,    10,   466,   119,   258,    12,   470,   118,   472,
     263,   264,   244,   476,   246,   247,   208,  1016,    54,   482,
      50,   828,   791,    53,   793,    48,    12,   810,   491,    50,
    1008,  1009,    53,   816,    12,    48,   499,   820,   807,    12,
     155,   175,   176,   177,    80,    53,    88,    89,    90,   139,
      12,   166,    75,   165,   166,   170,    88,    89,    90,    82,
      12,  1039,    75,    48,    12,   101,   156,   103,    12,    82,
      93,   200,   201,    96,   843,    12,   166,    12,   815,    12,
      93,   173,   174,    96,    88,    89,    90,    12,   178,   179,
      75,    12,   644,   116,   273,   274,   275,    82,   888,   889,
     196,   197,   138,   116,   656,   657,    88,    89,    93,    12,
     133,    96,    88,    89,    90,   240,   241,   242,    88,    89,
     133,    12,   152,    12,   154,    19,   678,    88,    89,   681,
      12,   152,   168,   154,    92,   687,   688,   689,   678,   254,
      16,  1014,   288,   695,   152,     2,   821,   822,   133,    12,
       7,     8,     9,   960,    12,   938,    12,   283,    12,    14,
      12,  1139,    12,   715,    12,    14,   949,    12,    48,   171,
      13,  1118,   164,     5,     5,     4,    12,    39,    39,   115,
     732,   644,     4,   292,     4,    73,    14,     5,   732,   283,
    1168,   743,   744,   656,   657,    75,   180,   166,   199,   180,
       5,   242,    82,   115,   259,    12,   283,    13,    74,    13,
      13,    13,   269,    93,    13,   678,    96,  1024,   681,   959,
    1219,    13,    13,    13,   687,   688,   689,    13,  1035,  1219,
      13,   783,   695,     4,     4,   787,   116,   789,   790,     4,
     792,  1010,     4,    12,    12,   797,    12,  1194,    12,  1122,
     802,   803,   715,   133,  1276,    12,    12,   809,   810,  1132,
      12,   813,   814,  1253,   816,    18,     8,    12,   820,   732,
     810,   115,    39,    14,   814,   122,  1223,   829,   830,    14,
      13,   744,  1276,    14,   278,  1092,  1233,   839,  1095,  1288,
      14,   198,   198,    19,    12,    12,   283,    77,  1288,    14,
    1064,   176,   181,   181,   255,     4,   265,     4,    13,    64,
      13,    13,    13,    13,     4,   234,    74,     5,    81,     5,
     783,     5,     5,     5,   787,     5,   789,   790,   185,     5,
       4,     4,    93,    14,   797,     5,   193,   115,   195,   802,
     803,   198,   199,    79,    13,   121,   809,   810,    27,  1296,
     813,   814,    12,   816,   906,   283,     5,   820,   257,    14,
     912,   913,    39,    13,   221,    13,   829,   830,   225,    23,
      77,    25,    26,    13,    13,    13,   839,   234,   235,    13,
      34,   238,  1174,    13,    13,    13,   938,    14,    14,   210,
      14,    12,    12,   153,    13,    13,    89,   949,    93,    53,
      14,   285,    56,   153,    19,    14,     4,     4,    62,    12,
     211,     5,    13,   123,   256,    14,   267,    71,    14,   153,
     164,    74,     4,   115,     4,    13,   115,   287,    12,    12,
      84,    85,    13,    12,    12,    12,    12,   283,     4,    74,
      13,   119,    27,   906,    14,    14,     4,    14,     3,   912,
     913,     4,  1004,   192,   108,    16,  1008,  1009,    89,   153,
    1012,  1013,     4,    12,  1016,     5,    89,    12,    14,   283,
     157,   142,     4,     4,     4,   938,   130,    13,   153,    12,
     239,   281,  1034,    89,    19,   213,   949,  1039,   345,   143,
      64,     4,     4,  1276,   351,    13,   353,   151,   152,   286,
       5,     5,   359,     5,    12,   159,   160,    13,     4,  1061,
      13,     3,  1319,    13,    13,  1067,  1068,  1061,    13,    12,
      77,  1285,    13,    13,   166,   125,   134,    12,   153,    93,
     184,   185,   186,   187,   188,   189,   190,   191,   125,    93,
     153,  1004,    69,   249,    16,  1008,  1009,     4,  1100,  1012,
    1013,    74,    93,  1016,    93,    93,    93,   120,    13,   134,
     251,   166,     4,   153,   207,   250,   488,   225,  1108,   428,
     172,  1034,   244,    25,  1171,   699,  1039,   245,   495,   436,
     247,   198,  1288,   248,  1253,  1245,  1138,   253,   252,  1141,
     234,  1143,   199,   250,   715,   488,   492,  1093,  1061,   938,
    1100,  1034,  1204,  1016,  1067,  1068,  1143,   431,   461,   466,
     622,  1125,   452,   470,  1053,   472,  1168,   608,   457,   476,
     759,   526,   906,  1058,   510,   482,  1194,   743,  1296,   969,
     517,   729,   720,   353,   491,   476,     6,  1100,   690,   513,
     383,   792,   499,   375,    -1,    -1,    -1,    -1,    -1,  1201,
      -1,    -1,    -1,     2,    -1,    -1,    -1,    -1,     7,     8,
       9,    -1,    -1,    -1,    -1,    -1,    -1,  1219,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1138,  1139,    -1,  1141,    -1,
    1143,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1168,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1274,    -1,  1276,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1288,    -1,  1201,    -1,
      -1,    -1,    -1,  1283,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1219,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   644,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   656,
     657,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1274,    -1,  1276,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   678,    -1,    -1,   681,  1288,    -1,    -1,    -1,    -1,
     687,   688,   689,    -1,    -1,    -1,   185,    -1,   695,    -1,
      -1,    -1,    -1,    -1,   193,    -1,   195,    -1,    -1,   198,
     199,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   715,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   221,    -1,    -1,   732,   225,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   234,   235,   744,    -1,   238,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   783,    -1,    -1,    -1,
     787,    -1,   789,   790,    -1,    -1,    -1,    -1,    -1,    -1,
     797,    -1,    -1,    -1,    -1,   802,   803,    -1,    -1,    -1,
      -1,    -1,   809,   810,    -1,    -1,   813,   814,    -1,   816,
      -1,    -1,    -1,   820,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   829,   830,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   839,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   345,    -1,    -1,    -1,
      -1,    -1,   351,    -1,   353,    -1,    -1,    -1,    -1,    -1,
     359,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   906,
      -1,    -1,    -1,    -1,    -1,   912,   913,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     2,    -1,    -1,    -1,    -1,     7,     8,
       9,   938,    -1,    -1,    -1,    -1,    -1,   436,    -1,    -1,
      -1,    -1,   949,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   466,    -1,    -1,
      -1,   470,    -1,   472,    -1,    -1,    -1,   476,    -1,    -1,
      -1,    -1,    -1,   482,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   491,    -1,    -1,    -1,    -1,  1004,    -1,    -1,
     499,  1008,  1009,    -1,    -1,  1012,  1013,    -1,    -1,  1016,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1034,    -1,    -1,
      -1,    -1,  1039,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1061,    -1,    -1,    -1,    -1,    -1,
    1067,  1068,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1100,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   185,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   193,    -1,   195,    -1,    -1,   198,
     199,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1138,    -1,    -1,  1141,    -1,  1143,    -1,    -1,    -1,
      -1,    -1,   221,    -1,    -1,   644,   225,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   234,   235,   656,   657,   238,
      -1,  1168,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   678,
      -1,    -1,   681,    -1,    -1,    -1,    -1,    -1,   687,   688,
     689,    -1,    -1,    -1,  1201,    -1,   695,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1219,    -1,    -1,    -1,   715,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   732,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   744,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   345,  1274,    -1,  1276,
      -1,    -1,   351,    -1,   353,    -1,    -1,    -1,    -1,    -1,
     359,  1288,    -1,    -1,   783,    -1,    -1,    -1,   787,    -1,
     789,   790,    -1,    -1,    -1,    -1,    -1,    -1,   797,    -1,
      -1,    -1,    -1,   802,   803,    -1,    -1,    -1,    -1,    -1,
     809,   810,    -1,    -1,   813,   814,    -1,   816,    -1,    -1,
      -1,   820,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     829,   830,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     839,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   436,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   470,    -1,   472,    -1,    -1,    -1,   476,    -1,    -1,
      -1,    -1,    -1,   482,    -1,    -1,    -1,   906,    -1,    -1,
      -1,    -1,    -1,   912,   913,    -1,    -1,    -1,    -1,    -1,
     499,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   938,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     949,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   231,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1004,    -1,    -1,    -1,  1008,
    1009,   245,    -1,  1012,  1013,    -1,    -1,  1016,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   259,    -1,   261,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1034,    -1,    -1,   272,    -1,
    1039,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1061,    -1,    -1,   644,    -1,    -1,  1067,  1068,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   656,   657,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1100,   681,    -1,    -1,    -1,    -1,    -1,   687,   688,
     689,    -1,    -1,   347,    -1,    -1,   695,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   360,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   715,    -1,    -1,  1138,
      -1,    -1,  1141,    -1,  1143,    -1,    -1,    -1,    -1,    -1,
     384,    -1,    -1,   732,    -1,    -1,    -1,    -1,   392,    -1,
      -1,   395,   396,    -1,    -1,   744,    -1,    -1,   402,  1168,
      -1,   405,    -1,    -1,   408,    -1,   410,    -1,   412,   413,
      -1,    -1,   416,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     424,    -1,    -1,    -1,   428,    -1,   430,   431,   432,    -1,
     434,    -1,  1201,    -1,   783,    -1,    -1,    -1,   787,    -1,
      -1,   445,    -1,    -1,   448,    -1,    -1,   451,   797,    -1,
    1219,    -1,    -1,   802,   803,    -1,    -1,    -1,    -1,    -1,
     809,   465,    -1,    -1,   813,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     829,   830,    -1,    -1,    -1,    -1,    -1,    -1,   492,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1274,   510,  1276,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1288,
     524,   525,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   906,    -1,    -1,
      -1,    -1,    -1,   912,   913,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   621,    -1,   623,
      -1,   625,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   640,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1004,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1012,  1013,    -1,    -1,  1016,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1034,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   699,    -1,    -1,   702,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1061,    -1,    -1,    -1,    -1,    -1,  1067,  1068,
      -1,    -1,   726,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   737,   738,    -1,    -1,    -1,    -1,   743,
      -1,   745,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1100,    -1,    -1,    -1,    -1,    -1,   761,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   773,
      -1,   775,    -1,    -1,    -1,   779,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1138,
    1139,    -1,  1141,    -1,  1143,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   828,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1201,    -1,    -1,    -1,    -1,   861,   862,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1219,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   886,    -1,    -1,    -1,    -1,   891,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   901,   902,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1274,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1288,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   960,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1024,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,  1035,    -1,    -1,    -1,    -1,  1040,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    17,    18,    19,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    -1,    35,    36,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,
      -1,    54,    -1,    -1,    57,    -1,    59,    60,  1092,    62,
      -1,  1095,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    -1,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
      -1,   134,    -1,   136,   137,   138,    -1,  1171,    -1,   142,
      -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,  1245,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,    -1,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,    -1,     3,
       4,     5,     6,     7,     8,  1319,    10,    -1,    12,   292,
      -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    51,    52,    53,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    96,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,   110,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,   133,
     134,    -1,   136,   137,   138,   139,   140,    -1,   142,    -1,
      -1,   145,   146,   147,   148,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,   166,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,     5,
       6,     7,     8,    -1,    10,    -1,    12,    13,    -1,    -1,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    51,    52,    53,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    70,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      96,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,   110,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,   133,   134,    -1,
     136,   137,   138,    -1,   140,    -1,   142,    -1,    -1,   145,
     146,   147,   148,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,   225,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   289,   290,     3,     4,     5,     6,     7,
       8,    -1,    10,    -1,    12,    -1,    -1,    -1,    -1,    17,
      18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    29,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    51,    52,    53,    54,    -1,    -1,    57,
      -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      78,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    96,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,   133,   134,    -1,   136,   137,
     138,    -1,   140,    -1,   142,    -1,    -1,   145,   146,   147,
     148,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
      -1,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   289,   290,     3,     4,     5,     6,     7,     8,    -1,
      10,    -1,    12,    13,    -1,    -1,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    29,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    51,    52,    53,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,   133,   134,    -1,   136,   137,   138,   139,
     140,    -1,   142,    -1,    -1,   145,   146,   147,   148,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,   166,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,   225,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,    -1,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   289,
     290,     3,     4,     5,     6,     7,     8,    -1,    10,    -1,
      12,    13,    -1,    -1,    -1,    17,    18,    -1,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    29,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,
      52,    53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    96,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,   133,   134,    -1,   136,   137,   138,   139,   140,    -1,
     142,    -1,    -1,   145,   146,   147,   148,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,   166,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,    -1,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   289,   290,     3,
       4,     5,     6,     7,     8,    -1,    10,    -1,    12,    -1,
      -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    51,    52,    53,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    96,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,   110,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,   133,
     134,    -1,   136,   137,   138,    -1,   140,    -1,   142,    -1,
      -1,   145,   146,   147,   148,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,     5,
       6,     7,     8,    -1,    10,    -1,    12,    -1,    -1,    -1,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    51,    52,    53,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      96,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,   133,   134,    -1,
     136,   137,   138,   139,   140,    -1,   142,    -1,    -1,   145,
     146,   147,   148,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
     166,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,   225,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   289,   290,     3,     4,     5,     6,     7,
       8,    -1,    10,    -1,    12,    -1,    -1,    -1,    -1,    17,
      18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    29,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    51,    52,    53,    54,    -1,    -1,    57,
      -1,    59,    60,    61,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    96,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,   133,   134,    -1,   136,   137,
     138,    -1,   140,    -1,   142,    -1,    -1,   145,   146,   147,
     148,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,    -1,
      -1,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
      -1,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,    -1,    -1,    -1,   284,    -1,    -1,    -1,
      -1,   289,   290,     3,     4,     5,     6,     7,     8,    -1,
      10,    -1,    12,    -1,    -1,    15,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    29,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    51,    52,    53,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,   133,   134,    -1,   136,   137,   138,    -1,
     140,    -1,   142,    -1,   144,   145,   146,   147,   148,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,   225,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,    -1,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   289,
     290,     3,     4,     5,     6,     7,     8,    -1,    10,    -1,
      12,    -1,    -1,    -1,    -1,    17,    18,    -1,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    29,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,
      52,    53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    96,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,   133,   134,    -1,   136,   137,   138,    -1,   140,    -1,
     142,    -1,    -1,   145,   146,   147,   148,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,    -1,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
      -1,    -1,   284,    -1,    -1,    -1,    -1,   289,   290,     3,
       4,     5,     6,     7,     8,    -1,    10,    -1,    12,    13,
      -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    51,    52,    53,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    96,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,   133,
     134,    -1,   136,   137,   138,    -1,   140,    -1,   142,    -1,
      -1,   145,   146,   147,   148,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,     5,
       6,     7,     8,    -1,    10,    -1,    12,    -1,    -1,    -1,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    51,    52,    53,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      96,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,   133,   134,    -1,
     136,   137,   138,    -1,   140,    -1,   142,    -1,    -1,   145,
     146,   147,   148,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,   225,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    -1,    -1,   284,    -1,
      -1,    -1,    -1,   289,   290,     3,     4,     5,     6,     7,
       8,    -1,    10,    -1,    12,    13,    -1,    -1,    -1,    17,
      18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    29,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    51,    52,    53,    54,    -1,    -1,    57,
      -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    96,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,   133,   134,    -1,   136,   137,
     138,    -1,   140,    -1,   142,    -1,    -1,   145,   146,   147,
     148,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,    -1,
      -1,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
      -1,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   289,   290,     3,     4,     5,     6,     7,     8,    -1,
      10,    -1,    12,    13,    -1,    -1,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    29,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    51,    52,    53,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    96,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,   133,   134,    -1,   136,   137,   138,    -1,
     140,    -1,   142,    -1,    -1,   145,   146,   147,   148,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,   225,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,    -1,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   289,
     290,     3,     4,     5,     6,     7,     8,    -1,    10,    -1,
      12,    13,    -1,    -1,    -1,    17,    18,    -1,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    29,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,
      52,    53,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    96,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,   133,   134,    -1,   136,   137,   138,    -1,   140,    -1,
     142,    -1,    -1,   145,   146,   147,   148,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,    -1,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   289,   290,     3,
       4,     5,     6,     7,     8,    -1,    10,    -1,    12,    -1,
      -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    29,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    51,    52,    53,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    96,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,   133,
     134,    -1,   136,   137,   138,    -1,   140,    -1,   142,    -1,
      -1,   145,   146,   147,   148,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   289,   290,     3,     4,     5,
       6,     7,     8,    -1,    10,    -1,    12,    -1,    -1,    -1,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    29,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    51,    52,    53,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      96,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,   133,   134,    -1,
     136,   137,   138,    -1,   140,    -1,   142,    -1,    -1,   145,
     146,   147,   148,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,   225,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    -1,    -1,     3,    -1,
      -1,    -1,    -1,   289,   290,    10,    -1,    12,    -1,    -1,
      -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,    -1,   134,
      -1,   136,   137,   138,   139,    -1,    -1,   142,    -1,    -1,
     145,   146,   147,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,   166,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
      -1,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,     3,    -1,    -1,    -1,
      -1,    -1,   287,    10,    -1,    12,    -1,    -1,    -1,    -1,
      17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,   214,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,    -1,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,     3,    -1,    -1,    -1,    -1,    -1,
     287,    -1,    -1,    12,    -1,    -1,    -1,    -1,    17,    18,
      -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,    -1,   134,    -1,   136,   137,   138,
      -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,   214,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,    -1,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,     3,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,    -1,    17,    18,    -1,    20,    21,   287,    -1,
      24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    -1,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,    -1,
     134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,
      -1,   145,   146,   147,    -1,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,     3,    -1,    -1,
      -1,    -1,    -1,   287,    -1,    -1,    -1,    -1,    14,    -1,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,     3,    -1,    -1,   284,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    14,    -1,    -1,    17,
      18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,
      -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,    -1,   134,    -1,   136,   137,
     138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,
      -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,    -1,
      -1,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
      -1,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,     3,    -1,    -1,   284,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    14,    -1,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,    -1,   134,    -1,   136,   137,   138,    -1,
      -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,    -1,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,     3,    -1,    -1,   284,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    14,    -1,    -1,    17,    18,    -1,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,
      -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,
     142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,     3,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
      17,    18,   284,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,     3,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,    -1,    17,    18,   284,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,
      -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,
     142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,     3,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
      17,    18,   284,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,     3,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,    -1,    17,    18,   284,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,
      -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,
     142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,     3,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
      17,    18,   284,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,     3,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,    -1,    17,    18,   284,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,
      -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,
     142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,     3,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,    -1,
      17,    18,   284,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,     3,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,    -1,    17,    18,   284,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    51,
      52,    -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    96,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,   133,   134,    -1,   136,   137,   138,    -1,   140,    -1,
     142,    -1,    -1,   145,   146,   147,   148,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,    -1,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,     3,
       4,     5,     6,     7,     8,    -1,    10,    -1,    -1,    -1,
      -1,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,    53,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    -1,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,    -1,
     134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,
      -1,   145,   146,   147,    -1,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,    -1,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,     3,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    12,    -1,    -1,    -1,
      -1,    17,    18,    19,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,   139,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
     166,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,    -1,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,     3,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    12,    -1,    -1,    -1,    -1,    17,
      18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,
      -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,
      -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,
      98,    99,   100,   101,   102,   103,   104,    -1,    -1,   107,
      -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,
      -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,
     128,   129,    -1,   131,   132,    -1,   134,    -1,   136,   137,
     138,   139,    -1,    -1,   142,    -1,    -1,   145,   146,   147,
      -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,
      -1,   159,    -1,    -1,   162,   163,    -1,    -1,   166,   167,
     168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,   179,   180,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,   194,   195,   196,   197,
     198,    -1,   200,   201,   202,   203,   204,   205,   206,   207,
      -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,   227,
     228,   229,   230,   231,   232,   233,    -1,   235,   236,    -1,
      -1,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,    -1,   255,   256,    -1,
     258,   259,   260,   261,   262,   263,   264,    -1,   266,   267,
      -1,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,     3,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    12,    -1,    -1,    -1,    -1,    17,    18,    -1,
      20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,
      30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,
      -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,
      60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,
      90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,
     100,   101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,
      -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,
     120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,
      -1,   131,   132,    -1,   134,    -1,   136,   137,   138,   139,
      -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,
     150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,
      -1,    -1,   162,   163,    -1,    -1,   166,   167,   168,   169,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,
     180,   181,   182,    -1,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,   194,   195,   196,   197,   198,    -1,
     200,   201,   202,   203,   204,   205,   206,   207,    -1,    -1,
      -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,   227,   228,   229,
     230,   231,   232,   233,    -1,   235,   236,    -1,    -1,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,    -1,   255,   256,    -1,   258,   259,
     260,   261,   262,   263,   264,    -1,   266,   267,    -1,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,     3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    14,    -1,    -1,    17,    18,    -1,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,
      -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,
      -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,
      62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,
      -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,
      -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,   101,
     102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,
      -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,
      -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,   131,
     132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,
     142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,
     152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,
     162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,
     182,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,   194,   195,   196,   197,   198,    -1,   200,   201,
     202,   203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,
     212,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,   227,   228,   229,   230,   231,
     232,   233,    -1,   235,   236,    -1,    -1,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,    -1,   255,   256,    -1,   258,   259,   260,   261,
     262,   263,   264,    -1,   266,   267,    -1,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,     3,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      14,    -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,
      24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,
      -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,
      54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,
      -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,
      -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,
      94,    95,    -1,    -1,    98,    99,   100,   101,   102,   103,
     104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,
     114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,
     124,   125,   126,    -1,   128,   129,    -1,   131,   132,    -1,
     134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,
      -1,   145,   146,   147,    -1,    -1,   150,   151,   152,   153,
     154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,
      -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
     194,   195,   196,   197,   198,    -1,   200,   201,   202,   203,
     204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,   227,   228,   229,   230,   231,   232,   233,
      -1,   235,   236,    -1,    -1,   239,   240,   241,   242,   243,
     244,   245,   246,   247,   248,   249,   250,   251,   252,   253,
      -1,   255,   256,    -1,   258,   259,   260,   261,   262,   263,
     264,    -1,   266,   267,     3,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,    -1,    17,    18,
      19,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,    -1,   134,    -1,   136,   137,   138,
      -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,    -1,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,    -1,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,     3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    -1,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,    -1,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
       3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    14,    -1,    -1,    17,    18,    -1,    20,    21,    -1,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    -1,    -1,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    45,    46,    47,    -1,    49,    -1,    -1,    -1,
      -1,    54,    -1,    -1,    57,    -1,    59,    60,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    72,
      -1,    -1,    -1,    76,    -1,    -1,    -1,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    87,    88,    89,    90,    -1,    -1,
      -1,    94,    95,    -1,    -1,    98,    99,   100,   101,   102,
     103,   104,    -1,    -1,   107,    -1,    -1,    -1,   111,    -1,
     113,   114,   115,    -1,   117,    -1,    -1,   120,    -1,    -1,
      -1,   124,   125,   126,    -1,   128,   129,    -1,   131,   132,
      -1,   134,    -1,   136,   137,   138,    -1,    -1,    -1,   142,
      -1,    -1,   145,   146,   147,    -1,    -1,   150,   151,   152,
     153,   154,    -1,    -1,    -1,    -1,   159,    -1,    -1,   162,
     163,    -1,    -1,    -1,   167,   168,   169,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,   179,   180,   181,   182,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,   194,   195,   196,   197,   198,    -1,   200,   201,   202,
     203,   204,   205,   206,   207,    -1,    -1,    -1,    -1,   212,
      -1,    -1,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   224,    -1,   226,   227,   228,   229,   230,   231,   232,
     233,    -1,   235,   236,    -1,    -1,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   250,   251,   252,
     253,    -1,   255,   256,    -1,   258,   259,   260,   261,   262,
     263,   264,    -1,   266,   267,    -1,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,     3,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    12,    -1,    -1,
      -1,    -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,
      -1,    -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,
      -1,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      45,    46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,
      -1,    -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,
      -1,    76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,
      -1,    -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,    98,    99,   100,   101,   102,   103,   104,
      -1,    -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,
     115,    -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,
     125,   126,    -1,   128,   129,    -1,   131,   132,    -1,   134,
      -1,   136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,
     145,   146,   147,    -1,    -1,   150,   151,   152,   153,   154,
      -1,    -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,
      -1,    -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,   179,   180,   181,   182,    -1,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,   194,
     195,   196,   197,   198,    -1,   200,   201,   202,   203,   204,
     205,   206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
      -1,   226,   227,   228,   229,   230,   231,   232,   233,    -1,
     235,   236,    -1,    -1,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,    -1,
     255,   256,    -1,   258,   259,   260,   261,   262,   263,   264,
      -1,   266,   267,    -1,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,     3,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    14,    -1,    -1,
      17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,    36,
      37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,    46,
      47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,    -1,
      57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,    76,
      -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,
      87,    88,    89,    90,    -1,    -1,    -1,    94,    95,    -1,
      -1,    98,    99,   100,   101,   102,   103,   104,    -1,    -1,
     107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,    -1,
     117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,   126,
      -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,   136,
     137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,   146,
     147,    -1,    -1,   150,   151,   152,   153,   154,    -1,    -1,
      -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,    -1,
     167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,   179,   180,   181,   182,    -1,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,   194,   195,   196,
     197,   198,    -1,   200,   201,   202,   203,   204,   205,   206,
     207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,   216,
     217,   218,   219,   220,   221,   222,   223,   224,    -1,   226,
     227,   228,   229,   230,   231,   232,   233,    -1,   235,   236,
      -1,    -1,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,   252,   253,    -1,   255,   256,
      -1,   258,   259,   260,   261,   262,   263,   264,    -1,   266,
     267,    -1,   269,   270,   271,   272,   273,   274,   275,   276,
     277,   278,   279,   280,     3,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    14,    -1,    -1,    17,    18,
      -1,    20,    21,    -1,    -1,    24,    -1,    -1,    -1,    -1,
      -1,    30,    31,    -1,    33,    -1,    -1,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,    -1,
      49,    -1,    -1,    -1,    -1,    54,    -1,    -1,    57,    -1,
      59,    60,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,
      -1,    80,    -1,    -1,    -1,    -1,    -1,    -1,    87,    88,
      89,    90,    -1,    -1,    -1,    94,    95,    -1,    -1,    98,
      99,   100,   101,   102,   103,   104,    -1,    -1,   107,    -1,
      -1,    -1,   111,    -1,   113,   114,   115,    -1,   117,    -1,
      -1,   120,    -1,    -1,    -1,   124,   125,   126,    -1,   128,
     129,    -1,   131,   132,    -1,   134,    -1,   136,   137,   138,
      -1,    -1,    -1,   142,    -1,    -1,   145,   146,   147,    -1,
      -1,   150,   151,   152,   153,   154,    -1,    -1,    -1,    -1,
     159,    -1,    -1,   162,   163,    -1,    -1,    -1,   167,   168,
     169,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
     179,   180,   181,   182,    -1,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,   194,   195,   196,   197,   198,
      -1,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,    -1,   212,    -1,    -1,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,    -1,   226,   227,   228,
     229,   230,   231,   232,   233,    -1,   235,   236,    -1,    -1,
     239,   240,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,    -1,   255,   256,    -1,   258,
     259,   260,   261,   262,   263,   264,    -1,   266,   267,    -1,
     269,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,     3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    12,    -1,    -1,    -1,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,     3,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,     3,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,     3,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,     3,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    17,    18,    -1,    20,
      21,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      31,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    47,    -1,    49,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    59,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    99,   100,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,   124,   125,   126,    -1,   128,   129,    -1,
     131,   132,    -1,   134,    -1,   136,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,   145,   146,   147,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,   162,   163,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,   202,   203,   204,   205,   206,   207,    -1,    -1,    -1,
      -1,   212,    -1,    -1,   215,   216,   217,   218,   219,   220,
     221,   222,   223,   224,    -1,   226,   227,   228,   229,   230,
     231,   232,   233,    -1,   235,   236,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,     3,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      -1,    17,    18,    -1,    20,    21,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    45,
      46,    47,    -1,    49,    -1,    -1,    -1,    -1,    54,    -1,
      -1,    57,    -1,    59,    60,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    72,    -1,    -1,    -1,
      76,    -1,    -1,    -1,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    87,    88,    89,    90,    -1,    -1,    -1,    94,    95,
      -1,    -1,    98,    99,   100,   101,   102,   103,   104,    -1,
      -1,   107,    -1,    -1,    -1,   111,    -1,   113,   114,   115,
      -1,   117,    -1,    -1,   120,    -1,    -1,    -1,   124,   125,
     126,    -1,   128,   129,    -1,   131,   132,    -1,   134,    -1,
     136,   137,   138,    -1,    -1,    -1,   142,    -1,    -1,   145,
     146,   147,    -1,    -1,   150,   151,   152,   153,   154,    -1,
      -1,    -1,    -1,   159,    -1,    -1,   162,   163,    -1,    -1,
      -1,   167,   168,   169,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   179,   180,   181,   182,    -1,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,   194,   195,
     196,   197,   198,    -1,   200,   201,   202,   203,   204,   205,
     206,   207,    -1,    -1,    -1,    -1,   212,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,   227,   228,   229,   230,   231,   232,   233,    -1,   235,
     236,    -1,    -1,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,   252,   253,    -1,   255,
     256,    -1,   258,   259,   260,   261,   262,   263,   264,    -1,
     266,   267,     3,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,    -1,    -1,    18,    -1,    20,
      -1,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    30,
      -1,    -1,    33,    -1,    -1,    36,    37,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    45,    46,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    54,    -1,    -1,    57,    -1,    -1,    60,
      -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,    80,
      -1,    -1,    -1,    -1,    -1,    -1,    87,    88,    89,    90,
      -1,    -1,    -1,    94,    95,    -1,    -1,    98,    -1,    -1,
     101,   102,   103,   104,    -1,    -1,   107,    -1,    -1,    -1,
     111,    -1,   113,   114,   115,    -1,   117,    -1,    -1,   120,
      -1,    -1,    -1,    -1,   125,   126,    -1,    -1,   129,    -1,
     131,   132,    -1,   134,    -1,    -1,   137,   138,    -1,    -1,
      -1,   142,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   150,
     151,   152,   153,   154,    -1,    -1,    -1,    -1,   159,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   167,   168,   169,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,   179,   180,
     181,    -1,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,   194,   195,   196,   197,   198,    -1,   200,
     201,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,    -1,   255,   256,    -1,   258,   259,   260,
     261,   262,   263,   264,    -1,   266,   267,    -1,   269,   270,
     271,   272,    -1,    -1,    -1,    -1,    -1,   278,   279,   280
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,    12,    28,    58,    83,   139,   156,   166,   178,   179,
     296,   297,   298,   299,   304,   305,   309,   311,   312,   317,
     318,   319,   320,   321,   327,   347,   502,   503,   318,     3,
      17,    18,    20,    21,    24,    30,    31,    33,    36,    37,
      45,    46,    47,    49,    54,    57,    59,    60,    62,    69,
      72,    76,    80,    87,    88,    89,    90,    94,    95,    98,
      99,   100,   101,   102,   103,   104,   107,   111,   113,   114,
     115,   117,   120,   124,   125,   126,   128,   129,   131,   132,
     134,   136,   137,   138,   142,   145,   146,   147,   150,   151,
     152,   153,   154,   159,   162,   163,   167,   168,   169,   177,
     179,   180,   181,   182,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   194,   195,   196,   197,   198,   200,   201,
     202,   203,   204,   205,   206,   207,   212,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   226,   227,   228,
     229,   230,   231,   232,   233,   235,   236,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   255,   256,   258,   259,   260,   261,   262,   263,
     264,   266,   267,   269,   270,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   287,   303,   375,   437,   446,
     478,   484,   486,    74,   194,    91,   172,   487,   487,   131,
     348,   349,   484,   375,   375,     0,    65,   282,    68,   119,
     155,   254,   441,   442,    86,   319,    13,   484,    12,   166,
     499,    14,   313,   314,   375,    74,   375,     5,    10,    15,
      61,   355,   314,   348,   283,    12,   350,   351,    12,   317,
     350,   317,   350,   297,   355,    27,   355,   355,   170,   494,
     355,   441,     3,     4,     5,     6,     7,     8,    10,    12,
      13,    29,    30,    31,    36,    45,    47,    51,    52,    53,
      70,    96,   110,   111,   133,   140,   148,   152,   182,   207,
     215,   220,   221,   222,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,   233,   289,   290,   300,   301,   302,
     322,   323,   326,   374,   376,   392,   393,   394,   395,   396,
     397,   398,   399,   400,   406,   407,   408,   409,   410,   411,
     412,   413,   414,   415,   416,   417,   418,   428,   429,   434,
     436,   437,   438,   445,   446,   463,   467,   478,   484,   485,
     486,   515,   175,   176,   177,    14,   484,   165,   328,   123,
     509,   512,   313,   306,   509,    12,   284,   356,   357,   412,
     141,   349,   352,   484,    19,   158,   310,   310,   320,   412,
     443,   444,   320,   320,     5,    10,    73,   468,   469,   488,
     489,   490,   321,   494,    12,   318,   392,   412,   419,   499,
     412,   430,    12,    12,    12,    12,    12,    12,     4,    12,
     323,   325,    12,   394,   412,    12,    12,     4,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,   416,   416,    13,   283,    19,
      12,   118,    16,    92,    22,    35,    38,    39,    40,    41,
      42,    43,    44,    81,    92,    97,   110,   208,   268,   291,
     292,   423,   424,   425,   483,   289,   290,   426,   284,   293,
     294,   427,   288,    12,    12,    12,    14,    12,    12,    12,
      14,   484,    14,   392,   499,    12,    19,   480,   481,   482,
     484,   328,    12,   195,   307,   308,   317,   353,   481,    12,
     412,    74,   283,   329,   480,   315,   316,   412,    13,   283,
     325,   419,   166,   328,   501,   166,   328,    20,    60,   345,
     283,   171,   495,   495,   156,   258,   263,   264,   499,   488,
     318,    13,    13,    13,   283,   164,   431,   432,   412,     5,
     419,   412,    61,   284,   412,     5,   412,   412,    13,   419,
     284,   412,   420,   419,   412,   464,   465,   466,   484,   478,
     412,   412,   419,   419,   464,   279,   280,   517,     4,   516,
     516,   516,   516,   152,   428,   428,   270,   271,   272,   412,
     439,   499,   301,    23,    25,    26,    34,    53,    56,    62,
      71,    84,    85,   108,   130,   143,   151,   152,   159,   160,
     184,   185,   186,   187,   188,   189,   190,   191,   474,   475,
     476,   477,    13,   464,   393,   394,     6,     9,   110,   422,
     412,   375,    39,    39,    41,   325,   418,   421,     8,   110,
     412,    22,    81,    97,   208,   115,   412,    39,   292,    15,
      17,   144,   412,   479,   413,   323,   418,   414,   415,   416,
     355,    13,   419,   412,    14,   284,   484,   485,     3,   434,
     484,   497,   498,     4,     4,   484,    14,    14,   484,     5,
     510,   482,    73,   513,   514,   499,   354,   374,   484,   196,
     197,   310,   317,   317,   307,   317,   353,    10,    12,   193,
     214,   324,   325,   358,   359,   360,   361,   362,   363,   370,
     375,   377,   378,   385,   389,   463,   484,   357,   328,   283,
     329,   330,    39,   484,   180,   166,   500,   180,   199,   435,
     444,     5,   242,   496,   496,   115,   491,   259,    12,   471,
     471,   499,    13,   412,   392,   412,    63,   432,   433,    19,
      13,    13,   157,   419,    13,    13,    13,   283,   283,    13,
     283,   441,    13,   283,   183,    74,   441,   441,   441,    13,
      13,   516,   269,   518,    13,    13,    13,    13,    13,    13,
      13,    74,     4,    12,    12,    12,    12,    12,    12,    12,
      18,    13,   422,    16,     8,    66,   412,   421,   412,   115,
     412,   269,   412,   122,   448,    13,    13,    14,   484,    14,
      14,    13,   283,    13,    39,   484,   484,    14,    13,   283,
     278,    13,   283,    14,   198,   198,   499,   310,   317,    19,
      12,   359,   377,    12,    12,   480,   283,    48,    75,    82,
      93,    96,   133,   386,   480,   480,   480,    73,   285,   287,
     369,   490,   504,   505,   506,   507,   508,   509,   480,    14,
      77,   331,   316,   328,   412,   181,   176,   181,   200,   201,
     255,   354,   173,   174,   493,     4,   472,   473,   265,   470,
     470,   149,   149,   412,    64,   474,   375,    13,   447,   448,
     412,   412,     4,    13,   448,   465,   374,   428,   412,    13,
      13,    13,   448,   448,    13,     4,    81,   234,   440,   440,
     412,    74,     5,     5,     5,     5,     5,     5,     5,   412,
     412,    16,    66,   412,     4,    13,    12,   449,   450,   484,
     448,   484,    14,    14,   284,   484,   485,   484,   499,   497,
     499,     4,   484,     5,   115,   374,   484,   499,   484,    13,
      18,   371,   372,   374,   318,   463,   350,   359,    93,   121,
     387,    88,    89,    90,   388,   359,   387,   387,    29,    93,
     379,   209,   210,   392,   364,   365,   484,   480,   484,    27,
      79,   346,   499,    12,     5,   257,   492,    39,    13,   283,
     476,   412,   412,    13,    13,    13,    13,    13,   283,    13,
     447,   447,   447,   412,    77,   448,   448,    13,   412,    13,
     283,    13,    13,    13,    13,    13,    13,   412,   412,   447,
     266,   450,   451,   452,    14,   284,   484,   484,    14,    14,
      13,   210,    14,    12,    12,    13,   283,    13,    13,   360,
     388,   153,   153,    89,   116,   388,   388,    93,   359,    19,
      22,    74,    19,   286,    14,   285,   211,   368,   511,    14,
     355,   392,     4,   256,     4,   473,    90,   273,   274,   275,
     276,   277,   401,   402,   403,   404,   166,   519,    12,    13,
       5,   267,    13,   123,   453,   284,   484,    14,    14,   284,
     484,   485,   484,   499,    74,   484,   390,   391,   484,   419,
      19,   166,   373,   372,    88,    89,    90,    88,    89,    90,
     153,   392,   164,   380,   381,   116,   115,     4,     4,   115,
     287,   365,   367,   392,    12,   197,   212,   484,    12,    78,
     237,   238,   332,   333,   337,   412,    13,    12,    12,    12,
      12,    12,    12,    13,   403,   283,     4,    74,   520,   442,
      13,   375,    27,   441,   284,   484,   284,   484,    14,    14,
       4,    14,    13,   283,   476,    13,     3,   192,    89,    89,
      88,    89,    90,   392,    63,   381,   384,   392,     4,    16,
     153,     4,   364,   157,   366,     5,    12,    12,    14,    13,
     142,   283,   239,   338,   339,     4,   405,   428,     3,   116,
       4,     4,   419,   404,     5,    10,   523,   281,   521,    13,
     419,   127,   135,   454,   455,   284,   484,     3,   486,   153,
     391,    12,    19,    89,   149,   213,   382,    64,     4,     4,
      88,    89,   153,   286,    13,     5,     5,   333,     5,    12,
     494,    13,    13,   283,    13,    13,    13,    13,    13,    15,
     523,    77,   522,    22,    50,   154,   428,   456,   457,   458,
     459,     4,   352,     3,   382,    12,   383,   153,   153,    88,
      89,    13,    13,    12,   334,   335,   336,   374,   418,   166,
     340,   428,   523,   154,   428,   457,   460,   461,   134,   125,
     125,    69,   462,    13,    12,   356,    74,    88,    89,    88,
      89,    93,    93,    12,    13,   336,   418,    13,   283,   240,
     241,   242,   249,   344,    72,    72,    16,    50,    77,   109,
     150,   352,    13,   359,    93,    93,    93,    93,   441,   335,
       4,   243,   246,   341,   343,   460,   134,   120,    13,   116,
      13,   250,   244,   247,   245,   342,   392,   251,   166,     4,
     253,   248,   252,   153,   172
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   295,   296,   297,   297,   298,   298,   298,   298,   299,
     299,   299,   300,   300,   301,   301,   302,   302,   303,   304,
     305,   305,   305,   305,   305,   306,   306,   307,   307,   308,
     308,   309,   309,   309,   309,   309,   309,   309,   310,   311,
     311,   312,   313,   314,   315,   315,   316,   317,   318,   318,
     319,   319,   319,   319,   320,   320,   321,   321,   322,   323,
     324,   325,   326,   327,   328,   328,   329,   330,   330,   331,
     331,   332,   332,   333,   333,   333,   334,   334,   335,   335,
     335,   336,   336,   337,   337,   337,   338,   339,   339,   340,
     340,   340,   340,   341,   341,   342,   342,   343,   343,   343,
     344,   344,   344,   344,   345,   345,   345,   346,   346,   347,
     347,   348,   348,   349,   350,   350,   351,   352,   352,   353,
     354,   354,   355,   355,   355,   356,   356,   357,   357,   358,
     358,   359,   359,   359,   360,   360,   361,   361,   361,   361,
     361,   361,   362,   362,   363,   363,   364,   364,   365,   365,
     366,   366,   366,   366,   366,   366,   366,   367,   367,   368,
     368,   369,   369,   369,   369,   369,   370,   371,   371,   372,
     372,   373,   373,   373,   373,   374,   374,   374,   374,   374,
     374,   374,   374,   374,   374,   374,   374,   374,   374,   374,
     374,   374,   375,   375,   375,   375,   375,   375,   375,   375,
     375,   376,   376,   376,   376,   377,   377,   378,   378,   378,
     379,   380,   380,   381,   382,   383,   384,   384,   385,   386,
     386,   386,   386,   387,   387,   388,   388,   388,   388,   388,
     388,   388,   388,   388,   388,   389,   390,   390,   391,   392,
     392,   393,   393,   394,   394,   395,   395,   395,   396,   396,
     397,   397,   397,   397,   397,   397,   397,   397,   397,   398,
     398,   399,   399,   400,   400,   401,   401,   401,   402,   402,
     402,   402,   402,   402,   403,   403,   404,   404,   404,   404,
     405,   405,   406,   406,   406,   407,   407,   408,   408,   408,
     408,   409,   409,   410,   410,   411,   412,   412,   412,   413,
     413,   414,   414,   415,   415,   416,   416,   416,   417,   417,
     417,   417,   417,   417,   418,   419,   419,   420,   420,   421,
     421,   422,   422,   423,   423,   423,   423,   423,   423,   423,
     423,   423,   423,   424,   424,   425,   426,   426,   427,   427,
     427,   428,   428,   428,   428,   428,   428,   428,   428,   429,
     430,   430,   431,   431,   432,   432,   433,   433,   434,   434,
     434,   434,   434,   435,   435,   435,   436,   436,   436,   436,
     436,   437,   437,   437,   437,   437,   437,   437,   437,   437,
     437,   437,   437,   437,   438,   438,   438,   438,   438,   438,
     438,   438,   438,   438,   438,   438,   438,   438,   439,   439,
     439,   440,   441,   441,   442,   443,   443,   444,   445,   445,
     446,   446,   446,   446,   446,   446,   446,   446,   446,   446,
     446,   447,   447,   448,   448,   449,   450,   451,   452,   452,
     452,   453,   453,   454,   454,   455,   455,   456,   456,   457,
     457,   457,   458,   459,   460,   460,   460,   461,   462,   462,
     462,   462,   462,   463,   463,   464,   464,   465,   465,   466,
     466,   467,   467,   467,   467,   467,   467,   467,   467,   467,
     467,   467,   467,   467,   468,   469,   470,   470,   471,   471,
     472,   472,   473,   474,   474,   475,   476,   476,   476,   476,
     476,   476,   476,   476,   476,   476,   476,   476,   476,   476,
     476,   476,   476,   476,   476,   476,   476,   476,   476,   476,
     476,   476,   476,   476,   476,   476,   476,   477,   477,   477,
     478,   478,   478,   478,   478,   478,   479,   479,   479,   480,
     480,   481,   481,   482,   483,   484,   484,   484,   484,   484,
     484,   484,   484,   484,   484,   484,   484,   484,   484,   484,
     484,   484,   484,   484,   484,   484,   484,   484,   484,   484,
     484,   484,   484,   485,   485,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   486,   486,   486,
     486,   486,   486,   486,   486,   486,   486,   487,   487,   487,
     488,   488,   489,   490,   490,   490,   490,   491,   491,   492,
     492,   493,   493,   493,   494,   494,   494,   495,   495,   496,
     496,   497,   497,   498,   498,   499,   499,   499,   499,   500,
     500,   501,   502,   502,   502,   503,   503,   503,   504,   504,
     504,   505,   506,   507,   508,   509,   510,   510,   511,   511,
     511,   512,   512,   513,   513,   514,   515,   515,   515,   515,
     515,   516,   517,   517,   517,   518,   518,   519,   519,   520,
     520,   521,   521,   521,   522,   522,   523,   523
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     1,     3,     0,     1,     1,     1,     6,
       5,     3,     1,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     0,     1,     3,
       3,     6,     5,     7,     6,     6,     8,     7,     2,     5,
       6,     8,     1,     4,     1,     3,     3,     1,     5,     6,
       1,     4,     4,     4,     1,     4,     1,     3,     3,     1,
       1,     1,     1,     8,     0,     2,     2,     0,     1,     0,
       4,     1,     3,     2,     1,     5,     1,     3,     4,     1,
       1,     1,     2,     2,     1,     1,     5,     0,     2,     0,
       2,     2,     2,     2,     2,     0,     2,     0,     2,     4,
       0,     2,     4,     8,     0,     1,     1,     0,     2,     2,
       3,     1,     3,     4,     0,     1,     3,     1,     3,     3,
       1,     3,     0,     1,     1,     1,     3,     2,     1,     1,
       3,     1,     1,     1,     1,     3,     4,     3,     2,     2,
       2,     2,     4,     4,     6,     3,     1,     3,     5,     1,
       0,     4,     4,     5,     5,     5,     5,     0,     1,     0,
       1,     0,     1,     1,     1,     1,     5,     3,     1,     4,
       1,     7,     5,     4,     2,     1,     3,     3,     5,     5,
       7,     7,     6,     6,     9,     9,     8,     8,     8,     8,
       7,     7,     1,     3,     5,     4,     7,     6,     6,     5,
       2,     1,     3,     5,     7,     1,     1,     6,     5,     3,
       5,     1,     2,     4,     6,     3,     0,     2,     4,     3,
       3,     3,     2,     0,     1,     0,     3,     3,     4,     3,
       4,     3,     4,     4,     5,     6,     1,     3,     2,     1,
       3,     1,     3,     1,     2,     1,     3,     4,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     5,
       3,     5,     4,     8,     6,     1,     2,     1,     1,     4,
       1,     4,     1,     4,     1,     3,     4,     4,     4,     4,
       1,     3,     3,     3,     3,     5,     6,     3,     5,     4,
       6,     3,     4,     3,     4,     2,     1,     2,     3,     1,
       3,     1,     3,     1,     3,     1,     2,     2,     1,     1,
       1,     1,     1,     1,     3,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     2,
       2,     2,     2,     1,     2,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     2,     5,
       0,     1,     1,     2,     4,     4,     0,     2,     1,     1,
       1,     1,     1,     0,     2,     2,     5,     5,     4,     6,
       4,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     5,     5,     6,     6,     6,     6,
       5,     1,     6,     6,     5,     7,     6,     4,     1,     1,
       1,     5,     0,     1,     3,     1,     3,     3,     4,     5,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     1,     2,     2,     3,     1,     4,     0,     1,
       3,     0,     3,     0,     3,     1,     1,     1,     1,     2,
       2,     1,     2,     4,     1,     2,     1,     2,     0,     3,
       2,     2,     3,     3,     4,     1,     3,     1,     1,     3,
       3,     4,     3,     4,     6,     6,     6,     4,     1,     4,
       1,     6,     1,     1,     4,     4,     0,     2,     0,     3,
       1,     3,     3,     1,     1,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     4,     1,     4,     1,     4,     1,     4,     1,     4,
       1,     4,     6,     1,     4,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       1,     2,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     2,     2,
       0,     1,     3,     5,     1,     1,     1,     0,     2,     0,
       2,     0,     1,     2,     0,     4,     4,     0,     2,     0,
       2,     1,     1,     1,     3,     0,     6,     6,     8,     0,
       5,     3,     5,     6,     3,     5,     7,     3,     1,     1,
       1,     5,     6,     6,     5,     4,     1,     3,     5,     5,
       4,     0,     1,     0,     1,     8,     5,     4,     4,     4,
       4,     8,     0,     1,     1,     0,     2,     0,     2,     0,
       2,     0,     2,     2,     0,     2,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (&yylloc, result, scanner, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if HANA_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined HANA_LTYPE_IS_TRIVIAL && HANA_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location, result, scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, result, scanner);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule, ParseResult* result, yyscan_t scanner)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       , result, scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, result, scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !HANA_DEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !HANA_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yytype)
    {
          case 3: /* NAME  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5740 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 4: /* STRING  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5746 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 5: /* INTNUM  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5752 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 6: /* BOOL  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5758 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 7: /* APPROXNUM  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5764 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 8: /* NULLX  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5770 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 9: /* UNKNOWN  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5776 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 10: /* QUESTIONMARK  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5782 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 296: /* sql_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5788 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 297: /* stmt_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5794 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 298: /* stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5800 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 299: /* call_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5806 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 300: /* sql_argument_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5812 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 301: /* sql_argument  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5818 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 302: /* value_expression  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5824 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 303: /* sp_name  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5830 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 304: /* dql_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5836 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 305: /* dml_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5842 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 306: /* opt_partition_rest  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5848 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 307: /* opt_column_ref_list_with_parens  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5854 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 308: /* overriding_value  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5860 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 309: /* insert_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5866 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 310: /* from_constructor  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5872 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 311: /* delete_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5878 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 312: /* update_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5884 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 313: /* delete_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5890 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 314: /* update_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5896 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 315: /* update_elem_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5902 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 316: /* update_elem  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5908 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 317: /* select_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5914 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 318: /* query_expression  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5920 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 319: /* query_expression_body  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5926 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 320: /* query_term  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5932 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 321: /* query_primary  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5938 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 322: /* select_with_parens  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5944 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 323: /* subquery  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5950 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 324: /* sap_table_subquery  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5956 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 325: /* table_subquery  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5962 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 326: /* row_subquery  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5968 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 327: /* simple_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5974 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 328: /* opt_where  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5980 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 329: /* from_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5986 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 330: /* opt_from_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5992 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 331: /* opt_groupby  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 5998 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 332: /* grouping_element_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6004 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 333: /* grouping_element  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6010 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 334: /* row_order_by_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6016 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 335: /* row_order_by  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6022 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 336: /* column_ref_perens  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6028 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 337: /* group_set_name  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6034 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 338: /* grouping_option  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6040 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 339: /* grouping_best  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6046 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 340: /* grouping_subtotal  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6052 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 341: /* grouping_prefix_tb  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6058 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 342: /* grouping_prefix  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6064 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 343: /* grouping_structured_res  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6070 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 344: /* grouping_text_filter  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6076 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 345: /* opt_asc_desc  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6082 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 346: /* opt_having  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6088 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 347: /* with_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6094 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 348: /* with_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6100 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 349: /* common_table_expr  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6106 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 350: /* opt_derived_column_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6112 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 351: /* simple_ident_list_with_parens  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6118 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 352: /* simple_ident_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6124 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 353: /* column_ref_list_with_parens  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6130 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 354: /* column_ref_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6136 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 355: /* opt_distinct  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6142 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 356: /* select_expr_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6148 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 357: /* projection  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6154 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 358: /* from_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6160 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 359: /* table_reference  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6166 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 360: /* table_primary  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6172 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 361: /* table_primary_non_join  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6178 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 362: /* lateral_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6184 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 363: /* associated_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6190 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 364: /* associated_ref_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6196 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 365: /* associated_ref  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6202 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 366: /* opt_many2one_part  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6208 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 367: /* opt_search_condition  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6214 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 368: /* opt_tablesample  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6220 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 369: /* opt_table_qualify  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6226 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 370: /* collection_derived_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6232 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 371: /* collection_value_expr_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6238 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 372: /* collection_value_expr  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6244 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 373: /* as_derived_part  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6250 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 374: /* column_ref  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6256 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 375: /* relation_factor  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6262 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 376: /* func_relation_factor  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6268 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 377: /* joined_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6274 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 378: /* qualified_join  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6280 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 379: /* case_join  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6286 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 380: /* case_join_when_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6292 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 381: /* case_join_when  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6298 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 382: /* ret_join_on  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6304 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 383: /* select_expr_list_parens  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6310 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 384: /* opt_case_join_else  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6316 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 385: /* cross_join  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6322 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 386: /* join_type  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6328 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 388: /* join_cardinality  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6334 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 389: /* hana_construct_table  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6340 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 390: /* construct_column_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6346 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 391: /* construct_column  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6352 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 392: /* search_condition  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6358 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 393: /* boolean_term  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6364 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 394: /* boolean_factor  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6370 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 395: /* boolean_test  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6376 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 396: /* boolean_primary  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6382 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 397: /* predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6388 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 398: /* like_regexpr_redicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6394 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 399: /* member_of_predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6400 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 400: /* bool_function  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6406 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 401: /* contains_param3  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6412 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 402: /* search_type  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6418 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 403: /* search_param_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6424 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 404: /* search_param  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6430 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 405: /* expr_const_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6436 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 406: /* comparison_predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6442 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 407: /* between_predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6448 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 408: /* like_predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6454 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 409: /* in_predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6460 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 410: /* null_predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6466 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 411: /* exists_predicate  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6472 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 412: /* row_expr  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6478 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 413: /* factor0  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6484 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 414: /* factor1  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6490 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 415: /* factor2  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6496 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 416: /* factor3  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6502 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 417: /* factor4  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6508 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 418: /* row_expr_list_parens  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6514 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 419: /* row_expr_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6520 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 420: /* row_expr_star  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6526 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 421: /* in_expr  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6532 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 422: /* truth_value  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6538 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 428: /* expr_const  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6544 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 429: /* case_expr  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6550 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 430: /* case_arg  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6556 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 431: /* when_clause_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6562 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 432: /* when_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6568 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 433: /* case_default  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6574 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 434: /* func_expr  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6580 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 435: /* opt_nulls  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6586 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 436: /* aggregate_expression  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6592 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 437: /* aggregate_name  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6598 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 438: /* sap_specific_function  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6604 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 439: /* trim_char  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6610 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 440: /* within_group  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6616 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 441: /* opt_order_by_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6622 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 442: /* order_by_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6628 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 443: /* order_by_expression_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6634 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 444: /* order_by_expression  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6640 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 445: /* ranking_windowed_function  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6646 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 446: /* ranking_function_name  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6652 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 447: /* opt_over_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6658 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 448: /* over_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6664 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 449: /* window_specification  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6670 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 450: /* window_name  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6676 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 451: /* window_specification_details  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6682 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 452: /* opt_existing_window_name  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6688 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 453: /* opt_window_partition_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6694 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 454: /* opt_window_frame_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6700 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 455: /* window_frame_units  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6706 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 456: /* window_frame_extent  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6712 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 457: /* window_frame_start  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6718 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 458: /* window_frame_preceding  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6724 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 459: /* window_frame_between  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6730 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 460: /* window_frame_bound  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6736 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 461: /* window_frame_following  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6742 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 462: /* opt_window_frame_exclusion  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6748 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 463: /* scalar_function  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6754 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 464: /* table_function_param_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6760 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 465: /* table_function_param  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6766 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 466: /* expr_point_val  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6772 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 467: /* function_call_keyword  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6778 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 468: /* for_xml  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6784 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 469: /* for_json  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6790 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 470: /* opt_returns_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6796 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 471: /* opt_option_string_list_p  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6802 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 472: /* option_string_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6808 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 473: /* option_string  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6814 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 474: /* data_type  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6820 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 475: /* array_type  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6826 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 476: /* predefined_type  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6832 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 477: /* lob_data_type  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6838 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 478: /* primary_datetime_field  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6844 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 480: /* opt_as_label  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6850 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 481: /* as_label  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6856 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 482: /* label  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6862 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 483: /* collate_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6868 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 484: /* name_r  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6874 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 485: /* name_f  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6880 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 486: /* reserved  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6886 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 487: /* top_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6892 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 488: /* opt_for_update  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6898 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 489: /* for_share_lock  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6904 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 490: /* for_update  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6910 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 491: /* opt_of_ident_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6916 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 492: /* opt_ignore_lock  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6922 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 493: /* wait_nowait  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6928 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 494: /* limit_num  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6934 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 495: /* offset_num  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6940 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 496: /* limit_total  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6946 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 497: /* with_hint_param  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6952 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 498: /* with_hint_param_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6958 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 499: /* hint_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6964 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 500: /* opt_with_range_restrict  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6970 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 501: /* with_primary_key  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6976 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 502: /* upsert_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6982 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 503: /* replace_stmt  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6988 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 504: /* for_system_time  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 6994 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 505: /* sys_as_of_spec  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7000 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 506: /* sys_from_to_spec  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7006 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 507: /* sys_between_spec  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7012 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 508: /* for_application_time  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7018 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 509: /* partition_restriction  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7024 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 510: /* intnum_list  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7030 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 511: /* tablesample_num  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7036 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 512: /* opt_partition_restriction  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7042 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 513: /* opt_for_application_time_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7048 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 514: /* for_application_time_clause  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7054 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 515: /* regexpr_str_function  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7060 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 516: /* regex_parameter  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7066 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 517: /* opt_st_or_af  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7072 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 518: /* opt_flag  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7078 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 519: /* opt_with_string  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7084 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 520: /* opt_from_position  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7090 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 521: /* opt_occurrence_num  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7096 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 522: /* opt_group_num  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7102 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;

    case 523: /* param_num  */
#line 98 "sqlparser_hana.yacc" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 7108 "sqlparser_hana_bison.cpp" /* yacc.c:1257  */
        break;


      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (ParseResult* result, yyscan_t scanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined HANA_LTYPE_IS_TRIVIAL && HANA_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

/* User initialization code.  */
#line 67 "sqlparser_hana.yacc" /* yacc.c:1429  */
{
	// Initialize
	yylloc.first_column = 0;
	yylloc.last_column = 0;
	yylloc.first_line = 0;
	yylloc.last_line = 0;
}

#line 7225 "sqlparser_hana_bison.cpp" /* yacc.c:1429  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, &yylloc, scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 274 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    g_QuestMarkId = 0;
    (yyval.node) = (yyvsp[-1].node);
    result->result_tree_ = (yyvsp[-1].node);
    result->accept = true;
    YYACCEPT;
}
#line 7420 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 4:
#line 286 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SEMICOLON_LIST_SERIALIZE_FORMAT;
}
#line 7429 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 5:
#line 293 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 7435 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 9:
#line 301 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, 3, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &CALL_HANA_SERIALIZE_FORMAT;
}
#line 7444 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 10:
#line 306 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, 3, (yyvsp[-3].node), nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &CALL_HANA_SERIALIZE_FORMAT;
}
#line 7453 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 11:
#line 311 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, 3, (yyvsp[-1].node), nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &CALL_HANA_SERIALIZE_FORMAT2;
}
#line 7462 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 13:
#line 320 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7471 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 14:
#line 328 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 7480 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 15:
#line 333 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 7489 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 25:
#line 361 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 7495 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 27:
#line 366 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) =nullptr;}
#line 7501 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 29:
#line 371 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node)= Node::makeTerminalNode(E_STRING,"OVERRIDING SYSTEM VALUE");}
#line 7507 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 30:
#line 372 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node)= Node::makeTerminalNode(E_STRING,"OVERRIDING USER VALUE");}
#line 7513 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 31:
#line 377 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, (yyvsp[-2].node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, (yyvsp[0].node));
    (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7524 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 32:
#line 384 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
     Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-2].node), nullptr, nullptr, nullptr, (yyvsp[-1].node));
     as->serialize_format = &AS_SERIALIZE_FORMAT;
     (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, (yyvsp[0].node));
     (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7535 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 33:
#line 391 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (yyvsp[-1].node), (yyvsp[0].node), nullptr);
    source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node), nullptr, nullptr, (yyvsp[-3].node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
    (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7548 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 34:
#line 400 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (yyvsp[-1].node), (yyvsp[0].node), nullptr);
    source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-3].node), nullptr, nullptr, nullptr, (yyvsp[-2].node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
    (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7561 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 35:
#line 409 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
     Node * source = Node::makeNonTerminalNode(E_INSERT_SELECT, 2, (yyvsp[-1].node), (yyvsp[0].node));
     source->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
     Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-3].node), nullptr, nullptr, nullptr, (yyvsp[-2].node));
     as->serialize_format = &AS_SERIALIZE_FORMAT;
     (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
     (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7574 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 36:
#line 418 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
    Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-3].node), nullptr, nullptr, (yyvsp[-4].node));
    as->serialize_format = &AS_SERIALIZE_FORMAT;
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
    (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7587 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 37:
#line 427 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
     Node * source = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
     source->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
     Node * as = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-4].node), nullptr, nullptr, nullptr, (yyvsp[-3].node));
     as->serialize_format = &AS_SERIALIZE_FORMAT;
     (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, as, source);
     (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 7600 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 38:
#line 439 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_VALUES_CTOR, E_VALUES_CTOR_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &TABLE_VALUE_CTOR_SERIALIZE_FORMAT;
}
#line 7609 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 39:
#line 448 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DELETE, E_DELETE_PROPERTY_CNT,
    		nullptr,	/* E_DELETE_OPT_WITH 0 */
    		nullptr,	/* E_DELETE_OPT_TOP 1 */
    		(yyvsp[-2].node),		/* E_DELETE_DELETE_RELATION 2 */
    		nullptr,	/* E_DELETE_DELETE_RELATION_OPT_TABLE_HINT 3 */
    		nullptr,        /* E_DELETE_OPT_OUTPUT 4 */
    		nullptr,	/* E_DELETE_FROM_LIST 5 */
    		(yyvsp[-1].node),		/* E_DELETE_OPT_WHERE 6 */
    		(yyvsp[0].node) 	/* E_DELETE_OPT_QUERY_HINT 7 */);
    (yyval.node)->serialize_format = &DELETE_SERIALIZE_FORMAT;
}
#line 7626 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 40:
#line 461 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DELETE, E_DELETE_PROPERTY_CNT,
        nullptr,	/* E_DELETE_OPT_WITH 0 */
        nullptr,	/* E_DELETE_OPT_TOP 1 */
        (yyvsp[-2].node),		/* E_DELETE_DELETE_RELATION 2 */
        nullptr,	/* E_DELETE_DELETE_RELATION_OPT_TABLE_HINT 3 */
        nullptr,        /* E_DELETE_OPT_OUTPUT 4 */
        nullptr,	/* E_DELETE_FROM_LIST 5 */
        (yyvsp[-1].node),		/* E_DELETE_OPT_WHERE 6 */
        (yyvsp[0].node) 	/* E_DELETE_OPT_QUERY_HINT 7 */);
    (yyval.node)->serialize_format = &DELETE_S_FORMAT;
}
#line 7643 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 41:
#line 478 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPDATE, E_UPDATE_PROPERTY_CNT,
    		nullptr,	/* E_UPDATE_OPT_WITH 0 */
    		(yyvsp[-6].node),	/* E_UPDATE_OPT_TOP 1 */
    		(yyvsp[-5].node),		    /* E_UPDATE_UPDATE_RELATION 2 */
    		nullptr,	/* E_UPDATE_UPDATE_RELATION_OPT_TABLE_HINT 3 */
    		(yyvsp[-3].node),		    /* E_UPDATE_UPDATE_ELEM_LIST 4 */
    		nullptr,	/* E_UPDATE_OPT_OUTPUT 5 */
    		(yyvsp[-2].node),	        /* E_UPDATE_FROM_LIST 6 */
    		(yyvsp[-1].node),		    /* E_UPDATE_OPT_WHERE 7 */
    		(yyvsp[0].node)		    /* E_UPDATE_OPT_QUERY_HINT 8 */);
    (yyval.node)->serialize_format = &UPDATE_SERIALIZE_FORMAT;
}
#line 7661 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 43:
#line 498 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    if((yyvsp[-2].node) == nullptr && (yyvsp[-1].node) == nullptr && (yyvsp[0].node) == nullptr) {
        (yyval.node) = (yyvsp[-3].node);
    } else {
        (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node), (yyvsp[0].node), nullptr, (yyvsp[-2].node));
        (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
    }

}
#line 7675 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 45:
#line 530 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPDATE_ELEM_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7684 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 46:
#line 538 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_EQ, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &OP_EQ_SERIALIZE_FORMAT;
}
#line 7693 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 47:
#line 547 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = (yyvsp[0].node);
    // 0 , 1  2 already
    // $$->setChild(E_DIRECT_SELECT_FOR_UPDATE, $2);
    // $$->setChild(E_DIRECT_HINT_CLAUSE, $3);
}
#line 7704 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 48:
#line 558 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, nullptr, (yyvsp[-4].node), nullptr, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT_HANA;

    if((yyvsp[-3].node) || (yyvsp[-2].node)){
        std::string val = "";
        if((yyvsp[-3].node)){val += (yyvsp[-3].node)->text(); delete((yyvsp[-3].node));}
        if((yyvsp[-2].node)){val += (yyvsp[-2].node)->text(); delete((yyvsp[-2].node));}
        Node * str  = Node::makeTerminalNode(E_STRING, val);
        (yyval.node)->setChild(2, str);
    }

}
#line 7722 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 49:
#line 572 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-4].node), nullptr,(yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT_HANA;

    if((yyvsp[-3].node) || (yyvsp[-2].node)){
        std::string val = "";
        if((yyvsp[-3].node)){val += (yyvsp[-3].node)->text(); delete((yyvsp[-3].node)); val += " ";}
        if((yyvsp[-2].node)){val += (yyvsp[-2].node)->text(); delete((yyvsp[-2].node)); }
        Node * str  = Node::makeTerminalNode(E_STRING, val);
        (yyval.node)->setChild(2, str);
    }
}
#line 7739 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 51:
#line 588 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_UNION, "UNION");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    (yyval.node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7770 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 52:
#line 615 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_EXCEPT, "EXCEPT");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    (yyval.node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7801 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 53:
#line 642 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_EXCEPT, "MINUS");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    (yyval.node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7832 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 55:
#line 673 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_INTERSECT, "INTERSECT");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr
                        );
    (yyval.node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT_HANA;
}
#line 7863 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 57:
#line 704 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
   // if ($2->getChild(E_DIRECT_SELECT_WITH)) {
    //    yyerror(&@1, result, scanner, "error use common table expression");
   //     YYABORT;
   // }
    //$$ = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, $2->getChild(E_DIRECT_SELECT_SELECT_CLAUSE));
    //$$->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
    //$2->setChild(E_DIRECT_SELECT_SELECT_CLAUSE, nullptr);
    //delete($2);

    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 7881 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 58:
#line 722 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 7890 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 63:
#line 740 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                    (yyvsp[-5].node),             /* E_SELECT_DISTINCT 0 */
                    (yyvsp[-4].node),             /* E_SELECT_SELECT_EXPR_LIST 1 */
                    (yyvsp[-3].node),             /* E_SELECT_FROM_LIST 2 */
                    (yyvsp[-2].node),             /* E_SELECT_OPT_WHERE 3 */
                    (yyvsp[-1].node),             /* E_SELECT_GROUP_BY 4 */
                    (yyvsp[0].node),             /* E_SELECT_HAVING 5 */
                    nullptr,        /* E_SELECT_SET_OPERATION 6 */
                    nullptr,        /* E_SELECT_ALL_SPECIFIED 7 */
                    nullptr,        /* E_SELECT_FORMER_SELECT_STMT 8 */
                    nullptr,        /* E_SELECT_LATER_SELECT_STMT 9 */
                    nullptr,             /* E_SELECT_ORDER_BY 10 */
                    nullptr,            /* E_SELECT_LIMIT 11 */
                    nullptr,        /* E_SELECT_FOR_UPDATE 12 */
                    nullptr,        /* E_SELECT_HINTS 13 */
                    nullptr,        /* E_SELECT_WHEN 14 */
                    (yyvsp[-6].node),             /* E_SELECT_OPT_TOP 15 */
                    nullptr,        /* E_SELECT_OPT_WITH 16 */
                    nullptr,        /* E_SELECT_OPT_OPTION 17 */
	                nullptr,         /* E_SELECT_OPT_INTO 18 */
	                nullptr
                    );
    (yyval.node)->serialize_format = &SELECT_SERIALIZE_FORMAT;
}
#line 7920 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 64:
#line 768 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 7926 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 65:
#line 770 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHERE_CLAUSE, E_WHERE_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHERE_SERIALIZE_FORMAT;
}
#line 7935 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 66:
#line 779 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FROM_CLAUSE, E_FROM_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &FROM_SERIALIZE_FORMAT;
}
#line 7944 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 67:
#line 785 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 7950 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 69:
#line 789 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 7956 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 70:
#line 791 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_GROUP_BY, E_GROUP_BY_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &GROUP_BY_SERIALIZE_FORMAT;
}
#line 7965 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 72:
#line 800 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 7974 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 73:
#line 807 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"()"); }
#line 7980 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 75:
#line 810 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string name = (yyvsp[-4].node)->text();
    delete((yyvsp[-4].node));
    if((yyvsp[-3].node)){
        name += (yyvsp[-3].node)->text();
        delete((yyvsp[-3].node));
    }
    Node * name_n = Node::makeTerminalNode(E_IDENTIFIER, name);
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name_n, nullptr, nullptr, (yyvsp[-1].node), nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 7996 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 77:
#line 825 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8005 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 78:
#line 832 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string ss = (yyvsp[-2].node)->text();
    delete((yyvsp[-2].node));
    if((yyvsp[-1].node)){
        ss += (yyvsp[-1].node)->text();
        delete((yyvsp[-1].node));
    }
    Node * node_s  = Node::makeTerminalNode(E_STRING, ss );
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, node_s);
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8021 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 82:
#line 849 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) =  Node::makeTerminalNode(E_STRING, "()" );
}
#line 8029 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 83:
#line 855 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "GROUPING SETS ");}
#line 8035 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 84:
#line 856 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROLLUP "); }
#line 8041 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 85:
#line 857 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)  = Node::makeTerminalNode(E_IDENTIFIER, "CUBE "); }
#line 8047 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 86:
#line 861 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
      std::string val;
      if((yyvsp[-4].node)){ val += (yyvsp[-4].node)->text(); delete((yyvsp[-4].node)); }
      if((yyvsp[-3].node)){ val += (yyvsp[-3].node)->text(); delete((yyvsp[-3].node)); }
      if((yyvsp[-2].node)){ val += (yyvsp[-2].node)->text(); delete((yyvsp[-2].node)); }
      if((yyvsp[-1].node)){ val += (yyvsp[-1].node)->text(); delete((yyvsp[-1].node)); }
      if((yyvsp[0].node)){ val += (yyvsp[0].node)->text(); delete((yyvsp[0].node)); }
      if(!val.empty()){
        (yyval.node) = Node::makeTerminalNode(E_STRING, val);
      } else { (yyval.node) = nullptr; }
}
#line 8063 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 87:
#line 875 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8069 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 88:
#line 876 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {    (yyval.node) = Node::makeTerminalNode(E_STRING, "BEST "+ (yyvsp[0].node)->text()); delete((yyvsp[0].node)); }
#line 8075 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 89:
#line 879 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8081 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 90:
#line 880 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {     (yyval.node) = Node::makeTerminalNode(E_STRING, "WITH SUBTOTAL " ); }
#line 8087 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 91:
#line 881 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {  (yyval.node) = Node::makeTerminalNode(E_STRING, "WITH BALANCE " ); }
#line 8093 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 92:
#line 882 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {     (yyval.node) = Node::makeTerminalNode(E_STRING, "WITH TOTAL " ); }
#line 8099 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 93:
#line 886 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
     std::string pref_s ;
     if((yyvsp[-1].node)){ pref_s += (yyvsp[-1].node)->text(); delete((yyvsp[-1].node)); }
     if((yyvsp[0].node)){ pref_s += (yyvsp[0].node)->text(); delete((yyvsp[0].node)); }
     if(!pref_s.empty()){
         (yyval.node) = Node::makeTerminalNode( E_STRING, pref_s );
     } else { (yyval.node) = nullptr;  }
}
#line 8112 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 94:
#line 894 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {    (yyval.node) = Node::makeTerminalNode(E_STRING, " MULTIPLE RESULTSETS " ); }
#line 8118 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 95:
#line 897 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8124 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 96:
#line 898 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {     (yyval.node) = Node::makeTerminalNode(E_STRING, "PREFIX "+ (yyvsp[0].node)->text() ); delete((yyvsp[0].node)); }
#line 8130 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 97:
#line 901 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8136 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 98:
#line 902 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {    (yyval.node) = Node::makeTerminalNode(E_STRING, "STRUCTURED RESULT " ); }
#line 8142 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 99:
#line 903 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {    (yyval.node) = Node::makeTerminalNode(E_STRING, "STRUCTURED RESULT WITH OVERVIEW " ); }
#line 8148 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 100:
#line 906 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8154 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 101:
#line 907 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {  (yyval.node) = Node::makeTerminalNode(E_STRING, "TEXT_FILTER " + (yyvsp[0].node)->text() + " " ); delete((yyvsp[0].node)); }
#line 8160 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 102:
#line 908 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {  (yyval.node) = Node::makeTerminalNode(E_STRING, "TEXT_FILTER " + (yyvsp[-2].node)->text() +  " FILL UP " );  delete((yyvsp[-2].node)); }
#line 8166 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 103:
#line 909 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {  (yyval.node) = Node::makeTerminalNode(E_STRING, "TEXT_FILTER " + (yyvsp[-6].node)->text() +  " FILL UP SORT MATCHES TO TOP " ); delete((yyvsp[-6].node)); }
#line 8172 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 104:
#line 946 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_ASC, "");
}
#line 8180 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 105:
#line 950 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_ASC, "ASC");
}
#line 8188 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 106:
#line 954 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_DESC, "DESC");
}
#line 8196 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 107:
#line 960 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8202 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 108:
#line 962 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_HAVING, E_HAVING_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &HAVING_SERIALIZE_FORMAT;
}
#line 8211 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 109:
#line 972 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WITH_CLAUSE_SERIALIZE_FORMAT;
}
#line 8220 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 110:
#line 977 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WITH_RECURSIVE_CLAUSE_SERIALIZE_FORMAT;
}
#line 8229 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 112:
#line 986 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WITH_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8238 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 113:
#line 995 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_COMMON_TABLE_EXPR, E_COMMON_TABLE_EXPR_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMON_TABLE_EXPR_SERIALIZE_FORMAT;
}
#line 8247 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 114:
#line 1002 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8253 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 116:
#line 1008 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8262 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 118:
#line 1017 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8271 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 119:
#line 1024 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8280 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 121:
#line 1032 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8289 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 122:
#line 1038 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8295 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 123:
#line 1040 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_ALL, "ALL");
}
#line 8303 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 124:
#line 1044 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_DISTINCT, "DISTINCT");
}
#line 8311 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 126:
#line 1052 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8320 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 127:
#line 1060 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    if (!(yyvsp[0].node)) {
    	(yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, (yyvsp[-1].node));
    	(yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
    else {
	Node* alias_node = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
        alias_node->serialize_format = &AS_SERIALIZE_FORMAT;

        (yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, alias_node);
        (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
}
#line 8338 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 128:
#line 1074 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* star_node = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, star_node);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 8348 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 130:
#line 1084 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FROM_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8357 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 135:
#line 1100 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8366 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 136:
#line 1111 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node), (yyvsp[0].node), nullptr, (yyvsp[-2].node));
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8375 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 137:
#line 1116 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8384 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 138:
#line 1121 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8393 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 139:
#line 1126 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8402 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 140:
#line 1131 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8411 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 141:
#line 1136 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 8420 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 142:
#line 1144 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_LATERAL_QUERY, 1, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &LATERAL_QUERY_SERIALIZE_FORMAT;
}
#line 8429 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 143:
#line 1149 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_LATERAL_QUERY, 1, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &LATERAL_QUERY_SERIALIZE_FORMAT;
}
#line 8438 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 144:
#line 1157 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ASSOCIATED_TABLE, 3, (yyvsp[-5].node), (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &ASSOCIATED_TAB_SERIALIZE_FORMAT;
}
#line 8447 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 145:
#line 1162 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ASSOCIATED_TABLE,3, (yyvsp[-2].node), nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &ASSOCIATED_TAB_SERIALIZE_FORMAT;
}
#line 8456 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 147:
#line 1170 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ASSOCIATED_REF_LIST, 2, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &ASSOCIATED_LIST_SERIALIZE_FORMAT;
}
#line 8465 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 148:
#line 1177 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ASSOCIATED_REF, 3, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &ASSOCIATED_REF_SERIALIZE_FORMAT;
}
#line 8474 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 149:
#line 1182 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ASSOCIATED_REF, 3, (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 8483 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 150:
#line 1188 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)=nullptr; }
#line 8489 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 151:
#line 1189 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "USING TO ONE JOIN"); }
#line 8495 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 152:
#line 1190 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "USING TO MANY JOIN"); }
#line 8501 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 153:
#line 1191 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "USING MANY TO ONE JOIN"); }
#line 8507 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 154:
#line 1192 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "USING MANY TO MANY JOIN"); }
#line 8513 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 155:
#line 1193 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "USING ONE TO ONE JOIN"); }
#line 8519 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 156:
#line 1194 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "USING ONE TO MANY JOIN"); }
#line 8525 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 157:
#line 1197 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)=nullptr; }
#line 8531 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 159:
#line 1202 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 8537 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 161:
#line 1207 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = NULL; }
#line 8543 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 166:
#line 1217 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UNNEST_TABLE, 2, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &UNNEST_TABLE_FORMAT;
}
#line 8552 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 167:
#line 1225 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_COLLECT_VAL_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 8561 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 169:
#line 1234 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_COLLECT_VAL, 1, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &COLLECT_VAL_FORMAT;
}
#line 8570 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 171:
#line 1243 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "WITH ORDINALITY AS");
    (yyval.node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (yyvsp[-3].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8580 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 172:
#line 1249 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "AS");
    (yyval.node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (yyvsp[-3].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8590 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 173:
#line 1255 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "WITH ORDINALITY AS");
    (yyval.node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (yyvsp[0].node),nullptr);
    (yyval.node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8600 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 174:
#line 1261 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * p1 = Node::makeTerminalNode(E_STRING, "AS");
    (yyval.node) = Node::makeNonTerminalNode(E_AS_DERIVED_PART, 3, p1, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &AS_DERIVED_PART_FORMAT;
}
#line 8610 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 175:
#line 1272 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8620 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 176:
#line 1278 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8630 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 177:
#line 1284 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8641 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 178:
#line 1291 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8651 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 179:
#line 1297 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), (yyvsp[-4].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8662 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 180:
#line 1304 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8672 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 181:
#line 1310 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8683 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 182:
#line 1317 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), nullptr, (yyvsp[-5].node), nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8693 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 183:
#line 1323 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), nullptr, (yyvsp[-5].node), nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8704 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 184:
#line 1330 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), (yyvsp[-8].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8714 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 185:
#line 1336 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), (yyvsp[-8].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8725 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 186:
#line 1343 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr, (yyvsp[-7].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8735 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 187:
#line 1349 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), (yyvsp[-4].node), nullptr, (yyvsp[-7].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8746 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 188:
#line 1356 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), nullptr, (yyvsp[-5].node), (yyvsp[-7].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8756 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 189:
#line 1362 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), nullptr, (yyvsp[-5].node), (yyvsp[-7].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8767 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 190:
#line 1369 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr, (yyvsp[-6].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8777 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 191:
#line 1375 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), nullptr, nullptr, (yyvsp[-6].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 8788 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 192:
#line 1386 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 8797 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 193:
#line 1391 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 8806 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 194:
#line 1396 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 8815 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 195:
#line 1401 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, (yyvsp[-3].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 8824 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 196:
#line 1406 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8833 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 197:
#line 1411 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, (yyvsp[-5].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8842 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 198:
#line 1416 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, (yyvsp[-3].node), (yyvsp[-5].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8851 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 199:
#line 1421 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, (yyvsp[-4].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8860 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 200:
#line 1426 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_VAR, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &TABLE_VAR_SERIALIZE_FORMAT;
}
#line 8869 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 201:
#line 1434 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 8878 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 202:
#line 1439 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 8887 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 203:
#line 1444 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 8896 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 204:
#line 1449 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 8905 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 207:
#line 1466 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 8914 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 208:
#line 1471 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_INNER, "");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 8924 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 209:
#line 1477 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_JOIN_TABLE, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 8933 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 210:
#line 1497 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_JOIN, 2, (yyvsp[-2].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &CASE_JOIN_SERIALIZE_FORMAT;
}
#line 8942 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 212:
#line 1505 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_JOIN_WHEN_LIST, 2, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SPACE_LIST_SERIALIZE_FORMAT;
}
#line 8951 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 213:
#line 1512 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_JOIN_WHEN, 2, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &CASE_JOIN_WHEN_SERIALIZE_FORMAT;
}
#line 8960 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 214:
#line 1519 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_JOIN_RET, 3, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &CASE_JOIN_RET_SERIALIZE_FORMAT;
}
#line 8969 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 215:
#line 1526 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 8978 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 216:
#line 1532 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 8984 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 217:
#line 1534 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_JOIN_ELSE, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &CASE_JOIN_ELSE_SERIALIZE_FORMAT;
}
#line 8993 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 218:
#line 1542 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_CROSS, "CROSS");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-3].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &JOINED_TB_2_SERIALIZE_FORMAT;
}
#line 9003 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 219:
#line 1568 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "FULL ";
    if ((yyvsp[-1].ival))
    {
        val += "OUTER ";
    }
    if((yyvsp[0].node)){
        val+=(yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node) = Node::makeTerminalNode(E_JOIN_FULL, val);
}
#line 9020 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 220:
#line 1581 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "LEFT ";
    if ((yyvsp[-1].ival))
    {
        val += "OUTER ";
    }
    if((yyvsp[0].node)){
        val+=(yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node) = Node::makeTerminalNode(E_JOIN_LEFT, val);
}
#line 9037 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 221:
#line 1594 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "RIGHT ";
    if ((yyvsp[-1].ival))
    {
        val+="OUTER ";
    }
    if((yyvsp[0].node)){
        val += (yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node) = Node::makeTerminalNode(E_JOIN_RIGHT, val);

}
#line 9055 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 222:
#line 1608 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "INNER ";
    if((yyvsp[0].node)){
        val += (yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node) = Node::makeTerminalNode(E_JOIN_INNER, val);
}
#line 9068 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 223:
#line 1619 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.ival) = 0; }
#line 9074 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 224:
#line 1620 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.ival) = 1; /*this is a flag*/}
#line 9080 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 225:
#line 1624 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 9086 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 226:
#line 1625 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "MANY TO MANY"); }
#line 9092 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 227:
#line 1626 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "MANY TO ONE"); }
#line 9098 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 228:
#line 1627 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "MANY TO EXACT ONE"); }
#line 9104 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 229:
#line 1628 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "ONE TO MANY"); }
#line 9110 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 230:
#line 1629 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "EXACT ONE TO MANY"); }
#line 9116 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 231:
#line 1630 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "ONE TO ONE"); }
#line 9122 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 232:
#line 1631 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "EXACT ONE TO ONE"); }
#line 9128 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 233:
#line 1632 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "ONE TO EXACT ONE"); }
#line 9134 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 234:
#line 1633 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "EXACT ONE TO EXACT ONE"); }
#line 9140 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 235:
#line 1638 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CONSTRUCT_FROM_TABLE, E_CONSTRUCT_FROM_TABLE_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-3].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &CONSTRUCT_FROM_TABLE_SERIALIZE_FORMAT;
}
#line 9149 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 237:
#line 1647 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CONSTRUCT_COL_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9158 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 238:
#line 1655 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CONSTRUCT_COL, E_CONSTRUCT_COL_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &CONSTRUCT_COL_SERIALIZE_FORMAT;
}
#line 9167 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 240:
#line 1665 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_OR, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_OR);
}
#line 9176 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 242:
#line 1674 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_AND, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_AND);
}
#line 9185 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 244:
#line 1683 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT);
}
#line 9194 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 246:
#line 1692 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 9203 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 247:
#line 1697 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 9212 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 249:
#line 1706 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 9221 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 259:
#line 1735 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE_REGEXPR, 3, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE_REGEXPR);
}
#line 9230 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 260:
#line 1740 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE_REGEXPR, 3, (yyvsp[-2].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE_REGEXPR);
}
#line 9239 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 261:
#line 1747 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_MEMBER, 2, (yyvsp[-4].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_MEMBER);
}
#line 9248 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 262:
#line 1752 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_MEMBER, 2, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_MEMBER);
}
#line 9257 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 263:
#line 1759 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * node_str = Node::makeTerminalNode(E_STRING, "CONTAINS");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		node_str, (yyvsp[-5].node), (yyvsp[-3].node), (yyvsp[-1].node), nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9268 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 264:
#line 1766 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * node_str = Node::makeTerminalNode(E_STRING, "CONTAINS");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        node_str, (yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9279 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 266:
#line 1776 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = (yyvsp[-1].node)->text() + " " + (yyvsp[0].node)->text();
    (yyval.node) = Node::makeTerminalNode(E_STRING, val);
    delete((yyvsp[-1].node));
    delete((yyvsp[0].node));
}
#line 9290 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 268:
#line 1785 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "EXACT"); }
#line 9296 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 269:
#line 1787 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "EXACT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9306 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 270:
#line 1792 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "FUZZY"); }
#line 9312 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 271:
#line 1794 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FUZZY");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9322 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 272:
#line 1799 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "LINGUISTIC"); }
#line 9328 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 273:
#line 1801 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LINGUISTIC");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9338 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 275:
#line 1810 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9347 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 276:
#line 1816 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "WEIGHT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9357 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 277:
#line 1822 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FULLTEXT");
    Node * on_ = Node::makeTerminalNode(E_IDENTIFIER, "ON");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, on_, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9368 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 278:
#line 1829 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FULLTEXT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9378 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 279:
#line 1835 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LANGUAGE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_BIGQUERY_S_FORMAT;
}
#line 9388 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 281:
#line 1844 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9397 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 282:
#line 1852 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode((yyvsp[-1].nodetype), E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 9406 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 283:
#line 1857 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode((yyvsp[-1].nodetype), E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 9415 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 284:
#line 1862 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode((yyvsp[-1].nodetype), E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 9424 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 285:
#line 1872 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_BTW, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_BTW);
}
#line 9433 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 286:
#line 1877 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_BTW, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_BTW);
}
#line 9442 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 287:
#line 1885 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 9451 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 288:
#line 1890 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 9460 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 289:
#line 1895 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 9469 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 290:
#line 1900 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 9478 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 291:
#line 1908 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IN);
}
#line 9487 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 292:
#line 1913 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_IN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_IN);
}
#line 9496 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 293:
#line 1921 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 9505 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 294:
#line 1926 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 9514 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 295:
#line 1934 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_EXISTS, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_EXISTS);
}
#line 9523 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 297:
#line 1944 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_COLLATE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 9532 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 298:
#line 1949 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_CNN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 9541 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 300:
#line 1958 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_ADD, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 9550 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 302:
#line 1967 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_MUL, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 9559 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 304:
#line 1976 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_POW, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_POW);
}
#line 9568 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 306:
#line 1985 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_POS, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_POS);
}
#line 9577 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 307:
#line 1990 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NEG, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NEG);
}
#line 9586 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 314:
#line 2006 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 9595 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 316:
#line 2014 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 9604 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 318:
#line 2022 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, " * "); }
#line 9610 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 323:
#line 2035 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LE; }
#line 9616 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 324:
#line 2036 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LT; }
#line 9622 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 325:
#line 2037 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GE; }
#line 9628 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 326:
#line 2038 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GT; }
#line 9634 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 327:
#line 2039 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_EQ; }
#line 9640 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 328:
#line 2040 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 9646 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 329:
#line 2041 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GE; }
#line 9652 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 330:
#line 2042 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LE; }
#line 9658 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 331:
#line 2043 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 9664 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 332:
#line 2044 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 9670 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 333:
#line 2048 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_CNN; }
#line 9676 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 334:
#line 2049 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_CNN; }
#line 9682 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 335:
#line 2055 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.nodetype) = Node::comp_all_some_any_op_form((yyvsp[-1].nodetype), (yyvsp[0].ival));
}
#line 9690 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 336:
#line 2061 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_ADD; }
#line 9696 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 337:
#line 2062 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MINUS; }
#line 9702 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 338:
#line 2066 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MUL; }
#line 9708 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 339:
#line 2067 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_DIV; }
#line 9714 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 340:
#line 2068 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_REM; }
#line 9720 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 347:
#line 2078 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "DATE" + (yyvsp[0].node)->text()); delete((yyvsp[0].node)); }
#line 9726 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 348:
#line 2079 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING,  "TIMESTAMP" + (yyvsp[0].node)->text()); delete((yyvsp[0].node));  }
#line 9732 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 349:
#line 2085 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE, E_CASE_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_CASE);
}
#line 9741 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 350:
#line 2092 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 9747 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 353:
#line 2099 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SPACE_LIST_SERIALIZE_FORMAT;
}
#line 9756 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 354:
#line 2107 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 9765 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 355:
#line 2112 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 9774 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 356:
#line 2119 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 9780 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 357:
#line 2121 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_DEFAULT, E_CASE_DEFAULT_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &ELSE_SERIALIZE_FORMAT;
}
#line 9789 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 363:
#line 2137 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 9795 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 364:
#line 2138 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "NULLS FIRST"); }
#line 9801 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 365:
#line 2139 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "NULLS LAST"); }
#line 9807 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 366:
#line 2145 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "COUNT");
    Node* star = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		name, star, nullptr, nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9819 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 367:
#line 2153 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "COUNT");
    Node * dist = Node::makeTerminalNode(E_IDENTIFIER, "DISTINCT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		name, dist, (yyvsp[-1].node), nullptr,  nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9831 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 368:
#line 2161 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "COUNT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9842 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 369:
#line 2168 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-5].node), (yyvsp[-3].node), (yyvsp[-2].node), nullptr,  (yyvsp[0].node));
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9852 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 370:
#line 2174 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* name = Node::makeTerminalNode(E_STRING, "CHAR");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 9863 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 371:
#line 2183 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CORR"); }
#line 9869 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 372:
#line 2184 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CORR_SPEARMAN"); }
#line 9875 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 373:
#line 2185 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MIN"); }
#line 9881 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 374:
#line 2186 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MEDIAN"); }
#line 9887 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 375:
#line 2187 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MAX"); }
#line 9893 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 376:
#line 2188 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SUM"); }
#line 9899 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 377:
#line 2189 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "AVG"); }
#line 9905 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 378:
#line 2190 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV"); }
#line 9911 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 379:
#line 2191 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR"); }
#line 9917 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 380:
#line 2192 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_POP"); }
#line 9923 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 381:
#line 2193 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_POP"); }
#line 9929 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 382:
#line 2194 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_SAMP"); }
#line 9935 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 383:
#line 2195 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_SAMP"); }
#line 9941 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 384:
#line 2200 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "BINNING");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (yyvsp[-2].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 9952 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 385:
#line 2207 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "SERIES_FILTER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (yyvsp[-2].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 9963 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 386:
#line 2214 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "EXTRACT");
    Node * from = Node::makeTerminalNode(E_STRING, "FROM");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, from, nullptr, (yyvsp[-3].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9975 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 387:
#line 2222 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "FIRST_VALUE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, (yyvsp[0].node), (yyvsp[-3].node), (yyvsp[-2].node));
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9986 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 388:
#line 2229 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LAST_VALUE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, (yyvsp[0].node), (yyvsp[-3].node), (yyvsp[-2].node));
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 9997 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 389:
#line 2236 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "NTH_VALUE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, (yyvsp[0].node), (yyvsp[-3].node), (yyvsp[-2].node));
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 10008 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 390:
#line 2243 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "NTILE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            name, (yyvsp[-2].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10019 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 392:
#line 2251 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_CONT");
    Node * over = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text() + (yyvsp[0].node)->text());
    delete((yyvsp[-1].node));
    delete((yyvsp[0].node));
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, over, (yyvsp[-3].node) ,nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 10033 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 393:
#line 2261 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_DISC");
    Node * over = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text() + (yyvsp[0].node)->text());
    delete((yyvsp[-1].node));
    delete((yyvsp[0].node));
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, over, (yyvsp[-3].node) ,nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 10047 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 394:
#line 2271 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "STRING_AGG");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, nullptr, (yyvsp[-2].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 10058 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 395:
#line 2278 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "TRIM");
    std::string param1;
    if((yyvsp[-4].node)){ param1 += (yyvsp[-4].node)->text(); param1 += " "; delete((yyvsp[-4].node)); }
    param1 += (yyvsp[-3].node)->text();
    delete((yyvsp[-3].node));
    param1 += " FROM ";
     Node * param = Node::makeTerminalNode(E_IDENTIFIER, param1);
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, (yyvsp[-1].node), nullptr, param, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 10075 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 396:
#line 2291 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "TRIM");
    std::string param1 = (yyvsp[-3].node)->text();
    delete((yyvsp[-3].node));
    param1 += " FROM ";
    Node * param = Node::makeTerminalNode(E_IDENTIFIER, param1);
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
             name, (yyvsp[-1].node), nullptr, param, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 10090 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 397:
#line 2302 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "TRIM");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
                name, nullptr, nullptr, (yyvsp[-1].node), nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 10101 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 398:
#line 2310 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {  (yyval.node)  = Node::makeTerminalNode(E_STRING, "LEADING"); }
#line 10107 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 399:
#line 2311 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {  (yyval.node)  = Node::makeTerminalNode(E_STRING, "TRAILING"); }
#line 10113 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 400:
#line 2312 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {  (yyval.node)  = Node::makeTerminalNode(E_STRING, "BOTH"); }
#line 10119 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 401:
#line 2317 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WITHIN_GROUP, 1, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &WITHIN_GROUP_SERIALIZE_FORMAT;
}
#line 10128 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 402:
#line 2324 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node)=nullptr; }
#line 10134 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 404:
#line 2330 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ORDER_BY_CLAUSE, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &AGG_ORDER_SERIALIZE_FORMAT;
}
#line 10143 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 406:
#line 2338 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 10152 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 407:
#line 2345 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ORDER_BY_EXPR, 4,
        		(yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &TRIPLE_SERIALIZE_FORMAT;
}
#line 10162 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 408:
#line 2354 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), nullptr, (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10172 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 409:
#line 2360 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
            (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10182 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 410:
#line 2369 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RANK"); }
#line 10188 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 411:
#line 2370 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DENSE_RANK"); }
#line 10194 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 412:
#line 2371 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENT_RANK"); }
#line 10200 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 413:
#line 2372 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CUME_DIST"); }
#line 10206 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 414:
#line 2373 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW_NUMBER"); }
#line 10212 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 415:
#line 2374 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LAG"); }
#line 10218 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 416:
#line 2375 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LEAD"); }
#line 10224 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 417:
#line 2376 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RANDOM_PARTITION"); }
#line 10230 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 418:
#line 2377 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "WEIGHTED_AVG"); }
#line 10236 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 419:
#line 2378 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CUBIC_SPLINE_APPROX"); }
#line 10242 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 420:
#line 2379 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LINEAR_APPROX"); }
#line 10248 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 421:
#line 2383 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr;}
#line 10254 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 423:
#line 2388 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OVER "+ (yyvsp[0].node)->text()); delete((yyvsp[0].node));
}
#line 10262 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 424:
#line 2392 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OVER_CLAUSE, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &OVER_CLAUSE_SERIALIZE_FORMAT;
}
#line 10271 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 425:
#line 2401 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[-1].node); }
#line 10277 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 427:
#line 2410 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WINDOW_SPECIFIC, E_WINDOW_SPECIFIC_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WINDOW_SPECIFIC_CLAUSE_SERIALIZE_FORMAT;
}
#line 10286 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 428:
#line 2417 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 10292 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 430:
#line 2421 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string s3 = (yyvsp[0].node) ? (yyvsp[0].node)->text() : "";
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SERIES TABLE "+s3);
    delete((yyvsp[0].node));
}
#line 10302 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 431:
#line 2429 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 10308 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 432:
#line 2431 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 10314 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 433:
#line 2435 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 10320 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 434:
#line 2437 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string s3 = (yyvsp[0].node) ? (yyvsp[0].node)->text() : "";
    (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, (yyvsp[-2].node)->text()+" "+(yyvsp[-1].node)->text()+" "+s3);
    delete((yyvsp[-2].node)); delete((yyvsp[-1].node)); delete((yyvsp[0].node));
}
#line 10330 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 435:
#line 2445 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"ROWS"); }
#line 10336 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 436:
#line 2446 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"RANGE"); }
#line 10342 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 439:
#line 2455 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED PRECEDING"); }
#line 10348 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 440:
#line 2456 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"CURRENT ROW"); }
#line 10354 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 442:
#line 2461 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,(yyvsp[-1].node)->text()+" PRECEDING"); delete((yyvsp[-1].node)); }
#line 10360 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 443:
#line 2466 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BETWEEN "+(yyvsp[-2].node)->text()+" AND "+(yyvsp[0].node)->text()); delete((yyvsp[-2].node)); delete((yyvsp[0].node)); }
#line 10366 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 445:
#line 2471 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED FOLLOWING"); }
#line 10372 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 447:
#line 2476 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,(yyvsp[-1].node)->text()+" FOLLOWING"); delete((yyvsp[-1].node)); }
#line 10378 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 448:
#line 2480 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 10384 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 449:
#line 2481 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE CURRENT ROW"); }
#line 10390 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 450:
#line 2482 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE GROUP"); }
#line 10396 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 451:
#line 2483 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE TIES"); }
#line 10402 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 452:
#line 2484 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE NO OTHERS"); }
#line 10408 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 453:
#line 2489 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-2].node), nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10418 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 454:
#line 2495 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10428 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 456:
#line 2506 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 10437 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 459:
#line 2518 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
        (yyval.node) = Node::makeNonTerminalNode(E_POINT_VALUE, 2, (yyvsp[-2].node), (yyvsp[0].node));
        (yyval.node)->serialize_format = &POINT_VALUE_FORMAT;
}
#line 10446 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 460:
#line 2523 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
        (yyval.node) = Node::makeNonTerminalNode(E_POINT_VALUE, 2, (yyvsp[-2].node), (yyvsp[0].node));
        (yyval.node)->serialize_format = &POINT_VALUE_FORMAT;
}
#line 10455 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 461:
#line 2534 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10465 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 462:
#line 2540 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "RIGHT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10476 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 463:
#line 2547 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "RIGHT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10487 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 464:
#line 2554 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* expr_list = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node));
    expr_list->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;

    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "LEFT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, expr_list, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10501 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 465:
#line 2564 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CAST");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-3].node), nullptr, nullptr, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &FUN_CALL_AS_SERIALIZE_FORMAT;
}
#line 10512 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 466:
#line 2571 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* transcoding_name = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text());
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT");
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        		fun_name, (yyvsp[-3].node), nullptr, nullptr, transcoding_name);
    (yyval.node)->serialize_format = &FUN_CALL_USING_SERIALIZE_FORMAT;
}
#line 10525 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 467:
#line 2580 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10536 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 468:
#line 2587 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10547 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 469:
#line 2594 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10558 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 470:
#line 2601 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10569 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 471:
#line 2608 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* expr_list = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node));
    expr_list->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, expr_list, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 10582 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 472:
#line 2617 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SESSION_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10593 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 473:
#line 2624 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SYSTEM_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 10604 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 474:
#line 2633 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* xml = Node::makeTerminalNode(E_STRING, "XML");
    (yyval.node) = Node::makeNonTerminalNode(E_FOR_CLAUSE_HANA, 4, xml, (yyvsp[-1].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &FOR_CLAUSE_FORMAT;
}
#line 10614 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 475:
#line 2640 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node* json = Node::makeTerminalNode(E_STRING, "JSON");
    (yyval.node) = Node::makeNonTerminalNode(E_FOR_CLAUSE_HANA, 4, json, (yyvsp[-1].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &FOR_CLAUSE_FORMAT;
}
#line 10624 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 476:
#line 2647 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = NULL;}
#line 10630 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 477:
#line 2649 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_RETURNS_CLAUSE, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &RETURNS_CLAUSE_FORMAT;
}
#line 10639 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 478:
#line 2655 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = NULL;}
#line 10645 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 479:
#line 2657 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 10654 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 481:
#line 2665 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 10663 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 482:
#line 2672 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OPTION_STRING, 2, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &OPTION_STRING_FORMAT;
}
#line 10672 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 485:
#line 2689 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = (yyvsp[-1].node);
    (yyvsp[-1].node)->set_text( (yyvsp[-1].node)->text() + " ARRAY ");
}
#line 10681 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 486:
#line 2733 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "DATE");}
#line 10687 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 487:
#line 2734 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "TIME");}
#line 10693 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 488:
#line 2735 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "SECONDDATE");}
#line 10699 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 489:
#line 2736 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP");}
#line 10705 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 490:
#line 2737 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "TINYINT");}
#line 10711 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 491:
#line 2738 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "SMALLINT");}
#line 10717 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 492:
#line 2739 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "INTEGER");}
#line 10723 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 493:
#line 2740 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "INT");  }
#line 10729 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 494:
#line 2741 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "BIGINT");}
#line 10735 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 495:
#line 2742 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "SMALLDECIMAL");}
#line 10741 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 496:
#line 2743 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "REAL");}
#line 10747 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 497:
#line 2744 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "DOUBLE");}
#line 10753 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 498:
#line 2745 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "TEXT");}
#line 10759 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 499:
#line 2746 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "BINTEXT");}
#line 10765 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 500:
#line 2747 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "VARCHAR");}
#line 10771 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 501:
#line 2748 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "VARCHAR("+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-1].node)); }
#line 10777 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 502:
#line 2749 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "NVARCHAR");}
#line 10783 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 503:
#line 2750 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "NVARCHAR("+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-1].node)); }
#line 10789 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 504:
#line 2751 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "ALPHANUM");}
#line 10795 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 505:
#line 2752 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "ALPHANUM("+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-1].node)); }
#line 10801 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 506:
#line 2753 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "VARBINARY");}
#line 10807 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 507:
#line 2754 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "VARBINARY("+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-1].node)); }
#line 10813 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 508:
#line 2755 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "SHORTTEXT");}
#line 10819 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 509:
#line 2756 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "SHORTTEXT("+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-1].node)); }
#line 10825 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 510:
#line 2757 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL");}
#line 10831 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 511:
#line 2758 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-1].node)); }
#line 10837 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 512:
#line 2759 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-3].node));delete((yyvsp[-1].node)); }
#line 10843 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 513:
#line 2760 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "FLOAT");}
#line 10849 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 514:
#line 2761 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "FLOAT("+(yyvsp[-1].node)->text()+")"); delete((yyvsp[-1].node)); }
#line 10855 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 515:
#line 2762 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "BOOLEAN");}
#line 10861 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 517:
#line 2766 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "BLOB");}
#line 10867 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 518:
#line 2767 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "CLOB");}
#line 10873 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 519:
#line 2768 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = Node::makeTerminalNode(E_STRING, "NCLOB");}
#line 10879 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 520:
#line 2856 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "YEAR");
}
#line 10887 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 521:
#line 2860 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "MONTH");
}
#line 10895 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 522:
#line 2864 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DAY");
}
#line 10903 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 523:
#line 2868 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "HOUR");
}
#line 10911 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 524:
#line 2872 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "MINUTE");
}
#line 10919 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 525:
#line 2876 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND");
}
#line 10927 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 526:
#line 3251 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.ival) = 0; }
#line 10933 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 527:
#line 3252 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.ival) = 1; }
#line 10939 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 528:
#line 3253 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.ival) = 2; }
#line 10945 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 529:
#line 3257 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 10951 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 531:
#line 3262 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 10957 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 534:
#line 3273 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "COLLATE "+(yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 10966 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 539:
#line 3285 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LOCATE_REGEXPR"); }
#line 10972 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 540:
#line 3286 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OCCURRENCES_REGEXPR"); }
#line 10978 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 541:
#line 3287 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "REPLACE_REGEXPR"); }
#line 10984 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 542:
#line 3288 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTRING_REGEXPR"); }
#line 10990 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 543:
#line 3289 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTR_REGEXPR"); }
#line 10996 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 544:
#line 3290 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STRING_AGG"); }
#line 11002 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 545:
#line 3291 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CHAR"); }
#line 11008 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 546:
#line 3292 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CONTAINS"); }
#line 11014 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 547:
#line 3293 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BINNING"); }
#line 11020 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 548:
#line 3294 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "EXTRACT"); }
#line 11026 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 549:
#line 3295 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FIRST_VALUE"); }
#line 11032 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 550:
#line 3296 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LAST_VALUE"); }
#line 11038 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 551:
#line 3297 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NTH_VALUE"); }
#line 11044 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 552:
#line 3298 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NTILE"); }
#line 11050 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 553:
#line 3299 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_CONT"); }
#line 11056 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 554:
#line 3300 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENTILE_DISC"); }
#line 11062 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 555:
#line 3301 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TRIM"); }
#line 11068 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 556:
#line 3302 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "COUNT"); }
#line 11074 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 557:
#line 3303 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "WEIGHT");  }
#line 11080 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 558:
#line 3304 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FULLTEXT");  }
#line 11086 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 559:
#line 3305 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LANGUAGE");  }
#line 11092 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 560:
#line 3306 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LINGUISTIC");  }
#line 11098 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 561:
#line 3307 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FUZZY");  }
#line 11104 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 562:
#line 3308 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ANY");  }
#line 11110 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 566:
#line 3318 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "K"); }
#line 11116 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 567:
#line 3319 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "M"); }
#line 11122 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 568:
#line 3320 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "G"); }
#line 11128 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 569:
#line 3321 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ARRAY"); }
#line 11134 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 570:
#line 3322 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BINARY"); }
#line 11140 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 571:
#line 3323 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CAST"); }
#line 11146 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 572:
#line 3324 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CHARACTERS"); }
#line 11152 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 573:
#line 3325 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CODE_UNITS"); }
#line 11158 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 574:
#line 3326 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CORRESPONDING"); }
#line 11164 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 575:
#line 3327 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FOLLOWING"); }
#line 11170 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 576:
#line 3328 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "INTERVAL"); }
#line 11176 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 577:
#line 3329 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LARGE"); }
#line 11182 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 578:
#line 3330 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MULTISET"); }
#line 11188 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 579:
#line 3331 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OBJECT"); }
#line 11194 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 580:
#line 3332 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OCTETS"); }
#line 11200 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 581:
#line 3333 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ONLY"); }
#line 11206 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 582:
#line 3334 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECEDING"); }
#line 11212 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 583:
#line 3335 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECISION"); }
#line 11218 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 584:
#line 3336 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RECURSIVE"); }
#line 11224 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 585:
#line 3337 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "REF"); }
#line 11230 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 586:
#line 3338 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW"); }
#line 11236 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 587:
#line 3339 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SCOPE"); }
#line 11242 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 588:
#line 3340 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "UNBOUNDED"); }
#line 11248 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 589:
#line 3341 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VARCHAR"); }
#line 11254 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 590:
#line 3342 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "WITHOUT"); }
#line 11260 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 591:
#line 3343 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ZONE"); }
#line 11266 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 592:
#line 3344 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OF"); }
#line 11272 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 593:
#line 3345 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "READ"); }
#line 11278 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 594:
#line 3346 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIMESTAMP"); }
#line 11284 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 595:
#line 3347 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIME");  }
#line 11290 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 596:
#line 3348 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DESC"); }
#line 11296 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 597:
#line 3349 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIES"); }
#line 11302 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 598:
#line 3350 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SETS"); }
#line 11308 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 599:
#line 3351 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OTHERS"); }
#line 11314 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 600:
#line 3352 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "EXCLUDE"); }
#line 11320 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 601:
#line 3353 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ASC"); }
#line 11326 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 602:
#line 3354 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE"); }
#line 11332 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 603:
#line 3355 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT"); }
#line 11338 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 604:
#line 3356 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF"); }
#line 11344 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 605:
#line 3357 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DEFAULT"); }
#line 11350 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 606:
#line 3358 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TO"); }
#line 11356 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 607:
#line 3359 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DOUBLE"); }
#line 11362 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 608:
#line 3360 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MOD"); }
#line 11368 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 609:
#line 3361 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MANY"); }
#line 11374 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 610:
#line 3362 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ONE"); }
#line 11380 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 611:
#line 3363 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PRIMARY"); }
#line 11386 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 612:
#line 3364 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "KEY"); }
#line 11392 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 613:
#line 3365 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SECONDDATE"); }
#line 11398 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 614:
#line 3366 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TINYINT"); }
#line 11404 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 615:
#line 3367 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SMALLDECIMAL"); }
#line 11410 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 616:
#line 3368 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TEXT"); }
#line 11416 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 617:
#line 3369 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BINTEXT"); }
#line 11422 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 618:
#line 3370 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ALPHANUM"); }
#line 11428 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 619:
#line 3371 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VARBINARY"); }
#line 11434 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 620:
#line 3372 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SHORTTEXT"); }
#line 11440 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 621:
#line 3373 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "REPLACE"); }
#line 11446 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 622:
#line 3374 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PARAMETERS"); }
#line 11452 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 623:
#line 3375 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "HISTORY"); }
#line 11458 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 624:
#line 3376 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OVERRIDING"); }
#line 11464 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 625:
#line 3377 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SYSTEM"); }
#line 11470 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 626:
#line 3378 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "USER"); }
#line 11476 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 627:
#line 3379 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VALUE"); }
#line 11482 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 628:
#line 3380 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "EXACT"); }
#line 11488 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 629:
#line 3381 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BERNOULLI"); }
#line 11494 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 630:
#line 3382 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NCHAR"); }
#line 11500 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 631:
#line 3385 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BEST"); }
#line 11506 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 632:
#line 3386 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SUBTOTAL"); }
#line 11512 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 633:
#line 3387 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BALANCE"); }
#line 11518 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 634:
#line 3388 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TOTAL"); }
#line 11524 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 635:
#line 3389 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MULTIPLE"); }
#line 11530 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 636:
#line 3390 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RESULTSETS"); }
#line 11536 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 637:
#line 3391 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PREFIX"); }
#line 11542 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 638:
#line 3392 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STRUCTURED"); }
#line 11548 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 639:
#line 3393 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RESULT"); }
#line 11554 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 640:
#line 3394 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OVERVIEW"); }
#line 11560 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 641:
#line 3395 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TEXT_FILTER"); }
#line 11566 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 642:
#line 3396 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FILL"); }
#line 11572 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 643:
#line 3397 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "UP"); }
#line 11578 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 644:
#line 3398 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MATCHES"); }
#line 11584 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 645:
#line 3399 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SORT"); }
#line 11590 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 646:
#line 3400 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROWCOUNT"); }
#line 11596 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 647:
#line 3401 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LOCKED"); }
#line 11602 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 648:
#line 3403 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SHARE"); }
#line 11608 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 649:
#line 3404 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LOCK"); }
#line 11614 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 650:
#line 3405 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "UTCTIMESTAMP"); }
#line 11620 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 651:
#line 3406 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "COMMIT"); }
#line 11626 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 652:
#line 3407 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ID"); }
#line 11632 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 653:
#line 3408 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "JSON"); }
#line 11638 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 654:
#line 3409 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "XML"); }
#line 11644 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 655:
#line 3412 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SERIES"); }
#line 11650 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 656:
#line 3413 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TABLE");  }
#line 11656 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 657:
#line 3414 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ORDINALITY");  }
#line 11662 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 658:
#line 3415 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FLAG");  }
#line 11668 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 659:
#line 3416 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LEADING");  }
#line 11674 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 660:
#line 3417 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TRAILING");  }
#line 11680 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 661:
#line 3418 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BOTH");  }
#line 11686 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 662:
#line 3419 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FIRST");  }
#line 11692 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 663:
#line 3420 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LAST");  }
#line 11698 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 664:
#line 3421 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PORTION");  }
#line 11704 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 665:
#line 3422 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "START");  }
#line 11710 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 666:
#line 3423 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "AFTER");  }
#line 11716 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 667:
#line 3428 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 11722 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 668:
#line 3430 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TOP_CLAUSE, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &TOP_CLAUSE_FORMAT;
}
#line 11731 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 669:
#line 3435 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TOP_CLAUSE, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &TOP_CLAUSE_FORMAT;
}
#line 11740 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 670:
#line 3459 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr;}
#line 11746 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 672:
#line 3464 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, " FOR SHARE LOCK "); }
#line 11752 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 673:
#line 3468 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FOR_UPDATE, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &FOR_UPDATE_FORMAT_HANA;
}
#line 11761 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 677:
#line 3478 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 11767 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 678:
#line 3480 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * p_of = Node::makeTerminalNode(E_STRING, "OF");
    (yyval.node) = Node::makeNonTerminalNode(E_OF_COLUMS, 2, p_of, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 11777 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 679:
#line 3487 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 11783 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 680:
#line 3488 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "IGNORE LOCKED");}
#line 11789 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 681:
#line 3491 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {(yyval.node) = nullptr;}
#line 11795 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 682:
#line 3492 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_NOWAIT, "NOWAIT");}
#line 11801 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 683:
#line 3494 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WAIT_INT, 1,  (yyvsp[0].node));
    (yyval.node)->serialize_format = &WAIT_TIME_FORMAT;
}
#line 11810 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 684:
#line 3501 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 11816 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 685:
#line 3503 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_LIMIT_NUM, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &LIMIT_NUM_FORMAT_HANA;
}
#line 11825 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 686:
#line 3508 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_LIMIT_NUM, 3, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &LIMIT_NUM_FORMAT_HANA;
}
#line 11834 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 687:
#line 3515 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 11840 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 688:
#line 3517 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OFFSET_NUM, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &OFFSET_NUM_FORMAT;
}
#line 11849 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 689:
#line 3524 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 11855 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 690:
#line 3526 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TOTAL ROWCOUNT");
}
#line 11863 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 694:
#line 3538 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 11872 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 695:
#line 3545 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 11878 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 696:
#line 3547 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
     std::string ss = (yyvsp[-2].node)->text();
     delete((yyvsp[-2].node));
    std::string sval = "WITH HINT(" + ss + ")";
    if((yyvsp[0].node)){
        sval += (yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11893 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 697:
#line 3558 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string ss = (yyvsp[-2].node)->text();
    delete((yyvsp[-2].node));
    std::string sval = "WITH RANGE_RESTRICTION(" + ss + ")";
    if((yyvsp[0].node)){
        sval += (yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11908 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 698:
#line 3569 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string ss1 = (yyvsp[-4].node)->text();
    delete((yyvsp[-4].node));
    std::string ss2 = (yyvsp[-2].node)->text();
    delete((yyvsp[-2].node));
    std::string sval = "WITH PARAMETERS(" + ss1 +"=" + ss2 + ")";
    if((yyvsp[0].node)){
        sval += (yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11925 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 699:
#line 3584 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 11931 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 700:
#line 3586 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string sval = "WITH RANGE_RESTRICTION(" + (yyvsp[-1].node)->text() + ")";
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeTerminalNode(E_STRING, sval);
}
#line 11941 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 701:
#line 3595 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "WITH PRIMARY KEY");
}
#line 11949 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 702:
#line 3602 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPSERT, 6, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node), NULL,NULL);
    (yyval.node)->serialize_format = &UPSERT_SERIALIZE_FORMAT;
}
#line 11958 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 703:
#line 3607 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {

    (yyval.node) = Node::makeNonTerminalNode(E_UPSERT, 6, (yyvsp[-4].node), (yyvsp[-3].node), (yyvsp[-2].node), NULL, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &UPSERT_SERIALIZE_FORMAT;
}
#line 11968 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 704:
#line 3613 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPSERT, 6, (yyvsp[-1].node), NULL, (yyvsp[0].node), NULL, NULL,NULL);
    (yyval.node)->serialize_format = &UPSERT_SERIALIZE_FORMAT;
}
#line 11977 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 705:
#line 3620 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_REPLACE, 5, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node), NULL);
    (yyval.node)->serialize_format = &REPLACE_SERIALIZE_FORMAT;
}
#line 11986 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 706:
#line 3625 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * pwith = Node::makeTerminalNode(E_STRING, " WITH PRIMARY KEY");
    (yyval.node) = Node::makeNonTerminalNode(E_REPLACE, 5, (yyvsp[-5].node), (yyvsp[-4].node), (yyvsp[-3].node), NULL, pwith);
    (yyval.node)->serialize_format = &REPLACE_SERIALIZE_FORMAT;
}
#line 11996 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 707:
#line 3631 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_REPLACE, 5, (yyvsp[-1].node), NULL, (yyvsp[0].node), NULL, NULL);
    (yyval.node)->serialize_format = &REPLACE_SERIALIZE_FORMAT;
}
#line 12005 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 711:
#line 3643 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FOR SYSTEM_TIME AS OF "+(yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 12014 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 712:
#line 3650 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FOR SYSTEM_TIME FROM "+(yyvsp[-2].node)->text()+" TO "+(yyvsp[0].node)->text());
    delete((yyvsp[-2].node));
    delete((yyvsp[0].node));
}
#line 12024 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 713:
#line 3658 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FOR SYSTEM_TIME BETWEEN "+(yyvsp[-2].node)->text()+" AND "+(yyvsp[0].node)->text());
    delete((yyvsp[-2].node));
    delete((yyvsp[0].node));
}
#line 12034 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 714:
#line 3666 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FOR APPLICATION_TIME AS OF "+(yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 12043 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 715:
#line 3673 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "PARTITION("+(yyvsp[-1].node)->text()+")";
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeTerminalNode(E_STRING,val );
}
#line 12053 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 717:
#line 3682 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 12062 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 718:
#line 3689 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "TABLESAMPLE BERNOULLI("+(yyvsp[-1].node)->text()+")";
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeTerminalNode(E_STRING,val );
}
#line 12072 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 719:
#line 3695 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "TABLESAMPLE SYSTEM("+(yyvsp[-1].node)->text()+")";
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeTerminalNode(E_STRING,val );
}
#line 12082 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 720:
#line 3701 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = "TABLESAMPLE("+(yyvsp[-1].node)->text()+")";
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeTerminalNode(E_STRING,val );
}
#line 12092 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 721:
#line 3709 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 12098 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 723:
#line 3712 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 12104 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 725:
#line 3717 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string ss = "FOR PORTION OF APPLICATION_TIME FROM ";
    ss += (yyvsp[-2].node)->text();
    delete((yyvsp[-2].node));
    ss += " TO ";
    ss += (yyvsp[0].node)->text();
    delete((yyvsp[0].node));
    (yyval.node)= Node::makeTerminalNode(E_STRING, ss);
}
#line 12118 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 726:
#line 3730 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "LOCATE_REGEXPR");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (yyvsp[-2].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 12128 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 727:
#line 3736 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "OCCURRENCES_REGEXPR");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (yyvsp[-1].node), nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 12138 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 728:
#line 3742 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "REPLACE_REGEXPR");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (yyvsp[-1].node), nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 12148 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 729:
#line 3748 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTRING_REGEXPR");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (yyvsp[-1].node), nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 12158 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 730:
#line 3754 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    Node * name = Node::makeTerminalNode(E_IDENTIFIER, "SUBSTR_REGEXPR");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT, name, nullptr, nullptr, (yyvsp[-1].node), nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_314_SERIALIZE_FORMAT;
}
#line 12168 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 731:
#line 3762 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    std::string val = (yyvsp[-7].node)->text();
    delete((yyvsp[-7].node));
    if((yyvsp[-6].node)){
        val += " ";
        val += (yyvsp[-6].node)->text();
        delete((yyvsp[-6].node));
    }
    val +=" IN ";
    val += (yyvsp[-4].node)->text();
    delete((yyvsp[-4].node));
    if((yyvsp[-3].node)){
        val+= " ";
        val+=(yyvsp[-3].node)->text();
        delete((yyvsp[-3].node));
    }
    if((yyvsp[-2].node)){
        val+= " ";
        val+=(yyvsp[-2].node)->text();
        delete((yyvsp[-2].node));
    }
    if((yyvsp[-1].node)){
        val+= " ";
        val+=(yyvsp[-1].node)->text();
        delete((yyvsp[-1].node));
    }
    if((yyvsp[0].node)){
        val+= " ";
        val+=(yyvsp[0].node)->text();
        delete((yyvsp[0].node));
    }
    (yyval.node)= Node::makeTerminalNode(E_STRING, val );
}
#line 12206 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 732:
#line 3796 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr;}
#line 12212 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 733:
#line 3797 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)= Node::makeTerminalNode(E_STRING, "START" ); }
#line 12218 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 734:
#line 3798 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)= Node::makeTerminalNode(E_STRING, "AFTER" ); }
#line 12224 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 735:
#line 3801 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node) = nullptr;}
#line 12230 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 736:
#line 3803 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node)= Node::makeTerminalNode(E_STRING, "FLAG "+(yyvsp[0].node)->text() );
    delete((yyvsp[0].node));
}
#line 12239 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 737:
#line 3808 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)=nullptr;}
#line 12245 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 738:
#line 3810 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node)= Node::makeTerminalNode(E_STRING, "WITH "+(yyvsp[0].node)->text() );
    delete((yyvsp[0].node));
}
#line 12254 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 739:
#line 3815 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)=nullptr;}
#line 12260 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 740:
#line 3817 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node)= Node::makeTerminalNode(E_STRING, "FROM "+(yyvsp[0].node)->text() );
    delete((yyvsp[0].node));
}
#line 12269 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 741:
#line 3822 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)=nullptr;}
#line 12275 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 742:
#line 3824 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node)= Node::makeTerminalNode(E_STRING, "OCCURRENCE "+(yyvsp[0].node)->text() );
    delete((yyvsp[0].node));
}
#line 12284 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 743:
#line 3828 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)= Node::makeTerminalNode(E_STRING, "OCCURRENCE ALL"); }
#line 12290 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 744:
#line 3831 "sqlparser_hana.yacc" /* yacc.c:1646  */
    { (yyval.node)=nullptr;}
#line 12296 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;

  case 745:
#line 3833 "sqlparser_hana.yacc" /* yacc.c:1646  */
    {
    (yyval.node)= Node::makeTerminalNode(E_STRING, "GROUP "+(yyvsp[0].node)->text() );
    delete((yyvsp[0].node));
}
#line 12305 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
    break;


#line 12309 "sqlparser_hana_bison.cpp" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (&yylloc, result, scanner, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (&yylloc, result, scanner, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, result, scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp, result, scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, result, scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, result, scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp, result, scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 3842 "sqlparser_hana.yacc" /* yacc.c:1906  */

/*********************************
 ** Section 4: Additional C code
 *********************************/

/* empty */
