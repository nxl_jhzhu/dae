/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         SQL2003_STYPE
#define YYLTYPE         SQL2003_LTYPE
/* Substitute the variable and function names.  */
#define yyparse         sql2003_parse
#define yylex           sql2003_lex
#define yyerror         sql2003_error
#define yydebug         sql2003_debug
#define yynerrs         sql2003_nerrs


/* Copy the first part of user declarations.  */
#line 1 "sqlparser_sql2003.y" /* yacc.c:339  */

/**
 * This Grammar is designed for sql2003.
 * https://github.com/Raphael2017/SQL/blob/master/sql-2003-2.bnf
 * sqlparser.y
 * defines sqlparser_sql2003_bison.h
 * outputs sqlparser_sql2003_bison.cpp
 *
 * Bison Grammar File Spec: http://dinosaur.compilertools.net/bison/bison_6.html
 *
 */
/*********************************
 ** Section 1: C Declarations
 *********************************/

#include "sqlparser_sql2003_bison.h"
#include "sqlparser_sql2003_flex.h"
#include "serialize_format.h"

#include <stdio.h>
#include <string.h>
//#include <strings.h>

/*
 * We provide parse error includes error message, first line, first column of error lex for debug
 */
int yyerror(YYLTYPE* llocp, ParseResult* result, yyscan_t scanner, const char *msg) {
    result->accept = false;
    result->errFirstLine = llocp->first_line;
    result->errFirstColumn = llocp->first_column;
    result->errDetail = msg;
	return 0;
}


#line 110 "sqlparser_sql2003_bison.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sqlparser_sql2003_bison.h".  */
#ifndef YY_SQL2003_SQLPARSER_SQL2003_BISON_H_INCLUDED
# define YY_SQL2003_SQLPARSER_SQL2003_BISON_H_INCLUDED
/* Debug traces.  */
#ifndef SQL2003_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define SQL2003_DEBUG 1
#  else
#   define SQL2003_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define SQL2003_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined SQL2003_DEBUG */
#if SQL2003_DEBUG
extern int sql2003_debug;
#endif
/* "%code requires" blocks.  */
#line 42 "sqlparser_sql2003.y" /* yacc.c:355  */

// %code requires block

#include "node.h"

#line 154 "sqlparser_sql2003_bison.cpp" /* yacc.c:355  */

/* Token type.  */
#ifndef SQL2003_TOKENTYPE
# define SQL2003_TOKENTYPE
  enum sql2003_tokentype
  {
    SQL2003_NAME = 258,
    SQL2003_STRING = 259,
    SQL2003_INTNUM = 260,
    SQL2003_BOOL = 261,
    SQL2003_APPROXNUM = 262,
    SQL2003_NULLX = 263,
    SQL2003_UNKNOWN = 264,
    SQL2003_QUESTIONMARK = 265,
    SQL2003_PARAM = 266,
    SQL2003_UMINUS = 267,
    SQL2003_ALL = 268,
    SQL2003_AND = 269,
    SQL2003_ANY = 270,
    SQL2003_ARRAY = 271,
    SQL2003_AS = 272,
    SQL2003_ASC = 273,
    SQL2003_AVG = 274,
    SQL2003_BETWEEN = 275,
    SQL2003_BIGINT = 276,
    SQL2003_BINARY = 277,
    SQL2003_BLOB = 278,
    SQL2003_BOOLEAN = 279,
    SQL2003_BY = 280,
    SQL2003_CALL = 281,
    SQL2003_CASE = 282,
    SQL2003_CAST = 283,
    SQL2003_CHAR = 284,
    SQL2003_CHARACTER = 285,
    SQL2003_CHARACTERS = 286,
    SQL2003_CLOB = 287,
    SQL2003_CNNOP = 288,
    SQL2003_COALESCE = 289,
    SQL2003_CODE_UNITS = 290,
    SQL2003_COLLATE = 291,
    SQL2003_COMP_EQ = 292,
    SQL2003_COMP_GE = 293,
    SQL2003_COMP_GT = 294,
    SQL2003_COMP_LE = 295,
    SQL2003_COMP_LT = 296,
    SQL2003_COMP_NE = 297,
    SQL2003_CONVERT = 298,
    SQL2003_CORRESPONDING = 299,
    SQL2003_COUNT = 300,
    SQL2003_CROSS = 301,
    SQL2003_CUME_DIST = 302,
    SQL2003_CURRENT = 303,
    SQL2003_CURRENT_TIMESTAMP = 304,
    SQL2003_CURRENT_USER = 305,
    SQL2003_DATE = 306,
    SQL2003_DAY = 307,
    SQL2003_DEC = 308,
    SQL2003_DECIMAL = 309,
    SQL2003_DEFAULT = 310,
    SQL2003_DELETE = 311,
    SQL2003_DENSE_RANK = 312,
    SQL2003_DESC = 313,
    SQL2003_DISTINCT = 314,
    SQL2003_DOUBLE = 315,
    SQL2003_ELSE = 316,
    SQL2003_END = 317,
    SQL2003_END_P = 318,
    SQL2003_ESCAPE = 319,
    SQL2003_ERROR = 320,
    SQL2003_EXCEPT = 321,
    SQL2003_EXCLUDE = 322,
    SQL2003_EXISTS = 323,
    SQL2003_FLOAT = 324,
    SQL2003_FOLLOWING = 325,
    SQL2003_FOR = 326,
    SQL2003_FROM = 327,
    SQL2003_FULL = 328,
    SQL2003_G = 329,
    SQL2003_GROUP = 330,
    SQL2003_GROUPING = 331,
    SQL2003_HAVING = 332,
    SQL2003_HOUR = 333,
    SQL2003_IN = 334,
    SQL2003_INNER = 335,
    SQL2003_INSERT = 336,
    SQL2003_INT = 337,
    SQL2003_INTEGER = 338,
    SQL2003_INTERSECT = 339,
    SQL2003_INTERVAL = 340,
    SQL2003_INTO = 341,
    SQL2003_IS = 342,
    SQL2003_JOIN = 343,
    SQL2003_K = 344,
    SQL2003_LARGE = 345,
    SQL2003_LEFT = 346,
    SQL2003_LIKE = 347,
    SQL2003_M = 348,
    SQL2003_MAX = 349,
    SQL2003_MIN = 350,
    SQL2003_MINUTE = 351,
    SQL2003_MOD = 352,
    SQL2003_MONTH = 353,
    SQL2003_MULTISET = 354,
    SQL2003_NATIONAL = 355,
    SQL2003_NATURAL = 356,
    SQL2003_NCHAR = 357,
    SQL2003_NCLOB = 358,
    SQL2003_NO = 359,
    SQL2003_NOT = 360,
    SQL2003_NULLIF = 361,
    SQL2003_NUMERIC = 362,
    SQL2003_OBJECT = 363,
    SQL2003_OCTETS = 364,
    SQL2003_OF = 365,
    SQL2003_ON = 366,
    SQL2003_ONLY = 367,
    SQL2003_OR = 368,
    SQL2003_ORDER = 369,
    SQL2003_OTHERS = 370,
    SQL2003_OUTER = 371,
    SQL2003_OVER = 372,
    SQL2003_PARTITION = 373,
    SQL2003_PERCENT_RANK = 374,
    SQL2003_PRECEDING = 375,
    SQL2003_PRECISION = 376,
    SQL2003_RANGE = 377,
    SQL2003_RANK = 378,
    SQL2003_READ = 379,
    SQL2003_REAL = 380,
    SQL2003_RECURSIVE = 381,
    SQL2003_REF = 382,
    SQL2003_RIGHT = 383,
    SQL2003_ROW = 384,
    SQL2003_ROWS = 385,
    SQL2003_ROW_NUMBER = 386,
    SQL2003_SCOPE = 387,
    SQL2003_SECOND = 388,
    SQL2003_SELECT = 389,
    SQL2003_SESSION_USER = 390,
    SQL2003_SET = 391,
    SQL2003_SETS = 392,
    SQL2003_SMALLINT = 393,
    SQL2003_SOME = 394,
    SQL2003_STDDEV_POP = 395,
    SQL2003_STDDEV_SAMP = 396,
    SQL2003_SUM = 397,
    SQL2003_SYSTEM_USER = 398,
    SQL2003_THEN = 399,
    SQL2003_TIES = 400,
    SQL2003_TIME = 401,
    SQL2003_TIMESTAMP = 402,
    SQL2003_TO = 403,
    SQL2003_UNBOUNDED = 404,
    SQL2003_UNION = 405,
    SQL2003_UPDATE = 406,
    SQL2003_USING = 407,
    SQL2003_VALUES = 408,
    SQL2003_VARCHAR = 409,
    SQL2003_VARYING = 410,
    SQL2003_VAR_POP = 411,
    SQL2003_VAR_SAMP = 412,
    SQL2003_WHEN = 413,
    SQL2003_WHERE = 414,
    SQL2003_WITH = 415,
    SQL2003_WITHOUT = 416,
    SQL2003_YEAR = 417,
    SQL2003_ZONE = 418,
    SQL2003_TOP = 419
  };
#endif

/* Value type.  */
#if ! defined SQL2003_STYPE && ! defined SQL2003_STYPE_IS_DECLARED

union SQL2003_STYPE
{
#line 85 "sqlparser_sql2003.y" /* yacc.c:355  */

    struct Node* node;
    int    ival;
    NodeType nodetype;

#line 337 "sqlparser_sql2003_bison.cpp" /* yacc.c:355  */
};

typedef union SQL2003_STYPE SQL2003_STYPE;
# define SQL2003_STYPE_IS_TRIVIAL 1
# define SQL2003_STYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined SQL2003_LTYPE && ! defined SQL2003_LTYPE_IS_DECLARED
typedef struct SQL2003_LTYPE SQL2003_LTYPE;
struct SQL2003_LTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define SQL2003_LTYPE_IS_DECLARED 1
# define SQL2003_LTYPE_IS_TRIVIAL 1
#endif



int sql2003_parse (ParseResult* result, yyscan_t scanner);

#endif /* !YY_SQL2003_SQLPARSER_SQL2003_BISON_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 367 "sqlparser_sql2003_bison.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined SQL2003_LTYPE_IS_TRIVIAL && SQL2003_LTYPE_IS_TRIVIAL \
             && defined SQL2003_STYPE_IS_TRIVIAL && SQL2003_STYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  120
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   5778

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  181
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  152
/* YYNRULES -- Number of rules.  */
#define YYNRULES  483
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  804

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   419

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   176,     2,     2,     2,   179,   180,     2,
      13,    14,   172,   174,   171,   175,    15,   178,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,   168,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   173,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   169,   177,   170,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167
};

#if SQL2003_DEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   232,   232,   242,   243,   251,   252,   253,   254,   258,
     263,   268,   273,   281,   282,   290,   295,   303,   304,   308,
     312,   316,   317,   318,   322,   330,   335,   340,   345,   350,
     359,   368,   385,   402,   403,   411,   420,   428,   433,   440,
     441,   468,   498,   499,   529,   530,   545,   552,   554,   556,
     559,   588,   589,   598,   606,   607,   615,   616,   624,   625,
     626,   636,   637,   641,   649,   650,   658,   667,   670,   674,
     681,   682,   692,   697,   705,   706,   715,   723,   724,   728,
     736,   737,   745,   746,   750,   757,   758,   766,   780,   789,
     790,   798,   799,   803,   804,   816,   821,   829,   830,   835,
     841,   847,   854,   860,   867,   873,   881,   887,   952,   957,
     962,   967,   972,   977,   982,   987,   996,   997,  1002,  1007,
    1028,  1053,  1064,  1075,  1086,  1093,  1094,  1099,  1100,  1108,
    1109,  1117,  1118,  1126,  1127,  1132,  1140,  1141,  1159,  1160,
    1161,  1162,  1163,  1164,  1165,  1169,  1177,  1185,  1190,  1198,
    1203,  1208,  1213,  1221,  1226,  1234,  1239,  1247,  1256,  1257,
    1262,  1270,  1271,  1279,  1280,  1288,  1289,  1297,  1298,  1303,
    1311,  1312,  1313,  1314,  1319,  1320,  1324,  1325,  1333,  1334,
    1341,  1341,  1345,  1346,  1347,  1348,  1349,  1350,  1351,  1352,
    1353,  1354,  1358,  1359,  1364,  1371,  1372,  1376,  1377,  1378,
    1379,  1380,  1384,  1385,  1386,  1387,  1388,  1389,  1390,  1391,
    1396,  1404,  1405,  1409,  1410,  1418,  1423,  1431,  1432,  1441,
    1442,  1443,  1444,  1449,  1455,  1461,  1467,  1473,  1485,  1500,
    1501,  1502,  1503,  1504,  1505,  1506,  1507,  1508,  1509,  1513,
    1522,  1523,  1524,  1525,  1526,  1530,  1534,  1543,  1548,  1552,
    1560,  1561,  1565,  1566,  1571,  1572,  1581,  1582,  1586,  1587,
    1591,  1592,  1593,  1597,  1601,  1606,  1607,  1608,  1612,  1616,
    1617,  1618,  1619,  1620,  1624,  1630,  1642,  1648,  1655,  1662,
    1669,  1678,  1685,  1692,  1699,  1706,  1715,  1722,  1735,  1736,
    1737,  1738,  1742,  1749,  1754,  1759,  1764,  1772,  1780,  1786,
    1791,  1799,  1804,  1809,  1814,  1815,  1820,  1821,  1822,  1823,
    1824,  1825,  1829,  1837,  1842,  1846,  1852,  1856,  1857,  1862,
    1869,  1874,  1875,  1880,  1885,  1892,  1896,  1900,  1904,  1908,
    1915,  1922,  1926,  1931,  1936,  1941,  1945,  1949,  1954,  1959,
    1964,  1968,  1975,  1976,  1980,  1985,  1990,  1995,  2000,  2005,
    2010,  2014,  2018,  2022,  2026,  2030,  2034,  2041,  2046,  2050,
    2054,  2061,  2066,  2071,  2076,  2081,  2086,  2091,  2095,  2100,
    2104,  2109,  2113,  2117,  2124,  2129,  2133,  2138,  2145,  2150,
    2154,  2159,  2163,  2168,  2173,  2178,  2183,  2188,  2192,  2197,
    2201,  2206,  2210,  2217,  2222,  2227,  2232,  2240,  2244,  2248,
    2255,  2259,  2263,  2270,  2274,  2281,  2282,  2283,  2287,  2288,
    2292,  2293,  2297,  2298,  2303,  2313,  2314,  2317,  2318,  2321,
    2322,  2323,  2324,  2325,  2326,  2327,  2328,  2329,  2330,  2331,
    2332,  2333,  2334,  2335,  2336,  2337,  2338,  2342,  2343,  2344,
    2345,  2346,  2347,  2348,  2349,  2350,  2351,  2352,  2353,  2354,
    2355,  2356,  2357,  2358,  2359,  2360,  2361,  2362,  2363,  2364,
    2365,  2366,  2367,  2368,  2369,  2370,  2371,  2372,  2373,  2374,
    2375,  2376,  2380,  2381,  2382,  2385,  2386,  2387,  2388,  2389,
    2393,  2394,  2401,  2402
};
#endif

#if SQL2003_DEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NAME", "STRING", "INTNUM", "BOOL",
  "APPROXNUM", "NULLX", "UNKNOWN", "QUESTIONMARK", "PARAM", "UMINUS",
  "'('", "')'", "'.'", "ALL", "AND", "ANY", "ARRAY", "AS", "ASC", "AVG",
  "BETWEEN", "BIGINT", "BINARY", "BLOB", "BOOLEAN", "BY", "CALL", "CASE",
  "CAST", "CHAR", "CHARACTER", "CHARACTERS", "CLOB", "CNNOP", "COALESCE",
  "CODE_UNITS", "COLLATE", "COMP_EQ", "COMP_GE", "COMP_GT", "COMP_LE",
  "COMP_LT", "COMP_NE", "CONVERT", "CORRESPONDING", "COUNT", "CROSS",
  "CUME_DIST", "CURRENT", "CURRENT_TIMESTAMP", "CURRENT_USER", "DATE",
  "DAY", "DEC", "DECIMAL", "DEFAULT", "DELETE", "DENSE_RANK", "DESC",
  "DISTINCT", "DOUBLE", "ELSE", "END", "END_P", "ESCAPE", "ERROR",
  "EXCEPT", "EXCLUDE", "EXISTS", "FLOAT", "FOLLOWING", "FOR", "FROM",
  "FULL", "G", "GROUP", "GROUPING", "HAVING", "HOUR", "IN", "INNER",
  "INSERT", "INT", "INTEGER", "INTERSECT", "INTERVAL", "INTO", "IS",
  "JOIN", "K", "LARGE", "LEFT", "LIKE", "M", "MAX", "MIN", "MINUTE", "MOD",
  "MONTH", "MULTISET", "NATIONAL", "NATURAL", "NCHAR", "NCLOB", "NO",
  "NOT", "NULLIF", "NUMERIC", "OBJECT", "OCTETS", "OF", "ON", "ONLY", "OR",
  "ORDER", "OTHERS", "OUTER", "OVER", "PARTITION", "PERCENT_RANK",
  "PRECEDING", "PRECISION", "RANGE", "RANK", "READ", "REAL", "RECURSIVE",
  "REF", "RIGHT", "ROW", "ROWS", "ROW_NUMBER", "SCOPE", "SECOND", "SELECT",
  "SESSION_USER", "SET", "SETS", "SMALLINT", "SOME", "STDDEV_POP",
  "STDDEV_SAMP", "SUM", "SYSTEM_USER", "THEN", "TIES", "TIME", "TIMESTAMP",
  "TO", "UNBOUNDED", "UNION", "UPDATE", "USING", "VALUES", "VARCHAR",
  "VARYING", "VAR_POP", "VAR_SAMP", "WHEN", "WHERE", "WITH", "WITHOUT",
  "YEAR", "ZONE", "TOP", "';'", "'{'", "'}'", "','", "'*'", "'^'", "'+'",
  "'-'", "'!'", "'|'", "'/'", "'%'", "'&'", "$accept", "sql_stmt",
  "stmt_list", "stmt", "call_stmt", "sql_argument_list", "sql_argument",
  "value_expression", "sp_name", "dql_stmt", "dml_stmt", "insert_stmt",
  "insert_columns_and_source", "from_constructor", "delete_stmt",
  "update_stmt", "update_elem_list", "update_elem", "select_stmt",
  "query_expression", "query_expression_body", "query_term",
  "query_primary", "select_with_parens", "subquery", "table_subquery",
  "row_subquery", "simple_table", "opt_where", "opt_from_clause",
  "opt_groupby", "grouping_element_list", "grouping_element",
  "opt_order_by", "order_by", "sort_list", "sort_key", "opt_asc_desc",
  "opt_having", "with_clause", "with_list", "common_table_expr",
  "opt_derived_column_list", "simple_ident_list_with_parens",
  "simple_ident_list", "opt_distinct", "select_expr_list", "projection",
  "from_list", "table_reference", "table_primary",
  "table_primary_non_join", "opt_simple_ident_list_with_parens",
  "column_ref", "relation_factor", "joined_table", "qualified_join",
  "cross_join", "join_type", "join_outer", "search_condition",
  "boolean_term", "boolean_factor", "boolean_test", "boolean_primary",
  "predicate", "comparison_predicate", "quantified_comparison_predicate",
  "between_predicate", "like_predicate", "in_predicate", "null_predicate",
  "exists_predicate", "row_expr", "factor0", "factor1", "factor2",
  "factor3", "factor4", "row_expr_list", "in_expr", "truth_value",
  "comp_op", "cnn_op", "comp_all_some_any_op", "plus_minus_op",
  "star_div_percent_mod_op", "expr_const", "case_expr", "case_arg",
  "when_clause_list", "when_clause", "case_default", "func_expr",
  "aggregate_windowed_function", "aggregate_function_name",
  "ranking_windowed_function", "ranking_function_name", "over_clause",
  "window_specification", "window_name", "window_specification_details",
  "opt_existing_window_name", "opt_window_partition_clause",
  "opt_window_frame_clause", "window_frame_units", "window_frame_extent",
  "window_frame_start", "window_frame_preceding", "window_frame_between",
  "window_frame_bound", "window_frame_following",
  "opt_window_frame_exclusion", "scalar_function", "function_call_keyword",
  "data_type", "user_defined_type_name", "relation_factor_type",
  "reference_type", "collection_type", "predefined_type", "interval_type",
  "interval_qualifier", "start_field", "end_field",
  "single_datetime_field", "non_second_primary_datetime_field",
  "boolean_type", "datetime_type", "numeric_type", "exact_numeric_type",
  "approximate_numeric_type", "character_string_type",
  "binary_large_object_string_type", "national_character_string_type",
  "large_object_length", "char_length_units", "multiplier",
  "distinct_or_all", "all_some_any", "opt_as_label", "as_label", "label",
  "collate_clause", "name_r", "name_type", "reserved_type",
  "reserved_other", "reserved_non_type", "reserved", "opt_top_clause",
  "intnum_parens", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,    40,    41,    46,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303,   304,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,   316,   317,   318,   319,   320,   321,
     322,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,   338,   339,   340,   341,
     342,   343,   344,   345,   346,   347,   348,   349,   350,   351,
     352,   353,   354,   355,   356,   357,   358,   359,   360,   361,
     362,   363,   364,   365,   366,   367,   368,   369,   370,   371,
     372,   373,   374,   375,   376,   377,   378,   379,   380,   381,
     382,   383,   384,   385,   386,   387,   388,   389,   390,   391,
     392,   393,   394,   395,   396,   397,   398,   399,   400,   401,
     402,   403,   404,   405,   406,   407,   408,   409,   410,   411,
     412,   413,   414,   415,   416,   417,   418,   419,    59,   123,
     125,    44,    42,    94,    43,    45,    33,   124,    47,    37,
      38
};
# endif

#define YYPACT_NINF -519

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-519)))

#define YYTABLE_NINF -317

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      62,    24,  5317,   122,    33,    71,  5317,  5465,    96,   140,
     134,    66,  -519,  -519,  -519,  -519,  -519,  -519,  -519,   113,
      99,   167,  -519,  -519,    65,   249,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
     267,  -519,  -519,  -519,  -519,   274,  -519,  -519,  -519,  5317,
    5317,  -519,  -519,   118,   158,  5317,  -519,   150,   289,  5317,
    -519,  -519,    62,   286,  -519,  -519,    71,    71,    71,    99,
    -519,  1492,  4427,   168,    56,    79,  2703,  3049,  -519,  5317,
    5317,   314,  -519,   332,  -519,  3049,    65,    65,    65,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  1319,  -519,  3049,   341,
     356,   358,   373,  -519,  -519,   375,   376,  2357,   382,   386,
    -519,  -519,  3049,  3049,   387,   237,   380,  -519,  -519,  -519,
    -519,   389,   293,   393,  -519,  -519,   326,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,   599,   100,   225,   247,  -519,
    -519,  -519,  -519,  -519,  -519,   408,  -519,   412,  -519,  -519,
     413,   283,  4575,   414,  2357,  -519,  3981,   271,  3049,  -519,
    -519,  -519,    59,  -519,    79,  -519,  2530,  -519,   353,   260,
    1139,   168,   261,    26,  -519,   419,   263,   375,  1665,  -519,
     264,    34,   167,   167,  -519,  1319,   422,    47,   197,   423,
      49,   278,  3049,  3049,  3049,   435,    24,  -519,  -519,   434,
    -519,   599,  3049,   436,  -519,  -519,  -519,  2357,  5613,  2876,
    2357,  2357,   149,  3049,  -519,  5317,  -519,  -519,   415,  -519,
     270,  -519,   440,    52,  3049,   204,   416,   277,  1838,  3049,
     375,  -519,  -519,  -519,  3049,  -519,  -519,  -519,  -519,  -519,
    3049,  3049,  2011,   446,  3049,  3370,  5317,   458,  4723,   293,
    -519,    58,  -519,  -519,  -519,   461,  2530,  4871,   168,  2703,
    -519,  5021,  -519,  -519,  -519,  -519,  -519,  3049,  3049,  -519,
    5317,  -519,   309,   470,  3049,  -519,  -519,  -519,   475,  -519,
    -519,  3049,  -519,  2357,   432,   278,    -4,   484,    40,   487,
    -519,    84,  -519,  -519,  -519,  -519,   417,   499,  -519,    17,
      50,   501,  -519,   503,   506,   396,   508,  -519,  -519,   212,
     333,    51,   509,   513,  -519,   514,  -519,    41,    43,   516,
    -519,  -519,    67,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,   255,  -519,   491,   517,  -519,
    -519,   523,   393,  -519,  -519,  -519,    87,  -519,     6,  -519,
    -519,  -519,  -519,  2530,  -519,  -519,  -519,   532,    44,  3049,
     440,  3049,  -519,  -519,  -519,  -519,  -519,    49,  -519,   100,
    -519,   225,   247,  -519,  -519,  -519,   527,     7,  3049,   424,
       8,  -519,   307,  -519,  5317,  5317,   533,  -519,  4129,  4279,
    -519,   287,  -519,  -519,  4279,  -519,  -519,  -519,   469,  -519,
    -519,  -519,    49,  -519,  -519,   381,  -519,   163,  -519,    19,
     316,  3049,   489,  -519,  5613,  -519,  5317,  -519,  3049,   441,
     550,   552,   447,   546,   555,   450,   551,   558,   561,   562,
    -519,   565,   560,  -519,   426,  -519,    23,    20,    55,   569,
     464,   571,   558,   573,  5317,   577,   437,   438,   580,   439,
     442,   584,   581,  -519,   451,  -519,  -519,   931,  -519,  -519,
    3049,   585,  -519,  3049,    14,  -519,   131,   424,   424,    12,
    5169,  -519,  -519,  3522,  -519,  -519,  5317,  4129,   241,   586,
     289,   504,   479,  -519,  4871,   479,   479,  4871,   511,  -519,
     578,   528,  -519,  3049,  3049,    49,  -519,   239,   593,    13,
     596,   597,   600,   603,   605,   604,   606,   612,   411,   607,
       5,    10,   609,   620,   228,   621,   623,   616,   625,   521,
     632,   619,   633,   631,   634,    11,   635,   636,   468,   485,
     638,   490,   492,   639,   650,  5317,   642,    49,  -519,    49,
    3049,  3049,  -519,  -519,   424,  5317,  -519,  -519,  -519,  -519,
     318,  -519,  -519,  -519,  -519,  4871,  -519,  -519,   347,  -519,
    -519,  -519,  4871,    71,  2357,  -519,    49,    49,  -519,  -519,
    -519,   654,  -519,  -519,   558,   647,  -519,   558,   648,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,   231,  -519,  -519,   658,
    -519,   659,  -519,    15,   652,  -519,  -519,   653,   655,   661,
     656,   660,   663,  -519,   558,   657,  -519,  -519,   667,   539,
     187,  -519,  -519,   216,  -519,  -519,  -519,   662,   491,   931,
      49,    49,  -519,  -519,   664,   554,  3674,  -519,  2357,   368,
    3222,   293,   665,   666,  -519,   668,  -519,  -519,   669,   671,
    -519,   681,   682,   537,  -519,   678,  -519,   558,   683,   684,
    -519,   685,  5317,   547,   556,   559,   563,  -519,  -519,   680,
    -519,   672,   113,  -519,   331,   293,  2357,  2184,   564,  -519,
     530,    49,  -519,  -519,  -519,  -519,  -519,   689,   695,  -519,
     699,  -519,  -519,  -519,  -519,   548,   566,   567,   568,   931,
    3049,   143,  3826,   293,  -519,   703,  3222,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,  -519,   460,
    -519,  -519,  3222,  -519,    94,   590,   594,   602,   670,  -519,
    -519,  -519,   712,     4,    16,  -519,   710,  -519,  -519,  -519,
    -519,   194,  -519,  -519,  -519,  -519,    94,   598,  -519,   610,
    -519,  -519,  -519,  -519
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       5,     0,     0,     0,     0,    82,     0,     0,     0,     0,
       0,     3,     8,     6,     7,    23,    22,    21,    20,    61,
      37,    39,    42,    44,     0,     0,   415,   420,   468,   229,
     433,   421,   440,   427,   429,   424,   469,   441,   470,   442,
     237,   243,   425,   327,   431,   241,   463,   467,   426,   443,
     460,   439,   238,   328,   419,   434,   444,   437,   445,   438,
     230,   231,   329,   326,   446,   428,   471,   430,   447,   448,
     461,   449,   466,   242,   450,   451,   240,   462,   432,   452,
     453,   454,   244,   455,   456,   465,   435,   233,   234,   232,
     464,   423,   422,   457,   436,   235,   236,   458,   325,   459,
       0,    19,   475,   476,   477,   108,   479,   478,   416,     0,
       0,    83,    84,   480,     0,   452,    72,    74,    77,     0,
       1,     2,     5,     0,    36,    62,    82,    82,    82,    38,
      45,     0,     0,    51,     0,     0,     0,     0,    73,     0,
       0,     0,    78,     0,     4,     0,     0,     0,     0,   202,
     205,   203,   206,   204,   207,   208,     0,    10,   211,   440,
     469,   470,   282,   284,   209,     0,     0,     0,   471,     0,
     286,   287,     0,     0,     0,    13,    15,    47,    49,   172,
     170,     0,    17,   127,   129,   131,   133,   136,   138,   143,
     139,   141,   140,   142,   144,    18,   158,   161,   163,   165,
     167,   171,   174,   175,   222,   475,   221,   476,   220,   219,
     477,    99,     0,   109,     0,    31,     0,     0,     0,    24,
      28,    25,     0,   482,     0,   481,     0,    88,     0,    85,
     408,    51,    33,     0,    75,     0,    80,     0,     0,    63,
      64,    67,    41,    40,    43,     0,     0,     0,   176,     0,
     212,     0,     0,     0,     0,     0,     0,    48,   157,     0,
     132,     0,     0,     0,   168,   169,     9,     0,     0,     0,
       0,     0,     0,     0,   192,     0,   186,   184,   185,   182,
     183,   187,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   159,   195,   196,     0,   200,   197,   198,   199,   201,
       0,     0,     0,     0,     0,     0,     0,   111,     0,    52,
      29,   176,    30,    27,    26,     0,     0,     0,    51,     0,
     413,     0,    87,   409,   411,   412,    32,     0,     0,    79,
       0,    76,     0,     0,     0,    68,    69,    66,     0,    46,
     137,     0,   173,     0,   217,   213,     0,     0,     0,     0,
     278,     0,   277,    14,   417,   354,     0,   377,   330,   372,
     373,   371,   331,   350,   356,     0,   359,   353,   352,     0,
       0,   392,   391,   355,   360,     0,   351,     0,     0,     0,
     472,   473,    16,   289,   292,   290,   291,   288,   311,   474,
     309,   310,   308,   342,   343,   304,   307,   306,   293,   418,
     274,     0,   128,   130,   180,   181,     0,   134,     0,   414,
     188,   189,   190,     0,   178,   153,   155,     0,   149,     0,
       0,     0,   191,   193,   405,   407,   406,   145,   194,   160,
     146,   162,   164,   166,   403,   404,     0,     0,     0,     0,
       0,   101,   100,   115,     0,     0,   110,   483,     0,   408,
      53,    89,    91,    93,   408,    92,   116,   117,    54,    86,
     410,    34,    35,    81,    12,     0,    65,    46,   177,     0,
       0,     0,     0,   214,     0,   281,     0,   283,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     358,     0,   324,   312,     0,   314,   321,   381,   379,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   299,   300,     0,   303,   305,     0,   275,   135,
       0,     0,   156,     0,     0,   154,   151,   228,   224,     0,
       0,   239,   276,     0,   113,   114,     0,     0,     0,    92,
      97,     0,   125,   124,     0,   125,   125,     0,     0,    95,
       0,    70,    11,     0,     0,   218,   210,     0,     0,     0,
     375,     0,     0,   369,     0,     0,   367,     0,   396,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   389,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   294,   147,   179,   150,
       0,     0,   227,   223,   226,   250,   246,   245,   248,   103,
     102,   112,    94,    98,    96,     0,   126,   121,     0,   122,
     123,    90,     0,    82,     0,    50,   215,   216,   279,   280,
     285,     0,   376,   362,     0,     0,   361,     0,     0,   397,
     398,   402,   400,   401,   399,   395,   394,   370,   349,     0,
     347,     0,   357,     0,   319,   313,   317,     0,     0,     0,
       0,   387,     0,   382,     0,     0,   390,   345,     0,     0,
     334,   335,   336,   339,   340,   341,   365,     0,   302,     0,
     148,   152,   225,   251,     0,   252,     0,   120,     0,     0,
       0,    71,     0,     0,   364,     0,   363,   393,     0,     0,
     323,     0,     0,   320,   380,     0,   378,     0,     0,     0,
     385,     0,     0,     0,     0,     0,     0,   298,   301,   295,
     247,     0,    61,   105,   104,   119,     0,     0,   238,    55,
      56,    59,   374,   368,   366,   348,   346,     0,     0,   384,
       0,   383,   388,   344,   297,     0,     0,     0,     0,     0,
       0,   254,     0,   118,    58,     0,     0,   322,   318,   386,
     332,   333,   337,   338,   296,   253,   257,   256,   249,     0,
     107,   106,     0,    57,     0,     0,     0,     0,   269,   258,
     262,   259,     0,     0,     0,   265,     0,   267,   261,   260,
     263,     0,   255,    60,   266,   268,     0,     0,   271,     0,
     272,   264,   270,   273
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -519,  -519,   613,  -519,  -519,  -164,  -519,  -519,   617,  -519,
    -519,  -519,  -519,   515,  -519,  -519,   404,  -519,   -63,     2,
     714,   229,   591,  -519,  -125,  -156,  -519,  -519,  -203,  -519,
    -519,  -513,  -519,    21,  -519,   407,  -519,  -519,  -519,  -519,
     -33,  -519,  -519,  -127,   418,  -114,   427,  -519,   195,  -428,
     130,  -519,  -519,  -519,     9,  -381,  -519,  -519,  -519,  -163,
    -155,   482,  -109,  -519,  -519,  -519,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,   629,   465,   459,   455,   -41,  -519,  -212,
     336,   351,  -519,  -519,  -519,  -519,  -519,  -518,  -519,  -519,
     425,  -519,  -519,  -519,  -519,    32,  -519,   275,  -257,  -519,
     153,  -519,  -519,  -519,  -519,  -519,  -519,   -10,  -519,  -519,
     -35,  -519,  -519,  -519,  -519,   288,  -519,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -519,    -2,  -519,  -519,  -519,
    -519,  -519,  -519,  -519,  -519,  -484,   117,  -519,  -519,  -519,
    -300,  -519,   443,  -387,   385,  -500,  -519,  -519,  -519,  -519,
    -519,   543
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     9,    10,    11,    12,   174,   175,   176,   100,    13,
      14,    15,   219,   220,    16,    17,   231,   232,    18,   246,
      20,    21,    22,   177,   178,   449,   179,    23,   215,   318,
     551,   729,   730,   124,   125,   239,   240,   337,   625,    24,
     116,   117,   141,   142,   235,   113,   228,   229,   450,   451,
     452,   453,   614,   180,   181,   455,   456,   457,   548,   617,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   311,   196,   197,   198,   199,   200,   249,
     415,   407,   288,   289,   290,   294,   300,   201,   202,   251,
     344,   345,   472,   203,   204,   205,   206,   207,   531,   606,
     607,   684,   685,   722,   768,   769,   778,   785,   780,   781,
     786,   787,   792,   208,   209,   382,   383,   384,   385,   386,
     387,   388,   493,   494,   655,   495,   210,   390,   391,   392,
     393,   394,   395,   396,   397,   569,   645,   646,   438,   428,
     322,   323,   324,   291,   211,   398,   106,   107,   399,   108,
     136,   225
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     104,   247,    19,    25,   104,   104,   312,   222,   515,   258,
     516,   101,   146,   147,   148,   114,   474,   596,   584,   648,
     538,   528,   532,   520,   650,   667,   604,   630,   326,   700,
     481,   600,   274,   576,   102,   275,   575,     1,   102,   102,
     257,   347,   274,   274,   274,   275,   275,   275,   274,   274,
     274,   275,   275,   275,   505,   335,   508,   401,   260,   309,
     416,   340,   274,   484,   499,   275,   328,   539,   578,   216,
     274,   221,     1,   275,   333,     1,   274,   794,     1,   275,
     274,   331,   138,   275,   223,   274,   512,   111,   275,   795,
     247,     2,   224,   404,   274,   336,   405,   275,   149,   150,
     151,   152,   153,   353,   154,   155,   234,   104,   104,   538,
     482,   523,   257,   104,   217,   458,   618,   104,   133,   134,
     274,     3,   110,   275,    19,   119,   414,   789,   101,   468,
     104,   264,   265,   112,   430,   270,    19,   104,   104,   790,
     120,   102,   102,   485,   500,   775,     4,   102,   579,   540,
     693,   102,   164,   695,   549,   404,   539,   257,   405,   314,
     417,     5,   403,   270,   102,   257,   553,   274,   126,   513,
     275,   102,   102,   287,  -316,   483,   649,   -45,   577,   719,
     709,   651,   668,   287,   287,   287,   701,     7,   469,   287,
     287,   287,   257,     5,   689,   476,     5,   109,   601,     5,
     121,   521,     5,   287,   506,   507,   509,   510,   486,   501,
     104,   287,   218,   580,   104,   218,     6,   287,    25,     7,
     273,   287,     7,   740,    19,     7,   287,   419,   104,   341,
     123,     8,   -45,   274,   122,   287,   275,   276,   277,   278,
     279,   280,   281,   773,   102,   797,   783,   338,   102,   764,
     -45,   777,   127,   628,   128,   478,   784,   406,   512,   782,
     433,   287,   102,   130,   414,   639,   389,    43,   766,   640,
     602,   603,   798,   104,   292,   293,   767,   103,   784,   282,
     131,   103,   103,    43,   409,   135,   420,   283,   514,   132,
     541,   718,   284,    53,   275,   257,  -108,   137,   305,   421,
     380,   799,   140,   104,   104,   285,   104,   102,   287,    53,
     411,    62,   412,    63,   145,   104,   -45,   542,   338,   104,
    -109,   139,   533,   257,   543,   295,   454,    62,   104,    63,
     214,  -110,   544,   686,   237,   545,   541,   102,   102,   273,
     102,   513,   800,   644,  -112,   238,   752,   682,   492,   102,
     713,   714,   274,   102,   252,   275,   276,   277,   278,   279,
     280,   281,   102,   542,   654,   497,   498,   496,   341,   253,
     543,   254,   546,   286,   287,   242,   243,    98,   544,   715,
     716,   545,   619,   620,   103,   103,   255,   105,   256,   259,
     103,   105,   118,    98,   103,   262,   541,   296,   282,   263,
     268,   266,   269,   297,   298,   299,   283,   103,   267,   270,
     271,   284,   257,   613,   103,   103,   272,   541,   546,   257,
     301,   302,   257,   542,   285,   303,   304,   310,   317,   308,
     543,   319,   327,   329,   330,   334,   339,   342,   544,   343,
     349,   545,   104,   104,   542,   639,   104,   104,   350,   640,
     352,   543,   104,   413,   423,   410,   422,   454,   547,   544,
     439,   688,   545,   554,   149,   150,   151,   152,   153,   691,
     154,   155,   389,   444,   104,   447,   102,   102,   546,   464,
     102,   102,   726,   774,   465,   558,   102,   103,   641,   467,
     257,   103,   286,   287,   105,   105,   471,   257,   475,   546,
     118,   477,   104,   642,   105,   103,   380,   643,   102,   690,
     479,   775,   480,   586,   487,   389,   488,   213,   164,   489,
     490,   491,   502,   644,   118,   236,   503,   504,   104,   511,
     275,   104,   517,   725,   104,   104,   102,   518,   765,   338,
     522,   527,   104,   381,   530,   104,   454,   550,   536,   380,
     103,   552,   560,   454,   556,   561,   454,   562,   563,   564,
     565,   566,   102,   568,   567,   102,   570,   571,   102,   102,
     572,   753,   656,   573,   581,   582,   102,   574,   585,   102,
     103,   103,   587,   103,   583,   590,   588,   589,   591,   593,
     595,   592,   103,   104,   594,   615,   103,   307,   616,   598,
     612,   236,   622,   104,   678,   103,   623,   629,   624,   631,
     635,   632,   776,   104,   633,   325,   634,   638,   636,   637,
     104,   647,   273,   652,   454,   653,   657,   102,   658,   659,
     660,   454,   661,   663,   671,   274,   665,   102,   275,   276,
     277,   278,   279,   280,   281,   662,   664,   102,   666,   669,
     670,   672,   673,   676,   102,   677,   674,   679,   675,   692,
     105,   694,   696,   698,   699,   702,   705,   703,   708,   704,
     706,   710,   711,   707,   712,   721,   717,   389,   720,   732,
     733,   282,   734,   735,   104,   736,   737,   738,  -315,   283,
     442,   443,   739,   446,   284,   749,   745,   741,   742,   743,
     750,   756,   105,   757,   755,   746,   325,   285,   747,   758,
     104,   380,   748,   759,   760,   236,   772,   789,   102,   103,
     103,   744,   788,   103,   103,   790,   793,   796,   803,   103,
     802,   461,   761,   762,   763,   144,   143,   313,   129,   244,
     791,   466,   621,   751,   102,   687,   459,   389,   463,   381,
     104,   103,   402,   431,   429,   432,   525,   519,   683,   779,
     195,   801,   557,   697,   460,   230,   233,   315,     0,     0,
     473,     0,     0,     0,   241,   286,   287,     0,     0,   103,
       0,   380,     0,     0,   102,   248,     0,   250,     0,     0,
       0,     0,   381,     0,     0,     0,   261,     0,     0,     0,
       0,     0,     0,     0,     0,   103,     0,     0,   103,     0,
       0,   103,   103,     0,     0,     0,     0,     0,     0,   103,
       0,     0,   103,     0,     0,     0,     0,     0,     0,   534,
     535,     0,     0,   105,   325,     0,     0,     0,     0,   325,
       0,     0,     0,   261,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   105,     0,     0,     0,     0,     0,   195,     0,     0,
     103,     0,     0,     0,   248,     0,     0,     0,     0,     0,
     103,   346,     0,   348,     0,     0,     0,     0,     0,   105,
     103,   351,     0,     0,     0,     0,   195,   103,     0,   261,
     261,     0,   408,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   418,     0,   608,     0,   427,   610,     0,
       0,   611,   105,     0,     0,     0,     0,     0,     0,   105,
       0,   437,   105,   440,   354,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   230,     0,
       0,     0,     0,    29,   381,     0,   233,   462,     0,     0,
       0,   103,     0,   241,     0,     0,     0,     0,     0,     0,
       0,     0,   470,     0,     0,     0,     0,     0,     0,    40,
     105,    41,     0,     0,     0,     0,    43,   103,     0,     0,
     608,    45,     0,     0,     0,     0,     0,     0,     0,     0,
     105,     0,     0,     0,     0,     0,     0,   105,     0,     0,
      52,     0,    53,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   381,     0,     0,   103,    60,    61,
      62,     0,    63,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   524,     0,
     526,     0,     0,    73,     0,     0,     0,    76,     0,     0,
       0,     0,     0,     0,     0,    82,     0,   529,     0,     0,
       0,   724,     0,     0,    87,    88,    89,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      95,    96,     0,     0,     0,     0,    98,   105,     0,     0,
     555,     0,     0,     0,     0,     0,     0,   559,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   771,     0,     0,
       0,     0,    26,   320,     0,     0,     0,     0,     0,   597,
       0,     0,   599,     0,     0,     0,     0,     0,    27,   321,
      28,    29,     0,    30,    31,     0,     0,     0,     0,     0,
      32,    33,    34,    35,     0,   274,    36,    37,   275,     0,
       0,     0,   626,   627,     0,    38,    39,    40,     0,    41,
       0,     0,     0,    42,    43,     0,    44,     0,     0,    45,
      46,     0,     0,     0,     0,     0,     0,     0,     0,    47,
       0,    48,    49,    50,     0,     0,    51,     0,    52,     0,
      53,     0,     0,     0,    54,    55,     0,    56,     0,   680,
     681,    57,    58,     0,     0,    59,    60,    61,    62,     0,
      63,    64,     0,     0,    65,     0,     0,     0,    66,    67,
      68,    69,    70,   261,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
      86,     0,    87,    88,    89,     0,     0,    90,    91,    92,
       0,    93,     0,     0,     0,     0,    94,     0,    95,    96,
       0,     0,     0,    97,    98,    99,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   287,   261,     0,   731,
       0,     0,    26,   149,   150,   151,   152,   153,     0,   154,
     155,     0,   245,     0,     0,     0,     0,     0,    27,     0,
      28,    29,     0,    30,    31,     0,     0,     0,     0,   158,
     159,    33,    34,    35,     0,   261,   160,    37,     0,     0,
       0,     0,     0,     0,     0,   161,    39,    40,     0,    41,
       0,   162,   163,    42,    43,     0,    44,   164,     0,    45,
      46,     0,     0,     0,     0,   731,     0,     0,     0,    47,
     165,    48,    49,    50,     0,     0,    51,     0,    52,     0,
      53,   731,     0,     0,    54,    55,     0,    56,     0,     0,
       0,    57,    58,   166,     0,    59,    60,    61,    62,     0,
      63,    64,     0,     0,    65,     0,     0,   167,   168,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
     169,    81,     0,    82,    83,    84,     5,   170,     0,    85,
      86,     0,    87,    88,    89,   171,     0,    90,    91,    92,
       0,    93,     0,     0,     0,     0,    94,     0,    95,    96,
       0,     0,     7,    97,    98,    99,     0,     0,     0,     0,
       0,     0,     0,   172,   173,    26,   149,   150,   151,   152,
     153,     0,   154,   155,     0,   156,   157,     0,     0,     0,
       0,    27,     0,    28,    29,     0,    30,    31,     0,     0,
       0,     0,   158,   159,    33,    34,    35,     0,     0,   160,
      37,     0,     0,     0,     0,     0,     0,     0,   161,    39,
      40,     0,    41,     0,   162,   163,    42,    43,     0,    44,
     164,     0,    45,    46,     0,     0,     0,     0,     0,     0,
       0,     0,    47,   165,    48,    49,    50,     0,     0,    51,
       0,    52,     0,    53,     0,     0,     0,    54,    55,     0,
      56,     0,     0,     0,    57,    58,   166,     0,    59,    60,
      61,    62,     0,    63,    64,     0,     0,    65,     0,     0,
     167,   168,    67,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
      78,    79,    80,   169,    81,     0,    82,    83,    84,     0,
     170,     0,    85,    86,     0,    87,    88,    89,   171,     0,
      90,    91,    92,     0,    93,     0,     0,     0,     0,    94,
       0,    95,    96,     0,     0,     0,    97,    98,    99,     0,
       0,     0,     0,     0,     0,     0,   172,   173,    26,   149,
     150,   151,   152,   153,     0,   154,   155,     0,   156,   332,
       0,     0,     0,     0,    27,     0,    28,    29,     0,    30,
      31,     0,     0,     0,     0,   158,   159,    33,    34,    35,
       0,     0,   160,    37,     0,     0,     0,     0,     0,     0,
       0,   161,    39,    40,     0,    41,     0,   162,   163,    42,
      43,     0,    44,   164,     0,    45,    46,     0,     0,     0,
       0,     0,     0,     0,     0,    47,   165,    48,    49,    50,
       0,     0,    51,     0,    52,     0,    53,     0,     0,     0,
      54,    55,     0,    56,     0,     0,     0,    57,    58,   166,
       0,    59,    60,    61,    62,     0,    63,    64,     0,     0,
      65,     0,     0,   167,   168,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,   169,    81,     0,    82,
      83,    84,     0,   170,     0,    85,    86,     0,    87,    88,
      89,   171,     0,    90,    91,    92,     0,    93,     0,     0,
       0,     0,    94,     0,    95,    96,     0,     0,     0,    97,
      98,    99,     0,     0,     0,     0,     0,     0,     0,   172,
     173,    26,   149,   150,   151,   152,   153,     0,   154,   155,
       0,   226,     0,     0,   424,     0,   425,    27,     0,    28,
      29,     0,    30,    31,     0,     0,     0,     0,   158,   159,
      33,    34,    35,     0,     0,   160,    37,     0,     0,     0,
       0,     0,     0,     0,   161,    39,    40,     0,    41,     0,
     162,   163,    42,    43,     0,    44,   164,     0,    45,    46,
       0,     0,     0,     0,     0,     0,     0,     0,    47,     0,
      48,    49,    50,     0,     0,    51,     0,    52,     0,    53,
       0,     0,     0,    54,    55,     0,    56,     0,     0,     0,
      57,    58,   166,     0,    59,    60,    61,    62,     0,    63,
      64,     0,     0,    65,     0,     0,     0,   168,    67,    68,
      69,    70,     0,    71,     0,     0,    72,     0,     0,     0,
      73,    74,    75,     0,    76,    77,    78,    79,    80,   169,
      81,     0,    82,    83,    84,     0,   170,     0,    85,    86,
     426,    87,    88,    89,   171,     0,    90,    91,    92,     0,
      93,     0,     0,     0,     0,    94,     0,    95,    96,     0,
       0,     0,    97,    98,    99,     0,     0,     0,     0,     0,
       0,     0,   172,   173,    26,   149,   150,   151,   152,   153,
       0,   154,   155,     0,   226,     0,     0,   434,     0,     0,
      27,     0,    28,    29,     0,    30,    31,     0,     0,     0,
       0,   158,   159,    33,    34,    35,     0,     0,   160,    37,
       0,     0,     0,     0,     0,     0,     0,   161,    39,    40,
       0,    41,     0,   162,   163,    42,    43,     0,    44,   164,
       0,    45,    46,   435,     0,     0,     0,     0,     0,     0,
       0,    47,     0,    48,    49,    50,     0,     0,    51,     0,
      52,     0,    53,     0,     0,     0,    54,    55,     0,    56,
       0,     0,     0,    57,    58,   166,     0,    59,    60,    61,
      62,     0,    63,    64,     0,     0,    65,     0,     0,     0,
     168,    67,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,    78,
      79,    80,   169,    81,     0,    82,    83,    84,     0,   170,
       0,    85,    86,     0,    87,    88,    89,   171,     0,    90,
      91,    92,     0,    93,     0,     0,     0,     0,    94,     0,
      95,    96,     0,     0,     0,    97,    98,    99,     0,     0,
       0,     0,     0,   436,     0,   172,   173,    26,   149,   150,
     151,   152,   153,     0,   154,   155,     0,   316,   754,     0,
       0,     0,     0,    27,     0,    28,    29,     0,    30,    31,
       0,     0,     0,     0,   158,   159,    33,    34,    35,     0,
       0,   160,    37,     0,     0,     0,     0,     0,     0,     0,
     161,    39,    40,     0,    41,     0,   162,   163,    42,    43,
       0,    44,   164,     0,    45,    46,     0,     0,     0,     0,
       0,     0,     0,     0,    47,     0,    48,    49,    50,     0,
       0,    51,     0,    52,     0,    53,     0,     0,     0,    54,
      55,     0,    56,     0,     0,     0,    57,    58,   166,     0,
      59,    60,    61,    62,     0,    63,    64,     0,     0,    65,
       0,     0,     0,   168,    67,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,    78,    79,    80,   169,    81,     0,    82,    83,
      84,     5,   170,     0,    85,    86,     0,    87,    88,    89,
     171,     0,    90,    91,    92,     0,    93,     0,     0,     0,
       0,    94,     0,    95,    96,     0,     0,     7,    97,    98,
      99,     0,     0,     0,     0,     0,     0,     0,   172,   173,
      26,   149,   150,   151,   152,   153,     0,   154,   155,     0,
     156,     0,     0,     0,     0,     0,    27,     0,    28,    29,
       0,    30,    31,     0,     0,     0,     0,   158,   159,    33,
      34,    35,     0,     0,   160,    37,     0,     0,     0,     0,
       0,     0,     0,   161,    39,    40,     0,    41,     0,   162,
     163,    42,    43,     0,    44,   164,     0,    45,    46,     0,
       0,     0,     0,     0,     0,     0,     0,    47,   165,    48,
      49,    50,     0,     0,    51,     0,    52,     0,    53,     0,
       0,     0,    54,    55,     0,    56,     0,     0,     0,    57,
      58,   166,     0,    59,    60,    61,    62,     0,    63,    64,
       0,     0,    65,     0,     0,   167,   168,    67,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,    78,    79,    80,   169,    81,
       0,    82,    83,    84,     0,   170,     0,    85,    86,     0,
      87,    88,    89,   171,     0,    90,    91,    92,     0,    93,
       0,     0,     0,     0,    94,     0,    95,    96,     0,     0,
       0,    97,    98,    99,     0,     0,     0,     0,     0,     0,
       0,   172,   173,    26,   149,   150,   151,   152,   153,     0,
     154,   155,     0,   316,     0,     0,     0,     0,     0,    27,
       0,    28,    29,     0,    30,    31,     0,     0,     0,     0,
     158,   159,    33,    34,    35,     0,     0,   160,    37,     0,
       0,     0,     0,     0,     0,     0,   161,    39,    40,     0,
      41,     0,   162,   163,    42,    43,     0,    44,   164,     0,
      45,    46,     0,     0,     0,     0,     0,     0,     0,     0,
      47,     0,    48,    49,    50,     0,     0,    51,     0,    52,
       0,    53,     0,     0,     0,    54,    55,     0,    56,     0,
       0,     0,    57,    58,   166,     0,    59,    60,    61,    62,
       0,    63,    64,     0,     0,    65,     0,     0,     0,   168,
      67,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,    78,    79,
      80,   169,    81,     0,    82,    83,    84,     5,   170,     0,
      85,    86,     0,    87,    88,    89,   171,     0,    90,    91,
      92,     0,    93,     0,     0,     0,     0,    94,     0,    95,
      96,     0,     0,     7,    97,    98,    99,     0,     0,     0,
       0,     0,     0,     0,   172,   173,    26,   149,   150,   151,
     152,   153,     0,   154,   155,     0,   226,     0,     0,     0,
       0,     0,    27,     0,    28,    29,     0,    30,    31,     0,
       0,     0,     0,   158,   159,    33,    34,    35,     0,     0,
     160,    37,     0,     0,     0,     0,     0,     0,     0,   161,
      39,    40,     0,    41,     0,   162,   163,    42,    43,     0,
      44,   164,     0,    45,    46,     0,     0,     0,     0,     0,
       0,     0,     0,    47,     0,    48,    49,    50,     0,     0,
      51,     0,    52,     0,    53,     0,     0,     0,    54,    55,
       0,    56,     0,     0,     0,    57,    58,   166,     0,    59,
      60,    61,    62,     0,    63,    64,     0,     0,    65,     0,
       0,     0,   168,    67,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,    78,    79,    80,   169,    81,     0,    82,    83,    84,
       0,   170,     0,    85,    86,     0,    87,    88,    89,   171,
       0,    90,    91,    92,     0,    93,     0,     0,     0,     0,
      94,     0,    95,    96,     0,     0,     0,    97,    98,    99,
       0,     0,     0,     0,     0,   227,     0,   172,   173,    26,
     149,   150,   151,   152,   153,     0,   154,   155,     0,   226,
     400,     0,     0,     0,     0,    27,     0,    28,    29,     0,
      30,    31,     0,     0,     0,     0,   158,   159,    33,    34,
      35,     0,     0,   160,    37,     0,     0,     0,     0,     0,
       0,     0,   161,    39,    40,     0,    41,     0,   162,   163,
      42,    43,     0,    44,   164,     0,    45,    46,     0,     0,
       0,     0,     0,     0,     0,     0,    47,     0,    48,    49,
      50,     0,     0,    51,     0,    52,     0,    53,     0,     0,
       0,    54,    55,     0,    56,     0,     0,     0,    57,    58,
     166,     0,    59,    60,    61,    62,     0,    63,    64,     0,
       0,    65,     0,     0,     0,   168,    67,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,    78,    79,    80,   169,    81,     0,
      82,    83,    84,     0,   170,     0,    85,    86,     0,    87,
      88,    89,   171,     0,    90,    91,    92,     0,    93,     0,
       0,     0,     0,    94,     0,    95,    96,     0,     0,     0,
      97,    98,    99,     0,     0,     0,     0,     0,     0,     0,
     172,   173,    26,   149,   150,   151,   152,   153,     0,   154,
     155,     0,   226,     0,     0,     0,     0,     0,    27,     0,
      28,    29,     0,    30,    31,     0,     0,     0,     0,   158,
     159,    33,    34,    35,     0,     0,   160,    37,     0,     0,
       0,     0,     0,     0,     0,   161,    39,    40,     0,    41,
       0,   162,   163,    42,    43,     0,    44,   164,     0,    45,
      46,     0,     0,     0,     0,     0,     0,     0,     0,    47,
       0,    48,    49,    50,     0,     0,    51,     0,    52,     0,
      53,     0,     0,     0,    54,    55,     0,    56,     0,     0,
       0,    57,    58,   166,     0,    59,    60,    61,    62,     0,
      63,    64,     0,     0,    65,     0,     0,     0,   168,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
     169,    81,     0,    82,    83,    84,     0,   170,     0,    85,
      86,     0,    87,    88,    89,   171,     0,    90,    91,    92,
       0,    93,     0,     0,     0,     0,    94,     0,    95,    96,
       0,     0,     0,    97,    98,    99,     0,     0,     0,     0,
       0,     0,     0,   172,   173,    26,   149,   150,   151,   152,
     153,     0,   154,   155,     0,   727,     0,     0,     0,     0,
       0,    27,     0,    28,    29,     0,    30,    31,     0,     0,
       0,     0,   158,   159,    33,    34,    35,     0,     0,   160,
      37,     0,     0,     0,     0,     0,     0,     0,   161,    39,
      40,     0,    41,     0,   162,   163,    42,    43,     0,    44,
     164,     0,    45,    46,     0,     0,     0,     0,     0,     0,
       0,     0,    47,     0,    48,    49,    50,     0,     0,    51,
       0,   728,     0,    53,     0,     0,     0,    54,    55,     0,
      56,     0,     0,     0,    57,    58,   166,     0,    59,    60,
      61,    62,     0,    63,    64,     0,     0,    65,     0,     0,
       0,   168,    67,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
      78,    79,    80,   169,    81,     0,    82,    83,    84,     0,
     170,     0,    85,    86,     0,    87,    88,    89,   171,     0,
      90,    91,    92,    26,    93,     0,     0,     0,     0,    94,
       0,    95,    96,     0,     0,   212,    97,    98,    99,    27,
       0,    28,    29,     0,    30,    31,   172,   173,     0,     0,
       0,    32,    33,    34,    35,     0,     0,    36,    37,     0,
       0,     0,     0,     0,     0,     0,    38,    39,    40,     0,
      41,     0,     0,     0,    42,    43,     0,    44,     0,     0,
      45,    46,     0,     0,     0,     0,     0,     0,     0,     0,
      47,     0,    48,    49,    50,     0,     0,    51,     0,    52,
       0,    53,     0,     0,     0,    54,    55,     0,    56,     0,
       0,     0,    57,    58,     0,     0,    59,    60,    61,    62,
       0,    63,    64,     0,     0,    65,     0,     0,     0,    66,
      67,    68,    69,    70,     0,    71,     0,     0,    72,     0,
       0,     0,    73,    74,    75,     0,    76,    77,    78,    79,
      80,     0,    81,     0,    82,    83,    84,     0,     0,     0,
      85,    86,     0,    87,    88,    89,     0,     0,    90,    91,
      92,     0,    93,     0,     0,    26,     0,    94,     0,    95,
      96,     0,     0,     0,    97,    98,    99,   445,     0,     0,
       0,    27,   441,    28,    29,     0,    30,    31,     0,     0,
       0,     0,     0,    32,    33,    34,    35,     0,     0,    36,
      37,     0,     0,     0,     0,     0,     0,     0,    38,    39,
      40,     0,    41,     0,     0,     0,    42,    43,     0,    44,
       0,     0,    45,    46,     0,     0,     0,     0,     0,     0,
       0,     0,    47,     0,    48,    49,    50,     0,     0,    51,
       0,    52,     0,    53,     0,     0,     0,    54,    55,     0,
      56,     0,     0,     0,    57,    58,     0,     0,    59,    60,
      61,    62,     0,    63,    64,     0,     0,    65,     0,     0,
       0,    66,    67,    68,    69,    70,     0,    71,     0,     0,
      72,     0,     0,     0,    73,    74,    75,     0,    76,    77,
      78,    79,    80,     0,    81,     0,    82,    83,    84,     0,
       0,     0,    85,    86,     0,    87,    88,    89,     0,     0,
      90,    91,    92,     0,    93,     0,     0,    26,     0,    94,
       0,    95,    96,     0,     0,     0,    97,    98,    99,     0,
       0,     0,     0,    27,   609,    28,    29,     0,    30,    31,
       0,     0,     0,     0,     0,    32,    33,    34,    35,     0,
       0,    36,    37,     0,     0,     0,     0,     0,     0,     0,
      38,    39,    40,     0,    41,     0,     0,     0,    42,    43,
       0,    44,     0,     0,    45,    46,     0,     0,     0,     0,
       0,     0,     0,     0,    47,     0,    48,    49,    50,     0,
       0,    51,     0,    52,     0,    53,     0,     0,     0,    54,
      55,     0,    56,     0,     0,     0,    57,    58,     0,     0,
      59,    60,    61,    62,     0,    63,    64,     0,     0,    65,
       0,     0,     0,    66,    67,    68,    69,    70,     0,    71,
       0,     0,    72,     0,     0,     0,    73,    74,    75,     0,
      76,    77,    78,    79,    80,     0,    81,     0,    82,    83,
      84,     0,     0,     0,    85,    86,     0,    87,    88,    89,
       0,     0,    90,    91,    92,     0,    93,     0,     0,    26,
       0,    94,     0,    95,    96,     0,     0,     0,    97,    98,
      99,     0,     0,     0,     0,    27,   723,    28,    29,     0,
      30,    31,     0,     0,     0,     0,     0,    32,    33,    34,
      35,     0,     0,    36,    37,     0,     0,     0,     0,     0,
       0,     0,    38,    39,    40,     0,    41,     0,     0,     0,
      42,    43,     0,    44,     0,     0,    45,    46,     0,     0,
       0,     0,     0,     0,     0,     0,    47,     0,    48,    49,
      50,     0,     0,    51,     0,    52,     0,    53,     0,     0,
       0,    54,    55,     0,    56,     0,     0,     0,    57,    58,
       0,     0,    59,    60,    61,    62,     0,    63,    64,     0,
       0,    65,     0,     0,     0,    66,    67,    68,    69,    70,
       0,    71,     0,     0,    72,     0,     0,     0,    73,    74,
      75,     0,    76,    77,    78,    79,    80,     0,    81,     0,
      82,    83,    84,     0,     0,     0,    85,    86,     0,    87,
      88,    89,     0,     0,    90,    91,    92,     0,    93,     0,
       0,     0,     0,    94,    26,    95,    96,     0,     0,     0,
      97,    98,    99,     0,     1,     0,     0,     0,   770,     0,
      27,     0,    28,    29,     0,    30,    31,     0,     0,     0,
       0,     0,    32,    33,    34,    35,     0,     0,    36,    37,
       0,     0,     0,     0,     0,     0,     0,    38,    39,    40,
       0,    41,     0,     0,     0,    42,    43,     0,    44,     0,
       0,    45,    46,     0,     0,     0,     0,     0,     0,     0,
       0,    47,     0,    48,    49,    50,     0,     0,    51,     0,
      52,     0,    53,     0,     0,     0,    54,    55,     0,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,     0,    63,    64,     0,     0,    65,     0,     0,     0,
      66,    67,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,    78,
      79,    80,     0,    81,     0,    82,    83,    84,     5,     0,
       0,    85,    86,     0,    87,    88,    89,     0,     0,    90,
      91,    92,    26,    93,     0,     0,     0,     0,    94,     0,
      95,    96,   537,     0,     7,    97,    98,    99,    27,     0,
      28,    29,     0,    30,    31,     0,     0,     0,     0,     0,
      32,    33,    34,    35,     0,     0,    36,    37,     0,     0,
       0,     0,     0,     0,     0,    38,    39,    40,     0,    41,
       0,     0,     0,    42,    43,     0,    44,     0,     0,    45,
      46,     0,     0,     0,     0,     0,     0,     0,     0,    47,
       0,    48,    49,    50,     0,     0,    51,     0,    52,     0,
      53,     0,     0,     0,    54,    55,     0,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,     0,
      63,    64,     0,     0,    65,     0,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     5,     0,     0,    85,
      86,     0,    87,    88,    89,     0,     0,    90,    91,    92,
       0,    93,    26,   320,     0,     0,    94,     0,    95,    96,
       0,     0,     7,    97,    98,    99,     0,     0,    27,   321,
      28,    29,     0,    30,    31,     0,     0,     0,     0,     0,
      32,    33,    34,    35,     0,     0,    36,    37,     0,     0,
       0,     0,     0,     0,     0,    38,    39,    40,     0,    41,
       0,     0,     0,    42,    43,     0,    44,     0,     0,    45,
      46,     0,     0,     0,     0,     0,     0,     0,     0,    47,
       0,    48,    49,    50,     0,     0,    51,     0,    52,     0,
      53,     0,     0,     0,    54,    55,     0,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,     0,
      63,    64,     0,     0,    65,     0,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
      86,     0,    87,    88,    89,     0,     0,    90,    91,    92,
      26,    93,     0,     0,     0,     0,    94,     0,    95,    96,
       0,     0,   212,    97,    98,    99,    27,     0,    28,    29,
       0,    30,    31,     0,     0,     0,     0,     0,    32,    33,
      34,    35,     0,     0,    36,    37,     0,     0,     0,     0,
       0,     0,     0,    38,    39,    40,     0,    41,     0,     0,
       0,    42,    43,     0,    44,     0,     0,    45,    46,     0,
       0,     0,     0,     0,     0,     0,     0,    47,     0,    48,
      49,    50,     0,     0,    51,     0,    52,     0,    53,     0,
       0,     0,    54,    55,     0,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,     0,    63,    64,
       0,     0,    65,     0,     0,     0,    66,    67,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,    78,    79,    80,     0,    81,
       0,    82,    83,    84,     0,     0,     0,    85,    86,     0,
      87,    88,    89,     0,     0,    90,    91,    92,    26,    93,
       0,     0,     0,     0,    94,     0,    95,    96,     0,     0,
     306,    97,    98,    99,    27,     0,    28,    29,     0,    30,
      31,     0,     0,     0,     0,     0,    32,    33,    34,    35,
       0,     0,    36,    37,     0,     0,     0,     0,     0,     0,
       0,    38,    39,    40,     0,    41,     0,     0,     0,    42,
      43,     0,    44,     0,     0,    45,    46,     0,     0,     0,
       0,     0,     0,     0,     0,    47,     0,    48,    49,    50,
       0,     0,    51,     0,    52,     0,    53,     0,     0,     0,
      54,    55,     0,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,     0,    63,    64,     0,     0,
      65,     0,     0,     0,    66,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,    79,    80,     0,    81,     0,    82,
      83,    84,     0,     0,     0,    85,    86,     0,    87,    88,
      89,     0,     0,    90,    91,    92,    26,    93,     0,     0,
       0,     0,    94,     0,    95,    96,     0,     0,   445,    97,
      98,    99,    27,     0,    28,    29,     0,    30,    31,     0,
       0,     0,     0,     0,    32,    33,    34,    35,     0,     0,
      36,    37,     0,     0,     0,     0,     0,     0,     0,    38,
      39,    40,     0,    41,     0,     0,     0,    42,    43,     0,
      44,     0,     0,    45,    46,     0,     0,     0,     0,     0,
       0,     0,     0,    47,     0,    48,    49,    50,     0,     0,
      51,     0,    52,     0,    53,     0,     0,     0,    54,    55,
       0,    56,     0,     0,     0,    57,    58,     0,     0,    59,
      60,    61,    62,     0,    63,    64,     0,     0,    65,     0,
       0,     0,    66,    67,    68,    69,    70,     0,    71,     0,
       0,    72,     0,     0,     0,    73,    74,    75,     0,    76,
      77,    78,    79,    80,     0,    81,     0,    82,    83,    84,
       0,     0,     0,    85,    86,     0,    87,    88,    89,     0,
       0,    90,    91,    92,    26,    93,     0,     0,     0,     0,
      94,     0,    95,    96,   448,     0,     0,    97,    98,    99,
      27,     0,    28,    29,     0,    30,    31,     0,     0,     0,
       0,     0,    32,    33,    34,    35,     0,     0,    36,    37,
       0,     0,     0,     0,     0,     0,     0,    38,    39,    40,
       0,    41,     0,     0,     0,    42,    43,     0,    44,     0,
       0,    45,    46,     0,     0,     0,     0,     0,     0,     0,
       0,    47,     0,    48,    49,    50,     0,     0,    51,     0,
      52,     0,    53,     0,     0,     0,    54,    55,     0,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,     0,    63,    64,     0,     0,    65,     0,     0,     0,
      66,    67,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,    78,
      79,    80,     0,    81,     0,    82,    83,    84,     0,     0,
       0,    85,    86,     0,    87,    88,    89,     0,     0,    90,
      91,    92,     0,    93,    26,   320,     0,     0,    94,     0,
      95,    96,     0,     0,     0,    97,    98,    99,     0,     0,
      27,     0,    28,    29,     0,    30,    31,     0,     0,     0,
       0,     0,    32,    33,    34,    35,     0,     0,    36,    37,
       0,     0,     0,     0,     0,     0,     0,    38,    39,    40,
       0,    41,     0,     0,     0,    42,    43,     0,    44,     0,
       0,    45,    46,     0,     0,     0,     0,     0,     0,     0,
       0,    47,     0,    48,    49,    50,     0,     0,    51,     0,
      52,     0,    53,     0,     0,     0,    54,    55,     0,    56,
       0,     0,     0,    57,    58,     0,     0,    59,    60,    61,
      62,     0,    63,    64,     0,     0,    65,     0,     0,     0,
      66,    67,    68,    69,    70,     0,    71,     0,     0,    72,
       0,     0,     0,    73,    74,    75,     0,    76,    77,    78,
      79,    80,     0,    81,     0,    82,    83,    84,     0,     0,
       0,    85,    86,     0,    87,    88,    89,     0,     0,    90,
      91,    92,    26,    93,     0,     0,     0,     0,    94,     0,
      95,    96,   605,     0,     0,    97,    98,    99,    27,     0,
      28,    29,     0,    30,    31,     0,     0,     0,     0,     0,
      32,    33,    34,    35,     0,     0,    36,    37,     0,     0,
       0,     0,     0,     0,     0,    38,    39,    40,     0,    41,
       0,     0,     0,    42,    43,     0,    44,     0,     0,    45,
      46,     0,     0,     0,     0,     0,     0,     0,     0,    47,
       0,    48,    49,    50,     0,     0,    51,     0,    52,     0,
      53,     0,     0,     0,    54,    55,     0,    56,     0,     0,
       0,    57,    58,     0,     0,    59,    60,    61,    62,     0,
      63,    64,     0,     0,    65,     0,     0,     0,    66,    67,
      68,    69,    70,     0,    71,     0,     0,    72,     0,     0,
       0,    73,    74,    75,     0,    76,    77,    78,    79,    80,
       0,    81,     0,    82,    83,    84,     0,     0,     0,    85,
      86,     0,    87,    88,    89,     0,     0,    90,    91,    92,
      26,    93,     0,     0,     0,     0,    94,     0,    95,    96,
       0,     0,     0,    97,    98,    99,    27,     0,    28,    29,
       0,    30,    31,     0,     0,     0,     0,     0,    32,    33,
      34,    35,     0,     0,    36,    37,     0,     0,     0,     0,
       0,     0,     0,    38,    39,    40,     0,    41,     0,     0,
       0,    42,    43,     0,    44,     0,     0,    45,    46,     0,
       0,     0,     0,     0,     0,     0,     0,    47,     0,    48,
      49,    50,     0,     0,    51,     0,    52,     0,    53,     0,
       0,     0,    54,    55,     0,    56,     0,     0,     0,    57,
      58,     0,     0,    59,    60,    61,    62,     0,    63,    64,
       0,     0,    65,     0,     0,     0,    66,    67,    68,    69,
      70,     0,    71,     0,     0,    72,     0,     0,     0,    73,
      74,    75,     0,    76,    77,    78,    79,    80,     0,    81,
       0,    82,    83,    84,     0,     0,     0,    85,    86,     0,
      87,    88,    89,     0,     0,    90,    91,    92,    26,    93,
       0,     0,     0,     0,    94,     0,    95,    96,     0,     0,
       0,    97,    98,    99,    27,     0,    28,    29,     0,    30,
      31,     0,     0,     0,     0,     0,    32,    33,    34,    35,
       0,     0,    36,    37,     0,     0,     0,     0,     0,     0,
       0,    38,    39,    40,     0,    41,     0,     0,     0,    42,
      43,     0,    44,     0,     0,    45,    46,     0,     0,     0,
       0,     0,     0,     0,     0,    47,     0,    48,    49,    50,
       0,     0,    51,     0,    52,     0,    53,     0,     0,     0,
      54,    55,     0,    56,     0,     0,     0,    57,    58,     0,
       0,    59,    60,    61,    62,     0,    63,    64,     0,     0,
      65,     0,     0,     0,    66,    67,    68,    69,    70,     0,
      71,     0,     0,    72,     0,     0,     0,    73,    74,    75,
       0,    76,    77,    78,   115,    80,     0,    81,     0,    82,
      83,    84,     0,     0,     0,    85,    86,     0,    87,    88,
      89,     0,     0,    90,    91,    92,   354,    93,     0,     0,
       0,     0,    94,     0,    95,    96,     0,     0,     0,    97,
      98,    99,     0,     0,     0,    29,     0,   355,   356,   357,
     358,     0,     0,     0,     0,   359,   360,     0,   361,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    40,     0,    41,     0,     0,     0,   362,    43,   363,
     364,     0,     0,    45,     0,     0,   365,     0,     0,     0,
       0,     0,     0,     0,     0,   366,     0,     0,     0,     0,
       0,     0,    52,     0,    53,     0,     0,     0,   367,   368,
       0,   369,     0,     0,     0,     0,     0,     0,     0,     0,
      60,    61,    62,     0,    63,     0,   370,     0,   371,   372,
       0,     0,     0,   373,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    73,     0,     0,     0,    76,
       0,   374,     0,   375,     0,     0,     0,    82,     0,     0,
       0,     0,     0,     0,   376,     0,    87,    88,    89,     0,
       0,     0,   377,   378,     0,     0,     0,     0,     0,     0,
     379,     0,    95,    96,     0,     0,     0,     0,    98
};

static const yytype_int16 yycheck[] =
{
       2,   156,     0,     1,     6,     7,   218,   134,   395,   165,
     397,     2,   126,   127,   128,     6,    20,   517,   502,    14,
     448,    14,    14,    17,    14,    14,    14,    14,   231,    14,
      13,    17,    36,    13,     2,    39,    13,    13,     6,     7,
     165,   253,    36,    36,    36,    39,    39,    39,    36,    36,
      36,    39,    39,    39,    13,    21,    13,   269,   167,   214,
       8,    14,    36,    13,    13,    39,    40,   448,    13,    13,
      36,   134,    13,    39,   238,    13,    36,    73,    13,    39,
      36,   237,   115,    39,     5,    36,    19,    16,    39,    73,
     245,    29,    13,     6,    36,    61,     9,    39,     4,     5,
       6,     7,     8,   267,    10,    11,   139,   109,   110,   537,
      93,    67,   237,   115,    58,   318,   544,   119,   109,   110,
      36,    59,    89,    39,   122,    29,   282,   123,   119,   341,
     132,   172,   173,    62,   290,   116,   134,   139,   140,   123,
       0,   109,   110,    93,    93,    51,    84,   115,    93,   449,
     634,   119,    58,   637,   454,     6,   537,   282,     9,   222,
     108,   137,   271,   116,   132,   290,   147,    36,    69,   102,
      39,   139,   140,   177,   151,   158,   171,    14,   158,   679,
     664,   171,   171,   177,   177,   177,   171,   163,   343,   177,
     177,   177,   317,   137,   622,   155,   137,    75,    67,   137,
      66,   413,   137,   177,   163,   164,   163,   164,   158,   158,
     212,   177,   156,   158,   216,   156,   154,   177,   216,   163,
      23,   177,   163,   707,   222,   163,   177,    23,   230,   171,
     117,   169,    69,    36,   168,   177,    39,    40,    41,    42,
      43,    44,    45,   756,   212,    51,   152,   245,   216,   749,
      87,   769,   153,    14,    87,   171,   774,   108,    19,   772,
     301,   177,   230,    14,   420,    34,   268,    55,   125,    38,
     527,   528,    78,   275,   174,   175,   133,     2,   796,    82,
      13,     6,     7,    55,   275,   167,    82,    90,    33,    15,
      49,   678,    95,    81,    39,   420,    13,   139,    15,    95,
     268,   107,    13,   305,   306,   108,   308,   275,   177,    81,
      40,    99,    42,   101,    28,   317,   153,    76,   316,   321,
      13,   171,    15,   448,    83,   100,   317,    99,   330,   101,
     162,    13,    91,    15,    20,    94,    49,   305,   306,    23,
     308,   102,   148,   112,    13,    13,    15,   604,   136,   317,
     163,   164,    36,   321,    13,    39,    40,    41,    42,    43,
      44,    45,   330,    76,   136,    32,    33,   369,   171,    13,
      83,    13,   131,   176,   177,   146,   147,   165,    91,   163,
     164,    94,   545,   546,   109,   110,    13,     2,    13,    13,
     115,     6,     7,   165,   119,    13,    49,   172,    82,    13,
      20,    14,    13,   178,   179,   180,    90,   132,   171,   116,
      17,    95,   537,   540,   139,   140,    90,    49,   131,   544,
     173,    13,   547,    76,   108,    13,    13,   156,    75,    15,
      83,   171,   171,    14,   171,   171,    14,    14,    91,   161,
       5,    94,   444,   445,    76,    34,   448,   449,    14,    38,
      14,    83,   454,    13,   177,    40,    40,   448,   171,    91,
      14,   114,    94,   147,     4,     5,     6,     7,     8,   624,
      10,    11,   474,    15,   476,    14,   444,   445,   131,   170,
     448,   449,   114,    23,    14,   476,   454,   212,    77,    14,
     615,   216,   176,   177,   109,   110,    64,   622,    14,   131,
     115,    14,   504,    92,   119,   230,   474,    96,   476,   623,
      93,    51,    13,   504,    13,   517,    13,   132,    58,    13,
     124,    13,    13,   112,   139,   140,    13,    13,   530,    13,
      39,   533,    15,   688,   536,   537,   504,    14,   750,   537,
       8,    14,   544,   268,   120,   547,   537,    78,    15,   517,
     275,   170,   111,   544,    65,     5,   547,     5,   111,    13,
       5,   111,   530,     5,    13,   533,     5,     5,   536,   537,
       5,   726,   574,    13,     5,   111,   544,   151,     5,   547,
     305,   306,     5,   308,    13,     5,   149,   149,   149,     5,
     139,   149,   317,   595,    13,    91,   321,   212,   119,    14,
      14,   216,    91,   605,   595,   330,    28,    14,    80,    13,
       5,    14,   152,   615,    14,   230,    13,     5,    14,    13,
     622,    14,    23,    14,   615,     5,     5,   595,     5,    13,
       5,   622,   111,    14,   166,    36,     5,   605,    39,    40,
      41,    42,    43,    44,    45,    13,    13,   615,    14,    14,
      14,   166,    14,    14,   622,     5,   166,    15,   166,     5,
     275,    14,    14,     5,     5,    13,     5,    14,     5,    14,
      14,    14,     5,    13,   135,   121,    14,   679,    14,    14,
      14,    82,    14,    14,   686,    14,     5,     5,   151,    90,
     305,   306,    14,   308,    95,    15,   149,    14,    14,    14,
      28,   171,   317,    14,   140,   149,   321,   108,   149,    14,
     712,   679,   149,    14,   166,   330,    13,   123,   686,   444,
     445,   712,   132,   448,   449,   123,    14,    17,   118,   454,
     132,   327,   166,   166,   166,   122,   119,   222,    24,   148,
      70,   334,   547,   722,   712,   615,   319,   749,   330,   474,
     752,   476,   270,   294,   289,   300,   420,   406,   605,   769,
     131,   796,   474,   646,   321,   136,   137,   224,    -1,    -1,
     345,    -1,    -1,    -1,   145,   176,   177,    -1,    -1,   504,
      -1,   749,    -1,    -1,   752,   156,    -1,   158,    -1,    -1,
      -1,    -1,   517,    -1,    -1,    -1,   167,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   530,    -1,    -1,   533,    -1,
      -1,   536,   537,    -1,    -1,    -1,    -1,    -1,    -1,   544,
      -1,    -1,   547,    -1,    -1,    -1,    -1,    -1,    -1,   444,
     445,    -1,    -1,   448,   449,    -1,    -1,    -1,    -1,   454,
      -1,    -1,    -1,   214,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   476,    -1,    -1,    -1,    -1,    -1,   238,    -1,    -1,
     595,    -1,    -1,    -1,   245,    -1,    -1,    -1,    -1,    -1,
     605,   252,    -1,   254,    -1,    -1,    -1,    -1,    -1,   504,
     615,   262,    -1,    -1,    -1,    -1,   267,   622,    -1,   270,
     271,    -1,   273,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   284,    -1,   530,    -1,   288,   533,    -1,
      -1,   536,   537,    -1,    -1,    -1,    -1,    -1,    -1,   544,
      -1,   302,   547,   304,     3,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   319,    -1,
      -1,    -1,    -1,    22,   679,    -1,   327,   328,    -1,    -1,
      -1,   686,    -1,   334,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   343,    -1,    -1,    -1,    -1,    -1,    -1,    48,
     595,    50,    -1,    -1,    -1,    -1,    55,   712,    -1,    -1,
     605,    60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     615,    -1,    -1,    -1,    -1,    -1,    -1,   622,    -1,    -1,
      79,    -1,    81,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   749,    -1,    -1,   752,    97,    98,
      99,    -1,   101,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   419,    -1,
     421,    -1,    -1,   122,    -1,    -1,    -1,   126,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   134,    -1,   438,    -1,    -1,
      -1,   686,    -1,    -1,   143,   144,   145,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     159,   160,    -1,    -1,    -1,    -1,   165,   712,    -1,    -1,
     471,    -1,    -1,    -1,    -1,    -1,    -1,   478,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   752,    -1,    -1,
      -1,    -1,     3,     4,    -1,    -1,    -1,    -1,    -1,   520,
      -1,    -1,   523,    -1,    -1,    -1,    -1,    -1,    19,    20,
      21,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    -1,
      31,    32,    33,    34,    -1,    36,    37,    38,    39,    -1,
      -1,    -1,   553,   554,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    54,    55,    -1,    57,    -1,    -1,    60,
      61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    72,    73,    74,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    86,    -1,    88,    -1,   600,
     601,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,    -1,    -1,   105,    -1,    -1,    -1,   109,   110,
     111,   112,   113,   624,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
     141,    -1,   143,   144,   145,    -1,    -1,   148,   149,   150,
      -1,   152,    -1,    -1,    -1,    -1,   157,    -1,   159,   160,
      -1,    -1,    -1,   164,   165,   166,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,   688,    -1,   690,
      -1,    -1,     3,     4,     5,     6,     7,     8,    -1,    10,
      11,    -1,    13,    -1,    -1,    -1,    -1,    -1,    19,    -1,
      21,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    30,
      31,    32,    33,    34,    -1,   726,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    52,    53,    54,    55,    -1,    57,    58,    -1,    60,
      61,    -1,    -1,    -1,    -1,   756,    -1,    -1,    -1,    70,
      71,    72,    73,    74,    -1,    -1,    77,    -1,    79,    -1,
      81,   772,    -1,    -1,    85,    86,    -1,    88,    -1,    -1,
      -1,    92,    93,    94,    -1,    96,    97,    98,    99,    -1,
     101,   102,    -1,    -1,   105,    -1,    -1,   108,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
     131,   132,    -1,   134,   135,   136,   137,   138,    -1,   140,
     141,    -1,   143,   144,   145,   146,    -1,   148,   149,   150,
      -1,   152,    -1,    -1,    -1,    -1,   157,    -1,   159,   160,
      -1,    -1,   163,   164,   165,   166,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   174,   175,     3,     4,     5,     6,     7,
       8,    -1,    10,    11,    -1,    13,    14,    -1,    -1,    -1,
      -1,    19,    -1,    21,    22,    -1,    24,    25,    -1,    -1,
      -1,    -1,    30,    31,    32,    33,    34,    -1,    -1,    37,
      38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,
      48,    -1,    50,    -1,    52,    53,    54,    55,    -1,    57,
      58,    -1,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    70,    71,    72,    73,    74,    -1,    -1,    77,
      -1,    79,    -1,    81,    -1,    -1,    -1,    85,    86,    -1,
      88,    -1,    -1,    -1,    92,    93,    94,    -1,    96,    97,
      98,    99,    -1,   101,   102,    -1,    -1,   105,    -1,    -1,
     108,   109,   110,   111,   112,   113,    -1,   115,    -1,    -1,
     118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,
     128,   129,   130,   131,   132,    -1,   134,   135,   136,    -1,
     138,    -1,   140,   141,    -1,   143,   144,   145,   146,    -1,
     148,   149,   150,    -1,   152,    -1,    -1,    -1,    -1,   157,
      -1,   159,   160,    -1,    -1,    -1,   164,   165,   166,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   174,   175,     3,     4,
       5,     6,     7,     8,    -1,    10,    11,    -1,    13,    14,
      -1,    -1,    -1,    -1,    19,    -1,    21,    22,    -1,    24,
      25,    -1,    -1,    -1,    -1,    30,    31,    32,    33,    34,
      -1,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    52,    53,    54,
      55,    -1,    57,    58,    -1,    60,    61,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    71,    72,    73,    74,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    86,    -1,    88,    -1,    -1,    -1,    92,    93,    94,
      -1,    96,    97,    98,    99,    -1,   101,   102,    -1,    -1,
     105,    -1,    -1,   108,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,   131,   132,    -1,   134,
     135,   136,    -1,   138,    -1,   140,   141,    -1,   143,   144,
     145,   146,    -1,   148,   149,   150,    -1,   152,    -1,    -1,
      -1,    -1,   157,    -1,   159,   160,    -1,    -1,    -1,   164,
     165,   166,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,
     175,     3,     4,     5,     6,     7,     8,    -1,    10,    11,
      -1,    13,    -1,    -1,    16,    -1,    18,    19,    -1,    21,
      22,    -1,    24,    25,    -1,    -1,    -1,    -1,    30,    31,
      32,    33,    34,    -1,    -1,    37,    38,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,
      52,    53,    54,    55,    -1,    57,    58,    -1,    60,    61,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,
      72,    73,    74,    -1,    -1,    77,    -1,    79,    -1,    81,
      -1,    -1,    -1,    85,    86,    -1,    88,    -1,    -1,    -1,
      92,    93,    94,    -1,    96,    97,    98,    99,    -1,   101,
     102,    -1,    -1,   105,    -1,    -1,    -1,   109,   110,   111,
     112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,    -1,
     122,   123,   124,    -1,   126,   127,   128,   129,   130,   131,
     132,    -1,   134,   135,   136,    -1,   138,    -1,   140,   141,
     142,   143,   144,   145,   146,    -1,   148,   149,   150,    -1,
     152,    -1,    -1,    -1,    -1,   157,    -1,   159,   160,    -1,
      -1,    -1,   164,   165,   166,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   174,   175,     3,     4,     5,     6,     7,     8,
      -1,    10,    11,    -1,    13,    -1,    -1,    16,    -1,    -1,
      19,    -1,    21,    22,    -1,    24,    25,    -1,    -1,    -1,
      -1,    30,    31,    32,    33,    34,    -1,    -1,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,
      -1,    50,    -1,    52,    53,    54,    55,    -1,    57,    58,
      -1,    60,    61,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    70,    -1,    72,    73,    74,    -1,    -1,    77,    -1,
      79,    -1,    81,    -1,    -1,    -1,    85,    86,    -1,    88,
      -1,    -1,    -1,    92,    93,    94,    -1,    96,    97,    98,
      99,    -1,   101,   102,    -1,    -1,   105,    -1,    -1,    -1,
     109,   110,   111,   112,   113,    -1,   115,    -1,    -1,   118,
      -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,   128,
     129,   130,   131,   132,    -1,   134,   135,   136,    -1,   138,
      -1,   140,   141,    -1,   143,   144,   145,   146,    -1,   148,
     149,   150,    -1,   152,    -1,    -1,    -1,    -1,   157,    -1,
     159,   160,    -1,    -1,    -1,   164,   165,   166,    -1,    -1,
      -1,    -1,    -1,   172,    -1,   174,   175,     3,     4,     5,
       6,     7,     8,    -1,    10,    11,    -1,    13,    14,    -1,
      -1,    -1,    -1,    19,    -1,    21,    22,    -1,    24,    25,
      -1,    -1,    -1,    -1,    30,    31,    32,    33,    34,    -1,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      46,    47,    48,    -1,    50,    -1,    52,    53,    54,    55,
      -1,    57,    58,    -1,    60,    61,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    70,    -1,    72,    73,    74,    -1,
      -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,
      86,    -1,    88,    -1,    -1,    -1,    92,    93,    94,    -1,
      96,    97,    98,    99,    -1,   101,   102,    -1,    -1,   105,
      -1,    -1,    -1,   109,   110,   111,   112,   113,    -1,   115,
      -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,
     126,   127,   128,   129,   130,   131,   132,    -1,   134,   135,
     136,   137,   138,    -1,   140,   141,    -1,   143,   144,   145,
     146,    -1,   148,   149,   150,    -1,   152,    -1,    -1,    -1,
      -1,   157,    -1,   159,   160,    -1,    -1,   163,   164,   165,
     166,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,   175,
       3,     4,     5,     6,     7,     8,    -1,    10,    11,    -1,
      13,    -1,    -1,    -1,    -1,    -1,    19,    -1,    21,    22,
      -1,    24,    25,    -1,    -1,    -1,    -1,    30,    31,    32,
      33,    34,    -1,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,    52,
      53,    54,    55,    -1,    57,    58,    -1,    60,    61,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,    71,    72,
      73,    74,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,
      -1,    -1,    85,    86,    -1,    88,    -1,    -1,    -1,    92,
      93,    94,    -1,    96,    97,    98,    99,    -1,   101,   102,
      -1,    -1,   105,    -1,    -1,   108,   109,   110,   111,   112,
     113,    -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,
     123,   124,    -1,   126,   127,   128,   129,   130,   131,   132,
      -1,   134,   135,   136,    -1,   138,    -1,   140,   141,    -1,
     143,   144,   145,   146,    -1,   148,   149,   150,    -1,   152,
      -1,    -1,    -1,    -1,   157,    -1,   159,   160,    -1,    -1,
      -1,   164,   165,   166,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   174,   175,     3,     4,     5,     6,     7,     8,    -1,
      10,    11,    -1,    13,    -1,    -1,    -1,    -1,    -1,    19,
      -1,    21,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,
      30,    31,    32,    33,    34,    -1,    -1,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,
      50,    -1,    52,    53,    54,    55,    -1,    57,    58,    -1,
      60,    61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      70,    -1,    72,    73,    74,    -1,    -1,    77,    -1,    79,
      -1,    81,    -1,    -1,    -1,    85,    86,    -1,    88,    -1,
      -1,    -1,    92,    93,    94,    -1,    96,    97,    98,    99,
      -1,   101,   102,    -1,    -1,   105,    -1,    -1,    -1,   109,
     110,   111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,
      -1,    -1,   122,   123,   124,    -1,   126,   127,   128,   129,
     130,   131,   132,    -1,   134,   135,   136,   137,   138,    -1,
     140,   141,    -1,   143,   144,   145,   146,    -1,   148,   149,
     150,    -1,   152,    -1,    -1,    -1,    -1,   157,    -1,   159,
     160,    -1,    -1,   163,   164,   165,   166,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   174,   175,     3,     4,     5,     6,
       7,     8,    -1,    10,    11,    -1,    13,    -1,    -1,    -1,
      -1,    -1,    19,    -1,    21,    22,    -1,    24,    25,    -1,
      -1,    -1,    -1,    30,    31,    32,    33,    34,    -1,    -1,
      37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,
      47,    48,    -1,    50,    -1,    52,    53,    54,    55,    -1,
      57,    58,    -1,    60,    61,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    70,    -1,    72,    73,    74,    -1,    -1,
      77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,    86,
      -1,    88,    -1,    -1,    -1,    92,    93,    94,    -1,    96,
      97,    98,    99,    -1,   101,   102,    -1,    -1,   105,    -1,
      -1,    -1,   109,   110,   111,   112,   113,    -1,   115,    -1,
      -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,
     127,   128,   129,   130,   131,   132,    -1,   134,   135,   136,
      -1,   138,    -1,   140,   141,    -1,   143,   144,   145,   146,
      -1,   148,   149,   150,    -1,   152,    -1,    -1,    -1,    -1,
     157,    -1,   159,   160,    -1,    -1,    -1,   164,   165,   166,
      -1,    -1,    -1,    -1,    -1,   172,    -1,   174,   175,     3,
       4,     5,     6,     7,     8,    -1,    10,    11,    -1,    13,
      14,    -1,    -1,    -1,    -1,    19,    -1,    21,    22,    -1,
      24,    25,    -1,    -1,    -1,    -1,    30,    31,    32,    33,
      34,    -1,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    46,    47,    48,    -1,    50,    -1,    52,    53,
      54,    55,    -1,    57,    58,    -1,    60,    61,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    72,    73,
      74,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,
      -1,    85,    86,    -1,    88,    -1,    -1,    -1,    92,    93,
      94,    -1,    96,    97,    98,    99,    -1,   101,   102,    -1,
      -1,   105,    -1,    -1,    -1,   109,   110,   111,   112,   113,
      -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,
     124,    -1,   126,   127,   128,   129,   130,   131,   132,    -1,
     134,   135,   136,    -1,   138,    -1,   140,   141,    -1,   143,
     144,   145,   146,    -1,   148,   149,   150,    -1,   152,    -1,
      -1,    -1,    -1,   157,    -1,   159,   160,    -1,    -1,    -1,
     164,   165,   166,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     174,   175,     3,     4,     5,     6,     7,     8,    -1,    10,
      11,    -1,    13,    -1,    -1,    -1,    -1,    -1,    19,    -1,
      21,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    30,
      31,    32,    33,    34,    -1,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    52,    53,    54,    55,    -1,    57,    58,    -1,    60,
      61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    72,    73,    74,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    86,    -1,    88,    -1,    -1,
      -1,    92,    93,    94,    -1,    96,    97,    98,    99,    -1,
     101,   102,    -1,    -1,   105,    -1,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
     131,   132,    -1,   134,   135,   136,    -1,   138,    -1,   140,
     141,    -1,   143,   144,   145,   146,    -1,   148,   149,   150,
      -1,   152,    -1,    -1,    -1,    -1,   157,    -1,   159,   160,
      -1,    -1,    -1,   164,   165,   166,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   174,   175,     3,     4,     5,     6,     7,
       8,    -1,    10,    11,    -1,    13,    -1,    -1,    -1,    -1,
      -1,    19,    -1,    21,    22,    -1,    24,    25,    -1,    -1,
      -1,    -1,    30,    31,    32,    33,    34,    -1,    -1,    37,
      38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,
      48,    -1,    50,    -1,    52,    53,    54,    55,    -1,    57,
      58,    -1,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    70,    -1,    72,    73,    74,    -1,    -1,    77,
      -1,    79,    -1,    81,    -1,    -1,    -1,    85,    86,    -1,
      88,    -1,    -1,    -1,    92,    93,    94,    -1,    96,    97,
      98,    99,    -1,   101,   102,    -1,    -1,   105,    -1,    -1,
      -1,   109,   110,   111,   112,   113,    -1,   115,    -1,    -1,
     118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,
     128,   129,   130,   131,   132,    -1,   134,   135,   136,    -1,
     138,    -1,   140,   141,    -1,   143,   144,   145,   146,    -1,
     148,   149,   150,     3,   152,    -1,    -1,    -1,    -1,   157,
      -1,   159,   160,    -1,    -1,    15,   164,   165,   166,    19,
      -1,    21,    22,    -1,    24,    25,   174,   175,    -1,    -1,
      -1,    31,    32,    33,    34,    -1,    -1,    37,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,
      50,    -1,    -1,    -1,    54,    55,    -1,    57,    -1,    -1,
      60,    61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      70,    -1,    72,    73,    74,    -1,    -1,    77,    -1,    79,
      -1,    81,    -1,    -1,    -1,    85,    86,    -1,    88,    -1,
      -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,    99,
      -1,   101,   102,    -1,    -1,   105,    -1,    -1,    -1,   109,
     110,   111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,
      -1,    -1,   122,   123,   124,    -1,   126,   127,   128,   129,
     130,    -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,
     140,   141,    -1,   143,   144,   145,    -1,    -1,   148,   149,
     150,    -1,   152,    -1,    -1,     3,    -1,   157,    -1,   159,
     160,    -1,    -1,    -1,   164,   165,   166,    15,    -1,    -1,
      -1,    19,   172,    21,    22,    -1,    24,    25,    -1,    -1,
      -1,    -1,    -1,    31,    32,    33,    34,    -1,    -1,    37,
      38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,
      48,    -1,    50,    -1,    -1,    -1,    54,    55,    -1,    57,
      -1,    -1,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    70,    -1,    72,    73,    74,    -1,    -1,    77,
      -1,    79,    -1,    81,    -1,    -1,    -1,    85,    86,    -1,
      88,    -1,    -1,    -1,    92,    93,    -1,    -1,    96,    97,
      98,    99,    -1,   101,   102,    -1,    -1,   105,    -1,    -1,
      -1,   109,   110,   111,   112,   113,    -1,   115,    -1,    -1,
     118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,
     128,   129,   130,    -1,   132,    -1,   134,   135,   136,    -1,
      -1,    -1,   140,   141,    -1,   143,   144,   145,    -1,    -1,
     148,   149,   150,    -1,   152,    -1,    -1,     3,    -1,   157,
      -1,   159,   160,    -1,    -1,    -1,   164,   165,   166,    -1,
      -1,    -1,    -1,    19,   172,    21,    22,    -1,    24,    25,
      -1,    -1,    -1,    -1,    -1,    31,    32,    33,    34,    -1,
      -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      46,    47,    48,    -1,    50,    -1,    -1,    -1,    54,    55,
      -1,    57,    -1,    -1,    60,    61,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    70,    -1,    72,    73,    74,    -1,
      -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,
      86,    -1,    88,    -1,    -1,    -1,    92,    93,    -1,    -1,
      96,    97,    98,    99,    -1,   101,   102,    -1,    -1,   105,
      -1,    -1,    -1,   109,   110,   111,   112,   113,    -1,   115,
      -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,
     126,   127,   128,   129,   130,    -1,   132,    -1,   134,   135,
     136,    -1,    -1,    -1,   140,   141,    -1,   143,   144,   145,
      -1,    -1,   148,   149,   150,    -1,   152,    -1,    -1,     3,
      -1,   157,    -1,   159,   160,    -1,    -1,    -1,   164,   165,
     166,    -1,    -1,    -1,    -1,    19,   172,    21,    22,    -1,
      24,    25,    -1,    -1,    -1,    -1,    -1,    31,    32,    33,
      34,    -1,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    46,    47,    48,    -1,    50,    -1,    -1,    -1,
      54,    55,    -1,    57,    -1,    -1,    60,    61,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    72,    73,
      74,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,
      -1,    85,    86,    -1,    88,    -1,    -1,    -1,    92,    93,
      -1,    -1,    96,    97,    98,    99,    -1,   101,   102,    -1,
      -1,   105,    -1,    -1,    -1,   109,   110,   111,   112,   113,
      -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,
     124,    -1,   126,   127,   128,   129,   130,    -1,   132,    -1,
     134,   135,   136,    -1,    -1,    -1,   140,   141,    -1,   143,
     144,   145,    -1,    -1,   148,   149,   150,    -1,   152,    -1,
      -1,    -1,    -1,   157,     3,   159,   160,    -1,    -1,    -1,
     164,   165,   166,    -1,    13,    -1,    -1,    -1,   172,    -1,
      19,    -1,    21,    22,    -1,    24,    25,    -1,    -1,    -1,
      -1,    -1,    31,    32,    33,    34,    -1,    -1,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,
      -1,    50,    -1,    -1,    -1,    54,    55,    -1,    57,    -1,
      -1,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    70,    -1,    72,    73,    74,    -1,    -1,    77,    -1,
      79,    -1,    81,    -1,    -1,    -1,    85,    86,    -1,    88,
      -1,    -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,
      99,    -1,   101,   102,    -1,    -1,   105,    -1,    -1,    -1,
     109,   110,   111,   112,   113,    -1,   115,    -1,    -1,   118,
      -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,   128,
     129,   130,    -1,   132,    -1,   134,   135,   136,   137,    -1,
      -1,   140,   141,    -1,   143,   144,   145,    -1,    -1,   148,
     149,   150,     3,   152,    -1,    -1,    -1,    -1,   157,    -1,
     159,   160,    13,    -1,   163,   164,   165,   166,    19,    -1,
      21,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    -1,
      31,    32,    33,    34,    -1,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    54,    55,    -1,    57,    -1,    -1,    60,
      61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    72,    73,    74,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    86,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,    -1,    -1,   105,    -1,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,   137,    -1,    -1,   140,
     141,    -1,   143,   144,   145,    -1,    -1,   148,   149,   150,
      -1,   152,     3,     4,    -1,    -1,   157,    -1,   159,   160,
      -1,    -1,   163,   164,   165,   166,    -1,    -1,    19,    20,
      21,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    -1,
      31,    32,    33,    34,    -1,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    54,    55,    -1,    57,    -1,    -1,    60,
      61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    72,    73,    74,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    86,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,    -1,    -1,   105,    -1,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
     141,    -1,   143,   144,   145,    -1,    -1,   148,   149,   150,
       3,   152,    -1,    -1,    -1,    -1,   157,    -1,   159,   160,
      -1,    -1,    15,   164,   165,   166,    19,    -1,    21,    22,
      -1,    24,    25,    -1,    -1,    -1,    -1,    -1,    31,    32,
      33,    34,    -1,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,    -1,
      -1,    54,    55,    -1,    57,    -1,    -1,    60,    61,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    72,
      73,    74,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,
      -1,    -1,    85,    86,    -1,    88,    -1,    -1,    -1,    92,
      93,    -1,    -1,    96,    97,    98,    99,    -1,   101,   102,
      -1,    -1,   105,    -1,    -1,    -1,   109,   110,   111,   112,
     113,    -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,
     123,   124,    -1,   126,   127,   128,   129,   130,    -1,   132,
      -1,   134,   135,   136,    -1,    -1,    -1,   140,   141,    -1,
     143,   144,   145,    -1,    -1,   148,   149,   150,     3,   152,
      -1,    -1,    -1,    -1,   157,    -1,   159,   160,    -1,    -1,
      15,   164,   165,   166,    19,    -1,    21,    22,    -1,    24,
      25,    -1,    -1,    -1,    -1,    -1,    31,    32,    33,    34,
      -1,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    -1,    -1,    54,
      55,    -1,    57,    -1,    -1,    60,    61,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    -1,    72,    73,    74,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    86,    -1,    88,    -1,    -1,    -1,    92,    93,    -1,
      -1,    96,    97,    98,    99,    -1,   101,   102,    -1,    -1,
     105,    -1,    -1,    -1,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,    -1,   132,    -1,   134,
     135,   136,    -1,    -1,    -1,   140,   141,    -1,   143,   144,
     145,    -1,    -1,   148,   149,   150,     3,   152,    -1,    -1,
      -1,    -1,   157,    -1,   159,   160,    -1,    -1,    15,   164,
     165,   166,    19,    -1,    21,    22,    -1,    24,    25,    -1,
      -1,    -1,    -1,    -1,    31,    32,    33,    34,    -1,    -1,
      37,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,
      47,    48,    -1,    50,    -1,    -1,    -1,    54,    55,    -1,
      57,    -1,    -1,    60,    61,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    70,    -1,    72,    73,    74,    -1,    -1,
      77,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,    86,
      -1,    88,    -1,    -1,    -1,    92,    93,    -1,    -1,    96,
      97,    98,    99,    -1,   101,   102,    -1,    -1,   105,    -1,
      -1,    -1,   109,   110,   111,   112,   113,    -1,   115,    -1,
      -1,   118,    -1,    -1,    -1,   122,   123,   124,    -1,   126,
     127,   128,   129,   130,    -1,   132,    -1,   134,   135,   136,
      -1,    -1,    -1,   140,   141,    -1,   143,   144,   145,    -1,
      -1,   148,   149,   150,     3,   152,    -1,    -1,    -1,    -1,
     157,    -1,   159,   160,    13,    -1,    -1,   164,   165,   166,
      19,    -1,    21,    22,    -1,    24,    25,    -1,    -1,    -1,
      -1,    -1,    31,    32,    33,    34,    -1,    -1,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,
      -1,    50,    -1,    -1,    -1,    54,    55,    -1,    57,    -1,
      -1,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    70,    -1,    72,    73,    74,    -1,    -1,    77,    -1,
      79,    -1,    81,    -1,    -1,    -1,    85,    86,    -1,    88,
      -1,    -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,
      99,    -1,   101,   102,    -1,    -1,   105,    -1,    -1,    -1,
     109,   110,   111,   112,   113,    -1,   115,    -1,    -1,   118,
      -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,   128,
     129,   130,    -1,   132,    -1,   134,   135,   136,    -1,    -1,
      -1,   140,   141,    -1,   143,   144,   145,    -1,    -1,   148,
     149,   150,    -1,   152,     3,     4,    -1,    -1,   157,    -1,
     159,   160,    -1,    -1,    -1,   164,   165,   166,    -1,    -1,
      19,    -1,    21,    22,    -1,    24,    25,    -1,    -1,    -1,
      -1,    -1,    31,    32,    33,    34,    -1,    -1,    37,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,
      -1,    50,    -1,    -1,    -1,    54,    55,    -1,    57,    -1,
      -1,    60,    61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    70,    -1,    72,    73,    74,    -1,    -1,    77,    -1,
      79,    -1,    81,    -1,    -1,    -1,    85,    86,    -1,    88,
      -1,    -1,    -1,    92,    93,    -1,    -1,    96,    97,    98,
      99,    -1,   101,   102,    -1,    -1,   105,    -1,    -1,    -1,
     109,   110,   111,   112,   113,    -1,   115,    -1,    -1,   118,
      -1,    -1,    -1,   122,   123,   124,    -1,   126,   127,   128,
     129,   130,    -1,   132,    -1,   134,   135,   136,    -1,    -1,
      -1,   140,   141,    -1,   143,   144,   145,    -1,    -1,   148,
     149,   150,     3,   152,    -1,    -1,    -1,    -1,   157,    -1,
     159,   160,    13,    -1,    -1,   164,   165,   166,    19,    -1,
      21,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    -1,
      31,    32,    33,    34,    -1,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    46,    47,    48,    -1,    50,
      -1,    -1,    -1,    54,    55,    -1,    57,    -1,    -1,    60,
      61,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,
      -1,    72,    73,    74,    -1,    -1,    77,    -1,    79,    -1,
      81,    -1,    -1,    -1,    85,    86,    -1,    88,    -1,    -1,
      -1,    92,    93,    -1,    -1,    96,    97,    98,    99,    -1,
     101,   102,    -1,    -1,   105,    -1,    -1,    -1,   109,   110,
     111,   112,   113,    -1,   115,    -1,    -1,   118,    -1,    -1,
      -1,   122,   123,   124,    -1,   126,   127,   128,   129,   130,
      -1,   132,    -1,   134,   135,   136,    -1,    -1,    -1,   140,
     141,    -1,   143,   144,   145,    -1,    -1,   148,   149,   150,
       3,   152,    -1,    -1,    -1,    -1,   157,    -1,   159,   160,
      -1,    -1,    -1,   164,   165,   166,    19,    -1,    21,    22,
      -1,    24,    25,    -1,    -1,    -1,    -1,    -1,    31,    32,
      33,    34,    -1,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    46,    47,    48,    -1,    50,    -1,    -1,
      -1,    54,    55,    -1,    57,    -1,    -1,    60,    61,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    70,    -1,    72,
      73,    74,    -1,    -1,    77,    -1,    79,    -1,    81,    -1,
      -1,    -1,    85,    86,    -1,    88,    -1,    -1,    -1,    92,
      93,    -1,    -1,    96,    97,    98,    99,    -1,   101,   102,
      -1,    -1,   105,    -1,    -1,    -1,   109,   110,   111,   112,
     113,    -1,   115,    -1,    -1,   118,    -1,    -1,    -1,   122,
     123,   124,    -1,   126,   127,   128,   129,   130,    -1,   132,
      -1,   134,   135,   136,    -1,    -1,    -1,   140,   141,    -1,
     143,   144,   145,    -1,    -1,   148,   149,   150,     3,   152,
      -1,    -1,    -1,    -1,   157,    -1,   159,   160,    -1,    -1,
      -1,   164,   165,   166,    19,    -1,    21,    22,    -1,    24,
      25,    -1,    -1,    -1,    -1,    -1,    31,    32,    33,    34,
      -1,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    47,    48,    -1,    50,    -1,    -1,    -1,    54,
      55,    -1,    57,    -1,    -1,    60,    61,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    70,    -1,    72,    73,    74,
      -1,    -1,    77,    -1,    79,    -1,    81,    -1,    -1,    -1,
      85,    86,    -1,    88,    -1,    -1,    -1,    92,    93,    -1,
      -1,    96,    97,    98,    99,    -1,   101,   102,    -1,    -1,
     105,    -1,    -1,    -1,   109,   110,   111,   112,   113,    -1,
     115,    -1,    -1,   118,    -1,    -1,    -1,   122,   123,   124,
      -1,   126,   127,   128,   129,   130,    -1,   132,    -1,   134,
     135,   136,    -1,    -1,    -1,   140,   141,    -1,   143,   144,
     145,    -1,    -1,   148,   149,   150,     3,   152,    -1,    -1,
      -1,    -1,   157,    -1,   159,   160,    -1,    -1,    -1,   164,
     165,   166,    -1,    -1,    -1,    22,    -1,    24,    25,    26,
      27,    -1,    -1,    -1,    -1,    32,    33,    -1,    35,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    48,    -1,    50,    -1,    -1,    -1,    54,    55,    56,
      57,    -1,    -1,    60,    -1,    -1,    63,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    72,    -1,    -1,    -1,    -1,
      -1,    -1,    79,    -1,    81,    -1,    -1,    -1,    85,    86,
      -1,    88,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      97,    98,    99,    -1,   101,    -1,   103,    -1,   105,   106,
      -1,    -1,    -1,   110,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   122,    -1,    -1,    -1,   126,
      -1,   128,    -1,   130,    -1,    -1,    -1,   134,    -1,    -1,
      -1,    -1,    -1,    -1,   141,    -1,   143,   144,   145,    -1,
      -1,    -1,   149,   150,    -1,    -1,    -1,    -1,    -1,    -1,
     157,    -1,   159,   160,    -1,    -1,    -1,    -1,   165
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,    13,    29,    59,    84,   137,   154,   163,   169,   182,
     183,   184,   185,   190,   191,   192,   195,   196,   199,   200,
     201,   202,   203,   208,   220,   200,     3,    19,    21,    22,
      24,    25,    31,    32,    33,    34,    37,    38,    46,    47,
      48,    50,    54,    55,    57,    60,    61,    70,    72,    73,
      74,    77,    79,    81,    85,    86,    88,    92,    93,    96,
      97,    98,    99,   101,   102,   105,   109,   110,   111,   112,
     113,   115,   118,   122,   123,   124,   126,   127,   128,   129,
     130,   132,   134,   135,   136,   140,   141,   143,   144,   145,
     148,   149,   150,   152,   157,   159,   160,   164,   165,   166,
     189,   235,   276,   278,   307,   325,   327,   328,   330,    75,
      89,    16,    62,   226,   235,   129,   221,   222,   325,    29,
       0,    66,   168,   117,   214,   215,    69,   153,    87,   201,
      14,    13,    15,   235,   235,   167,   331,   139,   221,   171,
      13,   223,   224,   189,   183,    28,   226,   226,   226,     4,
       5,     6,     7,     8,    10,    11,    13,    14,    30,    31,
      37,    46,    52,    53,    58,    71,    94,   108,   109,   131,
     138,   146,   174,   175,   186,   187,   188,   204,   205,   207,
     234,   235,   241,   242,   243,   244,   245,   246,   247,   248,
     249,   250,   251,   252,   253,   254,   255,   256,   257,   258,
     259,   268,   269,   274,   275,   276,   277,   278,   294,   295,
     307,   325,    15,   325,   162,   209,    13,    58,   156,   193,
     194,   199,   224,     5,    13,   332,    13,   172,   227,   228,
     254,   197,   198,   254,   221,   225,   325,    20,    13,   216,
     217,   254,   202,   202,   203,    13,   200,   241,   254,   260,
     254,   270,    13,    13,    13,    13,    13,   205,   206,    13,
     243,   254,    13,    13,   258,   258,    14,   171,    20,    13,
     116,    17,    90,    23,    36,    39,    40,    41,    42,    43,
      44,    45,    82,    90,    95,   108,   176,   177,   263,   264,
     265,   324,   174,   175,   266,   100,   172,   178,   179,   180,
     267,   173,    13,    13,    13,    15,    15,   325,    15,   241,
     156,   254,   260,   194,   199,   332,    13,    75,   210,   171,
       4,    20,   321,   322,   323,   325,   209,   171,    40,    14,
     171,   206,    14,   186,   171,    21,    61,   218,   200,    14,
      14,   171,    14,   161,   271,   272,   254,   260,   254,     5,
      14,   254,    14,   186,     3,    24,    25,    26,    27,    32,
      33,    35,    54,    56,    57,    63,    72,    85,    86,    88,
     103,   105,   106,   110,   128,   130,   141,   149,   150,   157,
     276,   278,   296,   297,   298,   299,   300,   301,   302,   307,
     308,   309,   310,   311,   312,   313,   314,   315,   326,   329,
      14,   260,   242,   243,     6,     9,   108,   262,   254,   235,
      40,    40,    42,    13,   206,   261,     8,   108,   254,    23,
      82,    95,    40,   177,    16,    18,   142,   254,   320,   255,
     206,   256,   257,   258,    16,    62,   172,   254,   319,    14,
     254,   172,   325,   325,    15,    15,   325,    14,    13,   206,
     229,   230,   231,   232,   235,   236,   237,   238,   209,   227,
     323,   197,   254,   225,   170,    14,   216,    14,   260,   241,
     254,    64,   273,   271,    20,    14,   155,    14,   171,    93,
      13,    13,    93,   158,    13,    93,   158,    13,    13,    13,
     124,    13,   136,   303,   304,   306,   307,    32,    33,    13,
      93,   158,    13,    13,    13,    13,   163,   164,    13,   163,
     164,    13,    19,   102,    33,   324,   324,    15,    14,   262,
      17,   260,     8,    67,   254,   261,   254,    14,    14,   254,
     120,   279,    14,    15,   325,   325,    15,    13,   230,   236,
     321,    49,    76,    83,    91,    94,   131,   171,   239,   321,
      78,   211,   170,   147,   147,   254,    65,   296,   235,   254,
     111,     5,     5,   111,    13,     5,   111,    13,     5,   316,
       5,     5,     5,    13,   151,    13,    13,   158,    13,    93,
     158,     5,   111,    13,   316,     5,   235,     5,   149,   149,
       5,   149,   149,     5,    13,   139,   326,   254,    14,   254,
      17,    67,   279,   279,    14,    13,   280,   281,   325,   172,
     325,   325,    14,   224,   233,    91,   119,   240,   230,   240,
     240,   229,    91,    28,    80,   219,   254,   254,    14,    14,
      14,    13,    14,    14,    13,     5,    14,    13,     5,    34,
      38,    77,    92,    96,   112,   317,   318,    14,    14,   171,
      14,   171,    14,     5,   136,   305,   307,     5,     5,    13,
       5,   111,    13,    14,    13,     5,    14,    14,   171,    14,
      14,   166,   166,    14,   166,   166,    14,     5,   235,    15,
     254,   254,   279,   281,   282,   283,    15,   231,   114,   230,
     226,   241,     5,   316,    14,   316,    14,   317,     5,     5,
      14,   171,    13,    14,    14,     5,    14,    13,     5,   316,
      14,     5,   135,   163,   164,   163,   164,    14,   324,   326,
      14,   121,   284,   172,   325,   241,   114,    13,    79,   212,
     213,   254,    14,    14,    14,    14,    14,     5,     5,    14,
     316,    14,    14,    14,   235,   149,   149,   149,   149,    15,
      28,   214,    15,   241,    14,   140,   171,    14,    14,    14,
     166,   166,   166,   166,   326,   260,   125,   133,   285,   286,
     172,   325,    13,   212,    23,    51,   152,   268,   287,   288,
     289,   290,   212,   152,   268,   288,   291,   292,   132,   123,
     123,    70,   293,    14,    73,    73,    17,    51,    78,   107,
     148,   291,   132,   118
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   181,   182,   183,   183,   184,   184,   184,   184,   185,
     185,   185,   185,   186,   186,   187,   187,   188,   188,   189,
     190,   191,   191,   191,   192,   193,   193,   193,   193,   193,
     194,   195,   196,   197,   197,   198,   199,   200,   200,   201,
     201,   201,   202,   202,   203,   203,   204,   205,   206,   207,
     208,   209,   209,   210,   211,   211,   212,   212,   213,   213,
     213,   214,   214,   215,   216,   216,   217,   218,   218,   218,
     219,   219,   220,   220,   221,   221,   222,   223,   223,   224,
     225,   225,   226,   226,   226,   227,   227,   228,   228,   229,
     229,   230,   230,   231,   231,   232,   232,   233,   233,   234,
     234,   234,   234,   234,   234,   234,   234,   234,   235,   235,
     235,   235,   235,   235,   235,   235,   236,   236,   237,   237,
     238,   239,   239,   239,   239,   240,   240,   241,   241,   242,
     242,   243,   243,   244,   244,   244,   245,   245,   246,   246,
     246,   246,   246,   246,   246,   247,   248,   249,   249,   250,
     250,   250,   250,   251,   251,   252,   252,   253,   254,   254,
     254,   255,   255,   256,   256,   257,   257,   258,   258,   258,
     259,   259,   259,   259,   259,   259,   260,   260,   261,   261,
     262,   262,   263,   263,   263,   263,   263,   263,   263,   263,
     263,   263,   264,   264,   265,   266,   266,   267,   267,   267,
     267,   267,   268,   268,   268,   268,   268,   268,   268,   268,
     269,   270,   270,   271,   271,   272,   272,   273,   273,   274,
     274,   274,   274,   275,   275,   275,   275,   275,   275,   276,
     276,   276,   276,   276,   276,   276,   276,   276,   276,   277,
     278,   278,   278,   278,   278,   279,   279,   280,   281,   282,
     283,   283,   284,   284,   285,   285,   286,   286,   287,   287,
     288,   288,   288,   289,   290,   291,   291,   291,   292,   293,
     293,   293,   293,   293,   294,   294,   295,   295,   295,   295,
     295,   295,   295,   295,   295,   295,   295,   295,   296,   296,
     296,   296,   297,   298,   298,   298,   298,   299,   300,   300,
     300,   301,   301,   301,   301,   301,   301,   301,   301,   301,
     301,   301,   302,   303,   303,   304,   304,   305,   305,   305,
     306,   306,   306,   306,   306,   307,   307,   307,   307,   307,
     308,   309,   309,   309,   309,   309,   309,   309,   309,   309,
     309,   309,   310,   310,   311,   311,   311,   311,   311,   311,
     311,   311,   311,   311,   311,   311,   311,   312,   312,   312,
     312,   313,   313,   313,   313,   313,   313,   313,   313,   313,
     313,   313,   313,   313,   314,   314,   314,   314,   315,   315,
     315,   315,   315,   315,   315,   315,   315,   315,   315,   315,
     315,   315,   315,   316,   316,   316,   316,   317,   317,   317,
     318,   318,   318,   319,   319,   320,   320,   320,   321,   321,
     322,   322,   323,   323,   324,   325,   325,   326,   326,   327,
     327,   327,   327,   327,   327,   327,   327,   327,   327,   327,
     327,   327,   327,   327,   327,   327,   327,   328,   328,   328,
     328,   328,   328,   328,   328,   328,   328,   328,   328,   328,
     328,   328,   328,   328,   328,   328,   328,   328,   328,   328,
     328,   328,   328,   328,   328,   328,   328,   328,   328,   328,
     328,   328,   329,   329,   329,   330,   330,   330,   330,   330,
     331,   331,   332,   332
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     1,     3,     0,     1,     1,     1,     5,
       4,     7,     6,     1,     3,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     4,     1,     2,     2,     1,     2,
       2,     4,     5,     1,     3,     3,     2,     1,     2,     1,
       4,     4,     1,     4,     1,     3,     3,     1,     1,     1,
       8,     0,     2,     2,     0,     4,     1,     3,     2,     1,
       5,     0,     1,     3,     1,     3,     2,     0,     1,     1,
       0,     2,     2,     3,     1,     3,     4,     0,     1,     3,
       1,     3,     0,     1,     1,     1,     3,     2,     1,     1,
       3,     1,     1,     1,     3,     2,     3,     0,     1,     1,
       3,     3,     5,     5,     7,     7,     9,     9,     1,     3,
       5,     4,     7,     6,     6,     5,     1,     1,     6,     5,
       4,     2,     2,     2,     1,     0,     1,     1,     3,     1,
       3,     1,     2,     1,     3,     4,     1,     3,     1,     1,
       1,     1,     1,     1,     1,     3,     3,     5,     6,     3,
       5,     4,     6,     3,     4,     3,     4,     2,     1,     2,
       3,     1,     3,     1,     3,     1,     3,     1,     2,     2,
       1,     1,     1,     3,     1,     1,     1,     3,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     2,
       2,     2,     1,     2,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       5,     0,     1,     1,     2,     4,     4,     0,     2,     1,
       1,     1,     1,     5,     4,     6,     5,     5,     4,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     4,
       1,     1,     1,     1,     1,     2,     2,     3,     1,     4,
       0,     1,     0,     3,     0,     3,     1,     1,     1,     1,
       2,     2,     1,     2,     4,     1,     2,     1,     2,     0,
       3,     2,     2,     3,     3,     4,     4,     3,     3,     6,
       6,     4,     1,     4,     1,     6,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     5,     7,     6,     5,     2,
       2,     5,     4,     2,     1,     2,     1,     1,     1,     1,
       1,     1,     2,     3,     1,     4,     1,     1,     4,     1,
       4,     1,     6,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     7,     7,     4,     4,     4,     7,     7,     4,
       4,     4,     1,     1,     6,     4,     6,     4,     6,     4,
       1,     1,     1,     1,     1,     1,     1,     4,     2,     1,
       1,     4,     4,     5,     5,     4,     6,     3,     6,     3,
       4,     1,     1,     1,     6,     3,     4,     1,     5,     2,
       5,     2,     4,     6,     6,     5,     7,     4,     6,     3,
       4,     1,     1,     3,     2,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     0,     1,
       2,     1,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     2,     1,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (&yylloc, result, scanner, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if SQL2003_DEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined SQL2003_LTYPE_IS_TRIVIAL && SQL2003_LTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location, result, scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, result, scanner);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule, ParseResult* result, yyscan_t scanner)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       , result, scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, result, scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !SQL2003_DEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !SQL2003_DEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, ParseResult* result, yyscan_t scanner)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (result);
  YYUSE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  switch (yytype)
    {
          case 3: /* NAME  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2923 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 4: /* STRING  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2929 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 5: /* INTNUM  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2935 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 6: /* BOOL  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2941 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 7: /* APPROXNUM  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2947 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 8: /* NULLX  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2953 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 9: /* UNKNOWN  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2959 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 10: /* QUESTIONMARK  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2965 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 11: /* PARAM  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2971 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 182: /* sql_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2977 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 183: /* stmt_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2983 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 184: /* stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2989 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 185: /* call_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 2995 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 186: /* sql_argument_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3001 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 187: /* sql_argument  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3007 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 188: /* value_expression  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3013 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 189: /* sp_name  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3019 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 190: /* dql_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3025 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 191: /* dml_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3031 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 192: /* insert_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3037 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 193: /* insert_columns_and_source  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3043 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 194: /* from_constructor  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3049 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 195: /* delete_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3055 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 196: /* update_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3061 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 197: /* update_elem_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3067 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 198: /* update_elem  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3073 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 199: /* select_stmt  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3079 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 200: /* query_expression  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3085 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 201: /* query_expression_body  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3091 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 202: /* query_term  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3097 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 203: /* query_primary  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3103 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 204: /* select_with_parens  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3109 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 205: /* subquery  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3115 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 206: /* table_subquery  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3121 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 207: /* row_subquery  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3127 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 208: /* simple_table  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3133 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 209: /* opt_where  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3139 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 210: /* opt_from_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3145 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 211: /* opt_groupby  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3151 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 212: /* grouping_element_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3157 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 213: /* grouping_element  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3163 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 214: /* opt_order_by  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3169 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 215: /* order_by  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3175 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 216: /* sort_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3181 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 217: /* sort_key  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3187 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 218: /* opt_asc_desc  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3193 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 219: /* opt_having  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3199 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 220: /* with_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3205 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 221: /* with_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3211 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 222: /* common_table_expr  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3217 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 223: /* opt_derived_column_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3223 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 224: /* simple_ident_list_with_parens  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3229 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 225: /* simple_ident_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3235 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 226: /* opt_distinct  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3241 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 227: /* select_expr_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3247 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 228: /* projection  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3253 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 229: /* from_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3259 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 230: /* table_reference  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3265 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 231: /* table_primary  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3271 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 232: /* table_primary_non_join  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3277 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 233: /* opt_simple_ident_list_with_parens  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3283 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 234: /* column_ref  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3289 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 235: /* relation_factor  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3295 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 236: /* joined_table  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3301 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 237: /* qualified_join  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3307 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 238: /* cross_join  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3313 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 239: /* join_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3319 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 241: /* search_condition  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3325 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 242: /* boolean_term  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3331 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 243: /* boolean_factor  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3337 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 244: /* boolean_test  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3343 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 245: /* boolean_primary  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3349 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 246: /* predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3355 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 247: /* comparison_predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3361 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 248: /* quantified_comparison_predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3367 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 249: /* between_predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3373 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 250: /* like_predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3379 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 251: /* in_predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3385 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 252: /* null_predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3391 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 253: /* exists_predicate  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3397 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 254: /* row_expr  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3403 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 255: /* factor0  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3409 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 256: /* factor1  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3415 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 257: /* factor2  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3421 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 258: /* factor3  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3427 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 259: /* factor4  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3433 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 260: /* row_expr_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3439 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 261: /* in_expr  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3445 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 262: /* truth_value  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3451 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 268: /* expr_const  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3457 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 269: /* case_expr  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3463 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 270: /* case_arg  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3469 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 271: /* when_clause_list  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3475 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 272: /* when_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3481 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 273: /* case_default  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3487 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 274: /* func_expr  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3493 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 275: /* aggregate_windowed_function  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3499 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 276: /* aggregate_function_name  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3505 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 277: /* ranking_windowed_function  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3511 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 278: /* ranking_function_name  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3517 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 279: /* over_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3523 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 280: /* window_specification  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3529 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 281: /* window_name  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3535 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 282: /* window_specification_details  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3541 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 283: /* opt_existing_window_name  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3547 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 284: /* opt_window_partition_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3553 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 285: /* opt_window_frame_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3559 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 286: /* window_frame_units  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3565 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 287: /* window_frame_extent  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3571 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 288: /* window_frame_start  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3577 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 289: /* window_frame_preceding  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3583 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 290: /* window_frame_between  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3589 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 291: /* window_frame_bound  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3595 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 292: /* window_frame_following  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3601 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 293: /* opt_window_frame_exclusion  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3607 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 294: /* scalar_function  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3613 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 295: /* function_call_keyword  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3619 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 296: /* data_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3625 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 297: /* user_defined_type_name  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3631 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 298: /* relation_factor_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3637 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 299: /* reference_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3643 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 300: /* collection_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3649 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 301: /* predefined_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3655 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 302: /* interval_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3661 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 303: /* interval_qualifier  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3667 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 304: /* start_field  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3673 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 305: /* end_field  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3679 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 306: /* single_datetime_field  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3685 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 307: /* non_second_primary_datetime_field  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3691 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 308: /* boolean_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3697 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 309: /* datetime_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3703 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 310: /* numeric_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3709 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 311: /* exact_numeric_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3715 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 312: /* approximate_numeric_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3721 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 313: /* character_string_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3727 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 314: /* binary_large_object_string_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3733 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 315: /* national_character_string_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3739 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 316: /* large_object_length  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3745 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 317: /* char_length_units  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3751 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 318: /* multiplier  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3757 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 319: /* distinct_or_all  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3763 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 321: /* opt_as_label  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3769 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 322: /* as_label  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3775 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 323: /* label  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3781 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 324: /* collate_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3787 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 325: /* name_r  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3793 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 326: /* name_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3799 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 327: /* reserved_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3805 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 328: /* reserved_other  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3811 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 329: /* reserved_non_type  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3817 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 330: /* reserved  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3823 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 331: /* opt_top_clause  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3829 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;

    case 332: /* intnum_parens  */
#line 95 "sqlparser_sql2003.y" /* yacc.c:1257  */
      { delete(((*yyvaluep).node)); }
#line 3835 "sqlparser_sql2003_bison.cpp" /* yacc.c:1257  */
        break;


      default:
        break;
    }
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (ParseResult* result, yyscan_t scanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined SQL2003_LTYPE_IS_TRIVIAL && SQL2003_LTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

/* User initialization code.  */
#line 64 "sqlparser_sql2003.y" /* yacc.c:1429  */
{
	// Initialize
	yylloc.first_column = 0;
	yylloc.last_column = 0;
	yylloc.first_line = 0;
	yylloc.last_line = 0;
}

#line 3952 "sqlparser_sql2003_bison.cpp" /* yacc.c:1429  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, &yylloc, scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 233 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = (yyvsp[-1].node);
    result->result_tree_ = (yyvsp[-1].node);
    result->accept = true;
    YYACCEPT;
}
#line 4146 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 4:
#line 244 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SEMICOLON_LIST_SERIALIZE_FORMAT;
}
#line 4155 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 5:
#line 251 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4161 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 9:
#line 259 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &CALL_SERIALIZE_FORMAT;
}
#line 4170 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 10:
#line 264 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-2].node), nullptr);
    (yyval.node)->serialize_format = &CALL_SERIALIZE_FORMAT;
}
#line 4179 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 11:
#line 269 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node));
    (yyval.node)->serialize_format = &CALL_SQL_SERVER_SERIALIZE_FORMAT;
}
#line 4188 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 12:
#line 274 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CALL, E_CALL_PROPERTY_CNT, (yyvsp[-3].node), nullptr);
    (yyval.node)->serialize_format = &CALL_SQL_SERVER_SERIALIZE_FORMAT;
}
#line 4197 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 14:
#line 283 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_STMT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4206 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 15:
#line 291 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 4215 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 16:
#line 296 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SQL_ARGUMENT, E_SQL_ARGUMENT_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SQL_ARGUMENT_SERIALIZE_FORMAT;
}
#line 4224 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 24:
#line 323 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT, E_INSERT_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &INSERT_SERIALIZE_FORMAT;
}
#line 4233 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 25:
#line 331 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4242 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 26:
#line 336 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4251 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 27:
#line 341 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4260 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 28:
#line 346 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4269 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 29:
#line 351 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* src = Node::makeTerminalNode(E_IDENTIFIER, "DEFAULT VALUES");
    (yyval.node) = Node::makeNonTerminalNode(E_INSERT_COLUMNS_AND_SOURCE, E_INSERT_COLUMNS_AND_SOURCE_PROPERTY_CNT, nullptr, src);
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4279 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 30:
#line 360 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_VALUES_CTOR, E_VALUES_CTOR_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &TABLE_VALUE_CTOR_SERIALIZE_FORMAT;
}
#line 4288 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 31:
#line 369 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DELETE, E_DELETE_PROPERTY_CNT,
    		nullptr,	/* E_DELETE_OPT_WITH 0 */
    		nullptr,	/* E_DELETE_OPT_TOP 1 */
    		(yyvsp[-1].node),		/* E_DELETE_DELETE_RELATION 2 */
    		nullptr,	/* E_DELETE_DELETE_RELATION_OPT_TABLE_HINT 3 */
    		nullptr,        /* E_DELETE_OPT_OUTPUT 4 */
    		nullptr,	/* E_DELETE_FROM_LIST 5 */
    		(yyvsp[0].node),		/* E_DELETE_OPT_WHERE 6 */
    		nullptr 	/* E_DELETE_OPT_QUERY_HINT 7 */);
    (yyval.node)->serialize_format = &DELETE_SERIALIZE_FORMAT;
}
#line 4305 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 32:
#line 386 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPDATE, E_UPDATE_PROPERTY_CNT,
    		nullptr,	/* E_UPDATE_OPT_WITH 0 */
    		nullptr,	/* E_UPDATE_OPT_TOP 1 */
    		(yyvsp[-3].node),		/* E_UPDATE_UPDATE_RELATION 2 */
    		nullptr,	/* E_UPDATE_UPDATE_RELATION_OPT_TABLE_HINT 3 */
    		(yyvsp[-1].node),		/* E_UPDATE_UPDATE_ELEM_LIST 4 */
    		nullptr,	/* E_UPDATE_OPT_OUTPUT 5 */
    		nullptr,	/* E_UPDATE_FROM_LIST 6 */
    		(yyvsp[0].node),		/* E_UPDATE_OPT_WHERE 7 */
    		nullptr		/* E_UPDATE_OPT_QUERY_HINT 8 */);
    (yyval.node)->serialize_format = &UPDATE_SERIALIZE_FORMAT;
}
#line 4323 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 34:
#line 404 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_UPDATE_ELEM_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4332 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 35:
#line 412 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_EQ, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &OP_EQ_SERIALIZE_FORMAT;
}
#line 4341 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 36:
#line 421 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = (yyvsp[-1].node);
    (yyval.node)->setChild(E_DIRECT_SELECT_ORDER, (yyvsp[0].node));
}
#line 4350 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 37:
#line 429 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, nullptr, (yyvsp[0].node), nullptr, nullptr,nullptr);
    (yyval.node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT;
}
#line 4359 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 38:
#line 434 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_DIRECT_SELECT, E_DIRECT_SELECT_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr,nullptr);
    (yyval.node)->serialize_format = &SELECT_DIRECT_SERIALIZE_FORMAT;
}
#line 4368 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 40:
#line 442 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_UNION, "UNION");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr             /*user for bigquery*/
                        );
    (yyval.node)->serialize_format = &SELECT_UNION_SERIALIZE_FORMAT;
}
#line 4399 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 41:
#line 469 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_EXCEPT, "EXCEPT");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,             /* E_SELECT_OPT_INTO 18 */
                        nullptr             /*user for bigquery*/
                        );
    (yyval.node)->serialize_format = &SELECT_EXCEPT_SERIALIZE_FORMAT;
}
#line 4430 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 43:
#line 500 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* set_op = Node::makeTerminalNode(E_SET_INTERSECT, "INTERSECT");
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                        nullptr,             /* E_SELECT_DISTINCT 0 */
                        nullptr,             /* E_SELECT_SELECT_EXPR_LIST 1 */
                        nullptr,             /* E_SELECT_FROM_LIST 2 */
                        nullptr,             /* E_SELECT_OPT_WHERE 3 */
                        nullptr,             /* E_SELECT_GROUP_BY 4 */
                        nullptr,             /* E_SELECT_HAVING 5 */
                        set_op,              /* E_SELECT_SET_OPERATION 6 */
                        (yyvsp[-1].node),                  /* E_SELECT_ALL_SPECIFIED 7 */
                        (yyvsp[-3].node),                  /* E_SELECT_FORMER_SELECT_STMT 8 */
                        (yyvsp[0].node),                  /* E_SELECT_LATER_SELECT_STMT 9 */
                        nullptr,             /* E_SELECT_ORDER_BY 10 */
                        nullptr,             /* E_SELECT_LIMIT 11 */
                        nullptr,             /* E_SELECT_FOR_UPDATE 12 */
                        nullptr,             /* E_SELECT_HINTS 13 */
                        nullptr,             /* E_SELECT_WHEN 14 */
                        nullptr,             /* E_SELECT_OPT_TOP 15 */
                        nullptr,             /* E_SELECT_OPT_WITH 16 */
                        nullptr,             /* E_SELECT_OPT_OPTION 17 */
                        nullptr,              /* E_SELECT_OPT_INTO 18 */
                        nullptr             /*user for bigquery*/
                        );
    (yyval.node)->serialize_format = &SELECT_INTERSECT_SERIALIZE_FORMAT;
}
#line 4461 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 45:
#line 531 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    if ((yyvsp[-1].node)->getChild(E_DIRECT_SELECT_WITH)) {
	yyerror(&(yylsp[-2]), result, scanner, "error use common table expression");
	YYABORT;
    }
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node)->getChild(E_DIRECT_SELECT_SELECT_CLAUSE));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
    (yyvsp[-1].node)->setChild(E_DIRECT_SELECT_SELECT_CLAUSE, nullptr);
    delete((yyvsp[-1].node));
}
#line 4476 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 46:
#line 546 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 4485 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 50:
#line 560 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT, E_SELECT_PROPERTY_CNT,
                    (yyvsp[-6].node),             /* E_SELECT_DISTINCT 0 */
                    (yyvsp[-4].node),             /* E_SELECT_SELECT_EXPR_LIST 1 */
                    (yyvsp[-3].node),             /* E_SELECT_FROM_LIST 2 */
                    (yyvsp[-2].node),             /* E_SELECT_OPT_WHERE 3 */
                    (yyvsp[-1].node),             /* E_SELECT_GROUP_BY 4 */
                    (yyvsp[0].node),             /* E_SELECT_HAVING 5 */
                    nullptr,        /* E_SELECT_SET_OPERATION 6 */
                    nullptr,        /* E_SELECT_ALL_SPECIFIED 7 */
                    nullptr,        /* E_SELECT_FORMER_SELECT_STMT 8 */
                    nullptr,        /* E_SELECT_LATER_SELECT_STMT 9 */
                    nullptr,        /* E_SELECT_ORDER_BY 10 */
                    nullptr,        /* E_SELECT_LIMIT 11 */
                    nullptr,        /* E_SELECT_FOR_UPDATE 12 */
                    nullptr,        /* E_SELECT_HINTS 13 */
                    nullptr,        /* E_SELECT_WHEN 14 */
                    (yyvsp[-5].node),        /* E_SELECT_OPT_TOP 15 */
                    nullptr,        /* E_SELECT_OPT_WITH 16 */
                    nullptr,        /* E_SELECT_OPT_OPTION 17 */
	                nullptr,         /* E_SELECT_OPT_INTO 18 */
	                nullptr             /*user for bigquery*/
                    );
    (yyval.node)->serialize_format = &SELECT_SERIALIZE_FORMAT;
}
#line 4515 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 51:
#line 588 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4521 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 52:
#line 590 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHERE_CLAUSE, E_WHERE_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHERE_SERIALIZE_FORMAT;
}
#line 4530 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 53:
#line 599 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FROM_CLAUSE, E_FROM_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &FROM_SERIALIZE_FORMAT;
}
#line 4539 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 54:
#line 606 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4545 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 55:
#line 608 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_GROUP_BY, E_GROUP_BY_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = &GROUP_BY_SERIALIZE_FORMAT;
}
#line 4554 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 57:
#line 617 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4563 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 58:
#line 624 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"()"); }
#line 4569 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 60:
#line 627 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "GROUPING SETS");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 4580 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 61:
#line 636 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4586 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 63:
#line 642 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ORDER_BY, E_ORDER_BY_PROPERTY_CNT, (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &ORDER_BY_SERIALIZE_FORMAT;
}
#line 4595 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 65:
#line 651 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SORT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4604 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 66:
#line 659 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SORT_KEY, E_SORT_KEY_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 4613 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 67:
#line 667 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_ASC, "");
}
#line 4621 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 68:
#line 671 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_ASC, "ASC");
}
#line 4629 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 69:
#line 675 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_SORT_DESC, "DESC");
}
#line 4637 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 70:
#line 681 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4643 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 71:
#line 683 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_HAVING, E_HAVING_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &HAVING_SERIALIZE_FORMAT;
}
#line 4652 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 72:
#line 693 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WITH_CLAUSE_SERIALIZE_FORMAT;
}
#line 4661 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 73:
#line 698 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OPT_WITH_CLAUSE, E_OPT_WITH_CLAUSE_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &WITH_RECURSIVE_CLAUSE_SERIALIZE_FORMAT;
}
#line 4670 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 75:
#line 707 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WITH_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4679 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 76:
#line 716 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_COMMON_TABLE_EXPR, E_COMMON_TABLE_EXPR_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMON_TABLE_EXPR_SERIALIZE_FORMAT;
}
#line 4688 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 77:
#line 723 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4694 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 79:
#line 729 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 4703 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 81:
#line 738 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SIMPLE_IDENT_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4712 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 82:
#line 745 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4718 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 83:
#line 747 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_ALL, "ALL");
}
#line 4726 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 84:
#line 751 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_DISTINCT, "DISTINCT");
}
#line 4734 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 86:
#line 759 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_SELECT_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4743 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 87:
#line 767 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    if (!(yyvsp[0].node)) {
    	(yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, (yyvsp[-1].node));
    	(yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
    else {
	Node* alias_node = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
        alias_node->serialize_format = &AS_SERIALIZE_FORMAT;

        (yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, alias_node);
        (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
    }
}
#line 4761 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 88:
#line 781 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* star_node = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_PROJECT_STRING, E_PROJECT_STRING_PROPERTY_CNT, star_node);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 4771 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 90:
#line 791 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FROM_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 4780 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 94:
#line 805 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 4789 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 95:
#line 817 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 4798 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 96:
#line 822 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_ALIAS, E_ALIAS_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &AS_SERIALIZE_FORMAT;
}
#line 4807 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 97:
#line 829 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 4813 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 99:
#line 836 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4823 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 100:
#line 842 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4833 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 101:
#line 848 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4844 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 102:
#line 855 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4854 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 103:
#line 861 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), (yyvsp[-4].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4865 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 104:
#line 868 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4875 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 105:
#line 874 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), nullptr);
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4886 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 106:
#line 882 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			(yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), (yyvsp[-8].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4896 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 107:
#line 888 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NAME_FIELD, E_OP_NAME_FIELD_PROPERTY_CNT,
    			nd, (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node), (yyvsp[-8].node));
    (yyval.node)->serialize_format = &OP_NAME_FIELD_SERIALIZE_FORMAT_4;
}
#line 4907 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 108:
#line 953 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 4916 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 109:
#line 958 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 4925 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 110:
#line 963 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 4934 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 111:
#line 968 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, (yyvsp[-3].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 4943 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 112:
#line 973 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 4952 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 113:
#line 978 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, (yyvsp[-5].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 4961 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 114:
#line 983 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, (yyvsp[-3].node), (yyvsp[-5].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 4970 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 115:
#line 988 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, (yyvsp[-4].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 4979 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 118:
#line 1003 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 4988 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 119:
#line 1008 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_INNER, "");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &JOINED_TB_1_SERIALIZE_FORMAT;
}
#line 4998 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 120:
#line 1029 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* nd = Node::makeTerminalNode(E_JOIN_CROSS, "CROSS");
    (yyval.node) = Node::makeNonTerminalNode(E_JOINED_TABLE, E_JOINED_TABLE_PROPERTY_CNT, nd, (yyvsp[-3].node), (yyvsp[0].node), nullptr);
    (yyval.node)->serialize_format = &JOINED_TB_2_SERIALIZE_FORMAT;
}
#line 5008 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 121:
#line 1054 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    if ((yyvsp[0].ival))
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_FULL, "FULL OUTER");
    }
    else
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_FULL, "FULL");
    }
}
#line 5023 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 122:
#line 1065 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    if ((yyvsp[0].ival))
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_LEFT, "LEFT OUTER");
    }
    else
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_LEFT, "LEFT");
    }
}
#line 5038 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 123:
#line 1076 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    if ((yyvsp[0].ival))
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_RIGHT, "RIGHT OUTER");
    }
    else
    {
        (yyval.node) = Node::makeTerminalNode(E_JOIN_RIGHT, "RIGHT");
    }
}
#line 5053 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 124:
#line 1087 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_JOIN_INNER, "INNER");
}
#line 5061 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 125:
#line 1093 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.ival) = 0; }
#line 5067 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 126:
#line 1094 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.ival) = 1; /*this is a flag*/}
#line 5073 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 128:
#line 1101 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_OR, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_OR);
}
#line 5082 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 130:
#line 1110 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_AND, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_AND);
}
#line 5091 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 132:
#line 1119 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT);
}
#line 5100 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 134:
#line 1128 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 5109 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 135:
#line 1133 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 5118 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 137:
#line 1142 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5127 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 145:
#line 1170 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode((yyvsp[-1].nodetype), E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 5136 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 146:
#line 1178 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode((yyvsp[-1].nodetype), E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 5145 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 147:
#line 1186 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_BTW, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_BTW);
}
#line 5154 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 148:
#line 1191 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_BTW, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_BTW);
}
#line 5163 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 149:
#line 1199 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 5172 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 150:
#line 1204 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_LIKE, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_LIKE);
}
#line 5181 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 151:
#line 1209 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 5190 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 152:
#line 1214 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_LIKE, E_OP_TERNARY_PROPERTY_CNT, (yyvsp[-5].node), (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_LIKE);
}
#line 5199 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 153:
#line 1222 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IN);
}
#line 5208 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 154:
#line 1227 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NOT_IN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NOT_IN);
}
#line 5217 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 155:
#line 1235 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS);
}
#line 5226 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 156:
#line 1240 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_IS_NOT, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_IS_NOT);
}
#line 5235 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 157:
#line 1248 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_EXISTS, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_EXISTS);
}
#line 5244 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 159:
#line 1258 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_COLLATE, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &DOUBLE_SERIALIZE_FORMAT;
}
#line 5253 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 160:
#line 1263 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_CNN, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 5262 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 162:
#line 1272 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_ADD, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 5271 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 164:
#line 1281 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_MUL, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format((yyvsp[-1].nodetype));
}
#line 5280 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 166:
#line 1290 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_POW, E_OP_BINARY_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_POW);
}
#line 5289 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 168:
#line 1299 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_POS, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_POS);
}
#line 5298 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 169:
#line 1304 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OP_NEG, E_OP_UNARY_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_OP_NEG);
}
#line 5307 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 173:
#line 1315 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5316 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 177:
#line 1326 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
}
#line 5325 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 179:
#line 1335 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 5334 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 182:
#line 1345 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LE; }
#line 5340 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 183:
#line 1346 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LT; }
#line 5346 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 184:
#line 1347 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GE; }
#line 5352 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 185:
#line 1348 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GT; }
#line 5358 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 186:
#line 1349 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_EQ; }
#line 5364 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 187:
#line 1350 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 5370 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 188:
#line 1351 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_GE; }
#line 5376 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 189:
#line 1352 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_LE; }
#line 5382 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 190:
#line 1353 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 5388 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 191:
#line 1354 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_NE; }
#line 5394 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 192:
#line 1358 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_CNN; }
#line 5400 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 193:
#line 1359 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_CNN; }
#line 5406 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 194:
#line 1365 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.nodetype) = Node::comp_all_some_any_op_form((yyvsp[-1].nodetype), (yyvsp[0].ival));
}
#line 5414 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 195:
#line 1371 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_ADD; }
#line 5420 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 196:
#line 1372 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MINUS; }
#line 5426 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 197:
#line 1376 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MUL; }
#line 5432 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 198:
#line 1377 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_DIV; }
#line 5438 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 199:
#line 1378 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_REM; }
#line 5444 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 200:
#line 1379 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_MOD; }
#line 5450 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 201:
#line 1380 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.nodetype) = E_OP_AND_SPC; }
#line 5456 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 209:
#line 1391 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_STRING, "DEFAULT"); }
#line 5462 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 210:
#line 1397 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE, E_CASE_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node));
    (yyval.node)->serialize_format = Node::op_serialize_format(E_CASE);
}
#line 5471 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 211:
#line 1404 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5477 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 214:
#line 1411 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &SPACE_LIST_SERIALIZE_FORMAT;
}
#line 5486 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 215:
#line 1419 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 5495 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 216:
#line 1424 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WHEN, E_WHEN_PROPERTY_CNT, (yyvsp[-2].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WHEN_SERIALIZE_FORMAT;
}
#line 5504 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 217:
#line 1431 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5510 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 218:
#line 1433 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_CASE_DEFAULT, E_CASE_DEFAULT_PROPERTY_CNT, (yyvsp[0].node));
    (yyval.node)->serialize_format = &ELSE_SERIALIZE_FORMAT;
}
#line 5519 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 223:
#line 1450 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5529 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 224:
#line 1456 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5539 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 225:
#line 1462 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-5].node), (yyvsp[-3].node), (yyvsp[-2].node), nullptr, (yyvsp[0].node));
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 5549 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 226:
#line 1468 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[-1].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_DIS_OVER_SERIALIZE_FORMAT;
}
#line 5559 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 227:
#line 1474 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    if (!Node::CHECK_FUNCTION_CALL_WITH_STAR((yyvsp[-4].node)))
    {
        yyerror(&(yylsp[-4]), result, scanner, "error use *");
        YYABORT;
    }
    Node* star = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-4].node), star, (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5575 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 228:
#line 1486 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    if (!Node::CHECK_FUNCTION_CALL_WITH_STAR((yyvsp[-3].node)))
    {
        yyerror(&(yylsp[-3]), result, scanner, "error use *");
        YYABORT;
    }
    Node* star = Node::makeTerminalNode(E_STAR, "*");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), star, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5591 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 229:
#line 1500 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "AVG"); }
#line 5597 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 230:
#line 1501 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MAX"); }
#line 5603 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 231:
#line 1502 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MIN"); }
#line 5609 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 232:
#line 1503 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SUM"); }
#line 5615 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 233:
#line 1504 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_POP"); }
#line 5621 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 234:
#line 1505 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "STDDEV_SAMP"); }
#line 5627 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 235:
#line 1506 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_POP"); }
#line 5633 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 236:
#line 1507 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VAR_SAMP"); }
#line 5639 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 237:
#line 1508 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "COUNT"); }
#line 5645 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 238:
#line 1509 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "GROUPING"); }
#line 5651 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 239:
#line 1514 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), nullptr, (yyvsp[0].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5661 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 240:
#line 1522 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RANK"); }
#line 5667 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 241:
#line 1523 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DENSE_RANK"); }
#line 5673 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 242:
#line 1524 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PERCENT_RANK"); }
#line 5679 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 243:
#line 1525 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CUME_DIST"); }
#line 5685 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 244:
#line 1526 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW_NUMBER"); }
#line 5691 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 245:
#line 1531 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OVER "+ (yyvsp[0].node)->text()); delete((yyvsp[0].node));
}
#line 5699 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 246:
#line 1535 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_OVER_CLAUSE, 1, (yyvsp[0].node));
    (yyval.node)->serialize_format = &OVER_CLAUSE_SERIALIZE_FORMAT;
}
#line 5708 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 247:
#line 1544 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[-1].node); }
#line 5714 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 249:
#line 1553 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_WINDOW_SPECIFIC, E_WINDOW_SPECIFIC_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-2].node), (yyvsp[-1].node), (yyvsp[0].node));
    (yyval.node)->serialize_format = &WINDOW_SPECIFIC_CLAUSE_SERIALIZE_FORMAT;
}
#line 5723 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 250:
#line 1560 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5729 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 252:
#line 1565 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5735 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 253:
#line 1567 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 5741 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 254:
#line 1571 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5747 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 255:
#line 1573 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    std::string s3 = (yyvsp[0].node) ? (yyvsp[0].node)->text() : "";
    (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, (yyvsp[-2].node)->text()+" "+(yyvsp[-1].node)->text()+" "+s3);
    delete((yyvsp[-2].node)); delete((yyvsp[-1].node)); delete((yyvsp[0].node));
}
#line 5757 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 256:
#line 1581 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"ROWS"); }
#line 5763 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 257:
#line 1582 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"RANGE"); }
#line 5769 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 260:
#line 1591 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED PRECEDING"); }
#line 5775 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 261:
#line 1592 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"CURRENT ROW"); }
#line 5781 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 263:
#line 1597 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,(yyvsp[-1].node)->text()+" PRECEDING"); delete((yyvsp[-1].node)); }
#line 5787 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 264:
#line 1602 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BETWEEN "+(yyvsp[-2].node)->text()+" AND "+(yyvsp[0].node)->text()); delete((yyvsp[-2].node)); delete((yyvsp[0].node)); }
#line 5793 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 266:
#line 1607 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"UNBOUNDED FOLLOWING"); }
#line 5799 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 268:
#line 1612 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,(yyvsp[-1].node)->text()+" FOLLOWING"); delete((yyvsp[-1].node)); }
#line 5805 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 269:
#line 1616 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 5811 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 270:
#line 1617 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE CURRENT ROW"); }
#line 5817 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 271:
#line 1618 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE GROUP"); }
#line 5823 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 272:
#line 1619 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE TIES"); }
#line 5829 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 273:
#line 1620 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER,"EXCLUDE NO OTHERS"); }
#line 5835 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 274:
#line 1625 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-2].node), nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5845 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 275:
#line 1631 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5855 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 276:
#line 1643 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		(yyvsp[-3].node), (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5865 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 277:
#line 1649 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "RIGHT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5876 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 278:
#line 1656 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "LEFT");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5887 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 279:
#line 1663 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CAST");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-3].node), nullptr, nullptr, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &FUN_CALL_AS_SERIALIZE_FORMAT;
}
#line 5898 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 280:
#line 1670 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* transcoding_name = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text());
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT");
    delete((yyvsp[-1].node));
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
        		fun_name, (yyvsp[-3].node), nullptr, nullptr, transcoding_name);
    (yyval.node)->serialize_format = &FUN_CALL_USING_SERIALIZE_FORMAT;
}
#line 5911 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 281:
#line 1679 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5922 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 282:
#line 1686 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 5933 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 283:
#line 1693 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_TIMESTAMP");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, (yyvsp[-1].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5944 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 284:
#line 1700 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "CURRENT_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 5955 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 285:
#line 1707 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* expr_list = Node::makeNonTerminalNode(E_EXPR_LIST, E_LIST_PROPERTY_CNT, (yyvsp[-3].node), (yyvsp[-1].node));
    expr_list->serialize_format = &COMMA_LIST_SERIALIZE_FORMAT;
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, expr_list, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &FUN_CALL_1_OVER_SERIALIZE_FORMAT;
}
#line 5968 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 286:
#line 1716 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SESSION_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 5979 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 287:
#line 1723 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    Node* fun_name = Node::makeTerminalNode(E_IDENTIFIER, "SYSTEM_USER");
    (yyval.node) = Node::makeNonTerminalNode(E_FUN_CALL, E_FUN_CALL_PROPERTY_CNT,
    		fun_name, nullptr, nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 5990 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 292:
#line 1743 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 5999 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 293:
#line 1750 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), nullptr, nullptr, nullptr);
    (yyval.node)->serialize_format = &SINGLE_SERIALIZE_FORMAT;
}
#line 6008 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 294:
#line 1755 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), nullptr, nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_1;
}
#line 6017 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 295:
#line 1760 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), nullptr);
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_2;
}
#line 6026 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 296:
#line 1765 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_TABLE_IDENT, E_TABLE_IDENT_PROPERTY_CNT, (yyvsp[0].node), (yyvsp[-2].node), (yyvsp[-4].node), (yyvsp[-6].node));
    (yyval.node)->serialize_format = &TABLE_IDENT_SERIALIZE_FORMAT_3;
}
#line 6035 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 297:
#line 1773 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "REF("+(yyvsp[-3].node)->text()+")SCOPE "+(yyvsp[0].node)->text());
    delete((yyvsp[-3].node));delete((yyvsp[0].node));
}
#line 6044 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 298:
#line 1781 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-4].node)->text()+" ARRAY("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-4].node));
    delete((yyvsp[-1].node));
}
#line 6054 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 299:
#line 1787 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" ARRAY");
    delete((yyvsp[-1].node));
}
#line 6063 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 300:
#line 1792 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" MULTISET");
    delete((yyvsp[-1].node));
}
#line 6072 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 301:
#line 1800 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-4].node)->text()+" CHARACTER SET "+(yyvsp[-1].node)->text()+(yyvsp[0].node)->text());
    delete((yyvsp[-4].node));delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6081 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 302:
#line 1805 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-3].node)->text()+" CHARACTER SET "+(yyvsp[0].node)->text());
    delete((yyvsp[-3].node));delete((yyvsp[0].node));
}
#line 6090 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 303:
#line 1810 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6099 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 305:
#line 1816 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6108 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 312:
#line 1830 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "INTERVAL "+(yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 6117 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 313:
#line 1838 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-2].node)->text()+" TO "+(yyvsp[0].node)->text());
    delete((yyvsp[-2].node));delete((yyvsp[0].node));
}
#line 6126 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 315:
#line 1847 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-3].node)->text()+"("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));
    delete((yyvsp[-1].node));
}
#line 6136 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 318:
#line 1858 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6145 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 319:
#line 1863 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND");
}
#line 6153 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 320:
#line 1870 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-3].node)->text()+"("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 6162 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 322:
#line 1876 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 6171 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 323:
#line 1881 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6180 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 324:
#line 1886 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SECOND");
}
#line 6188 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 325:
#line 1893 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "YEAR");
}
#line 6196 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 326:
#line 1897 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "MONTH");
}
#line 6204 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 327:
#line 1901 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DAY");
}
#line 6212 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 328:
#line 1905 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "HOUR");
}
#line 6220 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 329:
#line 1909 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "MINUTE");
}
#line 6228 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 330:
#line 1916 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BOOLEAN");
}
#line 6236 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 331:
#line 1923 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DATE");
}
#line 6244 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 332:
#line 1927 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME("+(yyvsp[-4].node)->text()+") WITH TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 6253 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 333:
#line 1932 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME("+(yyvsp[-4].node)->text()+") WITHOUT TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 6262 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 334:
#line 1937 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6271 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 335:
#line 1942 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME WITH TIME ZONE");
}
#line 6279 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 336:
#line 1946 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIME WITHOUT TIME ZONE");
}
#line 6287 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 337:
#line 1950 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP("+(yyvsp[-4].node)->text()+") WITH TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 6296 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 338:
#line 1955 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP("+(yyvsp[-4].node)->text()+") WITHOUT TIME ZONE");
    delete((yyvsp[-4].node));
}
#line 6305 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 339:
#line 1960 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6314 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 340:
#line 1965 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP WITH TIME ZONE");
}
#line 6322 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 341:
#line 1969 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TIMESTAMP WITHOUT TIME ZONE");
}
#line 6330 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 344:
#line 1981 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NUMERIC("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 6339 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 345:
#line 1986 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NUMERIC("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6348 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 346:
#line 1991 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 6357 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 347:
#line 1996 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6366 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 348:
#line 2001 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DEC("+(yyvsp[-3].node)->text()+","+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-3].node));delete((yyvsp[-1].node));
}
#line 6375 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 349:
#line 2006 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DEC("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6384 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 350:
#line 2011 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DEC");
}
#line 6392 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 351:
#line 2015 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "SMALLINT");
}
#line 6400 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 352:
#line 2019 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "INTEGER");
}
#line 6408 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 353:
#line 2023 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "INT");
}
#line 6416 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 354:
#line 2027 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BIGINT");
}
#line 6424 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 355:
#line 2031 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NUMERIC");
}
#line 6432 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 356:
#line 2035 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DECIMAL");
}
#line 6440 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 357:
#line 2042 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FLOAT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6449 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 358:
#line 2047 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "DOUBLE PRECISION");
}
#line 6457 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 359:
#line 2051 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "FLOAT");
}
#line 6465 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 360:
#line 2055 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "REAL");
}
#line 6473 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 361:
#line 2062 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6482 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 362:
#line 2067 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6491 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 363:
#line 2072 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6500 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 364:
#line 2077 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6509 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 365:
#line 2082 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "VARCHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6518 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 366:
#line 2087 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6527 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 367:
#line 2092 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER LARGE OBJECT");
}
#line 6535 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 368:
#line 2096 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6544 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 369:
#line 2101 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR LARGE OBJECT");
}
#line 6552 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 370:
#line 2105 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CLOB("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6561 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 371:
#line 2110 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CLOB");
}
#line 6569 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 372:
#line 2114 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHAR");
}
#line 6577 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 373:
#line 2118 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTER");
}
#line 6585 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 374:
#line 2125 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BINARY LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6594 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 375:
#line 2130 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BINARY LARGE OBJECT");
}
#line 6602 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 376:
#line 2134 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BLOB("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6611 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 377:
#line 2139 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "BLOB");
}
#line 6619 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 378:
#line 2146 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6628 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 379:
#line 2151 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER");
}
#line 6636 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 380:
#line 2155 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6645 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 381:
#line 2160 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHAR");
}
#line 6653 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 382:
#line 2164 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6662 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 383:
#line 2169 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6671 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 384:
#line 2174 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHAR VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6680 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 385:
#line 2179 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR VARYING("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6689 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 386:
#line 2184 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6698 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 387:
#line 2189 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NATIONAL CHARACTER LARGE OBJECT");
}
#line 6706 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 388:
#line 2193 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR LARGE OBJECT("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6715 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 389:
#line 2198 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR LARGE OBJECT");
}
#line 6723 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 390:
#line 2202 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCLOB("+(yyvsp[-1].node)->text()+")");
    delete((yyvsp[-1].node));
}
#line 6732 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 391:
#line 2207 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCLOB");
}
#line 6740 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 392:
#line 2211 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "NCHAR");
}
#line 6748 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 393:
#line 2218 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-2].node)->text()+" "+(yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text());
    delete((yyvsp[-2].node));delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6757 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 394:
#line 2223 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6766 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 395:
#line 2228 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[-1].node)->text()+" "+(yyvsp[0].node)->text());
    delete((yyvsp[-1].node));delete((yyvsp[0].node));
}
#line 6775 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 396:
#line 2233 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, (yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 6784 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 397:
#line 2241 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CHARACTERS");
}
#line 6792 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 398:
#line 2245 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "CODE_UNITS");
}
#line 6800 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 399:
#line 2249 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "OCTETS");
}
#line 6808 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 400:
#line 2256 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "K");
}
#line 6816 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 401:
#line 2260 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "M");
}
#line 6824 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 402:
#line 2264 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "G");
}
#line 6832 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 403:
#line 2271 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_ALL, "ALL");
}
#line 6840 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 404:
#line 2275 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_DISTINCT, "DISTINCT");
}
#line 6848 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 405:
#line 2281 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.ival) = 0; }
#line 6854 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 406:
#line 2282 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.ival) = 1; }
#line 6860 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 407:
#line 2283 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.ival) = 2; }
#line 6866 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 408:
#line 2287 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 6872 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 410:
#line 2292 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = (yyvsp[0].node); }
#line 6878 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 414:
#line 2304 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "COLLATE "+(yyvsp[0].node)->text());
    delete((yyvsp[0].node));
}
#line 6887 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 419:
#line 2321 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "INT"); }
#line 6893 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 420:
#line 2322 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ARRAY"); }
#line 6899 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 421:
#line 2323 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BINARY"); }
#line 6905 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 422:
#line 2324 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIMESTAMP"); }
#line 6911 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 423:
#line 2325 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIME");  }
#line 6917 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 424:
#line 2326 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CHARACTERS"); }
#line 6923 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 425:
#line 2327 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DATE"); }
#line 6929 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 426:
#line 2328 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FLOAT"); }
#line 6935 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 427:
#line 2329 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CHAR"); }
#line 6941 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 428:
#line 2330 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NCHAR"); }
#line 6947 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 429:
#line 2331 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CHARACTER"); }
#line 6953 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 430:
#line 2332 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NUMERIC"); }
#line 6959 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 431:
#line 2333 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DECIMAL"); }
#line 6965 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 432:
#line 2334 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "REAL"); }
#line 6971 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 433:
#line 2335 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "BIGINT"); }
#line 6977 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 434:
#line 2336 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "INTEGER"); }
#line 6983 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 435:
#line 2337 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SMALLINT"); }
#line 6989 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 436:
#line 2338 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "VARCHAR"); }
#line 6995 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 437:
#line 2342 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "K"); }
#line 7001 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 438:
#line 2343 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "M"); }
#line 7007 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 439:
#line 2344 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "G"); }
#line 7013 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 440:
#line 2345 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CAST"); }
#line 7019 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 441:
#line 2346 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CODE_UNITS"); }
#line 7025 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 442:
#line 2347 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CORRESPONDING"); }
#line 7031 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 443:
#line 2348 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FOLLOWING"); }
#line 7037 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 444:
#line 2349 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "INTERVAL"); }
#line 7043 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 445:
#line 2350 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "LARGE"); }
#line 7049 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 446:
#line 2351 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "MULTISET"); }
#line 7055 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 447:
#line 2352 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OBJECT"); }
#line 7061 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 448:
#line 2353 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OCTETS"); }
#line 7067 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 449:
#line 2354 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ONLY"); }
#line 7073 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 450:
#line 2355 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECEDING"); }
#line 7079 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 451:
#line 2356 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "PRECISION"); }
#line 7085 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 452:
#line 2357 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "RECURSIVE"); }
#line 7091 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 453:
#line 2358 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "REF"); }
#line 7097 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 454:
#line 2359 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ROW"); }
#line 7103 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 455:
#line 2360 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SCOPE"); }
#line 7109 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 456:
#line 2361 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SECOND"); }
#line 7115 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 457:
#line 2362 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "UNBOUNDED"); }
#line 7121 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 458:
#line 2363 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "WITHOUT"); }
#line 7127 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 459:
#line 2364 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ZONE"); }
#line 7133 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 460:
#line 2365 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "FOR"); }
#line 7139 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 461:
#line 2366 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OF"); }
#line 7145 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 462:
#line 2367 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "READ"); }
#line 7151 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 463:
#line 2368 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "DESC"); }
#line 7157 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 464:
#line 2369 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "TIES"); }
#line 7163 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 465:
#line 2370 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "SETS"); }
#line 7169 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 466:
#line 2371 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "OTHERS"); }
#line 7175 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 467:
#line 2372 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "EXCLUDE"); }
#line 7181 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 468:
#line 2373 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "ASC"); }
#line 7187 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 469:
#line 2374 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "COALESCE"); }
#line 7193 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 470:
#line 2375 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "CONVERT"); }
#line 7199 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 471:
#line 2376 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = Node::makeTerminalNode(E_IDENTIFIER, "NULLIF"); }
#line 7205 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 480:
#line 2393 "sqlparser_sql2003.y" /* yacc.c:1646  */
    { (yyval.node) = nullptr; }
#line 7211 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 481:
#line 2395 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeTerminalNode(E_STRING, "TOP "+(yyvsp[0].node)->text() );
    delete((yyvsp[0].node));
}
#line 7220 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;

  case 483:
#line 2403 "sqlparser_sql2003.y" /* yacc.c:1646  */
    {
    (yyval.node) = Node::makeNonTerminalNode(E_EXPR_LIST_WITH_PARENS, E_PARENS_PROPERTY_CNT, (yyvsp[-1].node));
    (yyval.node)->serialize_format = &SINGLE_WITH_PARENS_SERIALIZE_FORMAT;
}
#line 7229 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
    break;


#line 7233 "sqlparser_sql2003_bison.cpp" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (&yylloc, result, scanner, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (&yylloc, result, scanner, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, result, scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp, result, scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, result, scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, result, scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp, result, scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 2408 "sqlparser_sql2003.y" /* yacc.c:1906  */

/*********************************
 ** Section 4: Additional C code
 *********************************/

/* empty */
