package com.nextlabs.jdbc.driverproxy;
import java.sql.*;
import java.io.IOException;

public class nxlEnforcer {
    static {


        String cfg_path = "";
        String sys32_64 = System.getProperty("sun.arch.data.model");
        String os = System.getProperty("os.name");
        if (os.toLowerCase().contains("windows")) {
            cfg_path = "C:\\ProgramData\\Nextlabs\\EMDatabase\\config\\config.ini";
            String path = "";
            try {
                //C:\ProgramData\Nextlabs\EMDatabase\config\
                path = nxlUtil.readCfgValue("","GLOBAL","install_path","");
            } catch (IOException e) {
                throw new ExceptionInInitializerError(e.getMessage());
            }
            if(sys32_64.compareTo("64") == 0){
                System.load(path+"\\Common\\bin64\\sqlenforcer.dll");
            }else{
                System.load(path+"\\Common\\bin32\\sqlenforcer.dll");
            }

        }
        else {
            cfg_path = "/usr/nextlabs/emdb/config/config.ini";
            //cfg_path = "/home/joy_wu/EMDatabase/config/config.ini";
            String path = "";
            try {
                path = nxlUtil.readCfgValue("","GLOBAL","install_path","");
            } catch (IOException e) {
                throw new ExceptionInInitializerError(e.getMessage());
            }

            if(sys32_64.compareTo("64") == 0){
                System.load(path+"/Common/bin64/libsqlenforcer.so");
            }else{
                System.load(path+"/Common/bin32/libsqlenforcer.so");
            }
        }

        try{
            SQLEnforcerInit("jdbc",cfg_path);
        } catch (SQLException e) {
            throw new ExceptionInInitializerError(e.getMessage());
        }
    }

    private native boolean sqlEnforcerInit(String moduleName,String configFile, nxlSqlException exc);
    private native long sqlEnforcerNewContext(String userID,String password, nxlSqlException exc);
    private native void sqlEnforcerFreeContext(String userID);
    private native nxlEnforcedSqlResult sqlEnforcerEvaluation(long context,String sqlText, nxlSqlException exc);
    private native void  sqlEnforcerUserContext(long enforcerContext,int contextType,String contextInfo);
    private native int sqlEnforcerWriteLog(int level,String message);
    private native void sqlAddDBUserInfo(long enforcerContext, String attr, String val);


    public static boolean SQLEnforcerInit(String moduleName,String configFile)throws SQLException{
        nxlSqlException exc = new nxlSqlException();
        //System.out.println("CONFIG PATH:"+configFile);
        boolean bRet = new nxlEnforcer().sqlEnforcerInit(moduleName,configFile,exc);
        if( exc.IsException() )
            throw  new SQLException(exc.GetErrorString());
        return bRet;
    }
    public static long SQLEnforcerNewContext(String userID,String password)throws SQLException{
        nxlSqlException exc = new nxlSqlException();
        long lRet = new nxlEnforcer().sqlEnforcerNewContext(userID,password,exc);
        if( exc.IsException() )
            throw  new SQLException(exc.GetErrorString());
        return lRet;
    }
    public static void SQLEnforcerFreeContext(String userID){
        new nxlEnforcer().sqlEnforcerFreeContext(userID);
    }

    public static nxlEnforcedSqlResult SQLEnforcerEvaluation(long context,String sqlText)throws SQLException{
        nxlSqlException exc = new nxlSqlException();
        nxlEnforcedSqlResult ret =  new nxlEnforcer().sqlEnforcerEvaluation(context,sqlText,exc);
        if( exc.IsException() )
            throw  new SQLException(exc.GetErrorString());
        return ret;
    }
    public static void SQLEnforcerUserContext(long enforcerContext,int contextType,String contextInfo){
        new nxlEnforcer().sqlEnforcerUserContext(enforcerContext,contextType,contextInfo);
    }
    public static int SQLEnforcerWriteLog(int level,String message){
        return new nxlEnforcer().sqlEnforcerWriteLog(level,message);
    }

    public static void SQLAddDBUserInfo(long enforcerContext, String attr, String val) {
        new nxlEnforcer().sqlAddDBUserInfo(enforcerContext, attr, val);
    }

//     public void calledByC(String value)
//     {
//         System.out.println(value);
//         System.out.println("begin call back");
//     }


}
