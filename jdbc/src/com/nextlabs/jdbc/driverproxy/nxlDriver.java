package com.nextlabs.jdbc.driverproxy;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;
import java.io.*;




public class nxlDriver implements java.sql.Driver {
    static {
        try
        {
            DriverManager.registerDriver(new nxlDriver());
        }
        catch (SQLException E)
        {
            throw new RuntimeException("Can't register driver!");
        }
    }
    @Override
    public Connection connect(String url, Properties info) throws SQLException{
//        try{
//            File file =new File("/home/joy_wu/EMDatabase/config/log_jeff.txt");
//            if(!file.exists()){
//                file.createNewFile();
//            }
//            FileWriter fileWritter = new FileWriter(file.getName(),true);
////            fileWritter.write(url);
////            fileWritter.close();
////            System.out.println("url");
//            Writer out =new FileWriter(file);
//            out.write(url);
//            out.close();
//
//        }catch(IOException e){
//            e.printStackTrace();
//        }
//         if (url!=null){
//             System.out.println("url="+url);
//         }

        if (acceptsURL(url)) {
            try {
                //nxlEnforcer.SQLEnforcerInit("jdbc","C:\\ProgramData\\Nextlabs\\EMDatabase\\config\\config.ini");
                return new nxlConnection(url,info);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null ;
        } else {
            //throw new SQLException("Wrong url specified");
            return null ;
        }
    }

    @Override
    public boolean acceptsURL(String url) throws SQLException {
        if (url==null) {
            //System.out.println("url=null");
            throw new SQLException("Wrong url specified url=null");
            //return false;
        }
        //boolean ret = url.endsWith(";targetD=emdb");
        //return !ret;
        int intIndex = url.indexOf("nextlabs:");
        return (intIndex != -1);
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        return new DriverPropertyInfo[0];
    }

    @Override
    public int getMajorVersion() {
        return 1;
    }

    @Override
    public int getMinorVersion() {
        return 0;
    }

    @Override
    public boolean jdbcCompliant() {
        return false;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
