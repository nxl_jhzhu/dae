package com.nextlabs.jdbc.driverproxy;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class nxlUtil {
    public static void writeSQL2Log(String values) {
        String filePath;
        String fileName;

        if (isLinux()) {
            filePath = "/usr/nextlabs/emdb/sqllog";
        } else {
            filePath = "C:/ProgramData/Nextlabs/EMDatabase/sqllog";
        }

        File dir = new File(filePath);
        FileWriter writer = null;

        if (!dir.exists()) {
            dir.mkdirs();
        }
        File checkFile = new File(filePath + "/log.txt");
        try {
            if (!checkFile.exists()) {
                checkFile.createNewFile();
            }

            writer = new FileWriter(checkFile, true);
            writer.append(values).append("\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean isLinux() {
        String osName = System.getProperty("os.name").toLowerCase();
        return osName.contains("linux");
    }

    public static boolean isWindows() {
        String osName = System.getProperty("os.name").toLowerCase();
        return osName.contains("windows");
    }

    /**
     * @param file
     * @param section
     * @param variable
     * @param defaultValue
     * @return value
     * @throws IOException
     */
    public static String readCfgValue(String file, String section, String variable, String defaultValue) throws IOException {


        String cfg_path = "";
        String os = System.getProperty("os.name");
        if (os.toLowerCase().contains("windows")) {
            cfg_path = "C:\\ProgramData\\Nextlabs\\EMDatabase\\config\\config.ini";
        }
        else {
            cfg_path = "/usr/nextlabs/emdb/config/config.ini";
            //cfg_path = "/home/joy_wu/EMDatabase/config/config.ini";/*todo this need modify */
        }


        String strLine, value = "";
        int lineNumber = 0;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(cfg_path),"UTF-8"));
        boolean isInSection = false;
        try {
            while ((strLine = bufferedReader.readLine()) != null) {
                ++lineNumber;
                
                strLine = strLine.trim();

                if (1 == lineNumber) {
                    strLine = removeUTF8BOM(strLine);
                }

                Pattern p;
                Matcher m;
                p = Pattern.compile("\\[\\w+]");//Pattern.compile("file://[//s*.*//s*//]");
                m = p.matcher((strLine));
                if (m.matches()) {
                    p = Pattern.compile("\\[" + section + "\\]");//Pattern.compile("file://[//s*" + section + "file://s*//]");
                    m = p.matcher(strLine);
                    if (m.matches()) {
                        isInSection = true;
                    } else {
                        isInSection = false;
                    }
                }
                if (isInSection == true) {
                    strLine = strLine.trim();
                    String[] strArray = strLine.split("=");
                    if (strArray.length == 1) {
                        value = strArray[0].trim();
                        if (value.equalsIgnoreCase(variable)) {
                            value = "";
                            return value;
                        }
                    } else if (strArray.length == 2) {
                        value = strArray[0].trim();
                        if (value.equalsIgnoreCase(variable)) {
                            value = strArray[1].trim();
                            return value;
                        }
                    } else if (strArray.length > 2) {
                        value = strArray[0].trim();
                        if (value.equalsIgnoreCase(variable)) {
                            value = strLine.substring(strLine.indexOf("=") + 1).trim();
                            return value;
                        }
                    }
                }
            }
        } finally {
            bufferedReader.close();
        }
        return defaultValue;
    }

    private static String removeUTF8BOM(String s) {
        // FEFF because this is the Unicode char represented by the UTF-8 byte order mark (EF BB BF).
        final String UTF8_BOM = "\uFEFF";

        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }

        return s;
    }
}
