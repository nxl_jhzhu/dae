#include "NXLHttpClient.h"
#include "root_certificates.hpp"
#include <iostream>
#include <boost/bind.hpp>


NXLHttpClient::NXLHttpClient(const char* szServer, const char* szPort) :deadline_(io_context),
m_SSLCtx{ ssl::context::sslv23_client }
{
	m_strServer = szServer;
	std::transform(m_strServer.begin(), m_strServer.end(), m_strServer.begin(), tolower);

	m_strPort = szPort;

	m_pHttpsStream = NULL;
	m_pHttpSocket = NULL;

	//get host
	std::string strProtocol = IsHttps() ? "https://" : "http://";
	auto index = m_strServer.find(strProtocol);

	if (index != std::string::npos)
    {
        m_strHost = m_strServer.substr(index + strProtocol.size());
    }

	m_bConnected = false;
}

NXLHttpClient::~NXLHttpClient()
{
	Clear();
}

void NXLHttpClient::Reset()
{
	Clear();
	m_bConnected = false;
}

void NXLHttpClient::Clear()
{
	if (m_pHttpsStream)
	{
		m_pHttpsStream->next_layer().close();
		delete m_pHttpsStream;
		m_pHttpsStream = NULL;
	}

	if (m_pHttpSocket)
	{
		m_pHttpSocket->close();
		delete m_pHttpSocket;
		m_pHttpSocket = NULL;
	}
}

bool NXLHttpClient::IsHttps()
{

  size_t nFind =m_strServer.find("https://");

  return (nFind == 0);

}

bool NXLHttpClient::ConnectToServer()
{
	if (IsHttps())
	{
		return ConnectToHttpsServer();
	}
	else
	{
		return ConnectToHttpServer();
	}
}

bool NXLHttpClient::ConnectToHttpServer()
{
	tcp::resolver resolver{ io_context };

	// Look up the domain name
    auto const lookup = resolver.resolve({ m_strHost.c_str(), m_strPort.c_str() });

    // Make the connection on the IP address we get from a lookup
    m_pHttpSocket = new tcp::socket(io_context);
    boost::asio::connect(*m_pHttpSocket, lookup);

    return true;
}

bool NXLHttpClient::ConnectToHttpsServer()
{
	tcp::resolver resolver{ m_ioService };

	m_pHttpsStream = new ssl::stream<tcp::socket>{ m_ioService, m_SSLCtx};
	//m_pHttpsStream->set_verify_mode(SSL_VERIFY_PEER); //we didn't verify the certificate

	// Look up the domain name
	auto const lookup = resolver.resolve({ m_strHost.c_str(), m_strPort.c_str()});

	// Make the connection on the IP address we get from a lookup
	boost::asio::connect(m_pHttpsStream->next_layer(), lookup);

	// Perform the SSL handshake
	m_pHttpsStream->handshake(ssl::stream_base::client);

	return true;
}

bool NXLHttpClient::Request(http::request<http::string_body>& refReq,
	boost::beast::flat_buffer& refBuf,
	http::response<http::string_body>& refResp
	)
{
	//connect to server
	if (!m_bConnected)
	{
		m_bConnected = ConnectToServer();
	}

	//get http header
	refReq.set(http::field::host, m_strHost.c_str());

	//send and get response
	if (IsHttps())
	{
		// Send the HTTP request to the remote host
		http::write(*m_pHttpsStream, refReq);

		// Receive the HTTP response
		http::read(*m_pHttpsStream, refBuf, refResp);
	}
	else
    {
        stopped_ = false;
        btimeout_ = false;
        deadline_.async_wait(boost::bind(&NXLHttpClient::CheckDeadline, this));

        deadline_.expires_after(boost::asio::chrono::seconds(5));

        http::write(*m_pHttpSocket, refReq);

        http::async_read(*m_pHttpSocket, refBuf, refResp, boost::bind(&NXLHttpClient::OnRead, this, boost::placeholders::_1, boost::placeholders::_2));

        io_context.run();

        if(btimeout_){
            throw;
        }


    }


	return true;
}

void NXLHttpClient::OnRead(boost::beast::error_code  ec, std::size_t n){
    boost::ignore_unused(n);


    // Gracefully close the socket
    //m_pHttpSocket->shutdown(tcp::socket::shutdown_both, ec);

    // not_connected happens sometimes so don't bother reporting it.
//    if(ec && ec != boost::beast::errc::not_connected)
//        return ;
    stopped_ = true;
    deadline_.cancel();
}

void NXLHttpClient::CheckDeadline()
{
    if (stopped_)
        return;

    // Check whether the deadline has passed. We compare the deadline against
    // the current time since a new asynchronous operation may have moved the
    // deadline before this actor had a chance to run.
    if (deadline_.expiry() <= steady_timer::clock_type::now())
    {
        // The deadline has passed. The socket is closed so that any outstanding
        // asynchronous operations are cancelled.
        // m_pHttpSocket->cancel();
        m_pHttpSocket->close();
//        delete  m_pHttpSocket;
//        m_pHttpSocket = NULL;

        // There is no longer an active deadline. The expiry is set to the
        // maximum time point so that the actor takes no action until a new
        // deadline is set.
        deadline_.expires_at(steady_timer::time_point::max());
    }

    // Put the actor back to sleep.
    deadline_.async_wait(boost::bind(&NXLHttpClient::CheckDeadline, this));
}